import { enableProdMode, ReflectiveInjector   } from '@angular/core';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {HttpClient,XhrFactory,HttpClientModule,HttpHandler,HttpXhrBackend,HttpBackend,HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Interceptor } from './app/shared/service/httpinterceptor';
import useragent from 'useragent';

//const injector = ReflectiveInjector.resolveAndCreate(getAnnotations(HttpClientModule)[0].providers);
export class BrowserXhr implements XhrFactory {
	build(): any { return <any>(new XMLHttpRequest()); }
  }
  
const injector = ReflectiveInjector.resolveAndCreate([
	HttpClient,
	HttpXhrBackend,
	HttpClientModule,
	{ provide: HttpBackend, useExisting: HttpXhrBackend },
	{ provide: HttpHandler, useExisting: HttpBackend },
	{ provide: XhrFactory, useClass: BrowserXhr},
	{
		provide: HTTP_INTERCEPTORS,
		useClass: Interceptor,
		multi: true
	  }
  ]);
var httpService = injector.get(HttpClient)
if (environment.production) {
  enableProdMode();
}




let ua = useragent.is(navigator.userAgent);
var ua_ver = useragent.parse(navigator.userAgent);
let suport = true;
let browserName="";
let isEdgeBrowser = false;
let edgeVersion = 0;

if(ua.ie){
	browserName="IE";
}
else if(ua.chrome){
	var isEdge = window.navigator.userAgent;
	var edge = isEdge.indexOf('Edge/');
	if (edge > 0) {
    isEdgeBrowser = true;
	browserName="Edge";
    edgeVersion = parseInt(isEdge.substring(edge + 5, isEdge.indexOf('.', edge)), 10);
	console.log(edgeVersion);
    }else{
		browserName="Chrome";
	}
}
else if(ua.firefox){
	browserName="Firefox";
}else if(ua.safari){
	browserName="Safari";
}else if(ua.mozilla){
	browserName="Mozilla";
}else if(ua.opera){
	browserName="Opera";
}else if(ua.edge){
	browserName="Edge";
}
else{
	suport = false;
	document.getElementById('browser-not-support').style.display = 'block';
}
httpService.get("/api/auth/BrowserList/?currentBrowser="+browserName).subscribe(data => {
	if(data['code']=="succeed" && data['code'] !== null){
	  let acceptedBrowserVersion = 0;
	  let acceptedBrowsers = data;
	  let currentBrowser=parseInt(ua_ver.major);
	  if(isEdgeBrowser === false){
		  acceptedBrowserVersion=parseInt(acceptedBrowsers['version']);
	  }else{
		  acceptedBrowserVersion = edgeVersion;
	  }
	  
	
	if(acceptedBrowserVersion > 0){
	  if(currentBrowser >= acceptedBrowserVersion)
	  {
		suport = true;
		platformBrowserDynamic().bootstrapModule(AppModule);
	  }else{
		document.getElementById('browser-not-support').style.display = 'block';
	  }
	}else{
		suport = false;
		document.getElementById('browser-not-support').style.display = 'block';
	}

	}else{
		suport = false;
		document.getElementById('browser-not-support').style.display = 'block';
	}
  
	
  });

  
var User_info = localStorage.getItem('autodesk-data')
let Json_User_info = JSON.parse(User_info)
	if(Json_User_info.UserLevelId == 'STUDENT'){
		checkIdle();
	}
function checkIdle(){
	var IDLE_TIMEOUT = 900; 
	var idleSecondsCounter = 0;
	document.onclick = function() {
			idleSecondsCounter = 0;
	};
	document.onmousemove = function() {
			idleSecondsCounter = 0;
	};
	document.onkeypress = function() {
			idleSecondsCounter = 0;
	};
	window.setInterval(CheckIdleTime, 1000);

	function CheckIdleTime() {
			idleSecondsCounter++;
			if (idleSecondsCounter >= IDLE_TIMEOUT) {
				var User_info = localStorage.getItem('User')
				if(User_info){
					let Json_User_info = JSON.parse(User_info)
					if(Json_User_info.UserLevelId == 'STUDENT'){
						localStorage.clear();
						location.reload();
					}
				}else{
					idleSecondsCounter = 0;
				}
			}
	}
}
