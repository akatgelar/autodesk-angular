import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-profile-eim',
  templateUrl: './profile-eim.component.html',
  styleUrls: [
    './profile-eim.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ProfileEIMComponent implements OnInit {

  private _serviceUrl = 'api/ContactAll';
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  changepasswordform: FormGroup;
  changeemailform: FormGroup;
  myprofile: FormGroup;
  public useraccesdata: any;
  ProfilePicture: string;
  public success: Boolean = false;
  public UserLevelName : string = "";
  private isInstructor: Boolean=false;

  // get data
  honorific;
  gender;
  primarylanguage;
  secondarylanguage;
  primaryindustry;
  stateprovince;
  country;

  constructor(private http: Http, private _http: HttpClient, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    // if(this.useraccesdata.UserLevelId == 'ORGANIZATION'){
    //   this.UserLevelName = "Site Administrator";
    // } else{
    //   this.UserLevelName = this.useraccesdata.UserLevelId
    // }

    //validation
    let ContactId = new FormControl('');
    let FirstName = new FormControl('');
    let LastName = new FormControl('');
    let Salutation = new FormControl('');
    let Gender = new FormControl('');
    let PrimaryIndustryId = new FormControl('');
    let PrimaryLanguage = new FormControl('');
    let SecondaryLanguage = new FormControl('');
    let Address1 = new FormControl('');
    let Address2 = new FormControl('');
    let Address3 = new FormControl('');
    let City = new FormControl('');
    let StateProvince = new FormControl('');
    let CountryCode = new FormControl('');
    let PostalCode = new FormControl('');
    let WebsiteUrl = new FormControl('');
    let Bio = new FormControl('');
    let EmailAddress = new FormControl('');
    let Telephone1 = new FormControl('');
    let Mobile1 = new FormControl('');
    let DoNotEmail = new FormControl('');
    let ShareEmail = new FormControl('');
    let ShareTelephone = new FormControl('');
    let ShareMobile = new FormControl('');
    let ShowInSearch = new FormControl('');
    let MobileCode = new FormControl('');
    let TelephoneCode = new FormControl('');

    this.myprofile = new FormGroup({
      ContactId: ContactId,
      FirstName: FirstName,
      LastName: LastName,
      Salutation: Salutation,
      Gender: Gender,
      PrimaryIndustryId: PrimaryIndustryId,
      PrimaryLanguage: PrimaryLanguage,
      SecondaryLanguage: SecondaryLanguage,
      Address1: Address1,
      Address2: Address2,
      Address3: Address3,
      City: City,
      StateProvince: StateProvince,
      CountryCode: CountryCode,
      PostalCode: PostalCode,
      WebsiteUrl: WebsiteUrl,
      Bio: Bio,
      EmailAddress: EmailAddress,
      Telephone1: Telephone1,
      Mobile1: Mobile1,
      DoNotEmail: DoNotEmail,
      ShareEmail: ShareEmail,
      ShareTelephone: ShareTelephone,
      ShareMobile: ShareMobile,
      ShowInSearch: ShowInSearch,
      MobileCode: MobileCode,
      TelephoneCode: TelephoneCode
    });
  }

  id: string;
  ngOnInit() {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/Profile/' + this.useraccesdata.UserId, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = '';
        }
        else {
          this.data = result;
          this.buildForm();
          // this.getProfulePicture(this.data.ProfilePicture);
        }
      },
      error => {
        this.service.errorserver();
        this.data = '';
      });
      this.isInstructor=this.IsInstructorlvl(this.useraccesdata.UserLevelId);
	  
    /* fixed issue show role access related with -> issue 24092018 - populate country by role site admin */
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
      this.service.httpClientGet('api/UserLevel/' + userlvlarr[i], data)
      .subscribe(result => {
        let userLvl:any = result;
        if (result == "Not found") {
          this.UserLevelName += 'None';
        }
        else {
          this.UserLevelName += "<code>> </code>"+userLvl.Name+"<br/>";
        }
      },
      error => {
        this.UserLevelName += 'None';
      });
    }
    /* end line fixed issue show role access related with -> issue 24092018 - populate country by role site admin */

    this.getHonorfic();
    this.getGender();
    this.getPrimaryIndustry();
    this.getPrimaryLanguage();
    this.getSecondaryLanguage();
    this.getCountry();

  }

  // getProfulePicture(picture) {
  //   this.http.get("assets/profilepictures/" + picture)
  //     .map((response) => response.status)
  //     .catch((error) => Observable.of(error.status || 404))
  //     .subscribe((status) => {
  //       if (status == 200) {
  //         this.ProfilePicture = "assets/profilepictures/" + picture;
  //       }
  //       else {
  //         this.ProfilePicture = "assets/profilepictures/user.png";
  //       }
  //     });
  // }

  buildForm(): void {
    var GetDoNotEmail;
    if (this.data.DoNotEmail == 'Y') {
      GetDoNotEmail = true;
    }
    else {
      GetDoNotEmail = false;
    }

    var GetShowInSearch;
    if (this.data.ShowInSearch == '1') {
      GetShowInSearch = true;
    }
    else {
      GetShowInSearch = false;
    }

    var GetShareEmail;
    if (this.data.ShareEmail == '1') {
      GetShareEmail = true;
    }
    else {
      GetShareEmail = false;
    }

    var GetShareTelephone;
    if (this.data.ShareTelephone == '1') {
      GetShareTelephone = true;
    }
    else {
      GetShareTelephone = false;
    }

    var GetShareMobile;
    if (this.data.ShareMobile == '1') {
      GetShareMobile = true;
    }
    else {
      GetShareMobile = false;
    }

    let ContactId = new FormControl(this.data.ContactId);
    let FirstName = new FormControl(this.data.FirstName);
    let LastName = new FormControl(this.data.LastName);
    let Salutation = new FormControl(this.data.Salutation);
    let Gender = new FormControl(this.data.Gender);
    let PrimaryIndustryId = new FormControl(this.data.PrimaryIndustryId);
    let PrimaryLanguage = new FormControl(this.data.PrimaryLanguage);
    let SecondaryLanguage = new FormControl(this.data.SecondaryLanguage);
    let Address1 = new FormControl(this.data.Address1);
    let Address2 = new FormControl(this.data.Address2);
    let Address3 = new FormControl(this.data.Address3);
    let City = new FormControl(this.data.City);
    let CountryCode = new FormControl(this.data.CountryCode);
    let StateProvince = new FormControl(this.data.StateProvince);
    let PostalCode = new FormControl(this.data.PostalCode);
    let WebsiteUrl = new FormControl(this.data.WebsiteUrl);
    let Bio = new FormControl(this.data.Bio);
    let EmailAddress = new FormControl(this.data.EmailAddress);
    let Telephone1 = new FormControl(this.data.Telephone1);
    let Mobile1 = new FormControl(this.data.Mobile1);
    let DoNotEmail = new FormControl(GetDoNotEmail);
    let ShareEmail = new FormControl(GetShareEmail);
    let ShareTelephone = new FormControl(GetShareTelephone);
    let ShareMobile = new FormControl(GetShareMobile);
    let ShowInSearch = new FormControl(GetShowInSearch);
    let MobileCode = new FormControl(this.data.MobileCode);
    let TelephoneCode = new FormControl(this.data.TelephoneCode);

    if (this.data.CountryCode != null && this.data.CountryCode != "") {
      this.getStateProvince(this.data.CountryCode);
    }

    this.myprofile = new FormGroup({
      ContactId: ContactId,
      FirstName: FirstName,
      LastName: LastName,
      Salutation: Salutation,
      Gender: Gender,
      PrimaryIndustryId: PrimaryIndustryId,
      PrimaryLanguage: PrimaryLanguage,
      SecondaryLanguage: SecondaryLanguage,
      Address1: Address1,
      Address2: Address2,
      Address3: Address3,
      City: City,
      CountryCode: CountryCode,
      StateProvince: StateProvince,
      PostalCode: PostalCode,
      WebsiteUrl: WebsiteUrl,
      Bio: Bio,
      EmailAddress: EmailAddress,
      Telephone1: Telephone1,
      Mobile1: Mobile1,
      DoNotEmail: DoNotEmail,
      ShareEmail: ShareEmail,
      ShareTelephone: ShareTelephone,
      ShareMobile: ShareMobile,
      ShowInSearch: ShowInSearch,
      MobileCode: MobileCode,
      TelephoneCode: TelephoneCode
    });
  }

  IsInstructorlvl(userlevel){
	  if(userlevel!=null || userlevel!=undefined)
	  {
		  var istrainer=/trainer/gi;
		
		  
		  if(userlevel.toLowerCase().search(istrainer)==-1)
		  {
			  return false;
		  }
		  else{
			  return true;
		  }
	  }
	  else{
		  return false;
	  }
  }
  changeEmail() {
    this.router.navigate(['/change-email', this.useraccesdata.UserId]);
  }

  getHonorfic() {
    var honorific = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Salutation','Status':'A'}", honorific)
      .subscribe(result => {
        this.honorific = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getGender() {
    var gender = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Gender','Status':'A'}", gender)
      .subscribe(result => {
        this.gender = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getPrimaryIndustry() {
    var primaryindustry = '';
    this.service.httpClientGet("api/Industries/", primaryindustry)
      .subscribe(result => {
        this.primaryindustry = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getPrimaryLanguage() {
    var primarylanguage = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", primarylanguage)
      .subscribe(result => {
        this.primarylanguage = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSecondaryLanguage() {
    var secondarylanguage = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", secondarylanguage)
      .subscribe(result => {
        this.secondarylanguage = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getStateProvince(id) {
    var data = '';
    this.service.httpClientGet('api/States/where/{"CountryCode":"' + id + '"}', data)
      .subscribe(result => {
        this.stateprovince = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getCountry() {
    var country = '';
    this.service.httpClientGet("api/Countries/", country)
      .subscribe(result => {
        this.country = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  onSubmit() {
    if (this.myprofile.value.DoNotEmail) {
      this.myprofile.value.DoNotEmail = 'Y';
    }
    else {
      this.myprofile.value.DoNotEmail = 'N';
    }

    if (this.myprofile.value.ShowInSearch) {
      this.myprofile.value.ShowInSearch = 1;
    }
    else {
      this.myprofile.value.ShowInSearch = 0;
    }

    if (this.myprofile.value.ShareEmail) {
      this.myprofile.value.ShareEmail = 1;
    }
    else {
      this.myprofile.value.ShareEmail = 0;
    }

    if (this.myprofile.value.ShareTelephone) {
      this.myprofile.value.ShareTelephone = 1;
    }
    else {
      this.myprofile.value.ShareTelephone = 0;
    }

    if (this.myprofile.value.ShareMobile) {
      this.myprofile.value.ShareMobile = 1;
    }
    else {
      this.myprofile.value.ShareMobile = 0;
    }

    let data = JSON.stringify(this.myprofile.value);

    data = data.replace(/'/g,"\\\\'"); /* issue 27092018 - handling crud with char (') inside */

    /* autodesk plan 10 oct */

    this.service.httpCLientPut(this._serviceUrl + "/ProfileEIM/" + this.useraccesdata.UserId, data)
      .subscribe(result => {
        var data = result;
        if (data['code'] != '1') {
          swal(
            'Information!',
            "Update Data Failed",
            'error'
          );
        }
        else{
          //this.success = true;
          swal(
            'Information!',
            "Update Data Success",
            'success'
          );
          setTimeout(() => {
            this.success = false;
          }, 3000)
          
          this.router.navigate(['/profile']);
        }
      },
      error => {
          this.service.errorserver();
      });

    /* end line autodesk plan 10 oct */

  }

}
