import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import {PasswordValidators} from 'ngx-validators';

@Component({
  selector: 'app-change-password-eim',
  templateUrl: './change-password-eim.component.html',
  styleUrls: [
    './change-password-eim.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ChangePasswordEIMComponent implements OnInit {

  private _serviceUrl = 'api/ContactAll';
  public data: any;
  changepasswordform: FormGroup;
  public useraccesdata:any;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let currentpassword = new FormControl('', Validators.required);
    let password = new FormControl('', [Validators.required, CustomValidators.notEqualTo(currentpassword), Validators.minLength(6),Validators.compose([
      PasswordValidators.digitCharacterRule(1),
      PasswordValidators.specialCharacterRule(1)])]);
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({
      password: password,
      rpassword: rpassword,
      currentpassword: currentpassword
    });

  }

  id: string;
  ngOnInit() {

    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + this.useraccesdata.Email, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = '';
        }
        else {
          this.data = result;
        }
      },
      error => {
        this.service.errorserver();
        this.data = '';
      });

  }

  onSubmit() {
    this.changepasswordform.controls['password'].markAsTouched();
    this.changepasswordform.controls['rpassword'].markAsTouched();
    this.changepasswordform.controls['currentpassword'].markAsTouched();

    if(this.changepasswordform.valid){
      let data = JSON.stringify(this.changepasswordform.value);
      
      // this.service.httpCLientPutPassword(this._serviceUrl+'/ChangePassword',this.data.ContactId, data, '/logout-contact', '/change-password');

      var routingtrue = '/logout-contact';
      var routingtruearr = routingtrue.split(',');

      this.service.httpCLientPut(this._serviceUrl + '/ChangePassword/' + this.data.ContactId, data)
        .subscribe(res => {
          var result = res;
          if (result['code'] == '1') {
            if (routingtruearr.length > 1) {
              this.router.navigate([routingtruearr[0], routingtruearr[1], routingtruearr[2]]);
            } else {
              var routelogoutrole = routingtrue.split('-');
              this.router.navigate([routelogoutrole[0]], { queryParams: { id: this.data.ContactId, role:routelogoutrole[1] } });
            }
          }
          else {
            console.log("failed");
          }
        },
        error => {
          console.log("failed");
        });
    }
  }
}
