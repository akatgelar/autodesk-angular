import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactEIMComponent } from './contact-eim.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";

export const ContactEIMRoutes: Routes = [
  {
    path: '',
    component: ContactEIMComponent,
    data: {
      breadcrumb: 'Add Contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ContactEIMRoutes),
    SharedModule
  ],
  declarations: [ContactEIMComponent],
  providers:[AppService]
})
export class ContactEIMModule { }
