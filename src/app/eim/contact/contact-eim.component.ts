import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import { Router } from '@angular/router';
import {AppService} from "../../shared/service/app.service";

@Component({
  selector: 'app-contact-eim',
  templateUrl: './contact-eim.component.html',
  styleUrls: [
    './contact-eim.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ContactEIMComponent implements OnInit {
  
  private _serviceUrl = 'api/MainContact';
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  messageError: string = '';
  addcontactform: FormGroup;
 
  constructor(public http: Http, private router: Router,private service:AppService){

    //validation
    let FirstName = new FormControl('', Validators.required);
    let LastName = new FormControl('', Validators.required);
    let Honorific = new FormControl('', Validators.required);
    let Gender = new FormControl('', Validators.required);
    let FirstLanguage = new FormControl('', Validators.required);
    let SecondLanguage = new FormControl('', Validators.required);
    let PrimaryIndustry = new FormControl('', Validators.required);
    let Address1 = new FormControl('', Validators.required);
    let Address2 = new FormControl('', Validators.required);
    let Address3 = new FormControl('', Validators.required);
    let City = new FormControl('', Validators.required);
    let State = new FormControl('', Validators.required);
    let Country = new FormControl('', Validators.required);
    let PostalCode = new FormControl('', [Validators.required, CustomValidators.number]);
    let Website = new FormControl('', Validators.required);
    let Bio = new FormControl('', Validators.required);
    let EmailAddress = new FormControl('', [Validators.required,Validators.email]);
    let PhoneNumber = new FormControl('', [Validators.required, CustomValidators.number]);
    let MobileNumber = new FormControl('', [Validators.required, CustomValidators.number]);
    let ServiceRole = new FormControl('', Validators.required);
    let PrimarySite = new FormControl('', Validators.required);
    let CountrySite = new FormControl('', Validators.required);
    let PrefixPhoneNumber = new FormControl('+65');
    let PrefixMobileNumber = new FormControl('+65');

    this.addcontactform = new FormGroup({
      FirstName:FirstName,
      LastName:LastName,
      Honorific:Honorific,
      Gender:Gender,
      FirstLanguage:FirstLanguage,
      SecondLanguage:SecondLanguage,
      PrimaryIndustry:PrimaryIndustry,
      Address1:Address1,
      Address2:Address2,
      Address3:Address3,
      City:City,
      State:State,
      Country:Country,
      PostalCode:PostalCode,
      Website:Website,
      Bio:Bio,
      EmailAddress:EmailAddress,
      PhoneNumber:PhoneNumber,
      MobileNumber:MobileNumber,
      ServiceRole:ServiceRole,
      PrimarySite:PrimarySite,
      CountrySite:CountrySite,
      PrefixPhoneNumber:PrefixPhoneNumber,
      PrefixMobileNumber:PrefixMobileNumber
    });


  }

  ngOnInit() {

    this.http.get(`assets/data/data.json`)
      .subscribe((data)=> {
        this.data = data.json();
      });
  }

  //delete confirm
  openConfirmsSwal() {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      swal(
          'Deleted!',
          'Your file has been deleted.',
          'success'
      )
    }).catch(swal.noop);
  }

  //submit form add contact
  submittedAddContact: boolean;
  onSubmitAddContact() {
    this.submittedAddContact = true;

    this.addcontactform.value.ContactId = '40511';
    this.addcontactform.value.Telephone1 = this.addcontactform.value.PrefixPhoneNumber + this.addcontactform.value.PhoneNumber;
    this.addcontactform.value.Telephone2 = this.addcontactform.value.PrefixMobileNumber + this.addcontactform.value.MobileNumber;
    let data = JSON.stringify(this.addcontactform.value);
    
    //console.log("input -> "+data)
    
    //post action
    this.service.httpClientPost(this._serviceUrl, data)
    .subscribe(result => {
      var resource = result;
      if(resource['code'] == '1') {
          this.service.openSuccessSwal(resource['message']); 
      }
      else{
          swal(
            'Information!',
            "Insert Data Failed",
            'error'
          );
      }
      this.router.navigate(['/eim/contact']);
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  }

  //success notification add contact
  openSuccessSwalAddContact() {
    swal({
      title: 'Success!',
      text: 'Your contact has been added!',
      type: 'success'
    }).catch(swal.noop);
  }

  //add row organization
  private fieldArray: Array<any> = [];
  private newAttribute: any = {};

  addFieldValue() {
      this.fieldArray.push(this.newAttribute)
      this.newAttribute = {};
  }

  deleteFieldValue(index) {
      this.fieldArray.splice(index, 1);
  }

}
