import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, EmailValidator } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';

@Component({
    selector: 'app-change-email-eim',
    templateUrl: './change-email-eim.component.html',
    styleUrls: [
        './change-email-eim.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ChangeEmailComponent implements OnInit {

    private _serviceUrl = 'api/ContactAll';
    public data: any;
    changeemailform: FormGroup;
    public useraccesdata: any;
    private userId;

    constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let Password = new FormControl('', Validators.required);
        let Email = new FormControl('', Validators.required);

        this.changeemailform = new FormGroup({
            Password: Password,
            Email: Email
        });

    }

    ngOnInit() {
        this.userId = this.route.snapshot.params['id'];
    }

    getEmail() {
        return this.changeemailform.get('Email');
    }

    resetForm(){
        this.changeemailform.reset({
            'Password':'',
            'Email':''
        });
    }

    onSubmit() {
        this.changeemailform.controls['Password'].markAsTouched();
        this.changeemailform.controls['Email'].markAsTouched();

        if (this.changeemailform.valid) {
            var data: any;
            this.service.httpClientGet(this._serviceUrl + "/where/{'ContactId':'" + this.userId + "','Password':'" + this.changeemailform.value.Password + "'}", data)
                .subscribe(res => {
                    data = res;
                    if (data.length == 0) {
                        swal('Information!', 'Password not match', 'error');
                        this.changeemailform.patchValue({ Password: "" });
                    } else {
                        var cek_email: any;
                        this.service.httpClientGet(this._serviceUrl + '/where/{"EmailAddress":"' + this.changeemailform.value.Email + '"}', cek_email)
                            .subscribe(result => {
                                cek_email = result;
                                if (cek_email.length == 1) {
                                    swal('Information!', 'Please use another e-mail', 'error');
                                    this.changeemailform.patchValue({ Email: "" });
                                } else {
                                    // let updateEmail = JSON.stringify(this.changeemailform.value);
                                    let updateEmail = JSON.stringify(this.changeemailform.value).replace(/\'/g, "\\\\'"); /* issue 27092018 - handling crud with char (') inside */
                                    var checkRes: any
                                    this.service.httpCLientPut(this._serviceUrl + "/ChangeEmail/" + this.userId, updateEmail)
                                        .subscribe(result => {
                                            checkRes = result;
                                            if (checkRes['code'] == 1) {
                                                swal('Information!', checkRes['message'], 'success');
                                                this.router.navigate(['/logout']);
                                            } else {
                                                swal('Information!', checkRes['message'], 'error');
                                            }
                                        }, error => {
                                            this.service.errorserver();
                                        });
                                }
                            }, error => {
                                this.service.errorserver();
                            });
                    }
                }, error => {
                    this.service.errorserver();
                });
        }
    }

}
