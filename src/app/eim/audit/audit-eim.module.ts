import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditEIMComponent } from './audit-eim.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const AuditEIMRoutes: Routes = [
  {
    path: '',
    component: AuditEIMComponent,
    data: {
      breadcrumb: 'Audit Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuditEIMRoutes),
    SharedModule
  ],
  declarations: [AuditEIMComponent]
})
export class AuditEIMModule { }
