import { Component } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";

@Component({
  selector: "app-title",
  template: "<span></span>"
})
export class TitleComponent {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private translate: TranslateService
  ) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        let currentRoute = this.route.root;
        let title = "";
        do {
          const childrenRoutes = currentRoute.children;
          currentRoute = null;
          childrenRoutes.forEach(routes => {
            if (routes.outlet === "primary") {
              title = routes.snapshot.data.breadcrumb;
              currentRoute = routes;
            }
          });
        } while (currentRoute);

        let titleTemp: string = "";
        let path: any;
        path = (window.location.pathname).split("/").filter(v => v != "");
        if (path[0] == "admin") {
          titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
          // if (path[1] == "email") {
          //   titleTemp = "menu_" + path[0] + "." + path[1] + "_body." + path[1] + "_body";
          // }
          if (path[1] == "activities") {
            if (path[2] == "activities-list") {
              titleTemp = "menu_admin.activities.list_activities";
            }
            else if (path[2] == "activities-add") {
              titleTemp = "menu_admin.activities.add_activities";
            }
            else if (path[2] == "activities-update") {
              titleTemp = "epdb.admin.activities.edit_activities";
            }
          }
          else if (path[1] == "products") {
            if (path[2] == "list-category") {
              titleTemp = "menu_admin.products.list_category";
            }
            else if (path[2] == "add-category") {
              titleTemp = "menu_admin.products.add_category";
            }
            else if (path[2] == "products-category-update") {
              titleTemp = "epdb.admin.product.edit_category";
            }
            else if (path[2] == "products-list") {
              titleTemp = "menu_admin.products.list_product";
            }
            else if (path[2] == "products-add") {
              titleTemp = "menu_admin.products.add_product";
            }
            else if (path[2] == "products-update") {
              titleTemp = "epdb.admin.product.edit_product";
            }
          }
          else if (path[1] == "sku") {
            if (path[2] == "list-sku") {
              titleTemp = "menu_admin.sku.list_sku";
            }
            else if (path[2] == "add-sku") {
              titleTemp = "menu_admin.sku.add_sku";
            }
            else if (path[2] == "update-sku") {
              titleTemp = "epdb.admin.sku.add_edit_sku";
            }
          }
          else if (path[1] == "glossary") {
            if (path[2] == "glossary-list") {
              titleTemp = "menu_admin.glossary.list_glossary";
            }
            else if (path[2] == "glossary-add") {
              titleTemp = "menu_admin.glossary.add_glossary";
            }
            else if (path[2] == "glossary-update") {
              titleTemp = "epdb.admin.glosarry.edit_term";
            }
          }
          else if (path[1] == "country") {
            if (path[2] == "geo-list") {
              titleTemp = "menu_admin.country.list_geo";
            }
            else if (path[2] == "geo-add") {
              titleTemp = "menu_admin.country.add_geo";
            }
            else if (path[2] == "geo-update") {
              titleTemp = "epdb.admin.country.geo.edit_geo";
            }
            else if (path[2] == "territory-list") {
              titleTemp = "menu_admin.country.list_territory";
            }
            else if (path[2] == "territory-add") {
              titleTemp = "menu_admin.country.add_territory";
            }
            else if (path[2] == "territory-update") {
              titleTemp = "epdb.admin.country.territory.edit_territory";
            }
            else if (path[2] == "region-list") {
              titleTemp = "menu_admin.country.list_region";
            }
            else if (path[2] == "region-add") {
              titleTemp = "menu_admin.country.add_region";
            }
            else if (path[2] == "region-update") {
              titleTemp = "epdb.admin.country.region.edit_region";
            }
            else if (path[2] == "sub-region-list") {
              titleTemp = "menu_admin.country.list_sub_region";
            }
            else if (path[2] == "sub-region-add") {
              titleTemp = "menu_admin.country.add_sub_region";
            }
            else if (path[2] == "sub-region-update") {
              titleTemp = "epdb.admin.country.sub_region.edit_sub_region";
            }
            else if (path[2] == "countries-list") {
              titleTemp = "menu_admin.country.list_countries";
            }
            else if (path[2] == "countries-add") {
              titleTemp = "menu_admin.country.add_countries";
            }
            else if (path[2] == "countries-update") {
              titleTemp = "epdb.admin.country.countries.edit_country";
            }
          }

          // Market Type
          else if (path[1] == "market-type") {
            if (path[2] == "market-type-list") {
              titleTemp = "menu_admin.market_type.list_market_type";
            }
            else if (path[2] == "market-type-add") {
              titleTemp = "menu_admin.market_type.add_market_type";
            }
            else if (path[2] == "market-type-update") {
              titleTemp = "epdb.admin.market_type.add_update_market_type";
            }
          }

          // Variable
          else if (path[1] == "variables") {
            if (path[2] == "variables-list") {
              titleTemp = "menu_admin.variables.list_variables";
            }
            else if (path[2] == "variables-add") {
              titleTemp = "menu_admin.variables.add_variables";
            }
            else if (path[2] == "variables-update") {
              titleTemp = "epdb.admin.variables.edit_variable";
            }
          }

          // Partner Type
          else if (path[1] == "partner-type") {
            if (path[2] == "list-sub-partner-type") {
              titleTemp = "menu_admin.partner_type.sub_partner_type";
            }
            else if (path[2] == "add-sub-partner-type") {
              titleTemp = "epdb.admin.sub_partner_type.add_new_sub_partner_type";
            }
            else if (path[2] == "update-sub-partner-type") {
              titleTemp = "epdb.admin.sub_partner_type.edit_sub_partner_type";
            }
            else if (path[2] == "list-partner-type") {
              titleTemp = "menu_admin.partner_type.partner_type";
            }
            else if (path[2] == "add-partner-type") {
              titleTemp = "epdb.admin.partner_type.add_new_partner_type";
            }
            else if (path[2] == "update-partner-type") {
              titleTemp = "epdb.admin.partner_type.edit_partner_type";
            }
          }

          // Distributor
          else if (path[1] == "distributor") {
            if (path[2] == "add-distributor") {
              titleTemp = "menu_admin.distributor.add_distributor";
            }
            else {
              titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
            }
          }

          // Currency
          else if (path[1] == "currency") {
            titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
            if (path[2] == "currency-edit") {
              titleTemp = "epdb.admin.currency.edit_currency";
            }
            else if (path[2] == "currency-conversion-edit") {
              titleTemp = "epdb.admin.currency.edit_currency_conversion";
            }
          }

          // Language
          else if (path[1] == "language") {
            titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
            if (path[2] == "list-language") {
              titleTemp = "menu_admin.language.list_language";
            }
            else if (path[2] == "setup-language") {
              titleTemp = "epdb.admin.language.setup_language";
            }
          }

          // Email Body
          else if (path[1] == "email") {
            titleTemp = "menu_" + path[0] + "." + path[1] + "_body." + path[1] + "_body";
            if (path[2] == "list-email-body") {
              titleTemp = "menu_admin.email_body.list_email_body";
            }
            else if (path[2] == "add-edit-email") {
              titleTemp = "menu_admin.email_body.add_edit_email_body";
            }
          }
        }

        // Dahsboard
        else if (path[0] == "dashboard") {
          titleTemp = "menu_" + path[0] + "." + path[1]; 
        }

        // Manage
        else if (path[0] == "manage") {
          titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
          if (path[1] == "organization") {
            if (path[2] == "search-organization") {
              titleTemp = "menu_manage.organization.search_organization";
            }
            else if (path[2] == "add-organization") {
              titleTemp = "menu_manage.organization.add_organization";
            }
            else if (path[2] == "detail-organization") {
              titleTemp = "epdb.manage.organization.detail.detail_organization";
            }
            else if (path[2] == "edit-organization") {
              titleTemp = "epdb.manage.organization.add_edit.edit_organization";
            }
            else {
              titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
            }
          }
          else if (path[1] == "site") {
            if (path[2] == "search-site") {
              titleTemp = "menu_manage.site.search_site";
            }
            else if (path[2] == "add-site") {
              titleTemp = "epdb.manage.site.add_edit.add_new_site";
            }
            else if (path[2] == "detail-site") {
              titleTemp = "epdb.manage.site.detail.detail_site";
            }
            else if (path[2] == "edit-site") {
              titleTemp = "epdb.manage.site.add_edit.edit_site";
            }
            else {
              titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
            }
          }
          else if (path[1] == "contact") {
            if (path[2] == "search-contact") {
              titleTemp = "menu_manage.contact.search_contact";
            }
            else if (path[2] == "add-new-contact") {
              titleTemp = "menu_manage.contact.add_new_contact";
            }
            else if (path[2] == "detail-contact") {
              titleTemp = "epdb.manage.contact.detail.detail_contact";
            }
            else if (path[2] == "edit-contact") {
              titleTemp = "epdb.manage.contact.add_edit.edit_contact";
            }
            else {
              titleTemp = "menu_" + path[0] + "." + path[1] + "." + path[1];
            }
          }
          else if (path[1] == "search-general") {
            titleTemp = "Search General";
          }
        }

        else if (path[0] == "evaluation") {
          titleTemp = "menu_manage_" + path[0] + ".manage_" + path[0];
          if (path[1] == "survey-template-list") {
            titleTemp = "menu_manage_evaluation.list_template";
          }
          else if (path[1] == "add-edit-templatedata") {
            titleTemp = "menu_manage_evaluation.add_template";
          }
          else if (path[1] == "survey-template-copy") {
            titleTemp = "menu_manage_evaluation.copy_template";
          }
          else if (path[1] == "add-edit-form") {
            titleTemp = "eva.manage_evaluation.add_edit_form";
          }
          else if (path[1] == "update-templatedata") {
            titleTemp = "eva.manage_evaluation.edit_evaluation_template_data";
          }
        }

        else if (path[0] == "course") {
          if (path[1] == "course-list") {
            titleTemp = "menu_manage_course.list_course";
          }
          else if (path[1] == "course-add") {
            titleTemp = "menu_manage_course.add_course";
          }
          else if (path[1] == "course-update") {
            titleTemp = "eva.manage_course.add.edit_course";
          }
          else if (path[1] == "course-copy") {
            titleTemp = "Copy Course";
          }
          else {
            titleTemp = "menu_manage_" + path[0] + ".manage_" + path[0];
          }
        }

        else if (path[0] == "studentinfo") {
          titleTemp = "menu_manage_student.manage_student_info";
          if (path[1] == "search-student") {
            titleTemp = "menu_manage_student.list_student";
          }
          else if (path[1] == "terminate-student") {
            titleTemp = "menu_manage_student.list_request_terminate";
          }
        }

        else if (path[0] == "certificate") {
          titleTemp = "menu_manage_" + path[0] + ".manage_" + path[0];
          if (path[1] == "certificate-list-background") {
            titleTemp = "menu_manage_certificate.list_background";
          }
          else if (path[1] == "certificate-add-background") {
            titleTemp = "menu_manage_certificate.add_background";
          }
          else if (path[1] == "certificate-list-design") {
            titleTemp = "menu_manage_certificate.list_certificate";
          }
          else if (path[1] == "certificate-add-design") {
            titleTemp = "menu_manage_certificate.add_certificate";
          }
          else if (path[1] == "certificate-edit-design") {
            titleTemp = "eva.manage_certificate.form_edit_certificate";
          }
        }

        else if (path[0] == "student") {
          titleTemp = "";
          if (path[1] == "change-password") {
            titleTemp = "eim.profile.change_password.change_password";
          }
          else if (path[1] == "my-course") {
            titleTemp = "menu_course.my_course";
          }
          else if (path[1] == "next-course") {
            titleTemp = "menu_course.next_course";
          }
          else if (path[1] == "history-course") {
            titleTemp = "dashboard.student.finished_course";
          }
          else if (path[1] == "all-course") {
            titleTemp = "menu_course.all_course";
          }
          else if (path[1] == "search-course") {
            titleTemp = "menu_course.search_course";
          }
          else if (path[1] == "student-profile") {
            titleTemp = "eim.profile.my_profile.my_profile";
          }
          else if (path[1] == "survey-course") {
            titleTemp = "Survey Course";
          }
        }

        else if (path[0] == "report") {
          if (path[1] == "organization-report") {
            titleTemp = "menu_report_epdb.organization_report";
          }
          else if (path[1] == "sites-report") {
            titleTemp = "menu_report_epdb.site_report";
          }
          else if (path[1] == "contact-report") {
            titleTemp = "menu_report_epdb.contact_report";
          }
          else if (path[1] == "organization-journal-entry-report") {
            titleTemp = "menu_report_epdb.org_journal_entry";
          }
          else if (path[1] == "site-journal-entry-report") {
            titleTemp = "menu_report_epdb.site_journal_entry";
          }
          else if (path[1] == "invoice-report") {
            titleTemp = "menu_report_epdb.invoice_report";
          }
          else if (path[1] == "qualifications-by-contact-report") {
            titleTemp = "menu_report_epdb.qualification_contact_report";
          }
          else if (path[1] == "site-attributes-report") {
            titleTemp = "menu_report_epdb.site_attribute_report";
          }
          else if (path[1] == "contact-attributes-report") {
            titleTemp = "menu_report_epdb.contact_attribute_report";
          }
          else if (path[1] == "ATC-AAP-accreditation-counts-report") {
            titleTemp = "menu_report_epdb.atc_aap_acc_count_report";
          }
          else if (path[1] == "history-report") {
            titleTemp = "menu_report_epdb.history_report";
          }
          else {
            titleTemp = "menu_report_epdb.report_epdb";
          }
        }

        // Report Eva
        else if (path[0] == "report-eva") {
          if (path[1] == "organization-report-eva") {
            titleTemp = "epdb.report_eva.organization_report.organization_report";
          }
          else if (path[1] == "performance-overview-eva") {
            titleTemp = "epdb.report_eva.performance_overview.performance_overview";
          }
          else if (path[1] == "instructor-performance-eva") {
            titleTemp = "epdb.report_eva.instructor_performance.instructor_performance";
          }
          else if (path[1] == "evaluation-responses") {
            titleTemp = "epdb.report_eva.post_evaluation_responses.post_evaluation_responses";
          }
          else if (path[1] == "student-search-eva") {
            titleTemp = "epdb.report_eva.student_search.student_search";
          }
          else if (path[1] == "download-site-eva") {
            titleTemp = "epdb.report_eva.download_site_evaluation.download_site_evaluation";
          }
          else if (path[1] == "channel-performance-eva") {
            titleTemp = "epdb.report_eva.channel_performance.channel_performance";
          }
          else if (path[1] == "submissions-by-language-eva") {
            titleTemp = "epdb.report_eva.submission_language.submission_language";
          }
          else if (path[1] == "audit") {
            titleTemp = "epdb.report_eva.audit.audit";
          }
          else {
            titleTemp = "menu_" + path[0] + "." + path[0];
          }
        }

        else if (path[0] == "role") {
          titleTemp = "menu_" + path[0] + "." + path[0];
          if (path[1] == "add-role") {
            titleTemp = "menu_role.add_role";
          }
          else if (path[1] == "edit-role") {
            titleTemp = "epdb.role.edit_role";
          }
          else if (path[1] == "list-access") {
            titleTemp = "menu_role.list_access";
          }
        }

        else if (path[0] == "profile") {
          titleTemp = "eim.profile.my_profile.my_profile"
        }
        else if (path[0] == "login") {
          titleTemp = "Login"
        }
        else if (path[0] == "login-student") {
          titleTemp = "Login Student"
        }
        else if (path[0] == "logout") {
          titleTemp = "general.navbar.logout"
        }
        else if (path[0] == "register") {
          titleTemp = "Register"
        }
        else if (path[0] == "forget-password") {
          titleTemp = "Forget Password"
        }
        else if (path[0] == "forget-password-student") {
          titleTemp = "Forget Password Student"
        }
        else if (path[0] == "forbidden") {
          titleTemp = "Forbidden"
        }
        else if (path[0] == "error") {
          titleTemp = "Error"
        }
        else if (path[0] == "set-password") {
          titleTemp = "Set Password"
        }
        else if (path[0] == "terminate-account") {
          titleTemp = "Terminate Account"
        }
        else if (path[0] == "change-password") {
          titleTemp = "eim.profile.change_password.change_password"
        }
        else if (path[0] == "change-email") {
          titleTemp = "eim.profile.change_email.change_email"
        }
        else if (path[0] == "warningMessage") {
          titleTemp = "Warning"
        }
        else if (path[0] == "dashboard-admin") {
          titleTemp = "menu_dashboard.dashboard_admin";
        }
        else if (path[0] == "dashboard-distributor") {
          titleTemp = "menu_dashboard.dashboard_distributor";
        }
        else if (path[0] == "dashboard-organization") {
          titleTemp = "menu_dashboard.dashboard_organization";
        }
        else if (path[0] == "dashboard-trainer") {
          titleTemp = "menu_dashboard.dashboard_trainer";
        }
        else if (path[0] == "dashboard-student") {
          titleTemp = "menu_dashboard.dashboard_student";
        }
        else if (path[0] == "help-me") {
          titleTemp = "Help";
        }
        else {
          titleTemp = ""
        }

        titleTemp = titleTemp.replace(/-/g, '_')

        if (titleTemp != "") {
          this.translate.get(titleTemp).subscribe(res => {
            this.titleService.setTitle('Autodesk | ' + res);
          });
        } else {
          this.titleService.setTitle('Autodesk');
        }

      });
  }

}
