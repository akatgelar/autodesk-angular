import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import "rxjs/add/operator/filter";
import {
  state,
  style,
  transition,
  animate,
  trigger,
  AUTO_STYLE
} from "@angular/animations";

import { MenuItemsNew } from "../../shared/menu-items-new/menu-items-new";

import { TranslateService } from "@ngx-translate/core";
import { SessionService } from "../../shared/service/session.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import swal from "sweetalert2";
import { Observable } from "rxjs/Observable";
import { Http, Headers, Response } from "@angular/http";
import { SurveyAnswerStudentComponent } from "../../student/survey-answer/survey-answer-student.component";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { Router, NavigationEnd } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import $ from 'jquery/dist/jquery';


export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}

@Component({
  selector: "app-layout",
  templateUrl: "./admin-layout.component.html",
  styleUrls: ["./admin-layout.component.css"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger("slideInOut", [
      state(
        "in",
        style({
          transform: "translate3d(0, 0, 0)"
        })
      ),
      state(
        "out",
        style({
          transform: "translate3d(100%, 0, 0)"
        })
      ),
      transition("in => out", animate("400ms ease-in-out")),
      transition("out => in", animate("400ms ease-in-out"))
    ]),
    trigger("slideOnOff", [
      state(
        "on",
        style({
          transform: "translate3d(0, 0, 0)"
        })
      ),
      state(
        "off",
        style({
          transform: "translate3d(100%, 0, 0)"
        })
      ),
      transition("on => off", animate("400ms ease-in-out")),
      transition("off => on", animate("400ms ease-in-out"))
    ]),
    trigger("mobileMenuTop", [
      state(
        "no-block, void",
        style({
          overflow: "hidden",
          height: "0px"
        })
      ),
      state(
        "yes-block",
        style({
          height: AUTO_STYLE
        })
      ),
      transition("no-block <=> yes-block", [animate("400ms ease-in-out")])
    ])
  ],
  providers: [SurveyAnswerStudentComponent, AppFormatDate]
})
export class AdminLayoutNewComponent implements OnInit {
  public useraccesdata: any;
  deviceType = "desktop";
  verticalNavType = "expanded";
  verticalEffect = "shrink";
  chatToggle = "out";
  chatInnerToggle = "off";
  innerHeight: string;
  isScrolled = false;
  isCollapsedMobile = "no-block";
  isCollapsedSideBar = "no-block";
  toggleOn = true;
  windowWidth: number;
  listlanguage: any;
  listlanguage1 = [];
  public ActiveLang: string = "";

  sidebarMenu = [];

  @ViewChild("searchFriends") search_friends: ElementRef;
  @ViewChild("toggleButton") toggle_button: ElementRef;
  @ViewChild("sideMenu") side_menu: ElementRef;
  // @ViewChild(SurveyAnswerStudentComponent) studentSurvey: SurveyAnswerStudentComponent;

  constructor(
    private _http: Http,
    public menuItems: MenuItemsNew,
    private translate: TranslateService,
    public session: SessionService,
    private http: HttpClient,
	private service: AppService,
    private studentSurvey: SurveyAnswerStudentComponent,
    private router: Router
  ) {
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + "px";
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    this.http
      .get("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}")
      .subscribe(result => {
        translate.langs = [];
        var resTemp: any;
        resTemp = result;
        if (resTemp.length != 0) {
          for (let i = 0; i < resTemp.length; i++) {
            if (this.listlanguage1.length == 0) {
              this.listlanguage1.push(resTemp[i]);
            } else {
              for (let k = 0; k < this.listlanguage1.length; k++) {
                var exist = false;
                if (resTemp[i].Key == this.listlanguage1[k].Key) {
                  exist = true;
                }
              }
              if (exist == false) {
                this.listlanguage1.push(resTemp[i]);
              }
            }
          }
        }
        translate.addLangs(this.listlanguage1.sort(compare));
      });

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    menuItems.viewRoles();

    let menu_tmp = this.menuItems.getAll();
    this.sidebarMenu = menu_tmp[0].main;
    // console.log(this.sidebarMenu);
    //Ini hanya untuk mengatasi menu dashboard kalau user bisa mengakses lebih dari 1 dashboard
    if (this.sidebarMenu.length != 0) {
      let tmp_count = 0;
      let j = 5; //di set 5 karena di file menu-items-new.ts setiap menu dashboard di pisah jadi single state yang bakal ngehasilin panjang array = 5
      for (let i = 0; i < j; i++) {
        //ini cuma buat ngehitung jumlah dashboard yang bisa diakses
        //kalau > 1 sikaaaaaaaaaaaaaaaaat jek
        //kalau == 1, antepin aja
        if (this.sidebarMenu[i].icon == "ti-dashboard" && this.sidebarMenu[i].show == true) {
          tmp_count += 1;
        }
      }
      if (tmp_count > 1) {
        let tmp_children = [];
        //Cek satu per satu dashboard nya boleh ditampilin atau enggak
        for (let i = 0; i < j; i++) {
          if (this.sidebarMenu[i].show == true) {
            delete this.sidebarMenu[i].icon //dihapus property icon nya untuk dijadiin children
            this.sidebarMenu[i].name = this.dashboarName(this.sidebarMenu[i].state); //ganti namanya jek, karena kalau dashboard semua namanya sulit untuk membedakan
            tmp_children.push(this.sidebarMenu[i]);
          }
        }
        //jangan lupa susun lagi menu dashboard yg udh di modif biar tjaaaaaakep
        let tmp_dashboard =
        {
          children: tmp_children,
          icon: "ti-dashboard",
          name: "menu_dashboard.dashboard",
          show: true,
          state: "dashboard", //parentnya ngambil dari routing dashboard yg dulu punya children, diaktifin cuma untuk kondisi ini aja. Kalau yg single state bakal tetep manggil route yg single state
          type: "sub"
        }
        //karena single state dashboard panjang array nya 5, dipotong 4 sisain 1 buat ngeganti posisi yg baru
        this.sidebarMenu.splice(0, 4);
        this.sidebarMenu[0] = tmp_dashboard;
        // console.log(this.sidebarMenu);
      }
    }
  }

  public data: any;
  ProfilePicture: string;
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });

    var data = "";
    this.service.httpClientGet("api/ContactAll/" + this.useraccesdata.Email,"").subscribe(
      result => {
        if (result == "Not found") {
          this.data = "";
        } else {
          this.data = result;
        }
      },
      error => {
        this.data = "";
      }
    );
  }

  dashboarName(name) {
    //Nanti tinggal ditambah di file json language nya sesuain sama nama dashboardnya
    let trans: string = "";
    switch (name) {
      case "dashboard-organization":
        trans = "menu_dashboard.dashboard_organization";
        break;
      case "dashboard-distributor":
        trans = "menu_dashboard.dashboard_distributor";
        break;
      case "dashboard-trainer":
        trans = "menu_dashboard.dashboard_trainer";
        break;
      case "dashboard-student":
        trans = "menu_dashboard.dashboard_student";
        break;

      default:
        trans = "menu_dashboard.dashboard_admin";
        break;
    }

    return trans;
  }

  logout() {
    var role = "";

    if (this.useraccesdata.UserLevelId == "STUDENT") {
      role = "student";
    } else {
      role = "contact";
    }

    this.router.navigate(["/logout"], { queryParams: { id: this.useraccesdata.UserId, role: role } });
  }

  //hanya dipanggil ketika logout dari portal student
  //language key selanjutnya bakal di set default ketika login lagi
  resetLanguage() {
    if (localStorage.getItem("languageKey") != null) {
      localStorage.removeItem("languageKey");
    }
  }

  clearFilterStorage() {
    localStorage.removeItem("filter");
    // localStorage.removeItem("languageKey");
  }

  setLanguage(value, key) {
    //untuk mendapatkan alamat url yang sedang aktif
    function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    this.translate.use(value);
    localStorage.setItem("language", value);
    localStorage.setItem("languageKey", key);
    this.ActiveLang = value;

    //untuk ngehandle ketika di halaman survey answer student supaya ketika student milih bahasa tidak perlu di reload halamannya
    var temp = JSON.parse(getParameterByName("args", ""));
    if (temp == null) {
      location.reload();
    }
  }

  HelpMe() {
    this.router.navigate(['/help-me']);
  }

  //Digunakan untuk mengirim language key ke component survey answer student
  getKeyLang(value) {
    // console.log(value);
    this.studentSurvey.chageSurveyLanguage(value);
  }

  onClickedOutside(e: Event) {
    if (
      this.windowWidth < 768 &&
      this.toggleOn &&
      this.verticalNavType !== "offcanvas"
    ) {
      this.toggleOn = true;
      this.verticalNavType = "offcanvas";
    }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + "px";
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (
      this.deviceType === "tablet" &&
      this.windowWidth >= 768 &&
      this.windowWidth <= 1024
    ) {
      reSizeFlag = false;
    } else if (this.deviceType === "mobile" && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    if (reSizeFlag) {
      this.setMenuAttributs(this.windowWidth);
    }
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = "tablet";
      this.verticalNavType = "collapsed";
      this.verticalEffect = "push";
    } else if (windowWidth < 768) {
      this.deviceType = "mobile";
      this.verticalNavType = "offcanvas";
      this.verticalEffect = "overlay";
    } else {
      this.deviceType = "desktop";
      this.verticalNavType = "expanded";
      this.verticalEffect = "shrink";
    }
  }

  searchFriendList(event) {
    const search = this.search_friends.nativeElement.value.toLowerCase();
    let search_input: string;
    let search_parent: any;
    const friendList = document.querySelectorAll(
      ".userlist-box .media-body .chat-header"
    );
    Array.prototype.forEach.call(friendList, function (elements, index) {
      search_input = elements.innerHTML.toLowerCase();
      search_parent = elements.parentNode.parentNode;
      if (search_input.indexOf(search) !== -1) {
        search_parent.classList.add("show");
        search_parent.classList.remove("hide");
      } else {
        search_parent.classList.add("hide");
        search_parent.classList.remove("show");
      }
    });
  }

  toggleChat() {
    this.chatToggle = this.chatToggle === "out" ? "in" : "out";
  }

  toggleChatInner() {
    this.chatInnerToggle = this.chatInnerToggle === "off" ? "on" : "off";
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn =
        this.verticalNavType === "offcanvas" ? true : this.toggleOn;
      this.verticalNavType =
        this.verticalNavType === "expanded" ? "offcanvas" : "expanded";
    } else {
      this.verticalNavType =
        this.verticalNavType === "expanded" ? "collapsed" : "expanded";

      $('li').removeClass('pcoded-trigger');
    }
  }

  toggleOpenedSidebar() {
    this.isCollapsedSideBar =
      this.isCollapsedSideBar === "yes-block" ? "no-block" : "yes-block";
  }

  onMobileMenu() {
    this.isCollapsedMobile =
      this.isCollapsedMobile === "yes-block" ? "no-block" : "yes-block";
  }

  reload(state) {
    if (state == "survey-template-list" || state == "survey-template-copy" || state == "survey-course") {
      //setTimeout(() => {
      //  location.reload();
      //}, 500)
    }
  }
}
