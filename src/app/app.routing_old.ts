// import { Routes } from '@angular/router';

// import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
// import { AdminLayoutEIMComponent } from './layouts/admin-eim/admin-layout.component';
// import { AdminLayoutEVAComponent } from './layouts/admin-eva/admin-layout.component';
// import { AdminLayoutEPDBComponent } from './layouts/admin-epdb/admin-layout.component';
// import { AdminLayoutAutodeskAdminComponent } from './layouts/admin-autodesk-admin/admin-layout.component';
// import { AdminLayoutAutodeskUserComponent } from './layouts/admin-autodesk-user/admin-layout.component';
// import { AdminLayoutDistributorComponent } from './layouts/admin-distributor/admin-layout.component';
// import { AdminLayoutPartnerAdminComponent } from './layouts/admin-partner-admin/admin-layout.component';
// import { AdminLayoutPartnerUserComponent } from './layouts/admin-partner-user/admin-layout.component';
// import { AdminLayoutStudentComponent } from './layouts/admin-student/admin-layout.component';

// export const AppRoutes: Routes = [
//   {
//     path: '',
//     redirectTo: 'authentication/login/epdb',
//     pathMatch: 'full'
//   },
//   {
//     path: 'eim',
//     component: AdminLayoutEIMComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       }, {
//         path: 'dashboard',
//         loadChildren: './eim/dashboard/dashboard-eim.module#DashboardEIMModule'
//       }, {
//         path: 'profile',
//         loadChildren: './eim/profile/profile-eim.module#ProfileEIMModule'
//       }, {
//         path: 'contact',
//         loadChildren: './eim/contact/contact-eim.module#ContactEIMModule'
//       }, {
//         path: 'aci',
//         loadChildren: './eim/aci/aci-eim.module#ACIEIMModule'
//       }, {
//         path: 'sms',
//         loadChildren: './eim/sms/sms-eim.module#SMSEIMModule'
//       }, {
//         path: 'audit',
//         loadChildren: './eim/audit/audit-eim.module#AuditEIMModule'
//       },
//     ]
//   },
//   {
//     path: 'epdb',
//     component: AdminLayoutEPDBComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './epdb/dashboard/dashboard-epdb.module#DashboardEPDBModule'
//       },
//       {
//         path: 'detail-organization/:id',
//         loadChildren: './epdb/detail-organization/detail-organization-epdb.module#DetailOrganizationEPDBModule'
//       },
//       {
//         path: 'detail-site/:id',
//         loadChildren: './epdb/detail-site/detail-site-epdb.module#DetailSiteEPDBModule'
//       },
//       {
//         path: 'detail-contact/:id',
//         loadChildren: './epdb/detail-contact/detail-contact-epdb.module#DetailContactEPDBModule'
//       },
//       {
//         path: 'organization',
//         children:
//           [
//             {
//               path: 'add-organization',
//               loadChildren: './epdb/add-organization/add-organization-epdb.module#AddOrganizationEPDBModule'
//             },
//             {
//               path: 'search-organization',
//               loadChildren: './epdb/search-organization/search-organization-epdb.module#SearchOrganizationEPDBModule'
//             }
//           ]
//       },
//       {
//         path: 'distributor',
//         children:
//           [
//             {
//               path: 'list-distributor',
//               loadChildren: './epdb/distributor/list-distributor/list-distributor-epdb.module#ListDistributorEPDBModule'
//             },
//             {
//               path: 'add-distributor',
//               loadChildren: './epdb/distributor/add-distributor/add-distributor-epdb.module#AddDistributorEPDBModule'
//             },
//             {
//               path: 'add-master-distributor',
//               loadChildren: './epdb/distributor/add-master-distributor/add-master-distributor-epdb.module#AddMasterDistributorEPDBModule'
//             }
//           ]
//       },
//       {
//         path: 'add-site/:id',
//         loadChildren: './epdb/add-site/add-site-epdb.module#AddSiteEPDBModule'
//       },
//       {
//         path: 'add-contact/:id',
//         loadChildren: './epdb/add-contact/add-contact-epdb.module#AddContactEPDBModule'
//       },
//       {
//         path: 'add-academic-project/:id',
//         loadChildren: './epdb/add-academic-project/add-academic-project-epdb.module#AddAcademicProjectEPDBModule'
//       },
//       {
//         path: 'add-invoice/:id',
//         loadChildren: './epdb/add-invoice/add-invoice-epdb.module#AddInvoiceEPDBModule'
//       },
//       {
//         path: 'update-invoice/:id',
//         loadChildren: './epdb/update-invoice/update-invoice-epdb.module#UpdateInvoiceEPDBModule'
//       },
//       {
//         path: 'add-partner-type/:id',
//         loadChildren: './epdb/add-partner-type/add-partner-type-epdb.module#AddPartnerTypeEPDBModule'
//       }, {
//         path: 'update-partner-type/:id',
//         loadChildren: './epdb/update-partner-type/update-partner-type-epdb.module#UpdatePartnerTypeEPDBModule'
//       },
//       {
//         path: 'edit-organization/:id',
//         loadChildren: './epdb/edit-organization/edit-organization-epdb.module#EditOrganizationEPDBModule'
//       },
//       {
//         path: 'edit-site/:id',
//         loadChildren: './epdb/edit-site/edit-site-epdb.module#EditSiteEPDBModule'
//       },
//       {
//         path: 'edit-contact/:id',
//         loadChildren: './epdb/edit-contact/edit-contact-epdb.module#EditContactEPDBModule'
//       },
//       {
//         path: 'add-orgjournalentries/:id',
//         loadChildren: './epdb/add-orgjournalentries/add-orgjournalentries-epdb.module#AddOrgJournalEntriesEPDBModule'
//       },
//       {
//         path: 'edit-orgjournalentries/:id',
//         loadChildren: './epdb/edit-orgjournalentries/edit-orgjournalentries-epdb.module#EditOrgJournalEntriesEPDBModule'
//       },
//       {
//         path: 'edit-orgattributes',
//         loadChildren: './epdb/edit-orgattributes/edit-orgattributes-epdb.module#EditOrgAttributesEPDBModule'
//       },
//       {
//         path: 'edit-orgattachments',
//         loadChildren: './epdb/edit-orgattachments/edit-orgattachments-epdb.module#EditOrgAttachmentsEPDBModule'
//       },
//       {
//         path: 'pdb-history',
//         loadChildren: './epdb/pdb-history/pdb-history-epdb.module#PDBHistoryEPDBModule'
//       },
//       {
//         path: 'learning-history',
//         loadChildren: './epdb/learning-history/learning-history-epdb.module#LearningHistoryEPDBModule'
//       },
//       {
//         path: 'certifications',
//         loadChildren: './epdb/certifications/certifications-epdb.module#CertificationsEPDBModule'
//       },
//       {
//         path: 'specializations',
//         loadChildren: './epdb/specializations/specializations-epdb.module#SpecializationsEPDBModule'
//       },
//       {
//         path: 'site-affiliations/:id/:action/:idaction',
//         loadChildren: './epdb/site-affiliations/site-affiliations-epdb.module#SiteAffiliationsEPDBModule'
//       },
//       {
//         path: 'search-siebel/:id',
//         loadChildren: './epdb/search-siebel/search-siebel-epdb.module#SearchSiebelEPDBModule'
//       },
//       {
//         path: 'site',
//         children:
//           [
//             {
//               path: 'search-site',
//               loadChildren: './epdb/search-site/search-site-epdb.module#SearchSiteEPDBModule'
//             },
//             {
//               path: 'add-site',
//               loadChildren: './epdb/add-site/add-site-epdb.module#AddSiteEPDBModule'
//             },
//           ]
//       },
//       {
//         path: 'contact',
//         children:
//           [
//             {
//               path: 'search-contact',
//               loadChildren: './epdb/search-contact/search-contact-epdb.module#SearchContactEPDBModule'
//             },
//             {
//               path: 'add-contact',
//               loadChildren: './epdb/add-contact/add-contact-epdb.module#AddContactEPDBModule'
//             },
//             {
//               path: 'search-general',
//               loadChildren: './epdb/search-general/search-general-epdb.module#SearchGeneralEPDBModule',
//             }
//           ]
//       },
//       {
//         path: 'view-user-information',
//         loadChildren: './epdb/view-user-information/view-user-information-epdb.module#ViewUserInformationEPDBModule'
//       },
//       {
//         path: 'change-user-information',
//         loadChildren: './epdb/change-user-information/change-user-information-epdb.module#ChangeUserInformationEPDBModule'
//       },
//       {
//         path: 'administration',
//         loadChildren: './epdb/administration/administration-epdb.module#AdministrationEPDBModule'
//       },
//       {
//         path: 'activities',
//         children: [
//           {
//             path: 'activities-list',
//             loadChildren: './epdb/activities/activities-list/activities-list-epdb.module#ActivitiesListEPDBModule',
//           },
//           {
//             path: 'activities-add',
//             loadChildren: './epdb/activities/activities-add/activities-add-epdb.module#ActivitiesAddEPDBModule',
//           },
//           {
//             path: 'activities-update/:id',
//             loadChildren: './epdb/activities/activities-update/activities-update-epdb.module#ActivitiesUpdateEPDBModule',
//             data: {
//               breadcrumb: 'Update Activities '
//             }
//           },
//           {
//             path: 'activities-detail/:id',
//             loadChildren: './epdb/activities/activities-detail/activities-detail-epdb.module#ActivitiesDetailEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'invoice-type',
//         children: [
//           {
//             path: 'invoice-type-list',
//             loadChildren: './epdb/invoice-type/invoice-type-list/invoice-type-list-epdb.module#InvoiceTypeListEPDBModule',
//           },
//           {
//             path: 'invoice-type-add',
//             loadChildren: './epdb/invoice-type/invoice-type-add/invoice-type-add-epdb.module#InvoiceTypeAddEPDBModule',
//           },
//           {
//             path: 'invoice-type-update/:id',
//             loadChildren: './epdb/invoice-type/invoice-type-update/invoice-type-update-epdb.module#InvoiceTypeUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'line-item-summaries',
//         children: [
//           {
//             path: 'line-item-summaries-list',
//             loadChildren: './epdb/line-item-summaries/line-item-summaries-list/line-item-summaries-list-epdb.module#LineItemSummariesListEPDBModule',
//           },
//           {
//             path: 'line-item-summaries-add',
//             loadChildren: './epdb/line-item-summaries/line-item-summaries-add/line-item-summaries-add-epdb.module#LineItemSummariesAddEPDBModule',
//           },
//           {
//             path: 'line-item-summaries-update/:id',
//             loadChildren: './epdb/line-item-summaries/line-item-summaries-update/line-item-summaries-update-epdb.module#LineItemSummariesUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'products',
//         children: [
//           {
//             path: 'list-category',
//             loadChildren: './epdb/products/products-category-list/products-category-list-epdb.module#ProductsCategoryListEPDBModule',
//           },
//           {
//             path: 'products-list',
//             loadChildren: './epdb/products/products-list/products-list-epdb.module#ProductsListEPDBModule',
//           },
//           {
//             path: 'products-add',
//             loadChildren: './epdb/products/products-add/products-add-epdb.module#ProductsAddEPDBModule',
//           },
//           {
//             path: 'products-update/:id',
//             loadChildren: './epdb/products/products-update/products-update-epdb.module#ProductsUpdateEPDBModule',
//           },
//           {
//             path: 'add-category',
//             loadChildren: './epdb/products/products-category-add/products-category-add-epdb.module#ProductsCategoryAddEPDBModule',
//           },
//           {
//             path: 'products-category-update/:id',
//             loadChildren: './epdb/products/products-category-update/products-category-update-epdb.module#ProductsCategoryUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'sku',
//         children: [
//           {
//             path: 'add-sku',
//             loadChildren: './epdb/sku/sku-add/sku-add-epdb.module#AddSKUEPDBModule'
//           },
//           {
//             path: 'list-sku',
//             loadChildren: './epdb/sku/sku-list/sku-list-epdb.module#ListSKUEPDBModule'
//           },
//           {
//             path: 'update-sku/:id',
//             loadChildren: './epdb/sku/sku-update/sku-update-epdb.module#UpdateSKUEPDBModule'
//           },
//         ]
//       },
//       {
//         path: 'currency',
//         loadChildren: './epdb/currency/currency-epdb.module#CurrencyEPDBModule'
//       },
//       {
//         path: 'glosarry',
//         children: [
//           {
//             path: 'glosarry-list',
//             loadChildren: './epdb/glosarry/glosarry-list/glosarry-list-epdb.module#GlosarryListEPDBModule'
//           },
//           {
//             path: 'glosarry-add',
//             loadChildren: './epdb/glosarry/glosarry-add/glosarry-add-epdb.module#GlosarryAddEPDBModule'
//           },
//           {
//             path: 'glosarry-update/:id',
//             loadChildren: './epdb/glosarry/glosarry-update/glosarry-update-epdb.module#GlosarryUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'country',
//         children: [
//           {
//             path: 'country-list',
//             loadChildren: './epdb/country/country-list/country-epdb.module#CountryEPDBModule'
//           },
//           {
//             path: 'geo-list',
//             loadChildren: './epdb/country/geo-list/geo-list-epdb.module#GeoListEPDBModule'
//           },
//           {
//             path: 'geo-add',
//             loadChildren: './epdb/country/geo-add/geo-add-epdb.module#GeoAddEPDBModule'
//           },
//           {
//             path: 'geo-update/:id',
//             loadChildren: './epdb/country/geo-update/geo-update-epdb.module#GeoUpdateEPDBModule'
//           },
//           {
//             path: 'territory-list',
//             loadChildren: './epdb/country/territory-list/territory-list-epdb.module#TerritoryListEPDBModule'
//           },
//           {
//             path: 'territory-add',
//             loadChildren: './epdb/country/territory-add/territory-add-epdb.module#TerritoryAddEPDBModule'
//           },
//           {
//             path: 'territory-update/:id',
//             loadChildren: './epdb/country/territory-update/territory-update-epdb.module#TerritoryUpdateEPDBModule'
//           },
//           {
//             path: 'region-list',
//             loadChildren: './epdb/country/region-list/region-list-epdb.module#RegionListEPDBModule',
//           },
//           {
//             path: 'region-add',
//             loadChildren: './epdb/country/region-add/region-add-epdb.module#RegionAddEPDBModule',
//           },
//           {
//             path: 'region-update/:id',
//             loadChildren: './epdb/country/region-update/region-update-epdb.module#RegionUpdateEPDBModule',
//           },
//           {
//             path: 'sub-region-list',
//             loadChildren: './epdb/country/sub-region-list/sub-region-list-epdb.module#SubRegionListEPDBModule',
//           },
//           {
//             path: 'sub-region-add',
//             loadChildren: './epdb/country/sub-region-add/sub-region-add-epdb.module#SubRegionAddEPDBModule',
//           },
//           {
//             path: 'sub-region-update/:id',
//             loadChildren: './epdb/country/sub-region-update/sub-region-update-epdb.module#SubRegionUpdateEPDBModule',
//           },
//           {
//             path: 'countries-list',
//             loadChildren: './epdb/country/countries-list/countries-list-epdb.module#CountriesListEPDBModule',
//           },
//           {
//             path: 'countries-add',
//             loadChildren: './epdb/country/countries-add/countries-add-epdb.module#CountriesAddEPDBModule',
//           },
//           {
//             path: 'countries-update/:id',
//             loadChildren: './epdb/country/countries-update/countries-update-epdb.module#CountriesUpdateEPDBModule',
//           },
//           // {
//           //   path: 'sub-countries',
//           //   loadChildren: './epdb/country/sub-countries-add/sub-countries-add-epdb.module#SubCountriesAddEPDBModule',
//           // },
//         ]
//       },
//       {
//         path: 'territory',
//         children: [
//           {
//             path: 'territory-list',
//             loadChildren: './epdb/territory/territory-list/territory-list-epdb.module#TerritoryListEPDBModule'
//           },
//           {
//             path: 'territory-add',
//             loadChildren: './epdb/territory/territory-add/territory-add-epdb.module#TerritoryAddEPDBModule'
//           },
//           {
//             path: 'territory-update',
//             loadChildren: './epdb/territory/territory-update/territory-update-epdb.module#TerritoryUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'market-type',
//         children: [
//           {
//             path: 'market-type-list',
//             loadChildren: './epdb/market-type/market-type-list/market-type-list-epdb.module#MarketTypeListEPDBModule'
//           },
//           {
//             path: 'market-type-add',
//             loadChildren: './epdb/market-type/market-type-add/market-type-add-epdb.module#MarketTypeAddEPDBModule'
//           },
//           {
//             path: 'market-type-update/:id',
//             loadChildren: './epdb/market-type/market-type-update/market-type-update-epdb.module#MarketTypeUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'role-access',
//         children: [
//           {
//             path: 'list-access',
//             loadChildren: './epdb/role-access/list-access/list-access-epdb.module#ListAccessEPDBModule'
//           },
//           {
//             path: 'add-role',
//             loadChildren: './epdb/role-access/add-role/add-role-epdb.module#AddRoleEPDBModule'
//           }
//         ]
//       },
//       {
//         path: 'site-services',
//         children: [
//           {
//             path: 'site-services-list',
//             loadChildren: './epdb/site-services/site-services-list/site-services-list-epdb.module#SiteServicesListEPDBModule'
//           },
//           {
//             path: 'site-services-add',
//             loadChildren: './epdb/site-services/site-services-add/site-services-add-epdb.module#SiteServicesAddEPDBModule'
//           },
//           {
//             path: 'site-services-update/:id',
//             loadChildren: './epdb/site-services/site-services-update/site-services-update-epdb.module#SiteServicesUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'variables',
//         children: [
//           {
//             path: 'variables-list',
//             loadChildren: './epdb/variables/variables-list/variables-list-epdb.module#VariablesListEPDBModule'
//           },
//           {
//             path: 'variables-add',
//             loadChildren: './epdb/variables/variables-add/variables-add-epdb.module#VariablesAddEPDBModule'
//           },
//           {
//             path: 'variables-update/:id',
//             loadChildren: './epdb/variables/variables-update/variables-update-epdb.module#VariablesUpdateEPDBModule',
//           },
//         ]
//       },
//       {
//         path: 'partner-type',
//         children:
//           [
//             {
//               path: 'list-sub-partner-type',
//               loadChildren: './epdb/partner-type/list-sub-partner-type/list-sub-partner-type.module#ListSubPartnertTypeEPDBModule'
//             },
//             {
//               path: 'add-sub-partner-type',
//               loadChildren: './epdb/partner-type/add-sub-partner-type/add-sub-partner-type.module#AddSubPartnertTypeEPDBModule'
//             },
//             {
//               path: 'update-sub-partner-type/:id',
//               loadChildren: './epdb/partner-type/update-sub-partner-type/update-sub-partner-type.module#UpdateSubPartnertTypeEPDBModule'
//             },
//             {
//               path: 'list-partner-type',
//               loadChildren: './epdb/partner-type/list-partner-type/list-partner-type.module#ListPartnertTypeEPDBModule'
//             },
//             {
//               path: 'add-partner-type',
//               loadChildren: './epdb/partner-type/add-partner-type/add-partner-type.module#AddPartnertTypeEPDBModule'
//             },
//             {
//               path: 'update-partner-type/:id',
//               loadChildren: './epdb/partner-type/update-partner-type/update-partner-type.module#UpdatePartnertTypeEPDBModule'
//             },
//           ]
//       },
//       {
//         path: 'aci',
//         loadChildren: './epdb/aci/aci-eim.module#ACIEIMModule'
//       },
//       {
//         path: 'setup-language',
//         loadChildren: './epdb/setup-language/setup-language-epdb.module#SetupLanguageEPDBModule'
//       },
//       {
//         path: 'report-database-info/history-report',
//         loadChildren: './epdb/history-report/history-report-epdb.module#HistoryReportEPDBModule'
//       },
//       {
//         path: 'report-database-info/login-history-report',
//         loadChildren: './epdb/login-history-report/login-history-report-epdb.module#LoginHistoryReportEPDBModule'
//       },
//       {
//         path: 'report-database-info/login-history-graphs-report',
//         loadChildren: './epdb/login-history-graphs-report/login-history-graphs-report-epdb.module#LoginHistoryGraphsReportEPDBModule'
//       },
//       {
//         path: 'report-database-info/regional-mappings-report',
//         loadChildren: './epdb/regional-mappings-report/regional-mappings-report-epdb.module#RegionalMappingsReportEPDBModule'
//       },
//       {
//         path: 'report-database-info/regional-mappings-report',
//         loadChildren: './epdb/regional-mappings-report/regional-mappings-report-epdb.module#RegionalMappingsReportEPDBModule'
//       },
//       {
//         path: 'ATC-AAP-static-report/ATC-AAP-accreditation-counts-report',
//         loadChildren: './epdb/ATC-AAP-accreditation-counts-report/ATC-AAP-accreditation-counts-report-epdb.module#ATCAAPAccreditationCountsReportEPDBModule'
//       },
//       {
//         path: 'ATC-AAP-static-report/ATC-locator-2012-report-ALPHA',
//         loadChildren: './epdb/ATC-locator-2012-report-ALPHA/ATC-locator-2012-report-ALPHA-epdb.module#ATCLocator2012ReportALPHAEPDBModule'
//       },
//       {
//         path: 'academic-partner-report/academic-programs-report',
//         loadChildren: './epdb/academic-programs-report/academic-programs-report-epdb.module#AcademicProgramsReportEPDBModule'
//       },
//       {
//         path: 'academic-partner-report/academic-targets-report',
//         loadChildren: './epdb/academic-targets-report/academic-targets-report-epdb.module#AcademicTargetsReportEPDBModule'
//       },
//       {
//         path: 'academic-partner-report/academic-projects-report',
//         loadChildren: './epdb/academic-projects-report/academic-projects-report-epdb.module#AcademicProjectsReportEPDBModule'
//       },
//       {
//         path: 'report-general-info/organization-report',
//         loadChildren: './epdb/report-general-info/organization-report/organization-report-epdb.module#OrganizationReportEPDBModule',
//       },
//       {
//         path: 'report-general-info/sites-report',
//         loadChildren: './epdb/report-general-info/sites-report/sites-report-epdb.module#SitesReportEPDBModule',
//       },
//       {
//         path: 'report-general-info/contact-report',
//         loadChildren: './epdb/report-general-info/contact-report/contact-report-epdb.module#ContactReportEPDBModule',
//       },
//       {
//         path: 'report-general-info/email-list-generator',
//         loadChildren: './epdb/report-general-info/email-list-generator/email-list-generator-epdb.module#EmailListGeneratorEPDBModule',
//       },
//       {
//         path: 'report-general-info/show-security-roles',
//         loadChildren: './epdb/report-general-info/show-security-roles/show-security-roles-epdb.module#ShowSecurityRolesEPDBModule',
//       },
//       {
//         path: 'report-general-info/beta-report-builder',
//         loadChildren: './epdb/report-general-info/beta-report-builder/beta-report-builder-epdb.module#BetaReportBuilderEPDBModule',
//       },
//       {
//         path: 'report-sales-partner/var-organization-attributes-report',
//         loadChildren: './epdb/report-sales-partner/var-organization-attributes-report/var-organization-attributes-report-epdb.module#VAROrganizationAttributeReportEPDBModule',
//       },
//       {
//         path: 'report-sales-partner/var-invoices-report',
//         loadChildren: './epdb/report-sales-partner/var-invoices-report/var-invoices-report-epdb.module#VARInvoiceReportEPDBModule',
//       },
//       {
//         path: 'report-sales-partner/var-site-attributes-report',
//         loadChildren: './epdb/report-sales-partner/var-site-attributes-report/var-site-attributes-report-epdb.module#VARSiteAttributeReportEPDBModule',
//       },
//       {
//         path: 'report-sales-partner/var-contact-attributes-report',
//         loadChildren: './epdb/report-sales-partner/var-contact-attributes-report/var-contact-attributes-report-epdb.module#VARContactAttributeReportEPDBModule',
//       },
//       {
//         path: 'report-sales-partner/var-contact-journal-entries-report',
//         loadChildren: './epdb/report-sales-partner/var-contact-journal-entries-report/var-contact-journal-entries-report-epdb.module#VARContactJournalEntriesReportEPDBModule',
//       },
//       {
//         path: 'training-partners/organization-journal-entry-report',
//         loadChildren: './epdb/training-partners/organization-journal-entry-report/organization-journal-entry-report-epdb.module#OrganizationJournalEntryReportEPDBModule',
//       },
//       {
//         path: 'training-partners/organization-attributes-report',
//         loadChildren: './epdb/training-partners/organization-attributes-report/organization-attributes-report-epdb.module#OrganizationAttributeReportEPDBModule',
//       },
//       {
//         path: 'training-partners/atc-invoices-report',
//         loadChildren: './epdb/training-partners/atc-invoices-report/atc-invoices-report-epdb.module#ATCInvoiceReportEPDBModule',
//       },
//       {
//         path: 'training-partners/site-accreditations-report',
//         loadChildren: './epdb/training-partners/site-accreditations-report/site-accreditations-report-epdb.module#SiteAccreditationReportEPDBModule',
//       },
//       {
//         path: 'training-partners/qualifications-by-site-report',
//         loadChildren: './epdb/training-partners/qualifications-by-site-report/qualifications-by-site-report-epdb.module#QualificationBySiteReportEPDBModule',
//       },
//       {
//         path: 'training-partners/site-journal-entry-report',
//         loadChildren: './epdb/training-partners/site-journal-entry-report/site-journal-entry-report-epdb.module#SiteJournalEntryReportEPDBModule',
//       },
//       {
//         path: 'training-partners/site-attributes-report',
//         loadChildren: './epdb/training-partners/site-attributes-report/site-attributes-report-epdb.module#SiteAttributeReportEPDBModule',
//       },
//       {
//         path: 'training-partners/serial-numbers-report',
//         loadChildren: './epdb/training-partners/serial-numbers-report/serial-numbers-report-epdb.module#SerialNumberReportEPDBModule',
//       },
//       {
//         path: 'training-partners/qualifications-by-contact-report',
//         loadChildren: './epdb/training-partners/qualifications-by-contact-report/qualifications-by-contact-report-epdb.module#QualificationByContactReportEPDBModule',
//       },
//       {
//         path: 'training-partners/contact-journal-entry-report',
//         loadChildren: './epdb/training-partners/contact-journal-entry-report/contact-journal-entry-report-epdb.module#ContactJournalEntryReportEPDBModule',
//       },
//       {
//         path: 'training-partners/contact-attributes-report',
//         loadChildren: './epdb/training-partners/contact-attributes-report/contact-attributes-report-epdb.module#ContactAttributeReportEPDBModule',
//       },
//       {
//         path: 'troubleshooting',
//         loadChildren: './epdb/troubleshooting/troubleshooting-epdb.module#TroubleshootingEPDBModule',
//       },
//       {
//         path: 'glossary',
//         loadChildren: './epdb/glossary/glossary-epdb.module#GlossaryEPDBModule',
//       },
//       {
//         path: 'frequently-asked-questions',
//         loadChildren: './epdb/frequently-asked-questions/frequently-asked-questions-epdb.module#FAQEPDBModule',
//       },
//       {
//         path: 'merge-contacts',
//         loadChildren: './epdb/merge-contacts/merge-contacts-epdb.module#MergeContactEPDBModule',
//       },
//       {
//         path: 'search-tips',
//         loadChildren: './epdb/search-tips/search-tips-epdb.module#SearchTipsEPDBModule',
//       },
//       {
//         path: 'bookmarks-favorites',
//         loadChildren: './epdb/bookmarks-favorites/bookmarks-favorites-epdb.module#BookmarksFavoriteEPDBModule',
//       },
//       {
//         path: 'encoding-information',
//         loadChildren: './epdb/encoding-information/encoding-information-epdb.module#EncodingInformationEPDBModule',
//       },
//       {
//         path: 'future-plans',
//         loadChildren: './epdb/future-plans/future-plans-epdb.module#FuturePlanEPDBModule',
//       },
//       {
//         path: 'test-plan',
//         loadChildren: './epdb/test-plan/test-plan-epdb.module#TestPlanEPDBModule',
//       },
//       {
//         path: 'search-general',
//         loadChildren: './epdb/search-general/search-general-epdb.module#SearchGeneralEPDBModule',
//       },
//       {
//         path: 'ATC-AAP-static-report/show-attachments-report',
//         loadChildren: './epdb/show-attachments-report/show-attachments-report-epdb.module#ShowAttachmentsReportEPDBModule',
//       },
//       {
//         path: 'restore-duplicate-objects',
//         loadChildren: './epdb/restore-duplicate-objects/restore-duplicate-objects-epdb.module#RestoreDuplicateObjectsEPDBModule',
//       },
//       {
//         path: 'restore-deleted-objects',
//         loadChildren: './epdb/restore-deleted-objects/restore-deleted-objects-epdb.module#RestoreDeletedObjectsEPDBModule',
//       },
//       {
//         path: 'find-potential-duplicate-users',
//         loadChildren: './epdb/find-potential-duplicate-users/find-potential-duplicate-users-epdb.module#FindPotentialDuplicateUsersEPDBModule',
//       },
//       {
//         path: 'database-information',
//         loadChildren: './epdb/database-information/database-information-epdb.module#DatabaseInformationEPDBModule',
//       },
//       {
//         path: 'technical-documentation',
//         loadChildren: './epdb/technical-documentation/technical-documentation-epdb.module#TechnicalDocumentationEPDBModule',
//       },
//       {
//         path: 'change-site-id',
//         loadChildren: './epdb/change-site-id/change-site-id-epdb.module#ChangeSiteIdEPDBModule',
//       },
//       {
//         path: 'ATC-AAP-static-report/americas-ATC-locator-report',
//         loadChildren: './epdb/americas-ATC-locator-report/americas-ATC-locator-report-epdb.module#AmericasATCLocatorReportEPDBModule',
//       },
//       {
//         path: 'report-database-info/database-integrity-report',
//         loadChildren: './epdb/database-integrity-report/database-integrity-report-epdb.module#DatabaseIntegrityReportEPDBModule',
//       },
//       {
//         path: 'report',
//         loadChildren: './epdb/report/report-epdb.module#ReportEPDBModule'
//       }, 
//       {
//         path: 'add-role',
//         loadChildren: './epdb/role-access/add-role/add-role-epdb.module#AddRoleEPDBModule'
//       }, 
//       {
//         path: 'list-access',
//         loadChildren: './epdb/role-access/list-access/list-access-epdb.module#ListAccessEPDBModule'
//       }
//     ]
//   },
//   {
//     path: 'eva',
//     component: AdminLayoutEVAComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './eva/dashboard/dashboard-eva.module#DashboardEVAModule'
//       },
//       {
//         path: 'course-list',
//         loadChildren: './eva/course-list/course-list-eva.module#CourseListEVAModule',
//       },
//       {
//         path: 'course-add',
//         loadChildren: './eva/course-add/course-add-eva.module#CourseAddEVAModule',
//       },
//       {
//         path: 'course-update/:id',
//         loadChildren: './eva/course-update/course-update-eva.module#CourseUpdateEVAModule',
//       },
//       {
//         path: 'confirm-final-student/:id',
//         loadChildren: './eva/confirm-final-student/confirm-final-student-eva.module#ConfirmFinalStudentEVAModule',
//       },
//       {
//         path: 'student-list',
//         loadChildren: './eva/student-list/student-list-eva.module#StudentListEVAModule'
//       },
//       {
//         path: 'organization',
//         loadChildren: './eva/organization/organization-eva.module#OrganizationEVAModule'
//       },
//       {
//         path: 'performance-overview',
//         loadChildren: './eva/performance-overview/performance-overview-eva.module#PerformanceOverviewEVAModule'
//       },
//       {
//         path: 'instructor-performance',
//         loadChildren: './eva/instructor-performance/instructor-performance-eva.module#InstructorPerformanceEVAModule'
//       },
//       {
//         path: 'evaluation-responses',
//         loadChildren: './eva/evaluation-responses/evaluation-responses-eva.module#EvaluationResponsesEVAModule'
//       },
//       {
//         path: 'student-search',
//         loadChildren: './eva/student-search/student-search-eva.module#StudentSearchEVAModule'
//       },
//       {
//         path: 'products-trained',
//         loadChildren: './eva/products-trained/products-trained-eva.module#ProductsTrainedEVAModule'
//       },
//       {
//         path: 'training-materials-used',
//         loadChildren: './eva/training-materials-used/training-materials-used-eva.module#TrainingMaterialsUsedEVAModule'
//       },
//       {
//         path: 'score-calculation-method',
//         loadChildren: './eva/score-calculation-method/score-calculation-method-eva.module#ScoreCalculationMethodEVAModule'
//       },
//       {
//         path: 'download-site-evaluations',
//         loadChildren: './eva/download-site-evaluations/download-site-evaluations-eva.module#DownloadSiteEvaluationsEVAModule'
//       },
//       {
//         path: 'help',
//         loadChildren: './eva/help/help-eva.module#HelpEVAModule'
//       },
//       {
//         path: 'channel-performance',
//         loadChildren: './eva/channel-performance/channel-performance-eva.module#ChannelPerformanceEVAModule'
//       },
//       {
//         path: 'products-peformance',
//         loadChildren: './eva/products-peformance/products-peformance-eva.module#ProductsPeformanceEVAModule'
//       },
//       {
//         path: 'submissions-by-language',
//         loadChildren: './eva/submissions-by-language/submissions-by-language-eva.module#SubmissionsByLanguageEVAModule'
//       },
//       {
//         path: 'award-nominees',
//         loadChildren: './eva/award-nominees/award-nominees-eva.module#AwardNomineesEVAModule'
//       },
//       {
//         path: 'audit',
//         loadChildren: './eva/audit/audit-eva.module#AuditEVAModule'
//       },
//       {
//         path: 'management-summary',
//         loadChildren: './eva/management-summary/management-summary-eva.module#ManagementSummaryEVAModule'
//       },
//       {
//         path: 'data-integrity',
//         loadChildren: './eva/data-integrity/data-integrity-eva.module#DataIntegrityEVAModule'
//       },
//       {
//         path: 'certification-awareness-emails',
//         loadChildren: './eva/certification-awareness-emails/certification-awareness-emails-eva.module#CertificationAwarenessEmailsEVAModule'
//       },
//       {
//         path: 'compliance-reports',
//         loadChildren: './eva/compliance-reports/compliance-reports-eva.module#ComplianceReportsEVAModule'
//       },
//       {
//         path: 'products-trained-statistics',
//         loadChildren: './eva/products-trained-statistics/products-trained-statistics-eva.module#ProductsTrainedStatisticsEVAModule'
//       },
//       {
//         path: 'stats-summary-reports',
//         loadChildren: './eva/stats-summary-reports/stats-summary-reports-eva.module#StatsSummaryReportsEVAModule'
//       },
//       {
//         path: 'post-evaluation-overview',
//         loadChildren: './eva/post-evaluation-overview/post-evaluation-overview-eva.module#PostEvaluationOverviewEVAModule'
//       },
//       {
//         path: 'post-evaluation-responses',
//         loadChildren: './eva/post-evaluation-responses/post-evaluation-responses-eva.module#PostEvaluationResponseEVAModule'
//       },
//       {
//         path: 'comparison-report',
//         loadChildren: './eva/comparison-report/comparison-report-eva.module#ComparisonReportEVAModule'
//       },
//       {
//         path: 'management-summary-psr',
//         loadChildren: './eva/management-summary-psr/management-summary-psr-eva.module#ManagementSummaryPSREVAModule'
//       },
//       {
//         path: 'return-on-investment',
//         loadChildren: './eva/return-on-investment/return-on-investment-eva.module#ReturnOnInvestmentEVAModule'
//       },
//       {
//         path: 'skills-acquired',
//         loadChildren: './eva/skills-acquired/skills-acquired-eva.module#SkillAcquiredEVAModule'
//       },
//       {
//         path: 'search-courses',
//         loadChildren: './eva/search-courses/search-courses-eva.module#SearchCourseEVAModule'
//       },
//       {
//         path: 'add-course',
//         loadChildren: './eva/add-course/add-course-eva.module#AddCourseEVAModule'
//       },
//       {
//         path: 'olap',
//         loadChildren: './eva/olap/olap-eva.module#OlapEVAModule'
//       },
//       {
//         path: 'olap-2009',
//         loadChildren: './eva/olap-2009/olap-2009-eva.module#Olap2009EVAModule'
//       },

//       //New Routing 30 January
//       {
//         path: 'list-of-the-form',
//         loadChildren: './eva/list-of-the-form/list-of-the-form-eva.module#ListOfTheFormEVAModule'
//       },
//       {
//         path: 'add-edit-form',
//         loadChildren: './eva/survey-template-add/add-edit-form-eva.module#AddEditFormEVAModule'
//       },
//       {
//         path: 'add-edit-form/:id',
//         loadChildren: './eva/survey-template-add/add-edit-form-eva.module#AddEditFormEVAModule'
//       },
//       {
//         path: 'survey-template-list',
//         loadChildren: './eva/survey-template-list/survey-template-list-eva.module#SurveyTemplateListEVAModule'
//       },
//       {
//         path: 'survey-template-detail/:id',
//         loadChildren: './eva/survey-template-detail/survey-template-detail-eva.module#SurveyTemplateDetailEVAModule'
//       },
//       {
//         path: 'add-edit-templatedata',
//         loadChildren: './eva/survey-templatedata-add/add-edit-templatedata-eva.module#AddEditTemplateDataEVAModule'
//       },
//       {
//         path: 'update-templatedata/:id',
//         loadChildren: './eva/survey-templatedata-update/update-templatedata-eva.module#UpdateTemplateDataEVAModule'
//       },
//       {
//         path: 'survey-answer-list',
//         loadChildren: './eva/survey-answer-list/survey-answer-list-eva.module#SurveyAnswerListEVAModule'
//       },
//       {
//         path: 'survey-answer-add',
//         loadChildren: './eva/survey-answer-add/survey-answer-add-eva.module#SurveyAnswerAddEVAModule'
//       },
//       {
//         path: 'survey-answer-add/:id',
//         loadChildren: './eva/survey-answer-add/survey-answer-add-eva.module#SurveyAnswerAddEVAModule'
//       },
//       //end of abah tambah routing
//       {
//         path: 'search-student',
//         loadChildren: './eva/search-student/search-student-eva.module#SearchStudentEVAModule'
//       },
//       {
//         path: 'update-student-information/:id',
//         loadChildren: './eva/update-student-information/update-student-information-eva.module#UpdateStudentInformationEVAModule'
//       },
//       {
//         path: 'certificate-add-background',
//         loadChildren: './eva/certificate-add-background/certificate-add-background-eva.module#CertificateAddBackgroundEVAModule'
//       },
//       {
//         path: 'certificate-list-background',
//         loadChildren: './eva/certificate-list-background/certificate-list-background-eva.module#CertificateListBackgroundEVAModule'
//       },
//       {
//         path: 'certificate-list-design',
//         loadChildren: './eva/certificate-list-design/certificate-list-design-eva.module#CertificateListDesignEVAModule'
//       },
//       {
//         path: 'certificate-add-design',
//         loadChildren: './eva/certificate-add-design/certificate-add-design-eva.module#CertificateAddDesignEVAModule'
//       },
//       {
//         path: 'certificate-edit-design/:id',
//         loadChildren: './eva/certificate-edit-design/certificate-edit-design-eva.module#CertificateEditDesignEVAModule'
//       }
//     ]
//   },
//   {
//     path: 'autodesk-admin',
//     component: AdminLayoutAutodeskAdminComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './autodesk-admin/dashboard/dashboard-autodesk-admin.module#DashboardAutodeskAdminModule'
//       },
//     ]
//   },
//   {
//     path: 'autodesk-user',
//     component: AdminLayoutAutodeskUserComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './autodesk-user/dashboard/dashboard-autodesk-user.module#DashboardAutodeskUserModule'
//       },
//     ]
//   },

//   {
//     path: 'distributor',
//     component: AdminLayoutDistributorComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './distributor/dashboard/dashboard-distributor.module#DashboardDistributorModule'
//       },
//     ]
//   },

//   {
//     path: 'partner-admin',
//     component: AdminLayoutPartnerAdminComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './partner-admin/dashboard/dashboard-partner-admin.module#DashboardPartnerAdminModule'
//       },
//     ]
//   },

//   {
//     path: 'template-preview',
//     component: AuthLayoutComponent,
//     children: [
//       {
//         path: 'survey-preview-eva/:id',
//         loadChildren: './template-preview/survey-preview/survey-preview-eva.module#SurveyPreviewEVAModule',
//       },
//       {
//         path: 'certificate-preview-eva/:id',
//         loadChildren: './template-preview/certificate-preview/certificate-preview-eva.module#CertificatePreviewEVAModule',
//       },
//     ]
//   },

//   {
//     path: 'partner-user',
//     component: AdminLayoutPartnerUserComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './partner-user/dashboard/dashboard-partner-user.module#DashboardPartnerUserModule'
//       },
//     ]
//   },

//   {
//     path: 'student',
//     component: AdminLayoutStudentComponent,
//     children: [
//       {
//         path: '',
//         redirectTo: 'dashboard',
//         pathMatch: 'full'
//       },
//       {
//         path: 'dashboard',
//         loadChildren: './student/dashboard/dashboard-student.module#DashboardStudentModule'
//       },
//       {
//         path: 'my-course',
//         loadChildren: './student/my-course/my-course-student.module#MyCourseStudentModule',
//       },
//       {
//         path: 'next-course',
//         loadChildren: './student/next-course/next-course-student.module#NextCourseStudentModule',
//       },
//       {
//         path: 'history-course',
//         loadChildren: './student/history-course/history-course-student.module#HistoryCourseStudentModule',
//       },
//       {
//         path: 'detail-course',
//         loadChildren: './student/detail-course/detail-course-student.module#DetailCourseStudentModule',
//       },
//       {
//         path: 'answer-evaluation-form',
//         loadChildren: './student/answer-evaluation-form/answer-evaluation-form-student.module#AnswerEvalutionFormStudentModule',
//       },
//       {
//         path: 'download-certificate',
//         loadChildren: './student/download-certificate/download-certificate-student.module#DownloadCertificateStudentModule',
//       },
//       {
//         path: 'survey-course/:id/:studentid/:courseid',
//         loadChildren: './student/survey-answer/survey-answer-student.module#SurveyAnswerStudentModule',
//       }, 
//     ]
//   },

//   {
//     path: '',
//     component: AuthLayoutComponent,
//     children: [{
//       path: 'authentication',
//       loadChildren: './authentication/authentication.module#AuthenticationModule'
//     },
//     {
//       path: 'student/register',
//       loadChildren: './authentication/login/studentregister/studentregister.module#StudentRegisterModule',
//     }, 
//     {
//       path: 'student/login',
//       loadChildren: './authentication/login/student/student.module#StudentModule',
//     },
//     {
//       path: 'error',
//       loadChildren: './error/error.module#ErrorModule'
//     }, {
//       path: 'maintenance/offline-ui',
//       loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
//     },]
//   }, 
//   {
//     path: '**',
//     redirectTo: 'error/404'
//   }
// ];
