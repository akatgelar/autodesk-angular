import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
    userlevel: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsStudent {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
    userlevel: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsStudent[];
}

const MENUITEMSStudent = [
  {
    label: '',
    main: [
      {
        state: 'dashboard',
        main_state: 'student',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard',
        userlevel:'STUDENT'
      },
      {
        state: 'student',
        name: 'Course',
        type: 'sub',
        icon: 'ti-book',
        userlevel:'STUDENT',
        children: [
          {
            state: 'my-course',
            name: 'My Course',
            userlevel:'STUDENT'
          },
          {
            state: 'next-course',
            name: 'Next Course',
            userlevel:'STUDENT'
          },
          {
            state: 'history-course',
            name: 'History Course',
            userlevel:'STUDENT'
          }
        ]
      },
      // {
      //   state: 'answer-evaluation-form',
      //   main_state: 'student',
      //   name: 'Answer The Evaluation Form',
      //   type: 'link',
      //   icon: 'ti-pencil',
      //   userlevel:'STUDENT'
      // },
      // {
      //   state: 'download-certificate',
      //   main_state: 'student',
      //   name: 'Download Certificate',
      //   type: 'link',
      //   icon: 'ti-download',
      //   userlevel:'STUDENT'
      // }
    ],
  },

];

@Injectable()
export class MenuItemsStudent {
  getAll(): Menu[] {
    return MENUITEMSStudent;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
