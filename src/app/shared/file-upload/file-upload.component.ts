import { Component, OnInit, Input } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  @Input() cap;
  public files: File[] = [];
  private validTypes = ['image/png','image/jpeg','application/pdf','image/tiff','image/tif','image/x-png']
  constructor() { }

  ngOnInit() { }

  fileChange(input){
    let cap = this.cap?this.cap:5
    let files = []
    let file_too_large = false
    let file_wrong_type = false
    for(let i =0; i< input.target.files.length; i++){
      if(input.target.files[i].size >2097152)
        file_too_large = true
      if(this.validTypes.indexOf(input.target.files[i].type) == -1)
        file_wrong_type = true
      files.push(input.target.files[i])
    } 
    if(this.files.length + files.length >cap)
      swal('Maximum file reached','Maximum 5 files','error')
    else if(file_too_large)
      swal('Maximum file size reached','Maximum 2Mb for each file','error')
    else if(file_wrong_type)
      swal('Invalid file type','Only PDF,JPEG,TIFF,PNG are allowed','error')
    else
      this.files = this.files.concat(files)
    input.target.value = ""
  }

  onDrop(event) {
    event.preventDefault();
    event.stopPropagation();
    let cap = this.cap?this.cap:5
    let files = []
    let file_too_large = false
    let file_wrong_type = false
    for(let i =0; i< event.dataTransfer.files.length; i++){
      if(event.dataTransfer.files[i].size >2097152)
        file_too_large = true
      if(this.validTypes.indexOf(event.dataTransfer.files[i].type) == -1)
        file_wrong_type = true
      files.push(event.dataTransfer.files[i])
    } 
    if(this.files.length + files.length >cap)
      swal('Maximum file reached','Maximum 5 files','error')
    else if(file_too_large)
      swal('Maximum file size reached','Maximum 2Mb for each file','error')
    else if(file_wrong_type)
      swal('Invalid file type','Only PDF,JPEG,TIFF,PNG are allowed','error')
    else
      this.files = this.files.concat(files)
  }

  onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  removeImage(index){
    this.files.splice(index,1)
  }

  valid(){
    if(this.files.length>0)
      return true
    else
      return false
  }

}
