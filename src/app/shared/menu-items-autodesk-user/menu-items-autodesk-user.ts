import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsAutodeskUser {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsAutodeskUser[];
}

const MENUITEMSAutodeskUser = [
  {
    label: '',
    main: [ 
      {
        state: 'dashboard',
        main_state: 'autodesk-user',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard'
      },  
    ],
  },
 
];

@Injectable()
export class MenuItemsAutodeskUser {
  getAll(): Menu[] {
    return MENUITEMSAutodeskUser;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
