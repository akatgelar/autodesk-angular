import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsPartnerAdmin {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsPartnerAdmin[];
}

const MENUITEMSPartnerAdmin = [
  {
    label: '',
    main: [ 
      {
        state: 'dashboard',
        main_state: 'partner-admin',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard'
      },  
    ],
  },
 
];

@Injectable()
export class MenuItemsPartnerAdmin {
  getAll(): Menu[] {
    return MENUITEMSPartnerAdmin;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
