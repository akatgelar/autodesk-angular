import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsDistributor {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsDistributor[];
}

const MENUITEMSDistributor = [
  {
    label: '',
    main: [ 
      {
        state: 'dashboard',
        main_state: 'distributor',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard'
      },  
    ],
  },
 
];

@Injectable()
export class MenuItemsDistributor {
  getAll(): Menu[] {
    return MENUITEMSDistributor;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
