import { Injectable } from '@angular/core';
import { SessionService } from '../service/session.service';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

let MENUITEMSNEW2 = [];

const MENUITEMSNEW = [
  {
    label: '',
    main: [
      {
        state: 'dashboard-admin',
        name: 'menu_dashboard.dashboard',
        type: 'link',
        icon: 'ti-dashboard', 
        show: false,
      }, 
      {
        state: 'dashboard-distributor',
        name: 'menu_dashboard.dashboard',
        type: 'link',
        icon: 'ti-dashboard', 
        show: false,
      }, 
      {
        state: 'dashboard-organization',
        name: 'menu_dashboard.dashboard',
        type: 'link',
        icon: 'ti-dashboard', 
        show: false,
      },
      {
        state: 'dashboard-trainer',
        name: 'menu_dashboard.dashboard',
        type: 'link',
        icon: 'ti-dashboard', 
        show: false,
      }, 
      {
        state: 'dashboard-student',
        name: 'menu_dashboard.dashboard',
        type: 'link',
        icon: 'ti-dashboard', 
        show: false,
      },  
	   {
		  state: 'instructor-report',
		  name:'instructor report',
		  type:'sub',
		  icon:'ti-server',
		  show:true,
		  children:[
		  {
            state: 'instructor-list',
            name: 'instructor list',
            show: true,
            type: 'link'
          }
		  ]
	  },
      {
        state: 'admin',
        name: 'menu_admin.admin',
        type: 'sub',
        icon: 'ti-server',
        show: false,
        children: [
          {
            state: 'activities',
            name: 'menu_admin.activities.activities',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'activities-list',
                name: 'menu_admin.activities.list_activities',
                show: false,
                type: 'link'
              },
              {
                state: 'activities-add',
                name: 'menu_admin.activities.add_activities',
                show: false,
                type: 'link'
              },
            ]
          },
          {
            state: 'products',
            name: 'menu_admin.products.products',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'list-category',
                name: 'menu_admin.products.list_category',
                show: false,
                type: 'link'
              },
              {
                state: 'add-category',
                name: 'menu_admin.products.add_category',
                show: false,
                type: 'link'
              },
              {
                state: 'products-list',
                name: 'menu_admin.products.list_product',
                show: false,
                type: 'link'
              },
              {
                state: 'products-add',
                name: 'menu_admin.products.add_product',
                show: false,
                type: 'link'
              },
            ]
          },
          {
            state: 'sku',
            name: 'menu_admin.sku.sku',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'list-sku',
                name: 'menu_admin.sku.list_sku',
                show: false,
                type: 'link'
              },
              {
                state: 'add-sku',
                name: 'menu_admin.sku.add_sku',
                show: false,
                type: 'link'
              },
            ]
          },
          {
            state: 'glossary',
            name: 'menu_admin.glossary.glossary',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'glossary-list',
                name: 'menu_admin.glossary.list_glossary',
                show: false,
                type: 'link'
              },
              {
                state: 'glossary-add',
                name: 'menu_admin.glossary.add_glossary',
                show: false,
                type: 'link'
              }
              // {
              //   state: 'glossary-update',
              //   name: 'Edit Glossary',
              //   show: false,
              //   type: 'link'
              // }
            ]
          },
          {
            state: 'country',
            name: 'menu_admin.country.country',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'geo-list',
                name: 'menu_admin.country.list_geo',
                show: false,
                type: 'link'
              },
              {
                state: 'geo-add',
                name: 'menu_admin.country.add_geo',
                show: false,
                type: 'link'
              },
              {
                state: 'territory-list',
                name: 'menu_admin.country.list_territory',
                show: false,
                type: 'link'
              },
              {
                state: 'territory-add',
                name: 'menu_admin.country.add_territory',
                show: false,
                type: 'link'
              },
              {
                state: 'region-list',
                name: 'menu_admin.country.list_region',
                show: false,
                type: 'link'
              },
              {
                state: 'region-add',
                name: 'menu_admin.country.add_region',
                show: false,
                type: 'link'
              },
              {
                state: 'sub-region-list',
                name: 'menu_admin.country.list_sub_region',
                show: false,
                type: 'link'
              },
              {
                state: 'sub-region-add',
                name: 'menu_admin.country.add_sub_region',
                show: false,
                type: 'link'
              },
              {
                state: 'countries-list',
                name: 'menu_admin.country.list_countries',
                show: false,
                type: 'link'
              },
              {
                state: 'countries-add',
                name: 'menu_admin.country.add_countries',
                show: false,
                type: 'link'
              },
            ]
          },
          {
            state: 'market-type',
            name: 'menu_admin.market_type.market_type',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'market-type-list',
                name: 'menu_admin.market_type.list_market_type',
                show: false,
                type: 'link'
              },
              {
                state: 'market-type-add',
                name: 'menu_admin.market_type.add_market_type',
                show: false,
                type: 'link'
              },
            ]
          },
          // {
          //   state: 'site-services',
          //   name: 'Site Services',
          //   type: 'sub',
          //   show: false,
          //   children: [
          //     {
          //       state: 'site-services-list',
          //       name: 'List Site Services',
          //       show: false,
          //       type: 'link'
          //     },
          //     {
          //       state: 'site-services-add',
          //       name: 'Add Site Services',
          //       show: false,
          //       type: 'link'
          //     },
          //   ]
          // },
          {
            state: 'variables',
            name: 'menu_admin.variables.variables',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'variables-list',
                name: 'menu_admin.variables.list_variables',
                show: false,
                type: 'link'
              },
              {
                state: 'variables-add',
                name: 'menu_admin.variables.add_variables',
                show: false,
                type: 'link'
              },
            ]
          },
          {
            state: 'partner-type',
            name: 'menu_admin.partner_type.menu_partner_type',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'list-sub-partner-type',
                name: 'menu_admin.partner_type.sub_partner_type',
                show: false,
                type: 'link'
              },
              {
                state: 'list-partner-type',
                name: 'menu_admin.partner_type.partner_type',
                show: false,
                type: 'link'
              },
            ]
          },
          {
            state: 'distributor',
            name: 'menu_admin.distributor.distributor',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'search-distributor',
                name: 'menu_admin.distributor.search_distributor',
                show: false,
                type: 'link'
              },
              {
                state: 'add-distributor',
                name: 'menu_admin.distributor.add_distributor',
                show: false,
                type: 'link'
              }
            ]
          },
          // {
          //   state: 'setup-language',
          //   name: 'Setup Language',
          //   show: false,
          //   type: 'link'
          // },
          {
            state: 'currency',
            name: 'menu_admin.currency.currency',
            show: false,
            type: 'link'
          },
          //aek tambah menu language 02/03/2018
          {
            state: 'language',
            name: 'menu_admin.language.language',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'list-language',
                name: 'menu_admin.language.list_language',
                show: false,
                type: 'link'
              }
              // {
              //   state: 'add-language',
              //   name: 'Add Language',
              //   show: false,
              //   type: 'link'
              // },
            ]
          },
          {
            state: 'email',
            name: 'menu_admin.email_body.email_body',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'list-email-body',
                name: 'menu_admin.email_body.list_email_body',
                show: false,
                type: 'link'
              },
              {
                state: 'add-edit-email',
                name: 'menu_admin.email_body.add_edit_email_body',
                show: false,
                type: 'link'
              },
            ]
          },
        ]
      },
      {
        state: 'agreement',
        name: 'Agreements',
        show: false,
        icon: 'ti-file',
        type: 'link'
      },
      //{
      //  state: 'sqlquery',
      //  name: 'SQL Query',
      //  show: true,
      //  icon: 'ti-file',
      //  type: 'link'
      //},
      {
        state: 'manage',
        name: 'menu_manage.manage',
        type: 'sub',
        icon: 'ti-layout-grid2-alt',
        show: false,
        children: [
          {
            state: 'organization',
            name: 'menu_manage.organization.organization',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'add-organization',
                name: 'menu_manage.organization.add_organization',
                show: false,
                type: 'link'
              },
              {
                state: 'search-organization',
                name: 'menu_manage.organization.search_organization',
                show: false,
                type: 'link'
              }
            ]
          },
          {
            state: 'site',
            name: 'menu_manage.site.site',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'search-site',
                name: 'menu_manage.site.search_site',
                show: false,
                type: 'link'
              }
            ]
          },
          {
            state: 'contact',
            name: 'menu_manage.contact.contact',
            type: 'sub',
            show: false,
            children: [
              {
                state: 'search-contact',
                name: 'menu_manage.contact.search_contact',
                show: false,
                type: 'link'
              },
              //add aek 04/03/2018
              {
                state: 'add-new-contact',
                name: 'menu_manage.contact.add_new_contact',
                show: false,
                type: 'link'
              }
            ]
          },
          {
            state: 'search-general',
            name: 'epdb.manage.contact.search_general.general_search',
            show: false,
            type: 'link'
          }
          // {
          //   state: 'aci',
          //   name: 'ACI',
          //   type: 'link',
          //   show: false,
          // },
        ]
      },

      {
        state: 'evaluation',
        name: 'menu_manage_evaluation.manage_evaluation',
        type: 'sub',
        icon: 'ti-clipboard',
        show: false,
        children: [
          {
            state: 'survey-template-list',
            name: 'menu_manage_evaluation.list_template',
            show: false,
            type: 'link'
          },
          {
            state: 'add-edit-templatedata',
            name: 'menu_manage_evaluation.add_template',
            show: false,
            type: 'link'
          },
          // {
          //   state: 'survey-answer-list',
          //   name: 'menu_manage_evaluation.take_survey',
          //   show: false,
          //   type: 'link'
          // },
          {
            state: 'survey-template-copy',
            name: 'menu_manage_evaluation.copy_template',
            show: false,
            type: 'link'
          },
        ]
      },

      {
        state: 'course',
        name: 'menu_manage_course.manage_course',
        type: 'sub',
        icon: 'ti-book',
        show: false,
        children: [
          {
            state: 'course-list',
            name: 'menu_course.search_course',
            show: false,
            type: 'link'
          },
          {
            state: 'course-add',
            name: 'menu_manage_course.add_course',
            show: false,
            type: 'link'
          },
        ]
      },

      {
        state: 'studentinfo',
        name: 'menu_manage_student.manage_student_info',
        type: 'sub',
        icon: 'ti-user',
        show: false,
        children: [
          {
            state: 'search-student',
            name: 'menu_manage_student.list_student',
            show: false,
            type: 'link'
          },
          {
            state: 'terminate-student',
            name: 'menu_manage_student.list_request_terminate',
            show: false,
            type: 'link'
          },
        ]
      },

      {
        state: 'certificate',
        name: 'menu_manage_certificate.manage_certificate',
        type: 'sub',
        icon: 'ti-file',
        show: false,
        children: [
          {
            state: 'certificate-list-background',
            name: 'menu_manage_certificate.list_background',
            show: false,
            type: 'link'
          },
          {
            state: 'certificate-add-background',
            name: 'menu_manage_certificate.add_background',
            show: false,
            type: 'link'
          },
          {
            state: 'certificate-list-design',
            name: 'menu_manage_certificate.list_certificate',
            show: false,
            type: 'link'
          },
          {
            state: 'certificate-add-design',
            name: 'menu_manage_certificate.add_certificate',
            show: false,
            type: 'link'
          },
        ]
      },

      {
        state: 'student',
        name: 'menu_course.course',
        type: 'sub',
        icon: 'ti-pencil',
        show: false,
        children: [
          {
            state: 'all-course',
            name: 'menu_course.all_course',
            show: false,
            type: 'link'
          },
          {
            state: 'search-course',
            name: 'menu_course.search_course',
            show: false,
            type: 'link'
          },
          {
            state: 'my-course',
            name: 'menu_course.my_course',
            show: false,
            type: 'link'
          },
          {
            state: 'next-course',
            name: 'menu_course.next_course',
            show: false,
            type: 'link'
          },
          {
            state: 'history-course',
            name: 'dashboard.student.finished_course',
            show: false,
            type: 'link'
          },
        ]
      },

      {
        state: 'report',
        name: 'menu_report_epdb.report_epdb',
        type: 'sub',
        icon: 'ti-printer',
        show: false,
        children: [
          {
            state: 'organization-report',
            name: 'menu_report_epdb.organization_report',
            show: false,
            type: 'link'
          },
          {
            state: 'sites-report',
            name: 'menu_report_epdb.site_report',
            show: false,
            type: 'link'
          },
          {
            state: 'contact-report',
            name: 'menu_report_epdb.contact_report',
            show: false,
            type: 'link'
          },
          {
            state: 'instructor-request',
            name: 'menu_report_epdb.instructor-request',
            show: true,
            type: 'link'
          },
          // {
          //   state: 'email-list-generator',
          //   name: 'menu_report_epdb.email_list_generator',
          //   show: false,
          //   type: 'link'
          // },
          // {
          //   state: 'show-security-roles',
          //   name: 'menu_report_epdb.show_security_role',
          //   show: false,
          //   type: 'link'
          // },
          // {
          //   state: 'site-accreditations',
          //   name: 'menu_report_epdb.site_accreditation_report',
          //   show: false,
          //   type: 'link'
          // },
          // {
          //   state: 'beta-report-builder',
          //   name: 'BETA Report Builder',
          //   show: false,
          //   type: 'link'
          // }
          {
            state: 'organization-journal-entry-report',
            name: 'menu_report_epdb.org_journal_entry',
            show: false,
            type: 'link'
          },
          {
            state: 'site-journal-entry-report',
            name: 'menu_report_epdb.site_journal_entry',
            show: false,
            type: 'link'
          },
          {
            state: 'invoice-report',
            name: 'menu_report_epdb.invoice_report',
            show: false,
            type: 'link'
          },
          {
            state: 'invoice-report-new',
            name: 'Fee Management Report',
            show: true,
            type: 'link'
          },
          {
            state: 'qualifications-by-contact-report',
            name: 'menu_report_epdb.qualification_contact_report',
            show: false,
            type: 'link'
          },
          {
            state: 'site-attributes-report',
            name: 'menu_report_epdb.site_attribute_report',
            show: false,
            type: 'link'
          },
          {
            state: 'contact-attributes-report',
            name: 'menu_report_epdb.contact_attribute_report',
            show: false,
            type: 'link'
          },
          {
            state: 'ATC-AAP-accreditation-counts-report',
            name: 'menu_report_epdb.atc_aap_acc_count_report',
            show: false,
            type: 'link'
          },
          // {
          //   state: 'history-report',
          //   name: 'menu_report_epdb.history_report',
          //   show: false,
          //   type: 'link'
          // },
          {
            state: 'database-integrity-report',
            name: 'menu_report_epdb.db_integrity_report',
            show: false,
            type: 'link'
          },
          {
            state: 'history-report',
            name: 'menu_report_epdb.history_report',
            show: false,
            type: 'link'
          }
        ]
      },

      {
        state: 'report-eva',
        name: 'menu_report_eva.report_eva',
        type: 'sub',
        icon: 'ti-printer',
        show: false,
        children: [
          {
            state: 'organization-report-eva',
            name: 'epdb.report_eva.organization_report.organization_report',
            show: false,
            type: 'link'
          },
          {
            state: 'performance-overview-eva',
            name: 'epdb.report_eva.performance_overview.performance_overview',
            show: false,
            type: 'link'
          },
          {
            state: 'instructor-performance-eva',
            name: 'epdb.report_eva.instructor_performance.instructor_performance',
            show: false,
            type: 'link'
          },
          {
            state: 'evaluation-responses',
            name: 'epdb.report_eva.post_evaluation_responses.post_evaluation_responses',
            show: false,
            type: 'link'
          },
          {
            state: 'student-search-eva',
            name: 'epdb.report_eva.student_search.student_search',
            show: false,
            type: 'link'
          },
          {
            state: 'download-site-eva',
            name: 'epdb.report_eva.download_site_evaluation.download_site_evaluation',
            show: false,
            type: 'link'
          },
          {
            state: 'channel-performance-eva',
            name: 'epdb.report_eva.channel_performance.channel_performance',
            show: false,
            type: 'link'
          },
          //add product trained report dari google spreadsheet
          {
            state: 'product-trained-eva',
            name: 'Product Trained',
            show: false,
            type: 'link'
          },
          {
            state: 'product-trained-statistic',
            name: 'Product Trained Statistics',
            show: false,
            type: 'link'
          },
          {
            state: 'submissions-by-language-eva',
            name: 'epdb.report_eva.submission_language.submission_language',
            show: false,
            type: 'link'
          },
          {
            state: 'audit',
            name: 'epdb.report_eva.audit.audit',
            show: false,
            type: 'link'
          },
          // {
          //   state: 'post-evaluation-responses',
          //   name: 'Post Evaluation Responses',
          //   show: false,
          //   type: 'link'
          // },
          // {
          //   state: 'comparison-report',
          //   name: 'Comparison Report',
          //   show: false,
          //   type: 'link'
          // }
        ]
      },

      {
        state: 'role',
        name: 'menu_role.role',
        type: 'sub',
        icon: 'ti-lock',
        show: false,
        children: [
          {
            state: 'add-role',
            name: 'menu_role.add_role',
            show: false,
            type: 'link'
          },
          {
            state: 'list-access',
            name: 'menu_role.list_access',
            show: false,
            type: 'link'
          },
        ]
      },


    ],
  },

];

@Injectable()
export class MenuItemsNew {
  constructor(public session: SessionService, private router: Router) { }


  viewRoles() {
    if (this.session.isLoggedIn()) {

      var arr = MENUITEMSNEW[0].main;
      var access = JSON.parse(this.session.getData()).Access;
      var root = this.router.url.substring(1);
      var link = "";
      //this.recursif(arr, level);

      //level 1
      for (var i = 0; i < arr.length; i++) {
        for (const key of Object.keys(arr[i])) {
          if (key == "type") {
            if (arr[i]["type"] == "sub") {
              //console.log(arr[i].state);
              link = arr[i].state;
              for (const key of Object.keys(access)) {
                if (link == key) {
                  var split1 = access[key];
                  var split2 = split1.split('');
                  if (split2[0] == "1") {
                    arr[i].show = true;
                  } else {
                    arr[i].show = false;
                  }
                }
              }

              //level 2
              for (var j = 0; j < arr[i]["children"].length; j++) {
                for (const key of Object.keys(arr[i]["children"][j])) {
                  if (key == "type") {
                    if (arr[i]["children"][j]["type"] == "sub") {

                      link = arr[i].state + "/" + arr[i]["children"][j].state;
                      // console.log(link);

                      for (const key of Object.keys(access)) {
                        if (link == key) {
                          var split1 = access[key];
                          var split2 = split1.split('');
                          if (split2[0] == "1") {
                            // console.log("boleh");
                            arr[i]["children"][j].show = true;
                          } else {
                            // console.log("ditolak");
                            arr[i]["children"][j].show = false;
                          }
                        }
                      }


                      //level 3
                      for (var k = 0; k < arr[i]["children"][j]["children"].length; k++) {
                        for (const key of Object.keys(arr[i]["children"][j]["children"][k])) {
                          if (key == "type") {
                            if (arr[i]["children"][j]["children"][k]["type"] == "sub") {

                              link = arr[i].state + "/" + arr[i]["children"][j].state + "/" + arr[i]["children"][j]["children"][k].state;
                              // console.log(link);

                              for (const key of Object.keys(access)) {
                                if (link == key) {
                                  var split1 = access[key];
                                  var split2 = split1.split('');
                                  if (split2[0] == "1") {
                                    // console.log("boleh");
                                    arr[i]["children"][j]["children"][k].show = true;
                                  } else {
                                    // console.log("ditolak");
                                    arr[i]["children"][j]["children"][k].show = false;
                                  }
                                }
                              }

                            }
                            else {
                              link = arr[i].state + "/" + arr[i]["children"][j].state + "/" + arr[i]["children"][j]["children"][k].state;
                              // console.log(link);

                              for (const key of Object.keys(access)) {
                                if (link == key) {
                                  var split1 = access[key];
                                  var split2 = split1.split('');
                                  if (split2[0] == "1") {
                                    // console.log("boleh");
                                    arr[i]["children"][j]["children"][k].show = true;
                                  } else {
                                    // console.log("ditolak");
                                    arr[i]["children"][j]["children"][k].show = false;
                                  }
                                }
                              }

                            }
                          }
                        }
                      }


                    }
                    else {
                      link = arr[i].state + "/" + arr[i]["children"][j].state;
                      // console.log(link);

                      for (const key of Object.keys(access)) {
                        if (link == key) {
                          var split1 = access[key];
                          var split2 = split1.split('');
                          if (split2[0] == "1") {
                            // console.log("boleh");
                            arr[i]["children"][j].show = true;
                          } else {
                            // console.log("ditolak");
                            arr[i]["children"][j].show = false;
                          }
                        }
                      }

                    }
                  }
                }
              }
            }
            else {
              link = arr[i].state;
              // console.log(link);

              for (const key of Object.keys(access)) {
                if (link == key) {
                  var split1 = access[key];
                  var split2 = split1.split('');
                  if (split2[0] == "1") {
                    // console.log("boleh");
                    arr[i].show = true;
                  } else {
                    // console.log("ditolak");
                    arr[i].show = false;
                  }
                }
              }

            }
          }
        }
      }


      var arr2 = new Array();
      var obj = new Object();

      obj['label'] = "";
      obj['main'] = arr;

      arr2[0] = obj;

      MENUITEMSNEW2 = arr2;

      // console.log(MENUITEMSNEW2);
    }
    else {
      MENUITEMSNEW2 = MENUITEMSNEW;
    }

  }

  getAll(): Menu[] {
    return MENUITEMSNEW2;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
