export class htmlentityService{

    private vslueStr: string;
    constructor(){}
    
    encoder(str){
        this.vslueStr=str;
        for (var i=str.length-1;i>=0;i--) {
          if (str[i] == "'" || str[i] == '"' || str[i] == "\n" || str[i] == "\\" || str[i] == "{" || str[i] == "}")
            {
                var replacement='&#'+str[i].charCodeAt()+';';
                this.vslueStr=this.vslueStr.substr(0, i) + replacement+ this.vslueStr.substr(i+1);  
            }
        }
        return this.vslueStr;
    }
    decoder (str) {
        return str.replace(/&#(\d+);/g, function(match, dec) {
            return String.fromCharCode(dec);
        });
    }

    
}
