import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class AppFormatDate {

    constructor(private _http: Http) {}

    //for new Date 
    dateJStoYMD(str) {
        
        var d = new Date(str);

        var year = d.getFullYear(); 

        var month = '' + (d.getMonth() + 1);
        if (month.length == 1)
        month = "0" + month;
    
        var day = '' + d.getDate();
        if (day.length == 1)
        day = "0" + day;

        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();

        var dates = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;

        return dates;
    }

    //for calendar
    dateCalendarToYMD(str) {
    
        var obj = JSON.stringify(str);
        var json = JSON.parse(obj); 
    
        var jm = json['month']; //months from 1-12
        var jd = json['day'];
        var jy = json['year'];
    
        var date = jy + "/" + jm + "/" + jd;
    
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
    
        return [year, month, day].join('-');
    }
    
    //for calendar
    dateYMDToCalendar(str) {
        
        var e = new Date(str);
            
        var obj = {year:e.getFullYear(), month:(e.getMonth()+1), day:+e.getDate()}; 

        return obj;
    }

}