import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
    userlevel: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsEVA {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
    userlevel: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsEVA[];
}

const MENUITEMSEVA = [
  {
    label: '',
    main: [
      {
        state: 'dashboard',
        main_state: 'eva',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard',
        userlevel:'SUPERADMIN,ADMIN,DISTRIBUTOR,ORGANIZATION,TRAINER'
      },
      {
        state: 'eva',
        name: 'Manage Evaluation',
        type: 'sub',
        icon: 'ti-clipboard',
        userlevel:'SUPERADMIN,ADMIN',
        children: [
         {
           state: 'survey-template-list',
           name: 'List Template',
           userlevel:'SUPERADMIN,ADMIN'
         },
         {
           state: 'add-edit-templatedata',
           name: 'Add Template',
           userlevel : 'SUPERADMIN,ADMIN'
         },
         {
           state: 'survey-answer-list',
           name: 'Take Evalution Survey',
           userlevel:'SUPERADMIN,ADMIN'
         },

       ]
      },
      {
        state: 'eva',
        name: 'Manage Course',
        type: 'sub',
        icon: 'ti-book',
        userlevel:'SUPERADMIN,ADMIN',
        children: [
          {
            state: 'course-list',
            name: 'List Course',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'course-add',
            name: 'Add Course',
            userlevel:'SUPERADMIN,ADMIN'
          }
          // {
          //   state: 'student-list',
          //   name: 'Tag Student'
          // },

        ]
      },
      {
        state: 'eva',
        name: 'Manage Student information',
        type: 'sub',
        icon: 'ti-user',
        userlevel:'SUPERADMIN,ADMIN,DISTRIBUTOR,ORGANIZATION,TRAINER',
        children: [
          {
            state: 'search-student',
            name: 'List Student',
            userlevel:'SUPERADMIN,ADMIN,DISTRIBUTOR,ORGANIZATION,TRAINER'
          }
          // {
          //   state: 'update-student-information',
          //   name: 'Update Student Information'
          // }
        ]
      },
      {
        state: 'eva',
        name: 'Manage Certificate',
        type: 'sub',
        icon: 'ti-file',
        userlevel:'SUPERADMIN,ADMIN',
        children: [
          {
            state: 'certificate-list-background',
            name: 'List Background',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'certificate-add-background',
            name: 'Add Background',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'certificate-list-design',
            name: 'List Certificate',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'certificate-add-design',
            name: 'Add Certificate',
            userlevel:'SUPERADMIN,ADMIN'
          }
        ]
      },
      {
        state: 'eva',
        name: 'Report',
        type: 'sub',
        icon: 'ti-printer',
        userlevel:'SUPERADMIN,ADMIN',
        children: [
          {
            state: 'organization', //edited by aek 27-12-2017
            name: 'Organization',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'performance-overview', //edited by aek 28-12-2017
            name: 'Performance Overview',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'instructor-performance', //edited by aek 28-12-2017
            name: 'Instructor Performance',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'evaluation-responses', //edited by aek 28-12-2017
            name: 'Evaluation Responses',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'student-search', //edited by aek 28-12-2017
            name: 'Student Search',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'products-trained', //edited by aek 28-12-2017
            name: 'Products Trained',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'training-materials-used', //edited by aek 28-12-2017
            name: 'Training Materials Used',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'score-calculation-method', //edited by aek 28-12-2017
            name: 'Score Calculation Method',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'download-site-evaluations', //edited by aek 28-12-2017
            name: 'Download Site Evaluations',
            userlevel:'SUPERADMIN,ADMIN'
          },
          {
            state: 'help', //edited by aek 28-12-2017
            name: 'Help',
            userlevel:'SUPERADMIN,ADMIN'
          },
        ]
      },
      // {
      //   state: 'eva',
      //   name: 'Tag Student',
      //   type: 'sub',
      //   icon: 'ti-printer',
      //   children: [
      //     {
      //       state: 'student-list',
      //       name: 'List Student'
      //     },
      //   ]
      // },

      // {
      //   state: 'eva',
      //   name: 'Admin - Survey Report',
      //   type: 'sub',
      //   icon: 'ti-file',
      //   children: [
      //     {
      //       state: 'channel-performance', //edited by aek 28-12-2017
      //       name: 'Channel Performance'
      //     },
      //     {
      //       state: 'products-peformance', //edited by aek 28-12-2017
      //       name: 'Products Peformance'
      //     },
      //     {
      //       state: 'submissions-by-language', //edited by aek 28-12-2017
      //       name: 'Submissions by Language'
      //     },
      //     {
      //       state: 'award-nominees', //edited by aek 28-12-2017
      //       name: 'Award Nominees'
      //     },
      //     {
      //       state: 'audit', //edited by aek 28-12-2017
      //       name: 'Audit'
      //     },
      //     {
      //       state: 'management-summary', //edited by aek 28-12-2017
      //       name: 'Management Summary'
      //     },
      //     {
      //       state: 'data-integrity', //edited by aek 28-12-2017
      //       name: 'Data Integrity'
      //     },
      //     {
      //       state: 'certification-awareness-emails', //edited by aek 28-12-2017
      //       name: 'Certification Awareness Emails'
      //     },
      //     {
      //       state: 'olap', //edited by aek 28-12-2017
      //       name: 'OLAP'
      //     },
      //     {
      //       state: 'olap-2009', //edited by aek 28-12-2017
      //       name: 'OLAP 2009'
      //     },
      //     {
      //       state: 'compliance-reports', //edited by aek 28-12-2017
      //       name: 'Compliance Reports'
      //     },
      //     {
      //       state: 'products-trained-statistics', //edited by aek 28-12-2017
      //       name: 'Products Trained Statistics'
      //     },
      //     {
      //       state: 'stats-summary-reports', //edited by aek 28-12-2017
      //       name: 'Stats Summary Reports'
      //     },
      //   ]
      // },
      // {
      //   state: 'eva',
      //   name: 'Admin - Post-Survey Report',
      //   type: 'sub',
      //   icon: 'ti-files',
      //   children: [
      //     {
      //       state: 'post-evaluation-overview',
      //       name: 'Post Evaluation Overview'
      //     },
      //     {
      //       state: 'post-evaluation-responses',
      //       name: 'Post Evaluation Responses'
      //     },
      //     {
      //       state: 'comparison-report',
      //       name: 'Comparison Report'
      //     },
      //     {
      //       state: 'management-summary-psr',
      //       name: 'Management Summary'
      //     },
      //     {
      //       state: 'return-on-investment',
      //       name: 'Return On Investment'
      //     },
      //     {
      //       state: 'skills-acquired',
      //       name: 'Skills Acquired'
      //     },
      //   ]
      // },
      // {
      //   state: 'eva',
      //   name: 'Course Management Tool',
      //   type: 'sub',
      //   icon: 'ti-pin2',
      //   children: [
      //     {
      //       state: 'search-courses',
      //       name: 'Search Courses'
      //     },
      //     {
      //       state: 'add-course',
      //       name: 'Add Course'
      //     },
      //   ]
      // },
    ],
  },

];

@Injectable()
export class MenuItemsEVA {
  getAll(): Menu[] {
    return MENUITEMSEVA;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
