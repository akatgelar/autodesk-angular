import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";

@Injectable()
export class AppFilterGeo {

    messageError: string = '';

    constructor(private _http: Http,private service: AppService) { }

    filtered(arrTemp, arrDropdown) {
        arrTemp.forEach(item => {
            var exist = false;
            if (arrDropdown.length == 0) {
                arrDropdown.push(item);
            } else {
                arrDropdown.forEach(value => {
                    if (item.id == value.id) {
                        exist = true;
                    }
                })
                if (exist == false && item.id != "") {
                    arrDropdown.push(item);
                }
            }
        });
        return arrDropdown;
    }

    filterGeoOnSelect(id, dropdownListRegion): any {
        let dropdownListRegionTemp = [];
        var data = '';
        // this._http.get(
        //     "api/Region/RegionFilter/" + id,
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     })
        this.service.httpClientGet("api/Region/RegionFilter/" + id,'' )
            .subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListRegionTemp = JSON.parse(tmpresult).map((item) => {
                    //     return {
                    //         id: item.region_id,
                    //         itemName: item.region_name
                    //     }
                    // })
                    dropdownListRegionTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].region_id,
                          itemName: result[Index].region_name
                        }
                      })
                    this.filtered(dropdownListRegionTemp, dropdownListRegion);
                }
            },
                error => {
                    this.messageError = <any>error;
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterGeoOnDeSelect(id, dropdownListRegion): any {
        let dropdownListRegionTemp = [];
        var data = '';
        // this._http.get(
        //     "api/Region/RegionFilter/" + id,
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //})
        this.service.httpClientGet("api/Region/RegionFilter/" + id,'')
            .subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListRegionTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.region_id,
                    //         itemName: item.region_name
                    //     }
                    // })
                    dropdownListRegionTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].region_id,
                          itemName: result[Index].region_name
                        }
                      })
                    var index = JSON.stringify(dropdownListRegion).indexOf(JSON.stringify(dropdownListRegionTemp[0]));
                    if (index !== -1) {
                        dropdownListRegion.splice((index - 1), dropdownListRegionTemp.length);
                    }
                    return dropdownListRegion;
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterGeoOnSelectAll(selectedItemsGeo, dropdownListRegion): any {
        let dropdownListRegionTemp = [];
        var data = '';
        for (var i = 0; i < selectedItemsGeo.length; i++) {
            dropdownListRegionTemp = [];
            // this._http.get(
            //     "api/Region/RegionFilter/" + selectedItemsGeo[i].id,
            //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
            // )
            //     .map((response: Response) => {
            //         return response.text();
            //     }).subscribe(result => {
                this.service.httpClientGet("api/Region/RegionFilter/" + selectedItemsGeo[i].id,'')
                .subscribe(result => {
                    let tmpresult : any= result;
                    if (tmpresult == "Not found" || tmpresult == "[]") {
                        swal(
                            'Information!',
                            'Data not found.',
                            'error'
                        );
                    }
                    else {
                        // dropdownListRegionTemp = JSON.parse(result).map((item) => {
                        //     return {
                        //         id: item.region_id,
                        //         itemName: item.region_name
                        //     }
                        // })
                        dropdownListRegionTemp = Object.keys(tmpresult).map(function (Index) {
                            return {
                              id: result[Index].region_id,
                              itemName: result[Index].region_name
                            }
                          })
                        this.filtered(dropdownListRegionTemp, dropdownListRegion);
                    }
                },
                    error => {
                        this.messageError = <any>error
                        swal(
                            'Information!',
                            'Can\'t connect to server.',
                            'error'
                        );
                    });
        }
    }

    filterRegionOnSelect(id, dropdownListSubRegion): any {
        let dropdownListSubRegionTemp = [];
        var data = '';
        // this._http.get(
        //     "api/SubRegion/SubRegionFilter/" + id,
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     }).subscribe(result => {
            this.service.httpClientGet("api/SubRegion/SubRegionFilter/" + id,'')
            .subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListSubRegionTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.subregion_id,
                    //         itemName: item.subregion_name
                    //     }
                    // })
                    dropdownListSubRegionTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].subregion_id,
                          itemName: result[Index].subregion_name
                        }
                      })
                    this.filtered(dropdownListSubRegionTemp, dropdownListSubRegion);
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterRegionOnDeSelect(id, dropdownListSubRegion): any {
        let dropdownListSubRegionTemp = [];
        var data = '';
        // this._http.get(
        //     "api/SubRegion/SubRegionFilter/" + id,
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     })
        this.service.httpClientGet("api/SubRegion/SubRegionFilter/" + id,'')
            .subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListSubRegionTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.subregion_id,
                    //         itemName: item.subregion_name
                    //     }
                    // })
                    dropdownListSubRegionTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].subregion_id,
                          itemName: result[Index].subregion_name
                        }
                      })
                    var index = JSON.stringify(dropdownListSubRegion).indexOf(JSON.stringify(dropdownListSubRegionTemp[0]));

                    if (index !== -1) {
                        dropdownListSubRegion.splice((index - 1), dropdownListSubRegionTemp.length);
                    }
                    return dropdownListSubRegion;
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterRegionOnSelectAll(selectedItemsRegion, dropdownListSubRegion): any {
        let dropdownListSubRegionTemp = [];
        var data = '';
        for (var i = 0; i < selectedItemsRegion.length; i++) {
            dropdownListSubRegionTemp = [];
            // this._http.get(
            //     "api/SubRegion/SubRegionFilter/" + selectedItemsRegion[i].id,
            //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
            // )
            //     .map((response: Response) => {
            //         return response.text();
            //     }).subscribe(result => {
                this.service.httpClientGet("api/SubRegion/SubRegionFilter/" + selectedItemsRegion[i].id,'')
                .subscribe(result => {
                    let tmpresult : any= result;
                    if (tmpresult == "Not found" || tmpresult == "[]") {
                        swal(
                            'Information!',
                            'Data not found.',
                            'error'
                        );
                    }
                    else {
                        // dropdownListSubRegionTemp = JSON.parse(result).map((item) => {
                        //     return {
                        //         id: item.subregion_id,
                        //         itemName: item.subregion_name
                        //     }
                        // })
                        dropdownListSubRegionTemp = Object.keys(tmpresult).map(function (Index) {
                            return {
                              id: result[Index].subregion_id,
                              itemName: result[Index].subregion_name
                            }
                          })
                        this.filtered(dropdownListSubRegionTemp, dropdownListSubRegion);
                    }
                },
                    error => {
                        this.messageError = <any>error
                        swal(
                            'Information!',
                            'Can\'t connect to server.',
                            'error'
                        );
                    });
        }
    }

    filterSubRegionOnSelect(id, dropdownListCountry): any {
        let dropdownListCountryTemp = [];
        var data = '';
        // this._http.get(
        //     "api/Countries/CountryFilter/" + id,
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     }).subscribe(result => {
            this.service.httpClientGet("api/Countries/CountryFilter/" + id,'')
            .subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListCountryTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.countries_id,
                    //         itemName: item.countries_name
                    //     }

                    // })
                    dropdownListCountryTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].countries_id,
                          itemName: result[Index].countries_name
                        }
                      })
                    let temp = [];
                    for (let i = 0; i < dropdownListCountryTemp.length; i++) {
                        if (dropdownListCountryTemp[i] != undefined) {
                            temp.push(dropdownListCountryTemp[i]);
                        }
                    }
                    this.filtered(temp, dropdownListCountry);
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterSubRegionOnDeSelect(id, dropdownListCountry): any {
        let dropdownListCountryTemp = [];
        var data = '';
        // this._http.get(
        //     "api/Countries/CountryFilter/" + id,
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     }).subscribe(result => {
            this.service.httpClientGet("api/Countries/CountryFilter/" + id,'')
            .subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                    dropdownListCountryTemp = null;
                }
                else {
                    // dropdownListCountryTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.countries_id,
                    //         itemName: item.countries_name
                    //     }
                    // })
                    dropdownListCountryTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].countries_id,
                          itemName: result[Index].countries_name
                        }
                      })
                    var index = JSON.stringify(dropdownListCountry).indexOf(JSON.stringify(dropdownListCountryTemp[0]));
                    if (index !== -1) {
                        dropdownListCountry.splice((index - 1), dropdownListCountryTemp.length);
                    }
                    return dropdownListCountry;
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterSubRegionOnSelectAll(selectedItemsSubRegion, dropdownListCountry): any {
        var data = '';
        let dropdownListCountryTemp = [];
        for (var i = 0; i < selectedItemsSubRegion.length; i++) {
            dropdownListCountryTemp = [];
            // this._http.get(
            //     "api/Countries/CountryFilter/" + selectedItemsSubRegion[i].id,
            //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
            // )
            //     .map((response: Response) => {
            //         return response.text();
            //     }).subscribe(result => {
                this.service.httpClientGet("api/Countries/CountryFilter/" + selectedItemsSubRegion[i].id,'')
                .subscribe(result => {
                    let tmpresult : any= result;
                    if (tmpresult == "Not found" || tmpresult == "[]") {
                        swal(
                            'Information!',
                            'Data not found.',
                            'error'
                        );
                    }
                    else {
                        // dropdownListCountryTemp = JSON.parse(result).map((item) => {
                        //     return {
                        //         id: item.countries_id,
                        //         itemName: item.countries_name
                        //     }
                        // })
                        dropdownListCountryTemp = Object.keys(tmpresult).map(function (Index) {
                            return {
                              id: result[Index].countries_id,
                              itemName: result[Index].countries_name
                            }
                          })
                        let temp = [];
                        for (let i = 0; i < dropdownListCountryTemp.length; i++) {
                            if (dropdownListCountryTemp[i] != undefined) {
                                temp.push(dropdownListCountryTemp[i]);
                            }
                        }
                        this.filtered(temp, dropdownListCountry);
                    }
                },
                    error => {
                        this.messageError = <any>error
                        swal(
                            'Information!',
                            'Can\'t connect to server.',
                            'error'
                        );
                    });
        }
    }

    filterCountriesOnSelect(id, dropdownListSubCountry): any {
        let dropdownListSubCountryTemp = [];
        var data = '';
        // this._http.get(
        //     "api/SubCountries/where/{'countries_code':'" + id + "'}",
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     }).subscribe(result => {
            this.service.httpClientGet("api/SubCountries/where/{'countries_code':'" + id + "'}",'').subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListSubCountryTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.subcountries_code,
                    //         itemName: item.subcountries_name
                    //     }
                    // })
                    dropdownListSubCountryTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].subcountries_code,
                          itemName: result[Index].counsubcountries_nametries_name
                        }
                      })
                    for (var j = 0; j < dropdownListSubCountryTemp.length; j++) {
                        dropdownListSubCountry.push(dropdownListSubCountryTemp[j]);
                    }
                    return dropdownListSubCountry;
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterCountriesOnDeSelect(id, dropdownListSubCountry): any {
        let dropdownListSubCountryTemp = [];
        var data = '';
        // this._http.get(
        //     "api/SubCountries/where/{'countries_code':'" + id + "'}",
        //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        // )
        //     .map((response: Response) => {
        //         return response.text();
        //     }).subscribe(result => {
            this.service.httpClientGet("api/SubCountries/where/{'countries_code':'" + id + "'}",'').subscribe(result => {
                let tmpresult : any= result;
                if (tmpresult == "Not found" || tmpresult == "[]") {
                    swal(
                        'Information!',
                        'Data not found.',
                        'error'
                    );
                }
                else {
                    // dropdownListSubCountryTemp = JSON.parse(result).map((item) => {
                    //     return {
                    //         id: item.subcountries_code,
                    //         itemName: item.subcountries_name
                    //     }
                    // })
                    dropdownListSubCountryTemp = Object.keys(tmpresult).map(function (Index) {
                        return {
                          id: result[Index].subcountries_code,
                          itemName: result[Index].counsubcountries_nametries_name
                        }
                      })
                    var index = JSON.stringify(dropdownListSubCountry).indexOf(JSON.stringify(dropdownListSubCountryTemp[0]));
                    if (index !== -1) {
                        dropdownListSubCountry.splice((index - 1), dropdownListSubCountryTemp.length);
                    }
                    return dropdownListSubCountry;
                }
            },
                error => {
                    this.messageError = <any>error
                    swal(
                        'Information!',
                        'Can\'t connect to server.',
                        'error'
                    );
                });
    }

    filterCountriesOnSelectAll(selectedItemsCountry, dropdownListSubCountry): any {
        var data = '';
        let dropdownListSubCountryTemp = [];
        for (var i = 0; i < selectedItemsCountry.length; i++) {
            dropdownListSubCountryTemp = [];
            // this._http.get(
            //     "api/SubCountries/where/{'countries_code':'" + selectedItemsCountry[i].id + "'}",
            //     { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
            // )
            //     .map((response: Response) => {
            //         return response.text();
            //     }).subscribe(result => {
                this.service.httpClientGet("api/SubCountries/where/{'countries_code':'" + selectedItemsCountry[i].id + "'}",'').subscribe(result => {
                    let tmpresult : any= result;
                    if (tmpresult == "Not found" || tmpresult == "[]") {
                        swal(
                            'Information!',
                            'Data not found.',
                            'error'
                        );
                    }
                    else {
                        // dropdownListSubCountryTemp = JSON.parse(result).map((item) => {
                        //     return {
                        //         id: item.subcountries_code,
                        //         itemName: item.subcountries_name
                        //     }
                        // })
                        dropdownListSubCountryTemp = Object.keys(tmpresult).map(function (Index) {
                            return {
                              id: result[Index].subcountries_code,
                              itemName: result[Index].counsubcountries_nametries_name
                            }
                          })
                        for (var j = 0; j < dropdownListSubCountryTemp.length; j++) {
                            dropdownListSubCountry.push(dropdownListSubCountryTemp[j]);
                        }
                        return dropdownListSubCountry;
                    }
                },
                    error => {
                        this.messageError = <any>error
                        swal(
                            'Information!',
                            'Can\'t connect to server.',
                            'error'
                        );
                    });
        }
    }
}