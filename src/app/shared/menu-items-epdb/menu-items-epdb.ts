import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  userlevel: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsEPDB {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  userlevel: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsEPDB[];
}

const MENUITEMSEPDB = [
  {
    label: '',
    main: [
      {
        state: 'dashboard',
        main_state: 'epdb',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard',
        userlevel:'SUPERADMIN,ADMIN,DISTRIBUTOR,ORGANIZATION'
      },
      {
        state: 'epdb',
        name: 'Admin',
        type: 'sub',
        icon: 'ti-server',
        userlevel:'SUPERADMIN,ADMIN',
        children: [
          // {
          //   state: 'administration',
          //   type: 'link',
          //   name: 'Administration'
          // },
          {
            state: 'activities',
            name: 'Activities',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'activities-list',
                name: 'List Activities',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'activities-add',
                name: 'Add Activities',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          // {
          //   state: 'invoice-type',
          //   name: 'Invoice Type',
          //   type: 'sub',
          //   children: [
          //     {
          //       state: 'invoice-type-list',
          //       name: 'List Invoice Type'
          //     },
          //     {
          //       state: 'invoice-type-add',
          //       name: 'Add Invoice Type'
          //     },
          //   ]
          // },
          // {
          //   state: 'line-item-summaries',
          //   name: 'Line Item Summaries',
          //   type: 'sub',
          //   children: [
          //     {
          //       state: 'line-item-summaries-list',
          //       name: 'List Line Item Summaries'
          //     },
          //     {
          //       state: 'line-item-summaries-add',
          //       name: 'Add Line Item Summaries'
          //     },
          //   ]
          // },
          {
            state: 'products',
            name: 'Products',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'list-category',
                name: 'List Category',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'add-category',
                name: 'Add Category',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'products-list',
                name: 'List Products',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'products-add',
                name: 'Add Products',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          {
            state: 'sku',
            name: 'SKU',
            type: 'sub',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'add-sku',
                name: 'Add SKU',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'list-sku',
                name: 'List SKU',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          {
            state: 'glosarry',
            name: 'Glossary',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'glosarry-list',
                name: 'List Glossary',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'glosarry-add',
                name: 'Add Glossary',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          {
            state: 'country',
            name: 'Country',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'geo-list',
                name: 'List Geo',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'geo-add',
                name: 'Add Geo',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'territory-list',
                name: 'List Territory',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'territory-add',
                name: 'Add Territory',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'region-list',
                name: 'List Region',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'region-add',
                name: 'Add Region',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'sub-region-list',
                name: 'List Sub Region',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'sub-region-add',
                name: 'Add Sub Region',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'countries-list',
                name: 'List Countries',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'countries-add',
                name: 'Add Countries',
                userlevel:'SUPERADMIN'
              },
              // {
              //   state: 'sub-countries-list',
              //   name: 'Sub Countries'
              // },
            ]
          },
          // {
          //   state: 'territory',
          //   name: 'Territory',
          //   type: 'sub',
          //   children: [
          //     {
          //       state: 'territory-list',
          //       name: 'List Territory'
          //     },
          //     {
          //       state: 'territory-add',
          //       name: 'Add Territory'
          //     },
          //   ]
          // },
          {
            state: 'market-type',
            name: 'Market Type',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'market-type-list',
                name: 'List Market Type',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'market-type-add',
                name: 'Add Market Type',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          {
            state: 'site-services',
            name: 'Site Services',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'site-services-list',
                name: 'List Site Services',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'site-services-add',
                name: 'Add Site Services',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          {
            state: 'variables',
            name: 'Variables',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'variables-list',
                name: 'List Variables',
                userlevel:'SUPERADMIN'
              },
              {
                state: 'variables-add',
                name: 'Add Variables',
                userlevel:'SUPERADMIN'
              },
            ]
          },
          {
            state: 'partner-type',
            name: 'Partner Type',
            type: 'sub',
            userlevel:'SUPERADMIN',
            children: [
              {
                state: 'list-sub-partner-type',
                name: 'Sub Partner Type',
                userlevel:'SUPERADMIN'
              },
              // {
              //   state: 'add-sub-partner-type',
              //   name: 'Add Sub Partner Type'
              // },
              {
                state: 'list-partner-type',
                name: 'Partner Type',
                userlevel:'SUPERADMIN'
              },
              // {
              //   state: 'add-partner-type',
              //   name: 'Add Partner Type'
              // }
            ]
          },
          {
            state: 'setup-language',
            type: 'link',
            name: 'Setup Language',
            userlevel:'SUPERADMIN'
          },
          {
            state: 'currency',
            type: 'link',
            name: 'Currency',
            userlevel:'SUPERADMIN'
          }
        ]
      },
      {
        state: 'epdb',
        main_state: 'epdb',
        name: 'User',
        type: 'sub',
        icon: 'ti-user',
        userlevel:'SUPERADMIN',
        children: [
          {
            state: 'view-user-information',
            name: 'View User Information',
            userlevel:'SUPERADMIN'
          },
          {
            state: 'change-user-information',
            name: 'Change User Information',
            userlevel:'SUPERADMIN'
          },
        ]
      },
      {
        state: 'epdb',
        main_state: 'epdb',
        name: 'Manage',
        type: 'sub',
        icon: 'ti-layout-grid2-alt',
        userlevel:'SUPERADMIN,ADMIN,DISTRIBUTOR,ORGANIZATION',
        children: [
          {
            state: 'organization',
            name: 'Organization',
            type: 'sub',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'add-organization',
                name: 'Add New Organization',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'search-organization',
                name: 'Search Organization',
                userlevel:'SUPERADMIN,ADMIN'
              }
            ]
          },
          {
            state: 'site',
            name: 'Site',
            type: 'sub',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'search-site',
                name: 'Search Site',
                userlevel:'SUPERADMIN,ADMIN'
              },
              // {
              //   state: 'add-site',
              //   name: 'Add Site'
              // },
            ]
          },
          {
            state: 'contact',
            name: 'Contact',
            type: 'sub',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'search-contact',
                name: 'Search Contact',
                userlevel:'SUPERADMIN,ADMIN'
              },
              // {
              //   state: 'add-contact',
              //   name: 'Add Contact'
              // },
              {
                state: 'search-general',
                name: 'Search General',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
          {
            state: 'distributor',
            name: 'Distributor',
            type: 'sub',
            userlevel:'SUPERADMIN,ADMIN',
            children:[
              {
                state: 'list-distributor',
                name: 'List Distributor',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'add-distributor',
                name: 'Add Distributor',
                userlevel:'SUPERADMIN,ADMIN'
              }
            ]
          },
          {
            state: 'aci',
            name: 'ACI',
            userlevel:'SUPERADMIN,ADMIN'
          }
        ]
      },
      {
        state: 'epdb',
        name: 'Report',
        type: 'sub',
        icon: 'ti-printer',
        userlevel:'SUPERADMIN,ADMIN',
        children: [
          {
            state: 'report-general-info',
            name: 'Report General Info',
            type: 'sub',
            icon: 'ti-layers',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'organization-report',
                name: 'Organization Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'sites-report',
                name: 'Sites Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'contact-report',
                name: 'Contact Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'email-list-generator',
                name: 'Email List Generator',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'show-security-roles',
                name: 'Show Security Roles',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'beta-report-builder',
                name: 'BETA Report Builder',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
          {
            state: 'report-database-info',
            name: 'Report Database Info',
            type: 'sub',
            icon: 'ti-layers',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'history-report',
                name: 'History Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'login-history-report',
                name: 'Login History Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'login-history-graphs-report',
                name: 'Login History Graphs Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'database-integrity-report',
                name: 'Database Integrity Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'regional-mappings-report',
                name: 'Regional Mappings Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
          {
            state: 'report-sales-partner',
            name: 'Report Sales Partner',
            type: 'sub',
            icon: 'ti-layers',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'var-organization-attributes-report',
                name: 'VAR Organization Attributes Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'var-invoices-report',
                name: 'VAR Invoices Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'var-site-attributes-report',
                name: 'VAR Site Attributes Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'var-contact-attributes-report',
                name: 'VAR Contact Attributes Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'var-contact-journal-entries-report',
                name: 'VAR Contact Journal Entries Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
          {
            state: 'training-partners',
            name: 'Training Partners',
            type: 'sub',
            icon: 'ti-layers',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'organization-journal-entry-report',
                name: 'Organization Journal Entry Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'organization-attributes-report',
                name: 'Organization Attributes Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'atc-invoices-report',
                name: 'ATC Invoices Report ',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'site-accreditations-report',
                name: 'Site Accreditations Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'qualifications-by-site-report',
                name: 'Qualifications By Site Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'site-journal-entry-report',
                name: 'Site Journal Entry Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'site-attributes-report',
                name: 'Site Attributes Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'serial-numbers-report',
                name: 'Serial Numbers Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'qualifications-by-contact-report',
                name: 'Qualifications By Contact Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'contact-journal-entry-report',
                name: 'Contact Journal Entry Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'contact-attributes-report',
                name: 'Contact Attributes Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
          {
            state: 'ATC-AAP-static-report',
            name: 'ATC/AAP Static Report',
            type: 'sub',
            icon: 'ti-layers',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'ATC-AAP-accreditation-counts-report',
                name: 'ATC/AAP Accreditation Counts Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'americas-ATC-locator-report',
                name: 'Americas ATC Locator Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'ATC-locator-2012-report-ALPHA',
                name: 'ATC Locator 2012 Report - ALPHA',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'show-attachments-report',
                name: 'Show Attachments Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
          {
            state: 'academic-partner-report',
            name: 'Academic Partner Report',
            type: 'sub',
            icon: 'ti-layers',
            userlevel:'SUPERADMIN,ADMIN',
            children: [
              {
                state: 'academic-programs-report',
                name: 'Academic Programs Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'academic-targets-report',
                name: 'Academic Targets Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
              {
                state: 'academic-projects-report',
                name: 'Academic Projects Report',
                userlevel:'SUPERADMIN,ADMIN'
              },
            ]
          },
        ]
      },
      {
        state: 'epdb',
        main_state: 'epdb',
        name: 'Role Access',
        type: 'sub',
        icon: 'ti-user',
        userlevel:'SUPERADMIN',
        children: [
          {
            state: 'add-role',
            name: 'Add Role',
            userlevel:'SUPERADMIN'
          },
          {
            state: 'list-access',
            name: 'List Access',
            userlevel:'SUPERADMIN'
          },
        ]
      }

      // komen dulu, belum dipake
      // {
      //   state: 'report',
      //   main_state: 'epdb',
      //   name: 'Report',
      //   type: 'link',
      //   icon: 'ti-printer'
      // },




      // {
      //   state: 'epdb',
      //   name: ' New Report',
      //   type: 'sub',
      //   icon: 'ti-printer',
      //   children: [
      //     {
      //       state: 'rpt-general-info',
      //       name: 'Report General Info',
      //       icon: 'ti-layers'
      //     },
      //     {
      //       state: 'rpt-database-info',
      //       name: 'Report Database Info',
      //       icon: 'ti-layers'
      //     },
      //     {
      //       state: 'rpt-sales-partner',
      //       name: 'Report Sales Partner',
      //       icon: 'ti-layers'
      //     },
      //     {
      //       state: 'rpt-training-partner',
      //       name: 'Training Partner',
      //       icon: 'ti-layers'
      //     },
      //     {
      //       state: 'rpt-atc-static',
      //       name: 'ATC/AAP Static Report',
      //       icon: 'ti-layers'
      //     },
      //     {
      //       state: 'rpt-academic-partner',
      //       name: 'Academic Partner Report',
      //       icon: 'ti-layers'
      //     }
      //   ]
      // },
      // {
      //   state: 'epdb',
      //   name: 'Help',
      //   type: 'sub',
      //   icon: 'ti-help',
      //   children: [
      //     {
      //       state: 'troubleshooting',
      //       name: 'Troubleshooting'
      //     },
      //     {
      //       state: 'glossary',
      //       name: 'Glossary'
      //     },
      //     {
      //       state: 'frequently-asked-questions',
      //       name: 'FAQ'
      //     },
      //     {
      //       state: 'merge-contacts',
      //       name: 'Merge Contacts'
      //     },
      //     {
      //       state: 'search-tips',
      //       name: 'Search Tips'
      //     },
      //     {
      //       state: 'bookmarks-favorites',
      //       name: 'Bookmarks / Favorites'
      //     },
      //     {
      //       state: 'encoding-information',
      //       name: 'Encoding Information'
      //     },
      //     {
      //       state: 'future-plans',
      //       name: 'Future Plans'
      //     },
      //     {
      //       state: 'test-plan',
      //       name: 'Test Plan'
      //     }
      //   ]
      // }
    ],
  },

];


@Injectable()
export class MenuItemsEPDB {
  getAll(): Menu[] {
    return MENUITEMSEPDB;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
