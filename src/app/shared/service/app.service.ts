import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
//import * as UserLevel from '../../../assets/userlevel/userlevel.json';
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
// import { Angular2TokenService } from 'angular2-token';
//common http
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as Rollbar from 'rollbar';

@Injectable()

export class AppService {
 private useraccessdata;
 private token;
 private appversion="";
 private userRole=[];
 private UserAccessRole="";
 private UserId="";
    constructor(private _http: Http, private http: HttpClient, private router: Router,private session: SessionService, private rollbar: Rollbar) {
		
		let useracces = this.session.getData();
		this.token=this.session.getToken();
		let UserLevelId="";
			
		
		if(useracces!=null){
		  this.useraccessdata = JSON.parse(useracces);
		 
		  this.appversion=this.useraccessdata.Version;
		 // UserLevelId=this.useraccessdata.UserLevelId;
		  this.UserId=this.useraccessdata.UserId;
		  
		}
		/*if(UserLevelId.indexOf(",") !== -1 && UserLevelId!==""){
			const splitted: string[] = UserLevelId.split(",");
			this.initUserLevelValue(splitted);
			this.UserAccessRole=this.getSuperiorRole(this.userRole);
		}else{
			this.UserAccessRole=UserLevelId;
		}*/
	}
     
	/* initUserLevelValue(userlevelID){
		
		 if(userlevelID.length>0)
		 {
			for(var i=0;i<userlevelID.length;i++){
			var value={};
			if(UserLevel.hasOwnProperty(userlevelID[i])){
				value["Name"]=userlevelID[i];
				value["Value"]=UserLevel[userlevelID[i]];
				//value[userlevelID[i]]=UserLevel[userlevelID[i]];
				this.userRole.push(value);
			}
		}
		 }
		
	 }
	getSuperiorRole(role){
			
		let superiorRole:any;
		role.sort(function(a, b) {
				return a.Value > b.Value;
		});
		var last = role[role.length - 1];
		return last.Name;
		
	}*/
    //api get
	
    get(url, data): Observable<string> {
        let res: any;
        return this._http.get(
            url,
            //{ headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '','Token':localStorage.getItem('autodesk-token')}) }
			//{ headers: new Headers({ 'Content-Type': 'application/json', 'Role':this.UserAccessRole,'UserId':this.UserId}) }
        )
            .map((response: Response) => {
			             
                return response.text();
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });
    }

    //post data
    post(url, data): Observable<string> {
        return this._http.post(
            url,
            data,
            //{ headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '','Token':localStorage.getItem('autodesk-token')}) }
			//{ headers: new Headers({ 'Content-Type': 'application/json'}) }
        )
            .map((response: Response) => {
		
                return response.text();
				
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });
    }

    //update data
    put(url, data): Observable<string> {
		
        return this._http.put(
            url,
            data,
            //{ headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '','this.this.token':localStorage.getItem('autodesk-this.token')}) }
			//{ headers: new Headers({ 'Content-Type': 'application/json', 'Role':this.UserAccessRole,'UserId':this.UserId}) }
        )
            .map((response: Response) => {
				
                return response.text();
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });;
    }

    //delete data
    delete(url, data): Observable<string> {
        return this._http.delete(
            url,
          // { headers: new Headers({ 'Content-Type': 'application/json'}) }
        )
            .map((response: Response) => {
				console.log(JSON.parse(response["_body"]));
                return response.text();
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });;
    }

    //error message action
    errorserver() {
        /*
         swal(
             'Information!',
            'Can\'t connect to server.',
            'error'
         );
         */
        console.log("Can't connect to server.")
    }

    //not found
    notfound() {
         swal(
             'Information!',
             'Data not found.',
             'error'
         );
       // console.log("Data not found.")
    }

    //success notification
    openSuccessSwal(message) {
        swal({
            title: 'Information!',
            text: message,
            type: 'success'
        }).catch(swal.noop);
    }


    //http client get
    httpClientGet(url, data) {
		 let headers = new HttpHeaders().set('Content-Type', 'application/json');
		 headers=headers.set('UserId',this.UserId);
        return this.http.get(url);
    }

    //http client post
    httpClientPost(url, data) {
        //let headers = new HttpHeaders().set('Content-Type', 'application/json');
		 //headers=headers.set('UserId',this.UserId);
        return this.http.post(url, data);
    }

    //http client post
    httpClientPostWithOptions(url, data, options) {
        return this.http.post(url, data, options);
    }

     //http client post
     httpCLientPut(url, data) {
        //let headers = new HttpHeaders().set('Content-Type', 'application/json');
		 //headers=headers.set('UserId',this.UserId);
        return this.http.put(url, data);
    }

    //http client update
    // httpCLientPut(url, id, data) {
	// 	var isUpdated;
    //   //  let headers = new HttpHeaders().set('Content-Type', 'application/json');
	// //	headers=headers.set('UserId',this.UserId);
    //    return this.http.put(url + '/' + id, data).subscribe(result => {
	// 		if(result['code']==1)
	// 		{
	// 			   swal(
    //                  'Information!',
    //                  "Successfully Updated!",
    //                  'success'
    //              );
	// 		}
	// 		else{
				
	// 			 swal(
    //                  'Information!',
    //                  "Error occured while processing your request!",
    //                  'error'
    //              );
	// 		}
    //     },
    //         error => {
    //             this.rollbar.log("error", "Something went wrong " + error);
               
    //         });
			
			
    // }
    httpClientDelete(url, data) {
        //let headers = new HttpHeaders().set('Content-Type', 'application/json');
		 //headers=headers.set('UserId',this.UserId);
        return this.http.delete(url, data);
    }
    //http client delete with confirm
    // httpClientDelete(url, data, id, index) {
    //     swal({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //     }).then(result => {
    //         if (result == true) {
    //             this.http.delete(url + '/' + id)
    //                 .subscribe(result => {
    //                     var resource = result;
	// 					console.log(JSON.parse(resource["_body"]));
    //                     if (resource['code'] == '1') {
    //                         if (index !== -1) {
    //                             data.splice(index, 1);
    //                         }
    //                     }
    //                     else {
    //                         swal(
    //                             'Information!',
    //                             "Delete Data Failed",
    //                             'error'
    //                         );
    //                         this.rollbar.warning("Delete data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
    //                     }
    //                 },
    //                     error => {
    //                         this.rollbar.log("error", "Something went wrong " + error);
                        
    //                     });
    //         }
    //     }).catch(swal.noop);
    // }

    //http client post
    httpClientPostArray(url, data) {
        var success = false;
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        //headers = headers.set('authorization', this.token);
		 headers = headers.set('Version', this.appversion);
        for (var i = 0; i < data.length; i++) {
            this.http.post(url, data[i], { headers }).subscribe(result => {
                if (result['code'] != '1') {
                    swal(
                        'Information!',
                        "Insert Data Failed",
                        'error'
                    );
                    this.rollbar.warning("Insert data failed at URL: " + url + "; Data: " + data[i]);
                }
            },
                error => {
                    this.rollbar.log("error", "Something went wrong " + error);
                 
                });
        }
    }

	/*
    //http client post
    httpClientPostModal(url, data, refreshdata, dataarray) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        //headers = headers.set('authorization', this.token);
        this.http.post(url, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                refreshdata.push(dataarray);
            }
            else {
                swal(
                    'Information!',
                    "Insert Data Failed",
                    'error'
                );
                this.rollbar.warning("Insert data failed at URL: " + url + "; Data: " + data);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }
	*/
	/*
	
    //http client update
    httpCLientPutModal(url, id, data, index, refreshdata, dataarray) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        //headers = headers.set('authorization', this.token);
		 headers = headers.set('Version', this.appversion);
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                if (index !== -1) {
                    refreshdata[index] = dataarray;
                }
            }
            else {
                swal(
                    'Information!',
                    "Update Data Failed",
                    'error'
                );
                this.rollbar.warning("Update data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }
*/
    
	getNativeWindow() {
        return window;
    }


    //http client update
    httpCLientPutPassword(url, id, data, routingtrue, routingfalse) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        //headers = headers.set('authorization', this.token);
		 headers = headers.set('Version', this.appversion);
        // console.log(url + '/' + id);
        var routingtruearr = routingtrue.split(',');
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                if (routingtruearr.length > 1) {
                    this.router.navigate([routingtruearr[0], routingtruearr[1], routingtruearr[2]]);
                } else {
                    var routelogoutrole = routingtrue.split('-');
                    this.router.navigate([routelogoutrole[0]], { queryParams: { id: id, role:routelogoutrole[1] } });
                }
            }
            else {
                swal({
                    title: 'Information!',
                    text: result['message'],
                    type: 'error'
                }).catch(swal.noop);
                // this.router.navigate([routingfalse], { queryParams: { id: id } });
                this.rollbar.warning(result['message'] + " at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                
            });
    }

    DateConversion(date) {
        var dateString = date.substr(0, 10).replace(/\b0/g, '');
        var dateConversion = dateString.split("/");
        var newConversion = dateConversion[0] + "-" + dateConversion[1] + "-" + dateConversion[2];
        return newConversion;
    }

    httpCLientPutNew(url, id, data, getdata) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        //headers = headers.set('authorization', this.token);
		 headers = headers.set('Version', this.appversion);
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                this.http.get(getdata);
            }
            else {
                swal(
                    'Information!',
                    "Update Data Failed",
                    'error'
                );
                this.rollbar.warning("Update data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
              
            });
    }

    backDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + (d.getDate() - 1),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    ISO_date (date) {
      var year = "" + date.getFullYear();
      var month = "" + (date.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
      var day = "" + date.getDate(); if (day.length == 1) { day = "0" + day; }
      var hour = "" + date.getHours(); if (hour.length == 1) { hour = "0" + hour; }
      var minute = "" + date.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
      var second = "" + date.getSeconds(); if (second.length == 1) { second = "0" + second; }
      
      return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }


    private vslueStr: string;
    
    encoder(str){
        this.vslueStr=str;
        for (var i=str.length-1;i>=0;i--) {
          if (str[i] == "'" || str[i] == '"' || str[i] == "\n" || str[i] == "\\" || str[i] == "{" || str[i] == "}" || str[i] == "/")
            {
                var replacement='&#'+str[i].charCodeAt()+';';
                this.vslueStr=this.vslueStr.substr(0, i) + replacement+ this.vslueStr.substr(i+1);  
            }
        }
        return this.vslueStr;
    }
    decoder (str) {
        return str.replace(/&#(\d+);/g, function(match, dec) {
            return String.fromCharCode(dec);
        });
    }
}



