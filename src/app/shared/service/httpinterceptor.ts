import { Observable ,  BehaviorSubject, EmptyError, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { SessionService } from '../../shared/service/session.service';
import {
  HttpInterceptor, HttpRequest, HttpHandler,  HttpResponse, HttpErrorResponse ,HttpEvent
} from '@angular/common/http';
import swal from 'sweetalert2';


@Injectable()
export class Interceptor implements HttpInterceptor {
	private useraccesdata: any;
	private timeLeft: number = 5;
	private stop: string;
	private subscription: Subscription;

  constructor(public session: SessionService,  private router: Router,private route: ActivatedRoute) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.useraccesdata=this.session.getData();
		const token:string = this.session.getToken();
		this.stop = localStorage.getItem("stop");
		this.subscription = this.route.params.subscribe();
    if (token != null) {
			request = request.clone({
				setHeaders: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				}
				
			});
		}else{
			request = request.clone({
				setHeaders: {
					'Content-Type':  'application/json',
					
				}
				
			});
		}
     return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
		  
      if (err.status === 406) {			
	
			swal({
					title: 'New Version Avaliable',
					text: "There is a newer version updated in the system. You will be log out of the system in few seconds, time to update your version",
					type: 'warning',						
				});
			setTimeout(() => {						
				localStorage.removeItem("language");
				localStorage.removeItem("autodesk-token");
			
				if(localStorage.getItem("firstLoad") == "true"){
					localStorage.removeItem("firstLoad");
					location.reload();
				}					
				this.session.logOut();
					if(this.useraccesdata != null) {
						if (this.useraccesdata.UserLevelId != "STUDENT") {
							this.router.navigate(["/login"]);
						}else{
							this.router.navigate(["/login-student"]);
						}
					}else{
				this.router.navigate(["/login"]);
						}

			}, 3000);		
				localStorage.setItem("stop","true");
				this.subscription.unsubscribe();	
				next.handle(request).subscribe().unsubscribe();
	}else if (err.status === 408 || err.status === 504) {			
		swal({
			title: 'Slow Connection to server',
			text: "Please click OK to try again, if you are running a huge report, please try to narrow down your filters",
			type: 'warning',						
		});
  	}else if(err.status === 403){
		   this.session.logOut();
			if(this.useraccesdata != null) {
				if (this.useraccesdata.UserLevelId != "STUDENT") {
					this.router.navigate(["/login"]);
				}else{
					this.router.navigate(["/login-student"]);
				}
			}else{
				    this.router.navigate(["/login"]);
			}
	  }else{
		            this.session.logOut();
					if(this.useraccesdata != null) {
						if (this.useraccesdata.UserLevelId != "STUDENT") {
							this.router.navigate(["/login"]);
						}else{
							this.router.navigate(["/login-student"]);
						}
					}else{
				this.router.navigate(["/login"]);
			}
	  	}
      }
		});					
	
  }
}
