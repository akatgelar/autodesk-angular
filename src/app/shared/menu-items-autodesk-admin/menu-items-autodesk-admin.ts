import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsAutodeskAdmin {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsAutodeskAdmin[];
}

const MENUITEMSAutodeskAdmin = [
  {
    label: '',
    main: [ 
      {
        state: 'dashboard',
        main_state: 'autodesk-admin',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard'
      },  
    ],
  },
 
];

@Injectable()
export class MenuItemsAutodeskAdmin {
  getAll(): Menu[] {
    return MENUITEMSAutodeskAdmin;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
