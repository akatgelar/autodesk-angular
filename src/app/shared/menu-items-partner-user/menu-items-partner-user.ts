import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsPartnerUser {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsPartnerUser[];
}

const MENUITEMSPartnerUser = [
  {
    label: '',
    main: [ 
      {
        state: 'dashboard',
        main_state: 'partner-user',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard'
      },  
    ],
  },
 
];

@Injectable()
export class MenuItemsPartnerUser {
  getAll(): Menu[] {
    return MENUITEMSPartnerUser;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
