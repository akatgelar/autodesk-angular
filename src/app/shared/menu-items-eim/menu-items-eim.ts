import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItemsEIM {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItemsEIM[];
}

const MENUITEMSEIM = [
  {
    label: '',
    main: [ 
      {
        state: 'dashboard',
        main_state: 'eim',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard'
      },
      {
        state: 'profile',
        main_state: 'eim',
        name: 'My Profile',
        type: 'link',
        icon: 'ti-id-badge'
      },
      // {
      //   state: 'contact',
      //   main_state: 'eim',
      //   name: 'Add Contact',
      //   type: 'link',
      //   icon: 'ti-user' 
      // },
      // {
      //   state: 'aci',
      //   main_state: 'eim',
      //   name: 'ACI Application Search',
      //   type: 'link',
      //   icon: 'ti-search' 
      // },
      // {
      //   state: 'sms',
      //   main_state: 'eim',
      //   name: 'SMS Configuration',
      //   type: 'link',
      //   icon: 'ti-mobile' 
      // },
      // {
      //   state: 'audit',
      //   main_state: 'eim',
      //   name: 'Audit Report',
      //   type: 'link',
      //   icon: 'ti-printer' 
      // }
    ],
  },
 
];

@Injectable()
export class MenuItemsEIM {
  getAll(): Menu[] {
    return MENUITEMSEIM;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
