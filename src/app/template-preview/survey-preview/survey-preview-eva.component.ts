import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import * as Survey from "survey-angular";
// import * as _ from "lodash";
// import {Pipe, PipeTransform} from "@angular/core";
var surveyJSON ={};
var datadetail: any;

function closePreview(){
    window.close();
}

@Component({
    selector: 'app-survey-preview-eva',
    templateUrl: './survey-preview-eva.component.html',
    styleUrls: [
        './survey-preview-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SurveyPreviewEVAComponent implements OnInit {

    private template;
    messageResult: string = '';
    messageError: string = '';
    public data: any;
    public datadetail: any;
    public json: any;

    constructor(private service: AppService, private route: ActivatedRoute) { }

    ngOnInit() {
        var data = '';
        var templateId = this.route.snapshot.params['id'];
        this.service.httpClientGet("api/EvaluationQuestion/" + templateId, data)
            .subscribe(res => {
                if (res == "Not found") {
                    this.service.notfound();
                    this.data = null;
                }
                else {
                    datadetail = res;
                    surveyJSON = JSON.stringify(datadetail.EvaluationQuestionTemplate);
                    Survey.Survey.cssType = "bootstrap";
                    var survey = new Survey.Model(surveyJSON);
                    Survey.SurveyNG.render("surveyElement", { model: survey });
                    survey.onComplete.add(closePreview);
                }

            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });

    }

}
