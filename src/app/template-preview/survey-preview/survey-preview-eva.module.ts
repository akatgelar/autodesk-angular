import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyPreviewEVAComponent } from './survey-preview-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";

export const SurveyPreviewEVARoutes: Routes = [
  {
    path: '',
    component: SurveyPreviewEVAComponent,
    data: {
      breadcrumb: 'Template Preview',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyPreviewEVARoutes),
    SharedModule
  ],
  declarations: [SurveyPreviewEVAComponent],
  providers: [AppService]
})
export class SurveyPreviewEVAModule { }