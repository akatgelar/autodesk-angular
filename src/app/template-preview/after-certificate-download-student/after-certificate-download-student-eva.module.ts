import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AfterCertificateDownloadStudentEVAComponent } from './after-certificate-download-student-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";

export const AfterCertificateDownloadStudentEVARoutes: Routes = [
  {
    path: '',
    component: AfterCertificateDownloadStudentEVAComponent,
    data: {
      breadcrumb: 'Template Preview',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AfterCertificateDownloadStudentEVARoutes),
    SharedModule
  ],
  declarations: [AfterCertificateDownloadStudentEVAComponent],
  providers: [AppService]
})
export class AfterCertificateDownloadStudentEVAModule { }