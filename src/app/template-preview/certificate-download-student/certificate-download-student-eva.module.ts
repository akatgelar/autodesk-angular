import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateDownloadStudentEVAComponent } from './certificate-download-student-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
export const CertificateDownloadStudentEVARoutes: Routes = [
  {
    path: '',
    component: CertificateDownloadStudentEVAComponent,
    data: {
      breadcrumb: 'Template Preview',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificateDownloadStudentEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CertificateDownloadStudentEVAComponent],
  providers: [AppService]
})
export class CertificateDownloadStudentEVAModule { }