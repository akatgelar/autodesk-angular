import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import * as Survey from "survey-angular";
// import * as _ from "lodash";
// import {Pipe, PipeTransform} from "@angular/core";
// import { DatePipe } from '@angular/common';
import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';
declare var html2pdf: any
import { SessionService } from '../../shared/service/session.service';
import { Router } from '@angular/router';
import * as Rollbar from 'rollbar';
const now = new Date();

@Component({
    selector: 'app-certificate-download-student-eva',
    templateUrl: './certificate-download-student-eva.component.html',
    styleUrls: [
    './certificate-download-student-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})



export class CertificateDownloadStudentEVAComponent implements OnInit {
    private template;
    messageResult: string = '';
    messageError: string = '';
    public data: any;
    public Encode_data: any;
    private loading = false;
    DateNow: NgbDateStruct;

    ImageInstitutionLogo: any

    public useraccesdata: any;
    PartnerTypeId : any
    CertificateTypeId:any

    loadScripts() {
        const dynamicScripts = [
        '../../../../assets/html2pdf/jspdf.min.js',
        '../../../../assets/html2pdf/html2canvas.min.js',
        '../../../../assets/html2pdf/html2pdf.min.js',
        '../../../../assets/html2pdf/html2pdf.bundle.min.js',
        ];
        for (let i = 0; i < dynamicScripts.length; i++) {
            const node = document.createElement('script');
            node.src = dynamicScripts[i];
            node.type = 'text/javascript';
            node.async = false;
            node.charset = 'utf-8';
            document.getElementsByTagName('head')[0].appendChild(node);
        }
    }

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, public session: SessionService, public parserFormatter: NgbDateParserFormatter, private rollbar: Rollbar) {
        this.loadScripts()
    }

    ZoomOutText(PartnerType, CertificateId) {
        var userLang = window.navigator.language

        if (userLang == "zh-TW" || userLang == 'zh-CN' || userLang == 'th' || userLang == 'ja' || userLang == 'ko') {
            if (PartnerType == 1) {
                $('#textfooter').css({
                    'width': '1338px',
                    'transform': 'translate(-223px, 553px) scale(0.6)',
                    '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
                })
                $('#textheaderdescription').css({
                    'width': '324px',
                    'transform': 'translate(28px, 286px) scale(0.9)',
                    '-webkit-transform': 'translate(28px, 286px) scale(0.9)'
                })

            }
            else if (PartnerType == 61) {
                $('#textheaderdescription').css({
                    'width': '864px',
                    'transform': 'translate(1px, 286px) scale(0.9)',
                    '-webkit-transform': 'translate(1px, 286px) scale(0.9)'
                })
                $('#textfooter').css({
                    'width': '1338px',
                    'transform': 'translate(-223px, 553px) scale(0.6)',
                    '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
                })
            }
            else if (PartnerType == 58) {
                if (CertificateId == 2) {
                    $('#textfooter').css({
                        'width': '1100px',
                        'transform': 'translate(0px, 555px) scale(0.5)',
                        '-webkit-transform': 'translate(0px, 555px) scale(0.5)'
                    })
                    $('#textheaderdescription').css({
                        'width': '633.552px',
                        'transform': 'translate(216px, 137.111px) scale(0.9)',
                        '-webkit-transform': 'translate(216px, 137.111px) scale(0.9)'
                    })

                }
                else if (CertificateId == 3) {
                    $('#textfooter').css({
                        'width': '1237px',
                        'transform': 'translate(-180px, 538px) scale(0.6)',
                        '-webkit-transform': 'translate(-180px, 538px) scale(0.6)'
                    })
                    $('#textheaderdescription').css({
                        'width': '539px',
                        'transform': 'translate(300px, 131px) scale(0.9)',
                        '-webkit-transform': 'translate(300px, 131px) scale(0.9)'
                    })
                }
                else if (CertificateId == 1) {
                    $('#textfooter').css({
                        'width': '1338px',
                        'transform': 'translate(-223px, 553px) scale(0.6)',
                        '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
                    })

                    $('#textheaderdescription').css({
                        'transform': 'translate(28px, 283px) scale(0.9)',
                        '-webkit-transform': 'translate(28px, 283px) scale(0.9)'
                    })
                }
            }
        }
    }


    ngOnInit() {
        var data = '';
        var templateId = this.route.snapshot.params['id'];
        var courseid = this.route.snapshot.params['courseid'];
        var studentid = this.route.snapshot.params['studentid'];

        window.open("api/Certificate/CertificateDownload/where/" + templateId + "/" + studentid + "/" + courseid);

       

        //this.service.get("api/Certificate/" + templateId, data)
        //.subscribe(result => {
        //   if (result == "Not found") {
        //       this.service.notfound();
        //       this.data = '';
        //   }
        //   else {

        //       var dataResult = result.replace(/\t/g, "\\t");
        //       dataResult = dataResult.replace(/\r/g, "\\r");
        //       dataResult = dataResult.replace(/\n/g, "\\n");

        //       this.data = JSON.parse(dataResult);
        //       this.Encode_data = atob(this.data[0].HTMLDesign)
        //       let fragmentFromString = function (strHTML) {
        //           return document.createRange().createContextualFragment(strHTML);
        //       }

        //       let fragment = fragmentFromString(this.Encode_data);
        //       document.getElementById("forEditaCanvas").appendChild(fragment);

        //       $('#textheader').html(this.data[0].TitleTextHeader)
        //       $('#textheaderdescription').html(this.data[0].TitleHeaderDescription)
        //       $('#textfooter').html(this.data[0].TitleTextFooter)
        //       $('#titlecertno').html(this.data[0].TitleCertificateNo)
        //       $('#titlecertstudent').html(this.data[0].TitleStudentName)
        //       $('#titlecertevent').html(this.data[0].TitleEvent)
        //       $('#titlecerttitle').html(this.data[0].TitleCourseTitle)
        //       $('#titlecertproduct').html(this.data[0].TitleProduct)
        //       $('#titlecertinstructor').html(this.data[0].TitleInstructor)
        //       $('#titlecertdate').html(this.data[0].TitleDate)
        //       $('#titlecertduration').html(this.data[0].TitleDuration)
        //       $('#titlecertpartner').html(this.data[0].TitlePartner)
        //       $('#titlecertinstitution').html(this.data[0].TitleInstitution)
        //       $('#titlecerteventlocation').html(this.data[0].TitleLocation)
        //       this.PartnerTypeId = this.data[0].PartnerTypeId
        //       this.CertificateTypeId = this.data[0].CertificateTypeId
                
        //           //Logo
        //           if (this.ImageInstitutionLogo != '') {
        //               $('#content_logo').attr('src', this.ImageInstitutionLogo)
        //           }

        //           //get user level
        //           let useracces = this.session.getData();
        //           //  console.log("user level access eva raw :" +useracces);
        //           this.useraccesdata = JSON.parse(useracces);


        //           this.ZoomOutText(this.data[0].PartnerTypeId, this.data[0].CertificateTypeId);
        //           this.getUserDataAndSetIntoCanvas(this.useraccesdata.UserLevelId, courseid, studentid);



        //       }
        //   },
        //   error => {
        //       this.service.errorserver();
        //   });

    }


    @ViewChild('forEditaCanvas') forEditaCanvas: ElementRef;
    public downloadPDF(CertNo, DateNow) {
        var element = document.getElementById('forEditaCanvas');
        html2pdf(element, {
            filename: CertNo + '.pdf',

            image: { type: 'jpeg', quality: 1 },
            html2canvas: { scale: 2, logging: true, width: 870, height: 614 },
            jsPDF: { format: 'a4', orientation: 'l' }

        });

        // this.router.navigate(['/dashboard-student']);
    }
    // downloadPDF(NameStudent, DateNow) {
    //     html2canvas(document.getElementById('forEditaCanvas')).then(function (forEditaCanvas) {
    //         var self = this;
    //         var doc = new jspdf('l', 'in', 'a4');

    //         var img = forEditaCanvas.toDataURL("image/png");
    //         doc.addImage(img, 'PNG', 0, 0, 12.2, 8, {
    //             pagesplit: true,
    //             allowTaint: true,
    //             letterRendering: true,
    //           });
    //         doc.save('SampleCertificate.pdf');
    //     });
    // }



    getUserDataAndSetIntoCanvas(NameStudent, courseid, studentid) {
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        var data: any;
        this.service.httpClientGet("api/Certificate/Courses/where/{'CourseId':'" + courseid + "','StudentID':'" + studentid + "'}", data)
        .subscribe(result => {
            if (result == "Not found") {
                this.service.notfound();
                this.rollbar.log("warning", "Certificate data not found! CourseID : " + courseid + " and StudentID : " + studentid);
            }
            else {
                data = result;
                
                var startDate: Date = new Date(data.StartDate);
                var endDate: Date = new Date(data.CompletionDate);
                    // var oneDay = 24 * 60 * 60 * 1000;
                    // var diff = Math.round(Math.round((endDate.getTime() - startDate.getTime()) / (oneDay)));
                    $('#certstudent').html((data.Firstname.toUpperCase()) + " " + (data.Lastname.toUpperCase())); //Student Name

                    $('#certevent').html(data.Name); // Event
                    $('#certeventlocation').html(data.LocationName); //Location Name
                    $('#certstudenttag').html(data.LocationName); //Location Name
                    $('#certinstructor').html(data.ContactName); //Instructor Namecertstudenttag
                    $('#certtitle').html(data.Name); // Course Title
                    $('#certproduct').html(data.productName); // Product Name
                    $('#certdate').html(data.StartDate); //Course Date
                    $('#certduration').html(data.HoursTraining); //Course Duration
                    $('#certpartner').html(data.SiteName)//Site Name
                    $('#certinstitution').html(data.Institution)//Site Name

                    if(data.InstitutionLogo != ""){
                        $('#content_logo').attr('src', data.InstitutionLogo);
                    } else{
                        $('#content_logo').css({
                            'display': 'none'
                        });
                    }

                    var CertNo = data.CertNumber;
                    $('#certno').html(data.CertNumber) // Certificate Number

                    
                    if(data.Name.length > 40){
                        if(this.PartnerTypeId == '1'){
                            $('#certtitle').css({
                                'transform': 'translate(359px, 318px)',
                                'width': '250px',
                                'line-height': '12px'
                            })    
                        }
                        if(this.CertificateTypeId == "1"){
                            $('#certtitle').css({
                                'transform': 'translate(378px, 329px)',
                                'width': '250px',
                                'line-height': '12px'
                            })    
                        }
                        if(this.CertificateTypeId == "2"){
                            $('#certtitle').css({
                                'transform': 'translate(360.667px, 325px)',
                                'width': '250px',
                                'line-height': '12px'
                            })    
                        }
                        
                    }

                    

                    //Get Date Now
                    this.DateNow = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

                    setTimeout(() => {
                        this.downloadPDF(CertNo, this.parserFormatter.format(this.DateNow));
                    }, 3000);
                    

                }
            },
            error => {
                this.service.errorserver();
            });
    }

}
