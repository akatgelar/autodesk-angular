import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import * as Survey from "survey-angular";
// import * as _ from "lodash";
// import {Pipe, PipeTransform} from "@angular/core";

import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';
import $ from 'jquery/dist/jquery';
declare var html2pdf:any
import { SessionService } from '../../shared/service/session.service';

@Component({
  selector: 'app-certificate-preview-eva',
  templateUrl: './certificate-preview-eva.component.html',
  styleUrls: [
  './certificate-preview-eva.component.css',
  '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class CertificatePreviewEVAComponent implements OnInit {

  private template;
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public Encode_data: any;

  public useraccesdata: any;
  public loading = false;

  loadScripts() {
    const dynamicScripts = [
    '../../../../assets/html2pdf/jspdf.min.js',
    '../../../../assets/html2pdf/html2canvas.min.js',
    '../../../../assets/html2pdf/html2pdf.min.js',
    '../../../../assets/html2pdf/html2pdf.bundle.min.js',
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  constructor(private service: AppService, private route: ActivatedRoute, public session: SessionService) {
    this.loadScripts()
  }

  ZoomOutText(PartnerType, CertificateId) {
    var userLang = window.navigator.language
    console.log(userLang)
    if (userLang == "zh-TW" || userLang == 'zh-CN' || userLang == 'th' || userLang == 'ja' || userLang == 'ko') {
      if(PartnerType == 1){
        $('#textfooter').css({
          'width': '1338px',
          'transform': 'translate(-223px, 553px) scale(0.6)',
          '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
        })  
        $('#textheaderdescription').css({
          'width': '324px',
          'transform': 'translate(28px, 286px) scale(0.9)',
          '-webkit-transform': 'translate(28px, 286px) scale(0.9)'
        })
        
      }
      else if(PartnerType == 61){
        $('#textheaderdescription').css({
          'width': '864px',
          'transform': 'translate(1px, 286px) scale(0.9)',
          '-webkit-transform': 'translate(1px, 286px) scale(0.9)'
        })
        $('#textfooter').css({
          'width': '1338px',
          'transform': 'translate(-223px, 553px) scale(0.6)',
          '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
        })  
      }
      else if(PartnerType == 58){
        if(CertificateId == 2){
          $('#textfooter').css({
            'width': '1100px',
            'transform': 'translate(0px, 555px) scale(0.5)',
            '-webkit-transform': 'translate(0px, 555px) scale(0.5)'
          })  
          $('#textheaderdescription').css({
            'width': '633.552px',
            'transform': 'translate(216px, 137.111px) scale(0.9)',
            '-webkit-transform': 'translate(216px, 137.111px) scale(0.9)'
          })

        } 
        else if(CertificateId == 3){
          $('#textfooter').css({
            'width': '1237px',
            'transform': 'translate(-180px, 538px) scale(0.6)',
            '-webkit-transform': 'translate(-180px, 538px) scale(0.6)'
          })  
          $('#textheaderdescription').css({
            'width': '539px',
            'transform': 'translate(300px, 131px) scale(0.9)',
            '-webkit-transform': 'translate(300px, 131px) scale(0.9)'
          })
        }
        else if(CertificateId == 1){
          $('#textfooter').css({
            'width': '1338px',
            'transform': 'translate(-223px, 553px) scale(0.6)',
            '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
          })  

          $('#textheaderdescription').css({
            'transform': 'translate(28px, 283px) scale(0.9)',
            '-webkit-transform': 'translate(28px, 283px) scale(0.9)'
          })
        }
      }
    }
  }

  ngOnInit() {
    this.loading = true;
    var data = '';
    var templateId = this.route.snapshot.params['id'];
    window.open( "api/Certificate/CertificateDownload/where/" + templateId + "/0/0");
    
    //this.service.get("api/Certificate/" + templateId, data)
    //.subscribe(result => {
    //  if (result == "Not found") {
    //    this.service.notfound();
    //    this.data = '';
    //  }
    //  else {

    //    var dataResult = result.replace(/\t/g, " ");
    //    dataResult = dataResult.replace(/\r/g, " ");
    //    dataResult = dataResult.replace(/\n/g, " ");

    //    this.data = JSON.parse(dataResult);
    //    console.log(this.data)
    //    this.Encode_data = atob(this.data[0].HTMLDesign)
    //    let fragmentFromString = function (strHTML) {
    //      return document.createRange().createContextualFragment(strHTML);
    //    }
    //    let fragment = fragmentFromString(this.Encode_data);
    //    document.getElementById("forEditaCanvas").appendChild(fragment);
    //    $('#textheader').html(this.data[0].TitleTextHeader)
    //    $('#textheaderdescription').html(this.data[0].TitleHeaderDescription)
    //    $('#textfooter').html(this.data[0].TitleTextFooter)
    //    $('#titlecertno').html(this.data[0].TitleCertificateNo)
    //    $('#titlecertstudent').html(this.data[0].TitleStudentName)
    //    $('#titlecertevent').html(this.data[0].TitleEvent)
    //    $('#titlecerttitle').html(this.data[0].TitleCourseTitle)
    //    $('#titlecertproduct').html(this.data[0].TitleProduct)
    //    $('#titlecertinstructor').html(this.data[0].TitleInstructor)
    //    $('#titlecertdate').html(this.data[0].TitleDate)
    //    $('#titlecertduration').html(this.data[0].TitleDuration)
    //    $('#titlecertpartner').html(this.data[0].TitlePartner)
    //    $('#titlecertinstitution').html(this.data[0].TitleInstitution)
    //    $('#titlecerteventlocation').html(this.data[0].TitleLocation)

    //    this.ZoomOutText(this.data[0].PartnerTypeId, this.data[0].CertificateTypeId);
    //    this.getUserDataAndSetIntoCanvas();
    //    setTimeout(() => {
    //         this.downloadPDF();
    //    }, 3000);
       

        
    //  }
    //},
    //error => {
    //  this.service.errorserver();
    //});

  }

  @ViewChild('forEditaCanvas') forEditaCanvas: ElementRef;
  public downloadPDF() {
    var element = document.getElementById('forEditaCanvas');
    html2pdf(element, {
      filename: 'SampleCertificate.pdf',
      image: {type: 'jpeg', quality: 1},
      html2canvas: {scale: 2, logging: true, width:870, height:614},
      jsPDF: {format: 'a4', orientation: 'l'}
    });
    this.loading = false;
  }
  // public downloadPDF() {
  //   html2canvas(document.getElementById('forEditaCanvas')).then(function (forEditaCanvas) {
  //     var self = this;
  //     var doc = new jspdf('l', 'in', 'a4');

  //     var img = forEditaCanvas.toDataURL("image/png");
  //     doc.addImage(img, 'PNG', 0, 0, 12.2, 8, {
  //       pagesplit: true,
  //       allowTaint: true,
  //       letterRendering: true,
  //     });
  //     doc.save('SampleCertificate.pdf');
  //   });
  // }

  getUserDataAndSetIntoCanvas() {
        //get user level
        let useracces = this.session.getData();
        //  console.log("user level access eva raw :" +useracces);
        this.useraccesdata = JSON.parse(useracces);
        $('#certstudent').html(this.useraccesdata.UserLevelId)

      }

    }
