import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificatePreviewEVAComponent } from './certificate-preview-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
export const CertificatePreviewEVARoutes: Routes = [
  {
    path: '',
    component: CertificatePreviewEVAComponent,
    data: {
      breadcrumb: 'Template Preview',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificatePreviewEVARoutes),
    SharedModule,
     LoadingModule
  ],
  declarations: [CertificatePreviewEVAComponent],
  providers: [AppService]
})
export class CertificatePreviewEVAModule { }