import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { SessionService } from './shared/service/session.service';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, Response } from "@angular/http";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { AppService } from "./shared/service/app.service";
import { NgxPermissionsService } from 'ngx-permissions';
import * as Rollbar from 'rollbar';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
	private date: Date = new Date();
	private lastFY;
	private currentUserLevel;
	idleState = 'Not started.';
	timedOut = false;
	lastPing?: Date = null;
	private idelcounter = 0;
	  modalReference: NgbModalRef;
	@ViewChild('content') private content;

	

	
	constructor(public session: SessionService,private modalService: NgbModal, private router: Router, private _http: Http, private http: HttpClient, private datePipe: DatePipe, private service: AppService, private permissionsService: NgxPermissionsService, private rollbar: Rollbar, private idle: Idle, private keepalive: Keepalive) {
		
		
		
		
		// this.session.logOut();
		// this.session.logIn("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ew0KICAiVXNlcklkIjogIjEiLA0KICAiVXNlckxldmVsSWQiOiAiQURNIiwNCiAgIkVtYWlsIjogImFkbWluQGdtYWlsLmNvbSIsDQogICJQcm9maWxlUGljdHVyZSI6ICJVc2Vyc18yMDE3LjA5LjI1LTE2LjQzLjU3LS5wbmciLA0KICAiQWNjZXNzIjogew0KICAgICJVc2VyTGV2ZWxJZCI6ICJBRE0iLA0KICAgICJOYW1lIjogIlN1cGVyIEFkbWluIiwNCiAgICAiZWltIjogIjExMTExIiwNCiAgICAiZXBkYiI6ICIxMTExMSIsDQogICAgImV2YSI6ICIwMDAwMCINCiAgfSwNCiAgIkRhdGVDcmVhdGVkIjogIjIwMTgtMDItMDggMTE6NDg6NDQiDQp9.hBHiEpiljmOy2c2vUTOuhvmYmv0lRM8qoTA79v_pX8M","data");

		// router.events.forEach((event: NavigationEvent) => {
		// 	if (event instanceof NavigationEnd) {
		// 		var url = event.url;
		// 		var routing = url.split("/");

		// 		if (this.session.isLoggedIn()) {
		// 			console.log("session true");
		// 			var result = "";
		// 			let url = "api/Auth/CheckToken";
		// 			let data = "";
		// 			let token = this.session.getToken();
		// 			let headers = new HttpHeaders().set('Content-Type', 'application/json');
		// 			headers = headers.set('authorization', token);

		// 			//chech valid token
		// 			this.http.get(url, { headers }).subscribe(result => {
		// 				if (result['code'] == '1') {
		// 					console.log("token valid");
		// 					if ((routing[1] == "authentication") && (routing[3] == "epdb")) {
		// 						this.router.navigate(["/epdb/dashboard"]);
		// 					}
		// 				}
		// 				else {
		// 					console.log("token invalid");
		// 					this.router.navigate(["/authentication/login/epdb"]);
		// 				}
		// 			},
		// 				error => {
		// 					console.log("cant connect to db");
		// 					this.router.navigate(["/authentication/login/epdb"]);
		// 				});
		// 		}
		// 		else {
		// 			console.log("session false");
		// 			this.router.navigate(["/authentication/login/epdb"]);
		// 			//  console.log("test routing not login:"+routing[1]+""+routing[3]);
		// 			// if ((routing[1] == "studentregister") || (routing[3] == "studentregister"))
		// 			// {
		// 			// 	this.router.navigate(["/student/register"]);
		// 			// }
		// 			// else
		// 			// {
		// 			// 	this.router.navigate(["/authentication/login/epdb"]);
		// 			// }
		// 		}
		// 	}
		// });


		// console.log(this.session.isLoggedIn());
   		// console.log(this.session.getData());
		// console.log(this.session.getToken()); 
		

		const perm = ["EDITOR"];
		this.permissionsService.loadPermissions(perm);
		
		   
	    router.events.forEach((event: NavigationEvent) => 
	    { 
			if (event instanceof NavigationEnd) 
			{  
				var url = event.url; 
				var routing = url.split("/");  
 
 				//session true
	   			if(this.session.isLoggedIn())
	   			{	
	   				var result = "";
	   				let url = "api/Auth/CheckToken";
	   				let data = "";
	   				let token = this.session.getToken();
	   				let headers = new HttpHeaders().set('Content-Type', 'application/json');
			        headers = headers.set('authorization', token);
			        var userdata = JSON.parse(this.session.getData());
					var access = userdata.Access;
				
					//Konfigurasi Rollbar Person
			        rollbar.configure({
			            payload:{
			                person:{
			                    id: userdata.UserId,
			                    username: userdata.ContactName,
			                    email: userdata.Email
			                }
			            }
			        });

			        // rollbar.configure({checkIgnore: function(isUncaught,args,payload){
			        // 	return isUncaught === true;
			        // }});

			        rollbar.info(userdata.ContactName + " | " + userdata.Email + " logged in as " + userdata.UserLevelId);

			        //chech valid token
			        // this.http.get(url,{headers}).subscribe(result => {
			            // console.log("result action -> "+ result);
			            //token valid
			            // if(result['code'] == '1') 
			            // {
			            	// console.log(routing[1]);
			            	// console.log(access.UserLevelId );
			            	if(((routing[1] == "login")||(routing[1] == ""))&&(access.UserLevelId == "SUPERADMIN")){ 
				                this.router.navigate(["/dashboard-admin"]); 
							}
			            	else if(((routing[1] == "login")||(routing[1] == ""))&&(access.UserLevelId == "ADMIN")){ 
				                this.router.navigate(["/dashboard-admin"]); 
							}
			            	else if(((routing[1] == "login")||(routing[1] == ""))&&(access.UserLevelId == "ORGANIZATION")){ 
				                this.router.navigate(["/dashboard-organization"]); 
							}
			            	else if(((routing[1] == "login")||(routing[1] == ""))&&(access.UserLevelId == "DISTRIBUTOR")){ 
				                this.router.navigate(["/dashboard-distributor"]); 
							}
			            	else if(((routing[1] == "login")||(routing[1] == ""))&&(access.UserLevelId == "TRAINER")){ 
				                this.router.navigate(["/dashboard-trainer"]); 
							}
			            	else if(((routing[1] == "login")||(routing[1] == ""))&&(access.UserLevelId == "STUDENT")){ 
				                this.router.navigate(["/dashboard-student"]); 								
							}
							else{

							
							    
								// console.log(JSON.parse(this.session.getData()));  
								// console.log(JSON.parse(this.session.getData()).Access);
								// console.log("========");
								var root = "";
								for (const key of Object.keys(access)) {
									if((key != "UserLevelId") && (key != "Name")){ 

										if(routing[3]){
											root = routing[1]+"/"+routing[2]+"/"+routing[3]; 
											if(root == key){
												// console.log("sama 3"); 
												var split1 = access[key];
												var split2 = split1.split(''); 
												if(split2[0] == "1"){
													// console.log("boleh");
												}else{
													// console.log("ditolak");
													this.router.navigate(["/forbidden"]); 
												}
											}
										}
										else if(routing[2]){
											root = routing[1]+"/"+routing[2];
											if(root == key){
												// console.log("sama 2"); 
												var split1 = access[key];
												var split2 = split1.split(''); 
												if(split2[0] == "1"){
													// console.log("boleh");
												}else{
													// console.log("ditolak");
													this.router.navigate(["/forbidden"]); 
												}
											}
										}
										else if(routing[1]){
											root = routing[1];
											if(root == key){
												// console.log("sama 1"); 
												var split1 = access[key];
												var split2 = split1.split(''); 
												if(split2[0] == "1"){
													// console.log("boleh");
												}else{
													// console.log("ditolak");
													this.router.navigate(["/forbidden"]); 
												}
											}
										}

									}
									
								}
							}


			            // } 
			            //token invalid
			        //     else
			        //     {
			        //         swal(
			        //           'Information!',
			        //           result['msg'],
			        //           'error'
			        //         );
			        //         this.router.navigate(["/authentication/login/epdb"]);  
			        //     }
			        // },
			        // //cant connect to db
			        // error => { 
			        // 	swal(
		            //       'Information!',
		            //       'Can\'t connect to server.',
		            //       'error'
		            //     );
		            //     this.router.navigate(["/authentication/login/epdb"]); 
			        // });
 
	   			}
	   			else
	   			{
	   				// console.log(routing[1]);
	   				if(routing[1] == "register")
	   				{}
	   				else if (routing[1] == "forbidden")
	   				{}
	   				else if((routing[1] == "error")&&(routing[2] == "404"))
	   				{}
	   				else if(routing[1] == "login-student")
	   				{} 
	   				else if(routing[1] == "forget-password")
	   				{}
	   				else if(routing[1] == "reset-password")
	   				{} 
	   				else if(routing[1] == "forget-password-student")
	   				{}
	   				else if(routing[1] == "reset-password-student")
					{}
					else if(routing[1].substr(0,12) == "set-password")
					{}
					else if(routing[1].substr(0,6) == "logout")
					{}
					else if(routing[1].substr(0,10) == "activation")
					{}
					else if(routing[1].substr(0,14) == "warningMessage")
					{}
					else if(routing[1].substr(0,12) == "usertraining")
					{}   
					else if(routing[1] == "help-me")
					{}   
					else if(routing[1] == "no-role-page")
					{}
				    else if(routing[1] == "inactive-user")
	   				{}   
	   				else
		   			{ 
						
		   				this.router.navigate(["/login"]);
		   			}
	   			}
 
			}

		  });

		// this.getLastFY();
		// this.sendRequestTerminated(); //tutup sementara, error wae
	
			
	
		this.router.events.subscribe((ev) => {
          if (ev instanceof NavigationEnd) {
		  /* The code goes here on every router change */
		  var cuserdata = JSON.parse(this.session.getData());
			if(cuserdata != undefined && cuserdata != null){
				var caccess = cuserdata.Access;
				this.currentUserLevel = caccess.UserLevelId;
			}
		  if(this.currentUserLevel == "STUDENT"){
			this.start();
		  }
		  }
      } );
		
	}

	ngOnInit(){

	}
	start(){

		// sets an idle timeout of 900 seconds, for testing purposes.
			this.idle.setIdle(600);
			// sets a timeout period of 30 seconds. after 10 seconds of inactivity, the user will be considered timed out.
			this.idle.setTimeout(30);
			// sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
			this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
		
			this.idle.onIdleEnd.subscribe(() => this.reset());
			this.idle.onTimeout.subscribe(() => {
				this.currentUserLevel="";
				this.modalReference.close();
			  this.idleState = 'Timed out!';
			  this.timedOut = true;
			  this.session.logOut();
			  this.router.navigate(["/login-student"]);
			});
			this.idle.onIdleStart.subscribe(() => 
			{
				this.modalReference = this.modalService.open(this.content);
			});
			this.idle.onTimeoutWarning.subscribe((countdown) => 
			{
				this.idelcounter = countdown;
			});
		
			// sets the ping interval to 15 seconds
			this.keepalive.interval(5);
		
			this.keepalive.onPing.subscribe(() => this.lastPing = new Date());
		
			this.reset();
	}
	reset() {
		this.idle.watch();
		this.idleState = 'Started.';
		this.timedOut = false;
		
	  }

	sendRequestTerminated(){
		this.service.httpClientGet("api/Auth/EmailRequestSecondApprover", '')
		.subscribe(result => {
			console.log(result['message'])
		}, error => {
			this.service.errorserver();
		});
	}

	// getLastFY() {
	// 	var today = this.datePipe.transform(this.date, 'dd-MM-yyyy');
	// 	var dateTemp = today.split('-');
	// 	var year = parseInt(dateTemp[2]);
	// 	var data: any;
	// 	this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator'}", data)
	// 		.subscribe(result => {
	// 			data = result;
	// 			if (data.length > 0) {
	// 				this.lastFY = data[data.length - 1].Key;
	// 				var endDate: Date = new Date(this.lastFY + '-02-28');
	// 				if (this.date.getTime() > endDate.getTime()) {
	// 					this.checkAvailable(year, dateTemp);
	// 				}
	// 			}
	// 		}, error => {
	// 			this.service.errorserver();
	// 		});
	// }

	checkAvailable(year, args) {
		var data: any;
		this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Key':'" + (year + 1) + "','Status':'A'}", data)
			.subscribe(res => {
				data = res;
				if (data.length == 0) {
					var data_post = {
						'Key': year + 1,
						'Parent': 'FYIndicator',
						'KeyValue': 'FY' + (parseInt(args[2].slice(2, 4)) + 1) + ' (' + year + ')',
						'Status': 'A'
					};
					this.service.post("api/Dictionaries", data_post)
						.subscribe(result => {
							var posted = JSON.parse(result);
							if (posted['code'] != 1) this.checkAvailable(year, args);
							// console.log(posted['message']);
						}, error => {
							this.service.errorserver();
						});
				}
			}, error => {
				this.service.errorserver();
			});
	}
	

	
}
