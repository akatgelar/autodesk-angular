import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        }
    }
}

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe){}
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0,10)), 'dd-MMMM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => (row.CourseId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.StartDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteAddress1.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterStatus' })
export class DataFilterStatusPipe {
    transform(array: any[], query: string): any {
        if (query == "Active") {
            return _.filter(array, row => 
                (row.SurveyTaken.indexOf("0") > -1) &&
                (row.activesurveylink.indexOf("Active") > -1));
        }
        else if(query == "Expired" || query == "Pending"){
            return _.filter(array, row => 
                (row.activesurveylink.indexOf(query) > -1));
        }else if(query == "Completed"){
            return _.filter(array, row => 
                (row.SurveyTaken.indexOf("1") > -1));
        }
        return array;
    }
}


@Component({
    selector: 'app-all-course-student',
    templateUrl: './all-course-student.component.html',
    styleUrls: [
        './all-course-student.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AllCourseStudentComponent implements OnInit {

    private _serviceUrl = 'api/StudentsCourses';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public activelinksurvey = [];
    public useraccesdata: any;
    public dataStudent: any;
    public filterStatus : string ="All";

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
        this.getcourse("admin");
    }

    getcourse(studentid) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/allcourse/' + studentid, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.data = '';
                }
                else {
                    this.data = result;
                   //allow special character
                    if (this.data != null) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].Name != null && this.data[i].Name != '') {
                                this.data[i].Name = this.service.decoder(this.data[i].Name);
                            }
                        }
                    }
                }
            },
            error => {
                this.service.errorserver();
                this.data = '';
            });
    }

}
