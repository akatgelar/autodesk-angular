import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllCourseStudentComponent } from './all-course-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { DataFilterActivitiesPipe,DataFilterStatusPipe,ConvertDatePipe,ReplacePipe } from './all-course-student.component';
import { AppFormatDate } from "../../shared/format-date/app.format-date";

export const AllCourseStudentRoutes: Routes = [
  {
    path: '',
    component: AllCourseStudentComponent,
    data: {
      breadcrumb: 'menu_course.all_course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'All Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AllCourseStudentRoutes),
    SharedModule
  ],
  declarations: [AllCourseStudentComponent, DataFilterActivitiesPipe, DataFilterStatusPipe, ConvertDatePipe, ReplacePipe],
  providers: [AppService, AppFormatDate]
})
export class AllCourseStudentModule { }
