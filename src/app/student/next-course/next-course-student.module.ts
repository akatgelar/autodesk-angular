import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NextCourseStudentComponent, ConvertDatePipe } from './next-course-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { DataFilterNextCoursePipe } from './next-course-student.component';
import { LoadingModule } from 'ngx-loading';

export const NextCourseStudentRoutes: Routes = [
  {
    path: '',
    component: NextCourseStudentComponent,
    data: {
      breadcrumb: 'dashboard.student.next_course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Next Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(NextCourseStudentRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [NextCourseStudentComponent, ConvertDatePipe, DataFilterNextCoursePipe],
  providers: [AppService, AppFormatDate]
})
export class NextCourseStudentModule { }
