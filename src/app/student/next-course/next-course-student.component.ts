import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import * as _ from "lodash";

// Filter search
@Pipe({ name: 'dataFilterNextCourse' })
export class DataFilterNextCoursePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.CourseId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1)
                // (row.Due.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
    }
}

@Component({
    selector: 'app-next-course-student',
    templateUrl: './next-course-student.component.html',
    styleUrls: [
        './next-course-student.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class NextCourseStudentComponent implements OnInit {

    private _serviceUrl = 'api/StudentsCourses';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "";
    public sortOrder: string = "asc";
    public activelinksurvey = [];
    public useraccesdata: any;
    public dataStudent: any;
    private loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
        // var data = '';
        // this.service.httpClientGet('api/Student/' + this.useraccesdata.Email, data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             this.dataStudent = '';
        //         }
        //         else {
        //             this.dataStudent = result;
        //             this.getcourse(this.dataStudent.StudentID);
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //             this.dataStudent = '';
        //         });

        /* get studentid */
        this.getcourse(this.useraccesdata.UserId);
        /* get studentid */
    }

    getcourse(studentid) {
        this.loading = true;
        var data :any;
        this.service.httpClientGet(this._serviceUrl + '/nextcourse/' + studentid, data)
            .subscribe(result => {
                if (result == 'Not found' || result == null) {
                   // this.service.notfound();
                    this.data = '';
                    this.loading = false;
                }
                else {
                    this.data = result;
                    //allow special character
                    if (this.data != null) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].Name != null && this.data[i].Name != '') {
                                this.data[i].Name = this.service.decoder(this.data[i].Name);
                            }
                        }
                      }
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
    }

}
