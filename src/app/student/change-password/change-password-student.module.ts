import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordStudentComponent } from './change-password-student.component';
import { RouterModule, Routes} from "@angular/router";
import { SharedModule} from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";


export const ChangePasswordStudentRoutes: Routes = [
  {
    path: '',
    component: ChangePasswordStudentComponent,
    data: {
      breadcrumb: 'eim.profile.change_password.change_password',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ChangePasswordStudentRoutes),
    SharedModule
  ],
  declarations: [ChangePasswordStudentComponent],
  providers:[AppService]
})
export class ChangePasswordStudentModule { }
