import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnswerEvaluationFormStudentComponent } from './answer-evaluation-form-student.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { CalenderComponent } from './calender/calender.component';

export const AnswerEvaluationFormStudentRoutes: Routes = [
  {
    path: '',
    component: AnswerEvaluationFormStudentComponent,
    data: {
      breadcrumb: 'Answer Evaluation Form',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Answer Evaluation Form'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AnswerEvaluationFormStudentRoutes),
    SharedModule
  ],
  declarations: [AnswerEvaluationFormStudentComponent, CalenderComponent]
})
export class AnswerEvalutionFormStudentModule { }
