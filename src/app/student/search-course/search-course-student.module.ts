import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchCourseStudentComponent } from './search-course-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { DataFilterActivitiesPipe, ConvertDatePipe } from './search-course-student.component';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const SearchCourseStudentRoutes: Routes = [
  {
    path: '',
    component: SearchCourseStudentComponent,
    data: {
      breadcrumb: 'menu_course.search_course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Search Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchCourseStudentRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [SearchCourseStudentComponent, DataFilterActivitiesPipe, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class SearchCourseStudentModule { }
