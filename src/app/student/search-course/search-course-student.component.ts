import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { Location } from '@angular/common';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { AcademicProjectsReportEPDBComponent } from '../../epdb/academic-projects-report/academic-projects-report-epdb.component.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.CourseId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.NAME.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.StartDate.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.SiteAddress1.toLowerCase().indexOf(query.trim().toLowerCase()) > -1)
            );
        }
        return array;
    }
}



@Component({
    selector: 'app-search-course-student',
    templateUrl: './search-course-student.component.html',
    styleUrls: [
        './search-course-student.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SearchCourseStudentComponent implements OnInit {

    private _serviceUrl = 'api/Courses';
    public studentcourse: any;
    public data: any = null;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "NAME";
    public sortOrder: string = "asc";
    public activelinksurvey = [];
    public useraccesdata: any;
    public dataStudent: any;
    searchCourse: FormGroup;
    public loading = false;


    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private route: ActivatedRoute, private router: Router, private location: Location) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let CourseId = new FormControl('');

        this.searchCourse = new FormGroup({
            CourseId: CourseId
        });
    }

    language:any;
    ngOnInit() {

        var lang = localStorage.getItem("language");
        this.service.httpClientGet('api/Language/' + lang, '')
        .subscribe(result => {
            this.language = result;
        });

        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.searchCourse.patchValue({ CourseId: item.CourseId });
            this.onSubmit();
        }

    }

    TeachingLevel: string = "";
    onSubmit() {

        var params =
            {
                CourseId: this.searchCourse.value.CourseId
            }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        // this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl + "/DetailCourseForEnroll/" + this.searchCourse.value.CourseId, data)
            .subscribe(result => {console.log(result)
                if (result == "Not found" || result == "{}" || result == null) {
                   // this.service.notfound();
                    this.data = '';
                    this.loading = false;
                }
                else {
                    this.data = result;
                    //allow special character
                    if (this.data != null) {
                        if (this.data.Name != null && this.data.Name != '') {
                            this.data.Name = this.service.decoder(this.data.Name);
                        }
                    }
                    var tl = [this.data.Update, this.data.Essentials, this.data.Intermediate, this.data.Advanced, this.data.Customized, this.data.ctl_Other].filter(v => v != '');
                    this.TeachingLevel = tl.join("<br/>");
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
    }

    enroll(value, coursename) {
     
        console.log("called");
        console.log(this.language["general"]);
        console.log(this.language["student"]);
        /* issue 14112018 - label student portal translate */
        //var popupMessage='';
      
        var popupMessage = this.language["student"]["search_course"]["popupMesage"];
        popupMessage = popupMessage.replace("{CourseId}",value);  
        popupMessage = popupMessage.replace("{CourseTitle}",coursename);  
        
        console.log("called2");
        /* end line issue 14112018 - label student portal translate */
        console.log(this.language["general"]);
        swal({
            //title: 'confirmation', 
            title: this.language["general"]["confirmation"], /* issue 14112018 - label student portal translate */
            html: popupMessage, /* issue 14112018 - label student portal translate */
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes', /* issue 14112018 - label student portal translate */
            cancelButtonText: 'Cancel'
           // confirmButtonText: this.language["general"]["yes_sure"], /* issue 14112018 - label student portal translate */
           // cancelButtonText: this.language["general"]["btn_cancel"] /* issue 14112018 - label student portal translate */
        }).then(result => {
            if (result == true) {
                this.loading = true;
                var studentCourse =
                    {
                        'CourseId': value,
                        'StudentID': this.useraccesdata.UserId,
                        'Status': 'A',
                        'CourseTaken': 0,
                        'SurveyTaken': 0,
                        'StudentName': this.useraccesdata.StudentName,
                        'Email': this.useraccesdata.Email,
                        'cuid': this.useraccesdata.StudentName
                    };
console.log("called3");
                this.service.httpClientGet("api/StudentsCourses/where/{'CourseId':'" + value + "','StudentID':'" + this.useraccesdata.UserId + "'}", "")
                    .subscribe(res => {
                        if (res[0] == undefined || res[0]["CourseStudentId"] == null || res[0]["CourseStudentId"] == '' || res[0]["CourseStudentId"] == undefined) {
                            this.service.httpClientPost("api/Courses/CourseStudent", studentCourse)
                                .subscribe(result => {
                                    if (result["code"] == "1") {
                                        this.router.navigate(['/student/my-course']);
                                        swal(
                                            'Information',
                                            'Successfully Enrolled',
                                            'success'
                                        )
                                    } else {
                                        this.loading = false;
                                    }
                                }, error => { this.service.errorserver(); this.loading = false; });
                        } else {
                            swal(
                                'Information!',
                                'You already on this course',
                                'error'
                            );
                            this.loading = false;
                        }
                    }, error => { this.service.errorserver(); this.loading = false; });
            }
        })
    }
}