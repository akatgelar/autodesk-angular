import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import * as Survey from "survey-angular";
import { Injectable } from '@angular/core';
import * as Rollbar from 'rollbar';

var surveyJSON = {};
var datadetail: any;

@Component({
  selector: 'survey-answer-student',
  templateUrl: './survey-answer-student.component.html',
  styleUrls: [
    './survey-answer-student.component.css',
    '../../../../node_modules/c3/c3.min.css',
    '../../../../node_modules/survey-angular/survey.css',
    //'../../../../src/assets/css/bootstrap.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyAnswerStudentComponent implements OnInit {
  private _serviceUrl = 'api/EvaluationQuestion';
  private _serviceUrlCourses = 'api/Courses'
  messageResult: string = '';
  messageError: string = '';
  totalQuestions = 0;
  progress = '0%';
  answeredQuestions = {};
  answeredQuestionsNumber = 0;
  public data: any;
  public datadetail: any;
  public json: any;
  id: string;
  public studentcourse: any;
  sended: boolean = false;
  public dataStudent: any;
  private FyId;
  private TrainingId;
  private ProjectId;
  private EventId;
  private useraccessdata;
  private studentName;
  public loading = false;
  private token:string ='';

  private StatusTrainingFacility: any


  constructor(private service: AppService, private route: ActivatedRoute,
    private router: Router, private formatdate: AppFormatDate, private session: SessionService, private rollbar: Rollbar) {
    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
  }

  ngOnInit() {
    var sub: any;
    var args: any;
    sub = this.route.queryParams.subscribe(params => {
      args = JSON.parse(params['args']);
    });
    this.token = this.session.getToken();
    console.log(this.token);
    var data :any = JSON.stringify({
      'CourseId': args.CourseId,
      'StudentID': args.StudentID
  });

  this.service.httpClientPost("api/Courses/EncrollCheck", data).subscribe(result => {
      if (result["code"] == '0') {
        swal(
          'Information',
          'You haven not enrolled this course, Please check your enroll list!',
          'success'
      )
        this.router.navigate(['/student/my-course']);
       }
      else{
        this.setSurvey(args);
      }
  });
   
  }

  setCSSIntoTabel() {
    var ukuran = $(window).width()
    if (ukuran <= 600) {
      $('.main-body .page-wrapper').css('padding', '0.3rem')
      $('.pcoded-inner-content').css('padding', '-9px')
      $('.card-block').css('padding', '0.8rem')
      $('.card h5').css({
        'font-size': '14px',
        'color': 'black',
        'font-weight': '300',
        'line-height': '1.6'
      })
      $('table td, .table th').css({
        'height': '40px'
      })
    }
  }


  setSurvey(args) {
    var data: any;
    var dataCourses: any

    //Get Data Courses
    this.service.httpClientGet(this._serviceUrlCourses + '/Detail/' + args.CourseId, dataCourses)
      .subscribe(result => {
        //Set Online Training Facility
        dataCourses = result;
        this.StatusTrainingFacility = dataCourses.Online
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });

    let FYear: any;
    FYear = args.FyId;
    //Cek ada atau engga
    /*this.service.get(this._serviceUrl + "/where/{'CertificateTypeId':'" + args.CertificateType + "','Year':'" + FYear + "','LanguageID':'" + args.LanguageID + "'}", data)
      .subscribe(result => {
        data = result.replace(/\r\n/g, " ");
        data = data.replace(/\n/g, " ");
        data = JSON.parse(data);
        
        if (data.length != 0) {
          FYear = args.FyId;
        } else {
          FYear = parseInt(args.FyId) - 1;
        }
*/
        //get data according to id
        this.service.httpClientGet(this._serviceUrl + "/where/{'CertificateTypeId':'" + args.CertificateType + "','Year':'" + FYear + "','LanguageID':'" + args.LanguageID + "'}", data)
          .subscribe(result => {
           data = result;

            let total = 0
            data.map((el)=>{
              return total = total + el.EvaluationQuestionJson.pages.length
            })
            this.totalQuestions = total
            if (data.length != 0) {
              data[0].EvaluationQuestionTemplate.completedHtml = "<p><h4>Thank you for sharing this information with us.</h4></p>";
              if (this.StatusTrainingFacility == '0') {
                let tempString = JSON.stringify(data[0].EvaluationQuestionTemplate);
                if (tempString.includes("FR_3") == true) {
                  for (var i = 0; i < data[0].EvaluationQuestionTemplate.pages[0].elements[0].elements[0].elements[4].rows.length; i++) {
                    if (data[0].EvaluationQuestionTemplate.pages[0].elements[0].elements[0].elements[4].rows[i]['value'] === 'FR_3') {
                      data[0].EvaluationQuestionTemplate.pages[0].elements[0].elements[0].elements[4].rows.splice(i, 1);
                    }
                  }
                  surveyJSON = tempString;
                } else {
                  surveyJSON = JSON.stringify(data[0].EvaluationQuestionTemplate);
                }
              }
              else {
                data[0].EvaluationQuestionTemplate.completedHtml = "<p><h4>Thank you for sharing this information with us.</h4></p>";
                if (data[0].PartnerType != '1' && (data[0].PartnerType != '58' && data[0].CertificateTypeId != '1')) {
                  let tempString = JSON.stringify(data[0].EvaluationQuestionTemplate);
                  if (tempString.includes("FR_3") == true) {
                    for (var i = 0; i < data[0].EvaluationQuestionTemplate.pages[0].elements[0].elements[0].elements[4].rows.length; i++) {
                      if (data[0].EvaluationQuestionTemplate.pages[0].elements[0].elements[0].elements[4].rows[i]['value'] === 'FR_3') {
                        data[0].EvaluationQuestionTemplate.pages[0].elements[0].elements[0].elements[4].rows.splice(i, 1);
                      }
                    }
                    surveyJSON = tempString;
                  } else {
                    surveyJSON = JSON.stringify(data[0].EvaluationQuestionTemplate);
                  }
                } else {
                  surveyJSON = JSON.stringify(data[0].EvaluationQuestionTemplate);
                }
              }
              Survey.Survey.cssType = "bootstrap";
              var survey = new Survey.Model(surveyJSON);
              survey.onComplete.add(this.sendDataToServer);

              var myCSSHilman = {
                matrix: {
                  root: "table table-striped table-responsive"
                }
              };
              Survey.SurveyNG.render("surveyElement", { model: survey, css: myCSSHilman });
              let that = this
              document.getElementById("surveyElement").addEventListener('input', function (evt) {
                let qs = document.getElementsByClassName('sv_qstn')
                for(let i = 0; i<qs.length; i++){
                  let input = event.target
                  let q = $(input).parents('.sv_qstn').first()
                  if(q[0]){
                    if(input['value'] == "")
                      delete that.answeredQuestions[q[0].id]
                    else
                      that.answeredQuestions[q[0].id] = true
                    that.progress = Math.floor(Object.keys(that.answeredQuestions).length*100/that.totalQuestions)+'%'
                    that.answeredQuestionsNumber = Object.keys(that.answeredQuestions).length
                  }
                }
              });
              this.setCSSIntoTabel()
            }
         /* },
            error => {
              this.messageError = <any>error
              this.service.errorserver();
            });*/

      });
  }

  //Fungsi yang dipanggil dari admin-layout-component di folder layout > admin-new
  chageSurveyLanguage(keyLang) {

    function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var temp = JSON.parse(getParameterByName("args", ""));
    var argsnew =
    {
      "CertificateType": temp.CertificateType,
      "CourseId": temp.CourseId,
      "FyId": temp.FyId,
      "StudentID": temp.StudentID,
      "LanguageID": keyLang
    };
    
    this.setSurvey(argsnew);
  }

  sendDataToServer(survey, options) {
    this.loading = true;
    var token= localStorage.getItem('autodesk-token');;
    let evaAnswerUrl = 'api/EvaluationAnswer';

    var NbAnswerCCM: number = 0;
    var ScoreCCM: number = 0;
    var NbAnswerCOE: number = 0;
    var ScoreCOE: number = 0;
    var NbAnswerWDT: number = 0;
    var ScoreWDT: number = 0;
    var NbAnswerFR: number = 0;
    var ScoreFR: number = 0;
    var NbAnswerIQ: number = 0;
    var ScoreIQ: number = 0;
    var NbAnswerOE: number = 0;
    var ScoreOE: number = 0;
    var NbAnswerOP: number = 0;
    var ScoreOP: number = 0;

    function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    for (let i = 0; i < Object.keys(survey.data).length; i++) {
      var question = Object.keys(survey.data)[i];
      for (let j = 0; j < Object.keys(survey.data[question]).length; j++) {
        var answerType = Object.keys(survey.data[question])[j];
        var check1 = answerType.substr(0, 2); //FR || IQ || OP || OE
        var check2 = answerType.substr(0, 3); //CCM || COE || WDT
        var answerValue = parseInt(survey.data[question][answerType]);

        if (answerValue != 0) {
          switch (check1) {
            case 'FR':
              NbAnswerFR = NbAnswerFR + 1;
              ScoreFR += answerValue;
              break;
            case 'IQ':
              NbAnswerIQ += 1;
              ScoreIQ += answerValue;
              break;
            case 'OE':
              NbAnswerOE += 1;
              ScoreOE += answerValue;
              break;
            case 'OP':
              NbAnswerOP += 1;
              ScoreOP += answerValue;
              break;
          }

          switch (check2) {
            case 'CCM':
              NbAnswerCCM += 1;
              ScoreCCM += answerValue;
              break;
            case 'COE':
              NbAnswerCOE += 1;
              ScoreCOE += answerValue;
              break;
            case 'WDT':
              NbAnswerWDT += 1;
              ScoreWDT += answerValue;
              break;
          }
        }
      }
    }

    var params = JSON.parse(getParameterByName('args', window.location));

    var quarter: number;
    var submittedDate = new Date().getMonth() + 1;

    if (submittedDate == 1 || (submittedDate >= 11 && submittedDate <= 12)) {
      quarter = 4;
    } else if (submittedDate >= 2 && submittedDate <= 4) {
      quarter = 1;
    } else if (submittedDate >= 5 && submittedDate <= 7) {
      quarter = 2;
    } else {
      quarter = 3;
    }
 var rollbar: Rollbar;
    var answerResult = {
      'CertificateType': params.CertificateType,
      'FyId': params.FyId,
      'LanguageId': params.LanguageID,
      'CourseId': params.CourseId,
      'StudentID': params.StudentID,
      'NbAnswerCCM': NbAnswerCCM,
      'ScoreCCM': ScoreCCM,
      'NbAnswerFR': NbAnswerFR,
      'ScoreFR': ScoreFR,
      'NbAnswerIQ': NbAnswerIQ,
      'ScoreIQ': ScoreIQ,
      'NbAnswerOE': NbAnswerOE,
      'ScoreOE': ScoreOE,
      'NbAnswerOP': NbAnswerOP,
      'ScoreOP': ScoreOP,
      'Status': 'A',
      'EvaluationAnswerJson':survey.data,
      'CreatedBy': params.StudentName,
      'Quarter': quarter, //Created Date ada di API nya
      'Indicator': parseInt(params.FyId.substring(2, 4)) + 1,
    };

    
    options.showDataSaving("Please wait... while we saving your response");
	 var rollbar: Rollbar;
    setTimeout(() => {
      var xhr = new XMLHttpRequest();
      xhr.open(
        "POST",
        evaAnswerUrl
      );
      
      xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      xhr.setRequestHeader("Authorization",`Bearer ` + token);
      xhr.onload = xhr.onerror = function () {
        var result = xhr.response ? xhr.response : null;
        if (xhr.status === 200) {
          let tmpResponse: any = JSON.parse(xhr.responseText)
          if (tmpResponse.code == 1) {
            options.showDataSavingSuccess();
            swal(
              'Good job!',
              'Thank you for participating the survey. You can now download the certificate of attendance',
              'success'
            ).then(function () {
              var current_url = window.location;

              var base_url = "";

              if (current_url.pathname.split('/').length > 3) {
                base_url = current_url.protocol + "//" + current_url.host + "/" + current_url.pathname.split('/')[1] + "/" + current_url.pathname.split('/')[2];
              } else {
                base_url = current_url.protocol + "//" + current_url.host + "/" + current_url.pathname.split('/')[1];
              }

              location.href = base_url + "/my-course/";
            });
          }
          else {
            swal(
              'Information!',
              'System found out you have not enrolled this course, Please check your enroll list!',
              'warning'
            ).then(function () {
              var current_url = window.location;

              var base_url = "";

              if (current_url.pathname.split('/').length > 3) {
                base_url = current_url.protocol + "//" + current_url.host + "/" + current_url.pathname.split('/')[1] + "/" + current_url.pathname.split('/')[2];
              } else {
                base_url = current_url.protocol + "//" + current_url.host + "/" + current_url.pathname.split('/')[1];
              }

              location.href = base_url + "/my-course/";
            });
          }

          // options.showDataSavingSuccess();
          // swal(
          //   'Good job!',
          //   'Thank you for participating the survey. You can now download the certificate of attendance',
          //   'success'
          // ).then(function(){
          //   var current_url = window.location;

          //   var base_url = "";

          //   if (current_url.pathname.split('/').length > 3) {
          //     base_url = current_url.protocol + "//" + current_url.host + "/" + current_url.pathname.split('/')[1] + "/" + current_url.pathname.split('/')[2];
          //   } else {
          //     base_url = current_url.protocol + "//" + current_url.host + "/" + current_url.pathname.split('/')[1];
          //   }

          //   location.href = base_url + "/my-course/";
          // });

        } else {
          options.showDataSavingError();
        }
      };
      xhr.send(
        JSON.stringify(answerResult)
      );
      this.loading = false;
    }, 2000)
  }
}
