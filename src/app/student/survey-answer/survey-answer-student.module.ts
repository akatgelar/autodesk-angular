import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyAnswerStudentComponent } from './survey-answer-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const SurveyAnswerStudentRoutes: Routes = [
  {
    path: '',
    component: SurveyAnswerStudentComponent,
    data: {
      breadcrumb: 'Survey Course',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyAnswerStudentRoutes),
    SharedModule, LoadingModule
  ],
  declarations: [SurveyAnswerStudentComponent],
  providers: [AppService, AppFormatDate]
})
export class SurveyAnswerStudentModule { }
