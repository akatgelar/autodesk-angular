import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadCertificateStudentComponent } from './download-certificate-student.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";


export const DownloadCertificateStudentRoutes: Routes = [
  {
    path: '',
    component: DownloadCertificateStudentComponent,
    data: {
      breadcrumb: 'My Course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'My Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DownloadCertificateStudentRoutes),
    SharedModule
  ],
  declarations: [DownloadCertificateStudentComponent]
})
export class DownloadCertificateStudentModule { }
