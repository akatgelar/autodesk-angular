import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.CourseId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.CompletionDate.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.SiteAddress1.toLowerCase().indexOf(query.trim().toLowerCase()) > -1)
                );
        }
        return array;
    }
}

@Component({
    selector: 'app-history-course-student',
    templateUrl: './history-course-student.component.html',
    styleUrls: [
    './history-course-student.component.css',
    '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class HistoryCourseStudentComponent implements OnInit {

    private _serviceUrl = 'api/StudentsCourses';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "Name";
    public sortOrder: string = "asc";
    public dataStudent: any;
    public useraccesdata: any;
    private StudentId: any;
    private loading = false;
    public language = "35";
    public isMobile = false;

    constructor(private service: AppService, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
		//var thisYear=(new Date()).getFullYear();
		
		//if(FyId!=thisYear){			
		//	FyId=thisYear;			
		//}
        if(localStorage.getItem("languageKey") != null && localStorage.getItem("languageKey") != undefined && localStorage.getItem("languageKey") != ""){
            this.language = localStorage.getItem("languageKey");
        }

        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
                this.isMobile = true;
        }
        // var data = '';
        // this.service.httpClientGet('api/Student/' + this.useraccesdata.Email, data)
        // .subscribe(result => {
        //     if (result == "Not found") {
        //         this.service.notfound();
        //         this.dataStudent = '';
        //     }
        //     else {
        //         this.dataStudent = result;
        //         this.getcourse(this.dataStudent.StudentID);
        //     }
        // },
        // error => {
        //     this.service.errorserver();
        //     this.dataStudent = '';
        // });

        /* get studentid */
        this.getcourse(this.useraccesdata.UserId);
        /* get studentid */
    }

    getcourse(studentid) {
        this.loading = true;
        this.StudentId = studentid;
        var data :any;
        this.service.httpClientGet(this._serviceUrl + "/historycourse/" + studentid, data) /* keep loading when data empty (02102018) */
        .subscribe(result => {
            // console.log(result);
            if (result == "Not found" || result == null) {
                //this.service.notfound();
                this.data = '';
                this.loading = false;
            }
            else {
                this.data = result; /* keep loading when data empty (02102018) */
                //allow special character
                if (this.data != null) {
                    for (var i = 0; i < this.data.length; i++) {
                        if (this.data[i].Name != null && this.data[i].Name != '') {
                            this.data[i].Name = this.service.decoder(this.data[i].Name);
                        }
                    }
                }
                this.loading = false;
            }
        },
        error => {
            this.service.errorserver();
            this.data = '';
            this.loading = false;
        });
    }

    DownloadCertificateStudent(FyId, TrainingId, ProjectId, EventId, CourseId, PartnerType) {
        this.loading = true;

        let certificateType;
        let partnerType;
        let data: any;

        switch (PartnerType) {
            case "AAP Course":
            partnerType = 58;
            certificateType = 1;
            break;
            case "AAP Project":
            partnerType = 58;
            certificateType = 2;
            break;
            case "AAP Event":
            partnerType = 58;
            certificateType = 3;
            break;

            default:
            partnerType = 1;
            certificateType = 0;
            break;
        }
        // if (TrainingId > 0 && ProjectId == 0 && EventId == 0) {
        //     certificateType = 1;
        // } else if (TrainingId == 0 && ProjectId > 0 && EventId == 0) {
        //     certificateType = 2;
        // } else if (TrainingId == 0 && ProjectId == 0 && EventId > 0) {
        //     certificateType = 3;
        // } else {
        //     certificateType = 0;
        // }

        // if(certificateType == 0){
        //     partnerType = 1;
        // } else{
        //     partnerType = 58;
        // }

        this.service.httpClientGet("api/Certificate/where/{'CertificateTypeId':'" + certificateType + "','Year':'" + FyId + "','LanguageCertificate':'" + this.language + "','PartnerType':'" + partnerType + "','courseid':'" + CourseId + "','UserId':'" + this.useraccesdata.UserId + "'}", data)
        .subscribe(result => {
            if (result == "Not found" || Object.keys(result).length < 1) {
                console.log("Certificate Not Found");
                this.loading = false;
            }
            else {
                data = result;
                $("#certificate_iframe").empty();
                var iframe = document.createElement('iframe');
                iframe.src = 'template-preview/certificate-download-student-eva/' + data[0].CertificateId + '/' + CourseId + '/' + this.StudentId 
                document.getElementById("certificate_iframe").appendChild(iframe);
                    this.loading = false;
            }
        },
        error => {
            this.service.errorserver();
            this.loading = false;
        });
    }

    GetCertificateViaEmail(partnerType, FYIndicatorKey, language, studentId, courseId) {
        this.loading = true;
        this.service.httpClientGet("api/Certificate/EmailCertificate/" + partnerType + "/" + FYIndicatorKey + "/" + this.language + "/" + studentId + "/" + courseId, '')
        .subscribe(result => {
            swal('Successful!', 'The certificate is sent to your email, please check the inbox!', 'info');
            this.loading = false;
        },
        error => {
            this.service.errorserver();
            this.loading = false;
        });
    }
}
