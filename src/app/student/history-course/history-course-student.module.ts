import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryCourseStudentComponent } from './history-course-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';
import { AppService } from "../../shared/service/app.service";
import { DataFilterActivitiesPipe, ConvertDatePipe } from './history-course-student.component';


export const HistoryCourseStudentRoutes: Routes = [
  {
    path: '',
    component: HistoryCourseStudentComponent,
    data: {
      breadcrumb: 'dashboard.student.finished_course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'History Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HistoryCourseStudentRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [HistoryCourseStudentComponent, DataFilterActivitiesPipe, ConvertDatePipe],
  providers: [AppService]
})
export class HistoryCourseStudentModule { }
