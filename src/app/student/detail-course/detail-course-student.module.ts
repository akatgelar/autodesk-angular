import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailCourseStudentComponent } from './detail-course-student.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { CalenderComponent } from './calender/calender.component';

export const DetailCourseStudentRoutes: Routes = [
  {
    path: '',
    component: DetailCourseStudentComponent,
    data: {
      breadcrumb: 'Detail Course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Detail Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailCourseStudentRoutes),
    SharedModule
  ],
  declarations: [DetailCourseStudentComponent, CalenderComponent]
})
export class DetailCourseStudentModule { }
