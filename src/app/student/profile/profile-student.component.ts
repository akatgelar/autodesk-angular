import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-profile-student',
  templateUrl: './profile-student.component.html',
  styleUrls: [
    './profile-student.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ProfileStudentComponent implements OnInit {

  private _serviceUrl = 'api/ContactAll';
  private _serviceStudentUrl = 'api/Student';

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  changepasswordform: FormGroup;
  changeemailform: FormGroup;
  myprofile: FormGroup;
  public useraccesdata: any;
  ProfilePicture: string;
  public success:Boolean=false;
  public loading = false;

  // get data
  honorific;
  gender;
  primarylanguage;
  // secondarylanguage;
  primaryindustry;
  stateprovince;
  country;

  statusLevel=["Student","Professional"];

  public minDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  
  public minDate2: NgbDateStruct;

  constructor(private formatdate: AppFormatDate, private _http:HttpClient, private http: Http, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    this.minDate = { year: new Date().getFullYear() - 68, month: new Date().getMonth() + 1, day: new Date().getDate() };
    this.maxDate = { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1};

    this.minDate2 = { year: new Date().getFullYear(), month: new Date().getMonth()+1, day: new Date().getDate() };

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let FirstName = new FormControl('');
    let LastName = new FormControl('');
    let Salutation = new FormControl('');
    let Gender = new FormControl('');
    let PrimaryIndustryId = new FormControl('');
    let PrimaryLanguage = new FormControl('');
    let Address1 = new FormControl('');
    let Address2 = new FormControl('');
    let City = new FormControl('');
    let StateProvince = new FormControl('');
    let CountryID = new FormControl('');
    let CountryCode = new FormControl('');
    let PostalCode = new FormControl('');
    let Company = new FormControl('');
    let EmailAddress = new FormControl('');
    let EmailAddress2 = new FormControl('');
    let Telephone1 = new FormControl('');
    let Mobile1 = new FormControl('');
    let MobileCode = new FormControl('');
    let TelephoneCode = new FormControl('');
    let DateBirth = new FormControl('');
    let EGD = new FormControl('');
    let StatusLevel = new FormControl('');

    this.myprofile = new FormGroup({
      FirstName: FirstName,
      LastName: LastName,
      Salutation: Salutation,
      Gender: Gender,
      PrimaryIndustryId: PrimaryIndustryId,
      PrimaryLanguage: PrimaryLanguage,
      Address1: Address1,
      Address2: Address2,
      City: City,
      StateProvince: StateProvince,
      CountryID: CountryID,
      CountryCode: CountryCode,
      PostalCode: PostalCode,
      Company: Company,
      EmailAddress: EmailAddress,
      EmailAddress2: EmailAddress2,
      Telephone1: Telephone1,
      Mobile1: Mobile1,
      MobileCode: MobileCode,
      TelephoneCode: TelephoneCode,
      DateBirth:DateBirth,
      EGD:EGD,
      StatusLevel:StatusLevel
    });
  }

  generatePassword(){
    this.loading = true;
    var data = {
      "StudentID":this.useraccesdata.UserId
    }

    /* autodesk plan 10 oct */

    // this.service.httpCLientPut('api/Auth/GeneratePassStudent','',data);
    // this.loading = false;
    this.service.httpCLientPut('api/Auth/GeneratePassStudent',data)
      .subscribe(result => {
          var res = result;
          if(res['code'] == "1"){
            swal(
              'Information!',
              res['message'],
              'success'
            );
            this.loading = false;
          }else{
            console.log("failed");
            this.loading = false;
          }
      }, error => {
        console.log("failed");
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */

  }

  ngOnInit() {
    var data = '';
    this.service.httpClientGet(this._serviceStudentUrl + '/GetStudent/' + this.useraccesdata.UserId, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = '';
        }
        else {
          this.data = result;
            if (this.data.Firstname != null && this.data.Firstname != '') {
                this.data.Firstname = this.service.decoder(this.data.Firstname);
            }
            if (this.data.Lastname != null && this.data.Lastname != '') {
              this.data.Lastname = this.service.decoder(this.data.Lastname);
        }
          this.buildForm();
          this.getProfulePicture(this.data.ProfilePicture);
        }
      },
      error => {
        this.service.errorserver();
        this.data = '';
      });

    this.getHonorfic();
    this.getGender();
    this.getPrimaryIndustry();
    this.getPrimaryLanguage();
    this.getCountry();

  }

  getProfulePicture(picture) {

    /* autodesk plan 10 oct */

    this.service.httpClientGet("assets/profilepictures/" + picture,"")
      .subscribe((status) => {
        if (status == "200") {
          this.ProfilePicture = "assets/profilepictures/" + picture;
        }
        else {
          this.ProfilePicture = "assets/profilepictures/user.png";
        }
      });

    /* end line autodesk plan 10 oct */
    
  }
  modelPopup1:any
  modelPopup2:any
  buildForm(): void {
    var dobtmp:any;
    if(this.data.DateBirth == "00/00/0000"){
      dobtmp = null;
    }else{
      dobtmp = this.formatdate.dateYMDToCalendar(this.data.DateBirth.toString().substr(0,10));
    }
    this.modelPopup2 = dobtmp

    var egdtmp:any;
    if(this.data.ExpetationGradDate == "00/00/0000"){
      egdtmp = null;
    }else{
      egdtmp = this.formatdate.dateYMDToCalendar(this.data.ExpetationGradDate.toString().substr(0,10));
    }
    this.modelPopup1 = egdtmp

    let EmailAddress = new FormControl(this.data.Email);
    let FirstName = new FormControl(this.data.Firstname);
    let LastName = new FormControl(this.data.Lastname);
    let Salutation = new FormControl(this.data.Salutation);
    let Gender = new FormControl(this.data.Gender);
    let PrimaryIndustryId = new FormControl(this.data.PrimaryIndustryId);
    let PrimaryLanguage = new FormControl(this.data.LanguageID);
    let Address1 = new FormControl(this.data.Address);
    let Address2 = new FormControl(this.data.Address2);
    let City = new FormControl(this.data.City);
    let CountryCode = new FormControl(this.data.CountryID);
    let StateProvince = new FormControl(this.data.StateProvince);
    let PostalCode = new FormControl(this.data.PostalCode);
    let Company = new FormControl(this.data.Company);
    let Telephone1 = new FormControl(this.data.Phone);
    let Mobile1 = new FormControl(this.data.Mobile);
    let MobileCode = new FormControl(this.data.MobileCode);
    let TelephoneCode = new FormControl(this.data.TelephoneCode);
    let EmailAddress2 = new FormControl(this.data.Email2);
    let DateBirth = new FormControl(dobtmp);
    let EGD = new FormControl(egdtmp);
    let StatusLevel = new FormControl(this.data.StatusLevel);

    if (this.data.CountryID != null && this.data.CountryID != "") {
      this.getStateProvince(this.data.CountryID);
    }

    this.myprofile = new FormGroup({
      EmailAddress: EmailAddress,
      EmailAddress2: EmailAddress2,
      FirstName: FirstName,
      LastName: LastName,
      Salutation: Salutation,
      Gender: Gender,
      PrimaryIndustryId: PrimaryIndustryId,
      PrimaryLanguage: PrimaryLanguage,
      Address1: Address1,
      Address2: Address2,
      City: City,
      CountryCode: CountryCode,
      StateProvince: StateProvince,
      PostalCode: PostalCode,
      Company:Company,
      Telephone1: Telephone1,
      Mobile1: Mobile1,
      MobileCode: MobileCode,
      TelephoneCode: TelephoneCode,
      DateBirth:DateBirth,
      EGD:EGD,
      StatusLevel:StatusLevel
    });
  }

  getHonorfic() {
    var honorific = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Salutation','Status':'A'}", honorific)
      .subscribe(result => {
        this.honorific = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getGender() {
    var gender = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Gender','Status':'A'}", gender)
      .subscribe(result => {
        this.gender = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getPrimaryIndustry() {
    var primaryindustry = '';
    this.service.httpClientGet("api/Industries/", primaryindustry)
      .subscribe(result => {
        this.primaryindustry = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getPrimaryLanguage() {
    var primarylanguage = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", primarylanguage)
      .subscribe(result => {
        this.primarylanguage = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getStateProvince(id) {
    var data = '';
    this.service.httpClientGet('api/States/where/{"CountryCode":"' + id + '"}', data)
      .subscribe(result => {
        this.stateprovince = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getCountry() {
    var country = '';
    this.service.httpClientGet("api/Countries/", country)
      .subscribe(result => {
        this.country = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  onSubmit() {
    let format = /[!$%^&*+\-=\[\]{};:\\|.<>\/?]/
    if(format.test(this.myprofile.value.FirstName) || format.test(this.myprofile.value.LastName))
        return swal('ERROR','Special character not allowed in Name','error')
    if(this.myprofile.value.DateBirth == undefined || this.myprofile.value.DateBirth == null){
      this.myprofile.value.DateBirth = "";
    }else{
      this.myprofile.value.DateBirth = this.formatdate.dateCalendarToYMD(this.myprofile.value.DateBirth);
    }

    if(this.myprofile.value.EGD == undefined || this.myprofile.value.EGD == null){
      this.myprofile.value.EGD = "";
    }else{
      this.myprofile.value.EGD = this.formatdate.dateCalendarToYMD(this.myprofile.value.EGD);
    }
    this.myprofile.value.FirstName = this.service.encoder(this.myprofile.value.FirstName);
    this.myprofile.value.LastName=this.service.encoder(this.myprofile.value.LastName);
    let data = JSON.stringify(this.myprofile.value);

    // this.service.httpCLientPut(this._serviceStudentUrl + "/UpdateProfile",this.useraccesdata.UserId, data)
    this.service.httpCLientPut(this._serviceStudentUrl + "/UpdateProfile/"+this.useraccesdata.UserId, data)
      .subscribe(res=>{
        console.log(res);
        swal(
          'Information',
           res["message"],
          'success'
      )
      });
      
    this.success = true;
    setTimeout(() => {
      this.success = false;
    }, 3000)
    this.router.navigate(['/student/student-profile']);
  }

}
