import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileStudentComponent} from './profile-student.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';


export const ProfileStudentRoutes: Routes = [
  {
    path: '',
    component: ProfileStudentComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProfileStudentRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ProfileStudentComponent],
  providers:[AppService,AppFormatDate]
})
export class ProfileStudentModule { }
