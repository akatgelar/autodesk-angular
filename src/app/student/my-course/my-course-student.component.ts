import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { Location } from '@angular/common';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { AcademicProjectsReportEPDBComponent } from '../../epdb/academic-projects-report/academic-projects-report-epdb.component.js';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe){}
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0,10)), 'dd-MMMM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.CourseId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.StartDate.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterStatus' })
export class DataFilterStatusPipe {
    transform(array: any[], query: string): any {
        if (query == "Active") {
            return _.filter(array, row => 
                (row.SurveyTaken.indexOf("0") > -1) &&
                (row.activesurveylink.indexOf("Active") > -1));
        }
        else if(query == "Expired" || query == "Not Started"){
            return _.filter(array, row => 
                (row.activesurveylink.indexOf(query) > -1));
        }else if(query == "Completed"){
            return _.filter(array, row => 
                (row.SurveyTaken.indexOf("1") > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-my-course-student',
    templateUrl: './my-course-student.component.html',
    styleUrls: [
        './my-course-student.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class MyCourseStudentComponent implements OnInit {

    private _serviceUrl = 'api/StudentsCourses';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "NAME";
    public sortOrder: string = "asc";
    public activelinksurvey = [];
    public useraccesdata: any;
    public dataStudent: any;
    certid = [];
    public filterStatus : string ="All";
    private loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private route: ActivatedRoute, private router: Router, private location: Location) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
        this.location.go('/student/my-course');

        // var data = '';
        // this.service.httpClientGet('api/Student/' + this.useraccesdata.Email, data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             this.dataStudent = '';
        //         }
        //         else {
        //             this.dataStudent = result;
        //             this.getcourse(this.dataStudent.StudentID);
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //             this.dataStudent = '';
        //         });

        /* get studentid */
        this.getcourse(this.useraccesdata.UserId);
        /* get studentid */
    }

    getcourse(studentid) {
        this.loading = true;
        var data :any;
        this.service.httpClientGet(this._serviceUrl + '/allcourse/' + studentid, data)
            .subscribe(result => {
                if (result == 'Not found' || result == null) {
                    //this.service.notfound();
                    this.data = '';
                    this.loading = false;
                }
                else {
                    this.data = result;
                    //allow special character
                    if (this.data != null) {
                        for (var i = 0; i < this.data.length; i++) {
                            if (this.data[i].Name != null && this.data[i].Name != '') {
                                this.data[i].Name = this.service.decoder(this.data[i].Name);
                            }
                        }
                      }
                        
                   
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
    }

    // getCertificateType(TrainingId, ProjectId, EventId) {
    //     let certificateType;

    //     if (TrainingId != "" && ProjectId == "" && EventId == "") {
    //         certificateType = 1;
    //     } else if (TrainingId == "" && ProjectId != "" && EventId == "") {
    //         certificateType = 2;
    //     } else if (TrainingId == "" && ProjectId == "" && EventId != "") {
    //         certificateType = 3;
    //     } else {
    //         certificateType = 0;
    //     }

    //     return certificateType;
    // }

    getCertificateType(PartnerType) {
        let certificateType;

        switch (PartnerType) {
            case "AAP Course":
            certificateType = 1;
            break;
            case "AAP Project":
            certificateType = 2;
            break;
            case "AAP Event":
            certificateType = 3;
            break;

            default:
            certificateType = 0;
            break;
        }

        return certificateType;
    }

    takeSurvey(FyId, TrainingId, ProjectId, EventId, CourseId, PartnerType) {
        var access = Object.keys(this.useraccesdata)[5];
        var name = Object.keys(this.useraccesdata[access])[2];

        var language = "35";

        if(localStorage.getItem("languageKey") != null && localStorage.getItem("languageKey") != undefined && localStorage.getItem("languageKey") != ""){
            language = localStorage.getItem("languageKey");
        }

        var data :any = JSON.stringify({
            'StudentName': this.useraccesdata[access][name],
            // 'CertificateType': this.getCertificateType(TrainingId, ProjectId, EventId),
            'CertificateType': this.getCertificateType(PartnerType),
            'CourseId': CourseId,
            'StudentID': this.useraccesdata.UserId,
            'FyId': FyId,
            // 'LanguageID': this.useraccesdata.LanguageID
            'LanguageID': language
        });
    
        this.service.httpClientPost("api/Courses/EncrollCheck", data).subscribe(result => {
            if (result["code"] == '1') {
                this.router.navigate(['/student/survey-course'], { queryParams: { args: data } });
                setTimeout(() => {
                    location.reload()
                }, 500);
             }
            else{
                swal('Information', result["message"], 'error');
                this.ngOnInit();
            }
        });
        
        
    }

    DownloadCertificateStudent(FyId, TrainingId, ProjectId, EventId, CourseId, StudentID) {
        let LanguageCertificate = this.useraccesdata.LanguageID
        let certificateType;
        let data: any;
        if (TrainingId == 1 && ProjectId == 0 && EventId == 0) {
            certificateType = 1;
        } else if (TrainingId == 0 && ProjectId == 1 && EventId == 0) {
            certificateType = 2;
        } else if (TrainingId == 0 && ProjectId == 0 && EventId == 1) {
            certificateType = 3;
        } else {
            certificateType = 0;
        }

        this.service.httpClientGet("api/Certificate/where/{'CertificateTypeId':'" + certificateType + "','Year':'" + FyId + "','LanguageCertificate':'" + LanguageCertificate + "'}", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    data = result;
                    var newWindow = window.open('template-preview/certificate-download-student-eva/' + data[0].CertificateId + '/' + CourseId + '/' + StudentID, 'mywin', 'left=270,top=30,width=900,height=650,toolbar=1,resizable=0');
                    if (window.focus()) { newWindow.focus() }
                    return false;
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

}