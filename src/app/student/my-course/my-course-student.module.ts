import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyCourseStudentComponent } from './my-course-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { CalenderComponent } from './calender/calender.component';
import { LoadingModule } from 'ngx-loading';
import { AppService } from "../../shared/service/app.service";
import { DataFilterActivitiesPipe, ConvertDatePipe, DataFilterStatusPipe } from './my-course-student.component';
import { AppFormatDate } from "../../shared/format-date/app.format-date";

export const MyCourseStudentRoutes: Routes = [
  {
    path: '',
    component: MyCourseStudentComponent,
    data: {
      breadcrumb: 'menu_course.my_course',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'My Course'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MyCourseStudentRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [MyCourseStudentComponent, CalenderComponent, DataFilterActivitiesPipe, ConvertDatePipe, DataFilterStatusPipe],
  providers: [AppService, AppFormatDate]
})
export class MyCourseStudentModule { }
