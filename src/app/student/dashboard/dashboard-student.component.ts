import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> (row.CourseId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.StartDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteAddress1.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        } 
        return array;
    }
}


@Component({
  selector: 'app-dashboard-student',
  templateUrl: './dashboard-student.component.html',
  styleUrls: [
    './dashboard-student.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class DashboardStudentComponent implements OnInit {

  private _serviceUrl = 'api/StudentsCourses';
  public studentcourse: any;
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  public activelinksurvey=[];
  public useraccesdata:any;
  public dataStudent:any;

  constructor(private service:AppService, private formatdate:AppFormatDate, private session:SessionService) { 
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
  }

  ngOnInit() {
   var data = '';
   this.service.httpClientGet('api/Student/'+this.useraccesdata.Email,data)
   .subscribe(result => { 
       if(result=="Not found"){
           this.service.notfound();
           this.dataStudent = '';
       }
       else{
           this.dataStudent = result;
           this.getcourse(this.dataStudent.StudentID);
       } 
   },
   error => {
       this.service.errorserver();
       this.dataStudent = '';
   });
  }


  public mycourse:any;
  public countMyCourse:number=0;
  public historycourse:any;
  public countHistoryCourse:number=0;
  public nextcourse:any;
  public countNextCourse:number=0;


  getcourse(studentid){
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/allcourse/'+studentid,data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.data = '';
        }
        else{
            this.data = result;
        } 
    },
    error => {
        this.service.errorserver();
        this.data = '';
    });

    this.service.httpClientGet(this._serviceUrl+'/mycourse/'+studentid,data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.mycourse = '';
        }
        else{
            this.mycourse = result;
            this.countMyCourse = this.mycourse.length;
        } 
    },
    error => {
        this.service.errorserver();
        this.mycourse = '';
    });

    this.service.httpClientGet(this._serviceUrl+'/historycourse/'+studentid,data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.historycourse = '';
        }
        else{
            this.historycourse = result;
            this.countHistoryCourse = this.historycourse.length;
        } 
    },
    error => {
        this.service.errorserver();
        this.historycourse = '';
    });

    this.service.httpClientGet(this._serviceUrl+'/nextcourse/'+studentid,data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.nextcourse = '';
        }
        else{
            this.nextcourse = result;
            this.countNextCourse = this.nextcourse.length;
        } 
    },
    error => {
        this.service.errorserver();
        this.nextcourse = '';
    });
  }

  detailCourse(id) {
    if (id != "") {
      var data = '';
      var detail: any;
      this.service.httpClientGet("api/Courses/Detail/" + id, data)
        .subscribe(res => {
            detail = res;
            if (detail.CourseId != "") {
                this.datadetail = detail;
            } else {
                swal(
                    'Information!',
                    "Data Not Found",
                    'error'
                );
            }
        }, error => {
            this.service.errorserver();
        });
    } else {
      swal(
          'Information!',
          "Data Not Found",
          'error'
      );
    }
  }

}
