import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardStudentComponent } from './dashboard-student.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import { DataFilterActivitiesPipe } from './dashboard-student.component';
import {AppFormatDate} from "../../shared/format-date/app.format-date";

export const DashboardStudentRoutes: Routes = [
  {
    path: '',
    component: DashboardStudentComponent,
    data: {
      breadcrumb: 'Dashboard',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardStudentRoutes),
    SharedModule
  ],
  declarations: [DashboardStudentComponent, DataFilterActivitiesPipe],
  providers:[AppService,AppFormatDate]
})
export class DashboardStudentModule { }
