import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule,ErrorHandler,Injectable,Injector,InjectionToken } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutes } from './app.routing';
import {LocatorComponent} from './locator/locator.component';
import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
// import { AdminLayoutEIMComponent } from './layouts/admin-eim/admin-layout.component';
// import { AdminLayoutEVAComponent } from './layouts/admin-eva/admin-layout.component';
// import { AdminLayoutEPDBComponent } from './layouts/admin-epdb/admin-layout.component';
// import { AdminLayoutAutodeskAdminComponent } from './layouts/admin-autodesk-admin/admin-layout.component';
// import { AdminLayoutAutodeskUserComponent } from './layouts/admin-autodesk-user/admin-layout.component';
// import { AdminLayoutDistributorComponent } from './layouts/admin-distributor/admin-layout.component';
// import { AdminLayoutPartnerAdminComponent } from './layouts/admin-partner-admin/admin-layout.component';
// import { AdminLayoutPartnerUserComponent } from './layouts/admin-partner-user/admin-layout.component';
// import { AdminLayoutStudentComponent } from './layouts/admin-student/admin-layout.component';
import { AdminLayoutNewComponent } from './layouts/admin-new/admin-layout.component';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import { ScrollModule } from './scroll/scroll.module';
import { LocationStrategy, PathLocationStrategy, DatePipe } from '@angular/common';
import { HttpClient, HttpClientModule,HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgxPermissionsModule } from 'ngx-permissions';
import { Interceptor } from './shared/service/httpinterceptor';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { Http } from "@angular/http";
import { SessionService } from './shared/service/session.service';
import { AppService } from "./shared/service/app.service";
import * as Rollbar from 'rollbar';
import { from } from 'rxjs/observable/from';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "api/Language/", "");
}

const rollbarConfig = {
    accessToken: 'e484ef92addd422b9552590cfa943d93',
    captureUncaught: true,
    captureUnhandledRejections: true,
    hostWhiteList: ['autodesk.ligarian.com','autodesk.bluecube.com.sg'],
    checkIgnore: function(isUncaught, args, payload) {
        return isUncaught === true;
    },
    ignoredMessages: ["TypeError:","'Invalid Date' for pipe 'DatePipe'","Cannot read property"]
};

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) { }
  handleError(err:any) : void {
    var rollbar = this.injector.get(Rollbar);
    rollbar.error(err.originalError || err);
  }
}

export function rollbarFactory(){
    return new Rollbar(rollbarConfig);
}

export const RollbarService = new InjectionToken<Rollbar>('rollbar');

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    
    // AdminLayoutEIMComponent,
    // AdminLayoutEVAComponent,
    // AdminLayoutEPDBComponent,
    // AdminLayoutAutodeskAdminComponent,
    // AdminLayoutAutodeskUserComponent,
    // AdminLayoutDistributorComponent,
    // AdminLayoutPartnerAdminComponent,
    // AdminLayoutPartnerUserComponent,
    // AdminLayoutStudentComponent,
    AdminLayoutNewComponent,
    BreadcrumbsComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    NgIdleKeepaliveModule.forRoot(),
    FormsModule,
    HttpModule,
    ScrollModule,
    HttpClientModule,
	NgbModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [ScrollModule],
  providers: [
    
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    SessionService, DatePipe, AppService,LocatorComponent, 
    { provide: ErrorHandler, useClass: RollbarErrorHandler },
    { provide: Rollbar, useFactory: rollbarFactory },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
