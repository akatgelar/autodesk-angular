import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UserTrainingComponent } from './usertraining.component';
import { UTRoutes } from './usertraining.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UTRoutes),
        SharedModule
    ],
    declarations: [UserTrainingComponent]
})
export class UserTrainingModule { }
