import { Routes } from '@angular/router';
import { UserTrainingComponent } from './usertraining.component';

export const UTRoutes: Routes = [{
    path: '',
    component: UserTrainingComponent,
    data: {
        breadcrumb: 'UserTrainingInfo'
    }
}];
