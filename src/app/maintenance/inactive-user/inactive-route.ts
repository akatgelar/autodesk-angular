import { Routes } from '@angular/router';
import { InactiveUserComponent } from './inactive-user.component';

export const InactiveRoutes: Routes = [{
    path: '',
    component: InactiveUserComponent,
    data: {
        breadcrumb: 'inactive'
    }
}];
