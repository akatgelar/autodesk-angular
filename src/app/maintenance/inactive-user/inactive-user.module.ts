import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InactiveUserComponent } from './inactive-user.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { InactiveRoutes } from './inactive-route';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(InactiveRoutes),
        SharedModule
      
    ],
    declarations: [InactiveUserComponent]
})
export class InactiveUserModule { }