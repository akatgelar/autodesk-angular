import { Routes } from '@angular/router';
import { HelpMeComponent } from './help.component';

export const HelpRoutes: Routes = [{
    path: '',
    component: HelpMeComponent,
    data: {
        breadcrumb: 'Help'
    }
}];
