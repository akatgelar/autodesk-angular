import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HelpMeComponent } from './help.component';
import { HelpRoutes } from './help.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(HelpRoutes),
        SharedModule
    ],
    declarations: [HelpMeComponent]
})
export class HelpModule { }
