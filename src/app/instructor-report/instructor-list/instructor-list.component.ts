import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { SessionService } from '../../shared/service/session.service';
import * as Rollbar from 'rollbar';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-instructor-list',
  templateUrl: './instructor-list.component.html',
  styleUrls: ['./instructor-list.component.css']
})
export class InstructorListComponent implements OnInit {

  closeResult: string;
  modalReference: NgbModalRef;
  private currentUserId: any;
  private instructorReportViewModel={};
  private removeAffiliateStatus=false;
  private instructorListModel=[];
  private reasonType:any;
  public  messageError: string = '';
  private baseApiUrl: string ="api/InstructorDeletion/";
  public loading = false;    
  private selectedRespondType;
  private requestedSiteId:any;
  private selectedInstructorID;
  private ddlSiteList=[];
  private dropdownSettings = {};
  private selectedSites=[];
  private siteList=[];
  private searchParam;
  private rowsOnPage: number = 10;


  constructor(private service: AppService,private session: SessionService,private rollbar: Rollbar, private modalService: NgbModal, private router :Router) { }

  ngOnInit() {
  
    var userCurrentData = this.session.getData();
    
      if(userCurrentData != null){
        userCurrentData= JSON.parse(userCurrentData);
        this.currentUserId=userCurrentData['UserId'];
      
        var userLevel=userCurrentData['UserLevelId'].split(',');
        if(userLevel.indexOf("ORGANIZATION") != -1){
          this.getInstructor();
          this.getReasonType();
          this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            badgeShowLimit: 5
           
          };
       
        }else{
          this.router.navigate(['/forbidden']);
        }

       
      }else{
        this.router.navigate(['/no-role-page']);
      }
  }

   getInstructor(){
      if(this.currentUserId != null){
       
        this.service.httpClientGet(this.baseApiUrl+"GetInstructor/"+this.currentUserId,'')
        .subscribe(res => {
            var data = res;
            if(data['code'] == true){
              this.instructorListModel=data['model'];
            }else if(data['code'] == false){
              this.rollbar.log("error",data['message']);
            }
            if(data == null){
                this.rollbar.log("error","Something went wrong when parsing SKU data!");
                this.loading = false;
            }
          
        }, error => {
            this.messageError = <any>error;
            this.service.errorserver();
            this.loading = false;
        });
      }
   }

   getReasonType(){
    this.service.httpClientGet(this.baseApiUrl+"GetRespondType/InstructorDeletion",'')
    .subscribe(res => {
       var data=res;
      if(data['code'] == true){
         this.reasonType = data['model'];
      }else if(data['code'] == false){
        this.rollbar.log("error",data['message']);       
      }      
    }, error => {
      this.messageError = <any>error;
      this.service.errorserver();
      this.loading = false;
    });
   }

   ddlRespondChange(data){
     console.log(data);
    console.log(this.selectedRespondType);
    if(data != undefined && data != null){
      if(data.name == "Remove Affiliation" || data.name == "Add Affiliation"){
        this.removeAffiliateStatus=true;
      }else{
        this.removeAffiliateStatus=false;
      }
    }
  
   }
  
   requestAction(){

     if(this.selectedRespondType != null){
      this.instructorReportViewModel={ 
        ContactID:this.selectedInstructorID,
        CurrentUserId: this.currentUserId,
        DistributorId: this.currentUserId,
        RequestedSiteId:this.selectedSites.map(e => e.id).join(","),
        SiteId:this.siteList,
        SelectedRespondType:this.selectedRespondType.name,
        Reason:this.selectedRespondType.name,
        Status:""
      }
      this.service.httpClientPost(this.baseApiUrl+"RequestDeletion",this.instructorReportViewModel)
      .subscribe(res=>{
           if(res['code'] == true){
             var updatedModel=this.instructorListModel.find(x=>x.contactID == this.selectedInstructorID);
             var itemIndex=this.instructorListModel.indexOf(updatedModel);

             this.instructorListModel[itemIndex].status="Pending";
             this.loading=false;
           }
            this.JoinAndClose();
      });
     }
   }

   cancelRequest(contactID){
     if(contactID != null && contactID != undefined){
       this.service.httpClientGet(this.baseApiUrl+"CancelRequest/"+contactID+"/"+this.currentUserId,'')
       .subscribe(res=>{
        if(res['code']==true){
          var updatedModel=this.instructorListModel.find(x=>x.contactID == contactID);
             var itemIndex=this.instructorListModel.indexOf(updatedModel);

             this.instructorListModel[itemIndex].status=null;
             this.loading=false;
        }
       });
     }
   }
   open(content,item) {

     this.removeAffiliateStatus=false;
     this.selectedInstructorID=null;
     this.selectedRespondType=null;
      this.selectedSites=[];
     this.selectedInstructorID=item.contactID;
     this.siteList=item.siteId;
     this.ddlSiteList=item.siteId.map((item) => {
      return {
          id: item.id,
          itemName: item.name
      }
      });
    this.modalReference = this.modalService.open(content);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return 'with: ${reason}';
    }
  }

  onSiteSelect(item:any){
  
  }
  OnSiteDeSelect(item:any){

  }
  onSelectAll(items: any){

  }
  onDeSelectAll(items: any){

  }
    
    JoinAndClose() {
    this.modalReference.close();
    }
}
