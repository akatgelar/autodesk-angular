import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorListComponent } from './instructor-list.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchPipe } from './instructor-search-pipe';
export const InstructorListRoutes: Routes = [
    {
      path: '',
      component: InstructorListComponent,
      data: {
        breadcrumb: 'instructor-report/instructor-list',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(InstructorListRoutes),
        SharedModule,
        LoadingModule,
        NgbModule,
        AngularMultiSelectModule
      
    ],
    declarations: [InstructorListComponent, SearchPipe,],
    providers: [AppService, AppFilterGeo,AppFormatDate]
})
export class InstructorListModule { }