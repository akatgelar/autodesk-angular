import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataManagementComponent } from './data-management.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import { AppService } from "../shared/service/app.service";
import { AppFilterGeo } from "../shared/filter-geo/app.filter-geo";
import {AppFormatDate} from "../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
export const DataMagementRoute: Routes = [
    {
      path: '',
      component: DataManagementComponent,
      data: {
        breadcrumb: 'data-management-report',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DataMagementRoute),
        SharedModule,
        LoadingModule,
        NgbModule,
        AngularMultiSelectModule
      
    ],
    declarations: [DataManagementComponent],
    providers: [AppService, AppFilterGeo,AppFormatDate]
})
export class DataMagementModule { }