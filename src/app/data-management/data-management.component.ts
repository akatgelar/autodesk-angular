import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from "../shared/service/app.service";
import { AppFilterGeo } from "../shared/filter-geo/app.filter-geo";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { SessionService } from '../shared/service/session.service';
import * as Rollbar from 'rollbar';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-data-magement',
  templateUrl: './data-management.component.html',
  styleUrls: ['./data-management.component.css']
})
export class DataManagementComponent implements OnInit {

  closeResult: string;
  modalReference: NgbModalRef;
  private currentUserId: any;
  private dataManagementModel=[];
  private selectedKey;
  private tablesModel=[];
  public  messageError: string = '';
  private baseApiUrl: string ="api/DataManagement/";
  public loading = false;    
  
  private rowsOnPage: number = 10;


  constructor(private service: AppService,private session: SessionService,private rollbar: Rollbar, private modalService: NgbModal, private router :Router) { }

  ngOnInit() {
  
    var userCurrentData = this.session.getData();
    
      if(userCurrentData != null){
        userCurrentData= JSON.parse(userCurrentData);
        this.currentUserId=userCurrentData['UserId'];
      
        var userLevel=userCurrentData['UserLevelId'].split(',');
        if(userLevel.indexOf("SUPERADMIN") != -1){
          this.getTables();      
        }else{
          this.router.navigate(['/forbidden']);
        }

       
      }else{
        this.router.navigate(['/no-role-page']);
      }
  }

   getTables(){
      if(this.currentUserId != null){
       
        this.service.httpClientGet(this.baseApiUrl+"GetDataManagementTables/"+this.currentUserId,'')
        .subscribe(res => {
            var data = res;
            if(data['code'] == true){
              this.tablesModel=data['model'];
            }else if(data['code'] == false){
              this.rollbar.log("error",data['message']);
            }
            if(data == null){
                this.rollbar.log("error","Something went wrong when parsing SKU data!");
                this.loading = false;
            }
          
        }, error => {
            this.messageError = <any>error;
            this.service.errorserver();
            this.loading = false;
        });
      }
   }

   ddlKeyChange(selectedValue){
     if(selectedValue != null || selectedValue != undefined){
       this.selectedKey = selectedValue;
     }
   }
  
   GetChanges(){ 
    if(this.selectedKey != null){
      var url = this.baseApiUrl;
       
      if(this.selectedKey == "Certificate"){
          url = url + "GetCertificateChanges";
      }else if(this.selectedKey == "Evaluation Question"){
          url = url + "GetEvalQuestionChanges";
      }else if(this.selectedKey == "Languages"){
          url = url + "GetLanguagePackChanges";
      }

      this.service.httpClientGet(url,'')
      .subscribe(res => {
        var data=res;
       if(data['code'] == true){
          this.dataManagementModel = data['model'];
       }else if(data['code'] == false){
         this.rollbar.log("error",data['message']);       
       }      
     }, error => {
       this.messageError = <any>error;
       this.service.errorserver();
       this.loading = false;
     });
    }
   }

   rollBack(ID){
     console.log(this.selectedKey);
     if(ID != null && ID != undefined){
       this.service.httpCLientPut(this.baseApiUrl+"RollBackData/"+this.currentUserId+"/"+ID+"/"+this.selectedKey,'')
       .subscribe(res=>{
        if(res['code']==true){
          console.log(res);
          var updatedModel=this.dataManagementModel.find(x=>x.id == ID);
          console.log(updatedModel);
             var itemIndex=this.dataManagementModel.indexOf(updatedModel);
             this.loading=false;
        }
       });
     }
   }
   open(content,item) {  
    this.modalReference = this.modalService.open(content);
  }
}
