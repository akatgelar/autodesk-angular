import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateAddDesignEVAComponent } from './certificate-add-design-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const CertificateAddDesignEVARoutes: Routes = [
  {
    path: '',
    component: CertificateAddDesignEVAComponent,
    data: {
      breadcrumb: 'eva.manage_certificate.add_certificate',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificateAddDesignEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CertificateAddDesignEVAComponent],
  providers:[AppService]
})
export class CertificateAddDesignEVAModule { }
