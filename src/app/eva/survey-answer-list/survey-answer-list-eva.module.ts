import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyAnswerListEVAComponent } from './survey-answer-list-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";

import { DataFilterListSurveyPipe } from './survey-answer-list-eva.component';

export const SurveyAnswerListEVARoutes: Routes = [
  {
    path: '',
    component: SurveyAnswerListEVAComponent,
    data: {
      breadcrumb: 'List Survey',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyAnswerListEVARoutes),
    SharedModule
  ],
  declarations: [SurveyAnswerListEVAComponent, DataFilterListSurveyPipe],
  providers:[AppService]
})
export class SurveyAnswerListEVAModule { }
