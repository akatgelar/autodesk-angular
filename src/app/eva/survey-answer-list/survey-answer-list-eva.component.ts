import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../shared/service/app.service";

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({ name: 'dataFilterListSurvey' })
export class DataFilterListSurveyPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
              (row.EvaluationQuestionCode.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
              (row.CourseId.toLowerCase().indexOf(query.toLowerCase()) > -1));
        } 
        return array;
    }
}

@Component({
  selector: 'app-survey-answer-list-eva',
  templateUrl: './survey-answer-list-eva.component.html',
  styleUrls: [
    './survey-answer-list-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyAnswerListEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";

  constructor(private service:AppService) { }

  ngOnInit() {

    //get data action
    var data = '';
    this.service.httpClientGet(this._serviceUrl,data)
    .subscribe(result => {
      if(result=="Not found"){
          this.service.notfound();
          this.data = null;
      }
      else{
          this.data =result;
      }
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

  }

  //view detail
  viewdetail(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+id, data)
    .subscribe(result => {
        this.datadetail = result;
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  }

  //delete confirm
  openConfirmsSwal(id, index) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
    .then(result => {
        if (result == true) {
          var data = '';
          this.service.httpClientDelete(this._serviceUrl+'/'+id, data)
          .subscribe(result => {
            let tmpData : any = result;
              this.messageResult = tmpData;
              var resource = result;
              if(resource['code'] == '1') {
                this.service.openSuccessSwal(resource['name']);
                var index = this.data.findIndex(x => x.glossary_id == id);
                if (index !== -1) {
                    this.data.splice(index, 1);
                }
              }
              else{
                swal(
                  'Information!',
                  "Delete Data Failed",
                  'error'
                );
              }
          },
          error => {
              this.messageError = <any>error
              this.service.errorserver();
          });
        }
    }).catch(swal.noop);
  }

}
