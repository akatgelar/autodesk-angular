import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateAddBackgroundEVAComponent } from './certificate-add-background-eva.component';
import { RouterModule, Routes } from "@angular/router";

import { SharedModule } from "../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';

export const CertificateAddBackgroundEVARoutes: Routes = [
    {
        path: '',
        component: CertificateAddBackgroundEVAComponent,
        data: {
            breadcrumb: 'eva.manage_certificate.add_background.add_background',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CertificateAddBackgroundEVARoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [CertificateAddBackgroundEVAComponent]
})
export class CertificateAddBackgroundEVAModule { }