import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";
const URL = 'api/CertificateBackground/UploadFileBackground';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import swal from 'sweetalert2';
import { SessionService } from '../../shared/service/session.service';
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';

@Component({
    selector: 'app-certificate-add-background-eva',
    templateUrl: './certificate-add-background-eva.component.html',
    styleUrls: [
        './certificate-add-background-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})

export class CertificateAddBackgroundEVAComponent implements OnInit {
    uploader: FileUploader;
    hasBaseDropZoneOver = false;
    hasAnotherDropZoneOver = false;
    public loading = false;
    private useraccesdata: any;

    constructor(public session: SessionService,public http: Http, private router: Router) {
    
    }

    ngOnInit() {
        this.useraccesdata=this.session.getData();
		const token:string = this.session.getToken();

        this.uploader = new FileUploader({
            url: URL,
            isHTML5: true,
            headers:[{ name: 'Authorization', value: `Bearer ${token}` }]
        });
        this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
        this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);

    }

    onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        this.loading = true;
        console.log("cek status success" + response);
        //let data = JSON.parse(response); //success server response
        //console.log(data);
        this.openSuccessSwalSave()

        this.router.navigate(['/certificate/certificate-list-background']);
        this.loading = false;
    }

    onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        this.openFailedSwalSave()
        this.router.navigate(['/certificate/certificate-list-background']);
    }

    openSuccessSwalSave() {
        swal({
            title: 'Success!',
            text: 'The Background has been add!',
            type: 'success'
        }).catch(swal.noop);
    }

    openFailedSwalSave() {
        swal({
            title: 'Information!',
            text: 'The Background fail to add!',
            type: 'error'
        }).catch(swal.noop);
    }

    fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    /*
    fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
    }
    */

}
