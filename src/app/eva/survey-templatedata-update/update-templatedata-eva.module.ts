import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateTemplateDataEVAComponent } from './update-templatedata-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const UpdateTemplateDataEVARoutes: Routes = [
  {
    path: '',
    component: UpdateTemplateDataEVAComponent,
    data: {
      breadcrumb: 'eva.manage_evaluation.edit_evaluation_template_data',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateTemplateDataEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [UpdateTemplateDataEVAComponent],
  providers: [AppService, AppFormatDate]
})
export class UpdateTemplateDataEVAModule { }
