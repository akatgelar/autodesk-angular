import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '../../shared/service/session.service';

var surveyJSON = {
  "pages": [
    {
      "name": "page1"
    }
  ]
}


@Component({
  selector: 'app-update-templatedata-eva',
  templateUrl: './update-templatedata-eva.component.html',
  styleUrls: [
    './update-templatedata-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class UpdateTemplateDataEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  public data: any;
  public datadetail: any;
  edittemplatedata: FormGroup;
  id: string;
  public year;
  private dropdownCertificate;
  private dropdownPartner;
  private certificateType;
  public CertificateTypeStat = false;
  public kolom = true;
  private partnerSelected;
  private useraccessdata;
  public loading = false;

  constructor(private router: Router, private service: AppService, private session: SessionService,
    private formatdate: AppFormatDate, private http: HttpClient, private route: ActivatedRoute, private datePipe: DatePipe) {

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);

    let PartnerType = new FormControl('', Validators.required);
    let certificatetype = new FormControl('', Validators.required);
    let EvaluationQuestionCode = new FormControl('', Validators.required);
    let CourseId = new FormControl('', Validators.required);
    let Year = new FormControl('', Validators.required);

    this.edittemplatedata = new FormGroup({
      PartnerType: PartnerType,
      certificatetype: certificatetype,
      EvaluationQuestionCode: EvaluationQuestionCode,
      CourseId: CourseId,
      Year: Year
    });

  }

  getCertificateType() {
    var certificate: any;
    var parent = "AAPCertificateType";
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
      .subscribe(result => {
        this.dropdownCertificate = result;
      }, error => {
        this.service.errorserver();
      });
  }

  getPartnerType() {
    var partner = ['1', '58'];
    var partnerTemp: any;
    this.dropdownPartner = [];
    for (let i = 0; i < partner.length; i++) {
      this.service.httpClientGet("api/Roles/" + partner[i], partnerTemp)
        .subscribe(result => {
          partnerTemp = result;
          this.dropdownPartner.push(partnerTemp);
        }, error => {
          this.service.errorserver();
        });
    }
  }

  selectedPartner(newvalue) {
    this.partnerSelected = newvalue;
    if (newvalue == 58) {
      var certificate: any;
      var parent = "AAPCertificateType";
      this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
        .subscribe(result => {
          this.dropdownCertificate = result;
        }, error => {
          this.service.errorserver();
        });
      this.CertificateTypeStat = true;
      this.kolom = false;
    } else {
      this.CertificateTypeStat = false;
      this.kolom = true;
    }
  }

  getFYIndicator() {
    var parent = "FYIndicator";
    var data = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
      .subscribe(res => {
        this.year = res;
      }, error => {
        this.service.errorserver();
      });
  }

  ngOnInit() {
    this.getPartnerType();
    this.getCertificateType();
    this.getFYIndicator();
    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
      .subscribe(result => {
        // data = result.replace(/\r\n/g, " ");
        // data = data.replace(/\n/g, " ");
        // data = JSON.parse(data);
        let tmpData : any = result;
        if (tmpData == "Not found") {
          this.datadetail = '';
        }
        else {
          this.datadetail = tmpData;
          // console.log(this.datadetail);
          this.selectedPartner(this.datadetail.PartnerType);
          this.edittemplatedata.patchValue({ PartnerType: this.datadetail.PartnerType });
          this.datadetail.CertificateTypeId != 0 ? this.edittemplatedata.patchValue({ certificatetype: this.datadetail.CertificateTypeId }) : this.CertificateTypeStat = false;
          this.edittemplatedata.patchValue({ EvaluationQuestionCode: this.datadetail.EvaluationQuestionCode });
          this.edittemplatedata.patchValue({ CourseId: this.datadetail.CourseId });
          this.edittemplatedata.patchValue({ Year: this.datadetail.Year });
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
        });

  }

  resetFormUpdate() {
    this.edittemplatedata.reset({
      'EvaluationQuestionCode': this.datadetail.EvaluationQuestionCode,
      'CourseId': this.datadetail.CourseId,
      'Year': this.datadetail.Year,
      'CertificateType': this.datadetail.CertificateTypeId
    });
  }

  onSubmitEditSurveyTemplate() {

    this.edittemplatedata.controls['PartnerType'].markAsTouched();
    if (this.edittemplatedata.value.PartnerType == 1) {
      this.edittemplatedata.removeControl("certificatetype");
      this.certificateType = 0;
    } else {
      this.edittemplatedata.controls['certificatetype'].markAsTouched();
      this.certificateType = this.edittemplatedata.value.certificatetype;
    }
    this.edittemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.edittemplatedata.controls['CourseId'].markAsTouched();
    this.edittemplatedata.controls['Year'].markAsTouched();

    if (this.edittemplatedata.valid) {
      swal({
        title: 'Are you sure?',
        text: "If you wish to Update the record!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          this.edittemplatedata.value.CountryCode = "SG";
          this.edittemplatedata.value.EvaluationQuestionTemplate = surveyJSON;
          this.edittemplatedata.value.EvaluationQuestionJson = surveyJSON;
          // this.edittemplatedata.value.CreatedDate = this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"); /* issue 14112018 - Couldn’t add any records */
          // this.edittemplatedata.value.CreatedBy = this.useraccessdata.ContactName;
    
          var dataToPut = {
            'EvaluationQuestionCode': this.edittemplatedata.value.EvaluationQuestionCode,
            'CertificateTypeId': this.certificateType,
            'PartnerType': this.edittemplatedata.value.PartnerType,
            'CourseId': this.edittemplatedata.value.CourseId,
            // 'CountryCode': this.edittemplatedata.value.CountryCode,
            // 'EvaluationQuestionTemplate': this.edittemplatedata.value.EvaluationQuestionTemplate,
            // 'EvaluationQuestionJson': this.edittemplatedata.value.EvaluationQuestionJson,
            // 'CreatedBy': this.edittemplatedata.value.CreatedBy,
            // 'CreatedDate': this.edittemplatedata.value.CreatedDate,
            'Year': this.edittemplatedata.value.Year,
            'cuid' : this.useraccessdata.ContactName,
            'UserId': this.useraccessdata.UserId
          };
    
          let data = JSON.stringify(dataToPut);
    
          this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
            .subscribe(res=>{
              console.log(res)
            });

          setTimeout(() => {
            this.router.navigate(['/evaluation/survey-template-list']);
            this.loading = false;
          }, 1000)
        }
      }).catch(swal.noop);
    }
    else {
      swal(
        'Field is Required!',
        'Please enter form is required :)',
        'error'
      )
    }
  }
}
