import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateStudentInformationEVAComponent } from './update-student-information-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from 'app/shared/service/app.service';
import { LoadingModule } from 'ngx-loading';
import { DataFilterStudentPipe } from './update-student-information-eva.component';
import { SessionService } from '../../shared/service/session.service';
import { Ng2CompleterModule } from "ng2-completer";

export const UpdateStudentInformationEVARoutes: Routes = [
  {
    path: '',
    component: UpdateStudentInformationEVAComponent,
    data: {
      breadcrumb: 'eva.manage_student_info.edit_student_information',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateStudentInformationEVARoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [UpdateStudentInformationEVAComponent,DataFilterStudentPipe],
  providers: [AppService]
})
export class UpdateStudentInformationEVAModule { }
