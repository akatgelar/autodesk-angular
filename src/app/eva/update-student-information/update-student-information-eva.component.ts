import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import swal from 'sweetalert2';
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";;
import { AppService } from 'app/shared/service/app.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import * as _ from "lodash";
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
@Pipe({ name: 'dataFilterStudent' })
export class DataFilterStudentPipe {
  constructor(private service: AppService) {}
    transform(array: any[], query: string): any {
        if (query) {

            return _.filter(array, row =>
                (row.StudentID.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Firstname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Company.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.City.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.countries_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Phone.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Email.toLowerCase().indexOf(query.toLowerCase()) > -1));

        }
        return array;
    }
}
@Component({
  selector: 'app-update-student-information-eva',
  templateUrl: './update-student-information-eva.component.html',
  styleUrls: [
    './update-student-information-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class UpdateStudentInformationEVAComponent implements OnInit {

  private _serviceUrl = 'api/Student';
  private _serviceUrlCountry = 'api/Countries';
  messageError: string = '';
  updatestudent: FormGroup;
  id_student: string;
  public data: any;
  public country: any;
  public loading = false;
  public rowsOnPage: number = 10;
  useraccesdata:any;

  constructor(private service: AppService, private session:SessionService, private route: ActivatedRoute, private router: Router) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let StudentID = new FormControl();
    let Firstname = new FormControl('', Validators.required);
    let Lastname = new FormControl('', Validators.required);
    let Email = new FormControl('', Validators.required);
    let ConfirmEmail = new FormControl('', Validators.required);
    let Phone = new FormControl();
    let CountryID = new FormControl('', Validators.required);
    let StateID = new FormControl();
    let PostalCode = new FormControl();
    let Address = new FormControl();
    let Address2 = new FormControl();
    let City = new FormControl();
    let Company = new FormControl();

    this.updatestudent = new FormGroup({
      StudentID: StudentID,
      Firstname: Firstname,
      Lastname: Lastname,
      Email: Email,
      ConfirmEmail: Email,
      Phone: Phone,
      CountryID: CountryID,
      StateID: StateID,
      PostalCode: PostalCode,
      Address: Address,
      Address2: Address2,
      City: City,
      Company: Company
    });

  }
  accesUpdateStudentInfoBtn:Boolean=true;
  ngOnInit() {
    //Get data from id, then pass them to form update
    this.id_student = this.route.snapshot.params['id'];
    this.accesUpdateStudentInfoBtn = true;
    var data = '';
      

   
    this.service.httpClientGet(this._serviceUrl + "/GetStudent/" + this.id_student, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = null;
        }
        else {
          this.data = result;
          console.log(this.data)
          if (this.data.Firstname != null && this.data.Firstname != '') {
            this.data.Firstname = this.service.decoder(this.data.Firstname);
        }
        if (this.data.Lastname != null && this.data.Lastname != '') {
          this.data.Lastname = this.service.decoder(this.data.Lastname);
      }
          this.buildForm();
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
   
    //Get data from countries
    //var data = '';
    this.service.httpClientGet(this._serviceUrlCountry, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.country = null;
        }
        else {
          this.country = result;
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
      });
    //  var firstname =this.data.Firstname;
      this.getAllStudent(this.id_student);
  }

  getAllStudent(data) {
    //get data student
  this.loading = true;
  this.service.httpClientGet('api/Student/SelectByID/' + data, '')
        .subscribe(result => {
            var finalresult = result;
            if (finalresult == "Not found") {
                this.service.notfound();
                this.data = null;
                this.loading = false;
            }
            else {
                this.data = finalresult;
                 for (var i = 0; i < this.data.length; i++) {
                    if (this.data[i].Firstname != null && this.data[i].Firstname != '') {
                        this.data[i].Firstname = this.service.decoder(this.data[i].Firstname);
                    }
                    if (this.data[i].Lastname != null && this.data[i].Lastname != '') {
                      this.data[i].Lastname = this.service.decoder(this.data[i].Lastname);
                  }
                  
                  }
                this.loading = false;
            }
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
            this.loading = false;
        });
}

Update() {
  this.loading = true;
  var data = {
    "StudentID": this.id_student
  }
 
  //alert("Message");

  /* autodesk plan 10 oct */

  this.service.httpClientGet('api/Student/UpdatebyId/' +  this.id_student, '')
    .subscribe(result => {
      var res = result;
      if (res['code'] == "1") {
        swal(
          'Information!',
          res['message'],
          'success'
        );
        this.loading = false;
      } else {
        console.log("failed");
        this.loading = false;
      }
    }, error => {
      console.log("failed");
      this.loading = false;
      
    });

  // this.service.httpCLientPut('api/Auth/GeneratePassPopUpStudent','' ,data);
  // this.loading = false;

  /* end line autodesk plan 10 oct */
}

ketikaSiswaDipilih(value){
  this.data = '';
  this.data = [{
      StudentID : value["originalObject"].StudentID,
      Firstname : value["originalObject"].Firstname,
      Lastname : value["originalObject"].Lastname,
      Email : value["originalObject"].Email
  }];

  //Buat object untuk filter yang dipilih
  var params =
  {
      result: value
  }
  //Masukan object filter ke local storage
  localStorage.setItem("filter", JSON.stringify(params));

}
  generatePasswordPopUp() {
    this.loading = true;
    var data = {
      "StudentID": this.id_student,
      Userid : this.useraccesdata.Userid,
      cuid : this.useraccesdata.ContactName
    }

    /* autodesk plan 10 oct */

    this.service.httpCLientPut('api/Auth/GeneratePassPopUpStudent', data)
      .subscribe(result => {
        var res = result;
        if (res['code'] == "1") {
          swal(
            'Information!',
            res['message'],
            'success'
          );
          this.loading = false;
        } else {
          console.log("failed");
          this.loading = false;
        }
      }, error => {
        console.log("failed");
        this.loading = false;
      });

    // this.service.httpCLientPut('api/Auth/GeneratePassPopUpStudent','' ,data);
    // this.loading = false;

    /* end line autodesk plan 10 oct */
  }
  onSubmit() {
    let format = /[!$%^&*+\-=\[\]{};:\\|.<>\/?]/
    if(format.test(this.updatestudent.value.Firstname) || format.test(this.updatestudent.value.Lastname))
        return swal('ERROR','Special character not allowed in Name','error')
    this.updatestudent.controls['Firstname'].markAsTouched();
    this.updatestudent.controls['Lastname'].markAsTouched();
    this.updatestudent.controls['Email'].markAsTouched();
    this.updatestudent.controls['ConfirmEmail'].markAsTouched();
    this.updatestudent.controls['CountryID'].markAsTouched();
    this.updatestudent.controls['Company'].markAsTouched();
    this.updatestudent.controls['City'].markAsTouched();
    this.updatestudent.controls['Phone'].markAsTouched();

    if (this.updatestudent.valid) {
      swal({
        title: 'Are you sure?',
        text: "If you wish to Update the Student!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;

          this.updatestudent.value.cuid = this.useraccesdata.ContactName;
          this.updatestudent.value.UserId = this.useraccesdata.UserId;
          this.updatestudent.value.Firstname = this.service.encoder(this.updatestudent.value.Firstname);
          this.updatestudent.value.Lastname = this.service.encoder(this.updatestudent.value.Lastname);
          let data = JSON.stringify(this.updatestudent.value);
          let id = JSON.parse(data).StudentID;
          this.service.httpCLientPut(this._serviceUrl + "/" + id, data)
            .subscribe(result => {
              var resource = result;
              if (resource['code'] != '1') {
                swal(
                  'Information!',
                  "Update Data Failed",
                  'error'
                );
              }
              setTimeout(() => {
                this.router.navigate(['/studentinfo/search-student/']);
                this.loading = false;                
              }, 2000);
            },
              error => {
                this.messageError = <any>error
                this.service.errorserver();
                this.loading = false;
              });

        // this.service.httpCLientPut(this._serviceUrl,id, data);
        // setTimeout(() => {
        //   this.router.navigate(['/studentinfo/search-student/']);
        //   this.loading = false;                
        // }, 2000);
        }
      }).catch(swal.noop);
    }
    else {
      swal(
        'Field is Required!',
        'Please enter form is required :)',
        'error'
      )
    }
  }

  buildForm(): void {
    let StudentID = new FormControl(this.data.StudentID);
    let Firstname = new FormControl(this.data.Firstname, Validators.required);
    let Lastname = new FormControl(this.data.Lastname, Validators.required);
    let Email = new FormControl(this.data.Email, [Validators.required, Validators.email]);
    let ConfirmEmail = new FormControl(this.data.ConfirmEmail, [Validators.required, Validators.email]);
    let Phone = new FormControl(this.data.Phone);
    let CountryID = new FormControl(this.data.CountryID, Validators.required);
    let StateID = new FormControl(this.data.StateID);
    let PostalCode = new FormControl(this.data.PostalCode);
    let Address = new FormControl(this.data.Address);
    let Address2 = new FormControl(this.data.Address2);
    let City = new FormControl(this.data.City);
    let Company = new FormControl(this.data.Company);
    let Status = new FormControl(this.data.Status);
    let CourseCount = new FormControl(this.data.CourseCount);
    this.updatestudent = new FormGroup({
      StudentID: StudentID,
      Firstname: Firstname,
      Lastname: Lastname,
      Email: Email,
      ConfirmEmail: Email,
      Phone: Phone,
      CountryID: CountryID,
      StateID: StateID,
      PostalCode: PostalCode,
      Address: Address,
      Address2: Address2,
      City: City,
      Company: Company,
      Status :Status,
      CourseCount : CourseCount
    });
  }

  resetForm() {
    this.updatestudent.reset({
      'Firstname': this.data.Firstname,
      'Lastname': this.data.Lastname,
      'Email': this.data.Email,
      'CountryID': this.data.CountryID,
      'Company': this.data.Company,
      'City': this.data.City,
      'Phone': this.data.Phone
    });
  }
}
