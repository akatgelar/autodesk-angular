import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { StudentSearchEVAComponent } from './student-search-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';
import { ConvertDatePipe } from './student-search-eva.component';
import { Ng2CompleterModule } from "ng2-completer";
import { Ng2PaginationModule } from 'ng2-pagination';
export const StudentSearchEVARoutes: Routes = [
    {
        path: '',
        component: StudentSearchEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.student_search.student_search',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(StudentSearchEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule,
        Ng2CompleterModule,
        Ng2PaginationModule
    ],
    declarations: [StudentSearchEVAComponent, ConvertDatePipe],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class StudentSearchEVAModule { }