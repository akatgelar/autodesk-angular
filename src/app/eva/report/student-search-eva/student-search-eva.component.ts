import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
import { DatePipe } from '@angular/common';
import { Pipe } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }

    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
    }
}

@Component({
    selector: 'app-student-search-eva',
    templateUrl: './student-search-eva.component.html',
    styleUrls: [
        './student-search-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class StudentSearchEVAComponent implements OnInit {

    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};
    protected contactId: string = "";
    public data: any = '';
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    year;
    selectedValueDate;
    date;
    queryForm: FormGroup;
    public loading = false;
    dataresult = [];
    certificateType = [];
    eventType = [];
    listKolomCourse = [];
    listKolomStudent = [];
    cokelat = [];
    public showEvent = false;
    useraccesdata: any;
    questionStruct: any;
    protected dataService1: RemoteData;
    private ContactName: string;
    siteidDipilih: string = "";
    TotalEvaluation: any;
    Territory : any;
    report_temp = [];
    report = [];
    excel = [];
    expexcel = [];
    fileName = "StudentSearch.xls";
    constructor(private router: Router, public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService,
        private completerService: CompleterService) {

        let filterByFirstName = new FormControl('');
        let filterByLastName = new FormControl('');
        let filterBySiteId = new FormControl('');
        let filterByYear = new FormControl(new Date().getFullYear());
        let filterByDate = new FormControl(1);

        this.queryForm = new FormGroup({
            filterByFirstName: filterByFirstName,
            filterByLastName: filterByLastName,
            filterBySiteId: filterBySiteId,
            filterByYear: filterByYear,
            filterByDate: filterByDate
        });

        this.date = [
            { id: "", name: "Annual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];
        
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
        this.questionStruct = [
            {
                QuestionNumber: "QP1",
                Question: "What level are you going to teach?"
            },
            {
                QuestionNumber: "QP2",
                Question: "What is going to be the primary software used?"
            },
            {
                QuestionNumber: "QP3",
                Question: "How many hours of training will be delivered?"
            },
            {
                QuestionNumber: "QP4",
                Question: "What will be the training format (Check all that apply)?"
            },
            {
                QuestionNumber: "QP5",
                Question: "What training materials will you use in this class (Check all that apply)?"
            },
            {
                QuestionNumber: "Q1",
                Question: ""
            },
            {
                QuestionNumber: "Q1.1",
                Question: "I would like to receive my electronic Certificate of Completion at the e-mail address stated below. (Your full name is needed below)"
            },
            {
                QuestionNumber: "Q1.2",
                Question: "I would like to receive email from Autodesk, including information about new products and special promotions."
            },
            {
                QuestionNumber: "Q1.3",
                Question: "I agree the information provided in this form may be forwarded by Autodesk to the Authorized Education Partners and may be used by the Authorized Education Partners for internal quality monitoring purposes."
            },
            {
                QuestionNumber: "Q1.4",
                Question: "I would like to receive email from Autodesk, including information about new products and special promotions."
            },
            {
                QuestionNumber: "Q2",
                Question: ""
            },
            {
                QuestionNumber: "Q2.1",
                Question: "Overall experience"
            },
            {
                QuestionNumber: "Q2.2",
                Question: "Computer equipment"
            },
            {
                QuestionNumber: "Q2.3",
                Question: "Training Facility"
            },
            {
                QuestionNumber: "Q2.4",
                Question: "Online Training Facility"
            },
            {
                QuestionNumber: "Q2.5",
                Question: "Instructor"
            },
            {
                QuestionNumber: "Q2.6",
                Question: "CourseWare"
            },
            {
                QuestionNumber: "Q3",
                Question: "How likely or unlikely are you to recommend the following to a friend or colleague?"
            },
            {
                QuestionNumber: "Q3.1",
                Question: "This course"
            },
            {
                QuestionNumber: "Q3.2",
                Question: "This instructor"
            },
            {
                QuestionNumber: "Q3.3",
                Question: "The training facility"
            },
            {
                QuestionNumber: "Q4",
                Question: "How likely or unlikely are you to continue learning Autodesk software?"
            },
            {
                QuestionNumber: "Q5",
                Question: "What do you intend to use this training for?"
            },
            {
                QuestionNumber: "Q6",
                Question: "Which of the following best describes you?"
            },
            {
                QuestionNumber: "Q7",
                Question: "How much experience do you have with this Autodesk product?"
            },
            {
                QuestionNumber: "Q8",
                Question: "Do you plan to obtain an Autodesk Certification for the software learned in this course?"
            },
            {
                QuestionNumber: "Q8.1",
                Question: ""
            },
            {
                QuestionNumber: "Q8.2",
                Question: ""
            },
            {
                QuestionNumber: "Q8.3",
                Question: ""
            },
            {
                QuestionNumber: "Q8.4",
                Question: ""
            },
            {
                QuestionNumber: "Q9",
                Question: "To what extent do you agree or disagree with the following statements?"
            },
            {
                QuestionNumber: "Q9.1",
                Question: "I learned new knowledge and skills"
            },
            {
                QuestionNumber: "Q9.2",
                Question: "I will be able to apply the new skills I learned"
            },
            {
                QuestionNumber: "Q9.3",
                Question: "The new skills I learned will improve my performance"
            },
            {
                QuestionNumber: "Q9.4",
                Question: "I'm more likely to recommend Autodesk products as a result of this course"
            },
            {
                QuestionNumber: "Q10",
                Question: "Which of the following categories best describes your organization’s primary industry?"
            },
            {
                QuestionNumber: "Q11",
                Question: "Comments"
            },
            {
                QuestionNumber: "Q11.1",
                Question: ""
            },
            {
                QuestionNumber: "Q11.2",
                Question: ""
            },
            {
                QuestionNumber: "Q11.3",
                Question: ""
            },
            {
                QuestionNumber: "Q11.4",
                Question: ""
            },
            {
                QuestionNumber: "Q11.5",
                Question: ""
            },
            {
                QuestionNumber: "CourseFacility",
                Question: ""
            },
            {
                QuestionNumber: "ComputerEquipment",
                Question: ""
            },
        ];
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
          if (userlvlarr[i] == "DISTRIBUTOR") {
            this.distributorya = true;
          }
          else {
            this.distributorya = false;
          }
        }
      
    }
 
     urlGetOrgId(): string {
         var orgarr = this.useraccesdata.OrgId.split(',');
         var orgnew = [];
         for (var i = 0; i < orgarr.length; i++) {
             orgnew.push('"' + orgarr[i] + '"');
         }
         return orgnew.toString();
     }
 
     urlGetSiteId(): string {
         var sitearr = this.useraccesdata.SiteId.split(',');
         var sitenew = [];
         for (var i = 0; i < sitearr.length; i++) {
             sitenew.push('"' + sitearr[i] + '"');
         }
         return sitenew.toString();
     }
 
     /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    selectedScore(score) {
        var choosen: number = 0;
        switch (score) {
            case 100:
                choosen = 5;
                break;
            case 75:
                choosen = 4;
                break;
            case 50:
                choosen = 3;
                break;
            case 25:
                choosen = 2;
                break;
            default:
                choosen = 1;
                break;
        }
        return choosen;
    }

    question8(val) {
        var choosen: string = "";
        switch (val) {
            case "1":
                choosen = "Architecture, Engineering & Construction";
                break;
            case "2":
                choosen = "Automotive & Transportation";
                break;
            case "3":
                choosen = "Government";
                break;
            case "4":
                choosen = "Infrastructure and Civil Engineering";
                break;
            case "5":
                choosen = "Manufacturing";
                break;
            case "6":
                choosen = "Media & Entertainment";
                break;
            case "7":
                choosen = "Utilities & Telecommunications";
                break;

            default:
                choosen = "Other: ";
                break;
        }
        return choosen;
    }

    CourseTeachingLvl(update, essential, intermediate, advanced, custom, other, comment) {
        let arr_ctl = [update, essential, intermediate, advanced, custom, other];
        var index = arr_ctl.indexOf("1");
        var ctl = "";
        switch (index) {
            case 0:
                ctl = "Update"
                break;
            case 1:
                ctl = "Level 1 : Essentials";
                break;
            case 2:
                ctl = "Level 2 : Intermediate";
                break;
            case 3:
                ctl = "Level 3 : Advanced";
                break;
            case 4:
                ctl = "Customized";
                break;

            default:
                ctl = "Other : " + comment;
                break;
        }

        return ctl;
    }

    CourseTeachingMaterial(autodesk, aap, atc, independent, independent_online, atc_online, other, comment) {
        let arr_ctm = [autodesk, aap, atc, independent, independent_online, atc_online, other];
        var index = arr_ctm.indexOf("1");
        var ctm = "";
        switch (index) {
            case 0:
                ctm = "Autodesk Official Courseware (any Autodesk branded material)";
                break;
            case 1:
                ctm = "Autodesk Authors and Publishers (AAP) Program Courseware";
                break;
            case 2:
                ctm = "Course material developed by the ATC";
                break;
            case 3:
                ctm = "Course material developed by an independent vendor or instructor";
                break;
            case 4:
                ctm = "Online e-learning course material developed by an independent vendor or instructor";
                break;
            case 5:
                ctm = "Online e-learning course material developed by the ATC";
                break;

            default:
                ctm = "Other : " + comment;
                break;
        }

        return ctm;
    }
getDetailData(event){
    this.loading = true;
    this.data = null;
    this.cokelat = [];
    var limit = 10;
    var ofset = (event - 1) * 10;
    var endpage = Math.floor(this.countdata / 10) + 1;
    this.report_temp = this.report;
    if (event == 1) {
        ofset = 0;
    } else if (event == endpage) {
        limit = this.countdata % 10;
    }

    let geoarr = [];
    for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
    }

    let regionarr = [];
    for (var i = 0; i < this.selectedItemsRegion.length; i++) {
        regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
    }

    let subregionarr = [];
    for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
        subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
    }

    let countriesarr = [];
    for (var i = 0; i < this.selectedItemsCountry.length; i++) {
        countriesarr.push("'" + this.selectedItemsCountry[i].id + "'");
    }

    this.partnertypearr = [];
    for (var i = 0; i < this.selectedPartner.length; i++) {
        this.partnertypearr.push(this.selectedPartner[i].id);
    }

    this.certificateType = [];
    for (var i = 0; i < this.selectedCertificate.length; i++) {
        this.certificateType.push(this.selectedCertificate[i].id);
    }

    this.eventType = [];
    if (this.selectedEventType.length > 0) {
        for (let i = 0; i < this.selectedEventType.length; i++) {
            this.eventType.push('"' + this.selectedEventType[i].id + '"');
        }
    }

    this.queryForm.value.filterBySiteId = this.siteidDipilih;

    var params =
        {
            firstNameStud: this.queryForm.value.filterByFirstName,
            lastNameStud: this.queryForm.value.filterByLastName,
            siteIdStud: this.queryForm.value.filterBySiteId,
            yearStud: this.queryForm.value.filterByYear,
            dateStud: this.queryForm.value.filterByDate,
            selectGeoStud: this.selectedItemsGeo,
            dropGeoStud: this.dropdownListGeo,
            selectRegionStud: this.selectedItemsRegion,
            dropRegionStud: this.dropdownListRegion,
            selectSubRegionStud: this.selectedItemsSubRegion,
            dropSubRegionStud: this.dropdownListSubRegion,
            selectCountry: this.selectedItemsCountry,
            dropCountry: this.dropdownListCountry,
            selectPartnerStud: this.selectedPartner,
            dropPartnerStud: this.dropdownPartner,
            selectCertiStud: this.selectedCertificate,
            dropCertiStud: this.dropdownCertificate
        }

    //Masukan object filter ke local storage
    localStorage.setItem("filter", JSON.stringify(params));

    var contactid = "";
    var orgid = "";
    var distributor = "";
    var siteid = "";

    /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

    if (this.adminya) {
        if(this.distributorya){
            distributor = this.urlGetOrgId();
        }else{
            if(this.orgya){
                orgid = this.urlGetOrgId();
            }else{
                if(this.siteya){
                    siteid = this.urlGetSiteId();
                }else{
                    contactid = this.useraccesdata.UserId;
                }
            }
        }
    }
    
    /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

    var keyword = {
        filterByFirstName: this.queryForm.value.filterByFirstName,
        filterByLastName: this.queryForm.value.filterByLastName,
        filterBySiteId: this.queryForm.value.filterBySiteId,
        filterByGeo: geoarr.toString(),
        filterByRegion: regionarr.toString(),
        filterBySubRegion: subregionarr.toString(),
        filterByCertificateType: this.certificateType.toString(),
        filterBySurveyYear: this.queryForm.value.filterByYear,
        filterByDate: this.queryForm.value.filterByDate,
        filterByPartnerType: this.partnertypearr.toString(),
        filterByCountries: countriesarr.toString(),
        EventType: this.eventType.toString(),
        studentStatus: 'A',
        ContactId: contactid,
        OrgId: orgid,
        Distributor: distributor,
        SiteId: siteid,
        limit:limit,
        ofset:ofset
    };

    var data: any;
    var course: any;
    var student: any;

    /* autodesk plan 10 oct */
    this.service.httpClientPost("api/ReportEva/SearchingStudent", keyword)
        .subscribe(res1 => {
            if(res1 != null && Object.keys(res1).length != 0){
                data = res1;
                if(data.length > 0){
                    let courseTemp = [];
                    let studentTemp = [];
                    for(let n = 0; n < data.length; n++){
                        courseTemp.push('"' + data[n].CourseId + '"');
                        studentTemp.push(data[n].StudentId);
                    }
                    keyword["CourseID"] = courseTemp.toString();
                    keyword["StudentID"] = studentTemp.toString();
                    this.service.httpClientPost("api/ReportEva/CourseSetting", keyword)
                        .subscribe(res2 => {
                            course = res2;
                            if(res2 != null && course.length > 0){
                                this.listKolomCourse = [];
                                for (let key in course[0]) {
                                    this.listKolomCourse.push(key);
                                }
                                for(let i = 0; i < data.length; i++){
                                    for(let j = 0; j < course.length; j++){
                                        if(data[i].CourseId == course[j].CourseId){
                                            for(let m = 0; m < this.listKolomCourse.length; m++){
                                                let hmm = this.listKolomCourse[m];
                                                data[i][hmm] = course[j][hmm];
                                            }
                                        }
                                    }
                                }

                                this.service.httpClientPost("api/ReportEva/GetStudentData", keyword)
                                    .subscribe(res3 => {
                                        student = res3;
                                        if(res3 != null && student.length > 0){
                                            for (let key in student[0]) {
                                                this.listKolomStudent.push(key);
                                            }
                                           
                                            for(let a = 0; a < data.length; a++){
                                                for(let b = 0; b < student.length; b++){
                                                    if(data[a].StudentId == student[b].StudentId){
                                                        if (student[i].FirstName != null && student[i].FirstName != '') {
                                                            student[i].FirstName = this.service.decoder(student[i].FirstName);
                                                        }
                                                        if (student[i].LastName != null && student[i].LastName != '') {
                                                            student[i].LastName = this.service.decoder(student[i].LastName);
                                                        }
                                                        for(let z = 0; z < this.listKolomStudent.length; z++){
                                                            let hmm2 = this.listKolomStudent[z];
                                                            data[a][hmm2] = student[b][hmm2];
                                                        }
                                                    }
                                                }
                                            }

                                            // for (let k = 0; k < data.length; k++) {
                                            //     let answer = data[k].EvaluationAnswerJson;
                                            //     if ((typeof answer === 'object' && answer.constructor === Object ) || answer == '') {
                                            //         data[k]["QP1"] = this.CourseTeachingLvl(data[k].Update, data[k].Essentials, data[k].Intermediate, data[k].Advanced, data[k].Customized, data[k].OtherCTL, data[k].CommentCTL);
                                            //         data[k]["QP2"] = data[k].primaryProduct;
                                            //         data[k]["QP3"] = data[k].HoursTraining;
                                            //         data[k]["QP4"] = (data[k].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                                            //         data[k]["QP5"] = this.CourseTeachingMaterial(data[k].Autodesk, data[k].AAP, data[k].ATC, data[k].Independent, data[k].IndependentOnline, data[k].ATCOnline, data[k].OtherCTM, data[k].CommentCTM);
                                            //         data[k]["Q1.1"] = "";
                                            //         data[k]["Q1.2"] = "";
                                            //         data[k]["Q1.3"] = "";
                                            //         data[k]["Q1.4"] = "";
                                            //         data[k]["Q2"] = "";
                                            //         data[k]["Q2.1"] = "";
                                            //         data[k]["Q2.2"] = "";
                                            //         data[k]["Q2.3"] = "";
                                            //         data[k]["Q2.4"] = "";
                                            //         data[k]["Q2.5"] = "";
                                            //         data[k]["Q2.6"] = "";
                                            //         data[k]["Q3"] = "";
                                            //         data[k]["Q3.1"] = "";
                                            //         data[k]["Q3.2"] = "";
                                            //         data[k]["Q3.3"] = "";
                                            //         data[k]["Q4"] = "";
                                            //         data[k]["Q5"] = "";
                                            //         data[k]["Q6"] = "";
                                            //         data[k]["Q7"] = "";
                                            //         data[k]["Q8"] = "";
                                            //         data[k]["Q8.1"] = "";
                                            //         data[k]["Q8.2"] = "";
                                            //         data[k]["Q8.3"] = "";
                                            //         data[k]["Q8.4"] = "";
                                            //         data[k]["Q9"] = "";
                                            //         data[k]["Q9.1"] = "";
                                            //         data[k]["Q9.2"] = "";
                                            //         data[k]["Q9.3"] = "";
                                            //         data[k]["Q9.4"] = "";
                                            //         data[k]["Q10"] = "";
                                            //         data[k]["Q11"] = "";
                                            //         data[k]["Q11.1"] = "";
                                            //         data[k]["Q11.2"] = "";
                                            //         data[k]["Q11.3"] = "";
                                            //         data[k]["Q11.4"] = "";
                                            //         data[k]["Q11.5"] = "";
                                            //         data[k]["CourseFacility"] = (data[k].ATCFacility == '0') ? "ATC Facility" : "Onsite";
                                            //         data[k]["ComputerEquipment"] = (data[k].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                                            //     } else {
                                            //         if (answer.indexOf('\\r\\n') > -1) {
                                            //             answer = answer.replace(/\\r\\n/g, "");
                                            //             answer = answer.replace(/  /g, "");
                                            //             answer = answer.replace(/    /g, "");
                                            //         }else if (answer.indexOf('\\n') > -1) {
                                            //             answer = answer.replace(/\\n\\t\\t/g, "");
                                            //             answer = answer.replace(/\\n\\t/g, "");
                                            //             answer = answer.replace(/\\n/g, "");
                                            //             answer = answer.replace(/  /g, "");
                                            //             answer = answer.replace(/    /g, "");
                                            //         }
                                            //         answer = answer.replace(/\\/g, "");
                                            //         //cara untuk ngambil bagian komentarnya aja
                                            //         let index_1;
                                            //         let prev_comm;
                                                    
                                            //         //ambil comentar nya saja
                                            //         if (answer.indexOf('"comment": "') > -1) {
                                            //             index_1 = answer.indexOf('"comment": "'); 
                                            //             prev_comm = answer.substr(index_1 + 12, answer.length); 
                                            //         }

                                            //         if (answer.indexOf('"comment":"') > -1) {
                                            //             index_1 = answer.indexOf('"comment":"'); 
                                            //             prev_comm = answer.substr(index_1 + 11, answer.length); 
                                            //         }

                                            //         if (answer.indexOf('"Comments": "') > -1) {
                                            //             index_1 = answer.indexOf('"Comments": "');
                                            //             prev_comm = answer.substr(index_1 + 13, answer.length); 
                                            //             prev_comm = prev_comm.replace(/\n|\r/g, '');
                                            //         }

                                            //         if (answer.indexOf('"Comments":"') > -1) {
                                            //             index_1 = answer.indexOf('"Comments":"'); 
                                            //             prev_comm = answer.substr(index_1 + 12, answer.length); 
                                            //         }

                                            //         if (answer.indexOf('"comment" : "') > -1) {
                                            //             index_1 = answer.indexOf('"comment": "'); 
                                            //             prev_comm = answer.substr(index_1 + 13, answer.length); 
                                            //         }
                                            //         prev_comm = prev_comm.replace(/"}/g, ''); 
                                                    
                                            //         let new_comm = prev_comm.replace(/"/g, '\\"');
                                            //         let new_answer = answer.replace('"'+prev_comm+'"', '"'+new_comm+'"'); 

                                            //         /* issue 17102018 - cant extract excel -> escape char */

                                            //         new_answer = new_answer.toString().replace(/\r/g,'');
                                            //         new_answer = new_answer.toString().replace(/\n/g,'');

                                            //         /* end line issue 17102018 - cant extract excel -> escape char */ 
                                            //         new_answer = JSON.parse(new_answer);

                                            //         var koplak = Object.keys(new_answer);

                                            //         if(koplak[0] != "question1"){
                                            //             for (let q = 0; q < this.questionStruct.length; q++) {
                                            //                 switch (this.questionStruct[q].QuestionNumber) {
                                            //                     case "QP1":
                                            //                         data[k]["QP1"] = this.CourseTeachingLvl(data[k].Update, data[k].Essentials, data[k].Intermediate, data[k].Advanced, data[k].Customized, data[k].OtherCTL, data[k].CommentCTL);
                                            //                         break;
                                            //                     case "QP2":
                                            //                         data[k]["QP2"] = data[k].primaryProduct;
                                            //                         break;
                                            //                     case "QP3":
                                            //                         data[k]["QP3"] = data[k].HoursTraining;
                                            //                         break;
                                            //                     case "QP4":
                                            //                         data[k]["QP4"] = (data[k].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                                            //                         break;
                                            //                     case "QP5":
                                            //                         data[k]["QP5"] = this.CourseTeachingMaterial(data[k].Autodesk, data[k].AAP, data[k].ATC, data[k].Independent, data[k].IndependentOnline, data[k].ATCOnline, data[k].OtherCTM, data[k].CommentCTM);
                                            //                         break;
                                            //                     case "Q1.1":
                                            //                         data[k]["Q1.1"] = new_answer.q1;
                                            //                         break;
                                            //                     case "Q1.2":
                                            //                         data[k]["Q1.2"] = new_answer.q2;
                                            //                         break;
                                            //                     case "Q1.3":
                                            //                         data[k]["Q1.3"] = new_answer.q3;
                                            //                         break;
                                            //                     case "Q1.4":
                                            //                         data[k]["Q1.4"] = new_answer.q4;
                                            //                         break;
                                            //                     case "Q2":
                                            //                         data[k]["Q2"] = "";
                                            //                         break;
                                            //                     case "Q2.1":
                                            //                         data[k]["Q2.1"] = this.selectedScore(parseInt(new_answer.q5["OE"]));
                                            //                         break;
                                            //                     case "Q2.2":
                                            //                         data[k]["Q2.2"] = this.selectedScore(parseInt(new_answer.q5["FR_1"]));
                                            //                         break;
                                            //                     case "Q2.3":
                                            //                         data[k]["Q2.3"] = this.selectedScore(parseInt(new_answer.q5["FR_2"]));
                                            //                         break;
                                            //                     case "Q2.4":
                                            //                         data[k]["Q2.4"] = "";
                                            //                         break;
                                            //                     case "Q2.5":
                                            //                         data[k]["Q2.5"] = this.selectedScore(parseInt(new_answer.q5["IQ"]));
                                            //                         break;
                                            //                     case "Q2.6":
                                            //                         data[k]["Q2.6"] = this.selectedScore(parseInt(new_answer.q5["CCM"]));
                                            //                         break;
                                            //                     case "Q3":
                                            //                         data[k]["Q3"] = "";
                                            //                         break;
                                            //                     case "Q3.1":
                                            //                         data[k]["Q3.1"] = this.selectedScore(parseInt(new_answer.q6["CCM"]));
                                            //                         break;
                                            //                     case "Q3.2":
                                            //                         data[k]["Q3.2"] = this.selectedScore(parseInt(new_answer.q6["IQ"]));
                                            //                         break;
                                            //                     case "Q3.3":
                                            //                         data[k]["Q3.3"] = this.selectedScore(parseInt(new_answer.q6["FR"]));
                                            //                         break;
                                            //                     case "Q4":
                                            //                         data[k]["Q4"] = this.selectedScore(parseInt(new_answer.q7["CCM"]));
                                            //                         break;
                                            //                     case "Q5":
                                            //                         var arr_temp = new_answer.q8.filter(v => v != '');
                                            //                         data[k]["Q5"] = arr_temp.join(" / ");
                                            //                         break;
                                            //                     case "Q6":
                                            //                         data[k]["Q6"] = new_answer.q9.replace(/_/g, " ");
                                            //                         break;
                                            //                     case "Q7":
                                            //                         data[k]["Q7"] = new_answer.q10.replace(/_/g, " ");
                                            //                         break;
                                            //                     case "Q8":
                                            //                         data[k]["Q8"] = "";
                                            //                         break;
                                            //                     case "Q8.1":
                                            //                         data[k]["Q8.1"] = "";
                                            //                         break;
                                            //                     case "Q8.2":
                                            //                         data[k]["Q8.2"] = "";
                                            //                         break;
                                            //                     case "Q8.3":
                                            //                         data[k]["Q8.3"] = "";
                                            //                         break;
                                            //                     case "Q8.4":
                                            //                         data[k]["Q8.4"] = "";
                                            //                         break;
                                            //                     case "Q9":
                                            //                         data[k]["Q9"] = "";
                                            //                         break;
                                            //                     case "Q9.1":
                                            //                         data[k]["Q9.1"] = this.selectedScore(parseInt(new_answer.q12["r1"]));;
                                            //                         break;
                                            //                     case "Q9.2":
                                            //                         data[k]["Q9.2"] = this.selectedScore(parseInt(new_answer.q12["r2"]));;
                                            //                         break;
                                            //                     case "Q9.3":
                                            //                         data[k]["Q9.3"] = this.selectedScore(parseInt(new_answer.q12["r3"]));;
                                            //                         break;
                                            //                     case "Q9.4":
                                            //                         data[k]["Q9.4"] = this.selectedScore(parseInt(new_answer.q12["r4"]));;
                                            //                         break;
                                            //                     case "Q10":
                                            //                         data[k]["Q10"] = this.question8(new_answer.q13) + (new_answer["q13-Comment"] != undefined) ? new_answer["q13-Comment"] : "";;
                                            //                         break;
                                            //                     case "Q11":
                                            //                         data[k]["Q11"] = new_answer.comment;
                                            //                         break;
                                            //                     case "Q11.1":
                                            //                         data[k]["Q11.1"] = "";
                                            //                         break;
                                            //                     case "Q11.2":
                                            //                         data[k]["Q11.2"] = "";
                                            //                         break;
                                            //                     case "Q11.3":
                                            //                         data[k]["Q11.3"] = "";
                                            //                         break;
                                            //                     case "Q11.4":
                                            //                         data[k]["Q11.4"] = "";
                                            //                         break;
                                            //                     case "Q11.5":
                                            //                         data[k]["Q11.5"] = "";
                                            //                         break;
                                            //                     case "CourseFacility":
                                            //                         data[k]["CourseFacility"] = (data[k].ATCFacility == '0') ? "ATC Facility" : "Onsite";;
                                            //                         break;
                                            //                     case "ComputerEquipment":
                                            //                         data[k]["ComputerEquipment"] = (data[k].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                                            //                         break;
                                            //                 }
                                            //             }
                                            //         } else{
                                            //             for (let q = 0; q < this.questionStruct.length; q++) {
                                            //                 switch (this.questionStruct[q].QuestionNumber) {
                                            //                     case "QP1":
                                            //                         data[k]["QP1"] = this.CourseTeachingLvl(data[k].Update, data[k].Essentials, data[k].Intermediate, data[k].Advanced, data[k].Customized, data[k].OtherCTL, data[k].CommentCTL);
                                            //                         break;
                                            //                     case "QP2":
                                            //                         data[k]["QP2"] = data[k].primaryProduct;
                                            //                         break;
                                            //                     case "QP3":
                                            //                         data[k]["QP3"] = data[k].HoursTraining;
                                            //                         break;
                                            //                     case "QP4":
                                            //                         data[k]["QP4"] = (data[k].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                                            //                         break;
                                            //                     case "QP5":
                                            //                         data[k]["QP5"] = this.CourseTeachingMaterial(data[k].Autodesk, data[k].AAP, data[k].ATC, data[k].Independent, data[k].IndependentOnline, data[k].ATCOnline, data[k].OtherCTM, data[k].CommentCTM);
                                            //                         break;
                                            //                     case "Q1.1":
                                            //                         data[k]["Q1.1"] = new_answer.question1;
                                            //                         break;
                                            //                     case "Q1.2":
                                            //                         data[k]["Q1.2"] = new_answer.question2;
                                            //                         break;
                                            //                     case "Q1.3":
                                            //                         data[k]["Q1.3"] = new_answer.question3;
                                            //                         break;
                                            //                     case "Q1.4":
                                            //                         data[k]["Q1.4"] = new_answer.question4;
                                            //                         break;
                                            //                     case "Q2":
                                            //                         data[k]["Q2"] = "";
                                            //                         break;
                                            //                     case "Q2.1":
                                            //                         data[k]["Q2.1"] = this.selectedScore(parseInt(new_answer.question5["OE"]));
                                            //                         break;
                                            //                     case "Q2.2":
                                            //                         data[k]["Q2.2"] = this.selectedScore(parseInt(new_answer.question5["FR_1"]));
                                            //                         break;
                                            //                     case "Q2.3":
                                            //                         data[k]["Q2.3"] = this.selectedScore(parseInt(new_answer.question5["FR_2"]));
                                            //                         break;
                                            //                     case "Q2.4":
                                            //                         data[k]["Q2.4"] = "";
                                            //                         break;
                                            //                     case "Q2.5":
                                            //                         data[k]["Q2.5"] = this.selectedScore(parseInt(new_answer.question5["IQ"]));
                                            //                         break;
                                            //                     case "Q2.6":
                                            //                         data[k]["Q2.6"] = this.selectedScore(parseInt(new_answer.question5["CCM"]));
                                            //                         break;
                                            //                     case "Q3":
                                            //                         data[k]["Q3"] = "";
                                            //                         break;
                                            //                     case "Q3.1":
                                            //                         data[k]["Q3.1"] = this.selectedScore(parseInt(new_answer.question6["CCM"]));
                                            //                         break;
                                            //                     case "Q3.2":
                                            //                         data[k]["Q3.2"] = this.selectedScore(parseInt(new_answer.question6["IQ"]));
                                            //                         break;
                                            //                     case "Q3.3":
                                            //                         data[k]["Q3.3"] = this.selectedScore(parseInt(new_answer.question6["FR"]));
                                            //                         break;
                                            //                     case "Q4":
                                            //                         data[k]["Q4"] = this.selectedScore(parseInt(new_answer.question7["CCM"]));
                                            //                         break;
                                            //                     case "Q5":
                                            //                         var arr_temp = new_answer.question8.filter(v => v != '');
                                            //                         data[k]["Q5"] = arr_temp.join(" / ");
                                            //                         break;
                                            //                     case "Q6":
                                            //                         data[k]["Q6"] = new_answer.question9.replace(/_/g, " ");
                                            //                         break;
                                            //                     case "Q7":
                                            //                         data[k]["Q7"] = new_answer.question10.replace(/_/g, " ");
                                            //                         break;
                                            //                     case "Q8":
                                            //                         data[k]["Q8"] = "";
                                            //                         break;
                                            //                     case "Q8.1":
                                            //                         data[k]["Q8.1"] = "";
                                            //                         break;
                                            //                     case "Q8.2":
                                            //                         data[k]["Q8.2"] = "";
                                            //                         break;
                                            //                     case "Q8.3":
                                            //                         data[k]["Q8.3"] = "";
                                            //                         break;
                                            //                     case "Q8.4":
                                            //                         data[k]["Q8.4"] = "";
                                            //                         break;
                                            //                     case "Q9":
                                            //                         data[k]["Q9"] = "";
                                            //                         break;
                                            //                     case "Q9.1":
                                            //                         data[k]["Q9.1"] = this.selectedScore(parseInt(new_answer.question11["r1"]));;
                                            //                         break;
                                            //                     case "Q9.2":
                                            //                         data[k]["Q9.2"] = this.selectedScore(parseInt(new_answer.question11["r2"]));;
                                            //                         break;
                                            //                     case "Q9.3":
                                            //                         data[k]["Q9.3"] = this.selectedScore(parseInt(new_answer.question11["r3"]));;
                                            //                         break;
                                            //                     case "Q9.4":
                                            //                         data[k]["Q9.4"] = this.selectedScore(parseInt(new_answer.question11["r4"]));;
                                            //                         break;
                                            //                     case "Q10":
                                            //                         data[k]["Q10"] = this.question8(new_answer.question12);
                                            //                         break;
                                            //                     case "Q11":
                                            //                         data[k]["Q11"] = new_answer.comment;
                                            //                         break;
                                            //                     case "Q11.1":
                                            //                         data[k]["Q11.1"] = "";
                                            //                         break;
                                            //                     case "Q11.2":
                                            //                         data[k]["Q11.2"] = "";
                                            //                         break;
                                            //                     case "Q11.3":
                                            //                         data[k]["Q11.3"] = "";
                                            //                         break;
                                            //                     case "Q11.4":
                                            //                         data[k]["Q11.4"] = "";
                                            //                         break;
                                            //                     case "Q11.5":
                                            //                         data[k]["Q11.5"] = "";
                                            //                         break;
                                            //                     case "CourseFacility":
                                            //                         data[k]["CourseFacility"] = (data[k].ATCFacility == '0') ? "ATC Facility" : "Onsite";;
                                            //                         break;
                                            //                     case "ComputerEquipment":
                                            //                         data[k]["ComputerEquipment"] = (data[k].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                                            //                         break;
                                            //                 }
                                            //             }
                                            //         }
                                            //     }

                                            //     delete data[k].EvaluationAnswerJson;
                                            //     delete data[k].ATCFacility;
                                            //     delete data[k].ATCComputer;
                                            //     delete data[k].HoursTraining;
                                            //     delete data[k].Update;
                                            //     delete data[k].Essentials;
                                            //     delete data[k].Intermediate;
                                            //     delete data[k].Advanced;
                                            //     delete data[k].Customized;
                                            //     delete data[k].OtherCTL;
                                            //     delete data[k].CommentCTL;
                                            //     delete data[k].InstructorLed;
                                            //     delete data[k].Online;
                                            //     delete data[k].Autodesk;
                                            //     delete data[k].AAP;
                                            //     delete data[k].ATC;
                                            //     delete data[k].Independent;
                                            //     delete data[k].IndependentOnline;
                                            //     delete data[k].ATCOnline;
                                            //     delete data[k].OtherCTM;
                                            //     delete data[k].CommentCTM;
                                            // }

                                            for (let key in data[0]) {
                                                this.cokelat.push(key);
                                            }

                                            this.report = data;
                                            this.dataFound = true;
                                            this.loading = false;
                                            this.dataresult = this.data;
                                           // this.TotalEvaluation = this.data.length;
                                        }
                                    });
                            }
                        });
                }
            } else{
                this.report = [];
                this.data = '';
                this.dataFound = true;
                this.loading = false;
                this.dataresult = this.data;
                this.TotalEvaluation = 0;
            }
        }, error => {
            this.report = [];
            this.data = '';
            this.dataFound = true;
            this.loading = false;
            this.dataresult = this.data;
            this.TotalEvaluation = 0;
        });
        return event;
}

    partnertypearr = [];
    certificatearr = [];
    countdata: number = 0;
    onSubmit() {
    
        this.loading = true;
        this.data = null;
    
        let geoarr = [];
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }
    
        let regionarr = [];
        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
        }
    
        let subregionarr = [];
        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
        }
    
        let countriesarr = [];
        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countriesarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }
    
        this.partnertypearr = [];
        for (var i = 0; i < this.selectedPartner.length; i++) {
            this.partnertypearr.push(this.selectedPartner[i].id);
        }
    
        this.certificateType = [];
        for (var i = 0; i < this.selectedCertificate.length; i++) {
            this.certificateType.push(this.selectedCertificate[i].id);
        }
    
        this.eventType = [];
        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }
    
        this.queryForm.value.filterBySiteId = this.siteidDipilih;
    
        var params =
            {
                firstNameStud: this.queryForm.value.filterByFirstName,
                lastNameStud: this.queryForm.value.filterByLastName,
                siteIdStud: this.queryForm.value.filterBySiteId,
                yearStud: this.queryForm.value.filterByYear,
                dateStud: this.queryForm.value.filterByDate,
                selectGeoStud: this.selectedItemsGeo,
                dropGeoStud: this.dropdownListGeo,
                selectRegionStud: this.selectedItemsRegion,
                dropRegionStud: this.dropdownListRegion,
                selectSubRegionStud: this.selectedItemsSubRegion,
                dropSubRegionStud: this.dropdownListSubRegion,
                selectCountry: this.selectedItemsCountry,
                dropCountry: this.dropdownListCountry,
                selectPartnerStud: this.selectedPartner,
                dropPartnerStud: this.dropdownPartner,
                selectCertiStud: this.selectedCertificate,
                dropCertiStud: this.dropdownCertificate
            }
    
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    
        var contactid = "";
        var orgid = "";
        var distributor = "";
        var siteid = "";
    
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
    
        if (this.adminya) {
            if(this.distributorya){
                distributor = this.urlGetOrgId();
            }else{
                if(this.orgya){
                    orgid = this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        siteid = this.urlGetSiteId();
                    }else{
                        contactid = this.useraccesdata.UserId;
                    }
                }
            }
        }
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
    
        var keyword = {
            filterByFirstName: this.queryForm.value.filterByFirstName,
            filterByLastName: this.queryForm.value.filterByLastName,
            filterBySiteId: this.queryForm.value.filterBySiteId,
            filterByGeo: geoarr.toString(),
            filterByRegion: regionarr.toString(),
            filterBySubRegion: subregionarr.toString(),
            filterByCertificateType: this.certificateType.toString(),
            filterBySurveyYear: this.queryForm.value.filterByYear,
            filterByDate: this.queryForm.value.filterByDate,
            filterByPartnerType: this.partnertypearr.toString(),
            filterByCountries: countriesarr.toString(),
            EventType: this.eventType.toString(),
            studentStatus: 'A',
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor,
            SiteId: siteid
        };
    
        var data: any;
        var course: any;
        var student: any;
        this.TotalEvaluation = 0;
        this.service.httpClientPost("api/ReportEva/TotalStudentEvaCount", keyword)
        .subscribe(res1 => {
        /* end line autodesk plan 10 oct */
        data=null;
                    data = res1;
                    this.countdata  = parseInt(data["EvalID"]);
                    this.TotalEvaluation = this.countdata;
                    this.dataFound = true;
                    this.getDetailData(1);
                    this.loading = false;
        });
    }

    redirectsite(siteId, year, createdDate, roleId, courseId) {
        this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteId: siteId, filterBySurveyYear: year, filterByDate: createdDate, filterByPartnerType: roleId, filterByCertificateType: this.certificateType.toString(), CourseId: courseId } });
    }

    // getGeo() {
    //     this.loading = true;
    //     // get Geo
    //     var data = '';
    //     this.service.get("api/Geo", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListGeo = null;
    //                 this.loading = false;
    //             }
    //             else {
    //                 this.dropdownListGeo = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.geo_code,
    //                         itemName: item.geo_name
    //                     }
    //                 });
    //                 this.selectedItemsGeo = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.geo_code,
    //                         itemName: item.geo_name
    //                     }
    //                 });
    //                 this.loading = false;
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //                 this.loading = false;
    //             });
    //     // this.selectedItemsGeo = [];
    // }

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
       
    //if(this.itsDistributor()){
    //    alert("one");
    //    alert(this.itsDistributor());
    //    var url = "api/GeoTer/"+ this.useraccesdata.Territory;
       
    //}
    //else{
       
    //    var url = "api/Geo";
    //    alert("two");
    //    alert(this.useraccesdata.Territory)
    //    alert(url);
    //}
    //  if (this.checkrole()) {
    //        if (this.itsinstructor()) {
    //            url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
    //        } else {
    //            if (this.itsDistributor()) {
    //                url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
    //            } else {
    //                url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
    //            }
    //        }
    //}
      var url = "api/Geo/SelectAdmin";
    // if (this.adminya) {
    //   if (this.distributorya) {
    //     url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
    //   } else {
    //     if (this.orgya) {
    //       url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
    //     } else {
    //       if (this.siteya) {
    //         url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
    //       } else {
    //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
    //       }
    //     }
    //   }
    // }
    var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })



                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    allcountries = [];
    getCountry() {
      this.loading = true;
      var url2 = "api/Countries/SelectAdmin";
    //   if (this.adminya) {
    //     if (this.distributorya) {
    //       url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
    //     } else {
    //       if (this.orgya) {
    //         url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
    //       } else {
    //         if (this.siteya) {
    //           // url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
    //           url2 = "api/Territory/where/{'OnlyCountrySiteId':'" + this.urlGetSiteId() + "'}"; /* fixed issue 24092018 - populate country by role site admin */
    //         } else {
    //           url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
    //         }
    //       }
    //     }
    //   }
      var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;var OnlyCountrySiteId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountrySiteId = this.urlGetSiteId();
                        url2 = "api/Territory/wherenew/'OnlyCountrySiteId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            OnlyCountrySiteId:OnlyCountrySiteId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        var data = '';
        this.service.httpClientPost(url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                    this.loading = false;
                }
                this.allcountries = this.dropdownListCountry;

                /* issue doc report 11102018 - default dropdown country for role site and org admin */

                if(this.adminya){
                    if(!this.distributorya){
                        this.selectedItemsCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                }

                /* end line issue doc report 11102018 - default dropdown country for role site and org admin */

            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    certificateDropdown() {
        this.dropdownCertificate = [];

        /* issue 17102018 - automatic remove when change selected partner type */

        this.dropdownEventType = [];
        this.selectedEventType = [];
        
        /* end line issue 17102018 - automatic remove when change selected partner type */

        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }


        // this.selectedCertificate = [];
    }

    ExportExceltoXls()
    {
        this.fileName = "StudentSearch.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "StudentSearch.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "StudentSearch.csv";
        this.ExportExcel();
     } 


    ExportExcel() {
        this.loading = true;
        this.expexcel = [];
        this.excel = [];
        let geoarr = [];
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }
    
        let regionarr = [];
        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
        }
    
        let subregionarr = [];
        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
        }
    
        let countriesarr = [];
        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countriesarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }
    
        this.partnertypearr = [];
        for (var i = 0; i < this.selectedPartner.length; i++) {
            this.partnertypearr.push(this.selectedPartner[i].id);
        }
    
        this.certificateType = [];
        for (var i = 0; i < this.selectedCertificate.length; i++) {
            this.certificateType.push(this.selectedCertificate[i].id);
        }
    
        this.eventType = [];
        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }
    
        this.queryForm.value.filterBySiteId = this.siteidDipilih;
    
        var params =
            {
                firstNameStud: this.queryForm.value.filterByFirstName,
                lastNameStud: this.queryForm.value.filterByLastName,
                siteIdStud: this.queryForm.value.filterBySiteId,
                yearStud: this.queryForm.value.filterByYear,
                dateStud: this.queryForm.value.filterByDate,
                selectGeoStud: this.selectedItemsGeo,
                dropGeoStud: this.dropdownListGeo,
                selectRegionStud: this.selectedItemsRegion,
                dropRegionStud: this.dropdownListRegion,
                selectSubRegionStud: this.selectedItemsSubRegion,
                dropSubRegionStud: this.dropdownListSubRegion,
                selectCountry: this.selectedItemsCountry,
                dropCountry: this.dropdownListCountry,
                selectPartnerStud: this.selectedPartner,
                dropPartnerStud: this.dropdownPartner,
                selectCertiStud: this.selectedCertificate,
                dropCertiStud: this.dropdownCertificate
            }
    
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    
        var contactid = "";
        var orgid = "";
        var distributor = "";
        var siteid = "";
    
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
    
        if (this.adminya) {
            if(this.distributorya){
                distributor = this.urlGetOrgId();
            }else{
                if(this.orgya){
                    orgid = this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        siteid = this.urlGetSiteId();
                    }else{
                        contactid = this.useraccesdata.UserId;
                    }
                }
            }
        }
   
        var keyword = {
            filterByFirstName: this.queryForm.value.filterByFirstName,
            filterByLastName: this.queryForm.value.filterByLastName,
            filterBySiteId: this.queryForm.value.filterBySiteId,
            filterByGeo: geoarr.toString(),
            filterByRegion: regionarr.toString(),
            filterBySubRegion: subregionarr.toString(),
            filterByCertificateType: this.certificateType.toString(),
            filterBySurveyYear: this.queryForm.value.filterByYear,
            filterByDate: this.queryForm.value.filterByDate,
            filterByPartnerType: this.partnertypearr.toString(),
            filterByCountries: countriesarr.toString(),
            EventType: this.eventType.toString(),
            studentStatus: 'A',
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor,
            SiteId: siteid
        };
    
        var data: any;
        this.service.httpClientPost("api/ReportEva/SearchingStudentExcel", keyword)
            .subscribe(res => {
				data=null;
                data = res;
                if (data.success==true) {
                    this.expexcel =data.data;
                    this.excelexport(res);
					  }
				  	else {
						this.excel = [];
						this.loading = false;
						 swal(
								'Process Failure',
								data.data,
								'error'
                            )
                 
				  	}
            });
    }

    excelexport(value) {
        let d : any;
        d = value;
        this.expexcel = [];
        this.expexcel = JSON.parse(d.data);
        //var fileName = "StudentSearch.xls";
        var wb = XLSX.utils.book_new();
        var ws = XLSX.utils.json_to_sheet(this.expexcel);

        XLSX.utils.book_append_sheet(wb, ws, this.fileName);
        // XLSX.writeFile(wb, fileName);
        XLSX.writeFile(wb, this.fileName, { bookType: 'xlsx', type: 'buffer' }); /* fixed issue 20092018 - Download site evaluation report, extract into excel, 2 sheets created in the same spread sheet */
        this.loading = false;

       
    }
    // ExportExcel() {
    //     let jsonData = [];
    //     if (this.dataresult.length > 0) {
    //         for (let i = 0; i < this.dataresult.length; i++) {
    //             jsonData.push({
    //                 "Eval ID": this.dataresult[i].EvalID,
    //                 "Site ID": this.dataresult[i].SiteId,
    //                 "Survey Partner Type": (this.dataresult[i].RoleId == 1) ? "ATC" : "AAP",
    //                 "Site Name": this.dataresult[i].SiteName,
    //                 "Instructor Name": this.dataresult[i].InstructorName,
    //                 "Date Submitted": this.dataresult[i].DateSubmitted,
    //                 "First Name": this.dataresult[i].FirstName,
    //                 "Last Name": this.dataresult[i].LastName,
    //                 "Phone": this.dataresult[i].Phone,
    //                 "Company": this.dataresult[i].Company,
    //                 "Email": this.dataresult[i].Email,
    //                 "Address": this.dataresult[i].Address,
    //                 "Language": this.dataresult[i].Language,
    //                 "Country": this.dataresult[i].Country,
    //                 "Course ID": this.dataresult[i].CourseId,
    //                 "Course Title": this.dataresult[i].CourseTitle,
    //                 "Course Start Date": this.dataresult[i].StartDate,
    //                 "Course End Date": this.dataresult[i].EndDate,
    //                 "ScoreOP": this.dataresult[i].ScoreOP,
    //                 "QP1": this.dataresult[i].QP1,
    //                 "QP2": this.dataresult[i].QP2,
    //                 "QP3": this.dataresult[i].QP3,
    //                 "QP4": this.dataresult[i].QP4,
    //                 "QP5": this.dataresult[i].QP5,
    //                 "Q1.1": this.dataresult[i]["Q1.1"],
    //                 "Q1.2": this.dataresult[i]["Q1.2"],
    //                 "Q1.3": this.dataresult[i]["Q1.3"],
    //                 "Q1.4": this.dataresult[i]["Q1.4"],
    //                 "Q2.1": this.dataresult[i]["Q2.1"],
    //                 "Q2.2": this.dataresult[i]["Q2.2"],
    //                 "Q2.3": this.dataresult[i]["Q2.3"],
    //                 "Q2.4": this.dataresult[i]["Q2.4"],
    //                 "Q2.5": this.dataresult[i]["Q2.5"],
    //                 "Q2.6": this.dataresult[i]["Q2.6"],
    //                 "Q3.1": this.dataresult[i]["Q3.1"],
    //                 "Q3.2": this.dataresult[i]["Q3.2"],
    //                 "Q3.3": this.dataresult[i]["Q3.3"],
    //                 "Q4": this.dataresult[i]["Q4"],
    //                 "Q5": this.dataresult[i]["Q5"],
    //                 "Q6": this.dataresult[i]["Q6"],
    //                 "Q7": this.dataresult[i]["Q7"],
    //                 "Q8": this.dataresult[i]["Q8"],
    //                 "Q9.1": this.dataresult[i]["Q9.1"],
    //                 "Q9.2": this.dataresult[i]["Q9.2"],
    //                 "Q9.3": this.dataresult[i]["Q9.3"],
    //                 "Q9.4": this.dataresult[i]["Q9.4"],
    //                 "Q10": this.dataresult[i]["Q10"],
    //                 "Comments": this.dataresult[i]["Comments"]
    //             });
    //         }
    //         var date = this.service.formatDate();
    //         var fileName = "StudentSearch.xls";
    //         var ws = XLSX.utils.json_to_sheet(jsonData);
    //         var wb = XLSX.utils.book_new();
    //         XLSX.utils.book_append_sheet(wb, ws, fileName);
    //         XLSX.writeFile(wb, fileName);
    //     }
    // }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.queryForm.patchValue({ filterByYear: this.year[this.year.length - 1].Key });
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                   this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                      this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })


                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    roleinstructor: boolean = false;
    ngOnInit() {
        this.checkrole();
        this.itsinstructor();
        this.itsOrganization();
        this.itsSite();
        this.itsDistributor();
        /* call function get user level id (issue31082018)*/

        this.checkrole();
        this.itsinstructor();
        this.itsOrganization();
        this.itsSite();
        this.itsDistributor();


        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getCountry();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.queryForm.patchValue({ filterByFirstName: item.firstNameStud });
            this.queryForm.patchValue({ filterByLastName: item.lastNameStud });
            this.queryForm.patchValue({ filterBySiteId: item.siteIdStud });
            this.queryForm.patchValue({ filterByYear: item.yearStud });
            this.queryForm.patchValue({ filterByDate: item.dateStud });
            this.selectedItemsGeo = item.selectGeoStud;
            this.dropdownListGeo = item.dropGeoStud;
            this.selectedItemsRegion = item.selectRegionStud;
            this.dropdownListRegion = item.dropRegionStud;
            this.selectedItemsSubRegion = item.selectSubRegionStud;
            this.dropdownListSubRegion = item.dropSubRegionStud;
            this.selectedItemsCountry = item.selectCountry;
            this.dropdownListCountry = item.dropCountry;
            this.dropdownPartner = item.dropPartnerStud;
            this.selectedPartner = item.selectPartnerStud;
            this.selectedCertificate = item.selectCertiStud;
            this.dropdownCertificate = item.dropCertiStud;
            this.onSubmit();
        }
        else {
            // this.selectedValueDate = "";
            this.getGeo();
            this.getCountry();
            this.getPartnerType();
            // this.selectedPartner = [];     
        }
        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        /* call function get user level id (issue31082018)*/

       

        setTimeout(() => {
            if (this.adminya) {
                if (this.trainerya) {
                    this.contactId = this.useraccesdata.InstructorId;
                    this.ContactName = this.useraccesdata.ContactName;
                    this.roleinstructor = true;
                }
            }

            let status = ['"A"', '"H"', '"I"'];
            let siteUrl: any;
            if (this.adminya) {
                if(this.distributorya){
                    siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Distributor':'" + this.urlGetOrgId() + "'}";
                }else{
                    if(this.orgya){
                        siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Organization':'" + this.urlGetOrgId() + "'}";
                    }else{
                        if(this.siteya){
                            siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','SiteId':'" + this.urlGetSiteId() + "'}";
                        }else{
                            siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','ContactId':'" + this.useraccesdata.UserId + "'}";
                        }
                    }
                }
            }else {
                siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
            }

            this.dataService1 = this.completerService.remote(
                null,
                "Sites",
                "Sites");
            this.dataService1.urlFormater(term => {
                return siteUrl + "/" + term;
            });
            this.dataService1.dataField("results");
        }, 3000);

        /* call function get user level id (issue31082018)*/
    }

    onSelected(item) {
        if (item != null) {
            let splitVal = (item.originalObject.Sites).split(" | ");
            this.siteidDipilih = splitVal[0];
        }
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    // onGeoSelect(item: any) {
    //     this.selectedItemsCountry = [];
    //     this.arraygeoid.push('"' + item.id + '"');

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // OnGeoDeSelect(item: any) {
    //     this.selectedItemsCountry = [];
    //     var tmpGeo = "''"
    //     this.arraygeoid = [];
    //     if (this.selectedItemsGeo.length != 0) {
    //         if (this.selectedItemsGeo.length > 0) {
    //             for (var i = 0; i < this.selectedItemsGeo.length; i++) {
    //                 this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
    //             }
    //             tmpGeo = this.arraygeoid.toString()
    //         }
    //     } else {
    //         if (this.dropdownListGeo.length > 0) {
    //             for (var i = 0; i < this.dropdownListGeo.length; i++) {
    //                 this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
    //             }
    //             tmpGeo = this.arraygeoid.toString()
    //         }
    //     }
    //     // console.log(tmpGeo);
    //     // Countries
    //     var tmpTerritory = "''"

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // onGeoSelectAll(items: any) {
    //     this.arraygeoid = [];

    //     for (var i = 0; i < items.length; i++) {
    //         this.arraygeoid.push('"' + items[i].id + '"');
    //     }

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.dropdownListCountry = this.allcountries;
    //     this.selectedItemsCountry = [];
    // }

    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }

        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })



                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        //this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
        this.service.httpClientPost(url, keyword)   
        .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                    // console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.dataFound = false;
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {
        this.dataFound = false;
        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp = result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = [];
            this.dropdownListCountry = [];

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.dataFound = false;
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dataFound = false;
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.dataFound = false;
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {
        this.dataFound = false;
        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = [];

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.dataFound = false;
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dataFound = false;
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onCountriesSelect(item: any) { this.dataFound = false; }
    OnCountriesDeSelect(item: any) { this.dataFound = false; }
    onCountriesSelectAll(items: any) { this.dataFound = false; }
    onCountriesDeSelectAll(items: any) { this.dataFound = false; }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }
}
