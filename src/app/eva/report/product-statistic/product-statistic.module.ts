import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ProductStatisticEVAComponent } from './product-statistic.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';
import { Ng2PaginationModule } from 'ng2-pagination';

export const ProductStatisticEVARoutes: Routes = [
    {
        path: '',
        component: ProductStatisticEVAComponent,
        data: {
            breadcrumb: 'Products Trained Statistics',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProductStatisticEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule,
        Ng2PaginationModule
    ],
    declarations: [ProductStatisticEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class ProductStatisticEVAModule { }