import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-product-statistic-eva',
    templateUrl: './product-statistic.component.html',
    styleUrls: [
        './product-statistic.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProductStatisticEVAComponent implements OnInit {

    apiCall:any;
    selectedPrimaryProduct;
    Primary=[];

    selectedSecondaryProduct;
    Secondary=[];
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    public loading = false;
    public loading_1 = false;
    year;
    selectedYear;
    reportResult = [];
    // evalTotal: number = 0;
    eventType = [];
    public showEvent = false;
    public SiteActive = "A";
    public header = [];
    useraccesdata: any;
    totalEval: number = 0;
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    selectedValueDateRange;
    keyword: any;
    report = [];
    report_temp = [];
    listkolom = [];
    fileName = "ProductTrainedStatistics.xls";
    // public getServerData(event) {
    //     // console.log(event);
    //     this.loading_1 = true;
    //     this.report_temp = this.report; //hanya untuk menahan ukuran tinggi tabel
    //     var limit = 10;
    //     var ofset = (event - 1) * 10;
    //     var endpage = Math.floor(this.countdata / 10) + 1;

    //     if (event == 1) {
    //         ofset = 0;
    //     } else if (event == endpage) {
    //         limit = this.countdata % 10;
    //     }

    //     this.report = null;

    //     this.keyword.Limit = limit;
    //     this.keyword.Offset = ofset

    //     var data: any;

    //     // this.http.post("api/EvaluationAnswer/ProductStatistic", this.keyword, { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) })
    //     //     .subscribe(res => {
    //     //         data = JSON.parse(res["_body"]);
    //     //         if (data.length > 0) {

    //     //             // console.log(data);
    //     //             for (let key in data[0]) {
    //     //                 this.listkolom.push(key);
    //     //             }

    //     //             this.report = data;

    //     //         } else {
    //     //             this.report = [];
    //     //             this.listkolom = [];
    //     //             this.loading_1 = false;
    //     //         }
    //     //         this.dataFound = true;
    //     //         this.loading_1 = false;
    //     //     }, error => {
    //     //         this.report = [];
    //     //         this.listkolom = [];
    //     //         this.dataFound = true;
    //     //         this.loading_1 = false;
    //     //     });

    //     // return event;
    // }

    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private parserFormatter: NgbDateParserFormatter, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    getGeo() {
        this.loading = true;
        var url = "api/Geo/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         }

        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
        //         else {
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }
        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */

        //         // else if (this.itsSite()) {
        //         //     url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         // } 
        //         // else {
        //         //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         // }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url, keyword) /* end issue popualte geo on country */
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                    this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsGeo = [];
    }

    territorydefault: string = "";
    getTerritory() {
        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'Distributor'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteId'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteId";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            Distributor: DistributorGeo,
            SiteId: siteRes,
            OrgId: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.adminya) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    allcountries = [];
    getCountry() {
        this.loading = true;
        var data = '';
        var url2 = "api/Countries/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; // tambah kalo site / org country nya based org
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
                //  url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 // url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //                 url2 = "api/Territory/where/{'OnlyCountrySiteId':'" + this.urlGetSiteId() + "'}"; /* fixed issue 24092018 - populate country by role site admin */
        //             }else{
        //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;var OnlyCountrySiteId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountrySiteId = this.urlGetSiteId();
                        url2 = "api/Territory/wherenew/'OnlyCountrySiteId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            OnlyCountrySiteId:OnlyCountrySiteId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        var data = '';
        this.service.httpClientPost(url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    this.loading = false;
                }
                this.allcountries = this.dropdownListCountry;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    arrayterritory = [];
    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        // this.arraygeoid = [];
        // if (this.selectedItemsGeo.length > 0) {
        //     for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //         this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //     }
        //     tmpGeo = this.arraygeoid.toString()
        // }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        // Countries
        var data = '';

       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdminTerr/" + this.arrayterritory.toString() + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var SiteAdminTerr = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        SiteAdminTerr = this.urlGetSiteId();
                        url = "api/Countries/SiteAdminTerr";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            SiteAdminTerr: SiteAdminTerr,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        //split countries
        // let countriesArrTmp = [];
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        // this.arraygeoid = [];
        // if (this.selectedItemsGeo.length > 0) {
        //     for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //         this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //     }
        //     tmpGeo = this.arraygeoid.toString()
        // }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';
           // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
            var url = "api/Countries/filterByGeoByTerritory";
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
            
            // if (this.adminya) {
            //     if(this.distributorya){
            //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
            //     }else{
            //         if(this.orgya){
            //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
            //         }else{
            //             if(this.siteya){
            //                 // url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
            //                 url = "api/Countries/SiteAdminTerr/" + this.arrayterritory.toString() + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
            //             }
            //         }
            //     }
            // }
            var  DistTerr = null; var OrgSiteTerr = null;var SiteAdminTerr = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistTerr = this.urlGetOrgId();
                   url = "api/Countries/DistTerr";
                } else {
                    if (this.orgya) {
                        OrgSiteTerr = this.urlGetOrgId();
                        url = "api/Countries/OrgSiteTerr";
                    } else {
                        if (this.siteya) {
                            SiteAdminTerr = this.urlGetSiteId();
                            url = "api/Countries/SiteAdminTerr";
                        }
                    }
                }
                
            }
            var keyword : any = {
                CtmpGeo:tmpGeo,
                CDistTerr:DistTerr,
                COrgSiteTerr:OrgSiteTerr,
                SiteAdminTerr: SiteAdminTerr,
                CtmpTerritory:this.arrayterritory.toString()
               
            };
            keyword = JSON.stringify(keyword);

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        // this.arraygeoid = [];
        // if (this.selectedItemsGeo.length > 0) {
        //     for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //         this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //     }
        //     tmpGeo = this.arraygeoid.toString()
        // }
        // Countries
        var data = '';
     //  var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
var url = "api/Countries/filterByGeoByTerritory";
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdminTerr/" + this.arrayterritory.toString() + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var SiteAdminTerr = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        SiteAdminTerr = this.urlGetSiteId();
                        url = "api/Countries/SiteAdminTerr";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            SiteAdminTerr: SiteAdminTerr,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    certificateDropdown() {
        this.dropdownCertificate = [];
        // this.selectedCertificate = [];
        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }

    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        // this.selectedItemsMarketType = [];
    }


    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    //show value certificate
                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    ExportExceltoXls()
    {
        this.fileName = "ProductTrainedStatistics.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "ProductTrainedStatistics.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "ProductTrainedStatistics.csv";
        this.ExportExcel();
     } 

    ExportExcel() {
        if (this.report_temp.length > 0) {
            let unique = [];
            let count = []; //untuk nampung total geo,territory,country
            let total = 0;
            let position = 0;
            let key = this.listkey;
            // pertama filter nama produk supaya tidak duplikat / pisahkan yang unik nya
            for(let n = 0; n < this.report_temp.length; n++){
                if(unique.indexOf(this.report_temp[n].productName) == -1){
                    unique.push(this.report_temp[n].productName);
                }
            }

            // hapus nama property/key dalam array key dari hasil objek pencarian (hapus productName,PartnerType,Version,WorlWide)
            // yang kita butuhkan hanya nama geo,territory, atau country untuk nantinya dijumlahkan
            key.splice(key.indexOf("productName"),1);
            key.splice(key.indexOf("PartnerType"),1);
            key.splice(key.indexOf("Version"),1);
            key.splice(key.indexOf("WorlWide"),1);

            for(let m = 0; m < key.length; m++){
                count.push(0);
            }
            
            for(let j = 0; j < unique.length; j++){
                for(let i = 0; i < this.report_temp.length; i++){
                    if(this.report_temp[i].productName == unique[j]){
                        total += parseInt(this.report_temp[i].WorldWide);
                        position = i;
                        for(let p = 0; p < key.length; p++){
                            let curr_key = key[p];
                            count[p] += parseInt(this.report_temp[i][curr_key]);
                        }
                    }
                }
                if(total != 0 && position != 0){
                    let arr_temp = {
                        productName: "Total",
                        Version: "",
                        PartnerType:"",
                        WorldWide: total
                    }
                    for(let z = 0; z < key.length; z++){
                        let adding = key[z];
                        arr_temp[adding] = count[z];
                        count[z] = 0; //set value array menjadi 0 kembali untuk menjumlahkan produk yg lain
                    }
                    this.report_temp.splice(position + 1,0,arr_temp);
                }
                total = 0;
                position = 0;
            }
            
            for(let x = 0; x < this.report_temp.length; x++){
                delete this.report_temp[x].ProductVersionsId
            }

           // var fileName = "ProductTrainedStatistics.xls";
            var ws = XLSX.utils.json_to_sheet(this.report_temp);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, this.fileName);
            XLSX.writeFile(wb, this.fileName);
        }
        this.getReport();
    }

    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        /* issue 15102018 default date filter by now */

        this.modelPopup1 = {
            year:new Date().getFullYear(),
            month:new Date().getMonth() + 1,
            day:new Date().getDate()
        }

        this.modelPopup2 = {
            year:new Date().getFullYear(),
            month:new Date().getMonth() + 1,
            day:new Date().getDate()
        }

        /* end line issue 15102018 default date filter by now */

        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getCountry();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoLang;
            this.dropdownListGeo = item.dropGeoLang;
            this.selectedItemsCountry = item.selectCountryLang;
            this.dropdownListCountry = item.dropCountryLang;
            this.selectedYear = item.selectYearLang;
            this.selectedItemsMarketType = item.selectMarket;
            this.dropdownListMarketType = item.dropMarket;
            // this.selectedPartner = item.selectPartnerLang;
            // this.dropdownPartner = item.dropPartnerLang;
            // this.selectedCertificate = item.selectCertiLang;
            // this.dropdownCertificate = item.dropCertiLang;
            this.selectedValueDateRange = item.range;
            if (item.startdate != "") {
                var datestart = item.startdate.split("-");
                this.modelPopup1 = {
                    "year": parseInt(datestart[0]),
                    "month": parseInt(datestart[1]),
                    "day": parseInt(datestart[2])
                };
            }

            if (item.enddate != "") {
                var dateend = item.enddate.split("-");
                this.modelPopup2 = {
                    "year": parseInt(dateend[0]),
                    "month": parseInt(dateend[1]),
                    "day": parseInt(dateend[2])
                };
            }
            this.getFY2();

            this.getReport();
        }
        else {
this.getPrimaryAndSecondaryProduct();
            this.getGeo();
            this.getTerritory();
            this.getCountry();
            // this.getPartnerType();
            this.getMarketType();
            this.selectedValueDateRange = "SurveyYear";
            this.getFY();
            // this.selectedYear = "";
            // this.selectedPartner = [];
        }
      
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid.push('"' + item.id + '"');

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
       var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory    
        };
        keyword = JSON.stringify(keyword);
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"

        // Countries
        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }

        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory    
        };
        keyword = JSON.stringify(keyword);
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory    
        };
        keyword = JSON.stringify(keyword);
        

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];
    }

    onCountriesSelect(item: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
            this.dataFound = false;
        }
    }

    OnCountriesDeSelect(item: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
            this.dataFound = false;
        }
    }

    onCountriesSelectAll(items: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
            this.dataFound = false;
        }
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    getFY2() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
            }, error => {
                this.service.errorserver();
            });
    }

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
getPrimaryAndSecondaryProduct(){
    var productTemp;
    this.service.httpClientGet("api/Product/GetPrimaryAndSecondaryProducts", productTemp)
            .subscribe(result => {
                productTemp = result;
                this.Primary = productTemp.primaryProducts;
	        this.Secondary = productTemp.SecondaryProducts;
            });
   }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    listkey = [];
    // countdata: number = 0;
    // page: number = 0;
    getReport() {
        this.loading = true;
        var geo = [];
        var countries = [];
        var territory = [];
        var marketType = [];
        var fyear: string = "";
        var dateStart: string = "";
        var dateEnd: string = "";
        // var partnerType = [];
        // var certificateType = [];
        // this.eventType = [];
        // var course;
        // var project;

        // for (let i = 0; i < this.selectedPartner.length; i++) {
        //     partnerType.push(this.selectedPartner[i].id);
        // }

        // if (this.selectedCertificate.length > 0) {
        //     for (let i = 0; i < this.selectedCertificate.length; i++) {
        //         certificateType.push(this.selectedCertificate[i].id);
        //     }
        // } else {
        //     certificateType = [];
        // }

        // if (this.selectedEventType.length > 0) {
        //     for (let i = 0; i < this.selectedEventType.length; i++) {
        //         this.eventType.push('"' + this.selectedEventType[i].id + '"');
        //     }
        // }

        for (let i = 0; i < this.selectedItemsGeo.length; i++) {
            geo.push('"' + this.selectedItemsGeo[i].id + '"');
        }

        for (let i = 0; i < this.selectedItemsTerritories.length; i++) {
            territory.push('"' + this.selectedItemsTerritories[i].id + '"');
        }

        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            countries.push('"' + this.selectedItemsCountry[i].id + '"');
        }

        for (let i = 0; i < this.selectedItemsMarketType.length; i++) {
            marketType.push(this.selectedItemsMarketType[i].id);
        }

        if (this.selectedValueDateRange == "SurveyYear" && this.selectedYear != "") {
            fyear = this.selectedYear;
            dateStart = "";
            dateEnd = "";
        } else {
            if (this.modelPopup1 != null && this.modelPopup2 != null) {
                dateStart = this.parserFormatter.format(this.modelPopup1);
                dateEnd = this.parserFormatter.format(this.modelPopup2);
                fyear = "";
            }
        }

        var params =
        {
            selectGeoLang: this.selectedItemsGeo,
            dropGeoLang: this.dropdownListGeo,
            selectCountryLang: this.selectedItemsCountry,
            dropCountryLang: this.dropdownListCountry,
            selectYearLang: fyear,
            selectTerritory: this.selectedItemsTerritories,
            dropTerritory: this.dropdownListTerritories,
            range: this.selectedValueDateRange,
            startdate: dateStart,
            enddate: dateEnd,
            selectMarket: this.selectedItemsMarketType,
            dropMarket: this.dropdownListMarketType
            // selectPartnerLang: this.selectedPartner,
            // dropPartnerLang: this.dropdownPartner,
            // selectCertiLang: this.selectedCertificate,
            // dropCertiLang: this.dropdownCertificate,
            // siteActive: this.SiteActive
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        // "api/EvaluationAnswer/SubmissionByLanguage/{'CountryCode':'" + countries + "','PartnerType':'" + partnerType + "','AAP_Course':'" + course +
        // "','AAP_Project':'" + project + "'}"

        var contactid = "";
        var orgid = "";
        var distributor = "";
        var siteid = "";

        // if(this.checkrole()){
        //     if(this.itsDistributor()){
        //         distributor = this.urlGetOrgId();
        //     }else{
        //         if(this.itsinstructor()){
        //             contactid = this.useraccesdata.UserId;
        //         }else{
        //             orgid = this.urlGetOrgId();
        //         }
        //     }
        // }

        /* new request nambah user role -__ */

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         distributor = this.urlGetOrgId();
        //     } else {
        //         if (this.itsOrganization()) {
        //             orgid = this.urlGetOrgId();
        //         } else {
        //             if (this.itsSite()) {
        //                 siteid = this.urlGetSiteId();
        //             } else {
        //                 contactid = this.useraccesdata.UserId;
        //             }
        //         }
        //     }
        // }

        /* new request nambah user role -__ */

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        if (this.adminya) {
            if(this.distributorya){
                distributor = this.urlGetOrgId();
            }else{
                if(this.orgya){
                    orgid = this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        siteid = this.urlGetSiteId();
                    }else{
                        contactid = this.useraccesdata.UserId;
                    }
                }
            }
        }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.keyword = {
            Country: countries.toString(),
            Geo: geo.toString(),
            Range: this.selectedValueDateRange,
            Year: fyear,
            StartDate: dateStart,
            CompletionDate: dateEnd,
            MarketType: marketType.toString(),
            Territory: territory.toString(),
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor,
            SiteId: siteid,
	    Primary:this.selectedPrimaryProduct,
	    Secondary:this.selectedSecondaryProduct,
            // PartnerType: partnerType.toString(),
            // CertificateType: certificateType.toString(),
            // EventType: this.eventType.toString(),
            // SiteActive: this.SiteActive,
        };

        // console.log(keyword);
        this.reportResult = [];
        this.listkey = [];

        var data: any;
        var temp_total: any;
        // this.evalTotal = 0;
        let data_site = [];
        let sites: any;
        let total: any;
        this.totalEval = 0;
        // this.header = [];
        
        /* autodesk plan 18 oct */
        
        this.apiCall = this.service.httpClientPost("api/EvaluationAnswer/TotalProductStatistic",this.keyword)
           .subscribe(res_total => {
               total = res_total;
               if(parseInt(total.Total) != 0){
                   this.totalEval = total.Total;
                   this.service.httpClientPost("api/EvaluationAnswer/ProductTrainedStatistic",this.keyword)
                       .subscribe(result => {
                            data = result;
                            if(data.length != 0){
                                
                                this.report = data;

                                /* fix product trained statistic format issue 04/09/2018 */
                                
                                for (let key in this.report[0]) {
                                    this.listkey.push(key);
                                }

                                /* fix product trained statistic format issue 04/09/2018 */

                                this.report_temp = data;
                            } else{
                                this.report = [];
                                this.report_temp = [];
                            }
                            this.dataFound = true;
                            this.loading = false;
                        });
               } else{
                    this.totalEval = 0;
                    this.loading = false;
                    this.dataFound = false;
               }
        });

        /* end line autodesk plan 18 oct */
        
    }
}