import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { PostEvaluationResponseEVAComponent } from './post-evaluation-responses.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

export const PostEvaluationResponseEVARoutes: Routes = [
    {
        path: '',
        component: PostEvaluationResponseEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.post_evaluation_responses.post_evaluation_responses',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PostEvaluationResponseEVARoutes),
        SharedModule,
        AngularMultiSelectModule
    ],
    declarations: [PostEvaluationResponseEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class PostEvaluationResponseEVAModule { }