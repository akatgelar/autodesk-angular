import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

@Component({
    selector: 'app-post-evaluation-responses',
    templateUrl: './post-evaluation-responses.component.html',
    styleUrls: [
        './post-evaluation-responses.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class PostEvaluationResponseEVAComponent implements OnInit {

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    year;
    selectedValueDate;
    date;
    selectValueSite;
    siteActive;

    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo) { 

        this.date = [
            {id: 2, name: "February"},
            {id: 3, name: "March"},
            {id: 4, name: "April"},
            {id: 5, name: "May"},
            {id: 6, name: "June"},
            {id: 7, name: "July"},
            {id: 8, name: "August"},
            {id: 9, name: "September"},
            {id: 10, name: "October"},
            {id: 11, name: "November"},
            {id: 12, name: "December"},
            {id: 1, name: "January"},
            {id: "q1", name: "Q1"},
            {id: "q2", name: "Q2"},
            {id: "q3", name: "Q3"},
            {id: "q4", name: "Q4"}
        ];

        this.siteActive = [
            {id: "active", name: "Active Only"},
            {id: "inactive", name: "Inactive Only"}
        ];
    }

    getGeo() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Geo", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_id,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
          .subscribe(res => {
            this.year = res;
          }, error => {
            this.service.errorserver();
          });
    }

    ngOnInit() {
        this.getGeo();
        this.getFY();
        this.selectedValueDate = "";
        this.selectValueSite = "";
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onGeoSelect(item: any) {
        this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    OnGeoDeSelect(item: any) {
        this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onGeoSelectAll(items: any) {
        this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onGeoDeSelectAll(items: any) {
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListRegion = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onRegionSelect(item: any) {
        this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    OnRegionDeSelect(item: any) {
        this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onRegionSelectAll(items: any) {
        this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onRegionDeSelectAll(items: any) {
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onSubRegionSelect(item: any) {
        this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    OnSubRegionDeSelect(item: any) {
        this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onSubRegionSelectAll(items: any) {
        this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onSubRegionDeSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }
}