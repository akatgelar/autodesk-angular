import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { InstructorCourseEVAComponent } from './instructor-course.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
// import { ConvertDatePipe } from './instructor-course.component';

export const InstructorCourseEVARoutes: Routes = [
    {
        path: '',
        component: InstructorCourseEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.instructor_course.instructor_course_list',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(InstructorCourseEVARoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [InstructorCourseEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe]
})
export class InstructorCourseEVAModule { }