import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";

// const now = new Date();
// Convert Date
// @Pipe({ name: 'convertDate' })
// export class ConvertDatePipe {
//     constructor(private datepipe: DatePipe) { }
//     transform(date: string): any {
//         return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
//     }
// }

@Component({
    selector: 'app-instructor-course',
    templateUrl: './instructor-course.component.html',
    styleUrls: [
        './instructor-course.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class InstructorCourseEVAComponent implements OnInit {

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    year;
    selectedValueDate;
    selectedYear;
    date;
    Month;
    private InstructorId;
    private SiteId;
	private ProductId;
    courseList = [];
    SiteName;
    private SAPShipTo_retired;
    private Evals: number = 0;
    public loading = false;
    FYIndicatorKey;
    RoleId;

    constructor(public http: Http, private service: AppService, private route: ActivatedRoute, private router: Router) {

        this.date = [
            { id: "", name: "Anual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
            }, error => {
                this.service.errorserver();
            });
    }

    ngOnInit() {
        var sub: any;
        var date: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || "";
			this.ProductId=params['ProductID'] || "";
            this.selectedYear = params['Year'] || "";
            date = params['Month'] || "";
            this.InstructorId = params['InstructorId'] || "";
            this.SiteName = params['SiteName'] || "";
            this.RoleId = params['RoleId'] || "";
        });

        if (date != "" && date != "q1" && date != "q2" && date != "q3" && date != "q4") {
            this.selectedValueDate = parseInt(date);
        } else {
            this.selectedValueDate = date;
        }

        this.getInstCourse();
        this.getFY();
    }

    getInstCourse() {
        this.loading = true;

        var keyword = {
            SiteId: this.SiteId,
			ProductID:this.ProductId,
            Year: this.selectedYear,
            Instructor: this.InstructorId,
            Month: this.selectedValueDate,
            PartnerType: this.RoleId
        };

        var data: any;
        this.Evals = 0;
        this.courseList = [];
        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost("api/EvaluationAnswer/InstructorCourse/", keyword)
            .subscribe(res => {
                data = res;
                // console.log(data);
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        this.Evals += parseInt(data[i].TotalStudent);
                    }
                    this.courseList = data;
                    this.loading = false;
                } else {
                    this.courseList = [];
                    this.SiteName = "";
                    this.SAPShipTo_retired = "";
                    this.Evals = 0;
                    this.loading = false;
                }
            }, error => {
                this.courseList = [];
                this.SiteName = "";
                this.SAPShipTo_retired = "";
                this.Evals = 0;
                this.loading = false;
            });
        /* new post for autodesk plan 17 oct */
    }
}