import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-product-trained-eva',
    templateUrl: './product-trained.component.html',
    styleUrls: [
        './product-trained.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProductTrainedEVAComponent implements OnInit {

    apiCall:any;
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    public loading = false;
    year;
    selectedYear;
    reportResult = [];
    // evalTotal: number = 0;
    eventType = [];
    public showEvent = false;
    public SiteActive = "A";
    public header = [];
    useraccesdata: any;
    totalEval: number = 0;
    fileName = "ProductTrainedEva.xls";
    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    getGeo() {
        this.loading = true;
        var url = "api/Geo/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } 

        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
        //         else {
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }
        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
                
        //         // else if (this.itsSite()) {
        //         //     url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         // } 
        //         // else {
        //         //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         // }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url, keyword) /* end issue popualte geo on country */
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                    this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsGeo = [];
    }

    allcountries = [];
    getCountry() {
        this.loading = true;
        var data = '';
        var url2 = "api/Countries/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; // tambah kalo site / org country nya based org
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 // url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //                 url2 = "api/Territory/where/{'OnlyCountrySiteId':'" + this.urlGetSiteId() + "'}"; /* fixed issue 24092018 - populate country by role site admin */
        //             }else{
        //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;var OnlyCountrySiteId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountrySiteId = this.urlGetSiteId();
                        url2 = "api/Territory/wherenew/'OnlyCountrySiteId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            OnlyCountrySiteId:OnlyCountrySiteId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    this.loading = false;

                    /* issue doc report 11102018 - default dropdown country for role site and org admin */

                    if(this.adminya){
                        if(!this.distributorya){
                            this.selectedItemsCountry = Object.keys(result).map(function (Index) {
                                return {
                                  id: result[Index].countries_code,
                                  itemName: result[Index].countries_name
                                }
                              })
                        }
                    }

                    /* end line issue doc report 11102018 - default dropdown country for role site and org admin */

                }
                this.allcountries = this.dropdownListCountry;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    certificateDropdown() {
        this.dropdownCertificate = [];
        // this.selectedCertificate = [];
        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }

    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    //show value certificate
                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    ExportExceltoXls()
    {
        this.fileName = "ProductTrainedEva.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "ProductTrainedEva.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "ProductTrainedEva.csv";
        this.ExportExcel();
     } 

    ExportExcel() {
        let jsonData = [];
        if (this.reportResult.length > 0) {
            // for (let i = 0; i < this.reportResult.length; i++) {
            //     for(let n = 0; n < this.listkey.length; n++){
            //         jsonData.push({
            //         "SiteId": this.reportResult[i].SiteId,
            //         "# of Evaluation": this.reportResult[i].Evals,
            //         });
            //     }
            // }

            // var date = this.service.formatDate();
            // var fileName = "ProductTrainedEva.xls";
            var ws = XLSX.utils.json_to_sheet(this.reportResult);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, this.fileName);
            XLSX.writeFile(wb, this.fileName);
        }
    }

    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getCountry();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoLang;
            this.dropdownListGeo = item.dropGeoLang;
            this.selectedItemsCountry = item.selectCountryLang;
            this.dropdownListCountry = item.dropCountryLang;
            this.selectedYear = item.selectYearLang;
            this.selectedPartner = item.selectPartnerLang;
            this.dropdownPartner = item.dropPartnerLang;
            this.selectedCertificate = item.selectCertiLang;
            this.dropdownCertificate = item.dropCertiLang;
            this.getReport();
        }
        else {
            this.getGeo();
            this.getCountry();
            this.getPartnerType();
            // this.selectedYear = "";
            // this.selectedPartner = [];
        }

        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid.push('"' + item.id + '"');

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
      //  var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
      var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //  var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
      //  var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

         /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        //  if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedPartner = [];
        this.selectedYear = [];
    }

    listkey = [];
    getReport() {
        this.loading = true;
        var geo = [];
        var countries = [];
        var partnerType = [];
        var certificateType = [];
        this.eventType = [];
        var course;
        var project;

        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            countries.push('"' + this.selectedItemsCountry[i].id + '"');
        }

        for (let i = 0; i < this.selectedPartner.length; i++) {
            partnerType.push(this.selectedPartner[i].id);
        }

        if (this.selectedCertificate.length > 0) {
            for (let i = 0; i < this.selectedCertificate.length; i++) {
                certificateType.push(this.selectedCertificate[i].id);
            }
        } else {
            certificateType = [];
        }

        for (let i = 0; i < this.selectedItemsGeo.length; i++) {
            geo.push('"' + this.selectedItemsGeo[i].id + '"');
        }

        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }

        var params =
        {
            selectGeoLang: this.selectedItemsGeo,
            dropGeoLang: this.dropdownListGeo,
            selectCountryLang: this.selectedItemsCountry,
            dropCountryLang: this.dropdownListCountry,
            selectYearLang: this.selectedYear,
            selectPartnerLang: this.selectedPartner,
            dropPartnerLang: this.dropdownPartner,
            selectCertiLang: this.selectedCertificate,
            dropCertiLang: this.dropdownCertificate,
            siteActive: this.SiteActive
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        // "api/EvaluationAnswer/SubmissionByLanguage/{'CountryCode':'" + countries + "','PartnerType':'" + partnerType + "','AAP_Course':'" + course +
        // "','AAP_Project':'" + project + "'}"

        var contactid = "";
        var orgid = "";
        var distributor = "";
        var siteid = "";

        // if(this.checkrole()){
        //     if(this.itsDistributor()){
        //         distributor = this.urlGetOrgId();
        //     }else{
        //         if(this.itsinstructor()){
        //             contactid = this.useraccesdata.UserId;
        //         }else{
        //             orgid = this.urlGetOrgId();
        //         }
        //     }
        // }

        /* new request nambah user role -__ */

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         distributor = this.urlGetOrgId();
        //     } else {
        //         if (this.itsOrganization()) {
        //             orgid = this.urlGetOrgId();
        //         } else {
        //             if (this.itsSite()) {
        //                 siteid = this.urlGetSiteId();
        //             } else {
        //                 contactid = this.useraccesdata.UserId;
        //             }
        //         }
        //     }
        // }

        /* new request nambah user role -__ */

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        if (this.adminya) {
            if(this.distributorya){
                distributor = this.urlGetOrgId();
            }else{
                if(this.orgya){
                    orgid = this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        siteid = this.urlGetSiteId();
                    }else{
                        contactid = this.useraccesdata.UserId;
                    }
                }
            }
        }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var keyword = {
            CountryCode: countries.toString(),
            PartnerType: partnerType.toString(),
            CertificateType: certificateType.toString(),
            GeoCode: geo.toString(),
            Year: this.selectedYear,
            EventType: this.eventType.toString(),
            SiteActive: this.SiteActive,
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor,
            SiteId: siteid,
        };

        this.reportResult = [];
        this.listkey = [];

        var data: any;
        var temp_total: any;
        // this.evalTotal = 0;
        let data_site = [];
        let sites: any;
        let total: any;
        this.totalEval = 0;
        this.header = [];

        /* autodesk plan 18 oct */

        this.apiCall = this.service.httpClientPost("api/EvaluationAnswer/ProductTrained", keyword)
            .subscribe(res => {
               // data = res.replace(/\t/g, " ");
               data = res;
                if (data != "hah...kossssongg??") {
                    //data = JSON.parse(data);
                    if (data.length > 0) {
                        this.reportResult = data;
                        for (let key in this.reportResult[0]) {
                            this.listkey.push(key);
                        }
                        //Untuk nyari total evaluationnya
                        this.service.httpClientPost("api/EvaluationAnswer/TotalProductTrained", keyword)
                            .subscribe(resTot => {
                                total = resTot;
                                if (total != null && total != undefined) {
                                    this.totalEval = total.Jumlah;
                                } else {
                                    this.totalEval = 0;
                                }
                                this.dataFound = true;
                                this.loading = false;
                            });
                    } else {
                        this.reportResult = [];
                        this.totalEval = 0;
                        this.dataFound = true;
                        this.loading = false;
                    }
                }else{ /* issue doc report 11102018 - filter on inactive site or no certificate type keep loading */
                    this.reportResult = [];
                    this.totalEval = 0;
                    this.dataFound = true;
                    this.loading = false;
                }
            }, error => {
                this.reportResult = [];
                this.dataFound = true;
                this.loading = false;
            });

        /* end line autodesk plan 18 oct */

    }
}