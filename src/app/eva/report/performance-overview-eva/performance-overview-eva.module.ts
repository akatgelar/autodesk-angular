import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { PerformanceOverviewEVAComponent,ConvertPercent } from './performance-overview-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const PerformanceOverviewEVARoutes: Routes = [
    {
        path: '',
        component: PerformanceOverviewEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.performance_overview.performance_overview',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PerformanceOverviewEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [PerformanceOverviewEVAComponent,ConvertPercent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class PerformanceOverviewEVAModule { }