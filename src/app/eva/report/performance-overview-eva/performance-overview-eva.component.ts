import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { DecimalPipe } from '@angular/common';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';
//import { resolve } from 'path';


@Pipe({ name: 'convertPercent' })
export class ConvertPercent {
    transform(value: string): any {
        return Number.parseFloat(value).toFixed(2);;
    }
}

@Component({
    selector: 'app-performance-overview-eva',
    templateUrl: './performance-overview-eva.component.html',
    styleUrls: [
        './performance-overview-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class PerformanceOverviewEVAComponent implements OnInit {

    apiCall:any
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    Year;
    YearSelected;
    DateSelected;
    date;
    SiteActive;
    performanceReport = [];
    totalEva: number = 0;
    countries = [];
    partnerType = [];
    certificateType = [];
    geoCode = [];
    course: any;
    project: any;
    dataProduct;
    ProductSelected = "";
    eventType = [];
    public showEvent = false;
    public loading = false;
    useraccesdata: any;
    distTerritory = "";
    fileName = "PerformanceOverviewReport.xls";
    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private route: ActivatedRoute, private router: Router, private session: SessionService) {
        this.date = [
            { id: "", name: "Annual Year" },
            { val: 2, name: "February" },
            { val: 3, name: "March" },
            { val: 4, name: "April" },
            { val: 5, name: "May" },
            { val: 6, name: "June" },
            { val: 7, name: "July" },
            { val: 8, name: "August" },
            { val: 9, name: "September" },
            { val: 10, name: "October" },
            { val: 11, name: "November" },
            { val: 12, name: "December" },
            { val: 1, name: "January" },
            { val: "q1", name: "Q1" },
            { val: "q2", name: "Q2" },
            { val: "q3", name: "Q3" },
            { val: "q4", name: "Q4" }
        ];

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    // getGeo() {
    //     // get Geo
    //     var data = '';
    //     this.service.get("api/Geo", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListGeo = null;
    //             }
    //             else {
    //                 this.dropdownListGeo = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.geo_id,
    //                         itemName: item.geo_name
    //                     }
    //                 })
    //                 this.selectedItemsGeo = this.dropdownListGeo;
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    //     // this.selectedItemsGeo = [];
    // }

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        var url = "api/Geo/SelectAdmin";
        
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                    this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    certificateDropdown() {
        let certificate: any;
        let temp = [];
        this.dropdownCertificate = [];

        /* issue 17102018 - automatic remove when change selected partner type */

        this.dropdownEventType = [];
        this.selectedEventType = [];
        
        /* end line issue 17102018 - automatic remove when change selected partner type */

        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.Year = res;
                this.YearSelected = this.Year[this.Year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    getFY1() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.Year = res;
                // this.YearSelected = this.Year[this.Year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    getProduct() {
        let data: any;
        this.service.httpClientGet("api/Product/ProductMaster", data)
            .subscribe(res => {
                // data = res;
                // data = data.replace(/\t|\n|\r|\f|\\|\/|'/g, " ");
                // data = JSON.parse(data);
                this.dataProduct = res;
            });
    }

    // getCountry(){
    //     let country: any;
    //     this.service.httpClientGet("api/Countries", country)
    //         .subscribe(res => {
    //             country = res;
    //             this.dropdownListCountry = country.map((item)=>{
    //                 return{
    //                     id: item.countries_code,
    //                     itemName: item.countries_name
    //                 }
    //             });
    //         });   
    // }

    allcountries = [];
    getCountry() {
        this.loading = true;
        var data = '';

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var url2 = "api/Countries/SelectAdmin";
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url2 = "api/Territory/where/{'OnlyCountrySiteId':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var DistributorCountry = null; 
        var OnlyCountryOrgId = null;
        var OnlyCountrySiteId = null;
        var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountrySiteId = this.urlGetSiteId();
                        url2 = "api/Territory/wherenew/'OnlyCountrySiteId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            OnlyCountrySiteId:OnlyCountrySiteId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    this.loading = false;
                }
                this.allcountries = this.dropdownListCountry;

                /* issue doc report 11102018 - default dropdown country for role site and org admin */

                if(this.adminya){
                    if(!this.distributorya){
                        this.selectedItemsCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                }

                /* end line issue doc report 11102018 - default dropdown country for role site and org admin */
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                    //Show value to dropdown certificate
                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getCountry();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.getFY1();
            this.selectedItemsGeo = item.selectGeoPerf;
            this.dropdownListGeo = item.dropGeoPerf;
            this.selectedItemsRegion = item.selectRegionPerf;
            this.dropdownListRegion = item.dropRegionPerf;
            this.selectedItemsSubRegion = item.selectSubRegionPerf;
            this.dropdownListSubRegion = item.dropSubRegionPerf;
            this.selectedItemsCountry = item.selectCountryPerf;
            this.dropdownListCountry = item.dropCountryPerf;
            this.selectedPartner = item.selectPartnerPerf;
            this.dropdownPartner = item.dropPartnerPerf;
            this.selectedCertificate = item.selectCertiPerf;
            this.dropdownCertificate = item.dropCertiPerf;
            this.YearSelected = parseInt(item.selectYearPerf);
            this.DateSelected = item.selectDatePerf;
            this.SiteActive = item.siteActivePerf;
            this.ProductSelected = item.selectProductPerf;
            this.getReport();
        }
        else {
            this.getGeo();
            this.getCountry();
            this.getPartnerType();
            // this.YearSelected = "";
            this.DateSelected = "";
            // this.selectedPartner = [];
            this.getFY();
        }
        this.getProduct();
        this.SiteActive = 'A';
        this.rowsOnPage = 10;
        this.performanceReport = [];
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    ExportExceltoXls()
    {
        this.fileName = "PerformanceOverviewReport.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "PerformanceOverviewReport.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "PerformanceOverviewReport.csv";
        this.ExportExcel();
     }

    ExportExcel() {
        let jsonData = [];
        if (this.performanceReport.length > 0) {
            for (let i = 0; i < this.performanceReport.length; i++) {
                jsonData.push({
                    "Site ID": this.performanceReport[i].SiteId,
                    "Survey Partner Type": this.performanceReport[i].PartnerType,
                    "Site Name": this.performanceReport[i].SiteName,
                    "Country": this.performanceReport[i].countries_name,
                    "# of Evaluation": parseInt(this.performanceReport[i].Evals),
                    "F&R %":Number(this.performanceReport[i].FR) + "%",
                    "IQ %": Number(this.performanceReport[i].IQ) + "%",
                    "CC&M %": Number(this.performanceReport[i].CCM) + "%",
                    "OP %": Number(this.performanceReport[i].OP) + "%",
                    "OE %": Number(this.performanceReport[i].OE.toFixed(2).toString()) + "%",
                    "COE": Number(this.performanceReport[i].COE.toFixed(2).toString()),
                });
            }

            // var date = this.service.formatDate();
          //  var fileName = "PerformanceOverviewReport.xls";
            var ws = XLSX.utils.json_to_sheet(jsonData);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, this.fileName);
            XLSX.writeFile(wb, this.fileName);
        }
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory         
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"

        // Countries
        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
       var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); 
        //             }
        //         }
        //     }
        // }

        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory        
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        //       //  url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //             url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); 
        //             }
        //         }
        //     }
        // }

        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory       
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

      //  this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
      this.service.httpClientPost(url, keyword)
        .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    //console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }

    // onGeoSelect(item: any) {
    //     this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // OnGeoDeSelect(item: any) {
    //     this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoSelectAll(items: any) {
    //     this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.selectedItemsRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelect(item: any) {
    //     this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnRegionDeSelect(item: any) {
    //     this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelectAll(items: any) {
    //     this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelectAll(items: any) {
    //     this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedPartner = [];
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    redirectsite(siteId, year, productId) {
        // this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteId: siteId, filterBySurveyYear: year, filterByDate: this.DateSelected, filterByPartnerType: roleId, filterByCertificateType: this.certificateType.toString(), CourseId: courseId } });
		// console.log(productId);
        this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteId: siteId, filterBySurveyYear: year, filterByDate: this.DateSelected, ProductId: productId } });
    }

    Product2Selected:string = ""; // issue 15112018
    getReport() {
        this.loading = true;
        this.countries = [];
        this.partnerType = [];
        this.certificateType = [];
        this.eventType = [];
        this.geoCode = [];

        //Country
        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            this.countries.push('"' + this.selectedItemsCountry[i].id + '"');
        }

        //Partner type
        for (let i = 0; i < this.selectedPartner.length; i++) {
            this.partnerType.push('"' + this.selectedPartner[i].id + '"');
        }

        //Certificate type
        if (this.selectedCertificate.length > 0) {
            for (let i = 0; i < this.selectedCertificate.length; i++) {
                this.certificateType.push('"' + this.selectedCertificate[i].id + '"');
            }
        } else {
            this.certificateType = [];
        }

        //Geo
        for (let n = 0; n < this.selectedItemsGeo.length; n++) {
            this.geoCode.push('"' + this.selectedItemsGeo[n].id + '"');
        }

        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }

        var params =
        {
            selectGeoPerf: this.selectedItemsGeo,
            dropGeoPerf: this.dropdownListGeo,
            selectRegionPerf: this.selectedItemsRegion,
            dropRegionPerf: this.dropdownListRegion,
            selectSubRegionPerf: this.selectedItemsSubRegion,
            dropSubRegionPerf: this.dropdownListSubRegion,
            selectCountryPerf: this.selectedItemsCountry,
            dropCountryPerf: this.dropdownListCountry,
            selectPartnerPerf: this.selectedPartner,
            dropPartnerPerf: this.dropdownPartner,
            selectCertiPerf: this.selectedCertificate,
            dropCertiPerf: this.dropdownCertificate,
            selectYearPerf: this.YearSelected,
            selectDatePerf: this.DateSelected,
            siteActivePerf: this.SiteActive,
            selectProductPerf: this.ProductSelected
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        // "api/EvaluationAnswer/PerformanceReport/{'Geo':'" + this.geoCode + "','CountryCode':'" + this.countries + "','Year':'" + this.YearSelected +
        // "','Date':'" + this.DateSelected + "','SiteActive':'" + this.SiteActive + "','PartnerType':'" + this.partnerType + "','CertificateType':'" + this.certificateType.toString() + "','Product':'" + this.ProductSelected + "'}"

        // var distTemp = "";
        // if(this.itsDistributor()){
        //     distTemp = this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var contactid = "";
        var orgid = "";
        var distributor = "";
        var siteid = "";

        if (this.adminya) {
            if(this.distributorya){
                distributor = this.urlGetOrgId();
            }else{
                if(this.orgya){
                    orgid = this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        siteid = this.urlGetSiteId();
                    }else{
                        contactid = this.useraccesdata.UserId;
                    }
                }
            }
        }
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var keyword = {
            Geo: this.geoCode.toString(),
            CountryCode: this.countries.toString(),
            Year: this.YearSelected,
            Date: this.DateSelected,
            SiteActive: this.SiteActive,
            PartnerType: this.partnerType.toString(),
            CertificateType: this.certificateType.toString(),
            Product: this.ProductSelected,
            Product2: this.Product2Selected,
            // EventType: this.eventType.toString(),
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor,
            SiteId: siteid
        };

        // console.log(keyword);
        this.performanceReport = [];
        let sites = [];
        var performance: any;
        var totalEvals: any;
        var sumScoreOE: any;
        this.totalEva = 0;
        /* new post for autodesk plan 17 oct */
        this.apiCall = this.service.httpClientPost("api/EvaluationAnswer/PerformanceReport", keyword)
            .subscribe(res => {
                performance = res;
                // console.log(performance);
                if (performance.length > 0) {
                    for (let i = 0; i < performance.length; i++) {
                        this.totalEva += parseInt(performance[i].Evals);
                        sites.push('"' + performance[i].SiteId + '"');
                    }
                    //pencarian pertama
                    this.performanceReport = performance;

                    //untuk nyari total eval nya
                    keyword["SiteId"] = sites.toString();
                    this.service.httpClientPost("api/EvaluationAnswer/GetCOE", keyword)
                        .subscribe(res => {
                            totalEvals = res;
                            if (totalEvals.length > 0) {
                                //untuk nyari sumScoreOE-nya
                                this.service.httpClientPost("api/EvaluationAnswer/GetOE", keyword)
                                    .subscribe(res => {
                                        sumScoreOE = res;
                                        let ifKetemu = false;
                                        let OE_temp = 0;
                                        //susun ulang report, tambahkan perhitungan OE dan COE
                                        for (let n = 0; n < this.performanceReport.length; n++) {
                                            // if (this.performanceReport[n].SiteId == totalEvals[n].SiteId) {
                                                var OE = 0;
                                                var COE = 0;

                                                //Perhitungan OE
                                                for(let f = 0; f < sumScoreOE.length; f++){
                                                    if(this.performanceReport[n].SiteId == sumScoreOE[f].SiteId && this.performanceReport[n].RoleId == sumScoreOE[f].RoleId){
                                                        ifKetemu = true;
                                                        OE_temp = parseInt(sumScoreOE[f].scoreOE)
                                                    }
                                                }
                                                
                                                if(ifKetemu == true && OE_temp != 0){
                                                    if (parseInt(this.performanceReport[n].Evals) > 99) {
                                                        OE = (OE_temp / parseInt(this.performanceReport[n].Evals)) * 100
                                                    } else {
                                                        OE = 0;
                                                    }
                                                } else{
                                                    OE = 0;
                                                }
                                                
                                                //Perhitungan COE
                                                if (parseInt(this.performanceReport[n].Evals) == 0) {
                                                    COE = 0;
                                                } else {
                                                    COE = OE - (Math.sqrt(OE * (100 - OE) / parseInt(this.performanceReport[n].Evals)) * 2.56);
                                                }

                                                this.performanceReport[n]["OE"] = OE;
                                                this.performanceReport[n]["COE"] = COE;
                                                ifKetemu = false;
                                                OE_temp = 0;
                                            // }
                                        }
                                        // console.log(this.performanceReport);
                                    });
                            }
                        });
                    this.dataFound = true;
                    this.loading = false;
                } else {
                    this.performanceReport = [];
                    this.dataFound = true;
                    this.loading = false;
                }
            }, error => {
                this.performanceReport = [];
                this.dataFound = true;
                this.loading = false;
            });
        /* new post for autodesk plan 17 oct */
    }
}
