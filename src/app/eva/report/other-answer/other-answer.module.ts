import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { OtherAnswerEVAComponent } from './other-answer.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

export const OtherAnswerEVARoutes: Routes = [
    {
        path: '',
        component: OtherAnswerEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.other_answer.other_answer',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(OtherAnswerEVARoutes),
        SharedModule,
        AngularMultiSelectModule
    ],
    declarations: [OtherAnswerEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class OtherAnswerEVAModule { }