import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

@Component({
    selector: 'app-other-answer',
    templateUrl: './other-answer.component.html',
    styleUrls: [
        './other-answer.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class OtherAnswerEVAComponent implements OnInit {

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    year

    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo) { 

    }

    getGeo() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Geo", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_id,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
          .subscribe(res => {
            this.year = res;
          }, error => {
            this.service.errorserver();
          });
    }

    ngOnInit() {
        this.getGeo();
        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }
}