import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'toNumber' })
export class ToNumberPipe implements PipeTransform {
    transform(value: string): any {
        var newvalue = value.replace(',', '.');
        let retNumber = Number(newvalue);
        return isNaN(retNumber) ? 0 : retNumber;
    }
}

@Component({
    selector: 'app-instructor-performance-eva',
    templateUrl: './instructor-performance-eva.component.html',
    styleUrls: [
        './instructor-performance-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class InstructorPerformanceEVAComponent implements OnInit {

    apiCall:any
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};

    public data: any = '';
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    year: any;
    queryForm: FormGroup;
    date = [];
    siteActive = [];
    partnertypearr = [];
    certificateType = [];
    eventType = [];
    public showEvent = false;
    public loading = false;
    public totalEvaluation: number = 0;
    useraccesdata: any;
    fileName = "InstructorReport.xls";
    constructor(private router: Router, public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {
        // let filterByYear = new FormControl(new Date().getFullYear());
        this.date = [
            { id: "", name: "Annual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];

        this.siteActive = [
            { id: "A", name: "Active Only" },
            { id: "X", name: "Inactive Only" }
        ];

        let filterByYear = new FormControl('');
        let filterByDate = new FormControl(1);
        let filterBySiteStatus = new FormControl('A');

        this.queryForm = new FormGroup({
            filterByYear: filterByYear,
            filterByDate: filterByDate,
            filterBySiteStatus: filterBySiteStatus
        });

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    /* I have logged in as an ATC Site Admin and I can’t run the Instructor Performance Report. 
    As I’m assigned to the EMEA region, I shouldn’t need to select a GEO */
    
    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        var url = "api/Geo/SelectAdmin";
       
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } 

        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
        //         else {
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }
        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
                
        //         // else if (this.itsSite()) {
        //         //     url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         // } 
        //         // else {
        //         //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         // }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {
         
            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
       
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                 
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory: tmpTerritory        
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''";

        // Countries
        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory: tmpTerritory           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 //url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory: tmpTerritory           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }

    /* end I have logged in as an ATC Site Admin and I can’t run the Instructor Performance Report. As I’m assigned to the EMEA region, I shouldn’t need to select a GEO */
    ExportExceltoXls()
    {
        this.fileName = "InstructorReport.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "InstructorReport.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "InstructorReport.csv";
        this.ExportExcel();
     }

    ExportExcel() {
        let jsonData = [];
        if (this.data.length > 0) {
            for (let i = 0; i < this.data.length; i++) {
                jsonData.push({
                    "Site ID": this.data[i].SiteId,
                    "Survey Partner Type": (this.data[i].RoleId == 1) ? 'ATC' : 'AAP',
                    "Site Name": this.data[i].SiteName,
                    "Instructor ID": this.data[i].InstructorId,
                    "Instructor Name": this.data[i].ContactName,
                    "# of Evaluation": parseInt(this.data[i].Evals),
                    "WDT %": Number(this.data[i].WDT) + "%"
                });
            }
            // var date = this.service.formatDate();
          //  var fileName = "InstructorReport.xls";
            var ws = XLSX.utils.json_to_sheet(jsonData);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, this.fileName);
            XLSX.writeFile(wb, this.fileName);
        }
    }

    certificateDropdown() {
        let certificate: any;
        let temp = [];
        this.dropdownCertificate = [];

        /* issue 17102018 - automatic remove when change selected partner type */

        this.dropdownEventType = [];
        this.selectedEventType = [];
        
        /* end line issue 17102018 - automatic remove when change selected partner type */

        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }
        // this.selectedCertificate = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.queryForm.patchValue({ filterByYear: this.year[this.year.length - 1].Key });
            }, error => {
                this.service.errorserver();
            });
    }

    //Fix isu no.263
    DistributorCheck(): boolean {
        var user_lvl = this.useraccesdata.UserLevelId.split(",");
        for (let n = 0; n < user_lvl.length; n++) {
            if (user_lvl[n] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    GetCurrentOrg() {
        let org = this.useraccesdata.OrgId.split(",");
        let current_org = [];
        for (let i = 0; i < org.length; i++) {
            current_org.push('"' + org[i] + '"');
        }
        return current_org;
    }

    allcountries = [];
    getCountry() {
        this.loading = true;
        // let country: any;
        //Fix isu no.263
        /*let isDistributor = this.DistributorCheck();
        let current_org = this.GetCurrentOrg();
        this.service.httpClientGet("api/Countries/" + isDistributor + "/" + current_org, country)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = JSON.parse(country).map((item) => {
                        return {
                            id: item.countries_code,
                            itemName: item.countries_name
                        }
                    })
                    this.loading = false;
                }
                this.allcountries = this.dropdownListCountry;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];*/
        
        var url2 = "api/Countries/SelectAdmin";
        
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; // tambah kalo site / org country nya based org
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 // url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //                 url2 = "api/Territory/where/{'OnlyCountrySiteId':'" + this.urlGetSiteId() + "'}"; /* fixed issue 24092018 - populate country by role site admin */
        //             }else{
        //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;var OnlyCountrySiteId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountrySiteId = this.urlGetSiteId();
                        url2 = "api/Territory/wherenew/'OnlyCountrySiteId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            OnlyCountrySiteId:OnlyCountrySiteId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
                this.allcountries = this.dropdownListCountry;
                this.loading = false;

                /* issue doc report 11102018 - default dropdown country for role site and org admin */

                if(this.adminya){
                    if(!this.distributorya){
                        this.selectedItemsCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                }

                /* end line issue doc report 11102018 - default dropdown country for role site and org admin */

            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    getPartnerType() {
        // var data = '';
        // this.service.get("api/Roles/where/{'RoleType': 'Company'}", data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             this.dropdownPartner = null;
        //         }
        //         else {
        //             this.dropdownPartner = JSON.parse(result).map((item) => {
        //                 return {
        //                     id: item.RoleId,
        //                     itemName: item.RoleName
        //                 }
        //             });
        //             this.selectedPartner = JSON.parse(result).map((item) => {
        //                 return {
        //                     id: item.RoleId,
        //                     itemName: item.RoleName
        //                 }
        //             })
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                      this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    //Show value to dropdown certificate
                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getCountry();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoTor;
            this.dropdownListGeo = item.dropGeoTor;
            this.selectedItemsRegion = item.selectRegionTor;
            this.dropdownListRegion = item.dropRegionTor;
            this.selectedItemsSubRegion = item.selectSubRegionTor;
            this.dropdownListSubRegion = item.dropSubRegionTor;
            this.selectedItemsCountry = item.selectCountryTor;
            this.dropdownListCountry = item.dropCountryTor;
            this.selectedPartner = item.selectPartnerTor;
            this.dropdownPartner = item.dropPartnerTor;
            this.selectedCertificate = item.selectCertiTor;
            this.dropdownCertificate = item.dropCertiTor;
            this.queryForm.patchValue({ filterByYear: item.yearTor });
            this.queryForm.patchValue({ filterByDate: item.dateTor });
            this.queryForm.patchValue({ filterBySiteStatus: item.siteTor });
            this.onSubmit();
        }
        else {
            this.getGeo();
            this.getCountry();
            this.getPartnerType();
            // this.selectedPartner = [];
        }
        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    showhiperlink: boolean = true;
    onSubmit() {
        this.loading = true;
        this.data = null;
        this.partnertypearr = [];
        this.eventType = [];

        let geoarr = [];
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }

        let regionarr = [];
        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
        }

        let subregionarr = [];
        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
        }

        let countriesarr = [];
        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countriesarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }

        for (var i = 0; i < this.selectedPartner.length; i++) {
            this.partnertypearr.push("'" + this.selectedPartner[i].id + "'");
        }

        //Certificate type
        this.certificateType = [];
        if (this.selectedCertificate.length > 0) {
            for (let i = 0; i < this.selectedCertificate.length; i++) {
                this.certificateType.push(this.selectedCertificate[i].id);
            }
        } else {
            this.certificateType = [];
        }

        //cek jika dalam pencarian terdapat pencarian berdasarkan AAP Course maka cantumkan pula certificate type untuk project, karena di aplikasi eksisting nya seperti itu jek.
        //Kalau tidak seperti itu nanti total dan data yang ditampilkan nya akan berbeda dengan sistem eksisting, turutan we naon nu dikahayang ku dunungan nakarin mah

        //kalau yg dipilih hanya aap course tanpa aap project, sertakan project nya jek
        //kudu na mah ngan aya 2 siki, aap course sama project (tapi kahayang didinya na kitu, nya ges we turutan lah)
        if (this.certificateType.indexOf("1") != -1 && this.certificateType.indexOf("2") == -1) {
            this.certificateType.push("2");
        }

        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }

        var params =
        {
            selectGeoTor: this.selectedItemsGeo,
            dropGeoTor: this.dropdownListGeo,
            selectRegionTor: this.selectedItemsRegion,
            dropRegionTor: this.dropdownListRegion,
            selectSubRegionTor: this.selectedItemsSubRegion,
            dropSubRegionTor: this.dropdownListSubRegion,
            selectCountryTor: this.selectedItemsCountry,
            dropCountryTor: this.dropdownListCountry,
            selectPartnerTor: this.selectedPartner,
            dropPartnerTor: this.dropdownPartner,
            selectCertiTor: this.selectedCertificate,
            dropCertiTor: this.dropdownCertificate,
            yearTor: this.queryForm.value.filterByYear,
            dateTor: this.queryForm.value.filterByDate,
            siteTor: this.queryForm.value.filterBySiteStatus
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        // 'api/ReportEva/InstructorPerformance/{"filterByGeo":"' + geoarr.toString() + '","filterByRegion":"' + regionarr.toString() + '","filterBySubRegion":"' + subregionarr.toString() + '","filterByCountries":"' + countriesarr.toString() +
        // '","filterBySurveyYear":"' + this.queryForm.value.filterByYear + '","filterByDate":"' + this.queryForm.value.filterByDate + '","filterBySiteStatus":"' + this.queryForm.value.filterBySiteStatus + '","filterByPartnerType":"' + this.partnertypearr.toString() + '"}'

        var contactid = "";
        var orgid = "";
        var distributor = "";
        var siteid = "";

        // if (this.checkrole()) {
        //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        //     for (var i = 0; i < userlvlarr.length; i++) {
        //         if (userlvlarr[i] == "DISTRIBUTOR" || userlvlarr[i] == "ORGANIZATION") {
        //             orgid = this.urlGetOrgId();
        //         } else {
        //             contactid = this.useraccesdata.UserId;
        //         }
        //     }
        // }

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         distributor = this.urlGetOrgId();
        //     } else {
        //         if (this.itsinstructor()) {
        //             contactid = this.useraccesdata.UserId;
        //             this.showhiperlink = false;
        //         } else {
        //             orgid = this.urlGetOrgId();
        //         }
        //     }
        // }

        // /* new request nambah user role -__ */

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         distributor = this.urlGetOrgId();
        //     } else {
        //         if (this.itsOrganization()) {
        //             orgid = this.urlGetOrgId();
        //         } else {
        //             if (this.itsSite()) {
        //                 siteid = this.urlGetSiteId();
        //             } else {
        //                 contactid = this.useraccesdata.UserId;
        //                 this.showhiperlink = false;
        //             }
        //         }
        //     }
        // }

        // /* new request nambah user role -__ */

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        if (this.adminya) {
            if(this.distributorya){
                distributor = this.urlGetOrgId();
            }else{
                if(this.orgya){
                    orgid = this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        siteid = this.urlGetSiteId();
                    }else{
                        contactid = this.useraccesdata.UserId;
                        this.showhiperlink = false;
                    }
                }
            }
        }
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var keyword = {
            filterByGeo: geoarr.toString(),
            // filterByRegion: regionarr.toString(),
            // filterBySubRegion: subregionarr.toString(),
            filterByCountries: countriesarr.toString(),
            filterBySurveyYear: this.queryForm.value.filterByYear,
            filterByDate: this.queryForm.value.filterByDate,
            filterBySiteStatus: this.queryForm.value.filterBySiteStatus,
            filterByPartnerType: this.partnertypearr.toString(),
            filterByCertificateType: this.certificateType.toString(),
            // EventType: this.eventType.toString(),
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor,
            SiteId: siteid
        };

        // console.log(keyword)

        this.totalEvaluation = 0;

        /* autodesk plan 18 oct */

        this.apiCall = this.service.httpClientPost("api/ReportEva/InstructorPerformance", keyword)
            .subscribe(res => {
                if (res == "Not Found") {
                    this.data = '';
                    this.loading = false;
                    this.totalEvaluation = 0;
                } else {
                    this.data = res;
                    for (let i = 0; i < this.data.length; i++) {
                        this.totalEvaluation += parseInt(this.data[i].Evals);
                    }
                    this.dataFound = true;
                    this.loading = false;
                }
            }, error => {
                this.data = '';
                this.dataFound = true;
                this.loading = false;
                this.totalEvaluation = 0;
            });

        /* end line autodesk plan 18 oct */
    }

    redirectsite(siteId, instructor_id) {
        this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteId: siteId, filterBySurveyYear: this.queryForm.value.filterByYear, filterByDate: this.queryForm.value.filterByDate } });
    }

    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedPartner = [];
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    // arraygeoid = [];
    // onGeoSelect(item: any) {
    //     this.dataFound = false;
    //     this.arraygeoid.push('"' + item.id + '"');

    //     // isu no.263
    //     let isDistributor = this.DistributorCheck();
    //     let current_org = this.GetCurrentOrg();

    //     // Region
    //     var data = '';
    //     this.service.get("api/RegionFilter/" + this.arraygeoid.toString() + "/" + isDistributor + "/" + current_org, data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListRegion = null;
    //             }
    //             else {
    //                 this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.region_code,
    //                         itemName: item.region_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListSubRegion = null;
    //             }
    //             else {
    //                 this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.subregion_code,
    //                         itemName: item.subregion_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });


    // }

    //  OnGeoDeSelect(item: any) {
    //     var data = '';

    //     //split region
    //     let regionArrTmp = [];
    //     this.service.get("api/Region/filter/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 regionArrTmp = null;
    //             }
    //             else {
    //                 regionArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < regionArrTmp.length; i++) {
    //                     var index = this.selectedItemsRegion.findIndex(x => x.id == regionArrTmp[i].region_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsRegion.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     //split sub region
    //     let subregionArrTmp = [];
    //     this.service.get("api/SubRegion/filter/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 subregionArrTmp = null;
    //             }
    //             else {
    //                 subregionArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < subregionArrTmp.length; i++) {
    //                     var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsSubRegion.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filter/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 countriesArrTmp = null;
    //             }
    //             else {
    //                 countriesArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < countriesArrTmp.length; i++) {
    //                     var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsCountry.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     var index = this.arraygeoid.findIndex(x => x == '"' + item.id + '"');
    //     this.arraygeoid.splice(index, 1);
    //     this.selectedItemsRegion.splice(index, 1);
    //     this.selectedItemsSubRegion.splice(index, 1);
    //     this.selectedItemsCountry.splice(index, 1);

    //     if (this.arraygeoid.length > 0) {
    //         // Region
    //         var data = '';
    //         this.service.get("api/Region/filter/" + this.arraygeoid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListRegion = null;
    //                 }
    //                 else {
    //                     this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.region_code,
    //                             itemName: item.region_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });

    //         // SubRegion
    //         var data = '';
    //         this.service.get("api/SubRegion/filter/" + this.arraygeoid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListSubRegion = null;
    //                 }
    //                 else {
    //                     this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.subregion_code,
    //                             itemName: item.subregion_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });

    //         // Countries
    //         var data = '';
    //         this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListCountry = null;
    //                 }
    //                 else {
    //                     this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.countries_code,
    //                             itemName: item.countries_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });
    //     }
    //     else {
    //         // this.dropdownListRegion = this.allregion;
    //         // this.dropdownListSubRegion = this.allsubregion;
    //         // this.dropdownListCountry = this.allcountries;

    //         this.selectedItemsRegion.splice(index, 1);
    //         this.selectedItemsSubRegion.splice(index, 1);
    //         this.selectedItemsCountry.splice(index, 1);
    //     }
    // }

    // onGeoSelectAll(items: any) {
    //     // console.log(items);
    //     this.dataFound = false;
    //     this.arraygeoid = [];

    //     for (var i = 0; i < items.length; i++) {
    //         this.arraygeoid.push('"' + items[i].id + '"');
    //     }

    //     // Region
    //     var data = '';
    //     this.service.get("api/Region/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListRegion = null;
    //             }
    //             else {
    //                 this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.region_code,
    //                         itemName: item.region_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListSubRegion = null;
    //             }
    //             else {
    //                 this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.subregion_code,
    //                         itemName: item.subregion_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    // }



    // arrayregionid = [];

    // onRegionSelect(item: any) {
    //     this.dataFound = false;
    //     this.arrayregionid.push('"' + item.id + '"');

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListSubRegion = null;
    //             }
    //             else {
    //                 this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.subregion_code,
    //                         itemName: item.subregion_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // OnRegionDeSelect(item: any) {
    //     this.dataFound = false;
    //     //split sub region
    //     let subregionArrTmp = [];
    //     this.service.get("api/SubRegion/filterregion/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 subregionArrTmp = null;
    //             }
    //             else {
    //                 subregionArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < subregionArrTmp.length; i++) {
    //                     var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsSubRegion.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filterregion/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 countriesArrTmp = null;
    //             }
    //             else {
    //                 countriesArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < countriesArrTmp.length; i++) {
    //                     var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsCountry.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
    //     this.arrayregionid.splice(index, 1);
    //     this.selectedItemsSubRegion.splice(index, 1);
    //     this.selectedItemsCountry.splice(index, 1);

    //     if (this.arrayregionid.length > 0) {
    //         // SubRegion
    //         var data = '';
    //         this.service.get("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListSubRegion = null;
    //                 }
    //                 else {
    //                     this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.subregion_code,
    //                             itemName: item.subregion_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });

    //         // Countries
    //         var data = '';
    //         this.service.get("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListCountry = null;
    //                 }
    //                 else {
    //                     this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.countries_code,
    //                             itemName: item.countries_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });
    //     }
    //     else {
    //         this.dropdownListSubRegion = [];
    //         this.dropdownListCountry = [];

    //         this.selectedItemsSubRegion.splice(index, 1);
    //         this.selectedItemsCountry.splice(index, 1);
    //     }
    // }
    // onRegionSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.arrayregionid = [];

    //     for (var i = 0; i < items.length; i++) {
    //         this.arrayregionid.push('"' + items[i].id + '"');
    //     }

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListSubRegion = null;
    //             }
    //             else {
    //                 this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.subregion_code,
    //                         itemName: item.subregion_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    // }

    // arraysubregionid = [];

    // onSubRegionSelect(item: any) {
    //     this.dataFound = false;
    //     this.arraysubregionid.push('"' + item.id + '"');

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.dataFound = false;
    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filtersubregion/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 countriesArrTmp = null;
    //             }
    //             else {
    //                 countriesArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < countriesArrTmp.length; i++) {
    //                     var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsCountry.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
    //     this.arraysubregionid.splice(index, 1);
    //     this.selectedItemsCountry.splice(index, 1);

    //     if (this.arraysubregionid.length > 0) {
    //         // Countries
    //         var data = '';
    //         this.service.get("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListCountry = null;
    //                 }
    //                 else {
    //                     this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.countries_code,
    //                             itemName: item.countries_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });
    //     }
    //     else {
    //         this.dropdownListCountry = [];

    //         this.selectedItemsCountry.splice(index, 1);
    //     }
    // }
    // onSubRegionSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.arraysubregionid = [];

    //     for (var i = 0; i < items.length; i++) {
    //         this.arraysubregionid.push('"' + items[i].id + '"');
    //     }

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    // }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }



    onCountriesSelect(item: any) { this.dataFound = false; }
    OnCountriesDeSelect(item: any) { this.dataFound = false; }
    onCountriesSelectAll(items: any) { this.dataFound = false; }
    onCountriesDeSelectAll(items: any) { this.dataFound = false; }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }
}
