import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { InstructorPerformanceEVAComponent } from './instructor-performance-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { ToNumberPipe } from './instructor-performance-eva.component';
import { LoadingModule } from 'ngx-loading';

export const InstructorPerformanceEVARoutes: Routes = [
    {
        path: '',
        component: InstructorPerformanceEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.instructor_performance.instructor_performance',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(InstructorPerformanceEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [InstructorPerformanceEVAComponent, ToNumberPipe],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class InstructorPerformanceEVAModule { }