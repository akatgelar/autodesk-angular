import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { saveAs } from 'file-saver';
// Convert Date
@Pipe({ name: 'convertPercent' })
export class ConvertPercent {
    transform(value: string): any {
        return Number.parseFloat(value).toFixed(2);;
    }
}

@Component({
    selector: 'app-evaluation-responses',
    templateUrl: './evaluation-responses.component.html',
    styleUrls: [
        './evaluation-responses.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class EvaluationResponsesEVAComponent implements OnInit {

    // jsonanswer = [{"q1":"yes","q2":"yes","q3":"yes","question1":"yes","q5":{"OE":"0","FR_1":"25","FR_2":"50","IQ":"75","CCM":"100"},"q6":{"CCM":"0","IQ":"25","FR":"50"},"q7":{"CCM":"100"},"q8":["teaching_student"],"q9":"professional_full_time","q10":"more_than_6_month_less_than_1_year","q11":"yes","q12":{"r1":"0","r2":"25","r3":"50","r4":"75"},"q13":"item2","comment":"test"}];

    // {"r1":"50","r2":"50","r3":"50","r4":"50"},"question12":"c1","Comments":"comments"}
    validCountry = true;
    apiCall:any;
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};
    dropdownSettingsSingle = {};

    eventType = [];
    public showEvent = false;
    public data: any = '';
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    year: any;
    queryForm: FormGroup;
    date = [];
    siteActive = [];
    public loading = false;
    loading1 :boolean = false;
    dataresult = [];
    hourtraining: any;
    productcourse: any;
    QP1 = [];
    QP2 = [];
    QP3 = [];
    QP4 = [];
    QP5 = [];
    QP6 = [];
    itscourse: Boolean = false;
    itsproject: Boolean = false;
    totalEvaluation: number = 0;
    ATCFacility;
    ATCComputer;

    eva1 = [];
    eva2 = [];
    eva3 = [];
    eva4 = [];
    eva5 = [];
    eva6 = [];
    eva7 = [];
    eva8 = [];
    eva9 = [];
    eva10 = [];
    eva11 = [];
    eva12 = [];
    eva13 = [];
    eva14 = [];
    eva15 = [];
    eva16 = [];
    eva17 = [];
    eva18 = [];
    eva19 = [];
    eva20 = [];
    eva21 = [];
    eva22 = [];
    eva23 = [];
    eva24 = [];

    eva_madafaka;
    public course_software: any = '';
    hours_training = [];
    training_format = [];
    training_materials = [];
    teaching_level = [];

    //eva respons - issue 30-10-2018
    eva_respon = [];
    questionStruct: any;
    public rowsOnPage2: number = 10;
    public filterQuery2: string = "";
    public sortBy2: string = "type";
    public sortOrder2: string = "asc";

    total_eva: number = 0;
    facility: number = 0;
    facility_percent: number = 0;
    computer: number = 0;
    computer_percent: number = 0;
    private OrgId;
    private TerritoryId;
name:any;
width:any;
sub: any;

    useraccesdata: any;

    constructor(private router: Router, public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {

        //eva respons - issue 30-10-2018
        this.questionStruct = [
            {
                QuestionNumber: "Q1.1",
                Question: "I would like to receive my electronic Certificate of Completion at the e-mail address stated below. (Your full name is needed below)"
            },
            {
                QuestionNumber: "Q1.2",
                Question: "I would like to receive email from Autodesk, including information about new products and special promotions."
            },
            {
                QuestionNumber: "Q1.3",
                Question: "I agree the information provided in this form may be forwarded by Autodesk to the Authorized Education Partners and may be used by<br/>the Authorized Education Partners for internal quality monitoring purposes."
            },
            {
                QuestionNumber: "Q1.4",
                Question: "I would like to receive email from Autodesk, including information about new products and special promotions."
            },
            {
                QuestionNumber: "Q2",
                Question: ""
            },
            {
                QuestionNumber: "Q2.1",
                Question: "Overall experience"
            },
            {
                QuestionNumber: "Q2.2",
                Question: "Computer equipment"
            },
            {
                QuestionNumber: "Q2.3",
                Question: "Training Facility"
            },
            {
                QuestionNumber: "Q2.4",
                Question: "Online Training Facility"
            },
            {
                QuestionNumber: "Q2.5",
                Question: "Instructor"
            },
            {
                QuestionNumber: "Q2.6",
                Question: "CourseWare"
            },
            {
                QuestionNumber: "Q3",
                Question: "How likely or unlikely are you to recommend the following to a friend or colleague?"
            },
            {
                QuestionNumber: "Q3.1",
                Question: "This course"
            },
            {
                QuestionNumber: "Q3.2",
                Question: "This instructor"
            },
            {
                QuestionNumber: "Q3.3",
                Question: "The training facility"
            },
            {
                QuestionNumber: "Q4",
                Question: "How likely or unlikely are you to continue learning Autodesk software?"
            },
            {
                QuestionNumber: "Q5",
                Question: "What do you intend to use this training for?"
            },
            {
                QuestionNumber: "Q6",
                Question: "Which of the following best describes you?"
            },
            {
                QuestionNumber: "Q7",
                Question: "How much experience do you have with this Autodesk product?"
            },
            {
                QuestionNumber: "Q8",
                Question: "Do you plan to obtain an Autodesk Certification for the software learned in this course?"
            },
            {
                QuestionNumber: "Q8.1",
                Question: ""
            },
            {
                QuestionNumber: "Q8.2",
                Question: ""
            },
            {
                QuestionNumber: "Q8.3",
                Question: ""
            },
            {
                QuestionNumber: "Q8.4",
                Question: ""
            },
            {
                QuestionNumber: "Q9",
                Question: "To what extent do you agree or disagree with the following statements?"
            },
            {
                QuestionNumber: "Q9.1",
                Question: "I learned new knowledge and skills"
            },
            {
                QuestionNumber: "Q9.2",
                Question: "I will be able to apply the new skills I learned"
            },
            {
                QuestionNumber: "Q9.3",
                Question: "The new skills I learned will improve my performance"
            },
            {
                QuestionNumber: "Q9.4",
                Question: "I'm more likely to recommend Autodesk products as a result of this course"
            },
            {
                QuestionNumber: "Q10",
                Question: "Which of the following categories best describes your organization’s primary industry?"
            }
        ];


        let filterByYear = new FormControl('');
        let filterByDate = new FormControl('');
        let filterBySiteStatus = new FormControl('A');

        this.queryForm = new FormGroup({
            filterByYear: filterByYear,
            filterByDate: filterByDate,
            filterBySiteStatus: filterBySiteStatus
        });

        this.date = [
            { id: "", name: "Annual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];

        this.siteActive = [
            { id: "A", name: "Active Only" },
            { id: "X", name: "Inactive Only" }
        ];

        var user = localStorage.getItem("autodesk-data");
        var user_data = JSON.parse(user);
        if (user_data.UserLevelId == 'ORGANIZATION') {
            this.OrgId = user_data.OrgId;
        } else if (user_data.UserLevelId == 'DISTRIBUTOR') {
            //diambil country code nya untuk cari Territory nya di db
            this.TerritoryId = user_data.CountryCode;
        }

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        var url = "api/Geo/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         }

        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
        //         else {
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }
        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
                
        //         // else if (this.itsSite()) {
        //         //     url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         // } 
        //         // else {
        //         //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         // }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })



                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    //Fix isu no.263
    // DistributorCheck(): boolean {
    //     var user_lvl = this.useraccesdata.UserLevelId.split(",");
    //     for (let n = 0; n < user_lvl.length; n++) {
    //         if (user_lvl[n] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    GetCurrentOrg() {
        let org = this.useraccesdata.OrgId.split(",");
        let current_org = [];
        for (let i = 0; i < org.length; i++) {
            current_org.push('"' + org[i] + '"');
        }
        return current_org;
    }

    allcountries = [];
    getCountry() {
        this.loading = true;
        // let country: any;
        //Fix isu no.263
        /*let isDistributor = this.DistributorCheck();
        let current_org = this.GetCurrentOrg();
        this.service.httpClientGet("api/Countries/" + isDistributor + "/" + current_org, country)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = JSON.parse(country).map((item) => {
                        return {
                            id: item.countries_code,
                            itemName: item.countries_name
                        }
                    })
                    this.loading = false;
                }
                this.allcountries = this.dropdownListCountry;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];*/

        var url2 = "api/Countries/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; // tambah kalo site / org country nya based org
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 // url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //                 url2 = "api/Territory/where/{'OnlyCountrySiteId':'" + this.urlGetSiteId() + "'}"; /* fixed issue 24092018 - populate country by role site admin */
        //             }else{
        //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;var OnlyCountrySiteId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountrySiteId = this.urlGetSiteId();
                        url2 = "api/Territory/wherenew/'OnlyCountrySiteId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            OnlyCountrySiteId:OnlyCountrySiteId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
                this.allcountries = this.dropdownListCountry;
                this.loading = false;

                /* issue doc report 11102018 - default dropdown country for role site and org admin */

                if(this.adminya){
                    if(!this.distributorya){
                        this.selectedItemsCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                }

                /* end line issue doc report 11102018 - default dropdown country for role site and org admin */

            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    certificateDropdown() {
        this.dropdownCertificate = [];
        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }
        this.selectedCertificate = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.queryForm.patchValue({ filterByYear: this.year[this.year.length - 1].Key });
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                   this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    // this.selectedPartner = this.dropdownPartner;
                }
            },
                error => {
                    this.service.errorserver();
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]
                });
    }

    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        if (!(localStorage.getItem("filter") === null)) {
            // this.getCountry();
            // this.getGeo();
            // this.getCountry();
            // this.getPartnerType();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoRes;
            this.dropdownListGeo = item.dropGeoRes;
            this.selectedItemsRegion = item.selectRegionRes;
            this.dropdownListRegion = item.dropRegionRes;
            this.selectedItemsSubRegion = item.selectSubRegionRes;
            this.dropdownListSubRegion = item.dropSubRegionRes;
            this.selectedItemsCountry = item.selectCountryRes;
            this.dropdownListCountry = item.dropCountryRes;
            this.selectedPartner = item.selectPartnerRes;
            this.dropdownPartner = item.dropPartnerRes;
            this.selectedCertificate = item.selectCertificateRes;
            this.dropdownCertificate = item.dropCertificateRes;
            this.queryForm.patchValue({ filterByYear: item.yearRes });
            this.queryForm.patchValue({ filterByDate: item.dateRes });
            this.queryForm.patchValue({ filterBySiteStatus: item.siteRes });
            this.onSubmit();
        }
        else {
            this.getGeo();
            this.getCountry();
            // this.selectedPartner = [];
            this.getPartnerType();
        }

        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        this.dropdownSettingsSingle = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5,
            showCheckbox: false,
            closeOnSelect: true
        }

        // console.log(JSON.stringify({ "question2": ["item1"], "question1": "tes" }));

        /* -- try create array object of grouping value by element -- */

        // var testanswer = 
        //     [
        //         {
        //             "question1":"yes",
        //             "question2":"no",
        //         },
        //         {
        //             "question1":"yes",
        //             "question2":"no",
        //         },
        //         {
        //             "question1":"yes",
        //             "question2":"no",
        //         },
        //     ]

        // var answer = [];

        // for(var i=0;i<testanswer.length;i++){
        //     for(var key in testanswer[i]) {
        //         if (testanswer[i].hasOwnProperty(key)) {
        //             answer.push({answer:testanswer[i][key]})
        //         }
        //     }
        // }

        // console.log(answer)

        /* -- end try create array object of grouping value by element -- */

        /* -- grouping calculation per question (ex) -- */

        // var total=0;
        // var dataObject = 
        //     [
        //         {
        //             "question1":"yes",
        //         },
        //         {
        //             "question1":"no",
        //         },
        //         {
        //             "question1":"yes",
        //         }
        //     ],
        // groups = dataObject.reduce(function (r, o) {
        //     var k = o.question1;
        //     if (r[k]) {
        //         r[k].Count++ ;
        //     } else {
        //         r[k] = o; 
        //         r[k].Count = 1; 
        //     }
        //     total++;
        //     return r;
        // }, {});

        // var result = Object.keys(groups).map(function (k) {
        //     groups[k].Percent = Math.round(groups[k].Count/total*100);
        //     return groups[k];
        // });

        // console.log(result);

        /* -- end grouping calculation per question (ex) -- */

    }

    getproducts() {
        this.QP2 = [];
        this.service.httpClientGet('api/Product/ProductCourse', '')
            .subscribe(res => {
                if (res == "Not Found") {
                    this.productcourse = '';
                } else {
                    this.productcourse = res;
                    for (var i = 0; i < this.productcourse.length; i++) {
                        this.QP2.push(
                            {
                                "count": 0,
                                "percent": 0,
                                "name": this.productcourse[i].PrimaryProduct
                            }
                        )
                    }
                }
            }, error => {
                this.productcourse = '';
            }
            );
    }

    gethourtraining(id) {
        this.QP3 = [];
        this.service.httpClientGet('api/Courses/TrainingHour/' + id, '')
            .subscribe(res => {
                if (res == "Not Found") {
                    this.hourtraining = '';
                } else {
                    this.hourtraining = res;
                    for (var i = 0; i < this.hourtraining.length; i++) {
                        this.QP3.push(
                            {
                                "count": 0,
                                "percent": 0,
                                "name": this.hourtraining[i].HoursTraining
                            }
                        )
                    }
                }
            }, error => {
                this.hourtraining = '';
            }
            );
    }

    typeoncourse: any
    getType(id) {
        this.QP4 = [];
        if (id == '1') {
            this.service.httpClientGet('api/Dictionaries/where/{"Parent":"TrainingType"}', '')
                .subscribe(res => {
                    if (res == "Not Found") {
                        this.typeoncourse = '';
                    } else {
                        this.typeoncourse = res;
                        for (var i = 0; i < this.typeoncourse.length; i++) {
                            this.QP4.push(
                                {
                                    "count": 0,
                                    "percent": 0,
                                    "id": this.typeoncourse[i].Key,
                                    "name": this.typeoncourse[i].KeyValue
                                }
                            )
                        }
                    }
                }, error => {
                    this.typeoncourse = '';
                }
                );
        }
        else {
            this.service.httpClientGet('api/Dictionaries/where/{"Parent":"ProjectType"}', '')
                .subscribe(res => {
                    if (res == "Not Found") {
                        this.typeoncourse = '';
                    } else {
                        this.typeoncourse = res;
                        for (var i = 0; i < this.typeoncourse.length; i++) {
                            this.QP4.push(
                                {
                                    "count": 0,
                                    "percent": 0,
                                    "id": this.typeoncourse[i].Key,
                                    "name": this.typeoncourse[i].KeyValue
                                }
                            )
                        }
                    }
                }, error => {
                    this.typeoncourse = '';
                }
                );
        }
    }

    //eva respons - issue 30-10-2018
    selectedScore(score) {
        var choosen: number = 0;
        switch (score) {
            case 100:
                choosen = 5;
                break;
            case 75:
                choosen = 4;
                break;
            case 50:
                choosen = 3;
                break;
            case 25:
                choosen = 2;
                break;
            default:
                choosen = 1;
                break;
        }
        return choosen;
    }
    question8(val) {
        var choosen: string = "";
        switch (val) {
            case "1":
                choosen = "Architecture, Engineering & Construction";
                break;
            case "2":
                choosen = "Automotive & Transportation";
                break;
            case "3":
                choosen = "Government";
                break;
            case "4":
                choosen = "Infrastructure and Civil Engineering";
                break;
            case "5":
                choosen = "Manufacturing";
                break;
            case "6":
                choosen = "Media & Entertainment";
                break;
            case "7":
                choosen = "Utilities & Telecommunications";
                break;

            default:
                choosen = "Other";
                break;
        }
        return choosen;
    }
    resulteval=[];
getResponseData(res){
    let report = res;
    report = "[" + report + "]";
    //    report = JSON.parse(report);
    report = JSON.parse(report);
     if (report[0].TotalEvaluation != "0") {
                            this.eva_madafaka = report;
                             //Course Equipment
                            for (let c = 0; c < this.eva_madafaka[0].length; c++) {
                                this.total_eva =parseInt(this.eva_madafaka[0][c].TotalEvaluation);
                                this.facility = parseInt(this.eva_madafaka[0][c].Facility);
                                this.computer = parseInt(this.eva_madafaka[0][c].Computer);
                                if(this.total_eva == 0){
                                    this.facility_percent = 0;
                                    this.computer_percent = 0;
                                }
                                else{
                                    this.facility_percent = (this.facility / this.total_eva) * 100;
                                    this.computer_percent = (this.computer / this.total_eva) * 100;
                                }
                               
                                
                            }
                            // this.total_eva =parseInt(this.eva_madafaka[0].TotalEvaluation);
                            // this.facility = (this.eva_madafaka[0].Facility);
                            // this.computer = parseInt(this.eva_madafaka[0].Computer);
                            // this.facility_percent = (this.facility / this.total_eva) * 100;
                            // this.computer_perfcent = (this.computer / this.total_eva) * 100;

                            //Course Teaching Level
                            this.teaching_level = [];
                            let total_teaching: number = 0;
                            for (let t = 0; t < this.eva_madafaka[1].length; t++) {
                                total_teaching = parseInt(this.eva_madafaka[1][t].Update) + parseInt(this.eva_madafaka[1][t].Essentials) + parseInt(this.eva_madafaka[1][t].Intermediate) + parseInt(this.eva_madafaka[1][t].Advanced) + parseInt(this.eva_madafaka[1][t].Customized) + parseInt(this.eva_madafaka[1][t].Other);
                           if(total_teaching == 0){
                            this.teaching_level.push(
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Update),
                                    Percentage: 0,
                                    Name: "Update"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Essentials),
                                    Percentage: 0,
                                    Name: "Level 1: Essentials"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Intermediate),
                                    Percentage: 0,
                                    Name: "Level 2: Intermediate"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Advanced),
                                    Percentage: 0,
                                    Name: "Level 3: Advanced"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Customized),
                                    Percentage: 0,
                                    Name: "Customized"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Other),
                                    Percentage: 0,
                                    Name: "Other"
                                }
                            );
                           }
                           else{
                            this.teaching_level.push(
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Update),
                                    Percentage: (parseInt(this.eva_madafaka[1][t].Update) / total_teaching) * 100,
                                    Name: "Update"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Essentials),
                                    Percentage: (parseInt(this.eva_madafaka[1][t].Essentials) / total_teaching) * 100,
                                    Name: "Level 1: Essentials"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Intermediate),
                                    Percentage: (parseInt(this.eva_madafaka[1][t].Intermediate) / total_teaching) * 100,
                                    Name: "Level 2: Intermediate"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Advanced),
                                    Percentage: (parseInt(this.eva_madafaka[1][t].Advanced) / total_teaching) * 100,
                                    Name: "Level 3: Advanced"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Customized),
                                    Percentage: (parseInt(this.eva_madafaka[1][t].Customized) / total_teaching) * 100,
                                    Name: "Customized"
                                },
                                {
                                    Format: parseInt(this.eva_madafaka[1][t].Other),
                                    Percentage: (parseInt(this.eva_madafaka[1][t].Other) / total_teaching) * 100,
                                    Name: "Other"
                                }
                            );
                           }
                            
                        }
                            

                            //Course Software
                            let cs_temp = []
                            let total_use_software: number = 0;
                            if (this.eva_madafaka[2].length != undefined || this.eva_madafaka[2] != "") {
                                for (let n = 0; n < this.eva_madafaka[2].length; n++) {
                                    total_use_software += parseInt(this.eva_madafaka[2][n].TotalUse);
                                }
                                for (let j = 0; j < this.eva_madafaka[2].length; j++) {
                                    cs_temp.push({
                                        TotalUse: parseInt(this.eva_madafaka[2][j].TotalUse),
                                        Percentage: (parseInt(this.eva_madafaka[2][j].TotalUse) / total_use_software) * 100,
                                        ProductName: this.eva_madafaka[2][j].productName
                                    });
                                }
                                this.course_software = cs_temp;
                            } else {
                                this.course_software = null;
                            }

                            //Training Hours
                            this.hours_training = [];
                            let total_selected: number = 0;
                            if (this.eva_madafaka[3].length != undefined || this.eva_madafaka[3] != "") {
                                for (let x = 0; x < this.eva_madafaka[3].length; x++) {
                                    total_selected += parseInt(this.eva_madafaka[3][x].SelectedHour);
                                }
                                for (let y = 0; y < this.eva_madafaka[3].length; y++) {
                                    if(total_selected==0){
                                        this.hours_training.push({
                                            SelectedHour: parseInt(this.eva_madafaka[3][y].SelectedHour),
                                            Percentage: 0,
                                            HoursTraining: this.eva_madafaka[3][y].HoursTraining
                                        });
                                    }
                                    else{
                                        this.hours_training.push({
                                            SelectedHour: parseInt(this.eva_madafaka[3][y].SelectedHour),
                                            Percentage: (parseInt(this.eva_madafaka[3][y].SelectedHour) / total_selected) * 100,
                                            HoursTraining: this.eva_madafaka[3][y].HoursTraining
                                        });
                                    }
                                }
                            } else {
                                this.hours_training = null;
                            }

                            //Training Format
                            this.training_format = [];
                            let total_format: number = 0;
                            for (let f = 0; f < this.eva_madafaka[4].length; f++) {
                                total_format = parseInt(this.eva_madafaka[4][f].InstructorLed) + parseInt(this.eva_madafaka[4][f].Online);
                                if(total_format == 0){
                                    this.training_format.push(
                                        {
                                            Format: parseInt(this.eva_madafaka[4][f].InstructorLed),
                                            Percentage: 0,
                                            Name: "Instructor-led in the classroom"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[4][f].Online),
                                            Percentage: 0,
                                            Name: "Online or e-learning"
                                        }
                                    );
                                }
                                else{
                                    this.training_format.push(
                                        {
                                            Format: parseInt(this.eva_madafaka[4][f].InstructorLed),
                                            Percentage: (parseInt(this.eva_madafaka[4][f].InstructorLed) / total_format) * 100,
                                            Name: "Instructor-led in the classroom"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[4][f].Online),
                                            Percentage: (parseInt(this.eva_madafaka[4][f].Online) / total_format) * 100,
                                            Name: "Online or e-learning"
                                        }
                                    );
                                }
                            }
                           

                            //Training Materials
                            this.training_materials = [];
                            let total_materials: number = 0;
                            for (let m = 0; m < this.eva_madafaka[5].length; m++) {
                                total_materials = parseInt(this.eva_madafaka[5][m].AAP) + parseInt(this.eva_madafaka[5][m].ATC) + parseInt(this.eva_madafaka[5][m].ATCOnline) + parseInt(this.eva_madafaka[5][m].Autodesk) + parseInt(this.eva_madafaka[5][m].Independent) + parseInt(this.eva_madafaka[5][m].IndependentOnline) + parseInt(this.eva_madafaka[5][m].Other);
                                if(total_materials == 0){
                                    this.training_materials.push(
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].Autodesk),
                                            Percentage: 0,
                                            Name: "Autodesk Official Courseware (any Autodesk branded material)"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].AAP),
                                            Percentage: 0,
                                            Name: "Autodesk Authors and Publishers (AAP) Program Courseware"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].ATC),
                                            Percentage: 0,
                                            Name: "Course material developed by the ATC"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].Independent),
                                            Percentage: 0,
                                            Name: "Course material developed by an independent vendor or instructor"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].IndependentOnline),
                                            Percentage: 0,
                                            Name: "Online e-learning course material developed by an independent vendor or instructor"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].ATCOnline),
                                            Percentage: 0,
                                            Name: "Online e-learning course material developed by the ATC"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].Other),
                                            Percentage: 0,
                                            Name: "Other"
                                        }
                                    );
                                }
                                else{
                                    this.training_materials.push(
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].Autodesk),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].Autodesk) / total_materials) * 100,
                                            Name: "Autodesk Official Courseware (any Autodesk branded material)"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].AAP),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].AAP) / total_materials) * 100,
                                            Name: "Autodesk Authors and Publishers (AAP) Program Courseware"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].ATC),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].ATC) / total_materials) * 100,
                                            Name: "Course material developed by the ATC"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].Independent),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].Independent) / total_materials) * 100,
                                            Name: "Course material developed by an independent vendor or instructor"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].IndependentOnline),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].IndependentOnline) / total_materials) * 100,
                                            Name: "Online e-learning course material developed by an independent vendor or instructor"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].ATCOnline),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].ATCOnline) / total_materials) * 100,
                                            Name: "Online e-learning course material developed by the ATC"
                                        },
                                        {
                                            Format: parseInt(this.eva_madafaka[5][m].Other),
                                            Percentage: (parseInt(this.eva_madafaka[5][m].Other) / total_materials) * 100,
                                            Name: "Other"
                                        }
                                    );
                                }

                            }
                            // this.service.post("api/ReportEva/EvaluationResponseAnswer", keyword)
                            // .subscribe(res => {
        
                            //     report = "[" + res + "]";
                            //     report = JSON.parse(report);
                            //          this.resulteval = [];
                            //         var numQuestion = 6;
                                    
                            //         for(var q=0;q<report[0].length;q++){
        
        
                            //             if(report[0][q].Value != ""){
                            //                 var cal = 0;
                            //                 var grouped = _.mapValues(_.groupBy(report[1], report[0][q].Key),
                            //                     clist => clist.map(data => _.omit(report[1], report[0][q].Key)));
        
                            //                 var listAnswer = Object.keys(grouped);
        
                            //                 for(var w=0;w<listAnswer.length;w++){
                                               
                            //                     cal = grouped[listAnswer[w]].length;
                                              
                            //                     var countcal = (cal/report[1].length)*100;
                            //                     if(w<1){
                            //                         this.resulteval.push({'numquestion':numQuestion,'question':report[0][q].Value,'answer':listAnswer[w],'result':cal+" ("+countcal.toFixed(2)+"%)"})
                            //                     }else{
                            //                         this.resulteval.push({'question':'','answer':listAnswer[w],'result':cal+" ("+countcal.toFixed(2)+"%)"})
                            //                     }
                            //                 }
        
                            //                 numQuestion++;
                            //             }
                                       
                            //         }
                                   
                                    
                            // });
                          //  this.dataFound = true;

                        } else {
                            this.eva_madafaka = "";
                            this.dataFound = false;
                            this.loading = false;
                        }
}

getAnswerData(report){
     
   this.width= 10;
   var listAnswer:any;
    report = "[" + report + "]";
    report = JSON.parse(report);
    const groupByBrand = _.groupBy(report[1]["name"]);
         this.resulteval = [];
        var numQuestion = 6;
        
             
        for(var q=0;q<report[0].length;q++){
            if(report[0][q].Value != ""){
                var cal = 0;
                // this.countries = this.countries.map(country => {
                //     var concession = concessions.find(x => x.address.country === country.iso);
                //     country.address = concession.address;
                //     country.slug = concession.slug;
                //     return country;
                // });
              
                // var g= g.map(country => {
                //     var concession = report[1].find(x => x.name === report[0][q]["name"]);
                //     country.ansData =  concession.ansData;
                //     country.Count =  concession.Count;
                //     country.name =  concession.name;
                //     return country;
                // });
                var newArr = report[1].filter(function(item){
                    return item.name ===report[0][q]["name"];
                });
                
//                array1 = [{name:'arjun', place:'mysore'},{name:'kanka', place:'bangalore'}];
// array2 = [{name: 'arjun', degree:'BE'},{name:'kanka', degree:'MCA'}];

// let result = array1.map((a)=>{
//  let obj2 = array2.find((b)=> a.name === b.name);
//  if(obj2)
//   Object.assign(a,obj2);
//  return a;
// });

                // ansData = grp.Key,
                // Count = grp.Count(),
                // name = dc.ColumnName
            //   var grouped = _.mapValues(_.groupBy(x => { report[1].name === report[0][q]["name"]} ),
            //   clist => clist.map(data => _.omitx(report[1].name = report[0][q]["name"])));
             
                       //  listAnswer = Object.keys(newArr);
                    for(var w=0;w<newArr.length;w++){
                        if(cal != null)cal = newArr[w]["Count"];
                         
                       // cal = grouped[listAnswer[w]].length;
                       var TCount : any = report[2];
                        var countcal = (cal/TCount)*100;
                        if(w<1){
                            this.resulteval.push({'numquestion':numQuestion,'question':report[0][q].data,'answer':newArr[w]["ansData"],'result':cal+" ("+countcal.toFixed(2)+"%)"})
                        }else{
                            this.resulteval.push({'question':'','answer':newArr[w]["ansData"],'result':cal+" ("+countcal.toFixed(2)+"%)"})
                        }
                    }
                numQuestion++;
            }  
        }
       


        this.loading = false; 


}

GetTotalEvaluation(keyword){
    setTimeout(() => {
        this.service.httpClientPostWithOptions("api/ReportEva/EvaluationResponseNew", keyword, { responseType: 'arraybuffer'})
        .subscribe(res => { 
            let tmpData : any = res;
            var blob = new Blob([tmpData], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"});
            this.loading = false;
            saveAs(blob, "EvaluationResponse1_5.xls"); 
        });
    }, 1000);
    setTimeout(() => {
        this.service.httpClientPostWithOptions("api/ReportEva/EvaluationResponseAnswerNew", keyword, { responseType: 'arraybuffer'})
        .subscribe(res => { 
            let tmpData : any = res;
            var blob = new Blob([tmpData], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"});
            this.loading = false;
            saveAs(blob, "EvaluationResponse6_Above.xls"); 
        });
    }, 2000);
    
    
          // this.getResponseData(tmpData);
        
    //     this.ngProgress.start();
    //     this.service.httpClientPost("api/ReportEva/EvaluationResponseAnswer", keyword)
    //     .subscribe(res => {
    //         tmpData = res;
    //             this.getAnswerData(res);
      
    //    this.ngProgress.done();
    //     });
        this.dataFound = true;


            //eva respons - issue 30-10-2018


        //    var data = this.eva_madafaka[6];
        // //     this.resulteval = [];
        // //     data = JSON.parse(data);
        // //    if (data.length > 0) {
        //     for (let x = 0; x < data.length; x++) {
        //         let answer = data[x].EvaluationAnswerJson;
                
        //         if(typeof answer === 'object' || answer == ""){
        //             console.log("null");
        //         }else{
        //             if (answer.indexOf('\\r\\n') > -1) {
        //                 answer = answer.replace(/\\r\\n/g, "");
        //                 answer = answer.replace(/  /g, "");
        //                 answer = answer.replace(/    /g, "");
        //             }else if (answer.indexOf('\\n') > -1) {
        //                 answer = answer.replace(/\\n\\t\\t/g, "");
        //                 answer = answer.replace(/\\n\\t/g, "");
        //                 answer = answer.replace(/\\n/g, "");
        //                 answer = answer.replace(/  /g, "");
        //                 answer = answer.replace(/    /g, "");
        //             }
        //             answer = answer.replace(/\\/g, "");
                    
        //             //for while, till find solution -> its all from q8-comment,q13-comment that we dont use now, they all have (") inside
        //             answer = answer.replace('Курс обучения в КА "ШАГ"',''); 
        //             answer = answer.replace('almindelig opdatering af "penalhuset"','');
        //             answer = answer.replace('My Agency "likes" to see evidence of training an proficiency - hence, the annual training.','');
        //             answer = answer.replace('company does everythimg that we can do , hence the name "see it all ltd".','');
        //             answer = answer.replace('Shipping "Container Terminals"','');
        //             answer = answer.replace('A mix of "Government" and "Engineering, Construction, etc."','');
        //             answer = answer.replace('ООО"КИНЕФ"','');
        //             answer = answer.replace('To be able to speak to the Documentation Team in their own "Revit" language','');
        //             answer = answer.replace('applied arts "interior design"','');
        //             answer = answer.replace('"mit der Entwicklung Schritt halten"','');

        //             //cara untuk ngambil bagian komentarnya aja
        //             let index_1;
        //             let prev_comm;
                    
        //             //ambil comentar nya saja
        //             if (answer.indexOf('"comment": "') > -1) {
        //                 index_1 = answer.indexOf('"comment": "'); 
        //                 prev_comm = answer.substr(index_1 + 12, answer.length); 
        //             }

        //             if (answer.indexOf('"comment":"') > -1) {
        //                 index_1 = answer.indexOf('"comment":"'); 
        //                 prev_comm = answer.substr(index_1 + 11, answer.length); 
        //             }

        //             if (answer.indexOf('"Comments": "') > -1) {
        //                 index_1 = answer.indexOf('"Comments": "');
        //                 prev_comm = answer.substr(index_1 + 13, answer.length); 
        //                 prev_comm = prev_comm.replace(/\n|\r/g, '');
        //             }

        //             if (answer.indexOf('"Comments":"') > -1) {
        //                 index_1 = answer.indexOf('"Comments":"'); 
        //                 prev_comm = answer.substr(index_1 + 12, answer.length); 
        //             }

        //             if (answer.indexOf('"comment" : "') > -1) {
        //                 index_1 = answer.indexOf('"comment": "'); 
        //                 prev_comm = answer.substr(index_1 + 13, answer.length); 
        //             }
        //             prev_comm = prev_comm.replace(/"}/g, ''); 
                    
        //             //kosongin komentar nya, tuman
        //             let new_comm = "";
        //             if(prev_comm == ":" || prev_comm == ","){
        //                 new_comm = prev_comm.replace(/"/g, '\\"');
        //             } 
        //             let new_answer = answer.replace('"'+prev_comm+'"', '"'+new_comm+'"'); 

        //             /* issue 17102018 - cant extract excel -> escape char */

        //             new_answer = new_answer.toString().replace(/\r/g,'');
        //             new_answer = new_answer.toString().replace(/\n/g,'');

        //             /* end line issue 17102018 - cant extract excel -> escape char */
                    
        //             new_answer = JSON.parse(new_answer);
                    
        //             var koplak = Object.keys(new_answer);

        //             if (koplak[0] != "question1") {
        //                 for (let q = 0; q < this.questionStruct.length; q++) {
        //                     switch (this.questionStruct[q].QuestionNumber) {
        //                         case "Q1.1":
        //                             data[x]["Q1.1"] = new_answer.q1;
        //                             break;
        //                         case "Q1.2":
        //                             data[x]["Q1.2"] = new_answer.q2;
        //                             break;
        //                         case "Q1.3":
        //                             data[x]["Q1.3"] = new_answer.q3;
        //                             break;
        //                         case "Q1.4":
        //                             data[x]["Q1.4"] = "";
        //                             if (new_answer.hasOwnProperty('q4')) {
        //                                 data[x]["Q1.4"] = new_answer.q4;    
        //                             }
        //                             break;
        //                         case "Q2":
        //                             data[x]["Q2"] = "";
        //                             break;
        //                         case "Q2.1":
        //                             data[x]["Q2.1"] = this.selectedScore(parseInt(new_answer.q5["OE"]));
        //                             break;
        //                         case "Q2.2":
        //                             data[x]["Q2.2"] = this.selectedScore(parseInt(new_answer.q5["FR_1"]));
        //                             break;
        //                         case "Q2.3":
        //                             data[x]["Q2.3"] = this.selectedScore(parseInt(new_answer.q5["FR_2"]));
        //                             break;
        //                         case "Q2.4":
        //                             data[x]["Q2.4"] = "";
        //                             break;
        //                         case "Q2.5":
        //                             data[x]["Q2.5"] = this.selectedScore(parseInt(new_answer.q5["IQ"]));
        //                             break;
        //                         case "Q2.6":
        //                             data[x]["Q2.6"] = this.selectedScore(parseInt(new_answer.q5["CCM"]));
        //                             break;
        //                         case "Q3":
        //                             data[x]["Q3"] = "";
        //                             break;
        //                         case "Q3.1":
        //                             data[x]["Q3.1"] = this.selectedScore(parseInt(new_answer.q6["CCM"]));
        //                             break;
        //                         case "Q3.2":
        //                             data[x]["Q3.2"] = this.selectedScore(parseInt(new_answer.q6["IQ"]));
        //                             break;
        //                         case "Q3.3":
        //                             data[x]["Q3.3"] = this.selectedScore(parseInt(new_answer.q6["FR"]));
        //                             break;
        //                         case "Q4":
        //                             data[x]["Q4"] = this.selectedScore(parseInt(new_answer.q7["CCM"]));
        //                             break;
        //                         case "Q5":
        //                             var arr_temp = new_answer.q8.filter(v => v != '');
        //                             data[x]["Q5"] = arr_temp.join(" / ");
        //                             break;
        //                         case "Q6":
        //                             data[x]["Q6"] = new_answer.q9.replace(/_/g, " ");
        //                             break;
        //                         case "Q7":
        //                             data[x]["Q7"] = new_answer.q10.replace(/_/g, " ");
        //                             break;
        //                         case "Q8":
        //                             data[x]["Q8"] = "";
        //                             break;
        //                         case "Q8.1":
        //                             data[x]["Q8.1"] = "";
        //                             break;
        //                         case "Q8.2":
        //                             data[x]["Q8.2"] = "";
        //                             break;
        //                         case "Q8.3":
        //                             data[x]["Q8.3"] = "";
        //                             break;
        //                         case "Q8.4":
        //                             data[x]["Q8.4"] = "";
        //                             break;
        //                         case "Q9":
        //                             data[x]["Q9"] = "";
        //                             break;
        //                         case "Q9.1":
        //                             data[x]["Q9.1"] = this.selectedScore(parseInt(new_answer.q12["r1"]));;
        //                             break;
        //                         case "Q9.2":
        //                             data[x]["Q9.2"] = this.selectedScore(parseInt(new_answer.q12["r2"]));;
        //                             break;
        //                         case "Q9.3":
        //                             data[x]["Q9.3"] = this.selectedScore(parseInt(new_answer.q12["r3"]));;
        //                             break;
        //                         case "Q9.4":
        //                             data[x]["Q9.4"] = this.selectedScore(parseInt(new_answer.q12["r4"]));;
        //                             break;
        //                         case "Q10":
        //                             data[x]["Q10"] = this.question8(new_answer.q13);
        //                             break;
        //                     }
        //                 }
        //             } else {
        //                 for (let q = 0; q < this.questionStruct.length; q++) {
        //                     switch (this.questionStruct[q].QuestionNumber) {
        //                         case "Q1.1":
        //                             data[x]["Q1.1"] = new_answer.question1;
        //                             break;
        //                         case "Q1.2":
        //                             data[x]["Q1.2"] = new_answer.question2;
        //                             break;
        //                         case "Q1.3":
        //                             data[x]["Q1.3"] = new_answer.question3;
        //                             break;
        //                         case "Q1.4":
        //                             data[x]["Q1.4"] = new_answer.question4;
        //                             break;
        //                         case "Q2":
        //                             data[x]["Q2"] = "";
        //                             break;
        //                         case "Q2.1":
        //                             data[x]["Q2.1"] = this.selectedScore(parseInt(new_answer.question5["OE"]));
        //                             break;
        //                         case "Q2.2":
        //                             data[x]["Q2.2"] = this.selectedScore(parseInt(new_answer.question5["FR_1"]));
        //                             break;
        //                         case "Q2.3":
        //                             data[x]["Q2.3"] = this.selectedScore(parseInt(new_answer.question5["FR_2"]));
        //                             break;
        //                         case "Q2.4":
        //                             data[x]["Q2.4"] = "";
        //                             break;
        //                         case "Q2.5":
        //                             data[x]["Q2.5"] = this.selectedScore(parseInt(new_answer.question5["IQ"]));
        //                             break;
        //                         case "Q2.6":
        //                             data[x]["Q2.6"] = this.selectedScore(parseInt(new_answer.question5["CCM"]));
        //                             break;
        //                         case "Q3":
        //                             data[x]["Q3"] = "";
        //                             break;
        //                         case "Q3.1":
        //                             data[x]["Q3.1"] = this.selectedScore(parseInt(new_answer.question6["CCM"]));
        //                             break;
        //                         case "Q3.2":
        //                             data[x]["Q3.2"] = this.selectedScore(parseInt(new_answer.question6["IQ"]));
        //                             break;
        //                         case "Q3.3":
        //                             data[x]["Q3.3"] = this.selectedScore(parseInt(new_answer.question6["FR"]));
        //                             break;
        //                         case "Q4":
        //                             data[x]["Q4"] = this.selectedScore(parseInt(new_answer.question7["CCM"]));
        //                             break;
        //                         case "Q5":
        //                             var arr_temp = new_answer.question8.filter(v => v != '');
        //                             data[x]["Q5"] = arr_temp.join(" / ");
        //                             break;
        //                         case "Q6":
        //                             data[x]["Q6"] = new_answer.question9.replace(/_/g, " ");
        //                             break;
        //                         case "Q7":
        //                             data[x]["Q7"] = new_answer.question10.replace(/_/g, " ");
        //                             break;
        //                         case "Q8":
        //                             data[x]["Q8"] = "";
        //                             break;
        //                         case "Q8.1":
        //                             data[x]["Q8.1"] = "";
        //                             break;
        //                         case "Q8.2":
        //                             data[x]["Q8.2"] = "";
        //                             break;
        //                         case "Q8.3":
        //                             data[x]["Q8.3"] = "";
        //                             break;
        //                         case "Q8.4":
        //                             data[x]["Q8.4"] = "";
        //                             break;
        //                         case "Q9":
        //                             data[x]["Q9"] = "";
        //                             break;
        //                         case "Q9.1":
        //                             data[x]["Q9.1"] = this.selectedScore(parseInt(new_answer.question11["r1"]));;
        //                             break;
        //                         case "Q9.2":
        //                             data[x]["Q9.2"] = this.selectedScore(parseInt(new_answer.question11["r2"]));;
        //                             break;
        //                         case "Q9.3":
        //                             data[x]["Q9.3"] = this.selectedScore(parseInt(new_answer.question11["r3"]));;
        //                             break;
        //                         case "Q9.4":
        //                             data[x]["Q9.4"] = this.selectedScore(parseInt(new_answer.question11["r4"]));;
        //                             break;
        //                         case "Q10":
        //                             data[x]["Q10"] = this.question8(new_answer.question12);
        //                             break;
        //                     }
        //                 }
        //             }
        //         }
        //     }   
            
        //     this.resulteval = [];
        //     var numQuestion = 6;
        //     for(var q=0;q<this.questionStruct.length;q++){
        //         if(this.questionStruct[q].Question != ""){
        //             var cal = 0;

        //             var grouped = _.mapValues(_.groupBy(data, this.questionStruct[q].QuestionNumber),
        //                 clist => clist.map(data => _.omit(data, this.questionStruct[q].QuestionNumber)));
                    
        //             var listAnswer = Object.keys(grouped);

        //             for(var w=0;w<listAnswer.length;w++){
        //                 cal = grouped[listAnswer[w]].length;
        //                 var countcal = (cal/data.length)*100;
        //                 if(w<1){
        //                     this.resulteval.push({'numquestion':numQuestion,'question':this.questionStruct[q].Question,'answer':listAnswer[w],'result':cal+" ("+countcal.toFixed(2)+"%)"})
        //                 }else{
        //                     this.resulteval.push({'question':'','answer':listAnswer[w],'result':cal+" ("+countcal.toFixed(2)+"%)"})
        //                 }
        //             }

        //             numQuestion++;
        //         }
        //     }
            // for (let key in data[0]) {
            //     this.resulteval.push(key);
            // }
      
            
   
}
    
    onSubmit() {
        this.resulteval=null;
        if (this.selectedPartner.length == 1) {
            this.validpertner = true
            if(this.selectedItemsCountry.length < 1 ){//this.selectedItemsCountry.length > 1 ||//|| this.selectedItemsCountry.length == this.dropdownListCountry.length
                //use this till find another solution
                //return this.validpertner = false
                this.validCountry = false
            }else{
                this.validCountry = true
                this.loading = true;
                this.data = null;

                let geoarr = [];
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
                }

                let countries = [];
                for (var i = 0; i < this.selectedItemsCountry.length; i++) {
                    countries.push("'" + this.selectedItemsCountry[i].id + "'");
                }

                this.partnertypearr = [];
                for (var i = 0; i < this.selectedPartner.length; i++) {
                    this.partnertypearr.push("'" + this.selectedPartner[i].id + "'");
                }

                this.certificatearr = [];
                for (var i = 0; i < this.selectedCertificate.length; i++) {
                    this.certificatearr.push(this.selectedCertificate[i].id);
                }

                this.eventType = [];
                if (this.selectedEventType.length > 0) {
                    for (let i = 0; i < this.selectedEventType.length; i++) {
                        this.eventType.push('"' + this.selectedEventType[i].id + '"');
                    }
                }

                var contactid = "";
                var orgid = "";
                var distributor = "";
                var siteid = "";

                /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

                if (this.adminya) {
                    if(this.distributorya){
                        distributor = this.urlGetOrgId();
                    }else{
                        if(this.orgya){
                            orgid = this.urlGetOrgId();
                        }else{
                            if(this.siteya){
                                siteid = this.urlGetSiteId();
                            }else{
                                contactid = this.useraccesdata.UserId;
                            }
                        }
                    }
                }
                /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

                var params =
                {
                    selectGeoRes: this.selectedItemsGeo,
                    dropGeoRes: this.dropdownListGeo,
                    selectRegionRes: this.selectedItemsRegion,
                    dropRegionRes: this.dropdownListRegion,
                    selectSubRegionRes: this.selectedItemsSubRegion,
                    dropSubRegionRes: this.dropdownListSubRegion,
                    selectCountryRes: this.selectedItemsCountry,
                    dropCountryRes: this.dropdownListCountry,
                    selectPartnerRes: this.selectedPartner,
                    dropPartnerRes: this.dropdownPartner,
                    selectCertificateRes: this.selectedCertificate,
                    dropCertificateRes: this.dropdownCertificate,
                    yearRes: this.queryForm.value.filterByYear,
                    dateRes: this.queryForm.value.filterByDate,
                    siteRes: this.queryForm.value.filterBySiteStatus
                }
                
                //Masukan object filter ke local storage
                localStorage.setItem("filter", JSON.stringify(params));

                let keyword = {
                    "filterByGeo": geoarr.toString(),
                    "filterByCountry": countries.toString(),
                    "filterBySurveyYear": this.queryForm.value.filterByYear,
                    "filterByDate": this.queryForm.value.filterByDate,
                    "filterBySiteStatus": this.queryForm.value.filterBySiteStatus,
                    "filterByPartnerType": this.partnertypearr.toString(),
                    "filterByCertificateType": this.certificatearr.toString(),
                    "EventType": this.eventType.toString(),
                    "Organization": this.OrgId,
                    "Territory": this.TerritoryId,
                    "ContactId": contactid,
                    "OrgId": orgid,
                    "Distributor": distributor,
                    "SiteId": siteid,
                };
                let tmpData : any
                let tmpEvaluation : any;let tmpEvaluation1 :number = 1000000;
            //     this.service.httpClientPost("api/ReportEva/TotalEvaluationResponse", keyword)
            //     .subscribe(res => {
            //         if (res == "Not Found" || res == null) {
            //             tmpEvaluation = 0;
            //         }
            //         else{   
            //           tmpData  = res;
            //            tmpEvaluation = tmpData["TotalEvaluation"];
            //         }
            //      var tmpData : any=   JSON.stringify(keyword)          
            //   if(Number(tmpEvaluation) > tmpEvaluation1){
            //    // this.loading = false;
            //     //this.router.navigate(['/course/course-list']);
            //     // var newWindow = window.open('report-eva/evaluation-responsesmorecount/' + tmpData, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            //     // if (window.focus()) { newWindow.focus() }
            //     // //    return false;
            //         swal(
            //             'Warnng!',
            //             'It may take long time because total evaluation > 10000,<br/> please select only one month!',
            //             'warning'
            //           )
            //             this.loading = false;
            //       }
            //    else if(Number(tmpEvaluation) <= tmpEvaluation1){
                this.GetTotalEvaluation(keyword);
            //     }
           // });
                /* end line autodesk plan 18 oct */
                }
        } else {
            return this.validpertner = false
            /*
            swal(
                'Field is Required!',
                'Please select an Partner Type, field required!',
                'error'
            )
            */
       
    }
}
    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedPartner = [];
    }
    partnertypearr = [];
    certificatearr = [];
    certificatepick: string = "";
    validpertner: boolean = true;
    validcertificate: boolean = true;
    onSubmit_tmp() {
        try {
            if (this.selectedPartner.length > 0) {
                this.loading = true;
                this.data = null;

                this.certificatepick = this.selectedCertificate[0].id;
                if (this.certificatepick == '1') {
                    this.itscourse = true;
                    this.itsproject = false;
                }
                else if (this.certificatepick == '2') {
                    this.itscourse = false;
                    this.itsproject = true;
                }

                let geoarr = [];
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
                }

                let regionarr = [];
                for (var i = 0; i < this.selectedItemsRegion.length; i++) {
                    regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
                }

                let subregionarr = [];
                for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
                    subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
                }

                this.partnertypearr = [];
                for (var i = 0; i < this.selectedPartner.length; i++) {
                    this.partnertypearr.push("'" + this.selectedPartner[i].id + "'");
                }

                this.certificatearr = [];
                for (var i = 0; i < this.selectedCertificate.length; i++) {
                    this.certificatearr.push(this.selectedCertificate[i].id);
                }

                var params =
                {
                    selectGeoRes: this.selectedItemsGeo,
                    dropGeoRes: this.dropdownListGeo,
                    selectRegionRes: this.selectedItemsRegion,
                    dropRegionRes: this.dropdownListRegion,
                    selectSubRegionRes: this.selectedItemsSubRegion,
                    dropSubRegionRes: this.dropdownListSubRegion,
                    selectCountryRes: this.selectedItemsCountry,
                    dropCountryRes: this.dropdownListCountry,
                    selectPartnerRes: this.selectedPartner,
                    dropPartnerRes: this.dropdownPartner,
                    selectCertificateRes: this.selectedCertificate,
                    dropCertificateRes: this.dropdownCertificate,
                    yearRes: this.queryForm.value.filterByYear,
                    dateRes: this.queryForm.value.filterByDate,
                    siteRes: this.queryForm.value.filterBySiteStatus
                }
                //Masukan object filter ke local storage
                localStorage.setItem("filter", JSON.stringify(params));

                // let keyword = {
                //     "filterByGeo" : geoarr.toString(),
                //     "filterByRegion" : subregionarr.toString(),
                //     "filterByCertificateType" : this.certificatearr.toString(),
                //     "filterBySurveyYear" : this.queryForm.value.filterByYear,
                //     "filterByDate" : this.queryForm.value.filterByDate,
                //     "filterByPartnerType" : this.partnertypearr.toString()
                // };

                this.service.httpClientGet('api/ReportEva/StudentSearch/{"filterByGeo":"' + geoarr.toString() + '","filterByRegion":"' + regionarr.toString() + '","filterBySubRegion":"' + subregionarr.toString() + '","filterByCertificateType":"' + this.certificatearr.toString() +
                    '","filterBySurveyYear":"' + this.queryForm.value.filterByYear + '","filterByDate":"' + this.queryForm.value.filterByDate + '","filterByPartnerType":"' + this.partnertypearr.toString() + '"}', '')
                    .subscribe(res => {
                        if (res == "Not Found" || res == null) {
                            this.data = '';
                            this.totalEvaluation = 0;
                            this.ATCFacility = null;
                            this.ATCComputer = null;
                            this.QP1 = [];
                            this.QP2 = [];
                            this.QP3 = [];
                            this.QP4 = [];
                            this.QP5 = [];
                            this.QP6 = [];
                            this.eva1 = [];
                            this.eva2 = [];
                            this.eva3 = [];
                            this.eva4 = [];
                            this.eva5 = [];
                            this.eva6 = [];
                            this.eva7 = [];
                            this.eva8 = [];
                            this.eva9 = [];
                            this.eva10 = [];
                            this.eva11 = [];
                            this.eva12 = [];
                            this.eva13 = [];
                            this.eva14 = [];
                            this.eva15 = [];
                            this.eva16 = [];
                            this.eva17 = [];
                            this.eva18 = [];
                            this.eva19 = [];
                            this.eva20 = [];
                            this.eva21 = [];
                            this.eva22 = [];
                            this.eva23 = [];
                            this.eva24 = [];

                            this.loading = false;
                        } else {
                            this.data = res;

                            this.QP1 = [];
                            this.QP1 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Update"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Level 1: Essentials"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Level 2: Intermediate"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Level 3: Advanced"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Customized"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Other"
                                    }
                                ];

                            this.getproducts();

                            this.gethourtraining(this.certificatepick);

                            this.getType(this.certificatepick);

                            this.QP5 = [];
                            this.QP5 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Instructor-led in the classroom"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Online or e-learning"
                                    }
                                ];

                            this.QP6 = [];
                            this.QP6 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Autodesk Official Courseware (any Autodesk branded material)"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Course material developed by the ATC"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Third Party/Other"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Autodesk Authors and Publishers (AAP) Program Courseware"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Course material developed by the ATCCourse material developed by an independent vendor or instructor"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Online e-learning course material developed by an independent vendor or instructor"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "Online e-learning course material developed by the ATC"
                                    }
                                ];

                            this.eva1 = [];
                            this.eva1 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "yes"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "no"
                                    }
                                ];

                            this.eva2 = [];
                            this.eva2 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "yes"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "no"
                                    }
                                ];

                            this.eva3 = [];
                            this.eva3 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "yes"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "no"
                                    }
                                ];

                            this.eva4 = [];
                            this.eva4 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "yes"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "no"
                                    }
                                ];

                            this.eva5 = [];
                            this.eva5 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva6 = [];
                            this.eva6 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva7 = [];
                            this.eva7 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva8 = [];
                            this.eva8 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];


                            this.eva9 = [];
                            this.eva9 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva10 = [];
                            this.eva10 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva11 = [];
                            this.eva11 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];


                            this.eva12 = [];
                            this.eva12 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva13 = [];
                            this.eva13 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva14 = [];
                            this.eva14 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva15 = [];
                            this.eva15 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "teaching_student",
                                        "text": "Teaching student"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "job",
                                        "text": "Job"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "project",
                                        "text": "Project"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "career",
                                        "text": "Career"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "teaching",
                                        "text": "Teaching"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "evaluate",
                                        "text": "Evaluate"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "personal",
                                        "text": "Personal"
                                    }
                                ];

                            this.eva16 = [];
                            this.eva16 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "professional_part_time",
                                        "text": "Professional part time"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "professional_full_time",
                                        "text": "Professional full time"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "second_student",
                                        "text": "Second student"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "career_technical_student",
                                        "text": "Career technical student"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "higher_education_student",
                                        "text": "Higher education student"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "secondary_educator",
                                        "text": "Secondary educator"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "career_technical_educator",
                                        "text": "Career technical educator"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "higher_education_educator",
                                        "text": "Higher education educator"
                                    }
                                ];

                            this.eva17 = [];
                            this.eva17 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "less_than_6_months",
                                        "text": "Less than 6 months"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "more_than_6_month_less_than_1_year",
                                        "text": "More than 6 month less than 1 year"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "1_to_2_years",
                                        "text": "1 to 2 years"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "2_years_or_more",
                                        "text": "2 years or more"
                                    }
                                ];

                            // this.eva18=[];
                            // this.eva18 =
                            //     [
                            //         {
                            //             "count":0,
                            //             "percent":0,
                            //             "name":"yes",
                            //             "text":"Yes"
                            //         },
                            //         {
                            //             "count":0,
                            //             "percent":0,
                            //             "name":"not_applicable:_i_already_have_an_autodesk_certification",
                            //             "text":"Not Applicable: I already have an Autodesk Certification"
                            //         },
                            //         {
                            //             "count":0,
                            //             "percent":0,
                            //             "name":"no",
                            //             "text":"No"
                            //         },
                            //         {
                            //             "count":0,
                            //             "percent":0,
                            //             "name":"not_sure",
                            //             "text":"Not Sure"
                            //         }
                            //     ];

                            this.eva19 = [];
                            this.eva19 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva20 = [];
                            this.eva20 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva21 = [];
                            this.eva21 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva22 = [];
                            this.eva22 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "0"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "25"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "50"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "75"
                                    },
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "100"
                                    }
                                ];

                            this.eva23 = [];
                            this.eva23 =
                                [
                                    {
                                        "count": 0,
                                        "percent": 0,
                                        "name": "c1",
                                        "text": "Architecture, Engineering & Construction"
                                    }
                                ];


                            setTimeout(() => {
                                for (var i = 0; i < this.data.length; i++) {

                                    this.totalEvaluation = +this.data[i].totalEvaluation + this.totalEvaluation;
                                    this.ATCFacility = this.data[i].ATCFacility;
                                    this.ATCComputer = this.data[i].ATCComputer;

                                    var tl = [this.data[i].UpdateLvl, this.data[i].Essens, this.data[i].Intermed, this.data[i].Adv, this.data[i].Cust, this.data[i].OtherCTL].filter(v => v != '');
                                    var tf = [this.data[i].InstLed, this.data[i].InstOnline].filter(v => v != '');
                                    var tm = [this.data[i].Autodesk, this.data[i].AAP, this.data[i].ATC, this.data[i].Independent, this.data[i].IndependentOnline, this.data[i].ATCOnline, this.data[i].OtherCTM].filter(v => v != '');

                                    for (var j = 0; j < this.QP1.length; j++) {
                                        if (this.QP1[j].name == tl.join(" | ")) {
                                            this.QP1[j].count++;
                                        }
                                    }

                                    for (var j = 0; j < this.QP2.length; j++) {
                                        if (this.QP2[j].name == this.data[i].PrimaryProduct) {
                                            this.QP2[j].count++;
                                        }
                                    }

                                    for (var j = 0; j < this.QP3.length; j++) {
                                        if (this.QP3[j].name == this.data[i].HoursTraining) {
                                            this.QP3[j].count++;
                                        }
                                    }

                                    if (this.certificatepick == '1') {
                                        for (var j = 0; j < this.QP4.length; j++) {
                                            if (this.QP4[j].id == this.data[i].TrainingTypeKey) {
                                                this.QP4[j].count++;
                                            }
                                        }
                                    } else {
                                        for (var j = 0; j < this.QP4.length; j++) {
                                            if (this.QP4[j].id == this.data[i].ProjectTypeKey) {
                                                this.QP4[j].count++;
                                            }
                                        }
                                    }

                                    for (var j = 0; j < this.QP5.length; j++) {
                                        if (this.QP5[j].name == tf.join(" | ")) {
                                            this.QP5[j].count++;
                                        }
                                    }

                                    for (var j = 0; j < this.QP6.length; j++) {
                                        if (this.QP6[j].name == tm.join(" | ")) {
                                            this.QP6[j].count++;
                                        }
                                    }

                                    //eva survey calculation
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question1')) {
                                        for (var j = 0; j < this.eva1.length; j++) {
                                            if (this.eva1[j].name == this.data[i].EvaluationAnswerJson.question1) {
                                                this.eva1[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question2')) {
                                        for (var j = 0; j < this.eva2.length; j++) {
                                            if (this.eva2[j].name == this.data[i].EvaluationAnswerJson.question2) {
                                                this.eva2[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question3')) {
                                        for (var j = 0; j < this.eva3.length; j++) {
                                            if (this.eva3[j].name == this.data[i].EvaluationAnswerJson.question3) {
                                                this.eva3[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question4')) {
                                        for (var j = 0; j < this.eva4.length; j++) {
                                            if (this.eva4[j].name == this.data[i].EvaluationAnswerJson.question4) {
                                                this.eva4[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question5.hasOwnProperty('OE')) {
                                        for (var j = 0; j < this.eva5.length; j++) {
                                            if (this.eva5[j].name == this.data[i].EvaluationAnswerJson.question5.OE) {
                                                this.eva5[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question5.hasOwnProperty('FR_1')) {
                                        for (var j = 0; j < this.eva6.length; j++) {
                                            if (this.eva6[j].name == this.data[i].EvaluationAnswerJson.question5.FR_1) {
                                                this.eva6[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question5.hasOwnProperty('FR_2')) {
                                        for (var j = 0; j < this.eva7.length; j++) {
                                            if (this.eva7[j].name == this.data[i].EvaluationAnswerJson.question5.FR_2) {
                                                this.eva7[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question5.hasOwnProperty('IQ')) {
                                        for (var j = 0; j < this.eva8.length; j++) {
                                            if (this.eva8[j].name == this.data[i].EvaluationAnswerJson.question5.IQ) {
                                                this.eva8[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question5.hasOwnProperty('CCM')) {
                                        for (var j = 0; j < this.eva9.length; j++) {
                                            if (this.eva9[j].name == this.data[i].EvaluationAnswerJson.question5.CCM) {
                                                this.eva9[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question5.hasOwnProperty('FR_1')) {
                                        for (var j = 0; j < this.eva10.length; j++) {
                                            if (this.eva10[j].name == this.data[i].EvaluationAnswerJson.question5.FR_1) {
                                                this.eva10[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question6.hasOwnProperty('CCM')) {
                                        for (var j = 0; j < this.eva11.length; j++) {
                                            if (this.eva11[j].name == this.data[i].EvaluationAnswerJson.question6.CCM) {
                                                this.eva11[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question6.hasOwnProperty('IQ')) {
                                        for (var j = 0; j < this.eva12.length; j++) {
                                            if (this.eva12[j].name == this.data[i].EvaluationAnswerJson.question6.IQ) {
                                                this.eva12[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question6.hasOwnProperty('FR')) {
                                        for (var j = 0; j < this.eva13.length; j++) {
                                            if (this.eva13[j].name == this.data[i].EvaluationAnswerJson.question6.FR) {
                                                this.eva13[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question7.hasOwnProperty('CCM')) {
                                        for (var j = 0; j < this.eva14.length; j++) {
                                            if (this.eva14[j].name == this.data[i].EvaluationAnswerJson.question7.CCM) {
                                                this.eva14[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question8')) {
                                        for (var j = 0; j < this.eva15.length; j++) {
                                            for (var k = 0; k < this.data[i].EvaluationAnswerJson.question8.length; k++) {
                                                if (this.eva15[j].name == this.data[i].EvaluationAnswerJson.question8[k]) {
                                                    this.eva15[j].count++;
                                                }
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question9')) {
                                        for (var j = 0; j < this.eva16.length; j++) {
                                            if (this.eva16[j].name == this.data[i].EvaluationAnswerJson.question9) {
                                                this.eva16[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question10')) {
                                        for (var j = 0; j < this.eva17.length; j++) {
                                            if (this.eva17[j].name == this.data[i].EvaluationAnswerJson.question10) {
                                                this.eva17[j].count++;
                                            }
                                        }
                                    }
                                    // if(this.data[i].EvaluationAnswerJson.hasOwnProperty('question11')){
                                    //     for(var j=0;j<this.eva18.length;j++){
                                    //         if(this.eva18[j].name == this.data[i].EvaluationAnswerJson.question11){
                                    //             this.eva18[j].count++;
                                    //         }
                                    //     }
                                    // }
                                    if (this.data[i].EvaluationAnswerJson.question11.hasOwnProperty('r1')) {
                                        for (var j = 0; j < this.eva19.length; j++) {
                                            if (this.eva19[j].name == this.data[i].EvaluationAnswerJson.question11.r1) {
                                                this.eva19[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question11.hasOwnProperty('r2')) {
                                        for (var j = 0; j < this.eva20.length; j++) {
                                            if (this.eva20[j].name == this.data[i].EvaluationAnswerJson.question11.r2) {
                                                this.eva20[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question11.hasOwnProperty('r3')) {
                                        for (var j = 0; j < this.eva21.length; j++) {
                                            if (this.eva21[j].name == this.data[i].EvaluationAnswerJson.question11.r3) {
                                                this.eva21[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.question11.hasOwnProperty('r4')) {
                                        for (var j = 0; j < this.eva22.length; j++) {
                                            if (this.eva22[j].name == this.data[i].EvaluationAnswerJson.question11.r4) {
                                                this.eva22[j].count++;
                                            }
                                        }
                                    }
                                    if (this.data[i].EvaluationAnswerJson.hasOwnProperty('question12')) {
                                        for (var j = 0; j < this.eva23.length; j++) {
                                            if (this.eva23[j].name == this.data[i].EvaluationAnswerJson.question12) {
                                                this.eva23[j].count++;
                                            }
                                        }
                                    }
                                }

                                var totalQP1 = 0;
                                for (var i = 0; i < this.QP1.length; i++) {
                                    totalQP1 = totalQP1 + this.QP1[i].count;
                                }

                                for (var i = 0; i < this.QP1.length; i++) {
                                    if (totalQP1 > 0) {
                                        this.QP1[i].percent = (this.QP1[i].count / totalQP1) * 100;
                                    } else {
                                        this.QP1[i].percent = 0;
                                    }
                                }

                                var totalQP2 = 0;
                                for (var i = 0; i < this.QP2.length; i++) {
                                    totalQP2 = totalQP2 + this.QP2[i].count;
                                }

                                for (var i = 0; i < this.QP2.length; i++) {
                                    if (totalQP2 > 0) {
                                        this.QP2[i].percent = (this.QP2[i].count / totalQP2) * 100;
                                    } else {
                                        this.QP2[i].percent = 0;
                                    }
                                }

                                var totalQP3 = 0;
                                for (var i = 0; i < this.QP3.length; i++) {
                                    totalQP3 = totalQP3 + this.QP3[i].count;
                                }

                                for (var i = 0; i < this.QP3.length; i++) {
                                    if (totalQP3 > 0) {
                                        this.QP3[i].percent = (this.QP3[i].count / totalQP3) * 100;
                                    } else {
                                        this.QP3[i].percent = 0;
                                    }
                                }

                                var totalQP4 = 0;
                                for (var i = 0; i < this.QP4.length; i++) {
                                    totalQP4 = totalQP4 + this.QP4[i].count;
                                }

                                for (var i = 0; i < this.QP4.length; i++) {
                                    if (totalQP4 > 0) {
                                        this.QP4[i].percent = (this.QP4[i].count / totalQP4) * 100;
                                    } else {
                                        this.QP4[i].percent = 0;
                                    }
                                }

                                var totalQP5 = 0;
                                for (var i = 0; i < this.QP5.length; i++) {
                                    totalQP5 = totalQP5 + this.QP5[i].count;
                                }

                                for (var i = 0; i < this.QP5.length; i++) {
                                    if (totalQP5 > 0) {
                                        this.QP5[i].percent = (this.QP5[i].count / totalQP5) * 100;
                                    } else {
                                        this.QP5[i].percent = 0;
                                    }
                                }

                                var totalQP6 = 0;
                                for (var i = 0; i < this.QP6.length; i++) {
                                    totalQP6 = totalQP6 + this.QP6[i].count;
                                }

                                for (var i = 0; i < this.QP6.length; i++) {
                                    if (totalQP6 > 0) {
                                        this.QP6[i].percent = (this.QP6[i].count / totalQP6) * 100;
                                    } else {
                                        this.QP6[i].percent = 0;
                                    }
                                }

                                //calculation eva responses

                                var totaleva1 = 0;
                                for (var i = 0; i < this.eva1.length; i++) {
                                    totaleva1 = totaleva1 + this.eva1[i].count;
                                }

                                for (var i = 0; i < this.eva1.length; i++) {
                                    if (totaleva1 > 0) {
                                        this.eva1[i].percent = (this.eva1[i].count / totaleva1) * 100;
                                    } else {
                                        this.eva1[i].percent = 0;
                                    }
                                }

                                var totaleva2 = 0;
                                for (var i = 0; i < this.eva2.length; i++) {
                                    totaleva2 = totaleva2 + this.eva2[i].count;
                                }

                                for (var i = 0; i < this.eva2.length; i++) {
                                    if (totaleva2 > 0) {
                                        this.eva2[i].percent = (this.eva2[i].count / totaleva2) * 100;
                                    } else {
                                        this.eva2[i].percent = 0;
                                    }
                                }

                                var totaleva3 = 0;
                                for (var i = 0; i < this.eva3.length; i++) {
                                    totaleva3 = totaleva3 + this.eva3[i].count;
                                }

                                for (var i = 0; i < this.eva3.length; i++) {
                                    if (totaleva3 > 0) {
                                        this.eva3[i].percent = (this.eva3[i].count / totaleva3) * 100;
                                    } else {
                                        this.eva3[i].percent = 0;
                                    }
                                }

                                var totaleva4 = 0;
                                for (var i = 0; i < this.eva4.length; i++) {
                                    totaleva4 = totaleva4 + this.eva4[i].count;
                                }

                                for (var i = 0; i < this.eva4.length; i++) {
                                    if (totaleva4 > 0) {
                                        this.eva4[i].percent = (this.eva4[i].count / totaleva4) * 100;
                                    } else {
                                        this.eva4[i].percent = 0;
                                    }
                                }

                                var totaleva5 = 0;
                                for (var i = 0; i < this.eva5.length; i++) {
                                    totaleva5 = totaleva5 + this.eva5[i].count;
                                }

                                for (var i = 0; i < this.eva5.length; i++) {
                                    if (totaleva5 > 0) {
                                        this.eva5[i].percent = (this.eva5[i].count / totaleva5) * 100;
                                    } else {
                                        this.eva5[i].percent = 0;
                                    }
                                }

                                var totaleva6 = 0;
                                for (var i = 0; i < this.eva6.length; i++) {
                                    totaleva6 = totaleva6 + this.eva6[i].count;
                                }

                                for (var i = 0; i < this.eva6.length; i++) {
                                    if (totaleva6 > 0) {
                                        this.eva6[i].percent = (this.eva6[i].count / totaleva6) * 100;
                                    } else {
                                        this.eva6[i].percent = 0;
                                    }
                                }

                                var totaleva7 = 0;
                                for (var i = 0; i < this.eva7.length; i++) {
                                    totaleva7 = totaleva7 + this.eva7[i].count;
                                }

                                for (var i = 0; i < this.eva7.length; i++) {
                                    if (totaleva7 > 0) {
                                        this.eva7[i].percent = (this.eva7[i].count / totaleva7) * 100;
                                    } else {
                                        this.eva7[i].percent = 0;
                                    }
                                }

                                var totaleva8 = 0;
                                for (var i = 0; i < this.eva8.length; i++) {
                                    totaleva8 = totaleva8 + this.eva8[i].count;
                                }

                                for (var i = 0; i < this.eva8.length; i++) {
                                    if (totaleva8 > 0) {
                                        this.eva8[i].percent = (this.eva8[i].count / totaleva8) * 100;
                                    } else {
                                        this.eva8[i].percent = 0;
                                    }
                                }

                                var totaleva9 = 0;
                                for (var i = 0; i < this.eva9.length; i++) {
                                    totaleva9 = totaleva9 + this.eva9[i].count;
                                }

                                for (var i = 0; i < this.eva9.length; i++) {
                                    if (totaleva9 > 0) {
                                        this.eva9[i].percent = (this.eva9[i].count / totaleva9) * 100;
                                    } else {
                                        this.eva9[i].percent = 0;
                                    }
                                }

                                var totaleva10 = 0;
                                for (var i = 0; i < this.eva10.length; i++) {
                                    totaleva10 = totaleva10 + this.eva10[i].count;
                                }

                                for (var i = 0; i < this.eva10.length; i++) {
                                    if (totaleva10 > 0) {
                                        this.eva10[i].percent = (this.eva10[i].count / totaleva10) * 100;
                                    } else {
                                        this.eva10[i].percent = 0;
                                    }
                                }

                                var totaleva11 = 0;
                                for (var i = 0; i < this.eva11.length; i++) {
                                    totaleva11 = totaleva11 + this.eva11[i].count;
                                }

                                for (var i = 0; i < this.eva11.length; i++) {
                                    if (totaleva11 > 0) {
                                        this.eva11[i].percent = (this.eva11[i].count / totaleva11) * 100;
                                    } else {
                                        this.eva11[i].percent = 0;
                                    }
                                }

                                var totaleva12 = 0;
                                for (var i = 0; i < this.eva12.length; i++) {
                                    totaleva12 = totaleva12 + this.eva12[i].count;
                                }

                                for (var i = 0; i < this.eva12.length; i++) {
                                    if (totaleva12 > 0) {
                                        this.eva12[i].percent = (this.eva12[i].count / totaleva12) * 100;
                                    } else {
                                        this.eva12[i].percent = 0;
                                    }
                                }

                                var totaleva13 = 0;
                                for (var i = 0; i < this.eva13.length; i++) {
                                    totaleva13 = totaleva13 + this.eva13[i].count;
                                }

                                for (var i = 0; i < this.eva13.length; i++) {
                                    if (totaleva13 > 0) {
                                        this.eva13[i].percent = (this.eva13[i].count / totaleva13) * 100;
                                    } else {
                                        this.eva13[i].percent = 0;
                                    }
                                }

                                var totaleva14 = 0;
                                for (var i = 0; i < this.eva14.length; i++) {
                                    totaleva14 = totaleva14 + this.eva14[i].count;
                                }

                                for (var i = 0; i < this.eva14.length; i++) {
                                    if (totaleva14 > 0) {
                                        this.eva14[i].percent = (this.eva14[i].count / totaleva14) * 100;
                                    } else {
                                        this.eva14[i].percent = 0;
                                    }
                                }

                                var totaleva15 = 0;
                                for (var i = 0; i < this.eva15.length; i++) {
                                    totaleva15 = totaleva15 + this.eva15[i].count;
                                }

                                for (var i = 0; i < this.eva15.length; i++) {
                                    if (totaleva15 > 0) {
                                        this.eva15[i].percent = (this.eva15[i].count / totaleva15) * 100;
                                    } else {
                                        this.eva15[i].percent = 0;
                                    }
                                }

                                var totaleva16 = 0;
                                for (var i = 0; i < this.eva16.length; i++) {
                                    totaleva16 = totaleva16 + this.eva16[i].count;
                                }

                                for (var i = 0; i < this.eva16.length; i++) {
                                    if (totaleva16 > 0) {
                                        this.eva16[i].percent = (this.eva16[i].count / totaleva16) * 100;
                                    } else {
                                        this.eva16[i].percent = 0;
                                    }
                                }

                                var totaleva17 = 0;
                                for (var i = 0; i < this.eva17.length; i++) {
                                    totaleva17 = totaleva17 + this.eva17[i].count;
                                }

                                for (var i = 0; i < this.eva17.length; i++) {
                                    if (totaleva17 > 0) {
                                        this.eva17[i].percent = (this.eva17[i].count / totaleva17) * 100;
                                    } else {
                                        this.eva17[i].percent = 0;
                                    }
                                }

                                // var totaleva18=0;
                                // for(var i=0;i<this.eva18.length;i++){
                                //     totaleva18 = totaleva18 + this.eva18[i].count;
                                // }

                                // for(var i=0;i<this.eva18.length;i++){
                                //     if(totaleva18 > 0){
                                //         this.eva18[i].percent = (this.eva18[i].count/totaleva18)*100;
                                //     }else{
                                //         this.eva18[i].percent = 0;
                                //     }
                                // }

                                var totaleva19 = 0;
                                for (var i = 0; i < this.eva19.length; i++) {
                                    totaleva19 = totaleva19 + this.eva19[i].count;
                                }

                                for (var i = 0; i < this.eva19.length; i++) {
                                    if (totaleva19 > 0) {
                                        this.eva19[i].percent = (this.eva19[i].count / totaleva19) * 100;
                                    } else {
                                        this.eva19[i].percent = 0;
                                    }
                                }

                                var totaleva20 = 0;
                                for (var i = 0; i < this.eva20.length; i++) {
                                    totaleva20 = totaleva20 + this.eva20[i].count;
                                }

                                for (var i = 0; i < this.eva20.length; i++) {
                                    if (totaleva20 > 0) {
                                        this.eva20[i].percent = (this.eva20[i].count / totaleva20) * 100;
                                    } else {
                                        this.eva20[i].percent = 0;
                                    }
                                }

                                var totaleva21 = 0;
                                for (var i = 0; i < this.eva21.length; i++) {
                                    totaleva21 = totaleva21 + this.eva21[i].count;
                                }

                                for (var i = 0; i < this.eva21.length; i++) {
                                    if (totaleva21 > 0) {
                                        this.eva21[i].percent = (this.eva21[i].count / totaleva21) * 100;
                                    } else {
                                        this.eva21[i].percent = 0;
                                    }
                                }

                                var totaleva22 = 0;
                                for (var i = 0; i < this.eva22.length; i++) {
                                    totaleva22 = totaleva22 + this.eva22[i].count;
                                }

                                for (var i = 0; i < this.eva22.length; i++) {
                                    if (totaleva22 > 0) {
                                        this.eva22[i].percent = (this.eva22[i].count / totaleva22) * 100;
                                    } else {
                                        this.eva22[i].percent = 0;
                                    }
                                }

                                var totaleva23 = 0;
                                for (var i = 0; i < this.eva23.length; i++) {
                                    totaleva23 = totaleva23 + this.eva23[i].count;
                                }

                                for (var i = 0; i < this.eva23.length; i++) {
                                    if (totaleva23 > 0) {
                                        this.eva23[i].percent = (this.eva23[i].count / totaleva23) * 100;
                                    } else {
                                        this.eva23[i].percent = 0;
                                    }
                                }

                                setTimeout(() => {
                                    this.loading = false;
                                }, 1000)
                            }, 3000)

                        }
                    }, error => {
                        this.data = '';
                        this.dataFound = true;
                        this.loading = false;
                    });
            } else {
                this.loading = false;
                swal(
                    'Field is Required!',
                    'Please select partner type!',
                    'error'
                )
            }
        } catch (e) {
            this.data = '';
            this.totalEvaluation = 0;
            this.ATCFacility = null;
            this.ATCComputer = null;
            this.QP1 = [];
            this.QP2 = [];
            this.QP3 = [];
            this.QP4 = [];
            this.QP5 = [];
            this.QP6 = [];
            this.eva1 = [];
            this.eva2 = [];
            this.eva3 = [];
            this.eva4 = [];
            this.eva5 = [];
            this.eva6 = [];
            this.eva7 = [];
            this.eva8 = [];
            this.eva9 = [];
            this.eva10 = [];
            this.eva11 = [];
            this.eva12 = [];
            this.eva13 = [];
            this.eva14 = [];
            this.eva15 = [];
            this.eva16 = [];
            this.eva17 = [];
            this.eva18 = [];
            this.eva19 = [];
            this.eva20 = [];
            this.eva21 = [];
            this.eva22 = [];
            this.eva23 = [];
            this.eva24 = [];

            this.loading = false;

            console.log("failed");
        }
    }

    redirectsite(id) {
        this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteIdInt: id, filterBySurveyYear: this.queryForm.value.filterByYear, filterByDate: this.queryForm.value.filterByDate, filterByPartnerType: this.partnertypearr.toString() } });
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    // arraygeoid = [];
    // onGeoSelect(item: any) {
    //     this.dataFound = false;
    //     this.arraygeoid.push('"' + item.id + '"');

    //     // Region
    //     var data = '';
    //     this.service.get("api/Region/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListRegion = null;
    //             }
    //             else {
    //                 this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.region_code,
    //                         itemName: item.region_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListSubRegion = null;
    //             }
    //             else {
    //                 this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.subregion_code,
    //                         itemName: item.subregion_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });


    // }

    // OnGeoDeSelect(item: any) {
    //     this.dataFound = false;
    //     var data = '';

    //     //split region
    //     let regionArrTmp = [];
    //     this.service.get("api/Region/filter/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 regionArrTmp = null;
    //             }
    //             else {
    //                 regionArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < regionArrTmp.length; i++) {
    //                     var index = this.selectedItemsRegion.findIndex(x => x.id == regionArrTmp[i].region_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsRegion.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     //split sub region
    //     let subregionArrTmp = [];
    //     this.service.get("api/SubRegion/filter/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 subregionArrTmp = null;
    //             }
    //             else {
    //                 subregionArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < subregionArrTmp.length; i++) {
    //                     var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsSubRegion.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filter/'" + item.id + "'", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 countriesArrTmp = null;
    //             }
    //             else {
    //                 countriesArrTmp = JSON.parse(result);
    //                 for (var i = 0; i < countriesArrTmp.length; i++) {
    //                     var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                     if (index !== -1) {
    //                         this.selectedItemsCountry.splice(index, 1);
    //                     }
    //                 }
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     var index = this.arraygeoid.findIndex(x => x == '"' + item.id + '"');
    //     this.arraygeoid.splice(index, 1);
    //     this.selectedItemsRegion.splice(index, 1);
    //     this.selectedItemsSubRegion.splice(index, 1);
    //     this.selectedItemsCountry.splice(index, 1);

    //     if (this.arraygeoid.length > 0) {
    //         // Region
    //         var data = '';
    //         this.service.get("api/Region/filter/" + this.arraygeoid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListRegion = null;
    //                 }
    //                 else {
    //                     this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.region_code,
    //                             itemName: item.region_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });

    //         // SubRegion
    //         var data = '';
    //         this.service.get("api/SubRegion/filter/" + this.arraygeoid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListSubRegion = null;
    //                 }
    //                 else {
    //                     this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.subregion_code,
    //                             itemName: item.subregion_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });

    //         // Countries
    //         var data = '';
    //         this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //             .subscribe(result => {
    //                 if (result == "Not found") {
    //                     this.service.notfound();
    //                     this.dropdownListCountry = null;
    //                 }
    //                 else {
    //                     this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                         return {
    //                             id: item.countries_code,
    //                             itemName: item.countries_name
    //                         }
    //                     })
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                 });
    //     }
    //     else {
    //         this.dropdownListRegion = [];
    //         this.dropdownListSubRegion = [];
    //         this.dropdownListCountry = [];

    //         this.selectedItemsRegion.splice(index, 1);
    //         this.selectedItemsSubRegion.splice(index, 1);
    //         this.selectedItemsCountry.splice(index, 1);
    //     }
    // }

    // onGeoSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.arraygeoid = [];

    //     for (var i = 0; i < items.length; i++) {
    //         this.arraygeoid.push('"' + items[i].id + '"');
    //     }

    //     // Region
    //     var data = '';
    //     this.service.get("api/Region/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListRegion = null;
    //             }
    //             else {
    //                 this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.region_code,
    //                         itemName: item.region_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListSubRegion = null;
    //             }
    //             else {
    //                 this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.subregion_code,
    //                         itemName: item.subregion_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListCountry = null;
    //             }
    //             else {
    //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.countries_code,
    //                         itemName: item.countries_name
    //                     }
    //                 })
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.dataFound = false;
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    // }

    /* issue popualte geo on country */

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        if (this.adminya) {
            if(this.distributorya){
                url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
            }else{
                if(this.orgya){
                    url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
                }else{
                    if(this.siteya){
                        // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
                        url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
                    }
                }
            }
        }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory         
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin, 
            CtmpTerritory:tmpTerritory               
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
       //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
       var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else{
        //     if(this.itsOrganization() || this.itsSite()){ // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 // url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); /* fixed issue 24092018 - populate country by role site admin */
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory              
        };
        keyword = JSON.stringify(keyword);
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                    // console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }

    /* end issue popualte geo on country */

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.dataFound = false;
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {
        this.dataFound = false;
        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp =result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = [];
            this.dropdownListCountry = [];

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.dataFound = false;
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dataFound = false;
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.dataFound = false;
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {
        this.dataFound = false;
        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = [];

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.dataFound = false;
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dataFound = false;
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onCountriesSelect(item: any) { this.dataFound = false; }
    OnCountriesDeSelect(item: any) { this.dataFound = false; }
    onCountriesSelectAll(items: any) { this.dataFound = false; }
    onCountriesDeSelectAll(items: any) { this.dataFound = false; }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }
}