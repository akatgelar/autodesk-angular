import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EvaluationResponsesEVAComponent,ConvertPercent } from './evaluation-responses.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const EvaluationResponsesEVARoutes: Routes = [
    {
        path: '',
        component: EvaluationResponsesEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.post_evaluation_responses.post_evaluation_responses',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(EvaluationResponsesEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [EvaluationResponsesEVAComponent,ConvertPercent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class EvaluationResponsesEVAModule { }