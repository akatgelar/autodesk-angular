import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { DecimalPipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from '../../../shared/service/session.service';
//import { htmlentityService } from '../../../shared/htmlentities-service/htmlentity-service';

@Component({
    selector: 'app-organization-report-eva',
    templateUrl: './organization-report-eva.component.html',
    styleUrls: [
        './organization-report-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class OrganizationReportEVAComponent implements OnInit {

    apiCall:any
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public foundCourse = false;
    year;
    orgId;
    selectedValueDate;
    selectedYear;
    selectedOrg;
    date;
    performanceReport = [];
    sites = [];
    // countries = [];
    totalEva: any;
    public dataFound = false;
    FR: number = 0;
    IQ: number = 0;
    CCM: number = 0;
    OE: number = 0;
    OP: number = 0;
    partnerType = [];
    certificateType = [];
    eventType = [];
    course: any;
    project: any;
    countries;
    // loading
    public loading = false;
    public showEvent = false;

    // Validasi
    public showValidOrg = false;
    OrgName: string = "";

    useraccesdata: any;

    constructor(public http: Http, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {//, private htmlEntityService: htmlentityService
        this.date = [
            { id: "", name: "Annual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    getFY1() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
            }, error => {
                this.service.errorserver();
            });
    }

    getOrgId() {
        var data = '';
        this.service.httpClientGet("api/MainOrganization/getOrgId", data)
            .subscribe(res => {
                this.orgId = res;
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    dropdownSettingsOrg = {};
    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        this.loading = true;
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        var url = "api/MainOrganization/AddContact";

        // for (var i = 0; i < userlvlarr.length; i++) {
        //     if (userlvlarr[i] == "ORGANIZATION" || userlvlarr[i] == "DISTRIBUTOR" || userlvlarr[i] == "SITE") {
        //         var orgArr = this.useraccesdata.OrgId.toString().split(',');
        //         var orgTmp = [];
        //         for (var i = 0; i < orgArr.length; i++) {
        //             orgTmp.push("'" + orgArr[i] + "'");
        //         }

        //         url = 'api/MainOrganization/AddContactOrg/' + this.useraccesdata.UserLevelId + '/' + orgTmp.join(",");
        //     }
        // }

        /* fixed issue 24092018 - populate org with multiple role distributor and superadmin (selina case) */
        if (this.adminya) {
            url = 'api/MainOrganization/AddContactOrg/' + this.useraccesdata.UserLevelId + '/' + this.urlGetOrgId();
        }
        /* end line fixed issue 24092018 - populate org with multiple role distributor and superadmin (selina case) */

        if (!(localStorage.getItem("filter") == null)) {
            this.getDataOrg(url);
            this.getFY1();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item.selectDateOrg);
            this.selectedPartner = item.selectPartnerOrg;
            this.dropdownPartner = item.dropPartnerOrg;
            this.selectedCertificate = item.selectCertiOrg;
            this.dropdownCertificate = item.dropCertiOrg;
            this.selectedItemsOrganization = item.seletcOrganizationOrg;
            this.selectedYear = parseInt(item.selectYearOrg);
            if (item.selectDateOrg == null || item.selectDateOrg == "") {
                this.selectedValueDate = "";
            } else {
                if (item.selectDateOrg != "q1" && item.selectDateOrg != "q2" && item.selectDateOrg != "q3" && item.selectDateOrg != "q4") {
                    this.selectedValueDate = parseInt(item.selectDateOrg);
                } else {
                    this.selectedValueDate = item.selectDateOrg;
                }
            }
            this.getOrgReport();
        } else {
            // this.selectedPartner = [];
            this.getDataOrg(url);
            this.selectedItemsOrganization = [];
            this.selectedYear = "";
            this.selectedValueDate = "";
            this.getFY();
            this.getPartnerType();
            this.loading = false;
        }
        // this.selectedValueDate = 1;
        // this.getOrgId();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        //setting dropdown
        this.dropdownSettingsOrg = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            // disabled: false,1
            badgeShowLimit: 5,
            maxHeight: 120,
            showCheckbox: false,
            closeOnSelect: true
        };
    }

    certificateDropdown() {
        this.dropdownCertificate = [];

        /* issue 17102018 - automatic remove when change selected partner type */

        this.dropdownEventType = [];
        this.selectedEventType = [];

        /* end line issue 17102018 - automatic remove when change selected partner type */

        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }
        this.selectedCertificate = [];
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.foundCourse = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.foundCourse = false;
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onCertificateSelect(item: any) {
        this.getEventType();
    }
    OnCertificateDeSelect(item: any) {
        this.foundCourse = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.foundCourse = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    changeSelected(value) {
        this.dataFound = false;
    }

    getPerformance(keyword) {
        this.FR = 0;
        this.IQ = 0;
        this.CCM = 0;
        this.OE = 0;
        this.OP = 0;
        var data: any;
        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost("api/EvaluationAnswer/PerformanceReportOrg/", keyword)
            .subscribe(res => {
                data = res;
                // console.log(data);
                if (data != null) {
                    this.FR = data.FR;
                    this.IQ = data.IQ;
                    this.CCM = data.CCM;
                    this.OE = data.OE;
                    this.OP = data.OP;
                } else {
                    this.FR = 0;
                    this.IQ = 0;
                    this.CCM = 0;
                    this.OE = 0;
                    this.OP = 0;
                    // this.totalEva = "-";
                }
            }, error => {
                this.service.errorserver();
                this.FR = 0;
                this.IQ = 0;
                this.CCM = 0;
                this.OE = 0;
                this.OP = 0;
                // this.totalEva = "-";
            });
        /* new post for autodesk plan 17 oct */
    }
    redirectsite(siteId, year, month) {
        this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteId: siteId, filterBySurveyYear: year, filterByDate: month } });
    }

    dropdownListOrganization = [];
    selectedItemsOrganization = [];
    getDataOrg(url) {
        this.service.httpClientGet(url, '')
            .subscribe(result => {
                //var finalresult = result.replace(/\t/g, " ")
                var finalresult :any = result
                if (finalresult == "Not found") {
                    this.service.notfound();
                    this.dropdownListOrganization = null;
                }
                else {
                    this.dropdownListOrganization = Object.keys(finalresult).map(function (Index) {
                        return {
                          id: result[Index].OrgId,
                          itemName: result[Index].OrgId
                        }
                      })

                    /* issue doc report 11102018 - default dropdown org for role site and org admin */

                    if (this.adminya) {
                        if (!this.distributorya) {
                            this.selectedItemsOrganization = finalresult.map((item) => {
                                return {
                                    id: item.OrgId,
                                    itemName: item.OrgId
                                }
                            })
                        }
                    }

                    /* end line issue doc report 11102018 - default dropdown org for role site and org admin */

                }
            },
                error => {
                    this.service.errorserver();
                });

        this.selectedItemsOrganization = [];
    }

    goToInstCourseList(InstructorId, SiteId, FYIndicator, SiteName, RoleId) {
        this.router.navigate(['/report-eva/instructor-course'], { queryParams: { InstructorId: InstructorId, SiteId: SiteId, Year: FYIndicator, SiteName: SiteName, Month: this.selectedValueDate, RoleId: RoleId } });
    }

    getOrgReport() {
        // console.log(this.selectedItemsOrganization);
        this.partnerType = [];
        this.certificateType = [];
        this.eventType = [];
        this.totalEva = 0;

        if (this.selectedItemsOrganization.length > 0) {
            this.selectedOrg = this.selectedItemsOrganization[0].id;
        } else {
            this.selectedOrg = "";
        }

        for (let i = 0; i < this.selectedPartner.length; i++) {
            this.partnerType.push('"' + this.selectedPartner[i].id + '"');
        }

        if (this.selectedCertificate.length > 0) {
            for (let i = 0; i < this.selectedCertificate.length; i++) {
                this.certificateType.push(this.selectedCertificate[i].id);
            }
        }

        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }

        var params =
        {
            selectPartnerOrg: this.selectedPartner,
            dropPartnerOrg: this.dropdownPartner,
            selectCertiOrg: this.selectedCertificate,
            dropCertiOrg: this.dropdownCertificate,
            seletcOrganizationOrg: this.selectedItemsOrganization,
            selectYearOrg: this.selectedYear,
            selectDateOrg: this.selectedValueDate
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
        // console.log(JSON.parse(localStorage.getItem("filter")));

        if (this.selectedOrg == 0) {
            /*
            swal(
                'Field is Required!',
                'Please select an Organization, field required!',
                'error'
            )
            */
            if (this.selectedOrg == 0) {
                this.showValidOrg = true;
            }
            else {
                this.showValidOrg = false;
            }
        }
        else {
            this.loading = true;
            this.showValidOrg = false;
            this.performanceReport = [];
            this.sites = [];
            this.countries = "";

            // "api/EvaluationAnswer/OrganizationReport/{'PartnerType':'" + this.partnerType + "','CertificateType':'" + this.certificateType +
            // "','Year':'" + this.selectedYear + "','Date':'" + this.selectedValueDate + "'}" + "/" + this.selectedOrg

            // var keyword = {
            //     PartnerType: this.partnerType.toString(),
            //     CertificateType: this.certificateType.toString(),
            //     Year: this.selectedYear,
            //     Date: this.selectedValueDate,
            //     EventType: this.eventType.toString(),
            //     OrgId: this.selectedOrg
            // };

            // console.log(keyword);

            /* new request nambah user role -__ */

            var sites = "";

            // if (this.checkrole()) {
            //     if (!this.itsDistributor()) {
            //         if (!this.itsOrganization()) {
            //             if (this.itsSite()) {
            //                 sites = this.urlGetSiteId();
            //             }
            //         }
            //     }
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            if (this.adminya) {
                if (!this.distributorya) {
                    if (!this.orgya) {
                        if (this.siteya) {
                            sites = this.urlGetSiteId();
                        }
                    }
                }
            }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            var keyword = {
                PartnerType: this.partnerType.toString(),
                CertificateType: this.certificateType.toString(),
                Year: this.selectedYear,
                Date: this.selectedValueDate,
                EventType: this.eventType.toString(),
                OrgId: this.selectedOrg,
                SiteId: sites,
                SiteActive: 'A'
            };

            /* new request nambah user role -__ */

            var atc_site: any;
            var org: any;
            var performance: any;
            // this.countries =[];
            this.apiCall =  this.service.httpClientPost("api/EvaluationAnswer/OrganizationReport/", keyword)
                .subscribe(res => {
                    performance = res;
                    if (performance.length > 0) {
                        for (let i = 0; i < performance.length; i++) {
                            if (performance[i].SiteId != null && performance[i].SiteId != "") {
                                this.performanceReport.push(performance[i]);
                                // this.countries.push(performance[i].CountryCode);
                                // if(this.countries.indexOf(performance[i].countries_name) === -1) {
                                //     this.countries.push(performance[i].countries_name);
                                //   }
                                this.totalEva += parseInt(performance[i].Evals);
                            }
                        }
                        this.getPerformance(keyword);
                        // this.sites = performance;

                        this.dataFound = true;
                        this.loading = false;
                    } else {
                        this.FR = 0;
                        this.IQ = 0;
                        this.CCM = 0;
                        this.OE = 0;
                        this.OP = 0;
                        this.performanceReport = [];
                        this.dataFound = true;
                        this.loading = false;
                    }

                    //Punten, ini untuk ngambil yg ATC Site Id
                    // if (this.itsSite()) { //jika user sebagai site admin
                    //     var affiliated = this.urlGetSiteId();
                    // } else {
                    //     var affiliated = "null";
                    // }

                    /* saya tambah logic nya dikit */
                    var affiliated = '';
                    if (this.adminya) {
                        if (!this.distributorya) {
                            if (!this.orgya) {
                                if (this.siteya) {
                                    affiliated = this.urlGetSiteId();
                                }
                            }
                        }
                    }
                    var tmpData : any ={
                        orgId :this.selectedOrg,
                        siteId: affiliated
                    }

                    /* saya tambah logic nya dikit */
                    tmpData = JSON.stringify(tmpData);
                    this.countries =[];
                    let tmpcountries :any=[];

                   // this.service.httpClientGet("api/EvaluationAnswer/ATC_Site/" + this.selectedOrg + "/" + affiliated, atc_site)
                   this.service.httpClientPost("api/EvaluationAnswer/ATC_Site", tmpData)
                        .subscribe(result => {
                            atc_site = result;
                            if (atc_site.length != 0) {
                                this.sites = atc_site;

                                for (let i = 0; i < this.sites.length; i++) {
                                if(tmpcountries.indexOf(this.sites[i].countries_name) === -1) {
                                    tmpcountries.push(this.sites[i].countries_name);
                                  }
                                }
                                this.countries = tmpcountries;
                            } else {
                                this.sites = [];
                            }
                        }, error => {
                            this.sites = [];
                        });

                    //Ambil nama Org
                    this.service.httpClientGet("api/EvaluationAnswer/GetOrgName/" + this.selectedOrg, org)
                        .subscribe(res_org => {
                            org = res_org;
                            // console.log(org);
                            if (org != "") {
                                //this.OrgName = org.OrgName;
                               this.OrgName = this.service.decoder( this.OrgName);
                            } else {
                                this.OrgName = "";
                            }
                        }, error => {
                            this.OrgName = "";
                        });

                }, error => {
                    this.performanceReport = [];
                    this.dataFound = true;
                    this.loading = false;
                });

        }
    }

    resetForm() {
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsOrganization = [];
        this.selectedOrg = [];
        this.selectedPartner = [];
        this.selectedValueDate = [];
        this.selectedYear = [];
    }
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya: Boolean = true;
    checkrole() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya: Boolean = false;
    itsinstructor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya: Boolean = false;
    itsOrganization() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya: Boolean = false;
    itsSite() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya: Boolean = false;
    itsDistributor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/
}
