import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { OrganizationReportEVAComponent } from './organization-report-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const OrganizationReportEVARoutes: Routes = [
    {
        path: '',
        component: OrganizationReportEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.organization_report.organization_report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(OrganizationReportEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [OrganizationReportEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe]
})
export class OrganizationReportEVAModule { }