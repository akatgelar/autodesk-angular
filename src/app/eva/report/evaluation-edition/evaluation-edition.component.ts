import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";

@Component({
    selector: 'app-evaluation-edition',
    templateUrl: './evaluation-edition.component.html',
    styleUrls: [
        './evaluation-edition.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class EvaluationEditionEVAComponent implements OnInit {

    constructor(public http: Http, private service: AppService) { 

    }

    ngOnInit() {
        
    }
}