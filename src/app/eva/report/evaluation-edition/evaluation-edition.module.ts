import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EvaluationEditionEVAComponent } from './evaluation-edition.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";

export const EvaluationEditionEVARoutes: Routes = [
    {
        path: '',
        component: EvaluationEditionEVAComponent,
        data: {
            breadcrumb: 'Evaluation Edition',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(EvaluationEditionEVARoutes),
        SharedModule,
    ],
    declarations: [EvaluationEditionEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe]
})
export class EvaluationEditionEVAModule { }