import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { InstructorStudentEVAComponent } from './instructor-student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
// import { ConvertDatePipe } from './instructor-student.component';

export const InstructorStudentEVARoutes: Routes = [
    {
        path: '',
        component: InstructorStudentEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.instructor_student_list.instructor_student_list',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(InstructorStudentEVARoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [InstructorStudentEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe]
})
export class InstructorStudentEVAModule { }