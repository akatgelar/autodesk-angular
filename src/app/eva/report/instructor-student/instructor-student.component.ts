import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { SessionService } from '../../../shared/service/session.service';
import { Pipe } from "@angular/core";

// Convert Date
// @Pipe({ name: 'convertDate' })
// export class ConvertDatePipe {
//     constructor(private datepipe: DatePipe) { }
//     transform(date: string): any {
//         return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
//     }
// }

@Component({
    selector: 'app-instructor-student',
    templateUrl: './instructor-student.component.html',
    styleUrls: [
        './instructor-student.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class InstructorStudentEVAComponent implements OnInit {

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    private CourseId;
    courseStudent = [];
    SiteName: any;
    SiteId: any;
    InstructorId: any;
    CompletionDate;
    CourseDate: any;
    Evals: any;
    public loading = false;
    public useraccesdata: any;
    public studentName: boolean = false;

    constructor(public http: Http, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    // itsdistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false
    //         }
    //     }
    // }

    // getCourse(course_id) {
    //     this.loading = true;
    //     var data: any;
    //     this.service.httpClientGet("api/EvaluationAnswer/InstructorCourse_1/" + course_id, data)
    //         .subscribe(res => {
    //             data = res;
    //             if (data.length > 0) {
    //                 this.SiteName = data[0].SiteName;
    //                 this.SiteId = data[0].SiteId;
    //                 this.InstructorId = data[0].ContactId;
    //                 this.CourseDate = data[0].CourseDate;
    //                 this.Evals = data[0].Evals;
    //                 this.loading = false;
    //             } else {
    //                 this.SiteName = "-";
    //                 this.SiteId = "-";
    //                 this.InstructorId = "-";
    //                 this.CourseDate = "";
    //                 this.Evals = "-";
    //                 this.loading = false;
    //             }
    //         }, error => {
    //             this.SiteName = "-";
    //             this.SiteId = "-";
    //             this.InstructorId = "-";
    //             this.CourseDate = "";
    //             this.Evals = "-";
    //             this.loading = false;
    //         });
    // }

    getStudent() {
        this.courseStudent = [];
        var data: any;
        this.service.httpClientGet("api/Courses/InstructorStudent/" + this.SiteId + "/" + this.InstructorId + "/" + this.CompletionDate, data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.courseStudent = data;
                    this.Evals = data.length;
                    this.SiteName = data[0].SiteName;
                    this.CourseDate = data[0].CourseDate;
                } else {
                    this.courseStudent = [];
                }
            }, error => {
                this.courseStudent = [];
            });
    }

    ngOnInit() {
        this.SiteId = this.route.snapshot.params['SiteId'];
        this.InstructorId = this.route.snapshot.params['InstructorId'];
        this.CompletionDate = this.route.snapshot.params['CompletionDate'];
        // this.getCourse(this.CourseId);
        this.getStudent();

        if (!this.checkrole()) {
            this.studentName = true;
        }
        else {
            this.studentName = false;
        }
    }
}