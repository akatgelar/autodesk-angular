import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-audit',
    templateUrl: './audit.component.html',
    styleUrls: [
        './audit.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AuditEVAComponent implements OnInit {

    apiCall:any;
    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    public loading = false;
    year;
    selectedValueDate;
    selectedYear;
    date = [];
    report = [];
    listkolom = [];
    totalEvals: number = 0;
    useraccesdata: any;
    fileName = "AuditReport.xls";
    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {

        this.date = [
            { id: "", name: "Annual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    /* populate data issue role distributor */
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true;
            }
        }
    }

    itsinstructor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsDistributor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
    /* populate data issue role distributor */

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        // get Geo
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Geo";
        // } else {
        //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        // }
        /* populate data issue role distributor */
        var url = "api/Geo/SelectAdmin";
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        var  DistributorGeo = null; var OrgIdGeo = null;
        if (this.checkrole()) {
            if (this.itsinstructor()) {
                OrgIdGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'OrgIdGeo'";
            } else {
                if (this.itsDistributor()) {
                    DistributorGeo = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'DistributorGeo'";
                } else {
                    OrgIdGeo = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'OrgIdGeo'";
                }
            }
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                      this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        // this.selectedItemsMarketType = [];
    }

    territorydefault: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        /* populate data issue role distributor */

        // // this.loading = true;
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Territory";
        // } else {
        //     url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        // }
        var  Distributor = null; var OrgId = null;
        if (this.checkrole()) {

            if (this.itsinstructor()) {
                OrgId = this.urlGetOrgId();
               url = "api/Territory/wherenew/'OrgId'";
            } else {
                if (this.itsDistributor()) {
                    Distributor = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'Distributor'";
                } else {
                    OrgId = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'OrgId'";
                }
            }
            
        }
        var keyword : any = {
            Distributor: Distributor,
            OrgId: OrgId,
           
        };
        keyword = JSON.stringify(keyword);
        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.checkrole()) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    ExportExceltoXls()
    {
        this.fileName = "AuditReport.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "AuditReport.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "AuditReport.csv";
        this.ExportExcel();
     } 

    ExportExcel() {
        let jsonData = [];
        // var date = this.service.formatDate();
       // var fileName = "AuditReport.xls";
        var ws = XLSX.utils.json_to_sheet(this.report);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, this.fileName);
        XLSX.writeFile(wb, this.fileName);
    }

    allcountries = [];
    ngOnInit() {

        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // this.getGeo();
            // this.getTerritory();
            // this.getMarketType();
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoDit;
            this.dropdownListGeo = item.dropdownGeoDit;
            this.selectedItemsRegion = item.selectRegionDit;
            this.dropdownListRegion = item.dropdownRegionDit;
            this.selectedItemsTerritories = item.selectTerritoryDit;
            this.dropdownListTerritories = item.dropdownTerritoryDit;
            this.selectedItemsMarketType = item.selectMarketTypeDit;
            this.dropdownListMarketType = item.dropdownMarketTypeDit;
            this.selectedYear = item.selectYearDit;
            this.selectedValueDate = parseInt(item.selectDateDit);
            this.getReport();
        }
        else {
            this.getGeo();
            this.getTerritory();
            this.getMarketType();
            this.selectedValueDate = '';
            // this.selectedYear = "";
        }
        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    changeSelected(value) {
        this.dataFound = false;
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }


    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
            if (this.itsDistributor()) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);


        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
       var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        //this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
        this.service.httpClientPost(url, keyword) 
        .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    // console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }


    arrayterritory = [];
    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';

        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
        if (this.itsDistributor()) {
            DistTerr = this.urlGetOrgId();
           url = "api/Countries/DistTerr";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistTerr:DistTerr,
        CtmpTerritory:this.arrayterritory.toString()
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';

            /* populate data issue role distributor */
             // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }

            var  DistTerr = null;
            if (this.itsDistributor()) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            }
    
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        // Countries
        var data = '';

        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
            if (this.itsDistributor()) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            }
    
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    onRegionSelect(item: any) {
        this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    OnRegionDeSelect(item: any) {
        this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onRegionSelectAll(items: any) {
        this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onRegionDeSelectAll(items: any) {
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onSubRegionSelect(item: any) {
        this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    OnSubRegionDeSelect(item: any) {
        this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onSubRegionSelectAll(items: any) {
        this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onSubRegionDeSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    resetForm(){
        this.dataFound = false;
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsMarketType = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsTerritories = [];
        this.selectedValueDate = [];
        this.selectedYear = [];
    }

    getReport() {
        this.loading = true;
        var geo = [];
        var region = [];
        var territories = [];
        var marketType = [];

        for (let i = 0; i < this.selectedItemsGeo.length; i++) {
            geo.push('"' + this.selectedItemsGeo[i].id + '"');
        }

        for (let i = 0; i < this.selectedItemsRegion.length; i++) {
            region.push(this.selectedItemsRegion[i].id);
        }

        for (let i = 0; i < this.selectedItemsTerritories.length; i++) {
            territories.push(this.selectedItemsTerritories[i].id);
        }

        for (let i = 0; i < this.selectedItemsMarketType.length; i++) {
            marketType.push(this.selectedItemsMarketType[i].id);
        }

        var params =
        {
            selectGeoDit: this.selectedItemsGeo,
            dropdownGeoDit: this.dropdownListGeo,
            selectRegionDit: this.selectedItemsRegion,
            dropdownRegionDit: this.dropdownListRegion,
            selectTerritoryDit: this.selectedItemsTerritories,
            dropdownTerritoryDit: this.dropdownListTerritories,
            selectMarketTypeDit: this.selectedItemsMarketType,
            dropdownMarketTypeDit: this.dropdownListMarketType,
            selectYearDit: this.selectedYear,
            selectDateDit: this.selectedValueDate
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        let Territory: any;
        if (territories.length != 0) {
            Territory = territories;
        } else {
            Territory = "null";
        }

        let MarketType: any;
        if (marketType.length != 0) {
            MarketType = marketType;
        } else {
            MarketType = "null";
        }

        // "api/EvaluationAnswer/AuditReport/{'Geo':'" + geo + "','Year':'" + this.selectedYear + "','Date':'" + this.selectedValueDate + "'}" + "/" + MarketType + "/" + Territory

        var keyword = {
            Geo: geo.toString(),
            Year: this.selectedYear,
            Date: this.selectedValueDate
        };

        this.report = [];
        this.listkolom = [];
        this.totalEvals = 0;
        var data: any;
        /* new post for autodesk plan 17 oct */
        this.apiCall = this.service.httpClientPost("api/EvaluationAnswer/AuditReport/" + MarketType + "/" + Territory, keyword)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        this.report.push(data[i]);
                        // this.totalEvals += parseInt(data[i].Evals);
                    }

                    for (let key in data[0]) {
                        this.listkolom.push(key);
                    }

                    var count: any;
                    this.service.httpClientPost("api/EvaluationAnswer/TotalEvaluation", keyword)
                        .subscribe(res => {
                            count = res;
                            if (count != null && count != undefined) {
                                this.totalEvals = count.Jumlah;
                            } else {
                                this.totalEvals = 0;
                            }
                            this.dataFound = true;
                            this.loading = false;
                        }, error => {
                            this.totalEvals = 0;
                            this.loading = false;
                        });
                } else {
                    this.report = [];
                    this.listkolom = [];
                    this.dataFound = true;
                    this.loading = false;
                }
            }, error => {
                this.report = [];
                this.listkolom = [];
                this.dataFound = true;
                this.loading = false;
            });
        /* new post for autodesk plan 17 oct */
    }
}
