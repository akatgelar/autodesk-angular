import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { DecimalPipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'convertPercent' })
export class ConvertPercent {
    transform(value: string): any {
        return Number.parseFloat(value).toFixed(2);;
    }
}

@Component({
    selector: 'app-channel-performance-eva',
    templateUrl: './channel-performance-eva.component.html',
    styleUrls: [
        './channel-performance-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ChannelPerformanceEVAComponent implements OnInit {

    apiCall:any;
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    public loading = false;
    year;
    selectValueSite;
    siteActive = [];
    countries = [];
    partnerType = [];
    certificateType = [];
    report = [];
    selectedYear;
    eventType = [];
    public showEvent = false;
    public totalEva;
    useraccesdata: any;
    fileName = "ChannelPerformance.xls";
    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private route: ActivatedRoute, private router: Router, private session: SessionService) {

        this.siteActive = [
            { id: "A", name: "Active Only" },
            { id: "I", name: "Inactive Only" }
        ];

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    // getGeo() {
    //     // get Geo
    //     var data = '';
    //     this.service.get("api/Geo", data)
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.dropdownListGeo = null;
    //             }
    //             else {
    //                 this.dropdownListGeo = JSON.parse(result).map((item) => {
    //                     return {
    //                         id: item.geo_id,
    //                         itemName: item.geo_name
    //                     }
    //                 });
    //                 this.selectedItemsGeo = this.dropdownListGeo;
    //             }
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    //     // this.selectedItemsGeo = [];
    // }

    // getTerritory() {
    //     var data: any;
    //     this.service.httpClientGet("api/Territory", data)
    //         .subscribe(res => {
    //             data = res;
    //             if (data.length > 0) {
    //                 this.dropdownListTerritories = data.map((item) => {
    //                     return {
    //                         id: item.TerritoryId,
    //                         itemName: item.Territory_Name
    //                     }
    //                 });
    //             }
    //         }, error => {
    //             this.service.errorserver();
    //         });
    //     // this.selectedItemsTerritories = [];
    // }

    // getCountry(){
    //     let country: any;
    //     this.service.httpClientGet("api/Countries", country)
    //         .subscribe(res => {
    //             country = res;
    //             this.dropdownListCountry = country.map((item)=>{
    //                 return{
    //                     id: item.countries_code,
    //                     itemName: item.countries_name
    //                 }
    //             });
    //         });   
    // }

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    getGeo() {
        var url = "api/Geo/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // get Geo
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                      this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    allcountries = [];
    getCountry() {
        var data = '';
        this.service.httpClientGet("api/Countries", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
                this.allcountries = this.dropdownListCountry;
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsCountry = [];
    }


    territorydefault: string = "";
    getTerritory() {
        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'Distributor'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteId'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteId";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            Distributor: DistributorGeo,
            SiteId: siteRes,
            OrgId: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.adminya) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    ExportExceltoXls()
    {
        this.fileName = "ChannelPerformance.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "ChannelPerformance.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "ChannelPerformance.csv";
        this.ExportExcel();
     } 


    ExportExcel() {
        let jsonData = [];
        if (this.report.length > 0) {
            // console.log(this.report);
            for (let i = 0; i < this.report.length; i++) {
                jsonData.push({
                    "Site ID": this.report[i].SiteId,
                    "Survey Partner Type": (this.report[i].RoleId == '1') ? 'ATC' : 'AAP',
                    "Site Name": this.report[i].SiteName,
                    "Territory": this.report[i].Territory_Name,
                    "Market Type": this.report[i].MarketType,
                    "Type": this.report[i].ParamValue,
                    "Email": this.report[i].SiteManagerEmailAddress,
                    "Q1 Stations": Number(this.report[i].StationsQ1),
                    "Q1 Evals": Number(parseInt(this.report[i].EvalsQ1)),
                    "Q1%": Number(this.report[i].ResQ1),
                    "Q2 Stations": Number(this.report[i].StationsQ2),
                    "Q2 Evals": Number(parseInt(this.report[i].EvalsQ2)),
                    "Q2%": Number(this.report[i].ResQ2),
                    "Q3 Stations": Number(this.report[i].StationsQ3),
                    "Q3 Evals": Number(parseInt(this.report[i].EvalsQ3)),
                    "Q3%": Number(this.report[i].ResQ3),
                    "Q4 Stations": Number(this.report[i].StationsQ4),
                    "Q4 Evals": Number(parseInt(this.report[i].EvalsQ4)),
                    "Q4%": Number(this.report[i].ResQ4),
                    "Total Evals": Number(parseInt(this.report[i].TotalEvals)),
                    "Total%": Number(this.report[i].TotalRes),
                });
            }

            // var date = this.service.formatDate();
           // var fileName = "ChannelPerformance.xls";
            var ws = XLSX.utils.json_to_sheet(jsonData);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, this.fileName);
            XLSX.writeFile(wb, this.fileName);
        }
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        // this.selectedItemsMarketType = [];
    }

    certificateDropdown() {
        this.dropdownCertificate = [];
        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        } else {
            this.dropdownCertificate = [];
        }
        // this.selectedCertificate = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    arrayterritory = [];
    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdminTerr/" + this.arrayterritory.toString() + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var SiteAdminTerr = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        SiteAdminTerr = this.urlGetSiteId();
                        url = "api/Countries/SiteAdminTerr";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            SiteAdminTerr: SiteAdminTerr,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // Countries
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        //split countries
        // let countriesArrTmp = [];
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {

            //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
            var url = "api/Countries/filterByGeoByTerritory";
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
            
            // if (this.adminya) {
            //     if(this.distributorya){
            //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
            //     }else{
            //         if(this.orgya){
            //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
            //         }else{
            //             if(this.siteya){
            //                 url = "api/Countries/SiteAdminTerr/" + this.arrayterritory.toString() + "/" + this.urlGetSiteId();
            //             }
            //         }
            //     }
            // }
            var  DistTerr = null; var OrgSiteTerr = null;var SiteAdminTerr = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistTerr = this.urlGetOrgId();
                   url = "api/Countries/DistTerr";
                } else {
                    if (this.orgya) {
                        OrgSiteTerr = this.urlGetOrgId();
                        url = "api/Countries/OrgSiteTerr";
                    } else {
                        if (this.siteya) {
                            SiteAdminTerr = this.urlGetSiteId();
                            url = "api/Countries/SiteAdminTerr";
                        }
                    }
                }
                
            }
            var keyword : any = {
                CtmpGeo:tmpGeo,
                CDistTerr:DistTerr,
                COrgSiteTerr:OrgSiteTerr,
                SiteAdminTerr: SiteAdminTerr,
                CtmpTerritory:tmpTerritory
               
            };
            keyword = JSON.stringify(keyword);

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // Countries
            var data = '';
            this.service.httpClientPost(url, keyword)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdminTerr/" + this.arrayterritory.toString() + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var SiteAdminTerr = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistTerr = this.urlGetOrgId();
                   url = "api/Countries/DistTerr";
                } else {
                    if (this.orgya) {
                        OrgSiteTerr = this.urlGetOrgId();
                        url = "api/Countries/OrgSiteTerr";
                    } else {
                        if (this.siteya) {
                            SiteAdminTerr = this.urlGetSiteId();
                            url = "api/Countries/SiteAdminTerr";
                        }
                    }
                }
                
            }
            var keyword : any = {
                CtmpGeo:tmpGeo,
                CDistTerr:DistTerr,
                COrgSiteTerr:OrgSiteTerr,
                SiteAdminTerr: SiteAdminTerr,
                CtmpTerritory:tmpTerritory
               
            };
            keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // Countries
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        this.loading = true;
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // this.getGeo();
            // this.getCountry();
            // this.getTerritory();
            // this.getMarketType();
            // this.getPartnerType();
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoChan;
            this.dropdownListGeo = item.dropGeoChan;
            this.selectedItemsRegion = item.selectRegionChan;
            this.dropdownListRegion = item.dropRegionChan;
            this.selectedItemsSubRegion = item.selectSubRegionChan;
            this.dropdownListSubRegion = item.dropSubRegionChan;
            this.selectedItemsTerritories = item.selectTerritoryChan;
            this.dropdownListTerritories = item.dropTerritoryChan;
            this.selectedItemsCountry = item.selectCountryChan;
            this.dropdownListCountry = item.dropCountryChan;
            this.selectedItemsMarketType = item.selectMarketChan;
            this.dropdownListMarketType = item.dropMarketChan;
            this.selectedYear = item.selectYearChan;
            this.selectValueSite = item.selectSiteChan;
            this.selectedPartner = item.selectPartnerChan;
            this.dropdownPartner = item.dropPartnerChan;
            this.selectedCertificate = item.selectCertiChan;
            this.dropdownCertificate = item.dropCertiChan;
            this.getReport();
        }
        else {
            this.getGeo();
            this.getCountry();
            this.getTerritory();
            this.getMarketType();
            // this.selectedYear = "";
            this.selectValueSite = "";
            // this.selectedPartner = [];
            this.getPartnerType();
            this.loading = false;
        }
        this.selectValueSite = 'A';
        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    // onGeoSelect(item: any) {
    //     this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // OnGeoDeSelect(item: any) {
    //     this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoSelectAll(items: any) {
    //     this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.selectedItemsRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelect(item: any) {
    //     this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnRegionDeSelect(item: any) {
    //     this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelectAll(items: any) {
    //     this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelectAll(items: any) {
    //     this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId();
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory          
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // Countries
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

       //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
       var url = "api/Countries/filterByGeoByTerritory";

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); 
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory          
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // Countries
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        
        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }else{
        //         if(this.orgya){
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Countries/SiteAdmin/" + tmpGeo + "/" + this.urlGetSiteId(); 
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var SiteAdmin = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        SiteAdmin = this.urlGetSiteId();
                        url = "api/Countries/SiteAdmin";
                    } 
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            SiteAdmin: SiteAdmin,
            CtmpTerritory:tmpTerritory          
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // Countries
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    // console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onCertificateSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    redirectsite(siteId, year) {
        var createdDate = "";
        var roleId = "";
        var courseId = "";
        this.certificateType = [];
        this.router.navigate(['/report-eva/site-profile'], { queryParams: { SiteId: siteId, filterBySurveyYear: year, filterByDate: createdDate, filterByPartnerType: roleId, filterByCertificateType: this.certificateType.toString(), CourseId: courseId } });
    }

    changeSelected(value) {
        this.dataFound = false;
    }

    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsMarketType = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsTerritories = [];
        this.selectedPartner = [];
        this.selectedYear = [];
        this.selectValueSite = [];
    }

    getReport() {
        this.loading = true;
        var project: any;
        var course: any;
        var marketType = [];
        var territory = [];
        var geoCode = [];
        this.partnerType = [];
        this.countries = [];
        this.certificateType = [];
        this.eventType = [];

        for (let i = 0; i < this.selectedItemsGeo.length; i++) {
            geoCode.push('"' + this.selectedItemsGeo[i].id + '"');
        }

        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            this.countries.push('"' + this.selectedItemsCountry[i].id + '"');
        }

        for (let i = 0; i < this.selectedPartner.length; i++) {
            this.partnerType.push(this.selectedPartner[i].id);
        }

        for (let i = 0; i < this.selectedItemsMarketType.length; i++) {
            marketType.push(this.selectedItemsMarketType[i].id);
        }

        for (let i = 0; i < this.selectedItemsTerritories.length; i++) {
            territory.push(this.selectedItemsTerritories[i].id);
        }

        if (this.selectedCertificate.length > 0) {
            for (let i = 0; i < this.selectedCertificate.length; i++) {
                this.certificateType.push(this.selectedCertificate[i].id);
            }
        }

        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }

        var params =
        {
            selectGeoChan: this.selectedItemsGeo,
            dropGeoChan: this.dropdownListGeo,
            selectRegionChan: this.selectedItemsRegion,
            dropRegionChan: this.dropdownListRegion,
            selectSubRegionChan: this.selectedItemsSubRegion,
            dropSubRegionChan: this.dropdownListSubRegion,
            selectTerritoryChan: this.selectedItemsTerritories,
            dropTerritoryChan: this.dropdownListTerritories,
            selectCountryChan: this.selectedItemsCountry,
            dropCountryChan: this.dropdownListCountry,
            selectMarketChan: this.selectedItemsMarketType,
            dropMarketChan: this.dropdownListMarketType,
            selectYearChan: this.selectedYear,
            selectSiteChan: this.selectValueSite,
            selectPartnerChan: this.selectedPartner,
            dropPartnerChan: this.dropdownPartner,
            selectCertiChan: this.selectedCertificate,
            dropCertiChan: this.dropdownCertificate
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        var contactid = "";
        var orgid = "";
        var distributor = "";

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         distributor = this.urlGetOrgId();
        //     } else {
        //         if (this.itsinstructor()) {
        //             contactid = this.useraccesdata.UserId;
        //         } else {
        //             orgid = this.urlGetOrgId();
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        if (this.adminya) {
            if (this.distributorya) {
                distributor = this.urlGetOrgId();
            } else {
                if (this.trainerya) {
                    contactid = this.useraccesdata.UserId;
                } else {
                    orgid = this.urlGetOrgId();
                }
            }
        }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // "api/EvaluationAnswer/ChannelPerformance/{'CountryCode':'" + this.countries + "','Year':'" + this.selectedYear +
        // "','SiteActive':'" + this.selectValueSite + "','PartnerType':'" + this.partnerType + "','CertificateType':'" + this.certificateType + "','MarketType':'" + marketType + "'}"

        var keyword = {
            CountryCode: this.countries.toString(),
            Year: this.selectedYear,
            SiteActive: this.selectValueSite,
            PartnerType: this.partnerType.toString(),
            CertificateType: this.certificateType.toString(),
            MarketType: marketType.toString(),
            EventType: this.eventType.toString(),
            GeoCode: geoCode.toString(),
            Territory: territory.toString(),
            
            ContactId: contactid,
            OrgId: orgid,
            Distributor: distributor
        };
        // console.log(keyword);

        this.report = [];
        let report_temp = [];
        var station: any;
        var data: any;
        this.totalEva = 0;
        /* new post for autodesk plan 17 oct */
        this.apiCall = this.service.httpClientPost("api/EvaluationAnswer/ChannelPerformance", keyword)
            .subscribe(res => {
                data = res;
                // console.log(data);
                if (data.length > 0) {
                    report_temp = data;
                    // this.http.post("api/EvaluationAnswer/Station", keyword, { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) })
                    //     .subscribe(result => {
                    //         station = JSON.parse(result["_body"]);
                    //         if (station.length > 0) {

                    //         }
                    //     });
                    for (let i = 0; i < report_temp.length; i++) {
                        var resQ1: any = 0;
                        var resQ2: any = 0;
                        var resQ3: any = 0;
                        var resQ4: any = 0;
                        var TotalEvals: any = 0;
                        var TotalRes: any = 0;
                        var TotalStation: any = 0;

                        let EvalsQ1: any = 0;
                        let EvalsQ2: any = 0;
                        let EvalsQ3: any = 0;
                        let EvalsQ4: any = 0;
                        let MultiplierOverride: any = 0;
                        let StationsQ1: any = 0;
                        let StationsQ2: any = 0;
                        let StationsQ3: any = 0;
                        let StationsQ4: any = 0;

                        EvalsQ1 = parseInt(report_temp[i].EvalsQ1);
                        EvalsQ2 = parseInt(report_temp[i].EvalsQ2);
                        EvalsQ3 = parseInt(report_temp[i].EvalsQ3);
                        EvalsQ4 = parseInt(report_temp[i].EvalsQ4);
                        MultiplierOverride = parseInt(report_temp[i].MultiplierOverride);
                        StationsQ1 = parseInt(report_temp[i].StationsQ1);
                        StationsQ2 = parseInt(report_temp[i].StationsQ2);
                        StationsQ3 = parseInt(report_temp[i].StationsQ3);
                        StationsQ4 = parseInt(report_temp[i].StationsQ4);

                        //Q1%
                        if (EvalsQ1 == 0 || MultiplierOverride == 0 || StationsQ1 == 0) {
                            resQ1 = 0;
                        } else {
                            resQ1 = EvalsQ1 / ((MultiplierOverride / 4) * StationsQ1) * 100;
                        }

                        //Q2%
                        if (EvalsQ2 == 0 || MultiplierOverride == 0 || StationsQ2 == 0) {
                            resQ2 = 0;
                        } else {
                            resQ2 = EvalsQ2 / ((MultiplierOverride / 4) * StationsQ2) * 100;
                        }

                        //Q3%
                        if (EvalsQ3 == 0 || MultiplierOverride == 0 || StationsQ3 == 0) {
                            resQ3 = 0;
                        } else {
                            resQ3 = EvalsQ3 / ((MultiplierOverride / 4) * StationsQ3) * 100;
                        }

                        //Q4%
                        if (EvalsQ4 == 0 || MultiplierOverride == 0 || StationsQ4 == 0) {
                            resQ4 = 0;
                        } else {
                            resQ4 = EvalsQ4 / ((MultiplierOverride / 4) * StationsQ4) * 100;
                        }

                        TotalEvals = EvalsQ1 + EvalsQ2 + EvalsQ3 + EvalsQ4;
                        TotalStation = StationsQ1 + StationsQ2 + StationsQ3 + StationsQ4;

                        //TotalRes
                        if (TotalEvals == 0 || MultiplierOverride == 0 || TotalStation == 0) {
                            TotalRes = 0;
                        } else {
                            TotalRes = TotalEvals / (MultiplierOverride * TotalStation) * 100;
                        }

                        report_temp[i]["ResQ1"] = resQ1.toFixed(2);
                        report_temp[i]["ResQ2"] = resQ2.toFixed(2);
                        report_temp[i]["ResQ3"] = resQ3.toFixed(2);
                        report_temp[i]["ResQ4"] = resQ4.toFixed(2);
                        report_temp[i]["TotalEvals"] = report_temp[i].num_of_eval;
                        report_temp[i]["TotalRes"] = TotalRes.toFixed(2);

                    }
                    //Cari totalnya jek hehehe
                   
                    var count: any;
                    /* new post for autodesk plan 17 oct */
                    this.service.httpClientPost("api/EvaluationAnswer/TotalEvaluationChannel", keyword)
                        .subscribe(res => {
                            count = res;
                            if (count != null && count != undefined) {
                                this.totalEva = count.Jumlah;
                            } else {
                                this.totalEva = 0;
                            }
                            this.dataFound = true;
                            this.loading = false;
                        }, error => {
                            this.totalEva = 0;
                            this.dataFound = true;
                            this.loading = false;
                        });
                    /* new post for autodesk plan 17 oct */
                    this.report = report_temp;
                    // console.log(report_temp);
                } else {
                    this.totalEva = 0;
                    this.report = [];
                    this.loading = false;
                    this.dataFound = true;
                }
            }, error => {
                this.report = [];
                this.dataFound = true;
                this.loading = false;
            });
        /* new post for autodesk plan 17 oct */
    }
}
