import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ChannelPerformanceEVAComponent,ConvertPercent } from './channel-performance-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const ChannelPerformanceEVARoutes: Routes = [
    {
        path: '',
        component: ChannelPerformanceEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.channel_performance.channel_performance',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ChannelPerformanceEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [ChannelPerformanceEVAComponent,ConvertPercent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class ChannelPerformanceEVAModule { }