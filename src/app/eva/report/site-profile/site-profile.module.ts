import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SiteProfileEVAComponent } from './site-profile.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';
import { ToNumberPipe } from './site-profile.component';

export const SiteProfileEVARoutes: Routes = [
    {
        path: '',
        component: SiteProfileEVAComponent,
        data: {
            breadcrumb: 'Site Detail Performance',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SiteProfileEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [SiteProfileEVAComponent, ToNumberPipe],
    providers: [AppService, AppFormatDate, DatePipe]
})
export class SiteProfileEVAModule { }