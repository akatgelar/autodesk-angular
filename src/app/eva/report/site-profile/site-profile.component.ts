import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
// import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'toNumber' })
export class ToNumberPipe implements PipeTransform {
    transform(value: string): any {
        var newvalue = value.replace(',', '.');
        let retNumber = Number(newvalue);
        return isNaN(retNumber) ? 0 : retNumber;
    }
}

@Component({
    selector: 'app-site-profile',
    templateUrl: './site-profile.component.html',
    styleUrls: [
        './site-profile.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SiteProfileEVAComponent implements OnInit {

    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public foundCourse = false;
    year: any;
    date: any;
    id: String = "";

    SiteId: String = "";
	ProductId: String = "";
    filterBySurveyYear;
    filterByDate;
    filterByPartnerType: String = "";
    filterByCertificateType: String = "";
    queryForm: FormGroup;
    public loading = false;
    courseId;

    CCM: number = 0;
    FR: number = 0;
    IQ: number = 0;
    OP: number = 0;
    OE: number = 0;
    TotalEva: number = 0;
    Country;
    SiteName;
    InstructorId;

    constructor(public http: Http, private service: AppService, private route: ActivatedRoute, private router: Router, ) {

        let filterByYear = new FormControl('');
        let filterByDate = new FormControl('');

        this.queryForm = new FormGroup({
            filterByYear: filterByYear,
            filterByDate: filterByDate
        });

        this.date = [
            { id: "", name: "Annual Year" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" },
            { id: 1, name: "January" },
            { id: "q1", name: "Q1" },
            { id: "q2", name: "Q2" },
            { id: "q3", name: "Q3" },
            { id: "q4", name: "Q4" }
        ];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    // this.selectedPartner =
                    //     [
                    //         {
                    //             id: '1',
                    //             itemName: 'Authorized Training Center (ATC)'
                    //         },
                    //         {
                    //             id: '58',
                    //             itemName: 'Authorized Academic Partner (AAP)'
                    //         }
                    //     ]

                    // this.certificateDropdown()
                });
    }

    ngOnInit() {
        this.loading = true;

        this.getPartnerType();
        this.getFY();

        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.selectedPartner = item.selectPartnerTor;
            this.selectedCertificate = item.selectCertiTor;
        }

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        var sub: any;
        var date: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || "";
			this.ProductId =params['ProductId'] || "";
            this.filterBySurveyYear = params['filterBySurveyYear'] || "";
            date = params['filterByDate'] || "";
            this.filterByPartnerType = params['filterByPartnerType'] || "";
            this.filterByCertificateType = params['filterByCertificateType'] || "";
            this.courseId = params['CourseId'] || "";
            this.InstructorId = params['InstructorId'] || "";
        });

        if (date != "" && date != "q1" && date != "q2" && date != "q3" && date != "q4") {
            this.filterByDate = parseInt(date);
        } else {
            this.filterByDate = date;
        }

        this.queryForm.patchValue({ filterByDate: this.filterByDate });
        this.queryForm.patchValue({ filterByYear: this.filterBySurveyYear });
        let partnerselected = this.filterByPartnerType.split(',');
        let certificateselected = this.filterByCertificateType.split(',');

        for (var i = 0; i < partnerselected.length; i++) {
            if (partnerselected[i] == "'1'") {
                this.selectedPartner.push(
                    {
                        id: 1,
                        itemName: 'Authorized Training Center (ATC)'
                    }
                )
            }
            else if (partnerselected[i] == "'58'") {
                this.selectedPartner.push(
                    {
                        id: 58,
                        itemName: 'Authorized Academic Partner (AAP)'
                    }
                )
            }
        }

        for (var i = 0; i < certificateselected.length; i++) {
            if (certificateselected[i] == '1') {
                this.selectedCertificate.push(
                    {
                        id: 1,
                        itemName: 'Course'
                    }
                )
            }
            else if (certificateselected[i] == '2') {
                this.selectedCertificate.push(
                    {
                        id: 2,
                        itemName: 'Project'
                    }
                )
            }
            else if (certificateselected[i] == '3') {
                this.selectedCertificate.push(
                    {
                        id: 3,
                        itemName: 'Event'
                    }
                )
            }
        }
        this.TotalEva = 0;
        this.CCM = 0;
        this.FR = 0;
        this.IQ = 0;
        this.OP = 0;
        this.OE = 0;
        var detail_perf: any;
        var dataSite: any;
        this.service.httpClientGet('api/ReportEva/SiteDetailPerf/{"SiteId":"' + this.SiteId + '","ProductID":"' + this.ProductId + '","filterBySurveyYear":"' + this.filterBySurveyYear + '","filterByDate":"' + this.filterByDate + '"}', '')
            .subscribe(res => {
                detail_perf = res;

                if (detail_perf.SiteId == undefined) {
                    this.service.notfound();
                    this.CCM = 0;
                    this.FR = 0;
                    this.IQ = 0;
                    this.OP = 0;
                    this.OE = 0;
                    this.TotalEva = 0;
                    this.service.httpClientGet('api/ReportEva/SiteInfo/' + this.SiteId, dataSite)
                        .subscribe(res1 => {
                            dataSite = res1;
                            this.Country = dataSite.countries_name;
                            this.SiteName = dataSite.SiteName;
                        });
                    this.data = "";
                }
                else {
                    this.CCM = detail_perf.CCM;
                    this.FR = detail_perf.FR;
                    this.IQ = detail_perf.IQ;
                    this.OP = detail_perf.OP;
                    this.OE = detail_perf.OE;
                    this.TotalEva = detail_perf.Total;
                    this.Country = detail_perf.countries_name;
                    this.SiteName = detail_perf.SiteName;

                    //get Site Instructor Performance
                    this.service.httpClientGet('api/ReportEva/SiteInstructorPerf/{"SiteId":"' + this.SiteId + '","ProductID":"' + this.ProductId + '","filterBySurveyYear":"' + this.filterBySurveyYear + '","filterByDate":"' + this.filterByDate + '","filterByPartnerType":"' + this.filterByPartnerType + '","InstructorId":"' + this.InstructorId + '"}', '')
                        .subscribe(result => {
                            if (result == "Not found") {
                                this.service.notfound();
                                this.data = '';
                            }
                            else {
                                this.data = result
                            }
                            this.loading = false;
                        },
                            error => {
                                this.service.errorserver();
                                this.data = '';
                                this.loading = false;
                            });
                }
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });

    }

    goToInstCourseList(InstructorId) {
        this.router.navigate(['/report-eva/instructor-course'], { queryParams: { InstructorId: InstructorId, SiteId: this.SiteId,ProductID: this.ProductId, Year: this.filterBySurveyYear, SiteName: this.SiteName, Month: this.filterByDate } });
    }

    partnertypearr = [];
    certificatearr = [];
    onSubmit() {
        this.loading = true;
        this.data = null;
        this.TotalEva = 0;
        this.CCM = 0;
        this.FR = 0;
        this.IQ = 0;
        this.OP = 0;
        this.OE = 0;

        this.partnertypearr = [];
        for (var i = 0; i < this.selectedPartner.length; i++) {
            this.partnertypearr.push("'" + this.selectedPartner[i].id + "'");
        }

        this.certificatearr = [];
        for (var i = 0; i < this.selectedCertificate.length; i++) {
            this.certificatearr.push(this.selectedCertificate[i].id);
        }

        var detail_perf: any;
        var dataSite: any;
        this.service.httpClientGet('api/ReportEva/SiteDetailPerf/{"SiteId":"' + this.SiteId + '","ProductID":"' + this.ProductId + '","filterBySurveyYear":"' + this.queryForm.value.filterByYear + '","filterByDate":"' + this.queryForm.value.filterByDate + '"}', '')
            .subscribe(res => {
                detail_perf = res;
                if (detail_perf.SiteId == undefined) {
                    this.service.notfound();
                    this.CCM = 0;
                    this.FR = 0;
                    this.IQ = 0;
                    this.OP = 0;
                    this.OE = 0;
                    this.TotalEva = 0;
                    this.service.httpClientGet('api/ReportEva/SiteInfo/' + this.SiteId, dataSite)
                        .subscribe(res1 => {
                            dataSite = res1;
                            this.Country = dataSite.countries_name;
                            this.SiteName = dataSite.SiteName;
                        });
                    this.data = "";
                }
                else {
                    this.CCM = detail_perf.CCM;
                    this.FR = detail_perf.FR;
                    this.IQ = detail_perf.IQ;
                    this.OP = detail_perf.OP;
                    this.OE = detail_perf.OE;
                    this.TotalEva = detail_perf.Total;
                    this.Country = detail_perf.countries_name;
                    this.SiteName = detail_perf.SiteName;

                    //get Site Instructor Performance
                    this.service.httpClientGet('api/ReportEva/SiteInstructorPerf/{"SiteId":"' + this.SiteId + '","filterBySurveyYear":"' + this.queryForm.value.filterByYear + '","filterByDate":"' + this.queryForm.value.filterByDate + '"}', '')
                        .subscribe(result => {
                            if (result == "Not found") {
                                this.service.notfound();
                                this.data = '';
                            }
                            else {
                                this.data = result
                                this.filterBySurveyYear = this.queryForm.value.filterByYear;
                                this.filterByDate = this.queryForm.value.filterByDate;
                            }
                            this.loading = false;
                        },
                            error => {
                                this.service.errorserver();
                                this.data = '';
                                this.loading = false;
                            });
                }
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
    }

    certificateDropdown() {
        this.dropdownCertificate = [];
        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        } else {
            this.dropdownCertificate = [];
        }
        // this.selectedCertificate = [];
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.foundCourse = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.foundCourse = false;
    }

    onCertificateSelect(item: any) { }
    OnCertificateDeSelect(item: any) {
        this.foundCourse = false;
    }
    onCertificateSelectAll(item: any) { }
    onCertificateDeSelectAll(item: any) {
        this.foundCourse = false;
    }

    // findPerformance() {
    //     var partnerType = [];
    //     var course: any;
    //     var project: any;
    //     var eventAAP: any;

    //     for (let i = 0; i < this.selectedPartner.length; i++) {
    //         partnerType.push(this.selectedPartner[i].id);
    //     }

    //     if (this.selectedCertificate.length > 0) {
    //         for (let i = 0; i < this.selectedCertificate.length; i++) {
    //             if (this.selectedCertificate[i] == 1) {
    //                 course = 1;
    //             } else if (this.selectedCertificate[i] == 2) {
    //                 project = 1;
    //             } else if (this.selectedCertificate[i] == 3) {
    //                 eventAAP = 1;
    //             } else {
    //                 course = "";
    //                 project = "";
    //                 eventAAP = "";
    //             }
    //         }
    //     } else {
    //         course = "";
    //         project = "";
    //         eventAAP = "";
    //     }

    //     this.performance = [];
    //     var data: any;
    //     this.service.httpClientGet("api/EvaluationAnswer/PerformanceReport/{'SiteId':'" + this.SiteId + "','Year':'" + this.selectedYear + "','Date':'" + this.selectedValueDate +
    //         "','PartnerType':'" + partnerType + "','AAP_Course':'" + course + "','AAP_Project':'" + project +
    //         "','AAP_Event':'" + eventAAP + "','GroupByContact':'true'}", data)
    //         .subscribe(res => {
    //             data = res;
    //             if (data.length > 0) {
    //                 for (let i = 0; i < data.length; i++) {
    //                     if (data[i].SiteID != null && data[i].SiteID != "") {
    //                         this.performance.push(data[i]);
    //                     }
    //                 }
    //             } else {
    //                 this.performance = [];
    //             }
    //         }, error => {
    //             // this.service.errorserver();
    //             this.performance = [];
    //         });
    // }
}