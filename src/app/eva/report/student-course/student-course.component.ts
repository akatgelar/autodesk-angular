import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as Survey from "survey-angular";
import { Injectable } from '@angular/core';
import { SessionService } from '../../../shared/service/session.service';

var surveyJSON = {};

@Component({
    selector: 'app-student-course',
    templateUrl: './student-course.component.html',
    styleUrls: [
        './student-course.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class StudentCourseEVAComponent implements OnInit {

    private StudentID;
    private StudentName: string = "";
    private Company: string = "";
    private Address1: string = "";
    private Phone: string = "";
    private Email: string = "";
    private CourseDate: any = "";
    private ContactId: string = ""; //Pakai ContactID yg ada di tabel Courses (Nanti diganti pakai Instructor ID yaa)
    private TeachingLevel: string = "";
    private PrimarySoftware: string = "";
    private SecondarySoftware: string = "";
    private TrainingHours: string = "";
    private TrainingFormat: string = "";
    private TrainingMaterials: string = "";
    evaCode;
    surveyAnswer: any;
    questionResult = [];
    CourseId;
    EvaluationAnswerId;
    InstructorId;
    public loading = false;
    public useraccesdata: any;
    public studentName: boolean = false;

    constructor(public http: Http, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    CourseTeachingLvl(update, essential, intermediate, advanced, custom, other, comment) {
        let arr_ctl = [update, essential, intermediate, advanced, custom, other];
        var index = arr_ctl.indexOf(1);
        var ctl = "";
        switch (index) {
            case 0:
                ctl = "Update"
                break;
            case 1:
                ctl = "Level 1 : Essentials";
                break;
            case 2:
                ctl = "Level 2 : Intermediate";
                break;
            case 3:
                ctl = "Level 3 : Advanced";
                break;
            case 4:
                ctl = "Customized";
                break;

            default:
                ctl = "Other : " + comment;
                break;
        }

        return ctl;
    }

    CourseTrainingMaterial(autodesk, aap, atc, independent, independent_online, atc_online, other, comment) {
        let arr_ctm = [autodesk, aap, atc, independent, independent_online, atc_online, other];
        var index = arr_ctm.indexOf(1);
        var ctm = "";
        switch (index) {
            case 0:
                ctm = "Autodesk Official Courseware (any Autodesk branded material)";
                break;
            case 1:
                ctm = "Autodesk Authors and Publishers (AAP) Program Courseware";
                break;
            case 2:
                ctm = "Course material developed by the ATC";
                break;
            case 3:
                ctm = "Course material developed by an independent vendor or instructor";
                break;
            case 4:
                ctm = "Online e-learning course material developed by an independent vendor or instructor";
                break;
            case 5:
                ctm = "Online e-learning course material developed by the ATC";
                break;

            default:
                ctm = "Other : " + comment;
                break;
        }

        return ctm;
    }

    dataStudent() {
        this.loading = true;
        var data: any;
        this.service.httpClientGet("api/Student/GetStudentCourse_1/" + this.EvaluationAnswerId, data)
            .subscribe(res => {
                data = res;
                if (data != null) {
                    // console.log(data);
                        if (data.StudentName != null && data.StudentName != '') {
                            this.StudentName= this.service.decoder(data.StudentName);
                        }
                   // this.StudentName = data.StudentName;
                    this.Company = data.Company;
                    // this.Address1 = data.Address;
                    // this.Phone = data.Phone;
                    this.Email = data.Email;
                    this.CourseDate = data.CourseDate;
                    // var tl = [data.UpdateLvl, data.Essens, data.Intermed, data.Adv, data.Cust, data.OtherCTL].filter(v => v != '');
                    this.TeachingLevel = this.CourseTeachingLvl(data.Update, data.Essentials, data.Intermediate, data.Advanced, data.Customized, data.OtherCTL, data.CommentCTL);
                    this.PrimarySoftware = data.PrimaryProduct;
                    // this.SecondarySoftware = data.SecondaryProduct;
                    this.TrainingHours = data.HoursTraining;
                    // var tf = [data.InstLed, data.InstOnline].filter(v => v != '');
                    this.TrainingFormat = (data.InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                    // var tm = [data.Autodesk, data.AAP, data.ATC, data.Independent, data.IndependentOnline, data.ATCOnline, data.OtherCTM].filter(v => v != '');
                    this.TrainingMaterials = this.CourseTrainingMaterial(data.Autodesk, data.AAP, data.ATC, data.Independent, data.IndependentOnline, data.ATCOnline, data.OtherCTM, data.CommentCTM);
                    // this.evaCode = data.EvaluationQuestionCode;
                    this.surveyAnswer = data.EvaluationAnswerJson;

                    surveyJSON = JSON.stringify(data.EvaluationQuestionTemplate);
                    Survey.Survey.cssType = "bootstrap";
                    var survey = new Survey.Model(surveyJSON);
                    survey.data = data.EvaluationAnswerJson;
                    //read only
                    survey.mode = "display";
                    // survey.onComplete.add(this.sendDataToServer);
                    Survey.SurveyNG.render("surveyElement", { model: survey });
                    this.loading = false;
                }
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
    }

    ngOnInit() {
        this.EvaluationAnswerId = this.route.snapshot.params['EvaluationAnswerId'];
        this.InstructorId = this.route.snapshot.params['InstructorId'];
        this.dataStudent();

        if (!this.checkrole()) {
            this.studentName = true;
        }
        else {
            this.studentName = false;
        }
    }
}