import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { StudentCourseEVAComponent } from './student-course.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const StudentCourseEVARoutes: Routes = [
    {
        path: '',
        component: StudentCourseEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.student_course_evaluation.student_course_evaluation',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(StudentCourseEVARoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [StudentCourseEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe]
})
export class StudentCourseEVAModule { }