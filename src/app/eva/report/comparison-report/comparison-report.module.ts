import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ComparisonReportEVAComponent } from './comparison-report.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

export const ComparisonReportEVARoutes: Routes = [
    {
        path: '',
        component: ComparisonReportEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.comparison_report.comparison_report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ComparisonReportEVARoutes),
        SharedModule,
        AngularMultiSelectModule
    ],
    declarations: [ComparisonReportEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class ComparisonReportEVAModule { }