import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import * as c3 from "c3";
declare const $: any;
declare var Morris: any;
import "../../../../assets/echart/echarts-all.js";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from "sweetalert2";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

@Component({
  selector: "app-comparison-report",
  templateUrl: "./comparison-report.component.html",
  styleUrls: [
    "./comparison-report.component.css",
    "../../../../../node_modules/c3/c3.min.css"
  ],
  encapsulation: ViewEncapsulation.None
})
export class ComparisonReportEVAComponent implements OnInit {
  dropdownPartner = [];
  selectedPartner = [];

  dropdownCertificate = [];
  selectedCertificate = [];

  dropdownListGeo = [];
  selectedItemsGeo = [];

  dropdownListRegion = [];
  selectedItemsRegion = [];

  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];

  dropdownListCountry = [];
  selectedItemsCountry = [];

  dropdownSettings = {};

  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 500;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  public dataFound = false;
  year;
  selectedValueDate;
  date = [];
  selectValueSite;
  siteActive = [];
  selectedValueType;
  selectedYear;
  type;
  listkey = [];
  report = [];
  evalTotal: number = 0;

  constructor(
    public http: Http,
    private service: AppService,
    private filterGeo: AppFilterGeo
  ) {
    this.date = [
      { id: 2, name: "February" },
      { id: 3, name: "March" },
      { id: 4, name: "April" },
      { id: 5, name: "May" },
      { id: 6, name: "June" },
      { id: 7, name: "July" },
      { id: 8, name: "August" },
      { id: 9, name: "September" },
      { id: 10, name: "October" },
      { id: 11, name: "November" },
      { id: 12, name: "December" },
      { id: 1, name: "January" },
      { id: "q1", name: "Q1" },
      { id: "q2", name: "Q2" },
      { id: "q3", name: "Q3" },
      { id: "q4", name: "Q4" }
    ];

    this.siteActive = [
      { id: "active", name: "Active Only" },
      { id: "inactive", name: "Inactive Only" }
    ];

    this.type = [
      { id: "1", name: "Details" },
      { id: "2", name: "Summary by country" },
      { id: "3", name: "Summary by region" },
      { id: "4", name: "Summary by geo" },
      { id: "5", name: "Summary by world" }
    ];
  }

  getGeo() {
    // get Geo
    var data = "";
    this.service.httpClientGet("api/Geo", data).subscribe(
      result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListGeo = null;
        } else {
          this.dropdownListGeo = Object.keys(result).map(function (Index) {
            return {
              id: result[Index].geo_id,
              itemName: result[Index].geo_name
            }
          })
        }
      },
      error => {
        this.service.errorserver();
      }
    );
    this.selectedItemsGeo = [];
  }

  certificateDropdown() {
    if (this.selectedPartner.length > 0) {
      for (let i = 0; i < this.selectedPartner.length; i++) {
        if (this.selectedPartner[i].id == 58) {
          this.dropdownCertificate = [
            { id: 1, itemName: "Course" },
            { id: 2, itemName: "Project" },
            { id: 3, itemName: "Event" }
          ];
        } else {
          this.dropdownCertificate = [];
        }
      }
    } else {
      this.dropdownCertificate = [];
    }
    this.selectedCertificate = [];
  }

  getFY() {
    var parent = "FYIndicator";
    var data = "";
    this.service
      .httpClientGet(
        "api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}",
        data
      )
      .subscribe(
        res => {
          this.year = res;
        },
        error => {
          this.service.errorserver();
        }
      );
  }

  ngOnInit() {
    this.getGeo();
    this.getFY();
    this.selectedValueDate = "";
    this.selectValueSite = "";
    this.selectedValueType = "1";
    this.selectedYear = "";
    this.dropdownPartner = [
      { id: 1, itemName: "Authorized Training Center (ATC)" },
      { id: 58, itemName: "Authorized Academic Partner (AAP)" }
    ];
    this.selectedPartner = [];
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };
  }

  changeSelected(value) {
    this.dataFound = false;
  }

  onItemSelect(item: any) {
    this.dataFound = false;
  }
  OnItemDeSelect(item: any) {
    this.dataFound = false;
  }
  onSelectAll(items: any) {
    this.dataFound = false;
  }
  onDeSelectAll(items: any) {
    this.dataFound = false;
  }

  onGeoSelect(item: any) {
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
    this.dataFound = false;
  }

  OnGeoDeSelect(item: any) {
    this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
    this.dataFound = false;
  }

  onGeoSelectAll(items: any) {
    this.filterGeo.filterGeoOnSelectAll(
      this.selectedItemsGeo,
      this.dropdownListRegion
    );
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.selectedItemsRegion = [];
    this.dropdownListCountry = [];
    this.selectedItemsCountry = [];
    this.dataFound = false;
  }

  onGeoDeSelectAll(items: any) {
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
    this.dataFound = false;
  }

  onRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
    this.selectedItemsCountry = [];
    this.dataFound = false;
  }

  OnRegionDeSelect(item: any) {
    this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
    this.dataFound = false;
  }

  onRegionSelectAll(items: any) {
    this.filterGeo.filterRegionOnSelectAll(
      this.selectedItemsRegion,
      this.dropdownListSubRegion
    );
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
    this.selectedItemsCountry = [];
    this.dataFound = false;
  }

  onRegionDeSelectAll(items: any) {
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
    this.dataFound = false;
  }

  onSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.dataFound = false;
  }

  OnSubRegionDeSelect(item: any) {
    this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.dataFound = false;
  }

  onSubRegionSelectAll(items: any) {
    this.filterGeo.filterSubRegionOnSelectAll(
      this.selectedItemsSubRegion,
      this.dropdownListCountry
    );
    this.selectedItemsCountry = [];
    this.dataFound = false;
  }

  onSubRegionDeSelectAll(items: any) {
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
    this.dataFound = false;
  }

  onCountriesSelect(item: any) {
    this.dataFound = false;
  }

  OnCountriesDeSelect(item: any) {
    this.dataFound = false;
  }

  onCountriesSelectAll(items: any) {
    this.dataFound = false;
  }

  onCountriesDeSelectAll(items: any) {
    this.dataFound = false;
  }

  onPartnerSelect(item: any) {
    this.selectedCertificate = [];
    this.certificateDropdown();
    this.dataFound = false;
  }

  OnPartnerDeSelect(item: any) {
    this.selectedCertificate = [];
    this.certificateDropdown();
    this.dataFound = false;
  }

  onPartnerSelectAll(items: any) {
    this.selectedCertificate = [];
    this.certificateDropdown();
    this.dataFound = false;
  }

  onPartnerDeSelectAll(items: any) {
    this.selectedCertificate = [];
    this.dropdownCertificate = [];
    this.certificateDropdown();
    this.dataFound = false;
  }

  onCertificateSelect(item: any) {
    this.dataFound = false;
  }
  OnCertificateDeSelect(item: any) {
    this.dataFound = false;
  }
  onCertificateSelectAll(item: any) {
    this.dataFound = false;
  }
  onCertificateDeSelectAll(item: any) {
    this.dataFound = false;
  }

  getReport() {
    var countries = [];
    var partnerType = [];
    var certificateType = [];
    var serviceTemp: string = "";
    var course: string = "";
    var project: string = "";
    var event: string = "";

    for (let i = 0; i < this.selectedItemsCountry.length; i++) {
      countries.push(this.selectedItemsCountry[i].id);
    }

    for (let i = 0; i < this.selectedPartner.length; i++) {
      partnerType.push(this.selectedPartner[i].id);
    }

    if (this.selectedCertificate.length > 0) {
      for (let i = 0; i < this.selectedCertificate.length; i++) {
        // certificateType.push(this.selectedCertificate[i]);
        if (this.selectedCertificate[i] == 1) {
          course = "true";
        } else if (this.selectedCertificate[i] == 2) {
          project = "true";
        } else if (this.selectedCertificate[i] == 3) {
          event = "true";
        } else {
          course = "";
          project = "";
          event = "";
        }
      }
    } else {
      course = "";
      project = "";
      event = "";
    }

    var data: any;
    this.report = [];
    this.listkey = [];
    this.service
      .httpClientGet(
        "api/EvaluationAnswer/ComparisonRpt/{'CountryCode':'" +
          countries +
          "','PartnerType':'" +
          partnerType +
          "','CertificateType':'" +
          certificateType +
          "','Year':'" +
          this.selectedYear +
          "','Date':'" +
          this.selectedValueDate +
          "','SiteActive':'" +
          this.selectValueSite +
          "','AAP_Course':'" +
          course +
          "','AAP_Project':'" +
          project +
          "','AAP_Event':'" +
          event +
          "'}",
        data
      )
      .subscribe(
        res => {
          data = res;
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              if (this.selectedValueType == 1) {
                var columns = {
                  "Org ID": data[i].ATCOrgId,
                  "Site ID": data[i].ATCSiteId,
                  "Site Name": data[i].SiteName,
                  "Eval ID": data[i].EvaluationQuestionCode,
                  "First Name": data[i].Firstname,
                  "Last Name": data[i].Lastname,
                  "Survey Q5": "",
                  "Post Survey P5": "",
                  "Deviation Q5/P5": "",
                  "Survey Q1.1": "",
                  "Post Survey Q1": "",
                  "Deviation Q1.1/Q1": ""
                };
              } else if (this.selectedValueType == 2) {
                // var columns = {
                //     'Geo':
                //     'Region':
                //     'Country':
                //     'Certified Original Survey':
                //     'Certified Post Survey':
                //     'Changed his/her mind to getting certified (+)':
                //     'Changed his/her mind to not to get certified (-)':
                //     'Satisfaction Increased':
                //     'Satisfaction Same':
                //     'Satisfaction Decreased':
                // };
              } else if (this.selectedValueType == 3) {
                // var columns = {
                //     'Geo':
                //     'Region':
                //     'Certified Original Survey':
                //     'Certified Post Survey':
                //     'Changed his/her mind to getting certified (+)':
                //     'Changed his/her mind to not to get certified (-)':
                //     'Satisfaction Increased':
                //     'Satisfaction Same':
                //     'Satisfaction Decreased':
                // };
              } else if (this.selectedValueType == 4) {
                // var columns = {
                //     'Geo':
                //     'Certified Original Survey':
                //     'Certified Post Survey':
                //     'Changed his/her mind to getting certified (+)':
                //     'Changed his/her mind to not to get certified (-)':
                //     'Satisfaction Increased':
                //     'Satisfaction Same':
                //     'Satisfaction Decreased':
                // };
              } else {
                // var columns = {
                //     'Certified Original Survey':
                //     'Certified Post Survey':
                //     'Changed his/her mind to getting certified (+)':
                //     'Changed his/her mind to not to get certified (-)':
                //     'Satisfaction Increased':
                //     'Satisfaction Same':
                //     'Satisfaction Decreased':
                // };
              }
              this.report.push(columns);
              this.evalTotal = this.evalTotal + i;
            }
            for (let key in this.report[0]) {
              this.listkey.push(key);
            }
            // console.log(this.listkey);
            // console.log(this.report);
          } else {
            this.report = [];
            this.listkey = [];
          }
          this.dataFound = true;
        },
        error => {
          this.report = [];
          this.dataFound = true;
        }
      );
  }
}
