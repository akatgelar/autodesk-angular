import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SubmissionLanguageEVAComponent } from './submissions-by-language-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const SubmissionLanguageEVARoutes: Routes = [
    {
        path: '',
        component: SubmissionLanguageEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.submission_language.submission_language',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SubmissionLanguageEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [SubmissionLanguageEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class SubmissionLanguageEVAModule { }