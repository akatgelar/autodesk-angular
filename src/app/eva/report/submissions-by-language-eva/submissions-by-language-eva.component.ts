import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-submissions-by-language-eva',
    templateUrl: './submissions-by-language-eva.component.html',
    styleUrls: [
        './submissions-by-language-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SubmissionLanguageEVAComponent implements OnInit {

    apiCall:any;
    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};

    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    public loading = false;
    year;
    selectedYear;
    reportResult = [];
    evalTotal: number = 0;
    eventType = [];
    public showEvent = false;
    useraccesdata: any;
    fileName = "SubmissionsLang.xls";
    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {
        
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    /* populate data issue role distributor */
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true;
            }
        }
    }

    itsinstructor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsDistributor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
    /* populate data issue role distributor */

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        var url = "api/Geo/SelectAdmin";
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        var  DistributorGeo = null; var OrgIdGeo = null;
        if (this.checkrole()) {

            if (this.itsinstructor()) {
                OrgIdGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'OrgIdGeo'";
            } else {
                if (this.itsDistributor()) {
                    DistributorGeo = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'DistributorGeo'";
                } else {
                    OrgIdGeo = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'OrgIdGeo'";
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                    this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    allcountries = [];
    getCountry() {
        this.loading = true;
        // get Country
        var data = '';
        this.service.httpClientGet("api/Countries", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    this.loading = false;
                }
                this.allcountries = this.dropdownListCountry;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        // this.selectedItemsCountry = [];
    }

    certificateDropdown() {
        this.dropdownCertificate = [];
        // this.selectedCertificate = [];
        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        }

    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                    this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    this.selectedPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                    //show value certificate
                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }

    ExportExceltoXls()
    {
        this.fileName = "SubmissionsLang.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "SubmissionsLang.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "SubmissionsLang.csv";
        this.ExportExcel();
     } 

    ExportExcel() {
        let jsonData = [];
        if (this.reportResult.length > 0) {
            for (let i = 0; i < this.reportResult.length; i++) {
                jsonData.push({
                    "Language": this.reportResult[i].Text,
                    "# of Evaluation": parseInt(this.reportResult[i].Evals),
                });
            }

            // var date = this.service.formatDate();
           // var fileName = "SubmissionsLang.xls";
            var ws = XLSX.utils.json_to_sheet(jsonData);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, this.fileName);
            XLSX.writeFile(wb, this.fileName);
        }
    }

    ngOnInit() {

        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getCountry();
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.selectedItemsGeo = item.selectGeoLang;
            this.dropdownListGeo = item.dropGeoLang;
            this.selectedItemsCountry = item.selectCountryLang;
            this.dropdownListCountry = item.dropCountryLang;
            this.selectedYear = item.selectYearLang;
            this.selectedPartner = item.selectPartnerLang;
            // this.dropdownPartner = item.dropPartnerLang;
            this.selectedCertificate = item.selectCertiLang;
            this.dropdownCertificate = item.dropCertiLang;
            this.getReport();
        }
        else {
            this.getGeo();
            this.getCountry();
            this.getPartnerType();
            // this.selectedYear = "";
            // this.selectedPartner = [];
        }

        this.getFY();
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);
        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"

        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
        this.service.httpClientPost(url, keyword)  
        .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedPartner = [];
        this.selectedYear = [];
    }

    getReport() {
        this.loading = true;
        var geo = [];
        var countries = [];
        var partnerType = [];
        var certificateType = [];
        this.eventType = [];
        var course;
        var project;

        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            countries.push('"' + this.selectedItemsCountry[i].id + '"');
        }

        for (let i = 0; i < this.selectedPartner.length; i++) {
            partnerType.push(this.selectedPartner[i].id);
        }

        if (this.selectedCertificate.length > 0) {
            for (let i = 0; i < this.selectedCertificate.length; i++) {
                certificateType.push(this.selectedCertificate[i].id);
            }
        } else {
            certificateType = [];
        }

        for (let i = 0; i < this.selectedItemsGeo.length; i++) {
            geo.push('"' + this.selectedItemsGeo[i].id + '"');
        }

        if (this.selectedEventType.length > 0) {
            for (let i = 0; i < this.selectedEventType.length; i++) {
                this.eventType.push('"' + this.selectedEventType[i].id + '"');
            }
        }

        var params =
            {
                selectGeoLang: this.selectedItemsGeo,
                dropGeoLang: this.dropdownListGeo,
                selectCountryLang: this.selectedItemsCountry,
                dropCountryLang: this.dropdownListCountry,
                selectYearLang: this.selectedYear,
                selectPartnerLang: this.selectedPartner,
                dropPartnerLang: this.dropdownPartner,
                selectCertiLang: this.selectedCertificate,
                dropCertiLang: this.dropdownCertificate
            }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        // "api/EvaluationAnswer/SubmissionByLanguage/{'CountryCode':'" + countries + "','PartnerType':'" + partnerType + "','AAP_Course':'" + course +
        // "','AAP_Project':'" + project + "'}"

        var keyword = {
            CountryCode: countries.toString(),
            PartnerType: partnerType.toString(),
            CertificateType: certificateType.toString(),
            GeoCode: geo.toString(),
            Year: this.selectedYear,
            EventType: this.eventType.toString()
        };

        this.reportResult = [];
        var data: any;
        this.evalTotal = 0;
        /* new post for autodesk plan 17 oct */
        this.apiCall = this.service.httpClientPost("api/EvaluationAnswer/SubmissionByLanguage", keyword)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.reportResult = data;
                    for (let i = 0; i < data.length; i++) {
                        this.evalTotal += parseInt(data[i].Evals);
                    }
                } else {
                    this.reportResult = [];
                    this.evalTotal = 0;
                }
                this.dataFound = true;
                this.loading = false;
            }, error => {
                this.reportResult = [];
                this.dataFound = true;
                this.loading = false;
            });
        /* new post for autodesk plan 17 oct */

    }
}
