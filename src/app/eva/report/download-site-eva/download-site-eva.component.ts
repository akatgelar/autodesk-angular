import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';
import { saveAs } from 'file-saver';

const now = new Date();

@Component({
    selector: 'app-download-site-eva',
    templateUrl: './download-site-eva.component.html',
    styleUrls: [
        './download-site-eva.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DownloadSiteEVAComponent implements OnInit {
 
    apiCall:any;
    validTer = true;
    validCer = true;
    validPartner = true;
    selectedPrimaryProduct;
    Primary = [];

    selectedSecondaryProduct;
    Secondary = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownListOrganizationId = [];
    selectedItemsOrganizationId = [];

    dropdownListSiteId = [];
    selectedItemsSiteId = [];

    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownEventType = [];
    selectedEventType = [];

    dropdownSettings = {};
    dropdownSettingsSearch = {};
    dropdownSettingsSearch2 = {};
    performanceReport = [];
    dropdownSettingsSingle = {};
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public dataFound = false;
    public loading = false;
    public url2 = "api/Countries/SelectAdmin";
    public ptType :string;
    year;
    orgId;
    selectedValueDateRange;
    dateRange;
    selectedYear;
    report = [];
    excel = [];
    expexcel = [];
    report_temp = [];
    listkolom = [];
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    eventType = [];
    certificateType = [];
    public showEvent = false;
    useraccesdata: any;
    questionStruct: any;
    public loading_1 = false;
    keyword: any;
    totalEval: number = 0;
    validcertificate: boolean = true;
    fileName: string  = "DownloadSiteEvaluation.xls";
    
    // query_current: string = "";
    public getServerData(event,pType) {
        // console.log(event);
        if(pType == null){
            pType = JSON.parse(this.keyword.PartnerType);
        }

        var isSite = this.itsSite();
        var isOrg = this.itsOrganization();
        var isDistributor = this.itsDistributor();
        
        this.loading_1 = true;
        this.report_temp = this.report; //hanya untuk menahan ukuran tinggi tabel
        var limit = 10;
        var ofset = (event - 1) * 10;
        var endpage = Math.floor(this.countdata / 10) + 1;

        if (event == 1) {
            ofset = 0;
        } else if (event == endpage) {
            limit = this.countdata % 10;
        }

        this.report = null;
        if (isSite) {
            this.keyword.SiteID = this.useraccesdata.SiteId;
            this.keyword.OrgID = this.useraccesdata.OrgId;

        }
        if (isOrg) {
            this.keyword.SiteID = this.useraccesdata.SiteId;
            this.keyword.OrgID = this.useraccesdata.OrgId;

        }
        this.keyword.Role = this.useraccesdata.UserLevelId;
        this.keyword.Limit = limit;
        this.keyword.Offset = ofset;
        this.keyword.Primary = this.selectedPrimaryProduct;
        this.keyword.Secondary = this.selectedSecondaryProduct;
        var data: any;

        /* autodesk plan 18 oct */

        /*this.service.post("api/EvaluationAnswer/SiteEvaluation", this.keyword)
            .subscribe(res => {*/
                //Update by Soe
        this.service.httpClientPost("api/EvaluationAnswer/SiteEvaluation/"+pType, this.keyword)
           .subscribe(res => {
			   data=null;
			   this.listkolom=[];
                data = res;
                if (data.length > 0) {
                   /* for (let x = 0; x < data.length; x++) {
                        let answer = data[x].EvaluationAnswerJson;

                        if (typeof answer === 'object' || answer == "") { /* issue 17102018 - cant extract excel -> escape char */
                      /*      data[x]["QP1"] = this.CourseTeachingLvl(data[x].Update, data[x].Essentials, data[x].Intermediate, data[x].Advanced, data[x].Customized, data[x].OtherCTL, data[x].CommentCTL);
                            data[x]["QP2"] = data[x].primaryProduct;
                            data[x]["QP3"] = data[x].HoursTraining;
                            data[x]["QP4"] = (data[x].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                            data[x]["QP5"] = this.CourseTeachingMaterial(data[x].Autodesk, data[x].AAP, data[x].ATC, data[x].Independent, data[x].IndependentOnline, data[x].ATCOnline, data[x].OtherCTM, data[x].CommentCTM);
                            data[x]["Q1.1"] = "";
                            data[x]["Q1.2"] = "";
                            data[x]["Q1.3"] = "";
                            data[x]["Q1.4"] = "";
                            data[x]["Q2"] = "";
                            data[x]["Q2.1"] = "";
                            data[x]["Q2.2"] = "";
                            data[x]["Q2.3"] = "";
                            data[x]["Q2.4"] = "";
                            data[x]["Q2.5"] = "";
                            data[x]["Q2.6"] = "";
                            data[x]["Q3"] = "";
                            data[x]["Q3.1"] = "";
                            data[x]["Q3.2"] = "";
                            data[x]["Q3.3"] = "";
                            data[x]["Q4"] = "";
                            data[x]["Q5"] = "";
                            data[x]["Q6"] = "";
                            data[x]["Q7"] = "";
                            data[x]["Q8"] = "";
                            data[x]["Q8.1"] = "";
                            data[x]["Q8.2"] = "";
                            data[x]["Q8.3"] = "";
                            data[x]["Q8.4"] = "";
                            data[x]["Q9"] = "";
                            data[x]["Q9.1"] = "";
                            data[x]["Q9.2"] = "";
                            data[x]["Q9.3"] = "";
                            data[x]["Q9.4"] = "";
                            data[x]["Q10"] = "";
                            data[x]["Q11"] = "";
                            data[x]["Q11.1"] = "";
                            data[x]["Q11.2"] = "";
                            data[x]["Q11.3"] = "";
                            data[x]["Q11.4"] = "";
                            data[x]["Q11.5"] = "";
                            data[x]["CourseFacility"] = (data[x].ATCFacility == '0') ? "ATC Facility" : "Onsite";
                            data[x]["ComputerEquipment"] = (data[x].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                        } else {
                            if (answer.indexOf('\\r\\n') > -1) {
                                answer = answer.replace(/\\r\\n/g, "");
                                answer = answer.replace(/  /g, "");
                                answer = answer.replace(/    /g, "");
                            }
                            answer = answer.replace(/\\/g, "");
                            
                            //cara untuk ngambil bagian komentarnya aja
                            let index_1;
                            let prev_comm;
                            if (answer.indexOf('"comment": "') > -1) {
                                index_1 = answer.indexOf('"comment": "'); 
                                prev_comm = answer.substr(index_1 + 12, answer.length); 
                                prev_comm = prev_comm.replace(/\n|\r/g, ''); /* issue 01112018 - keep loading on submited date 10 oct - 11 oct */
                          /*  }

                            if (answer.indexOf('"comment":"') > -1) {
                                index_1 = answer.indexOf('"comment":"'); 
                                prev_comm = answer.substr(index_1 + 11, answer.length); 
                            }

                            /* issue 01112018 - keep loading on submited date 10 oct - 11 oct */

                           /* if (answer.indexOf('"Comments": "') > -1) {
                                index_1 = answer.indexOf('"Comments": "');
                                prev_comm = answer.substr(index_1 + 13, answer.length); 
                                prev_comm = prev_comm.replace(/\n|\r/g, '');
                            }

                            if (answer.indexOf('"Comments":"') > -1) {
                                index_1 = answer.indexOf('"Comments":"'); 
                                prev_comm = answer.substr(index_1 + 12, answer.length); 
                            }

                            /* end line issue 01112018 - keep loading on submited date 10 oct - 11 oct */

                          /*  prev_comm = prev_comm.replace(/"}/g, ''); //hilangkan karakter akhirnya
                            let new_comm = prev_comm.replace(/"/g, '\\"'); //escape char untuk isi komentarnya
                            let new_answer = answer.replace(prev_comm, new_comm); //replace yg lama dengan yg baru

                            /* issue 17102018 - cant extract excel -> escape char */

                          /*  new_answer = new_answer.toString().replace(/\r/g,"");
                            new_answer = new_answer.toString().replace(/\n/g,"");

                            /* end line issue 17102018 - cant extract excel -> escape char */
                            
                           /* new_answer = JSON.parse(new_answer);

                            var koplak = Object.keys(new_answer);

                            if (koplak[0] != "question1") {
                                for (let q = 0; q < this.questionStruct.length; q++) {
                                    switch (this.questionStruct[q].QuestionNumber) {
                                        case "QP1":
                                            data[x]["QP1"] = this.CourseTeachingLvl(data[x].Update, data[x].Essentials, data[x].Intermediate, data[x].Advanced, data[x].Customized, data[x].OtherCTL, data[x].CommentCTL);
                                            break;
                                        case "QP2":
                                            data[x]["QP2"] = data[x].primaryProduct;
                                            break;
                                        case "QP3":
                                            data[x]["QP3"] = data[x].HoursTraining;
                                            break;
                                        case "QP4":
                                            data[x]["QP4"] = (data[x].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                                            break;
                                        case "QP5":
                                            data[x]["QP5"] = this.CourseTeachingMaterial(data[x].Autodesk, data[x].AAP, data[x].ATC, data[x].Independent, data[x].IndependentOnline, data[x].ATCOnline, data[x].OtherCTM, data[x].CommentCTM);
                                            break;
                                        case "Q1.1":
                                            data[x]["Q1.1"] = new_answer.q1;
                                            break;
                                        case "Q1.2":
                                            data[x]["Q1.2"] = new_answer.q2;
                                            break;
                                        case "Q1.3":
                                            data[x]["Q1.3"] = new_answer.q3;
                                            break;
                                        case "Q1.4":
                                            
                                            /* issue 01112018 - keep loading on submited date 10 oct - 11 oct */
                                            
                                        /*    data[x]["Q1.4"] = "";
                                            if (new_answer.hasOwnProperty('q4')) {
                                                data[x]["Q1.4"] = new_answer.q4;    
                                            }

                                            /* end line issue 01112018 - keep loading on submited date 10 oct - 11 oct */
                                            
                                   /*         break;
                                        case "Q2":
                                            data[x]["Q2"] = "";
                                            break;
                                        case "Q2.1":
                                            data[x]["Q2.1"] = this.selectedScore(parseInt(new_answer.q5["OE"]));
                                            break;
                                        case "Q2.2":
                                            data[x]["Q2.2"] = this.selectedScore(parseInt(new_answer.q5["FR_1"]));
                                            break;
                                        case "Q2.3":
                                            data[x]["Q2.3"] = this.selectedScore(parseInt(new_answer.q5["FR_2"]));
                                            break;
                                        case "Q2.4":
                                            data[x]["Q2.4"] = "";
                                            break;
                                        case "Q2.5":
                                            data[x]["Q2.5"] = this.selectedScore(parseInt(new_answer.q5["IQ"]));
                                            break;
                                        case "Q2.6":
                                            data[x]["Q2.6"] = this.selectedScore(parseInt(new_answer.q5["CCM"]));
                                            break;
                                        case "Q3":
                                            data[x]["Q3"] = "";
                                            break;
                                        case "Q3.1":
                                            data[x]["Q3.1"] = this.selectedScore(parseInt(new_answer.q6["CCM"]));
                                            break;
                                        case "Q3.2":
                                            data[x]["Q3.2"] = this.selectedScore(parseInt(new_answer.q6["IQ"]));
                                            break;
                                        case "Q3.3":
                                            data[x]["Q3.3"] = this.selectedScore(parseInt(new_answer.q6["FR"]));
                                            break;
                                        case "Q4":
                                            data[x]["Q4"] = this.selectedScore(parseInt(new_answer.q7["CCM"]));
                                            break;
                                        case "Q5":
                                            var arr_temp = new_answer.q8.filter(v => v != '');
                                            data[x]["Q5"] = arr_temp.join(" / ");
                                            break;
                                        case "Q6":
                                            data[x]["Q6"] = new_answer.q9.replace(/_/g, " ");
                                            break;
                                        case "Q7":
                                            data[x]["Q7"] = new_answer.q10.replace(/_/g, " ");
                                            break;
                                        case "Q8":
                                            data[x]["Q8"] = "";
                                            break;
                                        case "Q8.1":
                                            data[x]["Q8.1"] = "";
                                            break;
                                        case "Q8.2":
                                            data[x]["Q8.2"] = "";
                                            break;
                                        case "Q8.3":
                                            data[x]["Q8.3"] = "";
                                            break;
                                        case "Q8.4":
                                            data[x]["Q8.4"] = "";
                                            break;
                                        case "Q9":
                                            data[x]["Q9"] = "";
                                            break;
                                        case "Q9.1":
                                            data[x]["Q9.1"] = this.selectedScore(parseInt(new_answer.q12["r1"]));;
                                            break;
                                        case "Q9.2":
                                            data[x]["Q9.2"] = this.selectedScore(parseInt(new_answer.q12["r2"]));;
                                            break;
                                        case "Q9.3":
                                            data[x]["Q9.3"] = this.selectedScore(parseInt(new_answer.q12["r3"]));;
                                            break;
                                        case "Q9.4":
                                            data[x]["Q9.4"] = this.selectedScore(parseInt(new_answer.q12["r4"]));;
                                            break;
                                        case "Q10":
                                            data[x]["Q10"] = this.question8(new_answer.q13);
                                            break;
                                        case "Q11":
                                            data[x]["Q11"] = new_answer.comment;
                                            break;
                                        case "Q11.1":
                                            data[x]["Q11.1"] = "";
                                            break;
                                        case "Q11.2":
                                            data[x]["Q11.2"] = "";
                                            break;
                                        case "Q11.3":
                                            data[x]["Q11.3"] = "";
                                            break;
                                        case "Q11.4":
                                            data[x]["Q11.4"] = "";
                                            break;
                                        case "Q11.5":
                                            data[x]["Q11.5"] = "";
                                            break;
                                        case "CourseFacility":
                                            data[x]["CourseFacility"] = (data[x].ATCFacility == '0') ? "ATC Facility" : "Onsite";;
                                            break;
                                        case "ComputerEquipment":
                                            data[x]["ComputerEquipment"] = (data[x].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                                            break;
                                    }
                                }
                            } else {
                                for (let q = 0; q < this.questionStruct.length; q++) {
                                    switch (this.questionStruct[q].QuestionNumber) {
                                        case "QP1":
                                            data[x]["QP1"] = this.CourseTeachingLvl(data[x].Update, data[x].Essentials, data[x].Intermediate, data[x].Advanced, data[x].Customized, data[x].OtherCTL, data[x].CommentCTL);
                                            break;
                                        case "QP2":
                                            data[x]["QP2"] = data[x].primaryProduct;
                                            break;
                                        case "QP3":
                                            data[x]["QP3"] = data[x].HoursTraining;
                                            break;
                                        case "QP4":
                                            data[x]["QP4"] = (data[x].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                                            break;
                                        case "QP5":
                                            data[x]["QP5"] = this.CourseTeachingMaterial(data[x].Autodesk, data[x].AAP, data[x].ATC, data[x].Independent, data[x].IndependentOnline, data[x].ATCOnline, data[x].OtherCTM, data[x].CommentCTM);
                                            break;
                                        case "Q1.1":
                                            data[x]["Q1.1"] = new_answer.question1;
                                            break;
                                        case "Q1.2":
                                            data[x]["Q1.2"] = new_answer.question2;
                                            break;
                                        case "Q1.3":
                                            data[x]["Q1.3"] = new_answer.question3;
                                            break;
                                        case "Q1.4":
                                            data[x]["Q1.4"] = new_answer.question4;
                                            break;
                                        case "Q2":
                                            data[x]["Q2"] = "";
                                            break;
                                        case "Q2.1":
                                            data[x]["Q2.1"] = this.selectedScore(parseInt(new_answer.question5["OE"]));
                                            break;
                                        case "Q2.2":
                                            data[x]["Q2.2"] = this.selectedScore(parseInt(new_answer.question5["FR_1"]));
                                            break;
                                        case "Q2.3":
                                            data[x]["Q2.3"] = this.selectedScore(parseInt(new_answer.question5["FR_2"]));
                                            break;
                                        case "Q2.4":
                                            data[x]["Q2.4"] = "";
                                            break;
                                        case "Q2.5":
                                            data[x]["Q2.5"] = this.selectedScore(parseInt(new_answer.question5["IQ"]));
                                            break;
                                        case "Q2.6":
                                            data[x]["Q2.6"] = this.selectedScore(parseInt(new_answer.question5["CCM"]));
                                            break;
                                        case "Q3":
                                            data[x]["Q3"] = "";
                                            break;
                                        case "Q3.1":
                                            data[x]["Q3.1"] = this.selectedScore(parseInt(new_answer.question6["CCM"]));
                                            break;
                                        case "Q3.2":
                                            data[x]["Q3.2"] = this.selectedScore(parseInt(new_answer.question6["IQ"]));
                                            break;
                                        case "Q3.3":
                                            data[x]["Q3.3"] = this.selectedScore(parseInt(new_answer.question6["FR"]));
                                            break;
                                        case "Q4":
                                            data[x]["Q4"] = this.selectedScore(parseInt(new_answer.question7["CCM"]));
                                            break;
                                        case "Q5":
                                            var arr_temp = new_answer.question8.filter(v => v != '');
                                            data[x]["Q5"] = arr_temp.join(" / ");
                                            break;
                                        case "Q6":
                                            data[x]["Q6"] = new_answer.question9.replace(/_/g, " ");
                                            break;
                                        case "Q7":
                                            data[x]["Q7"] = new_answer.question10.replace(/_/g, " ");
                                            break;
                                        case "Q8":
                                            data[x]["Q8"] = "";
                                            break;
                                        case "Q8.1":
                                            data[x]["Q8.1"] = "";
                                            break;
                                        case "Q8.2":
                                            data[x]["Q8.2"] = "";
                                            break;
                                        case "Q8.3":
                                            data[x]["Q8.3"] = "";
                                            break;
                                        case "Q8.4":
                                            data[x]["Q8.4"] = "";
                                            break;
                                        case "Q9":
                                            data[x]["Q9"] = "";
                                            break;
                                        case "Q9.1":
                                            data[x]["Q9.1"] = this.selectedScore(parseInt(new_answer.question11["r1"]));;
                                            break;
                                        case "Q9.2":
                                            data[x]["Q9.2"] = this.selectedScore(parseInt(new_answer.question11["r2"]));;
                                            break;
                                        case "Q9.3":
                                            data[x]["Q9.3"] = this.selectedScore(parseInt(new_answer.question11["r3"]));;
                                            break;
                                        case "Q9.4":
                                            data[x]["Q9.4"] = this.selectedScore(parseInt(new_answer.question11["r4"]));;
                                            break;
                                        case "Q10":
                                            data[x]["Q10"] = this.question8(new_answer.question12);
                                            break;
                                        case "Q11":
                                            data[x]["Q11"] = new_answer.comment;
                                            break;
                                        case "Q11.1":
                                            data[x]["Q11.1"] = "";
                                            break;
                                        case "Q11.2":
                                            data[x]["Q11.2"] = "";
                                            break;
                                        case "Q11.3":
                                            data[x]["Q11.3"] = "";
                                            break;
                                        case "Q11.4":
                                            data[x]["Q11.4"] = "";
                                            break;
                                        case "Q11.5":
                                            data[x]["Q11.5"] = "";
                                            break;
                                        case "CourseFacility":
                                            data[x]["CourseFacility"] = (data[x].ATCFacility == '0') ? "ATC Facility" : "Onsite";;
                                            break;
                                        case "ComputerEquipment":
                                            data[x]["ComputerEquipment"] = (data[x].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                                            break;
                                    }
                                }
                            }

                        }
                        delete data[x].EvaluationAnswerJson;
                        delete data[x].ATCFacility;
                        delete data[x].ATCComputer;
                        delete data[x].HoursTraining;
                        delete data[x].Update;
                        delete data[x].Essentials;
                        delete data[x].Intermediate;
                        delete data[x].Advanced;
                        delete data[x].Customized;
                        delete data[x].OtherCTL;
                        delete data[x].CommentCTL;
                        delete data[x].InstructorLed;
                        delete data[x].Online;
                        delete data[x].Autodesk;
                        delete data[x].AAP;
                        delete data[x].ATC;
                        delete data[x].Independent;
                        delete data[x].IndependentOnline;
                        delete data[x].ATCOnline;
                        delete data[x].OtherCTM;
                        delete data[x].CommentCTM;
                        delete data[x].primaryProduct;
                        // console.log(x + " : " + data[x]);
                    }
                    // console.log(data);
                    */
                    for (let key in data[0]) {
                        this.listkolom.push(key);
                    }

                    //this.report = data;

                //} else {
                
                this.report = data;
                this.ptType = pType;
               }
            else{
                    this.report = [];
                    this.listkolom = [];
                    this.loading_1 = false;
                }
                /*this.dataFound = true;
                this.loading_1 = false;
            }, error => {
                this.report = [];
                this.listkolom = [];
                this.dataFound = true;
                this.loading_1 = false;*/
                this.dataFound = true;
                     this.loading_1 = false;
                }, error => {
                    this.report = [];
                    this.listkolom = [];
                    this.dataFound = true;
                    this.loading_1 = false;

            });

        /* end line autodesk plan 18 oct */

        return event;
    }
     
    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private parserFormatter: NgbDateParserFormatter, private session: SessionService) {

        // this.dateRange = [
        //     { id: "radCourseEndDate", name: "Course End Dates" },
        //     { id: "radSubmittedDate", name: "Evaluation Submitted Dates" }
        // ]
        this.selectedValueDateRange = "radCourseEndDate";
        
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
        this.questionStruct = [
            {
                QuestionNumber: "QP1",
                Question: "What level are you going to teach?"
            },
            {
                QuestionNumber: "QP2",
                Question: "What is going to be the primary software used?"
            },
            {
                QuestionNumber: "QP3",
                Question: "How many hours of training will be delivered?"
            },
            {
                QuestionNumber: "QP4",
                Question: "What will be the training format (Check all that apply)?"
            },
            {
                QuestionNumber: "QP5",
                Question: "What training materials will you use in this class (Check all that apply)?"
            },
            {
                QuestionNumber: "Q1",
                Question: ""
            },
            {
                QuestionNumber: "Q1.1",
                Question: "I would like to receive my electronic Certificate of Completion at the e-mail address stated below. (Your full name is needed below)"
            },
            {
                QuestionNumber: "Q1.2",
                Question: "I would like to receive email from Autodesk, including information about new products and special promotions."
            },
            {
                QuestionNumber: "Q1.3",
                Question: "I agree the information provided in this form may be forwarded by Autodesk to the Authorized Education Partners and may be used by the Authorized Education Partners for internal quality monitoring purposes."
            },
            {
                QuestionNumber: "Q1.4",
                Question: "I would like to receive email from Autodesk, including information about new products and special promotions."
            },
            {
                QuestionNumber: "Q2",
                Question: ""
            },
            {
                QuestionNumber: "Q2.1",
                Question: "Overall experience"
            },
            {
                QuestionNumber: "Q2.2",
                Question: "Computer equipment"
            },
            {
                QuestionNumber: "Q2.3",
                Question: "Training Facility"
            },
            {
                QuestionNumber: "Q2.4",
                Question: "Online Training Facility"
            },
            {
                QuestionNumber: "Q2.5",
                Question: "Instructor"
            },
            {
                QuestionNumber: "Q2.6",
                Question: "CourseWare"
            },
            {
                QuestionNumber: "Q3",
                Question: "How likely or unlikely are you to recommend the following to a friend or colleague?"
            },
            {
                QuestionNumber: "Q3.1",
                Question: "This course"
            },
            {
                QuestionNumber: "Q3.2",
                Question: "This instructor"
            },
            {
                QuestionNumber: "Q3.3",
                Question: "The training facility"
            },
            {
                QuestionNumber: "Q4",
                Question: "How likely or unlikely are you to continue learning Autodesk software?"
            },
            {
                QuestionNumber: "Q5",
                Question: "What do you intend to use this training for?"
            },
            {
                QuestionNumber: "Q6",
                Question: "Which of the following best describes you?"
            },
            {
                QuestionNumber: "Q7",
                Question: "How much experience do you have with this Autodesk product?"
            },
            {
                QuestionNumber: "Q8",
                Question: "Do you plan to obtain an Autodesk Certification for the software learned in this course?"
            },
            {
                QuestionNumber: "Q8.1",
                Question: ""
            },
            {
                QuestionNumber: "Q8.2",
                Question: ""
            },
            {
                QuestionNumber: "Q8.3",
                Question: ""
            },
            {
                QuestionNumber: "Q8.4",
                Question: ""
            },
            {
                QuestionNumber: "Q9",
                Question: "To what extent do you agree or disagree with the following statements?"
            },
            {
                QuestionNumber: "Q9.1",
                Question: "I learned new knowledge and skills"
            },
            {
                QuestionNumber: "Q9.2",
                Question: "I will be able to apply the new skills I learned"
            },
            {
                QuestionNumber: "Q9.3",
                Question: "The new skills I learned will improve my performance"
            },
            {
                QuestionNumber: "Q9.4",
                Question: "I'm more likely to recommend Autodesk products as a result of this course"
            },
            {
                QuestionNumber: "Q10",
                Question: "Which of the following categories best describes your organization’s primary industry?"
            },
            {
                QuestionNumber: "Q11",
                Question: "Comments"
            },
            {
                QuestionNumber: "Q11.1",
                Question: ""
            },
            {
                QuestionNumber: "Q11.2",
                Question: ""
            },
            {
                QuestionNumber: "Q11.3",
                Question: ""
            },
            {
                QuestionNumber: "Q11.4",
                Question: ""
            },
            {
                QuestionNumber: "Q11.5",
                Question: ""
            },
            {
                QuestionNumber: "CourseFacility",
                Question: ""
            },
            {
                QuestionNumber: "ComputerEquipment",
                Question: ""
            },
        ];
        // console.log(this.questionStruct);
        // this.modelPopup1 = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        // this.modelPopup2 = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    }

    // /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }
    // /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya: Boolean = true;
    checkrole() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya: Boolean = false;
    itsinstructor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        };
    }

    orgya: Boolean = false;
    itsOrganization() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
                return true;
            }
        }
    }

    siteya: Boolean = false;
    itsSite() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
                return true;
            }
        }
    }

    distributorya: Boolean = false;
    itsDistributor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
                return true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        // get Geo
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Geo";
        // } else {
        //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        // }
        /* populate data issue role distributor */

        var url = "api/Geo/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } 

        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */
        //         else {
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }
        //         /* ternyata geo/country nya tetep populate based active sitecontactlinks untuk org admin ge */

        //         // else if (this.itsSite()) {
        //         //     url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         // } 
        //         // else {
        //         //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         // }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
              //  url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                    //url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                       //url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                        //url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })



                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    //Fix isu no.263
    // DistributorCheck(): boolean {
    //     var user_lvl = this.useraccesdata.UserLevelId.split(",");
    //     for (let n = 0; n < user_lvl.length; n++) {
    //         if (user_lvl[n] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    GetCurrentOrg() {
        let org = this.useraccesdata.OrgId.split(",");
        let current_org = [];
        for (let i = 0; i < org.length; i++) {
            current_org.push('"' + org[i] + '"');
        }
        return current_org;
    }

    getCountry() {
        this.loading = true;
        // Countries
        /* populate data issue role distributor */
       // var this.url2 = "api/Countries";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             this.url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; // tambah kalo site / org country nya based org
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
                // this.url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistributorCountry = this.urlGetOrgId();
                 this.url2 = "api/Territory/wherenew/'DistributorCountry'";
            } else {
                if (this.orgya) {
                    OnlyCountryOrgId = this.urlGetOrgId();
                    this.url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        this.url2 = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        this.url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorCountry: DistributorCountry,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(this.url2, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
                this.allcountries = this.dropdownListCountry;
                this.loading = false;

                /* issue doc report 11102018 - default dropdown country for role site and org admin */

                if(this.adminya){
                    if(!this.distributorya){
                        this.selectedItemsCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })


                        this.populateOrgByCountry(this.selectedItemsCountry);
                    }
                }

                /* end line issue doc report 11102018 - default dropdown country for role site and org admin */

            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        this.selectedItemsCountry = [];
    }

    territorydefault: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } 

        //         /* ternyata territory nya tetep populate based active sitecontactlinks untuk org admin ge */
        //         else {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         }
        //         /* ternyata territory nya tetep populate based active sitecontactlinks untuk org admin ge */

        //         // else if (this.itsSite()) {
        //         //     url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         // } 
        //         // else {
        //         //     url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         // }
        //     }
        // }
        /* populate data issue role distributor */

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'Distributor'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteId'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteId";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            Distributor: DistributorGeo,
            SiteId: siteRes,
            OrgId: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // // this.loading = true;
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Territory";
        // } else {
        //     url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        // }

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.adminya) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    // orgtmp: any;
    // getOrgId(url) {
    //     var data: any;
    //     this.service.get(url, data)
    //         .subscribe(res => {
    //             data = JSON.parse(res);
    //             if (data.length > 0) {
    //                 this.dropdownListOrganizationId = data.slice(0, 100).map((item) => {
    //                     return {
    //                         id: item.OrgId,
    //                         itemName: item.OrgId
    //                     }
    //                 });
    //                 this.orgtmp = data.map((item) => {
    //                     return {
    //                         id: item.OrgId,
    //                         itemName: item.OrgId
    //                     }
    //                 });
    //             }
    //         }, error => {
    //             this.service.errorserver();
    //         });
    //     // this.selectedItemsOrganizationId = [];
    // }

    // sitetmp: any;
    // getSiteId(url) {
    //     var data: any;
    //     this.service.httpClientGet(url, data)
    //         .subscribe(res => {
    //             data = res;
    //             if (data.length > 0) {
    //                 // this.dropdownListSiteId = data.slice(0, 100).map((item) => {
    //                 //     return {
    //                 //         id: item.SiteId,
    //                 //         itemName: item.SiteId
    //                 //     }
    //                 // });
    //                 this.sitetmp = data.map((item) => {
    //                     return {
    //                         id: item.SiteId,
    //                         itemName: item.SiteId
    //                     }
    //                 });
    //             }
    //         }, error => {
    //             this.service.errorserver();
    //         });
    //     // this.selectedItemsSiteId = [];
    // }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListMarketType = data.map((item) => {
                        return {
                            id: item.MarketTypeId,
                            itemName: item.MarketType
                        }
                    });

                    // for (let i = 0; i < data.length; i++) {
                    //     if (data[i].MarketTypeId != 1) {
                    //         var market = {
                    //             'id': data[i].MarketTypeId,
                    //             'itemName': data[i].MarketType
                    //         };
                    //         this.dropdownListMarketType.push(market);
                    //     }
                    // }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsMarketType = [];
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
                this.selectedYear = this.year[this.year.length - 1].Key;
            }, error => {
                this.service.errorserver();
            });
    }

    certificateDropdown() {
        this.dropdownCertificate = [];

        /* issue 17102018 - automatic remove when change selected partner type */

        this.dropdownEventType = [];
        this.selectedEventType = [];
        
        /* end line issue 17102018 - automatic remove when change selected partner type */

        if (this.selectedPartner.length > 0) {
            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id == 58) {
                    let certificate: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", certificate)
                        .subscribe(res => {
                            certificate = res;
                            for (let i = 0; i < certificate.length; i++) {
                                if (certificate[i].Key == 0) {
                                    certificate.splice(i, 1);
                                }
                            }
                            this.dropdownCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                            this.selectedCertificate = certificate.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            });
                        });
                }
            }
        } else {
            this.dropdownCertificate = [];
        }
        // this.selectedCertificate = [];
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles/ReportEva", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownPartner = null;
                }
                else {
                   this.dropdownPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                    /*this.selectedPartner = JSON.parse(result).map((item) => {
                        return {
                            id: item.RoleId,
                            itemName: item.RoleName
                        }
                    })*/

                    this.certificateDropdown()
                }
            },
                error => {
                    this.dropdownPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.selectedPartner =
                        [
                            {
                                id: '1',
                                itemName: 'Authorized Training Center (ATC)'
                            },
                            {
                                id: '58',
                                itemName: 'Authorized Academic Partner (AAP)'
                            }
                        ]

                    this.certificateDropdown()
                });
    }
    excelexport(value) {
        let d : any;
        d = value;
        this.expexcel = [];
        this.expexcel = JSON.parse(d.data);
        var fileName = "DownloadSiteEvaluation.xls";
        var wb = XLSX.utils.book_new();
        var ws = XLSX.utils.json_to_sheet(this.expexcel);
        // var wb = XLSX.read([], { type: "buffer" }); /* handling biger data excel download */
        XLSX.utils.book_append_sheet(wb, ws, fileName);
        // XLSX.writeFile(wb, fileName);
        XLSX.writeFile(wb, fileName, { bookType: 'xlsx', type: 'buffer' }); /* fixed issue 20092018 - Download site evaluation report, extract into excel, 2 sheets created in the same spread sheet */
        this.loading = false;

       
    }
 

    ExportExceltoXls()
    {
        this.fileName = "DownloadSiteEvaluation.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "DownloadSiteEvaluation.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "DownloadSiteEvaluation.csv";
        this.ExportExcel();
     }


    ExportExcel() {

        let partnerdata;
        for (let i = 0; i < this.selectedPartner.length; i++) {
           partnerdata =this.selectedPartner[i].id.toString();
        }


        this.loading = true;
        //Hapus limit dan offset
        delete this.keyword.Limit;
        delete this.keyword.Offset;
		    this.expexcel = [];
        this.excel = [];
        var data: any;
      
        /* autodesk plan 18 oct */
        //if(this.totalEval <10000){
            let thefile = {};
            this.service.httpClientPostWithOptions("api/EvaluationAnswer/DownloadExcelEval/" + partnerdata, this.keyword, { responseType: 'arraybuffer'})
            // .subscribe(res => {
            //     data=null;
            //     data = res;
                
            //     //   console.log(data);
            //         if (data.success==true) {
            //         //   this.expexcel = JSON.parse(data.data);
            //         this.expexcel =data.data;
            //         this.excelexport(res);
            //         //   var fileName = "DownloadSiteEvaluation.xls";
            //         //   var ws = XLSX.utils.json_to_sheet(this.expexcel);
            //         //   var wb = XLSX.utils.book_new();
            //         //   // var wb = XLSX.read([], { type: "buffer" }); /* handling biger data excel download */
            //         //   XLSX.utils.book_append_sheet(wb, ws, fileName);
            //         //   // XLSX.writeFile(wb, fileName);
            //         //   XLSX.writeFile(wb, fileName, { bookType: 'xlsx', type: 'buffer' }); /* fixed issue 20092018 - Download site evaluation report, extract into excel, 2 sheets created in the same spread sheet */
            //         //   this.loading = false;
            //           }
            //           else {
            //             this.excel = [];
            //             this.loading = false;
            //              swal(
            //                     'Process Failure',
            //                     data.data,
            //                     'error'
            // )
                
            //           }
            .subscribe(result => {
                var blob = new Blob([result], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"});
                this.loading = false;
                var name =this.fileName;
                saveAs(blob,name); 
            },
            error => {
                this.service.errorserver();
                this.loading = false;
            });
        // }
        // else{
        //     swal(
        //         'Warnng!',
        //         'It may take long time because total evaluation > 10000,<br/> please select only one month!',
        //         'warning'
        //     )
        //         this.loading = false;
        // }
        //this.service.post("api/EvaluationAnswer/DownloadExcelEval", this.keyword)
       


          /*    this.loading = true;
          //Hapus limit dan offset
          delete this.keyword.Limit;
          delete this.keyword.Offset;
          this.excel = [];
          this.expexcel = [];
          var data: any;
          this.http.post("api/EvaluationAnswer/DownloadExcelEval", this.keyword, { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) })
              .subscribe(res => {
                data = JSON.parse(res["_body"]);
                this.expexcel = data;
                  if (data.length > 0) {
                      for (let x = 0; x < data.length; x++) {
                          let answer = data[x].EvaluationAnswerJson;*/

                          // if (typeof answer === 'object' && answer.constructor === Object) {
                          // if (typeof answer === 'object') { 

                          //if (typeof answer === 'object' || answer == "") { /* issue 17102018 - cant extract excel -> escape char */
                          //    data[x]["QP1"] = this.CourseTeachingLvl(data[x].Update, data[x].Essentials, data[x].Intermediate, data[x].Advanced, data[x].Customized, data[x].OtherCTL, data[x].CommentCTL);
                          //    data[x]["QP2"] = data[x].primaryProduct;
                          //    data[x]["QP3"] = data[x].HoursTraining;
                          //    data[x]["QP4"] = (data[x].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                          //    data[x]["QP5"] = this.CourseTeachingMaterial(data[x].Autodesk, data[x].AAP, data[x].ATC, data[x].Independent, data[x].IndependentOnline, data[x].ATCOnline, data[x].OtherCTM, data[x].CommentCTM);
                          //    data[x]["Q1.1"] = "";
                          //    data[x]["Q1.2"] = "";
                          //    data[x]["Q1.3"] = "";
                          //    data[x]["Q1.4"] = "";
                          //    data[x]["Q2"] = "";
                          //    data[x]["Q2.1"] = "";
                          //    data[x]["Q2.2"] = "";
                          //    data[x]["Q2.3"] = "";
                          //    data[x]["Q2.4"] = "";
                          //    data[x]["Q2.5"] = "";
                          //    data[x]["Q2.6"] = "";
                          //    data[x]["Q3"] = "";
                          //    data[x]["Q3.1"] = "";
                          //    data[x]["Q3.2"] = "";
                          //    data[x]["Q3.3"] = "";
                          //    data[x]["Q4"] = "";
                          //    data[x]["Q5"] = "";
                          //    data[x]["Q6"] = "";
                          //    data[x]["Q7"] = "";
                          //    data[x]["Q8"] = "";
                          //    data[x]["Q8.1"] = "";
                          //    data[x]["Q8.2"] = "";
                          //    data[x]["Q8.3"] = "";
                          //    data[x]["Q8.4"] = "";
                          //    data[x]["Q9"] = "";
                          //    data[x]["Q9.1"] = "";
                          //    data[x]["Q9.2"] = "";
                          //    data[x]["Q9.3"] = "";
                          //    data[x]["Q9.4"] = "";
                          //    data[x]["Q10"] = "";
                          //    data[x]["Q11"] = "";
                          //    data[x]["Q11.1"] = "";
                          //    data[x]["Q11.2"] = "";
                          //    data[x]["Q11.3"] = "";
                          //    data[x]["Q11.4"] = "";
                          //    data[x]["Q11.5"] = "";
                          //    data[x]["CourseFacility"] = (data[x].ATCFacility == '0') ? "ATC Facility" : "Onsite";
                          //    data[x]["ComputerEquipment"] = (data[x].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                          //} else {
                          //    if (answer.indexOf('\\r\\n') > -1) {
                          //        answer = answer.replace(/\\r\\n/g, "");
                          //        answer = answer.replace(/  /g, "");
                          //        answer = answer.replace(/    /g, "");
                          //    }

                          //    answer = answer.replace(/\\/g, "");
                          //    //cara untuk ngambil bagian komentarnya aja
                          //    let index_1;
                          //    let prev_comm;
                          //    if (answer.indexOf('"comment": "') > -1) {
                          //        index_1 = answer.indexOf('"comment": "'); //untuk nyari posisi karakter komentar
                          //        prev_comm = answer.substr(index_1 + 12, answer.length); //untuk ngambil isi komentar nya saja
                          //    }

                          //    if (answer.indexOf('"comment":"') > -1) {
                          //        index_1 = answer.indexOf('"comment":"'); //untuk nyari posisi karakter komentar
                          //        prev_comm = answer.substr(index_1 + 11, answer.length); //untuk ngambil isi komentar nya saja
                          //    }

                          //    prev_comm = prev_comm.replace(/"}/g, ''); //hilangkan karakter akhirnya
                          //    let new_comm = prev_comm.replace(/"/g, '\\"'); //escape char untuk isi komentarnya
                          //    let new_answer = answer.replace(prev_comm, new_comm); //replace yg lama dengan yg baru

                          //    /* issue 17102018 - cant extract excel -> escape char */

                          //    new_answer = new_answer.toString().replace(/\r/g,"");
                          //    new_answer = new_answer.toString().replace(/\n/g,"");

                          //    /* end line issue 17102018 - cant extract excel -> escape char */

                          //    new_answer = JSON.parse(new_answer);
                          //    // console.log(x + " : " + answer);

                          //    var koplak = Object.keys(new_answer);

                          //    if (koplak[0] != "question1") {
                          //        for (let q = 0; q < this.questionStruct.length; q++) {
                          //            switch (this.questionStruct[q].QuestionNumber) {
                          //                case "QP1":
                          //                    data[x]["QP1"] = this.CourseTeachingLvl(data[x].Update, data[x].Essentials, data[x].Intermediate, data[x].Advanced, data[x].Customized, data[x].OtherCTL, data[x].CommentCTL);
                          //                    break;
                          //                case "QP2":
                          //                    data[x]["QP2"] = data[x].primaryProduct;
                          //                    break;
                          //                case "QP3":
                          //                    data[x]["QP3"] = data[x].HoursTraining;
                          //                    break;
                          //                case "QP4":
                          //                    data[x]["QP4"] = (data[x].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                          //                    break;
                          //                case "QP5":
                          //                    data[x]["QP5"] = this.CourseTeachingMaterial(data[x].Autodesk, data[x].AAP, data[x].ATC, data[x].Independent, data[x].IndependentOnline, data[x].ATCOnline, data[x].OtherCTM, data[x].CommentCTM);
                          //                    break;
                          //                case "Q1.1":
                          //                    data[x]["Q1.1"] = new_answer.q1;
                          //                    break;
                          //                case "Q1.2":
                          //                    data[x]["Q1.2"] = new_answer.q2;
                          //                    break;
                          //                case "Q1.3":
                          //                    data[x]["Q1.3"] = new_answer.q3;
                          //                    break;
                          //                case "Q1.4":
                          //                    data[x]["Q1.4"] = new_answer.q4;
                          //                    break;
                          //                case "Q2":
                          //                    data[x]["Q2"] = "";
                          //                    break;
                          //                case "Q2.1":
                          //                    data[x]["Q2.1"] = this.selectedScore(parseInt(new_answer.q5["OE"]));
                          //                    break;
                          //                case "Q2.2":
                          //                    data[x]["Q2.2"] = this.selectedScore(parseInt(new_answer.q5["FR_1"]));
                          //                    break;
                          //                case "Q2.3":
                          //                    data[x]["Q2.3"] = this.selectedScore(parseInt(new_answer.q5["FR_2"]));
                          //                    break;
                          //                case "Q2.4":
                          //                    data[x]["Q2.4"] = "";
                          //                    break;
                          //                case "Q2.5":
                          //                    data[x]["Q2.5"] = this.selectedScore(parseInt(new_answer.q5["IQ"]));
                          //                    break;
                          //                case "Q2.6":
                          //                    data[x]["Q2.6"] = this.selectedScore(parseInt(new_answer.q5["CCM"]));
                          //                    break;
                          //                case "Q3":
                          //                    data[x]["Q3"] = "";
                          //                    break;
                          //                case "Q3.1":
                          //                    data[x]["Q3.1"] = this.selectedScore(parseInt(new_answer.q6["CCM"]));
                          //                    break;
                          //                case "Q3.2":
                          //                    data[x]["Q3.2"] = this.selectedScore(parseInt(new_answer.q6["IQ"]));
                          //                    break;
                          //                case "Q3.3":
                          //                    data[x]["Q3.3"] = this.selectedScore(parseInt(new_answer.q6["FR"]));
                          //                    break;
                          //                case "Q4":
                          //                    data[x]["Q4"] = this.selectedScore(parseInt(new_answer.q7["CCM"]));
                          //                    break;
                          //                case "Q5":
                          //                    var arr_temp = new_answer.q8.filter(v => v != '');
                          //                    data[x]["Q5"] = arr_temp.join(" / ");
                          //                    break;
                          //                case "Q6":
                          //                    data[x]["Q6"] = new_answer.q9.replace(/_/g, " ");
                          //                    break;
                          //                case "Q7":
                          //                    data[x]["Q7"] = new_answer.q10.replace(/_/g, " ");
                          //                    break;
                          //                case "Q8":
                          //                    data[x]["Q8"] = "";
                          //                    break;
                          //                case "Q8.1":
                          //                    data[x]["Q8.1"] = "";
                          //                    break;
                          //                case "Q8.2":
                          //                    data[x]["Q8.2"] = "";
                          //                    break;
                          //                case "Q8.3":
                          //                    data[x]["Q8.3"] = "";
                          //                    break;
                          //                case "Q8.4":
                          //                    data[x]["Q8.4"] = "";
                          //                    break;
                          //                case "Q9":
                          //                    data[x]["Q9"] = "";
                          //                    break;
                          //                case "Q9.1":
                          //                    data[x]["Q9.1"] = this.selectedScore(parseInt(new_answer.q12["r1"]));;
                          //                    break;
                          //                case "Q9.2":
                          //                    data[x]["Q9.2"] = this.selectedScore(parseInt(new_answer.q12["r2"]));;
                          //                    break;
                          //                case "Q9.3":
                          //                    data[x]["Q9.3"] = this.selectedScore(parseInt(new_answer.q12["r3"]));;
                          //                    break;
                          //                case "Q9.4":
                          //                    data[x]["Q9.4"] = this.selectedScore(parseInt(new_answer.q12["r4"]));;
                          //                    break;
                          //                case "Q10":
                          //                    data[x]["Q10"] = this.question8(new_answer.q13);
                          //                    break;
                          //                case "Q11":
                          //                    data[x]["Q11"] = new_answer.comment;
                          //                    break;
                          //                case "Q11.1":
                          //                    data[x]["Q11.1"] = "";
                          //                    break;
                          //                case "Q11.2":
                          //                    data[x]["Q11.2"] = "";
                          //                    break;
                          //                case "Q11.3":
                          //                    data[x]["Q11.3"] = "";
                          //                    break;
                          //                case "Q11.4":
                          //                    data[x]["Q11.4"] = "";
                          //                    break;
                          //                case "Q11.5":
                          //                    data[x]["Q11.5"] = "";
                          //                    break;
                          //                case "CourseFacility":
                          //                    data[x]["CourseFacility"] = (data[x].ATCFacility == '0') ? "ATC Facility" : "Onsite";;
                          //                    break;
                          //                case "ComputerEquipment":
                          //                    data[x]["ComputerEquipment"] = (data[x].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                          //                    break;
                          //            }
                          //        }
                          //    } else {
                          //        for (let q = 0; q < this.questionStruct.length; q++) {
                          //            switch (this.questionStruct[q].QuestionNumber) {
                          //                case "QP1":
                          //                    data[x]["QP1"] = this.CourseTeachingLvl(data[x].Update, data[x].Essentials, data[x].Intermediate, data[x].Advanced, data[x].Customized, data[x].OtherCTL, data[x].CommentCTL);
                          //                    break;
                          //                case "QP2":
                          //                    data[x]["QP2"] = data[x].primaryProduct;
                          //                    break;
                          //                case "QP3":
                          //                    data[x]["QP3"] = data[x].HoursTraining;
                          //                    break;
                          //                case "QP4":
                          //                    data[x]["QP4"] = (data[x].InstructorLed == 1) ? "Instructor-led in the classroom" : "Online or e-learning";
                          //                    break;
                          //                case "QP5":
                          //                    data[x]["QP5"] = this.CourseTeachingMaterial(data[x].Autodesk, data[x].AAP, data[x].ATC, data[x].Independent, data[x].IndependentOnline, data[x].ATCOnline, data[x].OtherCTM, data[x].CommentCTM);
                          //                    break;
                          //                case "Q1.1":
                          //                    data[x]["Q1.1"] = new_answer.question1;
                          //                    break;
                          //                case "Q1.2":
                          //                    data[x]["Q1.2"] = new_answer.question2;
                          //                    break;
                          //                case "Q1.3":
                          //                    data[x]["Q1.3"] = new_answer.question3;
                          //                    break;
                          //                case "Q1.4":
                          //                    data[x]["Q1.4"] = new_answer.question4;
                          //                    break;
                          //                case "Q2":
                          //                    data[x]["Q2"] = "";
                          //                    break;
                          //                case "Q2.1":
                          //                    data[x]["Q2.1"] = this.selectedScore(parseInt(new_answer.question5["OE"]));
                          //                    break;
                          //                case "Q2.2":
                          //                    data[x]["Q2.2"] = this.selectedScore(parseInt(new_answer.question5["FR_1"]));
                          //                    break;
                          //                case "Q2.3":
                          //                    data[x]["Q2.3"] = this.selectedScore(parseInt(new_answer.question5["FR_2"]));
                          //                    break;
                          //                case "Q2.4":
                          //                    data[x]["Q2.4"] = "";
                          //                    break;
                          //                case "Q2.5":
                          //                    data[x]["Q2.5"] = this.selectedScore(parseInt(new_answer.question5["IQ"]));
                          //                    break;
                          //                case "Q2.6":
                          //                    data[x]["Q2.6"] = this.selectedScore(parseInt(new_answer.question5["CCM"]));
                          //                    break;
                          //                case "Q3":
                          //                    data[x]["Q3"] = "";
                          //                    break;
                          //                case "Q3.1":
                          //                    data[x]["Q3.1"] = this.selectedScore(parseInt(new_answer.question6["CCM"]));
                          //                    break;
                          //                case "Q3.2":
                          //                    data[x]["Q3.2"] = this.selectedScore(parseInt(new_answer.question6["IQ"]));
                          //                    break;
                          //                case "Q3.3":
                          //                    data[x]["Q3.3"] = this.selectedScore(parseInt(new_answer.question6["FR"]));
                          //                    break;
                          //                case "Q4":
                          //                    data[x]["Q4"] = this.selectedScore(parseInt(new_answer.question7["CCM"]));
                          //                    break;
                          //                case "Q5":
                          //                    var arr_temp = new_answer.question8.filter(v => v != '');
                          //                    data[x]["Q5"] = arr_temp.join(" / ");
                          //                    break;
                          //                case "Q6":
                          //                    data[x]["Q6"] = new_answer.question9.replace(/_/g, " ");
                          //                    break;
                          //                case "Q7":
                          //                    data[x]["Q7"] = new_answer.question10.replace(/_/g, " ");
                          //                    break;
                          //                case "Q8":
                          //                    data[x]["Q8"] = "";
                          //                    break;
                          //                case "Q8.1":
                          //                    data[x]["Q8.1"] = "";
                          //                    break;
                          //                case "Q8.2":
                          //                    data[x]["Q8.2"] = "";
                          //                    break;
                          //                case "Q8.3":
                          //                    data[x]["Q8.3"] = "";
                          //                    break;
                          //                case "Q8.4":
                          //                    data[x]["Q8.4"] = "";
                          //                    break;
                          //                case "Q9":
                          //                    data[x]["Q9"] = "";
                          //                    break;
                          //                case "Q9.1":
                          //                    data[x]["Q9.1"] = this.selectedScore(parseInt(new_answer.question11["r1"]));;
                          //                    break;
                          //                case "Q9.2":
                          //                    data[x]["Q9.2"] = this.selectedScore(parseInt(new_answer.question11["r2"]));;
                          //                    break;
                          //                case "Q9.3":
                          //                    data[x]["Q9.3"] = this.selectedScore(parseInt(new_answer.question11["r3"]));;
                          //                    break;
                          //                case "Q9.4":
                          //                    data[x]["Q9.4"] = this.selectedScore(parseInt(new_answer.question11["r4"]));;
                          //                    break;
                          //                case "Q10":
                          //                    data[x]["Q10"] = this.question8(new_answer.question12);
                          //                    break;
                          //                case "Q11":
                          //                    data[x]["Q11"] = new_answer.comment;
                          //                    break;
                          //                case "Q11.1":
                          //                    data[x]["Q11.1"] = "";
                          //                    break;
                          //                case "Q11.2":
                          //                    data[x]["Q11.2"] = "";
                          //                    break;
                          //                case "Q11.3":
                          //                    data[x]["Q11.3"] = "";
                          //                    break;
                          //                case "Q11.4":
                          //                    data[x]["Q11.4"] = "";
                          //                    break;
                          //                case "Q11.5":
                          //                    data[x]["Q11.5"] = "";
                          //                    break;
                          //                case "CourseFacility":
                          //                    data[x]["CourseFacility"] = (data[x].ATCFacility == '0') ? "ATC Facility" : "Onsite";;
                          //                    break;
                          //                case "ComputerEquipment":
                          //                    data[x]["ComputerEquipment"] = (data[x].ATCComputer == '0') ? "ATC Computer" : "Onsite";
                          //                    break;
                          //            }
                          //        }
                          //    }
                          //}
                          //delete data[x].EvaluationAnswerJson;
                          //delete data[x].ATCFacility;
                          //delete data[x].ATCComputer;
                          //delete data[x].HoursTraining;
                          //delete data[x].Update;
                          //delete data[x].Essentials;
                          //delete data[x].Intermediate;
                          //delete data[x].Advanced;
                          //delete data[x].Customized;
                          //delete data[x].OtherCTL;
                          //delete data[x].CommentCTL;
                          //delete data[x].InstructorLed;
                          //delete data[x].Online;
                          //delete data[x].Autodesk;
                          //delete data[x].AAP;
                          //delete data[x].ATC;
                          //delete data[x].Independent;
                          //delete data[x].IndependentOnline;
                          //delete data[x].ATCOnline;
                          //delete data[x].OtherCTM;
                          //delete data[x].CommentCTM;
                          //delete data[x].primaryProduct;
                 /*     }

                      this.excel = data;
                      var fileName = "DownloadSiteEvaluation.xls";
                      var ws = XLSX.utils.json_to_sheet(this.expexcel);
                      var wb = XLSX.utils.book_new();
                      // var wb = XLSX.read([], { type: "buffer" }); /* handling biger data excel download */
                    /*  XLSX.utils.book_append_sheet(wb, ws, fileName);
                      // XLSX.writeFile(wb, fileName);
                    /*  XLSX.writeFile(wb, fileName, { bookType: 'xlsx', type: 'buffer' }); /* fixed issue 20092018 - Download site evaluation report, extract into excel, 2 sheets created in the same spread sheet */

                /*  }

                  else {
                    this.expexcel = [];
                    this.loading = false;
                  }
                  this.loading = false;
            }, error => {
              this.expexcel = [];
              this.loading = false;
            });

		}*/
		
    }


    notadmin: boolean = false;
    allcountries = [];

    /* default org site */
    orgtmp = [];
    sitetmp = [];
    /* default org site */

    ngOnInit() {
        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()
		this.performanceReport = [];
        /* call function get user level id (issue31082018)*/

        /* set the date to be current date */
        // let currDate = this.service.backDate()
        // let arrDate = currDate.split('-');
        // this.modelPopup1 = {
        //     "year": parseInt(arrDate[0]),
        //     "month": parseInt(arrDate[1]),
        //     "day": parseInt(arrDate[2])
        // };

        // this.modelPopup2 = {
        //     "year": parseInt(arrDate[0]),
        //     "month": parseInt(arrDate[1]),
        //     "day": parseInt(arrDate[2])
        // };

        /* issue 15102018 - Download site admin> start and end date is defaulted to 2018-10-13 where in the system date was 14. Is this from different time zone */

        this.modelPopup1 = {
            year:new Date().getFullYear(),
            month:new Date().getMonth() + 1,
            day:new Date().getDate()
        }

        this.modelPopup2 = {
            year:new Date().getFullYear(),
            month:new Date().getMonth() + 1,
            day:new Date().getDate()
        }

        /* end line issue 15102018 - Download site admin> start and end date is defaulted to 2018-10-13 where in the system date was 14. Is this from different time zone */

        // let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        // var url = "api/MainOrganization/AddContact";
        // var this.url2 = "api/MainSite/getSiteId";
        // for (var i = 0; i < userlvlarr.length; i++) {
        //     if (userlvlarr[i] == "ORGANIZATION" || userlvlarr[i] == "DISTRIBUTOR" || userlvlarr[i] == "SITE") {
        //         var orgArr = this.useraccesdata.OrgId.toString().split(',');
        //         var orgTmp = [];
        //         for (var i = 0; i < orgArr.length; i++) {
        //             orgTmp.push("'" + orgArr[i] + "'");
        //         }

        //         url = 'api/MainOrganization/AddContactOrg/' + this.useraccesdata.UserLevelId + '/' + orgTmp.join(",");
        //         this.url2 = 'api/MainSite/getSiteIdByRole/' + this.useraccesdata.UserLevelId + '/' + orgTmp.join(",");
        //         this.notadmin = true;
        //     }
        // }

        if (!(localStorage.getItem("filter") === null)) {
            // this.getGeo();
            // this.getTerritory();
            // this.getCountry();

            var item = JSON.parse(localStorage.getItem("filter"));
            this.selectedItemsGeo = item.geoSelect;
            this.dropdownListGeo = item.geoDropdown;
            this.selectedItemsTerritories = item.territorySelect;
            this.dropdownListTerritories = item.territoryDropdown;
            this.selectedItemsCountry = item.countrySelect;
            this.dropdownListCountry = item.countryDropdown;
            this.selectedYear = item.yearSelect;
            this.selectedItemsOrganizationId = item.orgSelect;
            this.dropdownListOrganizationId = item.orgDropdown;
            this.selectedItemsSiteId = item.siteSelect;
            this.dropdownListSiteId = item.siteDropdown;
            this.selectedItemsMarketType = item.marketSelect;
            this.dropdownListMarketType = item.marketDropdown;
            this.selectedPartner = item.partnerSelect;
            this.dropdownPartner = item.partnerDropdown;
            this.selectedCertificate = item.certificateSelect;
            this.dropdownCertificate = item.certificateDropdown;
            this.selectedValueDateRange = item.dateRange;
            this.modelPopup1 = item.dateStart;
            this.modelPopup2 = item.dateEnd;
            this.getReport();
        }
        else {
            this.getGeo();
            this.getTerritory();
            this.getCountry();
            this.getPartnerType();
            this.getMarketType();

        }

        this.getFY();
        // this.getOrgId(url);
        // this.getSiteId(this.url2);
        this.getPrimaryAndSecondaryProduct();
        this.selectedYear = "";
        this.selectedPartner = [];
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
        //Added By Soe
        this.dropdownSettingsSingle = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5,
            showCheckbox: false,
            closeOnSelect: true
        }

        this.dropdownSettingsSearch = {
            singleSelection: false,
            text: "All Organizations",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5,
            noDataLabel: "All Organizations"
        };

        this.dropdownSettingsSearch2 = {
            singleSelection: false,
            text: "All Sites",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5,
            noDataLabel: "All Sites"
        };
    }

    onSelectDate(date: NgbDateStruct) {
        this.dataFound = false;
    }

    changeSelected(value) {
        this.dataFound = false;
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    // onGeoSelect(item: any) {
    //     this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // OnGeoDeSelect(item: any) {
    //     this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoSelectAll(items: any) {
    //     this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.selectedItemsRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelect(item: any) {
    //     this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnRegionDeSelect(item: any) {
    //     this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelectAll(items: any) {
    //     this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelectAll(items: any) {
    //     this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    /*
    arraygeoid = [];
    onGeoSelect(item: any) {
        this.arraygeoid.push('"' + item.id + '"');
    
        // Countries
        var data = '';
        this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = JSON.parse(result).map((item) => {
                        return {
                            id: item.countries_code,
                            itemName: item.countries_name
                        }
                    })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    
    OnGeoDeSelect(item: any) {
        var data = '';
    
        //split countries
        let countriesArrTmp = [];
        this.service.get("api/Countries/filter/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = JSON.parse(result);
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });
    
        var index = this.arraygeoid.findIndex(x => x == '"' + item.id + '"');
        this.arraygeoid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);
    
        if (this.arraygeoid.length > 0) {
            // Countries
            var data = '';
            this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = JSON.parse(result).map((item) => {
                            return {
                                id: item.countries_code,
                                itemName: item.countries_name
                            }
                        })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            // this.dropdownListCountry = this.allcountries;
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    
    onGeoSelectAll(items: any) {
        this.arraygeoid = [];
    
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
    
        // Countries
        var data = '';
        this.service.get("api/Countries/filter/" + this.arraygeoid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = JSON.parse(result).map((item) => {
                        return {
                            id: item.countries_code,
                            itemName: item.countries_name
                        }
                    })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    
    onGeoDeSelectAll(items: any) {
        // this.dropdownListCountry = this.allcountries;
        this.selectedItemsCountry = [];
    }
    */

    /* pupulate geo by role */

    arraygeoid = [];
    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url ="api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url =  url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url ="api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url =  url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url ="api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //            if (this.siteya) {
        //             url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }

        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                    // console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    /* end pupulate geo by role */

    arrayterritory = [];
    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';

        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';

            /* populate data issue role distributor */
            // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
            var url = "api/Countries/filterByGeoByTerritory";

            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }
            // else {
            //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
            //         url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //     }
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //     } else {
            //         if (this.orgya) {
            //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //         } else {
            //             if (this.siteya) {
            //             this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //         } else {
            //             this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //         }
            //     }
            // }
            var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistTerr = this.urlGetOrgId();
                   url = "api/Countries/DistTerr";
                } else {
                    if (this.orgya) {
                        OrgSiteTerr = this.urlGetOrgId();
                        url = "api/Countries/OrgSiteTerr";
                    } else {
                        if (this.siteya) {
                            OnlyCountryOrgId = this.urlGetOrgId();
                            url = "api/Territory/wherenew/'OnlyCountryOrgId";
                        } else {
                            CountryOrgId= this.urlGetOrgId();
                            url = "api/Territory/wherenew/'CountryOrgId'";
                        }
                    }
                }
                
            }
            var keyword : any = {
                CtmpGeo:tmpGeo,
                CDistTerr:DistTerr,
                COrgSiteTerr:OrgSiteTerr,
                OnlyCountryOrgId: OnlyCountryOrgId,
                CountryOrgId: CountryOrgId,
                CtmpTerritory:tmpTerritory
               
            };
            keyword = JSON.stringify(keyword);
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        // Countries
        var data = '';

        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory"

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }
        // else {
        //     if (this.itsOrganization() || this.itsSite()) { // tambah kalo site / org country nya based org
        //         url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 this.url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 this.url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    populateOrgByCountry(dataCountry) {
        this.dropdownListOrganizationId = [];
        this.orgtmp = [];

        if (dataCountry.length > 0) {
            var list = [];
            for (var i = 0; i < dataCountry.length; i++) {
                list.push('"' + dataCountry[i].id + '"');
            }

            /* poho akses role na, jangar duh */

            var sites = "";
            var orgs = "";
            var distributor = "";

            if (this.adminya) {
                if (this.distributorya) {
                    distributor = this.urlGetOrgId();
                } else {
                    if (this.orgya) {
                        orgs = this.urlGetOrgId();
                    } else {
                        if (this.siteya) {
                            sites = this.urlGetSiteId();
                        }
                    }
                }
            }

            /* poho akses role na, jangar duh */

            var data :any =
            {
                Country: list.toString(),
                SiteId: sites,
                OrgId: orgs,
                Distributor: distributor
            };
            data = JSON.stringify(data);
            this.service.httpClientPost("api/MainOrganization/populateByCountry", data)
                .subscribe(res => {
                    let dataTmp : any = res;
                    if (dataTmp.length > 0) {
                        this.dropdownListOrganizationId = dataTmp.map((item) => {
                            return {
                                id: item.OrgId,
                                itemName: item.OrgId
                            }
                        });
                        this.orgtmp = dataTmp.map((item) => {
                            return {
                                id: item.OrgId,
                                itemName: item.OrgId
                            }
                        });

                        /* issue doc report 11102018 - default dropdown org for role site and org admin */

                        if(this.adminya){
                            if(!this.distributorya){
                                this.selectedItemsOrganizationId = dataTmp.map((item) => {
                                    return {
                                        id: item.OrgId,
                                        itemName: item.OrgId
                                    }
                                });

                                if (this.selectedItemsOrganizationId.length > 0) {
                                    for (var i = 0; i < this.selectedItemsOrganizationId.length; i++) {
                                        this.filterSiteIdNew(this.selectedItemsOrganizationId[i].id);
                                    }
                                }
                            }
                        }

                        /* end line issue doc report 11102018 - default dropdown org for role site and org admin */
                        
                    }
                }, error => {
                    this.service.errorserver();
                });
        }
    }

    onCountriesSelect(item: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }

        if (this.selectedItemsOrganizationId.length != 0 && this.selectedItemsSiteId.length != 0) {
            this.selectedItemsOrganizationId = [];
            this.selectedItemsSiteId = [];

            this.dropdownListSiteId = [];
        }

        this.populateOrgByCountry(this.selectedItemsCountry); /* ternyata org kudu di populate hela by country nyet teh, naha kamari pas nyobaan di old system teu nyadar sih, jangar */

    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;

        if (this.selectedItemsOrganizationId.length != 0 && this.selectedItemsSiteId.length != 0) {
            this.selectedItemsOrganizationId = [];
            this.selectedItemsSiteId = [];

            this.dropdownListSiteId = [];
        }

        this.populateOrgByCountry(this.selectedItemsCountry); /* ternyata org kudu di populate hela by country nyet teh, naha kamari pas nyobaan di old system teu nyadar sih, jangar */

    }

    onCountriesSelectAll(items: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }

        if (this.selectedItemsOrganizationId.length != 0 && this.selectedItemsSiteId.length != 0) {
            this.selectedItemsOrganizationId = [];
            this.selectedItemsSiteId = [];

            this.dropdownListSiteId = [];
        }

        this.populateOrgByCountry(this.selectedItemsCountry); /* ternyata org kudu di populate hela by country nyet teh, naha kamari pas nyobaan di old system teu nyadar sih, jangar */

    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;

        if (this.selectedItemsOrganizationId.length != 0 && this.selectedItemsSiteId.length != 0) {
            this.selectedItemsOrganizationId = [];
            this.selectedItemsSiteId = [];

            this.dropdownListSiteId = [];
        }

        this.populateOrgByCountry(this.selectedItemsCountry); /* ternyata org kudu di populate hela by country nyet teh, naha kamari pas nyobaan di old system teu nyadar sih, jangar */

    }

    /* hemm */
    onSiteSelect(item: any) { }

    OnSiteDeSelect(item: any) { }

    onSiteSelectAll(items: any) { }

    onSiteDeSelectAll(items: any) { }
    /* hemm */

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.dataFound = false;
    }

    getPrimaryAndSecondaryProduct() {
        var productTemp;
        this.service.httpClientGet("api/Product/GetPrimaryAndSecondaryProducts", productTemp)
            .subscribe(result => {
                productTemp = result;
                this.Primary = productTemp.primaryProducts;
                this.Secondary = productTemp.SecondaryProducts;
            });
    }

    getEventType() {
        function compare(a, b) {

            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        if (this.selectedCertificate.length > 0) {
            this.selectedCertificate.forEach(item => {
                if (item.id == 3) {
                    var dataTemp: any;
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
                        .subscribe(result => {
                            dataTemp = result;
                            dataTemp.sort(compare);
                            this.dropdownEventType = dataTemp.map(obj => {
                                return {
                                    id: obj.Key,
                                    itemName: obj.KeyValue
                                }
                            });
                            this.showEvent = true;
                        }, error => { this.service.errorserver(); });
                } else {
                    this.showEvent = false;
                    this.selectedEventType = [];
                }
            });
        } else {
            this.showEvent = false;
            this.selectedEventType = [];
        }
    }

    onCertificateSelect(item: any) { this.getEventType(); }
    OnCertificateDeSelect(item: any) {
        this.dataFound = false;
        this.getEventType();
    }
    onCertificateSelectAll(item: any) { this.getEventType(); }
    onCertificateDeSelectAll(item: any) {
        this.dataFound = false;
        this.getEventType();
    }

    onEventTypeSelect(item: any) { }
    OnEventTypeDeSelect(item: any) { }
    onEventTypeSelectAll(item: any) { }
    onEventTypeDeSelectAll(item: any) { }

    selectedScore(score) {
        var choosen: number = 0;
        switch (score) {
            case 100:
                choosen = 5;
                break;
            case 75:
                choosen = 4;
                break;
            case 50:
                choosen = 3;
                break;
            case 25:
                choosen = 2;
                break;
            default:
                choosen = 1;
                break;
        }
        return choosen;
    }

    question8(val) {
        var choosen: string = "";
        switch (val) {
            case "1":
                choosen = "Architecture, Engineering & Construction";
                break;
            case "2":
                choosen = "Automotive & Transportation";
                break;
            case "3":
                choosen = "Government";
                break;
            case "4":
                choosen = "Infrastructure and Civil Engineering";
                break;
            case "5":
                choosen = "Manufacturing";
                break;
            case "6":
                choosen = "Media & Entertainment";
                break;
            case "7":
                choosen = "Utilities & Telecommunications";
                break;

            default:
                choosen = "Other";
                break;
        }
        return choosen;
    }

    CourseTeachingLvl(update, essential, intermediate, advanced, custom, other, comment) {
        let arr_ctl = [update, essential, intermediate, advanced, custom, other];
        var index = arr_ctl.indexOf(1);
        var ctl = "";
        switch (index) {
            case 0:
                ctl = "Update"
                break;
            case 1:
                ctl = "Level 1 : Essentials";
                break;
            case 2:
                ctl = "Level 2 : Intermediate";
                break;
            case 3:
                ctl = "Level 3 : Advanced";
                break;
            case 4:
                ctl = "Customized";
                break;

            default:
                ctl = "Other : " + comment;
                break;
        }

        return ctl;
    }

    CourseTeachingMaterial(autodesk, aap, atc, independent, independent_online, atc_online, other, comment) {
        let arr_ctm = [autodesk, aap, atc, independent, independent_online, atc_online, other];
        var index = arr_ctm.indexOf(1);
        var ctm = "";
        switch (index) {
            case 0:
                ctm = "Autodesk Official Courseware (any Autodesk branded material)";
                break;
            case 1:
                ctm = "Autodesk Authors and Publishers (AAP) Program Courseware";
                break;
            case 2:
                ctm = "Course material developed by the ATC";
                break;
            case 3:
                ctm = "Course material developed by an independent vendor or instructor";
                break;
            case 4:
                ctm = "Online e-learning course material developed by an independent vendor or instructor";
                break;
            case 5:
                ctm = "Online e-learning course material developed by the ATC";
                break;

            default:
                ctm = "Other : " + comment;
                break;
        }

        return ctm;
    }

    countdata: number = 0;
    page: number = 0;
    getReport() {
        if(this.selectedYear)
            if(
                (this.modelPopup1.year !=this.selectedYear && this.modelPopup1.year !=Number(this.selectedYear)+1) ||
                (this.modelPopup1.year == this.selectedYear && this.modelPopup1.month <2) || 
                (this.modelPopup1.year == Number(this.selectedYear)+1 && this.modelPopup1.month >1) || 
                (this.modelPopup2.year !=this.selectedYear && this.modelPopup2.year !=Number(this.selectedYear)+1) ||
                (this.modelPopup2.year == this.selectedYear && this.modelPopup2.month <2) || 
                (this.modelPopup2.year == Number(this.selectedYear)+1 && this.modelPopup2.month >1)
            )
                return swal(
                    'Date range is not valid',
                    'Please select date between 1 Feb '+this.selectedYear+' and 31 Jan '+Number(Number(this.selectedYear)+1),
                    'error'
                )
        let er = false
        if(this.selectedPartner.length == 0){
            this.validPartner = false
            er = true
        }else this.validPartner = true
        //if(this.selectedCertificate.length == 0){
        //    this.validCer = false
        //    er = true
        //}else this.validCer = true
        if(this.selectedItemsTerritories.length > 1 || this.selectedItemsTerritories.length==0){
            this.validTer = false
            er = true
        }else this.validTer = true
        //if(er)
        //    return
        if (this.selectedPartner.length != 0) // && this.selectedItemsTerritories.length == 1
		{
            if(this.selectedCertificate.length ==0 && this.selectedPartner[0].id ==58){
                swal(
                    'Field is Required!',
                    'Please select at least one certificate type',
                    'error'
                )
            }
            else{
            this.loading = true;
            var isSite = this.itsSite();
            var isOrg = this.itsOrganization();
            var isDistributor = this.itsDistributor();

            this.page = 1;
            var geo = [];
            var countries = [];
            var territories = [];
            var organization = [];
            var sites = [];
            var marketType = [];
            var partnerType = [];
            this.eventType = [];
            this.certificateType = [];
            var course;
            var project;
            var dateStart: string;
            var dateEnd: string;

            if (this.modelPopup1 != null && this.modelPopup2 != null) {
                dateStart = this.parserFormatter.format(this.modelPopup1);
                dateEnd = this.parserFormatter.format(this.modelPopup2);
            } else {
                dateStart = "";
                dateEnd = "";
            }

            for (let i = 0; i < this.selectedItemsGeo.length; i++) {
                geo.push('"' + this.selectedItemsGeo[i].id + '"');
            }

            for (let i = 0; i < this.selectedItemsCountry.length; i++) {
                countries.push('"' + this.selectedItemsCountry[i].id + '"');
            }

            for (let i = 0; i < this.selectedItemsTerritories.length; i++) {
                territories.push(this.selectedItemsTerritories[i].id);
            }

            if (this.notadmin) {
                if (this.selectedItemsOrganizationId.length > 0) {
                    for (let i = 0; i < this.selectedItemsOrganizationId.length; i++) {
                        organization.push('"' + this.selectedItemsOrganizationId[i].id + '"');
                    }
                } else {
                    for (let i = 0; i < this.orgtmp.length; i++) {
                        organization.push('"' + this.orgtmp[i].id + '"');
                    }
                }

                if (this.selectedItemsSiteId.length > 0) {
                    for (let i = 0; i < this.selectedItemsSiteId.length; i++) {
                        sites.push('"' + this.selectedItemsSiteId[i].id + '"');
                    }
                } else {
                    for (let i = 0; i < this.sitetmp.length; i++) {
                        sites.push('"' + this.sitetmp[i].id + '"');
                    }
                }
            } else {
                // for (let i = 0; i < this.selectedItemsOrganizationId.length; i++) {
                //     organization.push('"' + this.selectedItemsOrganizationId[i].id + '"');
                // }

                // for (let i = 0; i < this.selectedItemsSiteId.length; i++) {
                //     sites.push('"' + this.selectedItemsSiteId[i].id + '"');
                // }

                /* Download site evaluation: Select any country and generate a report the count remains same The country list is the list which are avaialble in the organisation */

                if(this.selectedItemsOrganizationId.length < 1 && this.dropdownListOrganizationId.length > 0 ){
                    for (let i = 0; i < this.dropdownListOrganizationId.length; i++) {
                        organization.push('"' + this.dropdownListOrganizationId[i].id + '"');
                    }
                }else{
                    for (let i = 0; i < this.selectedItemsOrganizationId.length; i++) {
                        organization.push('"' + this.selectedItemsOrganizationId[i].id + '"');
                    }
                }

                if(this.selectedItemsSiteId.length < 1 && this.dropdownListSiteId.length > 0 ){
                    for (let i = 0; i < this.dropdownListSiteId.length; i++) {
                        sites.push('"' + this.dropdownListSiteId[i].id + '"');
                    }
                }else{
                    for (let i = 0; i < this.selectedItemsSiteId.length; i++) {
                        sites.push('"' + this.selectedItemsSiteId[i].id + '"');
                    }
                }

                /* Download site evaluation: Select any country and generate a report the count remains same The country list is the list which are avaialble in the organisation */
            }

            for (let i = 0; i < this.selectedItemsMarketType.length; i++) {
                marketType.push('"' + this.selectedItemsMarketType[i].id + '"');
            }

            for (let i = 0; i < this.selectedPartner.length; i++) {
                partnerType.push('"' + this.selectedPartner[i].id + '"');
            }

            if (this.selectedCertificate.length > 0) {
                for (let i = 0; i < this.selectedCertificate.length; i++) {
                    if (this.selectedCertificate[i].id == 1) {
                        course = "true";
                    }if (this.selectedCertificate[i].id == 2) {
                        project = "true";
                    } /*else {
                        course = "";
                        project = "";
                    }*/
                }
            } else {
                course = "";
                project = "";
            }

            if (this.selectedEventType.length > 0) {
                for (let i = 0; i < this.selectedEventType.length; i++) {
                    this.eventType.push('"' + this.selectedEventType[i].id + '"');
                }
            }

            // this.dropdownListSiteId = this.sitetmp;
            // this.dropdownListOrganizationId = this.orgtmp;

            var params =
            {
                geoSelect: this.selectedItemsGeo,
                geoDropdown: this.dropdownListGeo,
                territorySelect: this.selectedItemsTerritories,
                territoryDropdown: this.dropdownListTerritories,
                countrySelect: this.selectedItemsCountry,
                countryDropdown: this.dropdownListCountry,
                yearSelect: this.selectedYear,
                orgSelect: this.selectedItemsOrganizationId,
                orgDropdown: this.dropdownListOrganizationId,
                siteSelect: this.selectedItemsSiteId,
                siteDropdown: this.dropdownListSiteId,
                marketSelect: this.selectedItemsMarketType,
                marketDropdown: this.dropdownListMarketType,
                partnerSelect: this.selectedPartner,
                partnerDropdown: this.dropdownPartner,
                certificateSelect: this.selectedCertificate,
                certificateDropdown: this.dropdownCertificate,
                dateRange: this.selectedValueDateRange,
                dateStart: this.modelPopup1,
                dateEnd: this.modelPopup2
            }
            //Masukan object filter ke local storage
            localStorage.setItem("filter", JSON.stringify(params));

            // "api/EvaluationAnswer/SiteEvaluation/{'Country':'" + countries + "','Territory':'" + territories + "','Year':'" + this.selectedYear +
            // "','PartnerType':'" + partnerType + "','MarketType':'" + marketType + "','Organization':'" + organization + "','Sites':'" + sites + "','AAP_Course':'" + course + "','AAP_Project':'" + project +
            // "','StartDate':'" + dateStart + "','CompletionDate':'" + dateEnd + "'}"

            /* kalo org atau site nya gak di pilih default sesuai role */
            var orgRes = organization.toString();
            var siteRes = sites.toString();

            // if (this.checkrole()) {
            //     if (!this.itsDistributor()) {
            //         if (this.itsSite()) {
            //             siteRes = this.urlGetSiteId();
            //         }
            //     }
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            if (this.adminya) {
                if (!this.distributorya) {
                    if (!this.orgya) {
                        if (this.siteya) {
                            siteRes = this.urlGetSiteId();
                        }
                    }
                }
            }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            /*if (course == "" && project == "") {
                partnerType = []; //dikosongin karena akan ngaruh ke totalnya
            }*/
            /* kalo org atau site nya gak di pilih default sesuai role */
            var isSiteRole = this.itsSite();
            var isOrgRole = this.itsOrganization();

            this.keyword = {
                GeoCode: geo.toString(),
                Country: countries.toString(),
                Territory: territories.toString(),
                Year: this.selectedYear,
                Role: this.useraccesdata.UserLevelId,
                PartnerType: partnerType.toString(),
                MarketType: marketType.toString(),
                Organization: orgRes,
                Sites: siteRes,
                AAP_Course: course,
                AAP_Project: project,
                Primary: this.selectedPrimaryProduct,
                Secondary: this.selectedSecondaryProduct,
                // CertificateType: this.certificateType,
                StartDate: dateStart,
                CompletionDate: dateEnd,
                EventType: this.eventType.toString(),
                DateRange: this.selectedValueDateRange,
                Limit: 0,
                Offset: 0
            };
            if (isSiteRole) {
                this.keyword.SiteID = this.useraccesdata.SiteId;
                this.keyword.OrgID = this.useraccesdata.OrgId;
                this.keyword.Country =countries.toString();
            }
            if (isOrgRole) {
                this.keyword.SiteID = this.useraccesdata.SiteId;
                this.keyword.OrgID = this.useraccesdata.OrgId;
                this.keyword.Country =countries.toString();
            }
           

            // console.log(this.keyword);
            //Get total record dari hasil pencarian

            var data: any;
            this.totalEval = 0;

            /* end line autodesk plan 18 oct */

            this.apiCall = this.service.httpClientPost(
                "api/EvaluationAnswer/TotalDownloadSite",
                this.keyword
            )
                .subscribe(result => {
					data=null;
                    data = result;
                    this.countdata = +data.count;
                    this.ptType = this.keyword.PartnerType;
                    this.getServerData(1,this.ptType);
                    //this.getServerData(1);
                    this.totalEval = this.countdata;
                    this.dataFound = true;
                    this.loading = false;
                });

            /* end line autodesk plan 18 oct */
            }
        } 
		else {
			var errorMessage="";
			if(this.selectedPartner.length == 0)
			{
				errorMessage='Please select a single Partner Type/Certificate Type for this particular Report!';
			}
			else if(this.selectedItemsTerritories.length > 1)
			{
				errorMessage='Please select a single Territory, currently we do not allow to choose multiple Territory';
			}
			else
			{
				errorMessage='Please select a Territory for this particular Report!';
			}
            swal(
                'Field is Required!',
                errorMessage,
                'error'
            )
        }
    }
    
    resetForm(){
        this.dataFound = false;
        this.selectedCertificate = [];
        this.selectedEventType = [];
        this.selectedItemsCountry = [];
        this.selectedItemsGeo = [];
        this.selectedItemsMarketType = [];
        this.selectedItemsOrganizationId = [];
        this.selectedItemsRegion = [];
        this.selectedItemsSiteId = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsTerritories = [];
        this.selectedPartner = [];
        this.selectedPrimaryProduct = [];
    }

    getUserAccessOrg(): string {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION" || userlvlarr[i] == "DISTRIBUTOR" || userlvlarr[i] == "SITE") {
                var orgArr = this.useraccesdata.OrgId.toString().split(',');
                var orgTmp = [];

                for (var i = 0; i < orgArr.length; i++) {
                    orgTmp.push('"' + orgArr[i] + '"');
                }

               // return orgTmp.join(",");
               return orgTmp.toString();
            } else {
                return "null";
            }
        }
    }

    // filterSiteId(value) {
    //     if (value.length > 4) {
    //         var data: any;
    //         this.service.httpClientGet(`api/MainOrganization/FindSite/` + this.useraccesdata.UserLevelId + `/` + this.getUserAccessOrg() + `/` + value, data)
    //             .subscribe(res => {
    //                 data = res["results"];
    //                 if (data.length > 0) {
    //                     this.dropdownListSiteId = data.map((item) => {
    //                         return {
    //                             id: item.SiteId,
    //                             itemName: item.SiteId
    //                         }
    //                     });
    //                     // this.sitetmp = data.map((item) => {
    //                     //     return {
    //                     //         id: item.SiteId,
    //                     //         itemName: item.SiteId
    //                     //     }
    //                     // });
    //                 }
    //             }, error => {
    //                 this.service.errorserver();
    //             });
    //     }
    // }

    // filterOrgId(value) {
    //     if (value.length > 4) {
    //         var data: any;
    //         this.service.httpClientGet(`api/MainOrganization/FindOrganization/` + this.useraccesdata.UserLevelId + `/` + this.getUserAccessOrg() + `/` + value, data)
    //             .subscribe(res => {
    //                 data = res["results"];
    //                 if (data.length > 0) {
    //                     this.dropdownListOrganizationId = data.map((item) => {
    //                         return {
    //                             id: item.OrgId,
    //                             itemName: item.OrgId
    //                         }
    //                     });
    //                 }
    //             }, error => {
    //                 this.service.errorserver();
    //             });
    //     }
    // }

    filterSiteIdNew(value) {

        this.dropdownListSiteId = [];
        this.selectedItemsSiteId = [];

        // if (this.itsSite()) {
        //     var keyword = this.urlGetSiteId();
        // } else {
        //     var keyword = this.getUserAccessOrg();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var id = this.getUserAccessOrg();
        if (this.adminya) {
            if (!this.distributorya) {
                if (!this.orgya) {
                    if (this.siteya) {
                        id = this.urlGetSiteId();
                    }
                }
            }
        }

       var keyword : any ={
            id:id
        }
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/
        keyword = JSON.stringify(keyword);
        var data: any;
        var tmp = [];
        this.service.httpClientPost(`api/MainOrganization/FindSiteNew/` + this.useraccesdata.UserLevelId + `/` + value, keyword)
            .subscribe(res => {
                data = res["results"];
                
                if (data.length > 0) {
                    
                    tmp = data.map((item) => {
                        return {
                            id: item.SiteId,
                            itemName: item.SiteId
                        }
                    });

                    for (var i = 0; i < tmp.length; i++) {
                        this.dropdownListSiteId.push({
                            id: tmp[i].id,
                            itemName: tmp[i].itemName
                        });
                        this.sitetmp.push({
                            id: tmp[i].id,
                            itemName: tmp[i].itemName
                        });

                        /* issue doc report 11102018 - default dropdown site for role site and org admin */

                        if(this.adminya){
                            if(!this.distributorya){
                                this.selectedItemsSiteId.push({
                                    id: tmp[i].id,
                                    itemName: tmp[i].itemName
                                });
                            }
                        }

                        /* end line issue doc report 11102018 - default dropdown site for role site and org admin */
                    }
                }
            }, error => {
                this.service.errorserver();
            });
    }

    onOrgSelect(item: any) {
        this.dataFound = false;
        this.selectedItemsSiteId = [];
        this.filterSiteIdNew(item.id);
    }

    OnOrgDeSelect(item: any) {
        this.dataFound = false;
        this.selectedItemsSiteId = [];
        this.dropdownListSiteId = [];
        this.sitetmp = [];
        if (this.selectedItemsOrganizationId.length > 0) {
            for (var i = 0; i < this.selectedItemsOrganizationId.length; i++) {
                this.filterSiteIdNew(this.selectedItemsOrganizationId[i].id);
            }
        }
    }

    onOrgSelectAll(items: any) {
        this.dataFound = false;
        this.selectedItemsSiteId = [];
        this.dropdownListSiteId = [];
        this.sitetmp = [];
        if (this.selectedItemsOrganizationId.length > 0) {
            for (var i = 0; i < this.selectedItemsOrganizationId.length; i++) {
                this.filterSiteIdNew(this.selectedItemsOrganizationId[i].id);
            }
        }
    }

    onOrgDeSelectAll(items: any) {
        this.dataFound = false;
        this.selectedItemsSiteId = [];
        this.dropdownListSiteId = [];
        this.sitetmp = [];
    }
}
