import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DownloadSiteEVAComponent } from './download-site-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppService } from "../../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';
import { Ng2PaginationModule } from 'ng2-pagination';

export const DownloadSiteEVARoutes: Routes = [
    {
        path: '',
        component: DownloadSiteEVAComponent,
        data: {
            breadcrumb: 'epdb.report_eva.download_site_evaluation.download_site_evaluation',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DownloadSiteEVARoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule,
        Ng2PaginationModule
    ],
    declarations: [DownloadSiteEVAComponent],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})
export class DownloadSiteEVAModule { }