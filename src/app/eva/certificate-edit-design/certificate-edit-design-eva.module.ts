import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateEditDesignEVAComponent } from './certificate-edit-design-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const CertificateEditDesignEVARoutes: Routes = [
  {
    path: '',
    component: CertificateEditDesignEVAComponent,
    data: {
      breadcrumb: 'Certificate Edit',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificateEditDesignEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CertificateEditDesignEVAComponent],
  providers:[AppService]
})
export class CertificateEditDesignEVAModule { }
