
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import * as interact from 'interactjs';
import $ from 'jquery/dist/jquery';
import { ColorPickerService, Rgba } from "ngx-color-picker";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';


import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';
declare var html2pdf :any;

import { SessionService } from '../../shared/service/session.service';



const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
!one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
!one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
? false : one.day > two.day : one.month > two.month : one.year > two.year;


const now = new Date();

@Component({
  selector: 'app-certificate-edit-design-eva',
  templateUrl: './certificate-edit-design-eva.component.html',
  styleUrls: [
  './certificate-edit-design-eva.component.css',
  '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [ColorPickerService]
})


export class CertificateEditDesignEVAComponent implements OnInit {
  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  public certDescColor: string = '#3dbecc';
  public certTypeColor: string = '#000';
  public certNoColor: string = '#000';
  public RegisteredColor: string = '#000';
  public StudentTagColor: string = '#000';
  public StudentNameColor: string = '#000';
  public TitleTagColor: string = '#000';
  public TitleColor: string = '#000';
  public ProductColor: string = '#000';
  public SignTagColor: string = '#000';
  public DateColor: string = '#000';
  enable: boolean = false;
  Logoenable: boolean = false;
  TextCertificateType: string = "";
  // TextCertificateNo: string = "Certificate No";
  TextCertificateDesc: string = "";
  TextRegistered: string = ""
  TextStudentTag: string = "";
  // TextStudentName: string = "Abdul Wahab Mukti";
  TextTitleTitle: string = "";
  TextTitle: string = ""
  TextProduct: string = "";
  TextSignTag: string = "";
  TextSignName: string = "";
  TextDate: string = '';
  ImgBackground: string;

  // Text Header
  TextTextHeader: string = "";
  TextTextHeaderDescription: string = ""
  TextTextFooter: string = ""

  //Certificate No
  TextTitleCertificateNo: string = "Certificate No.";
  TextCertificateNo: string = "";

  //Student Name
  TextTitleStudentName: string = "";
  TextStudentName: string = "Data:Student Name";

  //Event
  TextTitleEvent: string = "";
  TextEvent: string = "Data: Event";

  //Location Event
  TextTitleLocationEvent: string = "";
  TextLocationEvent: string = "Data: Event Location";

  //Parnet Site

  TextTitlePartner: string = "";
  TextPartner: string = "Data: Partner Site";

  // Product
  TextTitleProduct: string = "";

  // Instructor
  TextTitleInstructor: string = "";

  //Date
  TextTitleDate: string = "";
  YearNow = now.getFullYear();

  // Duration
  TextTitleDuration: string = "";

  // Institution
  TextTitleInstitution: string = "";


  updateTemplateCertificateForm: FormGroup;
  beforeSave: FormGroup;
  public TemplateCertificateType: any;
  private _serviceUrl = 'api/Certificate';
  id: string;
  public data: any;
  public Encode_data: any;
  public certificatedesign: any;
  //tambahan
  dropdownPartner = [];
  private dropdownCertificate;

  private survey;
  private PartnerType;
  private CertificateType;
  private Year;
  partnerType: any;
  certificateType: any;
  year: any;

  public showColorCode: string = '#db968d';

  public loading = false;
  private LanguageCertificateTemp;
  private LanguageCertificate;

  public useraccesdata: any;

  @ViewChild('forEditaCanvas') content: ElementRef;
  public downloadPDF() {
    this.loading = true;
    
    var element = document.getElementById('forEditaCanvas');
    html2pdf(element, {
      filename: 'SampleCertificate.pdf',
      image: {type: 'jpeg', quality: 1},
      html2canvas: {scale: 2, logging: true, width:870, height:614},
      jsPDF: {format: 'a4', orientation: 'l'}
    });

    setTimeout(() =>{
      this.loading = false;
    },5000)
  }
  // public downloadPDF() {
  //   html2canvas(document.getElementById('forEditaCanvas')).then(function (canvas) {
  //     var self = this;
  //     var doc = new jspdf('l', 'in', 'a4');
  //     var img = canvas.toDataURL("image/png");
  //     doc.addImage(img, 'PNG', -0.05, 0, 11.73, 8);
  //     doc.save('SampleCertificate.pdf');

  //   });
  // }




  loadScripts() {
    const dynamicScripts = [
    '../../../../assets/html2pdf/jspdf.min.js',
    '../../../../assets/html2pdf/html2canvas.min.js',
    '../../../../assets/html2pdf/html2pdf.min.js',
    '../../../../assets/html2pdf/html2pdf.bundle.min.js',
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  constructor(private session: SessionService, public cpService: ColorPickerService, public parserFormatter: NgbDateParserFormatter, private router: Router, private service: AppService, private route: ActivatedRoute, ) {
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    this.loadScripts()
    //validation
    let Name = new FormControl('', Validators.required);
    let Code = new FormControl('', Validators.required);
    this.updateTemplateCertificateForm = new FormGroup({
      Name: Name,
      Code: Code,

    });

    let PartnerTypeId = new FormControl('', Validators.required);
    let CertificateTypeId = new FormControl('', Validators.required);
    let Year = new FormControl('', Validators.required);
    let LanguageCertificateTemp = new FormControl('', Validators.required);
    this.beforeSave = new FormGroup({
      PartnerTypeId: PartnerTypeId,
      CertificateTypeId: CertificateTypeId,
      Year: Year,
      LanguageCertificateTemp: LanguageCertificateTemp
    })

  }


  ZoomOutText(PartnerType, CertificateId) {
    var userLang = window.navigator.language
    console.log(userLang)
    if (userLang == "zh-TW" || userLang == 'zh-CN' || userLang == 'th' || userLang == 'ja' || userLang == 'ko') {
      if(PartnerType == 1){
        $('#textfooter').css({
          'width': '1338px',
          'transform': 'translate(-223px, 553px) scale(0.6)',
          '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
        })  
        $('#textheaderdescription').css({
          'width': '324px',
          'transform': 'translate(28px, 286px) scale(0.9)',
          '-webkit-transform': 'translate(28px, 286px) scale(0.9)'
        })
        
      }
      else if(PartnerType == 61){
        $('#textheaderdescription').css({
          'width': '864px',
          'transform': 'translate(1px, 286px) scale(0.9)',
          '-webkit-transform': 'translate(1px, 286px) scale(0.9)'
        })
        $('#textfooter').css({
          'width': '1338px',
          'transform': 'translate(-223px, 553px) scale(0.6)',
          '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
        })  
      }
      else if(PartnerType == 58){
        if(CertificateId == 2){
          $('#textfooter').css({
            'width': '1100px',
            'transform': 'translate(0px, 555px) scale(0.5)',
            '-webkit-transform': 'translate(0px, 555px) scale(0.5)'
          })  
          $('#textheaderdescription').css({
            'width': '633.552px',
            'transform': 'translate(216px, 137.111px) scale(0.9)',
            '-webkit-transform': 'translate(216px, 137.111px) scale(0.9)'
          })

        } 
        else if(CertificateId == 3){
          $('#textfooter').css({
            'width': '1237px',
            'transform': 'translate(-180px, 538px) scale(0.6)',
            '-webkit-transform': 'translate(-180px, 538px) scale(0.6)'
          })  
          $('#textheaderdescription').css({
            'width': '539px',
            'transform': 'translate(300px, 131px) scale(0.9)',
            '-webkit-transform': 'translate(300px, 131px) scale(0.9)'
          })
        }
        else if(CertificateId == 1){
          $('#textfooter').css({
            'width': '1338px',
            'transform': 'translate(-223px, 553px) scale(0.6)',
            '-webkit-transform': 'translate(-223px, 553px) scale(0.6)'
          })  

          $('#textheaderdescription').css({
            'transform': 'translate(28px, 283px) scale(0.9)',
            '-webkit-transform': 'translate(28px, 283px) scale(0.9)'
          })
        }
      }
    }
  }


  getPartnerType() {
    var partner = ['1', '58', '61'];
    var partnerTemp: any;
    this.dropdownPartner = [];
    for (let i = 0; i < partner.length; i++) {
      this.service.httpClientGet("api/Roles/" + partner[i], partnerTemp)
      .subscribe(result => {
        partnerTemp = result;
        this.dropdownPartner.push(partnerTemp);
      }, error => {
        this.service.errorserver();
      });
    }
  }

  selectedPartner(newvalue) {
    this.PartnerType = newvalue;
    if (newvalue == 58) {
      var certificate: any;
      var parent = "AAPCertificateType";
      this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "'}", certificate)
      .subscribe(result => {
        this.dropdownCertificate = result;
      }, error => {
        this.service.errorserver();
      });
      this.beforeSave.patchValue({
        CertificateTypeId: ""
      })
      $('#CertificateTypeId').attr({ "disabled": false })

    } else if (newvalue == 1) {
      //Show Hide Div LOGO
      $('#DivLogo').css('display', 'none');
      $('#content_logo').css({ "visibility": "hidden" })
      this.dropdownCertificate = [];
      this.beforeSave.patchValue({
        CertificateTypeId: 0
      })
      $('#CertificateTypeId').attr({ "disabled": true })

      //Format Design Certificate
      $('#content_background').attr('src', '/assets/certificate/ATC-Course.png')
      this.DesignAAPATCCourse()
      this.ZoomOutText(newvalue, 0)
    } else if (newvalue == 61) {
      //Show Hide Div LOGO
      $('#DivLogo').css('display', 'none');
      $('#content_logo').css({ "visibility": "hidden" })
      this.dropdownCertificate = [];
      this.beforeSave.patchValue({
        CertificateTypeId: 0
      })
      $('#CertificateTypeId').attr({ "disabled": true })
      this.DesignCTCCourse()
      this.ZoomOutText(newvalue, 0)
    }

  }
  getLanguage() {
    var data: any
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", data)
    .subscribe(result => {
      this.LanguageCertificateTemp = result;
    }, error => {
      this.service.errorserver();
    });
  }


  selectedCertificate(newvalue) {
    newvalue != "" ? this.CertificateType = newvalue : this.CertificateType = null;
    this.FormatDesignCeritificate(newvalue)
    this.ZoomOutText(58, newvalue)
  }

  selectedYear(newvalue) {
    newvalue != "" ? this.Year = newvalue : this.Year = "";
  }

  selectedLanguage(newvalue) {
    this.LanguageCertificate = newvalue;
  }

  getSurveyIndicator() {
    var dataTemp: any;
    var parent = "FYIndicator";
    var status = "A";
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
    .subscribe(result => {
      this.survey = result;
    }, error => { this.service.errorserver(); });
  }


  ngOnInit() {

    this.getPartnerType();
    this.getSurveyIndicator();
    this.selectToday()

    //Languange
    this.getLanguage()


    this.id = this.route.snapshot.params['id'];
    var data = '';

    //get activity data detail
    this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
    .subscribe(result => {
      if (result == "Not found") {
        this.service.notfound();
        this.data = '';
      }
      else {
        // var dataResult = result.replace(/\t/g, " ");
        // dataResult = dataResult.replace(/\r/g, " ");
        // dataResult = dataResult.replace(/\n/g, " ");
        // this.data = JSON.parse(dataResult);
        this.data = result;
        console.log(this.data)
       
          // console.log(this.data);
          this.partnerType = this.data[0].PartnerTypeId;
          this.selectedPartner(this.partnerType);
          this.data[0].CertificateTypeId == 0 ? this.certificateType = "" : this.certificateType = this.data[0].CertificateTypeId;
          this.year = this.data[0].Year;
          this.LanguageCertificate = this.data[0].LanguageCertificate;
          this.buildForm();
          this.Encode_data = atob(this.data[0].HTMLDesign)

          let fragmentFromString = function (strHTML) {
            return document.createRange().createContextualFragment(strHTML);
          }

        let fragment = fragmentFromString(this.Encode_data);
        document.getElementById("forEditaCanvas").appendChild(fragment);
        
          $('#textheader').html(this.data[0].TitleTextHeader)
          $('#textheaderdescription').html(this.data[0].TitleHeaderDescription)
          $('#textfooter').html('-')
          $('#titlecertno').html(this.data[0].TitleCertificateNo)
          $('#titlecertstudent').html(this.data[0].TitleStudentName)
          $('#titlecertevent').html(this.data[0].TitleEvent)
          $('#titlecerttitle').html(this.data[0].TitleCourseTitle)
          $('#titlecertproduct').html(this.data[0].TitleProduct)
          $('#titlecertinstructor').html(this.data[0].TitleInstructor)
          $('#titlecertdate').html(this.data[0].TitleDate)
          $('#titlecertduration').html(this.data[0].TitleDuration)
          $('#titlecertpartner').html(this.data[0].TitlePartner)
          $('#titlecertinstitution').html(this.data[0].TitleInstitution)
         
        
          //Show Hide Div LOGO
          if (this.data[0].CertificateTypeId == '2') {
            $('#DivLogo').css('display', "");

        }
        
          this.TextTextHeader = this.data[0].TitleTextHeader
          this.TextTextHeaderDescription = this.data[0].TitleHeaderDescription
          this.TextTextFooter = this.data[0].TitleTextFooter
          this.TextTitleCertificateNo = this.data[0].TitleCertificateNo
          this.TextTitleStudentName = this.data[0].TitleStudentName
          this.TextTitleEvent = this.data[0].TitleEvent
          this.TextTitleTitle = this.data[0].TitleCourseTitle
          this.TextTitleProduct = this.data[0].TitleProduct
          this.TextTitleInstructor = this.data[0].TitleInstructor
          this.TextTitleDate = this.data[0].TitleDate
          this.TextTitleDuration = this.data[0].TitleDuration
          this.TextTitlePartner = this.data[0].TitlePartner
          this.TextTitleInstitution = this.data[0].TitleInstitution
          this.TextTitleLocationEvent = this.data[0].TitleLocation

        this.ZoomOutText(this.data[0].PartnerTypeId, this.data[0].CertificateTypeId)

        
        var parent = document.querySelector('content');
       
        if ($('#orglogocert').length) {
          if (document.getElementById("orglogo").style.visibility == "visible") {
            $('#org').addClass("checked");
            $('.switch_orglogo').attr("checked");
            this.enable = true;
          }
          else {
            $('#org').removeClass("checked");
            this.enable = false;
          }
        } else {
          $('#org').removeClass("checked");
          this.enable = false;
          $("#content").append("<div id='orglogocert' class='draggable drag'><img id = 'orglogo' name='orglogo' src = 'assets/certificate/logo.png' style ='max-width:100px; max-height:100px; visibility: hidden;'></div>");
          }

          if ($('#logocert').length) {
            if (document.getElementById("content_logo").style.visibility == "visible") {
              $('#logo').addClass("checked");
              $('.switch_cartlogo').attr("checked");
              this.Logoenable = true;
            }
            else {
              $('#logo').removeClass("checked");
              this.Logoenable = false;
            }
          } else {
            $('#logo').removeClass("checked");
            this.Logoenable = false;
            $("#content").append("<div id='logocert' class='draggable drag'><img id = 'content_logo' name='content_logo' src = 'assets/certificate/logo.png' style ='max-width:100px; max-height:100px; visibility: hidden;'></div>");
            }
        }
        
        
      },
      error => {
        this.service.errorserver();
      });

    //get data DesignBackground Category
    var data = '';
    this.service.httpClientGet('api/CertificateBackground', data)
    .subscribe(result => {
      if (result == "Not found") {
        this.service.notfound();
        this.certificatedesign = '';
      }
      else {
        this.certificatedesign = result;
      }
    },
    error => {
      this.service.errorserver();
      this.certificatedesign = '';
    });

    //interact JS
    interact('.draggable')
    .draggable({
        // enable inertial throwing
        inertia: true,
        // keep the element within the area of it's parent
        restrict: {
          restriction: "parent",
          endOnly: true,
          elementRect: { top: 0, left: 0, bottom: 0, right: 0 }
        },
        // enable autoScroll
        autoScroll: true,

        // call this function on every dragmove event
        onmove: dragMoveListener,
      });

    interact('.draggableresize')
    .resizable({
        // resize from all edges and corners
        edges: { left: true, right: true, bottom: true, top: true },

        // minimum size
        restrict: {

          endOnly: true,
        },
        // keep the edges inside the parent
        onmove: resizeMoveListener,
        inertia: true,
      })

    interact('.draggableresize').draggable({
      // enable inertial throwing
      inertia: true,
      // keep the element within the area of it's parent
      restrict: {
        restriction: "parent",
        endOnly: true,
        elementRect: { top: 0, left: 0, bottom: 0, right: 0 }
      },
      // enable autoScroll
      autoScroll: true,

      // call this function on every dragmove event
      onmove: dragMoveListener,
    });

    function dragMoveListener(event) {
      var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

      // translate the element
      target.style.webkitTransform =
      target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

      // update the posiion attributes
      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);

    }

    function resizeMoveListener(event) {
      var x, y;
      var target = event.target;
      x = (parseFloat(target.getAttribute('data-x')) || 0),
      y = (parseFloat(target.getAttribute('data-y')) || 0);

      // // update the element's style
      target.style.width = event.rect.width + 'px';
      target.style.height = event.rect.height + 'px';

      // // translate when resizing from top or left edges
      x += event.deltaRect.left;
      y += event.deltaRect.top;

      target.style.webkitTransform = target.style.transform =
      'translate(' + x + 'px,' + y + 'px)';

      target.setAttribute('data-x', x);
      target.setAttribute('data-y', y);
    }





  }

  buildForm(): void {
    let Name = new FormControl(this.data[0].Name, Validators.required);
    let Code = new FormControl(this.data[0].Code, Validators.required);
    this.updateTemplateCertificateForm = new FormGroup({
      Name: Name,
      Code: Code,
    });

    let PartnerTypeId = new FormControl(this.data[0].PartnerTypeId, Validators.required);
    let CertificateTypeId = new FormControl(this.data[0].CertificateTypeId, Validators.required);
    let Year = new FormControl(this.data[0].Year, Validators.required);
    let LanguageCertificateTemp = new FormControl(this.data[0].LanguageCertificate, Validators.required);
    this.beforeSave = new FormGroup({
      PartnerTypeId: PartnerTypeId,
      CertificateTypeId: CertificateTypeId,
      Year: Year,
      LanguageCertificateTemp: LanguageCertificateTemp
    })

  }
  //submit form change password
  onSubmitUpdateCert() {

    //Set Variable Certificate
    var TitleTextHeader = $('#textheader').html()
    var TitleHeaderDescription = $('#textheaderdescription').html();
    var TitleTextFooter = '-'
    var TitleCertificateNo = $('#titlecertno').html();
    var TitleStudentName = $('#titlecertstudent').html();
    var TitleEvent = $('#titlecertevent').html();
    var TitleCourseTitle = $('#titlecerttitle').html();
    var TitleProduct = $('#titlecertproduct').html();
    var TitleInstructor = $('#titlecertinstructor').html();
    var TitleDate = $('#titlecertdate').html();
    var TitleDuration = $('#titlecertduration').html();
    var TitlePartner = $('#titlecertpartner').html();
    var TitleInstitution = $('#titlecertinstitution').html();
    var TitleLocation = $('#titlecerteventlocation').html();

    


    this.updateTemplateCertificateForm.controls['Code'].markAsTouched();
    this.updateTemplateCertificateForm.controls['Name'].markAsTouched();
    
    //on valid
    if (this.updateTemplateCertificateForm.valid) {
      swal({
        title: 'Are you sure?',
        text: "If you wish to Update Certificate!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          $('#textheader').html('-')
          $('#textheaderdescription').html('-')
          $('#textfooter').html('-')
          $('#titlecertno').html('-')
          $('#titlecertstudent').html('-')
          $('#titlecertevent').html('-')
          $('#titlecerttitle').html('-')
          $('#titlecertproduct').html('-')
          $('#titlecertinstructor').html('-')
          $('#titlecertdate').html('-')
          $('#titlecertduration').html('-')
          $('#titlecertpartner').html('-')
          $('#titlecertinstitution').html('-')
          $('#titlecerteventlocation').html('-')


          var element = document.getElementById('content');
          var htmlCertificate = element.outerHTML;
          var htmlCertificate_Encode = btoa(htmlCertificate)
          this.updateTemplateCertificateForm.value.HTMLDesign = htmlCertificate_Encode;
          this.updateTemplateCertificateForm.value.CertificateBackgroundId = "-";
          this.updateTemplateCertificateForm.value.Status = "A";
          
          this.updateTemplateCertificateForm.value.PartnerTypeId = this.PartnerType;
          this.updateTemplateCertificateForm.value.CertificateTypeId = this.CertificateType;
          this.updateTemplateCertificateForm.value.Year = this.Year;
          this.updateTemplateCertificateForm.value.LanguageCertificate = this.LanguageCertificate;
          //Set Variable into Parameter Database
          this.updateTemplateCertificateForm.value.TitleTextHeader = TitleTextHeader
          this.updateTemplateCertificateForm.value.TitleHeaderDescription = TitleHeaderDescription
          this.updateTemplateCertificateForm.value.TitleTextFooter = TitleTextFooter
          this.updateTemplateCertificateForm.value.TitleCertificateNo = TitleCertificateNo
          this.updateTemplateCertificateForm.value.TitleStudentName = TitleStudentName
          this.updateTemplateCertificateForm.value.TitleEvent = TitleEvent
          this.updateTemplateCertificateForm.value.TitleCourseTitle = TitleCourseTitle
          this.updateTemplateCertificateForm.value.TitleProduct = TitleProduct
          this.updateTemplateCertificateForm.value.TitleInstructor = TitleInstructor
          this.updateTemplateCertificateForm.value.TitleDate = TitleDate
          this.updateTemplateCertificateForm.value.TitleDuration = TitleDuration
          this.updateTemplateCertificateForm.value.TitlePartner = TitlePartner
          this.updateTemplateCertificateForm.value.TitleInstitution = TitleInstitution
          this.updateTemplateCertificateForm.value.TitleLocation = TitleLocation
          if (this.PartnerType == '1' || this.PartnerType == '61') { this.updateTemplateCertificateForm.value.CertificateTypeId = 0 }
          else {
            this.updateTemplateCertificateForm.value.CertificateTypeId = this.data[0].CertificateTypeId;

          }
          //Audit Log
        this.updateTemplateCertificateForm.value.cuid = this.useraccesdata.ContactName;
        this.updateTemplateCertificateForm.value.UserId = this.useraccesdata.UserId;
          //End Audit Log


          //convert object to json
          let data = JSON.stringify(this.updateTemplateCertificateForm.value);
          console.log(this.updateTemplateCertificateForm.value)
          // console.log(data)
          //post action
          this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
            .subscribe(res=>{
              console.log(res)
            });
          // this.service.httpClientPos(this._serviceUrl, data);

          setTimeout(() => {
            //redirect
            this.router.navigate(['/certificate/certificate-list-design']);
            this.loading = false;
          }, 1000);
        }
      }).catch(swal.noop);
    }
  }

  //success notification save template
  openSuccessSwalSaveTemplateCert() {
    swal({
      title: 'Success!',
      text: 'The Design Templates has been add!',
      type: 'success'
    }).catch(swal.noop);
  }

  CertDate: NgbDateStruct;

  updateMyDate(value) {
    $("#datelabel").html(value);
  }
  selectToday() {
    this.CertDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    $("#datelabel").html(this.parserFormatter.format(this.CertDate));
  }

  change_TEXT(value, id) {
    $("#" + id + "").html(value);

    if (id == 'yeartitle') {
      $("." + id + "").html(value);
    }
  }

  changebackground(value) {

    $('#content_background').attr('src', value);
  }

  Background_WIDTH(value, targetValue) {
    $('#' + targetValue + '').css({ "width": value })
  }
  Background_HEIGHT(value, targetValue) {
    $('#' + targetValue + '').css({ "height": value })
  }

  FontColor(value, targetValue) {
    $('#' + targetValue + '').css({ "color": value })
  }

  IfChecked(ID, targetValue) {
    switch (targetValue) {
      case 'content_background':
      var valuetitle = 'content_background';
      break;
      case 'textheader':
      var valuetitle = 'textheader';
      break;
      case 'textheaderdescription':
      var valuetitle = 'textheaderdescription';
      break;
      case 'textfooter':
      var valuetitle = 'textfooter';
      break;
      case 'certstudent':
      var valuetitle = 'titlecertstudent';
      var valuecontent = 'certstudent';
      break;
      case 'certeventlocation':
      var valuetitle = 'certeventlocation';
      var valuecontent = 'titlecerteventlocation';
      break;
      case 'certno':
      var valuetitle = 'titlecertno';
      var valuecontent = 'certno';
      break;
      case 'certevent':
      var valuetitle = 'titlecertevent';
      var valuecontent = 'certevent';
      break;
      case 'certdate':
      var valuetitle = 'titlecertdate';
      var valuecontent = 'certdate';

      break;
      case 'certpartner':
      var valuetitle = 'titlecertpartner';
      var valuecontent = 'certpartner';
      break;
      case 'certtitle':
      var valuetitle = 'titlecerttitle';
      var valuecontent = 'certtitle';
      break;
      case 'certproduct':
      var valuetitle = 'titlecertproduct';
      var valuecontent = 'certproduct';
      break;
      case 'certinstructor':
      var valuetitle = 'titlecertinstructor';
      var valuecontent = 'certinstructor';
      break;
      case 'certinstitution':
      var valuetitle = 'titlecertinstitution';
      var valuecontent = 'certinstitution';
      break;
      case 'certduration':
      var valuetitle = 'titlecertduration';
      var valuecontent = 'certduration';
      break;
      case 'content_logo':
      var valuetitle = 'content_logo';
      break
      case 'orglogo':
      var valuetitle = 'orglogo';
      break
      case 'yeartitle':
      var valuetitle = 'yeartitle';
      if ($('.' + ID + '').hasClass('checked')) {
        $('.' + valuetitle + '').css({ "visibility": "hidden" })
      }
      else {
        $('.' + valuetitle + '').css({ "visibility": "visible" })
      }
      break
    }


    if ($('.' + ID + '').hasClass('checked')) {
      $('.' + ID + '').removeAttr("checked")
      $('.' + ID + '').removeClass("checked")
      $('#' + valuetitle + '').css({ "visibility": "hidden" })
      $('#' + valuecontent + '').css({ "visibility": "hidden" })
    }
    else {
      $('.' + ID + '').addClass("checked")
      $('.' + ID + '').attr("checked", "true")
      $('#' + valuetitle + '').css({ "visibility": "visible" })
      $('#' + valuecontent + '').css({ "visibility": "visible" })
    }

  }
  TextBold(value, targetValue) {
    if ($('#' + value + '').hasClass('bold')) {
      $('#' + value + '').removeClass('bold')
      $('#' + targetValue + '').css({ "font-weight": "normal" })
    } else {
      $('#' + value + '').addClass('bold')
      $('#' + targetValue + '').css({ "font-weight": "bold" })
    }
  }

  TextItalic(value, targetValue) {
    if ($('#' + value + '').hasClass('italic')) {
      $('#' + value + '').removeClass('italic')
      $('#' + targetValue + '').css({ "font-style": "normal" })
    } else {
      $('#' + value + '').addClass('italic')
      $('#' + targetValue + '').css({ "font-style": "italic" })
    }
  }

  TextUnderline(value, targetValue) {
    if ($('#' + value + '').hasClass('underline')) {
      $('#' + value + '').removeClass('underline')
      $('#' + targetValue + '').css({ "text-decoration": "none" })
    } else {
      $('#' + value + '').addClass('underline')
      $('#' + targetValue + '').css({ "text-decoration": "underline" })
    }
  }

  TextFontSize(value, targetValue) {
    if (value != "") {
      $('#' + targetValue + '').css({ "font-size": value + "px" })
    }
    else {
      $('#' + targetValue + '').css({ "font-size": "" })
    }
  }

  FormatDesignCeritificate(Key) {
    switch (Key) {
      case '1':
        //Course
        //Show Hide Div LOGO
        $('#DivLogo').css('display', 'none');
        $('#content_logo').css({ "visibility": "hidden" })
        $('#content_background').attr('src', '/assets/certificate/AAP-Course.png')
        this.DesignAAPATCCourse()
        break;
        case '2':
        //Project
        //Show Hide Div LOGO
        $('#DivLogo').css('display', "");
        this.DesignAAPProject()
        break;
        case '3':
        //Event
        //Show Hide Div LOGO
        $('#DivLogo').css('display', 'none');
        $('#content_logo').css({ "visibility": "hidden" })
        this.DesignAAPEvent()
        break;
        case '4':
        console.log('empat')
        break;

      }
    }


    DesignAAPATCCourse() {
      $('#textheader').css('transform', 'translate(44px, 251px)');
      $('#textheader').attr('data-x', '44');
      $('#textheader').attr('data-y', '251');
      $('#textheaderdescription').css({
        'min-height': '30px',
        'font-size': '12px',
        'width': '295px',
        'height': '55.9844px',
        'transform': 'translate(46px, 293px)'
      })
      $('#textheaderdescription').attr('data-x', '46');
      $('#textheaderdescription').attr('data-y', '293');

    //Text Footer
    $('#textfooter').css({
      'width': '781px',
      'height': '52px',
      'transform': 'translate(37px, 565px)'
    })
    $('#textfooter').attr('data-x', '37');
    $('#textfooter').attr('data-y', '565');

    //Certificate No
    $('#titlecertno').css({
      'transform': 'translate(45px, 460px)'
    })
    $('#titlecertno').attr('data-x', '45');
    $('#titlecertno').attr('data-y', '460');
    $('#certno').css({
      'transform': 'translate(162px, 460px)'
    })
    $('#certno').attr('data-x', '162');
    $('#certno').attr('data-y', '460');


    //Student
    $('#titlecertstudent').css('transform', 'translate(377.667px, 314px)')
    $('#titlecertstudent').attr('data-x', '377.667');
    $('#titlecertstudent').attr('data-y', '314');
    $('#certstudent').css('transform', 'translate(376.667px, 295.889px)')
    $('#certstudent').attr('data-x', '376.667');
    $('#certstudent').attr('data-y', '295.889');

    //Title
    $('#certtitle').css({ "visibility": "visible" })
    $('#titlecerttitle').css({ "visibility": "visible" })
    $('.switch_title span').css('background-color', 'rgb(26, 188, 156)')

    $('#titlecerttitle').css(
      'transform', 'translate(378px, 354px)'
      )
    $('#titlecerttitle').attr('data-x', '378');
    $('#titlecerttitle').attr('data-y', '354');
    $('#certtitle').css({
      'transform': 'translate(378px, 335px)'
    })
    $('#certtitle').attr('data-x', '378');
    $('#certtitle').attr('data-y', '335');

    //Instructor
    $('#titlecertinstructor').css({ "visibility": "visible" })
    $('#certinstructor').css({ "visibility": "visible" })
    $('.switch_certinstructor span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecertinstructor').css(
      'transform', 'translate(378px, 396px)'
      )
    $('#titlecertinstructor').attr('data-x', '378');
    $('#titlecertinstructor').attr('data-y', '396');
    $('#certinstructor').css({
      'transform': 'translate(378px, 375px)'
    })
    $('#certinstructor').attr('data-x', '378');
    $('#certinstructor').attr('data-y', '375');


    //Institution
    $('#titlecertinstitution').css({ "visibility": "hidden" })
    $('#certinstitution').css({ "visibility": "hidden" })
    $('.switch_certinstitution span').css('background-color', 'rgb(188, 26, 26)')

    //Partner Type
    $('#certpartner').css({
      'transform': 'translate(377px, 419px)'
    })
    $('#certpartner').attr('data-x', '377');
    $('#certpartner').attr('data-y', '419');
    $('#titlecertpartner').css({
      'transform': 'translate(379px, 438px)'
    })
    $('#titlecertpartner').attr('data-x', '379');
    $('#titlecertpartner').attr('data-y', '438');


    //Date
    $('#titlecertdate').css('transform', 'translate(575px, 397px)')
    $('#titlecertdate').attr('data-x', '575');
    $('#titlecertdate').attr('data-y', '397');
    $('#certdate').css('transform', 'translate(574px, 377px)')
    $('#certdate').attr('data-x', '574');
    $('#certdate').attr('data-y', '377');


    //Event
    $('#certevent').css({ "visibility": "hidden" })
    $('#titlecertevent').css({ "visibility": "hidden" })
    $('.switch_event  span').css('background-color', 'rgb(188, 26, 26)')

    //Event Location
    $('#certeventlocation').css({ "visibility": "hidden" })
    $('#titlecerteventlocation').css({ "visibility": "hidden" })
    $('.switch_certeventlocation span').css('background-color', 'rgb(188, 26, 26)')

    //Product
    $('#titlecertproduct').css('transform', 'translate(628.889px, 353.889px)')
    $('#titlecertproduct').attr('data-x', '629.889');
    $('#titlecertproduct').attr('data-y', '353.889');

    $('#certproduct').css('transform', 'translate(628.778px, 336.333px)')
    $('#certproduct').attr('data-x', '628.778');
    $('#certproduct').attr('data-y', '336.333');

    $('#certproduct').css({ "visibility": "visible" })
    $('#titlecertproduct').css({ "visibility": "visible" })
    $('.switch_certproduct span').css('background-color', 'rgb(26, 188, 156)')



    //Course Duration
    $('#titlecertduration').css({ "visibility": "visible" })
    $('#certduration').css({ "visibility": "visible" })
    $('.switch_certduration span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecertduration').css('transform', 'translate(695.444px, 395.111px)')
    $('#titlecertduration').attr('data-x', '695.444');
    $('#titlecertduration').attr('data-y', '395.111');
    $('#certduration').css('transform', 'translate(695.445px, 374.222px)')
    $('#certduration').attr('data-x', '695.445');
    $('#certduration').attr('data-y', '374.222');


    //Year
    $('#yeartitle').css('transform', 'translate(58px, 489px)')
    $('#yeartitle').attr('data-x', '58');
    $('#yeartitle').attr('data-y', '489');
  }

  DesignAAPProject() {
    this.ImgBackground = '/assets/certificate/AAP-Project.png';
    $('#textheader').css('transform', 'translate(249px, 103.778px)');
    $('#textheader').attr('data-x', '249');
    $('#textheader').attr('data-y', '103');
    $('#textheaderdescription').css({
      'min-height': '30px',
      'font-size': '14px',
      'width': '552.552px',
      'height': '56.9965px',
      'transform': 'translate(250px, 144.111px)'
    })
    $('#textheaderdescription').attr('data-x', '250');
    $('#textheaderdescription').attr('data-y', '114.111');

    $('#textfooter').css({
      'width': '540.99px',
      'height': '51.9966px',
      'transform': 'translate(277px, 565px)'
    })

    $('#textfooter').attr('data-x', '277');
    $('#textfooter').attr('data-y', '565');

    $('#titlecertno').css({
      'transform': 'translate(248.778px, 236.667px)'
    })
    $('#titlecertno').attr('data-x', '248.778');
    $('#titlecertno').attr('data-y', '236.667');


    $('#certno').css({
      'transform': 'translate(249px, 259.667px)'
    })
    $('#certno').attr('data-x', '249');
    $('#certno').attr('data-y', '236.667');


    $('#titlecertstudent').css('transform', 'translate(366.889px, 313px)')
    $('#titlecertstudent').attr('data-x', '366.889');
    $('#titlecertstudent').attr('data-y', '313');


    $('#certstudent').css('transform', 'translate(366.889px, 294.889px)')
    $('#certstudent').attr('data-x', '366.889');
    $('#certstudent').attr('data-y', '294.889');

    $('#titlecertdate').css('transform', 'translate(555.667px, 434px)')
    $('#titlecertdate').attr('data-x', '555.667');
    $('#titlecertdate').attr('data-y', '434');

    $('#certdate').css('transform', 'translate(554.556px, 413.111px)')
    $('#certdate').attr('data-x', '554.556');
    $('#certdate').attr('data-y', '413.111');

    
    $('#yeartitle').css('transform', 'translate(260.556px, 303.667px)')
    $('#yeartitle').attr('data-x', '260.556');
    $('#yeartitle').attr('data-y', '303.667');

    //Partner
    $('#titlecertpartner').css('transform', 'translate(366.889px, 477.111px)')
    $('#titlecertpartner').attr('data-x', '366.889');
    $('#titlecertpartner').attr('data-y', '477.111');
    $('#certpartner').css('transform', 'translate(366.889px, 458.889px)')
    $('#certpartner').attr('data-x', '366.889');
    $('#certpartner').attr('data-y', '458.889');

    //Instructor
    $('#titlecertinstructor').css({
      'transform': 'translate(365.111px, 435px)'
    })
    $('#titlecertinstructor').attr('data-x', '365.111');
    $('#titlecertinstructor').attr('data-y', '435');
    $('#certinstructor').css({
      'transform': 'translate(364.111px, 416.667px)'
    })
    $('#certinstructor').attr('data-x', '364.111');
    $('#certinstructor').attr('data-y', '416.667');

    //Title
    $('#titlecerttitle').css({
      'transform': 'translate(364.667px, 353px)'
    })
    $('#titlecerttitle').attr('data-x', '364.667');
    $('#titlecerttitle').attr('data-y', '353');
    $('#certtitle').css({
      'transform': 'translate(364.667px, 336px)'
    })
    $('#certtitle').attr('data-x', '364.667');
    $('#certtitle').attr('data-y', '336');

    //Event Location
    $('#certeventlocation').css({ "visibility": "hidden" })
    $('#titlecerteventlocation').css({ "visibility": "hidden" })
    $('.switch_certeventlocation span').css('background-color', 'rgb(188, 26, 26)')

    //Event
    $('#certevent').css({ "visibility": "hidden" })
    $('#titlecertevent').css({ "visibility": "hidden" })
    $('.switch_event  span').css('background-color', 'rgb(188, 26, 26)')
    //Title
    $('#certtitle').css({ "visibility": "visible" })
    $('#titlecerttitle').css({ "visibility": "visible" })
    $('.switch_title span').css('background-color', 'rgb(26, 188, 156)')

    //Product
    $('#titlecertproduct').css('transform', 'translate(627.889px, 352.889px)')
    $('#titlecertproduct').attr('data-x', '627.889');
    $('#titlecertproduct').attr('data-y', '352.889');

    $('#certproduct').css('transform', 'translate(627.889px, 335.333px')
    $('#certproduct').attr('data-x', '627.889');
    $('#certproduct').attr('data-y', '335.333');
    $('#certproduct').css({ "visibility": "visible" })
    $('#titlecertproduct').css({ "visibility": "visible" })
    $('.switch_certproduct span').css('background-color', 'rgb(26, 188, 156)')

    //Institution
    $('#titlecertinstitution').css({ "visibility": "visible" })
    $('#certinstitution').css({ "visibility": "visible" })
    $('.switch_certinstitution span').css('background-color', 'rgb(26, 188, 156)')
    $('#certinstitution').css('transform', 'translate(364.667px, 376.667px)')
    $('#certinstitution').attr('data-x', '364.667');
    $('#certinstitution').attr('data-y', '376.667');
    $('#titlecertinstitution').css('transform', 'translate(364.667px, 394.556px)')
    $('#titlecertinstitution').attr('data-x', '364.667');
    $('#titlecertinstitution').attr('data-y', '394.556');
    
    
    


    //Instructor
    $('#titlecertinstructor').css({ "visibility": "visible" })
    $('#certinstructor').css({ "visibility": "visible" })
    $('.switch_certinstructor span').css('background-color', 'rgb(26, 188, 156)')
    $('#certinstructor').css('transform', 'translate(364.667px, 416.667px)')
    $('#certinstructor').attr('data-x', '364.667');
    $('#certinstructor').attr('data-y', '416.667');
    $('#titlecertinstructor').css('transform', 'translate(364.667px, 436px)')
    $('#titlecertinstructor').attr('data-x', '364.667');
    $('#titlecertinstructor').attr('data-y', '436');
    

    
    //Course Duration
    $('#titlecertduration').css({ "visibility": "visible" })
    $('#certduration').css({ "visibility": "visible" })
    $('.switch_certduration span').css('background-color', 'rgb(26, 188, 156)')
  }

  DesignAAPEvent() {
    this.ImgBackground = '/assets/certificate/AAP-Event.png';


    $('#textheader').css('transform', 'translate(325px, 102px)')
    $('#textheader').attr('data-x', '325');
    $('#textheader').attr('data-y', '102');
    $('#textheaderdescription').css({
      'min-height': '30px',
      'font-size': '14px',
      'width': '467px',
      'height': '67px',
      'transform': 'translate(325px, 145px)'
    })
    $('#textheaderdescription').attr('data-x', '325');
    $('#textheaderdescription').attr('data-y', '145');

    $('#textfooter').css({
      'width': '781px',
      'height': '52px',
      'transform': 'translate(37px, 565px)'
    })

    $('#textfooter').attr('data-x', '37');
    $('#textfooter').attr('data-y', '565');


    $('#titlecertno').css({
      'transform': 'translate(328px, 228px)'
    })
    $('#titlecertno').attr('data-x', '328');
    $('#titlecertno').attr('data-y', '228');

    $('#certno').css({
      'transform': 'translate(448px, 229px)'
    })
    $('#certno').attr('data-x', '448');
    $('#certno').attr('data-y', '229');


    $('#titlecertstudent').css('transform', 'translate(328px, 295px)')
    $('#titlecertstudent').attr('data-x', '328');
    $('#titlecertstudent').attr('data-y', '295');

    


    $('#certstudent').css('transform', 'translate(328px, 276px)')
    $('#certstudent').attr('data-x', '328');
    $('#certstudent').attr('data-y', '276');

    $('#titlecertdate').css('transform', 'translate(629px, 374px)')
    $('#titlecertdate').attr('data-x', '629');
    $('#titlecertdate').attr('data-y', '374');

    $('#certdate').css('transform', 'translate(629px, 358px)')
    $('#certdate').attr('data-x', '629');
    $('#certdate').attr('data-y', '358');

    $('#titlecertpartner').css('transform', 'translate(328px, 417px)')
    $('#titlecertpartner').attr('data-x', '328');
    $('#titlecertpartner').attr('data-y', '417');

    $('#certpartner').css('transform', 'translate(328px, 397px)')
    $('#certpartner').attr('data-x', '328');
    $('#certpartner').attr('data-y', '397');


    $('#yeartitle').css('transform', 'translate(336px, 476px)')
    $('#yeartitle').attr('data-x', '336');
    $('#yeartitle').attr('data-y', '476');

    //Event Location
    $('#certeventlocation').css({ "visibility": "visible" })
    $('#titlecerteventlocation').css({ "visibility": "visible" })
    $('.switch_certeventlocation span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecerteventlocation').css('transform', 'translate(328px, 376px)')
    $('#titlecerteventlocation').attr('data-x', '376');
    $('#titlecerteventlocation').attr('data-y', '376');

    //Event
    $('#certevent').css({ "visibility": "visible" })
    $('#titlecertevent').css({ "visibility": "visible" })
    $('.switch_event  span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecertevent').css('transform', 'translate(328px, 335.852px)')
    $('#titlecertevent').attr('data-x', '328');
    $('#titlecertevent').attr('data-y', '335.852');



    //Course Title
    $('#certtitle').css({ "visibility": "hidden" })
    $('#titlecerttitle').css({ "visibility": "hidden" })
    $('.switch_title span').css('background-color', 'rgb(188, 26, 26)')


    //Product
    $('#certproduct').css({ "visibility": "hidden" })
    $('#titlecertproduct').css({ "visibility": "hidden" })
    $('.switch_certproduct span').css('background-color', 'rgb(188, 26, 26)')

    //Institution
    $('#titlecertinstitution').css({ "visibility": "hidden" })
    $('#certinstitution').css({ "visibility": "hidden" })
    $('.switch_certinstitution span').css('background-color', 'rgb(188, 26, 26)')

    //Instructor
    $('#titlecertinstructor').css({ "visibility": "hidden" })
    $('#certinstructor').css({ "visibility": "hidden" })
    $('.switch_certinstructor span').css('background-color', 'rgb(188, 26, 26)')


    //Course Duration
    $('#titlecertduration').css({ "visibility": "hidden" })
    $('#certduration').css({ "visibility": "hidden" })
    $('.switch_certduration span').css('background-color', 'rgb(188, 26, 26)')
  }

  DesignCTCCourse() {
    this.ImgBackground = '/assets/certificate/CTC-Course.png';
    $('#textheader').css('transform', 'translate(41px, 236px)');
    $('#textheader').attr('data-x', '41');
    $('#textheader').attr('data-y', '236');
    $('#textheaderdescription').css({
      'min-height': '30px',
      'font-size': '12px',
      'width': '735px',
      'height': '55.9833px',
      'transform': 'translate(43px, 273px)'
    })
    $('#textheaderdescription').attr('data-x', '43');
    $('#textheaderdescription').attr('data-y', '273');

    //Text Footer
    $('#textfooter').css({
      'width': '781px',
      'height': '52px',
      'transform': 'translate(37px, 565px)'
    })
    $('#textfooter').attr('data-x', '37');
    $('#textfooter').attr('data-y', '565');

    //Certificate No
    $('#titlecertno').css({
      'transform': 'translate(45px, 339px)'
    })
    $('#titlecertno').attr('data-x', '45');
    $('#titlecertno').attr('data-y', '339');
    $('#certno').css({
      'transform': 'translate(172px, 339px)'
    })
    $('#certno').attr('data-x', '172');
    $('#certno').attr('data-y', '339');


    //Student
    $('#titlecertstudent').css('transform', 'translate(45.667px, 396px)')
    $('#titlecertstudent').attr('data-x', '45.667');
    $('#titlecertstudent').attr('data-y', '396');
    $('#certstudent').css('transform', 'translate(45.667px, 375.889px)')
    $('#certstudent').attr('data-x', '45.667');
    $('#certstudent').attr('data-y', '375.889');

    //Title
    $('#certtitle').css({ "visibility": "visible" })
    $('#titlecerttitle').css({ "visibility": "visible" })
    $('.switch_title span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecerttitle').css({
      'transform': 'translate(45.667px, 437px)'
    })
    $('#titlecerttitle').attr('data-x', '45.667');
    $('#titlecerttitle').attr('data-y', '437');
    $('#certtitle').css({
      'transform': 'translate(45.667px, 419px)'
    })
    $('#certtitle').attr('data-x', '45.667');
    $('#certtitle').attr('data-y', '419');

    //Instructor
    $('#titlecertinstructor').css({ "visibility": "visible" })
    $('#certinstructor').css({ "visibility": "visible" })
    $('.switch_certinstructor span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecertinstructor').css({
      'transform': 'translate(45.667px, 479px)'
    })
    $('#titlecertinstructor').attr('data-x', '45.667');
    $('#titlecertinstructor').attr('data-y', '479');
    $('#certinstructor').css({
      'transform': 'translate(45.667px, 461px)'
    })
    $('#certinstructor').attr('data-x', '45.667');
    $('#certinstructor').attr('data-y', '461');


    //Institution
    $('#titlecertinstitution').css({ "visibility": "hidden" })
    $('#certinstitution').css({ "visibility": "hidden" })
    $('.switch_certinstitution span').css('background-color', 'rgb(188, 26, 26)')

    //Partner Type
    $('#certpartner').css({
      'transform': 'translate(45.667px, 502px)'
    })
    $('#certpartner').attr('data-x', '45.667');
    $('#certpartner').attr('data-y', '502');
    $('#titlecertpartner').css({
      'transform': 'translate(45.667px, 521px)'
    })
    $('#titlecertpartner').attr('data-x', '45.667');
    $('#titlecertpartner').attr('data-y', '521');


    //Date
    $('#titlecertdate').css('transform', 'translate(231px, 477px)')
    $('#titlecertdate').attr('data-x', '231');
    $('#titlecertdate').attr('data-y', '477');
    $('#certdate').css('transform', 'translate(231px, 461px)')
    $('#certdate').attr('data-x', '231');
    $('#certdate').attr('data-y', '461');


    //Event
    $('#certevent').css({ "visibility": "hidden" })
    $('#titlecertevent').css({ "visibility": "hidden" })
    $('.switch_event  span').css('background-color', 'rgb(188, 26, 26)')

    //Event Location
    $('#certeventlocation').css({ "visibility": "hidden" })
    $('#titlecerteventlocation').css({ "visibility": "hidden" })
    $('.switch_certeventlocation span').css('background-color', 'rgb(188, 26, 26)')

    //Product
    $('#titlecertproduct').css('transform', 'translate(316.889px, 436.889px)')
    $('#titlecertproduct').attr('data-x', '316.889');
    $('#titlecertproduct').attr('data-y', '436.889');

    $('#certproduct').css('transform', 'translate(316.778px, 420.333px)')
    $('#certproduct').attr('data-x', '316.778');
    $('#certproduct').attr('data-y', '420.333');

    $('#certproduct').css({ "visibility": "visible" })
    $('#titlecertproduct').css({ "visibility": "visible" })
    $('.switch_certproduct span').css('background-color', 'rgb(26, 188, 156)')



    //Course Duration
    $('#titlecertduration').css({ "visibility": "visible" })
    $('#certduration').css({ "visibility": "visible" })
    $('.switch_certduration span').css('background-color', 'rgb(26, 188, 156)')
    $('#titlecertduration').css('transform', 'translate(363.444px, 479.111px)')
    $('#titlecertduration').attr('data-x', '376.444');
    $('#titlecertduration').attr('data-y', '479.111');
    $('#certduration').css('transform', 'translate(364.445px, 461.222px)')
    $('#certduration').attr('data-x', '364.445');
    $('#certduration').attr('data-y', '461.222');


    //Year
    $('#yeartitle').css('transform', 'translate(669px, 421px)')
    $('#yeartitle').attr('data-x', '669');
    $('#yeartitle').attr('data-y', '421');

  }
}
