import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

import * as Survey from "survey-angular";

Survey.Survey.cssType = "bootstrap";

var surveyJSON = 
{
  pages:[
    {
      name:"page1",
      elements:[
        {
          type:"text",
          name:"question2"
        },
        {
          type:"file",
          name:"question3",
          maxSize:0
        },
        {
          type:"panel",
          name:"panel1"
        },
        {
          type:"radiogroup",
          name:"question1",
          choices:[
            "item1",
            "item2",
            "item3"
          ]
        }
      ]
    }
  ]
}

function sendDataToServer(survey) {
    //send Ajax request to your web server.
    alert("The results are: " + survey);
}

@Component({
  selector: 'list-of-the-form-eva',
  templateUrl: './list-of-the-form-eva.component.html',
  styleUrls: [
    './list-of-the-form-eva.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ListOfTheFormEVAComponent implements OnInit {

 
  constructor() { }

  ngOnInit() {
    var survey = new Survey.Model(surveyJSON);
    survey.onComplete.add(sendDataToServer);
    Survey.SurveyNG.render("surveyElement", { model: survey });
  }

}
