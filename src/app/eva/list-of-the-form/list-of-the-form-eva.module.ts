import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListOfTheFormEVAComponent } from './list-of-the-form-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ListOfTheFormEVARoutes: Routes = [
  {
    path: '',
    component: ListOfTheFormEVAComponent,
    data: {
      breadcrumb: 'List Of The Form',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListOfTheFormEVARoutes),
    SharedModule
  ],
  declarations: [ListOfTheFormEVAComponent]
})
export class ListOfTheFormEVAModule { }
