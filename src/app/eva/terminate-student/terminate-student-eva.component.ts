import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

import { AppService } from 'app/shared/service/app.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import swal from 'sweetalert2';

@Pipe({ name: 'dataFilterStudent' })
export class DataFilterStudentPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.StudentID.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Firstname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Lastname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Company.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.City.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.countries_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Phone.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Email.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-terminate-student-eva',
    templateUrl: './terminate-student-eva.component.html',
    styleUrls: [
        './terminate-student-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class TerminateStudentEVAComponent implements OnInit {

    private _serviceUrl:String= 'api/Student';
    data=[];
    public student_data: any;
    public student_detail: any;
    messageError: string = '';
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "Firstname";
    public sortOrder: string = "asc";
    student_detail_form: FormGroup;
    // loading:Boolean=false;
    public useraccesdata:any;
    public approver:any;

    private loading = false;

    constructor(private service: AppService, private session:SessionService) { 
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
        this.getAllStudent();
    }

    // getDataByApprover(data){
    //     this.service.get('api/auth/GetApprover', '')
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //                 this.approver = null;
    //             }
    //             else {
    //                 this.approver = JSON.parse(result);
    //                 for(var h=0;h<this.approver.length;h++){
    //                     for(var i=0;i<data.length;i++){
    //                         if(this.useraccesdata.UserLevelId == this.approver[h].UserLevelId && data[i].Status == "P"+(h+1)){
    //                             this.data.push(
    //                                 {
    //                                     "StudentID":data[i].StudentID,
    //                                     "Firstname":data[i].Firstname,
    //                                     "Company":data[i].Company,
    //                                     "City":data[i].City,
    //                                     "countries_name":data[i].countries_name,
    //                                     "Phone":data[i].Phone,
    //                                     "Email":data[i].Email
    //                                 }
    //                             );
    //                         }
    //                     }
    //                 }
    //             }
    //         },
    //         error => {
    //             this.service.errorserver();
    //         });
    // }

    getAllStudent() {
        //get data student
        this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl+'/PendingTerminated', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.data=[];
                    this.loading = false; 
                }
                else {
                    this.data=[];
                    this.student_data = result;
                    this.student_data.Firstname = this.service.decoder(this.student_data.Firstname);
                    // this.getDataByApprover(this.student_data);
                    for(var i=0;i<this.student_data.length;i++){
                        if(this.useraccesdata.UserLevelId.indexOf("APPROVER") >= 0 && this.useraccesdata.UserLevelId.indexOf(this.student_data[i].Status.substr(1,1)) >= 0){
                            this.data.push(
                                {
                                    "StudentID":this.student_data[i].StudentID,
                                    "Firstname":this.student_data[i].Firstname,
                                    "Company":this.student_data[i].Company,
                                    "City":this.student_data[i].City,
                                    "countries_name":this.student_data[i].countries_name,
                                    "Phone":this.student_data[i].Phone,
                                    "Email":this.student_data[i].Email
                                }
                            );
                        }
                    }
                    this.loading = false; 
                }
            },
            error => {
                this.service.errorserver();
                this.data=[];
                this.loading = false; 
            });
    }

    openConfirmsSwal(id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, terminate it!'
        }).then(result => {
            if (result == true) {
                var index = this.data.findIndex(x => x.StudentID == id);
                var data ={
                    cuid:this.useraccesdata.ContactName,
                    UserId:this.useraccesdata.UserId
                }
                this.service.httpCLientPut(this._serviceUrl + '/TerminatedAdmin/'+id, data)
                    .subscribe(res=>{
                        console.log(res)
                    });
                this.data.splice(index,1);
            }
        }).catch(swal.noop);
    }
}
