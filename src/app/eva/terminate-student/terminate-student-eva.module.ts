import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerminateStudentEVAComponent } from './terminate-student-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AppService } from 'app/shared/service/app.service';

import { DataFilterStudentPipe } from './terminate-student-eva.component';
import { LoadingModule } from 'ngx-loading';

export const TerminateStudentEVARoutes: Routes = [
  {
    path: '',
    component: TerminateStudentEVAComponent,
    data: {
      breadcrumb: 'eva.manage_student_info.list_terminate_account',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerminateStudentEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [TerminateStudentEVAComponent, DataFilterStudentPipe],
  providers:[AppService]
})
export class TerminateStudentEVAModule { }

