import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';

@Pipe({ name: 'dataFilterSurveyTemplate' })
export class DataFilterSurveyTemplatePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.eva_code.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.certificate_type.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.language.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.eva_title.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.year.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

var surveyJSON = {
  "pages": [
    {
      "name": "page1"
    }
  ]
}

@Component({
  selector: 'app-survey-template-list-eva',
  templateUrl: './survey-template-list-eva.component.html',
  styleUrls: [
    './survey-template-list-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyTemplateListEVAComponent implements OnInit {

  evaluationData = [];
  private _serviceUrl = 'api/EvaluationQuestion';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "eva_title";
  public sortOrder: string = "asc";
  edittemplatedata: FormGroup;
  addtemplatedata: FormGroup;
  private loading = false;

  useraccessdata:any

  constructor(public session: SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate, private http: HttpClient) {

    //validation
    let EvaluationQuestionCode = new FormControl('', Validators.required);
    let CourseId = new FormControl('', Validators.required);

    this.addtemplatedata = new FormGroup({
      EvaluationQuestionCode: EvaluationQuestionCode,
      CourseId: CourseId
    });

    let EvaluationQuestionCode_update = new FormControl('', Validators.required);
    let CourseId_update = new FormControl('', Validators.required);

    this.edittemplatedata = new FormGroup({
      EvaluationQuestionCode: EvaluationQuestionCode_update,
      CourseId: CourseId_update
    });

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
  }

  getCertificateName(certificate_id) {
    var name: string;
    switch (certificate_id) {
      case '1':
        name = "Course";
        break;
      case '2':
        name = "Project";
        break;
      case '3':
        name = "Event";
        break;
      default:
        name = "ATC";
    }
    return name;
  }

  accesAddTemplateBtn: Boolean = true;
  accesUpdateTemplateBtn: Boolean = true;
  accesDeleteTemplateBtn: Boolean = true;
  accesAddEditFormTemplateBtn: Boolean = true;
  ngOnInit() {
    this.getDataIntoTable();

    this.accesAddTemplateBtn = this.session.checkAccessButton("evaluation/add-edit-templatedata");
    this.accesUpdateTemplateBtn = this.session.checkAccessButton("evaluation/update-templatedata");
    this.accesDeleteTemplateBtn = this.session.checkAccessButton("evaluation/delete-templatedata");
    this.accesAddEditFormTemplateBtn = this.session.checkAccessButton("evaluation/add-edit-form");
  }

  getLanguage(langID) {
    var lang: any;
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A','Key':'" + langID + "'}", lang)
      .subscribe(res => {
        lang = res;
        return lang[0].KeyValue;
      });
  }

  getDataIntoTable() {
    this.evaluationData = [];
    //get data action
    var data: any;
    this.service.httpClientGet("api/EvaluationQuestion", data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.evaluationData = [];
        }
        else {
          data = result;
          for (let j = 0; j < data.length; j++) {
            var lang: any;
            var capcus = {
              'eva_code': data[j].EvaluationQuestionCode,
              'certificate_type': this.getCertificateName(data[j].CertificateTypeId),
              'eva_title': data[j].CourseId,
              'year': data[j].Year,
              'eva_id': data[j].EvaluationQuestionId,
              'language': data[j].Language,
              'language_id': data[j].LanguageID
            };
            this.evaluationData.push(capcus);
          }
        }
      },
        error => {
          this.service.errorserver();
          this.data = '';
        });
  }

  //view detail
  // viewdetail(id) {
  //   var data = '';
  //   this.service.httpClientGet(this._serviceUrl+'/'+id, data)
  //   .subscribe(result => {
  //     if(result=="Not found"){
  //       this.datadetail = '';
  //     }
  //     else{
  //       this.datadetail = result;
  //     }
  //   },
  //   error => {
  //       this.service.errorserver();
  //       this.datadetail = '';
  //   });
  // }

  //delete confirm
  openConfirmsSwal(id,code) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(result => {
        if (result == true) {
          this.loading = true;
          var cuid = this.useraccessdata.ContactName
          var UserId = this.useraccessdata.UserId
    
          var data = ''
         
          this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId + '/' + code, data)
            .subscribe(result => {
              let tmpData : any = result;
              this.messageResult = tmpData;
              var resource = result;
              if (resource['code'] == '1') {
                setTimeout(() => {
                  this.router.navigate(['/evaluation/survey-template-list']);
                  this.getDataIntoTable();
                  this.loading = false;                  
                }, 1000);
              }
              else {
                swal(
                  'Information!',
                  "Delete Data Failed",
                  'error'
                );
                this.loading = false;
              }
            },
              error => {
                this.messageError = <any>error
                this.service.errorserver();
                this.loading = false;
              });
        }
      }).catch(swal.noop);
  }

  editsurvey(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.datadetail = '';
        }
        else {
          this.datadetail = result;
          this.buildform();
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
        });
  }

  buildform() {
    let EvaluationQuestionId = new FormControl(this.datadetail.EvaluationQuestionId);
    let EvaluationQuestionCode = new FormControl(this.datadetail.EvaluationQuestionCode, Validators.required);
    let CourseId = new FormControl(this.datadetail.CourseId, Validators.required);
    let CountryCode = new FormControl(this.datadetail.CountryCode);
    let EvaluationQuestionTemplate = new FormControl(this.datadetail.EvaluationQuestionTemplate);
    let EvaluationQuestionJson = new FormControl(this.datadetail.EvaluationQuestionJson);
    let CreatedDate = new FormControl(this.datadetail.CreatedDate);
    let CreatedBy = new FormControl(this.datadetail.CreatedBy);

    this.edittemplatedata = new FormGroup({
      EvaluationQuestionId: EvaluationQuestionId,
      EvaluationQuestionCode: EvaluationQuestionCode,
      CourseId: CourseId,
      CountryCode: CountryCode,
      EvaluationQuestionTemplate: EvaluationQuestionTemplate,
      EvaluationQuestionJson: EvaluationQuestionJson,
      CreatedDate: CreatedDate,
      CreatedBy: CreatedBy
    });
  }

  resetFormUpdate() {
    this.edittemplatedata.reset({
      'EvaluationQuestionCode': this.datadetail.EvaluationQuestionCode,
      'CourseId': this.datadetail.CourseId
    });
  }

  onSubmitEditSurveyTemplate() {
    this.edittemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.edittemplatedata.controls['CourseId'].markAsTouched();

    if (this.edittemplatedata.valid) {
      //convert object to json
      let data = JSON.stringify(this.edittemplatedata.value);

      //post action

      // var index = this.data.findIndex(x => x.EvaluationQuestionId == this.edittemplatedata.value.EvaluationQuestionId);
      // this.service.httpCLientPutModal(this._serviceUrl, this.datadetail.EvaluationQuestionId, data, index, this.data, this.edittemplatedata.value);
    
      this.service.httpCLientPut(this._serviceUrl +"/"+ this.datadetail.EvaluationQuestionId, data)
        .subscribe(result => {
          if (result['code'] == '1') {
              this.ngOnInit();
          }
          else {
            console.log("failed");
          }
        },
        error => {
          console.log("failed");
        });
    
    }
  }

  onSubmit() {

    this.addtemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.addtemplatedata.controls['CourseId'].markAsTouched();

    if (this.addtemplatedata.valid) {
      this.addtemplatedata.value.CountryCode = "SG";
      this.addtemplatedata.value.EvaluationQuestionTemplate = surveyJSON;
      this.addtemplatedata.value.EvaluationQuestionJson = surveyJSON;
      this.addtemplatedata.value.CreatedDate = this.formatdate.dateJStoYMD(new Date());
      this.addtemplatedata.value.CreatedBy = "admin";

      //convert object to json
      let data = JSON.stringify(this.addtemplatedata.value);

      /* autodesk plan 18 oct */

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
        .subscribe(res => {
          var result = res; 
          if (result['code'] == '1') {
            this.router.navigate(['/evaluation/add-edit-form', result['EvaluationQuestionId']]);
          }
          else {
            swal(
              'Information!',
              "Insert Data Failed",
              'error'
            );
          }
        },
        error => {
          this.service.errorserver();
        });

      /* end line autodesk plan 18 oct */

    }
  }

  resetForm() {
    this.addtemplatedata.reset();
  }

}
