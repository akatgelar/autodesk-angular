import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyTemplateListEVAComponent } from './survey-template-list-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { DataFilterSurveyTemplatePipe } from './survey-template-list-eva.component';
import { SessionService } from '../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const SurveyTemplateListEVARoutes: Routes = [
  {
    path: '',
    component: SurveyTemplateListEVAComponent,
    data:
      {
        breadcrumb: 'eva.manage_evaluation.list_survey_template',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyTemplateListEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [SurveyTemplateListEVAComponent, DataFilterSurveyTemplatePipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class SurveyTemplateListEVAModule { }
