import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import { AppService } from "../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';

@Pipe({ name: 'dataFilterCertificat' })
export class DataFilterCertificatPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.CertificateBackgroundId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-certificate-list-background-eva',
    templateUrl: './certificate-list-background-eva.component.html',
    styleUrls: [
        './certificate-list-background-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CertificateListBackgroundEVAComponent implements OnInit {
    private _serviceUrl = 'api/CertificateBackground';
    messageResult: string = '';
    messageError: string = '';
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "Name";
    public sortOrder: string = "asc";

    id: string;
    public Encode_data: any;
    public dataDetail: any;
    private loading = false;

    constructor(public session: SessionService, private service: AppService) { }

    accesAddCertificateBackgroundBtn: Boolean = true;
    accesViewCertificateBackgroundBtn: Boolean = true;
    accesDeleteCertificateBackgroundBtn: Boolean = true;
    ngOnInit() {
        this.accesAddCertificateBackgroundBtn = this.session.checkAccessButton("certificate/certificate-add-background");
        this.accesViewCertificateBackgroundBtn = this.session.checkAccessButton("certificate/certificate-view-background");
        this.accesDeleteCertificateBackgroundBtn = this.session.checkAccessButton("certificate/certificate-delete-background");

        this.loading = true;
        //get data action
        var data = '';
        this.service.httpClientGet(this._serviceUrl, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.data = null;
                    this.loading = false;
                }
                else {
                    this.data = result;
                    this.loading = false;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading = false;
                });

    }

    openConfirmsSwal(id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
                this.service.httpClientDelete(this._serviceUrl+'/'+id, this.data)
                    .subscribe(res=>{
                        console.log(res);
                        this.ngOnInit();
                    }) 
            }
        }).catch(swal.noop);
    }

    //view detail
    viewdetail(id) {
        this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.datadetail = '';
                    this.loading = false;
                }
                else {
                    this.datadetail = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.datadetail = '';
                    this.loading = false;
                });
    }
}