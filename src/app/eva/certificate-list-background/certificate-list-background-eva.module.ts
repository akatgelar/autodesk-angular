import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateListBackgroundEVAComponent } from './certificate-list-background-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { DataFilterCertificatPipe } from './certificate-list-background-eva.component';
import { SessionService } from '../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const CertificateListBackgroundEVARoutes: Routes = [
  {
    path: '',
    component: CertificateListBackgroundEVAComponent,
    data: {
      breadcrumb: 'eva.manage_certificate.list_background',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificateListBackgroundEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CertificateListBackgroundEVAComponent, DataFilterCertificatPipe],
  providers: [AppService, SessionService]
})
export class CertificateListBackgroundEVAModule { }