import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Input, Output, EventEmitter } from "@angular/core";
import * as c3 from 'c3';
declare const $: any;
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";

import * as SurveyEditor from "surveyjs-editor";

@Component({
  selector: 'app-add-edit-form-eva',
  templateUrl: './add-edit-form-eva.component.html',
  styleUrls: [
    './add-edit-form-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    '../../../../node_modules/surveyjs-editor/surveyeditor.css',
    '../../../../node_modules/survey-angular/survey.css',
    //'../../../../src/assets/css/bootstrap.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddEditFormEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public json: any;
  public EvaluationQuestionTemplate: any;
  public EvaluationQuestionId: string = '';
  public CourseId: string = '';
  public CountryCode: string = '';
  public CreatedDate: any;
  public CreatedBy: string = ''; //akan diupdate ambil dari session
  public Year: string = '';
  public PartnerType: string = '';
  public CertificateTypeId: string = '';
  private dataDariTabel;
  public selectedLang;
  public language;
  public success = false;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private formatdate: AppFormatDate) { }

  id: string;
  code: string;
  currentLang: string;
  editor: SurveyEditor.SurveyEditor;

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.code = this.route.snapshot.params['code'];

    //Get available survey language in DB
    this.availableLang(this.id);

    this.json = {};

    //decalarasi survey js
    let editorOptions = {
      // show the embeded survey tab. It is hidden by default
      showEmbededSurveyTab: false,
      // hide the test survey tab. It is shown by default
      showTestSurveyTab: true,
      // hide the JSON text editor tab. It is shown by default
      showJSONEditorTab: true,
      // show the "Options" button menu. It is hidden by default
      showOptions: false
    };

    this.editor = new SurveyEditor.SurveyEditor('surveyEditorContainer', editorOptions);
    this.editor.onElementAllowOperations.add(function (sender, options){
      var obj = options.obj;
      if (!obj || !obj.page) return;
      if(obj.getType() == 'rating'){
        console.log(obj)
        obj.rateMax = 10
      }
    })
    if (this.id != null) {
      this.editor.saveSurveyFunc = this.updateSurvey;
    }
    else {
      this.editor.saveSurveyFunc = this.saveSurvey;
    }
    //end of decalarasi survey js
    var dataSurvey: any;
    //get data according to id
    this.service.httpClientGet(this._serviceUrl + "/GetAll/" + this.id, dataSurvey)
      .subscribe(result => {
        // console.log(result)
        if (result == "Not found") {
          this.service.notfound();
          this.data = null;
        }
        else {
          this.data = result;
          this.selectedLang = this.data.LanguageID;
          this.dataDariTabel = {
            'EvaluationQuestionId': this.data.EvaluationQuestionId,
            'EvaluationQuestionCode': this.data.EvaluationQuestionCode,
            'CourseId': this.data.CourseId,
            'CountryCode': this.data.CountryCode,
            'Year': this.data.Year,
            'PartnerType': this.data.PartnerType,
            'CertificateTypeId': this.data.CertificateTypeId,
            'CreatedBy': this.data.CreatedBy,
            'CreatedDate': this.data.CreatedDate
          };
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });

    //Dipisahin untuk jaga jaga kalau gagal parsing JSON (solusi coba pakai replace string)
    var template: any;
    this.service.httpClientGet(this._serviceUrl + "/GetTemplate/" + this.id, template)
      .subscribe(res => {
        template = res;
        // template = template.replace(/\r\n/g, " ");
        // template = template.replace(/\n/g, " ");
        // template = JSON.parse(template);

        this.json = JSON.stringify(template.EvaluationQuestionTemplate);
        this.editor.text = this.json;
      }, error => {
        this.service.errorserver();
      });
  }

  //Untuk ngeganti survey sesuai bahasa yang dipilih
  changeSurvey(value) {
    if (this.language.length > 0) {
      for (let i = 0; i < this.language.length; i++) {
        if (this.language[i].Key == value) {
          this.json = JSON.stringify(this.language[i].EvaluationQuestionTemplate);
          this.editor.text = this.json;
        }
      }
    }
  }

  //Untuk ngambil bahasa apa saja yang tersedia untuk survey yang ditampilkan
  availableLang(code) {
    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    var lang: any;
    this.service.httpClientGet(this._serviceUrl + "/AvailableLang/" + code, lang)
      .subscribe(res => {
        // console.log(res)
        lang = res;
        lang.length > 0 ? this.language = lang.sort(compare) : this.language = "";
      }, error => {
        this.language = "";
      });
  }

  updateSurvey = () => {
    if (this.selectedLang != "") {
      let dataUpdate = JSON.stringify({
        EvaluationQuestionTemplate: this.editor.text.replace(/\'/g, "\\'"),
        EvaluationQuestionJson: this.editor.text.replace(/\'/g, "\\'"),
        LanguageID: this.selectedLang
      })

      this.service.httpCLientPut(this._serviceUrl + "/UpdateTemplate/"+this.dataDariTabel.EvaluationQuestionId, dataUpdate)
        .subscribe(res=>{
          console.log(res)
        });
        
      this.success = true;

      window.scrollTo(0, 0);
      setTimeout(() => {
        this.success = false;
      }, 3000)
    }
  }

  saveSurvey = () => {
    console.log("save data =>" + JSON.stringify(this.editor.text));
  }

  //view detail
  viewdetail(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        this.datadetail = result;
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
  }

  //delete confirm
  openConfirmsSwal(id) {
    var index = this.data.findIndex(x => x.EvaluationQuestionId == id);
    //this.service.httpClientDelete(this._serviceUrl, this.data, id, index)
    this.service.httpClientDelete(this._serviceUrl+'/'+id, this.data)
  }

}
