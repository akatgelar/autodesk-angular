import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditFormEVAComponent } from './add-edit-form-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
//import { SurveyEditorComponent } from './survey.editor.component';

export const AddEditFormEVARoutes: Routes = [
  {
    path: '',
    component: AddEditFormEVAComponent,
    data: {
      breadcrumb: 'eva.manage_evaluation.add_edit_form',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddEditFormEVARoutes),
    SharedModule
  ],
  declarations: [
    AddEditFormEVAComponent
  ],
  providers:[AppService,AppFormatDate]
})
export class AddEditFormEVAModule { }
