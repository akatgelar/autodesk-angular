import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import { FormGroup, FormControl, Validators } from "@angular/forms";
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {AppService} from "../../shared/service/app.service";

import * as Survey from "survey-angular";

import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
var surveyJSON ={};
var datadetail: any;

function sendDataToServer(survey) {}

@Component({
  selector: 'survey-template-copy-detail-eva',
  templateUrl: './survey-template-copy-detail-eva.component.html',
  styleUrls: [
    './survey-template-copy-detail-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    '../../../../node_modules/survey-angular/survey.css',
    //'../../../../src/assets/css/bootstrap.css',
    ],
  encapsulation: ViewEncapsulation.None
})


export class SurveyTemplateCopyDetailEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  public data: any;
  public datadetail: any;
  public json:any;
  id:string;
  addtemplatedata: FormGroup;
  private dropdownCertificate;
  year;
  public ValueForEdit:any;

  Title_Survey:any;
  ValueForEditPages:any

  constructor(private service:AppService, private route:ActivatedRoute, private router: Router, private http: HttpClient, private formatdate: AppFormatDate,){
     //validation
     let EvaluationQuestionCode = new FormControl('', Validators.required);
     let CourseId = new FormControl('', Validators.required);
     let Year = new FormControl('', Validators.required);
     let CertificateType = new FormControl('', Validators.required);
 
     this.addtemplatedata = new FormGroup({
       EvaluationQuestionCode: EvaluationQuestionCode,
       CourseId: CourseId,
       Year: Year,
       CertificateType: CertificateType
     });

   }

   getCertificateType() {
    var certificate: any;
    var parent = "AAPCertificateType";
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "'}", certificate)
      .subscribe(result => {
        this.dropdownCertificate = result;
      }, error => {
        this.service.errorserver();
      });
  }

  getFY() {
    var parent = "FYIndicator";
    var data = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
      .subscribe(res => {
        this.year = res;
      }, error => {
        this.service.errorserver();
      });
  }

  ngOnInit() {
    this.getFY();
    this.getCertificateType();


    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.json={};

    //get data according to id
    this.service.httpClientGet(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        if(result=="Not found"){
            this.service.notfound();
            this.data = '';
        }
        else{
          this.data = result;
          surveyJSON = JSON.stringify(this.data.EvaluationQuestionTemplate);
          Survey.Survey.cssType = "bootstrap";
          var survey = new Survey.Model(surveyJSON);
          survey.onComplete.add(sendDataToServer);
          Survey.SurveyNG.render("surveyElement", { model: survey });

          let ValueForEdit = this.data.EvaluationQuestionJson;
          this.ValueForEditPages = this.data.EvaluationQuestionJson.pages;
          this.Title_Survey = this.data.EvaluationQuestionJson.title
          console.log(this.data.EvaluationQuestionJson);
          console.log(this.data.EvaluationQuestionJson.pages);

        }
      },
      error => {
          this.service.errorserver();
          this.datadetail = '';
      });

    //get data according to id
    this.service.httpClientGet('api/EvaluationQuestion/'+this.id, data)
      .subscribe(result => {
        if(result=="Not found"){
          this.datadetail = '';
        }
        else{
          this.datadetail = result;
        }
      },
      error => {
          this.service.errorserver();
          this.datadetail = '';
      });
  }

  //submit form
  submitted: boolean;
  onSubmit() {

    this.addtemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.addtemplatedata.controls['CourseId'].markAsTouched();
    this.addtemplatedata.controls['Year'].markAsTouched();
    this.addtemplatedata.controls['CertificateType'].markAsTouched();

    this.submitted = true;

    if (this.addtemplatedata.valid) {
      this.addtemplatedata.value.CountryCode = "SG";
      this.addtemplatedata.value.EvaluationQuestionTemplate = surveyJSON;
      this.addtemplatedata.value.EvaluationQuestionJson = surveyJSON;
      this.addtemplatedata.value.CreatedDate = this.formatdate.dateJStoYMD(new Date());
      this.addtemplatedata.value.CreatedBy = "admin";

      //convert object to json
      let data = JSON.stringify(this.addtemplatedata.value);

      /* autodesk plan 18 oct */

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
        .subscribe(res => {
          var result = res; 
          if (result['code'] == '1') {
            this.router.navigate(['/evaluation/add-edit-form', result['EvaluationQuestionId']]);
          }
          else {
            swal(
              'Information!',
              "Insert Data Failed",
              'error'
            );
          }
        },
        error => {
          this.service.errorserver();
        });

      /* autodesk plan 18 oct */

    }
  }

}
