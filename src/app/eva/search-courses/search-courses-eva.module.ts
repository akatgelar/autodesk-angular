import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchCourseEVAComponent } from './search-courses-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const SearchCourseEVARoutes: Routes = [
    {
      path: '',
      component: SearchCourseEVAComponent,
      data: {
        breadcrumb: 'Course Management Tool / Search Courses',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SearchCourseEVARoutes),
        SharedModule,
        AngularMultiSelectModule
    ],
    declarations: [SearchCourseEVAComponent]
})
export class SearchCourseEVAModule { }