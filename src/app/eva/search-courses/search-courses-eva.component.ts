import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
    selector: 'app-search-courses-eva',
    templateUrl: './search-courses-eva.component.html',
    styleUrls: [
        './search-courses-eva.component.css',
        '../../../../node_modules/c3/c3.min.css', 
        ], 
    encapsulation: ViewEncapsulation.None
})

export class SearchCourseEVAComponent implements OnInit {
    
    dropdownListPartner = [];
    selectedItemsPartner = [];
    dropdownSettingsPartner = {};

    dropdownListCertificate = [];
    selectedItemsCertificate = [];
    dropdownSettingsCertificate = {};

    public data: any;
    public partnertype: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";

    constructor(public http: Http) { }

    ngOnInit() {
        this.http.get(`assets/data/activities.json`)
        .subscribe((data)=> {
            this.data = data.json();
        });

        // Partner
        this.dropdownListPartner = [
            {"id":1,"itemName":"ATC"},
            {"id":2,"itemName":"AAP"}
        ];

        this.selectedItemsPartner = [];
        this.dropdownSettingsPartner = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Certificate
        this.dropdownListCertificate = [
            {"id":1,"itemName":"Course"},
            {"id":2,"itemName":"Project"},
            {"id":3,"itemName":"Event"}
          ];
        this.selectedItemsCertificate = [];
        this.dropdownSettingsCertificate = { 
                singleSelection: false, 
                text:"Please Select",
                selectAllText:'Select All',
                unSelectAllText:'UnSelect All',
                enableSearchFilter: true,
                classes:"myclass custom-class"
        };
    }
    onItemSelect(item:any){
        //console.log(item);
       // console.log(this.selectedItemsPartner);
       // console.log(this.selectedItemsCertificate);
    }
    OnItemDeSelect(item:any){
      //  console.log(item);
       // console.log(this.selectedItemsPartner);
       // console.log(this.selectedItemsCertificate);
    }
    onSiteSelectAll(items: any){
      //  console.log(items);
    }
    onSiteDeSelectAll(items: any){
        //console.log(items);
    }
}