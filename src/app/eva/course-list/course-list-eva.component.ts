import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { DatePipe } from '@angular/common';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import * as XLSX from 'xlsx';

const now = new Date();

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
    }
}

@Pipe({ name: 'dataFilterCourse' })
export class DataFilterCoursePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.CourseId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.CourseTitle.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-course-list-eva',
    templateUrl: './course-list-eva.component.html',
    styleUrls: [
        './course-list-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CourseListEVAComponent implements OnInit {

    dropdownPartner = [];
    selectedPartner = [];

    dropdownCertificate = [];
    selectedCertificate = [];

    dropdownSettings = {};

    private _serviceUrl = 'api/MainCourses';
    messageResult: string = '';
    messageError: string = '';
    private data;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "CourseTitle";
    public sortOrder: string = "asc";
    // public activePage = 1;
    // public itemsTotal = 0;
    public foundCourse = false;
    teachingLevel = [];
    survey: string = "";
    product: string = "";
    version: any;
    public today: Date = new Date();
    kursus = [];
    public fyIndicator;
    public pilihanSort;
    public CourseId;
    SearchCourse: FormGroup;
    public valid = true;
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    public loading = false;
    public loading_1 = false;
    private useraccessdata;
    // public showLink = false;

    //test
    query_current: string = "";
    kursustmp: any;
    public getServerData(event) {
        this.loading_1 = true;
        this.kursustmp = this.kursus;
        var limit = 10;
        var ofset = (event - 1) * 10;
        var endpage = Math.floor(this.countdata / 10) + 1;

        if (event == 1) {
            ofset = 0;
        } else if (event == endpage) {
            limit = this.countdata % 10;
        }

        this.kursus = null;

        this.dataFilter.Limit = limit;
        this.dataFilter.Offset = ofset;
		//this.dataFilter.Version=this.useraccessdata.Version;
  
        var data:any;

        // this.service.get(this.query_current+'/'+limit+'/'+ofset, "")
        //     .subscribe(res => {
        //         data = res;
        //         data = JSON.parse(data);
        //         if (data.length > 0) {
        //             this.kursus = data;
        //             this.loading_1=false;
        //         } else {
        //             this.kursus = [];
        //             this.loading_1=false;
        //         }
        //         this.foundCourse = true;
        //         this.loading_1=false;
        //     });

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost(
            "api/Courses/FilterCourse",
            this.dataFilter
        )
            .subscribe(result => {
                data = result;
               // data = JSON.parse(data);
                if (data.length > 0) {
                    this.kursus = data;
                    this.loading_1 = false;
                } else {
                    this.kursus = [];
                    this.loading_1 = false;
                }
                this.foundCourse = true;
                this.loading_1 = false;
            });
        /* new post for autodesk plan 17 oct */
        return event;
    }

    constructor(private _http: Http, public session: SessionService, private service: AppService, private formatdate: AppFormatDate, private router: Router,
        private datePipe: DatePipe, private route: ActivatedRoute, private parserFormatter: NgbDateParserFormatter) {

        // this.modelPopup1 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
        
        //ini untuk nampilin button copy sama delete course
        // let current_user = this.useraccessdata.UserLevelId.split(",");
        // if (current_user.indexOf("SUPERADMIN") > -1 == true || current_user.indexOf("DISTRIBUTOR") > -1 == true) {
        //     this.showLink = true;
        // } else {
        //     this.showLink = false;
        // }
        
        this.pilihanSort = [
            { id: "CourseId", itemName: "Course ID" },
            { id: "CourseTitle", itemName: "Course Title" },
            // { id: "SiteName", itemName: "Site Name" },
            { id: "(SELECT SiteName FROM Main_site s2 WHERE s2.SiteId = C.SiteId)", itemName: "Site Name" }, /* fixed issue 20092018 - Search course> sort by evaluation and site name takes time to fetch data loading screen for long time (Both for AAP and ATC) */
            // { id: "StartDate", itemName: "Start Date" },
            // { id: "CompletionDate", itemName: "End Date" },
            /* issue 27112018 - Sort direction (start date & asc) */
            { id: "DATE(StartDate)", itemName: "Start Date" },
            { id: "DATE(CompletionDate)", itemName: "End Date" },
            /* end line issue 27112018 - Sort direction (start date & asc) */
            // { id: "InstructorId", itemName: "Instructor" },
            { id: "(SELECT InstructorId FROM Contacts_All c2 WHERE c2.ContactId = C.ContactId)", itemName: "Instructor" }, /* fixed issue 20092018 - Search course> sort by evaluation and site name takes time to fetch data loading screen for long time (Both for AAP and ATC) */
            // { id: "Evals", itemName: "#Evals" },
            { id: "(SELECT count(*) FROM EvaluationAnswer ea WHERE ea.courseid = C.CourseId)", itemName: "#Evals" }, /* fixed issue 20092018 - Search course> sort by evaluation and site name takes time to fetch data loading screen for long time (Both for AAP and ATC) */
        ].sort((a,b)=>{
            if(a.itemName > b.itemName)
                return 1
            else if(a.itemName < b.itemName)
                return -1
            else return 0
        });

        this.getSurveyYear();

        let CourseId = new FormControl('');
        let OrganizationId = new FormControl('');
        let SiteId = new FormControl('');
        let InstructorId = new FormControl('');
        let CourseTitle = new FormControl('');
        let CourseStartDate = new FormControl('');
        let CourseEndDate = new FormControl('');
        let SurveyYear = new FormControl('');
        let SortValue = new FormControl('CourseId');
        let SortDirection = new FormControl('1');

        this.SearchCourse = new FormGroup({
            CourseId: CourseId,
            OrganizationId: OrganizationId,
            SiteId: SiteId,
            InstructorId: InstructorId,
            CourseTitle: CourseTitle,
            CourseStartDate: CourseStartDate,
            CourseEndDate: CourseEndDate,
            SurveyYear: SurveyYear,
            SortValue: SortValue,
            SortDirection: SortDirection
        });
    }

    accesAddCourseBtn: Boolean = true;
    accesUpdateCourseBtn: Boolean = true;
    accesConfirmStudentCourseBtn: Boolean = true;
    accesDeleteCourseBtn: Boolean = true;
    accesCopyCourseBtn: Boolean = true;
    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole();
        this.itsinstructor();
        this.itsOrganization();
        this.itsSite();
        this.itsDistributor();

        /* call function get user level id (issue31082018)*/

        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.selectedPartner = item.selectPartner;
            this.dropdownPartner = item.dropPartner;
            this.selectedCertificate = item.selectCerti;
            this.dropdownCertificate = item.dropCerti;
            this.SearchCourse.patchValue({ CourseId: item.idCourse });
            this.SearchCourse.patchValue({ OrganizationId: item.idOrg });
            this.SearchCourse.patchValue({ SiteId: item.idSite });
            this.SearchCourse.patchValue({ InstructorId: item.idInstructor });
            this.SearchCourse.patchValue({ CourseTitle: item.titleCourse });

            if (item.courseStart != undefined) {
                //pakai type of cuma untuk ngebaca ketika habis dari add course, else itu dipake kalau si user nyari langsung di search course page nya
                if (typeof (item.courseStart) == 'string') {
                    var startdate = item.courseStart.split("-");
                    this.modelPopup1 = {
                        "year": parseInt(startdate[0]),
                        "month": parseInt(startdate[1]),
                        "day": parseInt(startdate[2])
                    };
                } else {
                    this.modelPopup1 = {
                        "year": item.courseStart.year,
                        "month": item.courseStart.month,
                        "day": item.courseStart.day
                    };
                }
                this.SearchCourse.patchValue({ CourseStartDate: this.modelPopup1 });
            }

            if (item.courseEnd != undefined) {
                if (typeof (item.courseStart) == 'string') {
                    var enddate = item.courseEnd.split("-");
                    this.modelPopup2 = {
                        "year": parseInt(enddate[0]),
                        "month": parseInt(enddate[1]),
                        "day": parseInt(enddate[2])
                    };
                } else {
                    this.modelPopup2 = {
                        "year": item.courseEnd.year,
                        "month": item.courseEnd.month,
                        "day": item.courseEnd.day
                    };
                }
                this.SearchCourse.patchValue({ CourseEndDate: this.modelPopup2 });
            }

            this.SearchCourse.patchValue({ SurveyYear: item.year });
            this.SearchCourse.patchValue({ SortValue: item.sortV });
            if (item.sortD == "ASC") {
                item.sortD = "1";
            }
            this.SearchCourse.patchValue({ SortDirection: item.sortD });
            this.onSubmit();
        }
        else {
            this.selectedPartner = [];
            this.getPartnerType();
        }

        this.accesAddCourseBtn = this.session.checkAccessButton("course/course-add");
        this.accesUpdateCourseBtn = this.session.checkAccessButton("course/course-update");
        this.accesDeleteCourseBtn = this.session.checkAccessButton("course/course-delete");
        this.accesCopyCourseBtn = this.session.checkAccessButton("course/course-copy");
        this.accesConfirmStudentCourseBtn = this.session.checkAccessButton("studentinfo/confirm-final-student");

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120
        };
    }

    validEdit(item){
        var currentDate = new Date();
        var courseEndDate = new Date(item.CompletionDate);
        let currentTime = currentDate.getTime();
        let endTime = courseEndDate.getTime() + (24*60*60*1000);
        if (currentTime > endTime)
            return false
        else return true
    }

    getSurveyYear() {
        //Descending
        function compare(a, b) {
            const valueA = a.Key;
            const valueB = b.Key;

            let comparison = 0;
            if (valueA < valueB) {
                comparison = 1;
            } else if (valueA > valueB) {
                comparison = -1;
            }
            return comparison;
        }

        let year: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Status':'A'}", year)
            .subscribe(res => {
                year = res;
                this.fyIndicator = year.sort(compare);
            });
    }
    getPartnerType() {
        this.loading = true;
        let partner: any;
        this.service.httpClientGet("api/Roles/CoursePartner", partner)
            .subscribe(res => {
                partner = res;
                if (partner.length > 0) {
                    this.dropdownPartner = partner.map((item) => {
                        return {
                            id: item.RoleCode,
                            itemName: item.RoleName
                        }
                    });
                    this.loading = false;
                }
            });
        this.selectedPartner = [];
    }

    certificateDropdown() {
        this.loading = true;
        this.dropdownCertificate = [];
        //Descending
        function compare(a, b) {
            const valueA = a.Key;
            const valueB = b.Key;

            let comparison = 0;
            if (valueA < valueB) {
                comparison = 1;
            } else if (valueA > valueB) {
                comparison = -1;
            }
            return comparison;
        }

        let obj = this.selectedPartner.find(o => o.id === 'AAP');
        if (obj != undefined && obj.id == 'AAP') {
            let cert: any;
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Status':'A'}", cert)
                .subscribe(res => {
                    cert = res;
                    cert = cert.sort(compare);
                    this.dropdownCertificate = cert.sort((a,b)=>{
                        if(a.KeyValue > b.KeyValue)
                            return 1
                        else if(a.KeyValue < b.KeyValue)
                            return -1
                        else return 0
                    }).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.selectedCertificate = cert.map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.loading = false;
                });
        } else {
            this.dropdownCertificate = [];
            this.loading = false;
        }
        this.selectedCertificate = [];
    }

    onPartnerSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.valid = true;
    }


    OnPartnerDeSelect(item: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.foundCourse = false;
    }

    onPartnerSelectAll(items: any) {
        this.selectedCertificate = [];
        this.certificateDropdown();
        this.valid = true;
    }

    onPartnerDeSelectAll(items: any) {
        this.selectedCertificate = [];
        this.dropdownCertificate = [];
        this.certificateDropdown();
        this.foundCourse = false;
    }

    onCertificateSelect(item: any) { }
    OnCertificateDeSelect(item: any) {
        this.foundCourse = false;
    }
    onCertificateSelectAll(item: any) { }
    onCertificateDeSelectAll(item: any) {
        this.foundCourse = false;
    }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                this.datadetail = result;
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });
    }

    // getSVP(training, project, event) {
    //     var type: any;
    //     if (training != null && training != '') {
    //         type = "Course (AAP)";
    //     } else if (project != null && project != '') {
    //         type = "Project (AAP)";
    //     } else if (event != null && event != '') {
    //         type = "Event (AAP)";
    //     } else {
    //         type = "(ATC)";
    //     }
    //     return type;
    // }

    // lemparData(data) {
    //     this.kursus = [];
    //     if (data.length > 0) {
    //         for (let k = 0; k < data.length; k++) {
    //             var data_kursus = {
    //                 'course_id': data[k].CourseId,
    //                 'course_title': data[k].NAME,
    //                 'sitename': data[k].SiteName,
    //                 'svp': this.getSVP(data[k].TrainingTypeKey, data[k].ProjectTypeKey, data[k].EventTypeKey),
    //                 'startdate': data[k].StartDate,
    //                 'enddate': data[k].CompletionDate,
    //                 'instructor': data[k].ContactName,
    //                 'instructorid': data[k].InstructorId,
    //             };
    //             this.kursus.push(data_kursus);
    //         }
    //     } else {
    //         this.data = [];
    //     }
    //     this.foundCourse = true;
    // }

    // getATC_Course() {
    //     var data: any;
    //     this.service.httpClientGet("api/Courses/ATC_Course", data)
    //         .subscribe(result => {
    //             data = result;
    //             this.lemparData(data);
    //         }, error => {
    //             this.messageError = <any>error
    //             this.service.errorserver();
    //         });
    // }

    // getCourseByCertificateType() {
    //     var training: any = '';
    //     var project: any = '';
    //     var event: any = '';
    //     var data: any;
    //     if (this.selectedCertificate.length == 1) {
    //         if (this.selectedCertificate[0].id == 1) {
    //             training = 'true';
    //         } else if (this.selectedCertificate[0].id == 2) {
    //             project = 'true';
    //         } else {
    //             event = 'true';
    //         }
    //         this.service.get("api/Courses/where/{'Training': '" + training + "','Project':'" + project + "','Event':'" + event + "'}", data)
    //             .subscribe(res => {
    //                 data = JSON.parse(res);
    //                 this.lemparData(data);
    //             }, error => {
    //                 this.messageError = <any>error
    //                 this.service.errorserver();
    //             });
    //     } else {
    //         training = 'true';
    //         project = 'true';
    //         event = 'true';
    //         this.service.get("api/Courses/where/{'Training': '" + training + "','Project':'" + project + "','Event':'" + event + "'}", data)
    //             .subscribe(res => {
    //                 data = JSON.parse(res);
    //                 this.lemparData(data);
    //             }, error => {
    //                 this.messageError = <any>error
    //                 this.service.errorserver();
    //             });
    //     }
    // }

    // getFilterCourse() {
    //     var training: any = '';
    //     var project: any = '';
    //     var event: any = '';
    //     var data: any;
    //     let CourseWithCertificate = [];
    //     if (this.selectedCertificate.length > 0) {
    //         for (let i = 0; i < this.selectedCertificate.length; i++) {
    //             if (this.selectedCertificate[i].id == 1) {
    //                 training = 'true';
    //             } else if (this.selectedCertificate[i].id == 2) {
    //                 project = 'true';
    //             } else {
    //                 event = 'true';
    //             }
    //         }
    //         this.service.get("api/Courses/where/{'Training': '" + training + "','Project':'" + project + "','Event':'" + event + "'}", data)
    //             .subscribe(res => {
    //                 data = JSON.parse(res);
    //                 for (let k = 0; k < data.length; k++) {
    //                     var data_kursus = {
    //                         'CourseId': data[k].CourseId,
    //                         'NAME': data[k].NAME,
    //                         'SiteName': data[k].SiteName,
    //                         'TrainingTypeKey': data[k].TrainingTypeKey,
    //                         'ProjectTypeKey': data[k].ProjectTypeKey,
    //                         'EventTypeKey': data[k].EventTypeKey,
    //                         'StartDate': data[k].StartDate,
    //                         'CompletionDate': data[k].CompletionDate,
    //                         'ContactName': data[k].ContactName,
    //                     };
    //                     CourseWithCertificate.push(data_kursus);
    //                 }
    //                 this.service.get("api/Courses/ATC_Course", data)
    //                     .subscribe(res => {
    //                         data = JSON.parse(res);
    //                         for (let k = 0; k < data.length; k++) {
    //                             var data_kursus = {
    //                                 'CourseId': data[k].CourseId,
    //                                 'NAME': data[k].NAME,
    //                                 'SiteName': data[k].SiteName,
    //                                 'TrainingTypeKey': data[k].TrainingTypeKey,
    //                                 'ProjectTypeKey': data[k].ProjectTypeKey,
    //                                 'EventTypeKey': data[k].EventTypeKey,
    //                                 'StartDate': data[k].StartDate,
    //                                 'CompletionDate': data[k].CompletionDate,
    //                                 'ContactName': data[k].ContactName,
    //                             };
    //                             CourseWithCertificate.push(data_kursus);
    //                         }
    //                         this.lemparData(CourseWithCertificate);
    //                     }, error => {
    //                         this.messageError = <any>error
    //                         this.service.errorserver();
    //                     })
    //             }, error => {
    //                 this.messageError = <any>error
    //                 this.service.errorserver();
    //             });
    //     } else {
    //         this.service.get("api/Courses", data)
    //             .subscribe(res => {
    //                 data = JSON.parse(res);
    //                 this.lemparData(data);
    //             }, error => {
    //                 this.messageError = <any>error
    //                 this.service.errorserver();
    //             });
    //     }
    // }

    onSelectDateStart(date: NgbDateStruct) {
        if (date != null) {
            this.SearchCourse.value.CourseStartDate = date;
        }
    }

    onSelectDateEnd(date: NgbDateStruct) {
        if (date != null) {
            this.SearchCourse.value.CourseEndDate = date;
        }
    }

    // checkrole(): boolean {
    //     let userlvlarr = this.useraccessdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccessdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccessdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccessdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccessdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccessdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccessdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccessdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccessdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccessdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccessdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccessdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccessdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccessdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    countdata:number=0;
    page:number=0;
    dataFilter=
        {
            "SiteIdArr": "",
            "PartnerType": "",
            "CourseId": "",
            "SiteId": "",
            "CourseTitle": "",
            "CourseStartDate": "",
            "CourseEndDate": "",
            "SurveyYear": "",
            "InstructorId": "",
            "OrgId":"",
            "Distributor":"",
            "FilterOrgId":"",
            "FilterInst":"",
            "SortBy":"",
            "SortValue":"",
            "Limit":0,
            "Offset":0,
			"Version":""
        };
    onSubmit() {
        this.page = 1;
        
        if (this.selectedPartner.length > 0 || (this.SearchCourse.value.CourseId != null &&  this.SearchCourse.value.CourseId != "")) {
            this.loading = true;
            this.valid = true;
            var partnerType = [];
            var data: any;

            for (let i = 0; i < this.selectedPartner.length; i++) {
                if (this.selectedPartner[i].id === 'AAP') {
                    if (this.selectedCertificate.length > 0) {
                        for (let j = 0; j < this.selectedCertificate.length; j++) {
                            partnerType.push('"' + this.selectedPartner[i].id + " " + this.selectedCertificate[j].itemName + '"');
                        }
                    } else {
                        //Kalau pencarian hanya milih AAP saja tanpa memilih kategori AAP nya (Course,Project, atau Event)
                        partnerType.push('"AAP"');
                    }
                } else {
                    partnerType.push('"' + this.selectedPartner[i].id + '"');
                }
            }

            var sort = (this.SearchCourse.value.SortDirection == '1') ? 'ASC' : 'DESC';

            //saya set string null biar api address nya kebaca
            var orgId = "";
            var instructorId = "";
            var siteid = "";
            // let count: number = 0;
            var distributorkan = "";
            // var filterOrg = "";
            // var filterInst = "";

            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         instructorId = this.useraccessdata.InstructorId;
            //         orgId = "";
            //         siteid = "";
            //         // count += 1;
            //     } else {
            //         if (this.itsDistributor()) {
            //             orgId = "";
            //             siteid = "";
            //             instructorId = "";
            //             // count += 1;
            //             distributorkan = this.urlGetOrgId();
            //         } else {
            //             orgId = this.urlGetOrgId();
            //             siteid = "";
            //             instructorId = "";
            //             // count += 1;
            //         }
            //     }
            // }

            // /* new request nambah user role -__ */

            // if (this.checkrole()) {
            //     if(this.itsDistributor()){
            //         distributorkan = this.urlGetOrgId();
            //     }else{
            //         if(this.itsOrganization()){
            //             orgId = this.urlGetOrgId();
            //         }else{
            //             if(this.itsSite()){
            //                 siteid = this.urlGetSiteId();
            //             }else{
            //                 instructorId = this.useraccessdata.InstructorId;
            //             }
            //         }
            //     }
            // }
            
            // /* new request nambah user role -__ */

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            if (this.adminya) {
                if(this.distributorya){
                    distributorkan = this.urlGetOrgId();
                }else{
                    if(this.orgya){
                        orgId = this.urlGetOrgId();
                    }else{
                        if(this.siteya){
                            siteid = this.urlGetSiteId();
                        }else{
                            instructorId = this.useraccessdata.InstructorId;
                        }
                    }
                }
            }
            
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // let joinType: any;
            // // let arr = [this.SearchCourse.value.CourseId,this.SearchCourse.value.SiteId,this.SearchCourse.value.CourseTitle,this.SearchCourse.value.CourseStartDate,this.SearchCourse.value.CourseEndDate,this.SearchCourse.value.SurveyYear];
            // let arr = [this.SearchCourse.value.CourseId, this.SearchCourse.value.CourseTitle, this.SearchCourse.value.CourseStartDate, this.SearchCourse.value.CourseEndDate, this.SearchCourse.value.SurveyYear];
            // for (let y = 0; y < arr.length; y++) {
            //     if (arr[y] != "" && arr[y] != undefined) {
            //         count += 1;
            //     }
            // }

            // if (count > 0) {
            //     joinType = "RIGHT";
            // } else {
            //     joinType = "INNER";
            // }

            this.dataFilter =
                {
                    "SiteIdArr": siteid,
                    "PartnerType": partnerType.toString(),
                    "CourseId": this.SearchCourse.value.CourseId,
                    "SiteId": this.SearchCourse.value.SiteId,
                    "CourseTitle": this.SearchCourse.value.CourseTitle,
                    "CourseStartDate": this.parserFormatter.format(this.SearchCourse.value.CourseStartDate),
                    "CourseEndDate": this.parserFormatter.format(this.SearchCourse.value.CourseEndDate),
                    "SurveyYear": this.SearchCourse.value.SurveyYear,
                    "InstructorId": instructorId,
                    "OrgId":orgId,
                    "Distributor":distributorkan,
                    "FilterOrgId":this.SearchCourse.value.OrganizationId,
                    "FilterInst":this.SearchCourse.value.InstructorId,
                    "SortBy":sort,
                    "SortValue":this.SearchCourse.value.SortValue,
                    "Limit":0,
                    "Offset":0,
					"Version":this.useraccessdata.Version
                }

            this.service.httpClientPost(
                "api/Courses/FilterCourseCount",
                this.dataFilter
            )
                .subscribe(result => {
                    data = result;
                   // data = JSON.parse(data);
                    this.countdata = +data.count;
                    this.getServerData(1)
                    this.foundCourse = true;
                    this.loading = false
                });

            // this.query_current = "api/Courses/whereNew/{'SiteIdArr':'" + siteid + "','PartnerType':'" + partnerType + "','CourseId':'" + this.SearchCourse.value.CourseId +
            //     "','SiteId':'" + this.SearchCourse.value.SiteId + "','CourseTitle':'" + this.SearchCourse.value.CourseTitle + "','CourseStartDate':'" + this.parserFormatter.format(this.SearchCourse.value.CourseStartDate) + "','CourseEndDate':'" + this.parserFormatter.format(this.SearchCourse.value.CourseEndDate) +
            //     "','SurveyYear':'" + this.SearchCourse.value.SurveyYear + "','InstructorId':'" + instructorId + "'}/" + this.SearchCourse.value.SortValue + "/" + sort + "/" + orgId + "/" + joinType;

            // this.service.get("api/Courses/whereNew/{'SiteIdArr':'" + siteid + "','PartnerType':'" + partnerType + "','CourseId':'" + this.SearchCourse.value.CourseId +
            //     "','SiteId':'" + this.SearchCourse.value.SiteId + "','CourseTitle':'" + this.SearchCourse.value.CourseTitle + "','CourseStartDate':'" + this.parserFormatter.format(this.SearchCourse.value.CourseStartDate) + "','CourseEndDate':'" + this.parserFormatter.format(this.SearchCourse.value.CourseEndDate) +
            //     "','SurveyYear':'" + this.SearchCourse.value.SurveyYear + "','InstructorId':'" + instructorId + "'}/" + this.SearchCourse.value.SortValue + "/" + sort + "/" + orgId + "/" + joinType, data)
            //     .subscribe(res => {
            //         data = res;
            //         data = JSON.parse(data);
            //         this.countdata = +data.count;
            //         this.getServerData(1)
            //         this.foundCourse = true;
            //         this.loading = false
            //     });

        } else {
           
            this.valid = false;
            swal(
                'Field is Required!',
                'Please select Partner Type',
                'error'
            )
            
            // this.loading = false
        }

        //Buat object untuk filter yang dipilih
        var params =
        {
            selectPartner: this.selectedPartner,
            dropPartner: this.dropdownPartner,
            selectCerti: this.selectedCertificate,
            dropCerti: this.dropdownCertificate,
            idCourse: this.SearchCourse.value.CourseId,
            idOrg: this.SearchCourse.value.OrganizationId,
            idSite: this.SearchCourse.value.SiteId,
            idInstructor: this.SearchCourse.value.InstructorId,
            titleCourse: this.SearchCourse.value.CourseTitle,
            courseStart: this.modelPopup1,
            courseEnd: this.modelPopup2,
            year: this.SearchCourse.value.SurveyYear,
            sortV: this.SearchCourse.value.SortValue,
            sortD: this.SearchCourse.value.SortDirection

        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    }

    getTeachingLevel(data) {
        this.teachingLevel = [];
        if (data.Update == 1) {
            this.teachingLevel.push("Update");
        }
        if (data.Essentials == 1) {
            this.teachingLevel.push("Essentials");
        }
        if (data.Intermediate == 1) {
            this.teachingLevel.push("Intermediate");
        }
        if (data.Advanced == 1) {
            this.teachingLevel.push("Advanced");
        }
        if (data.Customized == 1) {
            this.teachingLevel.push("Customized");
        }
        if (data.Other == 1) {
            this.teachingLevel.push("Other : " + data.Comment);
        }

    }

    getVersion(data) {
        var ver: any;
        this.service.httpClientGet("api/Product/ProductVersion/" + data.ProductVersionsId, ver)
            .subscribe(res => {
                ver = res;
                ver != null ? this.version = ver[0].Version : this.version = "-";
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    getProduct(data) {
        var value: any;
        this.service.httpClientGet("api/Product/" + data.productId, value)
            .subscribe(res => {
                value = res;
                value != null ? this.product = value.productName : this.product = "-";
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    detailCourse(id) {
        if (id != "") {
            var data = '';
            var detail: any;
            this.service.httpClientGet("api/Courses/Detail/" + id, data)
                .subscribe(res => {
                    detail = res;
                    if (detail.CourseId != "") {
                        this.datadetail = detail;
                        this.getTeachingLevel(this.datadetail);
                        this.getProduct(this.datadetail);
                        this.getVersion(this.datadetail);
                    } else {
                        swal(
                            'Information!',
                            "Data Not Found",
                            'error'
                        );
                    }
                }, error => {
                    this.messageError = <any>error;
                    this.service.errorserver();
                });
        } else {
            swal(
                'Information!',
                "Data Not Found",
                'error'
            );
        }
    }

    confirmEdit(courseId, endDate, evalsNumber, partnerType) {
        //endDate ga jadi dipakai

        // var endCourse: Date = new Date(endDate);
        // if (this.today.getTime() <= endCourse.getTime()) {
        //     this.router.navigate(['/course/course-update', courseId]);
        // } else {
        //     swal('Information!', 'Course editting period has been expired', 'error');
        // }
        this.router.navigate(['/course/course-update', courseId, evalsNumber, partnerType]);
    }

    confirmFinalStudent(courseId, endDate) {
        // var endCourse: Date = new Date(endDate);
        // if (this.today.getTime() >= endCourse.getTime()) {
        //     this.router.navigate(['/studentinfo/confirm-final-student', courseId]);
        // } else {
        //     swal('Information!', 'Attendance student can confirm after this course have completed', 'error');
        // }
        this.router.navigate(['/studentinfo/confirm-final-student', courseId]);
    }

    CopyCourse(id) {
        this.router.navigate(['/course/course-copy', id]);
    }

    DeleteCourse(id) {
        // var index = this.kursus.findIndex(x => x.CourseId == id);
        // this.service.httpClientDelete("api/Courses/DeleteCourse", this.kursus, id, index);

        var cek: any;

        //Ganti logic delete course 01/08/2018
        //Cek apakah student sudah mengisi survey di kursus ini
        this.service.httpClientGet('api/Courses/isSurveyTaken/' + id, cek)
            .subscribe(res => {
                cek = res;
                //jika suda ada student yg mengisi survey, maka user tidak boleh menghapus kursus
                if (cek.length != 0) {
                    swal(
                        'Information!',
                        '"Cannot Delete This Course", there are some students who have completed the survey in this course',
                        'error'
                    );
                } else {
                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    })
                        .then(result => {
                            if (result == true) {
                                var cuid = this.useraccessdata.ContactName
                                var UserId = this.useraccessdata.UserId

                                var data = '';
                                this.service.httpClientDelete('api/Courses/DeleteCourse/' + id + '/' + cuid + '/' + UserId, data)
                                    .subscribe(result => {
                                        var resource = result;
                                        if (resource['code'] == '1') {
                                            this.router.navigate(['/course/course-list']);
                                            this.onSubmit();
                                        }
                                        else {
                                            swal(
                                                'Information!',
                                                "Delete Data Failed",
                                                'error'
                                            );
                                        }
                                    },
                                        error => {
                                            this.messageError = <any>error
                                            this.service.errorserver();
                                        });
                            }
                        }).catch(swal.noop);
                }
            });
    }

    clearFilterStorage() {
        localStorage.removeItem("filter");
    }

    ExportExcel() {
        this.loading= true;
        var data:any;

        this.dataFilter.Limit = 0;
        this.dataFilter.Offset = 0;
		
        this.service.httpClientPost(
            "api/Courses/FilterCourse",
            this.dataFilter
        )
            .subscribe(result => {
                data = result;
               // data = JSON.parse(data);
                if (data.length > 0) {
                    var fileName = "ListCourseReport.xls";
                    var ws = XLSX.utils.json_to_sheet(data);
                    var wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, fileName);
                    XLSX.writeFile(wb, fileName);
                    this.loading = false;
                } else {
                    this.loading = false;
                }
                this.loading = false
            });
    }
}