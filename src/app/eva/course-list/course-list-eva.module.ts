import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CourseListEVAComponent } from './course-list-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AppService } from "../../shared/service/app.service";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { DataFilterCoursePipe, ConvertDatePipe } from './course-list-eva.component';
import { SessionService } from '../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';
import { Ng2PaginationModule } from 'ng2-pagination';
// import { NG2DataTableModule } from "angular2-datatable-pagination";

export const CourseListEVARoutes: Routes = [
  {
    path: '',
    component: CourseListEVAComponent,
    data: {
      breadcrumb: 'eva.manage_course.list_course.list_course',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CourseListEVARoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule,
    Ng2PaginationModule
    // NG2DataTableModule
  ],
  declarations: [CourseListEVAComponent, DataFilterCoursePipe, ConvertDatePipe],
  providers: [AppService, AppFormatDate, DatePipe, SessionService]
})
export class CourseListEVAModule { }