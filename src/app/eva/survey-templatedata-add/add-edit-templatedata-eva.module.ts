import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditTemplateDataEVAComponent } from './add-edit-templatedata-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddEditTemplateDataEVARoutes: Routes = [
  {
    path: '',
    component: AddEditTemplateDataEVAComponent,
    data: {
      breadcrumb: 'eva.manage_evaluation.add_evaluation_template_data',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddEditTemplateDataEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddEditTemplateDataEVAComponent],
  providers: [AppService, AppFormatDate]
})
export class AddEditTemplateDataEVAModule { }
