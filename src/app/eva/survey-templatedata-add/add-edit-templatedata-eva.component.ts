import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '../../shared/service/session.service';

var surveyJSON = {
  "pages": [
    {
      "name": "page1"
    }
  ]
}

@Component({
  selector: 'app-add-edit-templatedata-eva',
  templateUrl: './add-edit-templatedata-eva.component.html',
  styleUrls: [
    './add-edit-templatedata-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddEditTemplateDataEVAComponent implements OnInit {

  private dropdownCertificate;
  private _serviceUrl = 'api/EvaluationQuestion';
  messageResult: string = '';
  messageError: string = '';
  addtemplatedata: FormGroup;
  year;
  dropdownPartner = [];
  private partnerSelected;
  public CertificateType = false;
  private useraccessdata;
  private certificateType;
  public loading = false;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate,
    private http: HttpClient, private datePipe: DatePipe, private session: SessionService) {
    
    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);

    let PartnerType = new FormControl('', Validators.required);
    let certificatetype = new FormControl('', Validators.required);
    let EvaluationQuestionCode = new FormControl('', Validators.required);
    let CourseId = new FormControl('', Validators.required);
    let Year = new FormControl('', Validators.required);

    this.addtemplatedata = new FormGroup({
      PartnerType: PartnerType,
      certificatetype: certificatetype,
      EvaluationQuestionCode: EvaluationQuestionCode,
      CourseId: CourseId,
      Year: Year
    });
  }

  getCertificateType() {
    var certificate: any;
    var parent = "AAPCertificateType";
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
      .subscribe(result => {
        this.dropdownCertificate = result;
      }, error => {
        this.service.errorserver();
      });
  }

  getPartnerType() {
    var partner = ['1', '58'];
    var partnerTemp: any;
    this.dropdownPartner = [];
    for (let i = 0; i < partner.length; i++) {
      this.service.httpClientGet("api/Roles/" + partner[i], partnerTemp)
        .subscribe(result => {
          partnerTemp = result;
          this.dropdownPartner.push(partnerTemp);
        }, error => {
          this.service.errorserver();
        });
    }
  }

  getFY() {
    var parent = "FYIndicator";
    var data = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
      .subscribe(res => {
        console.log(res)
        this.year = res;
      }, error => {
        this.service.errorserver();
      });
  }

  ngOnInit() {
    this.getFY();
    this.getCertificateType();
    this.getPartnerType();
  }

  //submit form
  onSubmit() {

    this.addtemplatedata.controls['PartnerType'].markAsTouched();
    if (this.addtemplatedata.value.PartnerType == 1) {
      this.addtemplatedata.removeControl("certificatetype");
      this.certificateType = 0;
    } else {
      this.addtemplatedata.controls['certificatetype'].markAsTouched();
      this.certificateType = this.addtemplatedata.value.certificatetype;
    }
    this.addtemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.addtemplatedata.controls['CourseId'].markAsTouched();
    this.addtemplatedata.controls['Year'].markAsTouched();

    if (this.addtemplatedata.valid) {
      swal({
        title: 'Are you sure?',
        text: "If you wish to Add the record!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          this.addtemplatedata.value.CountryCode = "SG";
          this.addtemplatedata.value.EvaluationQuestionTemplate = surveyJSON;
          this.addtemplatedata.value.EvaluationQuestionJson = surveyJSON;
          // this.addtemplatedata.value.CreatedDate = this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"); /* issue 14112018 - Couldn’t add any records */
          this.addtemplatedata.value.CreatedBy = this.useraccessdata.ContactName;

          var dataToPost = {
            'EvaluationQuestionCode': this.addtemplatedata.value.EvaluationQuestionCode,
            'CertificateTypeId': this.certificateType,
            'PartnerType': this.addtemplatedata.value.PartnerType,
            'CourseId': this.addtemplatedata.value.CourseId,
            'CountryCode': this.addtemplatedata.value.CountryCode,
            'EvaluationQuestionTemplate': this.addtemplatedata.value.EvaluationQuestionTemplate,
            'EvaluationQuestionJson': this.addtemplatedata.value.EvaluationQuestionJson,
            'CreatedBy': this.addtemplatedata.value.CreatedBy,
            'CreatedDate': this.addtemplatedata.value.CreatedDate,
            'Year': this.addtemplatedata.value.Year,
            'LanguageID': '35',

            'cuid' : this.useraccessdata.ContactName,
            'UserId': this.useraccessdata.UserId
          };

          


          let data = JSON.stringify(dataToPost);

          /* autodesk plan 10 oct */

          //post action
          this.service.httpClientPost(this._serviceUrl, data)
            .subscribe(res => {
              var result = res;
              if (result['code'] == '1') {
                this.router.navigate(['/evaluation/add-edit-form', result['EvaluationQuestionId'], this.addtemplatedata.value.EvaluationQuestionCode]);
                this.loading = false;
              }
              else {
                swal(
                  'Information!',
                  "Insert Data Failed",
                  'error'
                );
                this.loading = false;
              }
            },
            error => {
              this.service.errorserver();
              this.loading = false;
            });

          /* end line autodesk plan 10 oct */

        }
      }).catch(swal.noop);
    }
    else {
      /*
      swal(
        'Field is Required!',
        'Please enter form is required :)',
        'error'
      )
      */
    }
  }

  selectedPartner(newvalue) {
    this.partnerSelected = newvalue;
    if (newvalue == 58) {
      var certificate: any;
      var parent = "AAPCertificateType";
      this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
        .subscribe(result => {
          this.dropdownCertificate = result;
        }, error => {
          this.service.errorserver();
        });
      this.CertificateType = true;

    } else {
      this.CertificateType = false;
    }
  }

  resetForm() {
    this.addtemplatedata.reset({
      'CertificateType': '',
      'Year': ''
    });
  }

  //check code registered
  validcode: Boolean = true;
  checkCodeDuplicate(value) {
    let codedata: any;
    if (value != '' && value != null) {
      this.service.httpClientGet(this._serviceUrl + "/checkcode/" + value, '')
        .subscribe(result => {
          codedata = result;
          if (codedata.EvaluationQuestionCode != null) {
            this.validcode = false;
          }
          else if (codedata.EvaluationQuestionCode == null) {
            this.validcode = true;
          }
        },
          error => {
            this.service.errorserver();
          });
    }
    else {
      this.validcode = true;
    }
  }
}
