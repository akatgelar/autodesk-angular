import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyAnswerAddEVAComponent } from './survey-answer-add-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";

export const SurveyAnswerAddEVARoutes: Routes = [
  {
    path: '',
    component: SurveyAnswerAddEVAComponent,
    data: {
      breadcrumb: 'Take Survey',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyAnswerAddEVARoutes),
    SharedModule
  ],
  declarations: [SurveyAnswerAddEVAComponent],
  providers:[AppService]
})
export class SurveyAnswerAddEVAModule { }
