import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {AppService} from "../../shared/service/app.service";

import * as Survey from "survey-angular";

var surveyJSON ={};
var datadetail: any;

function sendDataToServer(survey) {
  let _serviceUrl = 'api/EvaluationAnswer';
  alert("The results are:" + JSON.stringify(survey.data));

  //update data
  let dataanswer =   JSON.stringify({
    EvaluationAnswerId: "",
    EvaluationQuestionCode:datadetail.EvaluationQuestionCode,
    CourseId:datadetail.CourseId,
    CountryCode:datadetail.CountryCode,
    StudentId:"123",
    EvaluationAnswerJson: JSON.stringify(survey.data),
    Status: "1",
    CreatedBy:"",
    CreatedDate:""
  })
  
  var xhr = new XMLHttpRequest();
  xhr.open(
    "POST",
    _serviceUrl
  );
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.onload = function() {
    var result = xhr.response ? JSON.parse(xhr.response) : null;
    if (xhr.status === 200) {
        alert("data send");
    }
  };
  xhr.send(
      dataanswer
  );
}

@Component({
  selector: 'survey-answer-add-eva',
  templateUrl: './survey-answer-add-eva.component.html',
  styleUrls: [
    './survey-answer-add-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    '../../../../node_modules/survey-angular/survey.css',
    //'../../../../src/assets/css/bootstrap.css',
    ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyAnswerAddEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  public data: any;
  public datadetail: any;
  public json:any;
  id:string;

  constructor(private service:AppService, private route:ActivatedRoute, private router: Router){ }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.json={};

    //get data according to id
    this.service.httpClientGet(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        if(result=="Not found"){
            this.service.notfound();
            this.datadetail = '';
        }
        else{
          datadetail = result;
          surveyJSON = JSON.stringify(datadetail.EvaluationQuestionTemplate);
          Survey.Survey.cssType = "bootstrap";
          var survey = new Survey.Model(surveyJSON);
          survey.onComplete.add(sendDataToServer);
          Survey.SurveyNG.render("surveyElement", { model: survey });
        }
      },
      error => {
          this.service.errorserver();
          this.datadetail = '';
      });
  }

}
