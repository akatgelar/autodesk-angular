import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseAddEVAComponent } from './course-add-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { FormsModule } from "@angular/forms";
import { Ng2CompleterModule } from "ng2-completer";

import { DataFilterStudentPipe, DataFilterPickPipe } from './course-add-eva.component';
import { LoadingModule } from 'ngx-loading';

export const CourseAddEVARoutes: Routes = [
  {
    path: '',
    component: CourseAddEVAComponent,
    data: {
      breadcrumb: 'eva.manage_course.add.add_course',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CourseAddEVARoutes),
    SharedModule,
    AngularMultiSelectModule,
    Ng2CompleterModule,
    FormsModule,
    LoadingModule
  ],
  declarations: [CourseAddEVAComponent, DataFilterStudentPipe, DataFilterPickPipe],
  providers: [AppService, AppFormatDate, AppFilterGeo]
})
export class CourseAddEVAModule { }