declare const $: any;
declare var Morris: any;
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import $ from 'jquery/dist/jquery';
import * as _ from "lodash";
import { Pipe, PipeTransform, ViewChild, Injectable, ElementRef } from "@angular/core";
import { error } from 'util';
import { WindowRef } from '@agm/core/utils/browser-globals';
import { RequestOptions } from '@angular/http';
// import { SessionService } from '../../shared/service/session.service.js';
import { SessionService } from '../../shared/service/session.service';


@Pipe({ name: 'dataFilterStudent' })
export class DataFilterStudentPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.StudentID.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Firstname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Lastname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Email.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Phone.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterPick' })
export class DataFilterPickPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.StudentID.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Firstname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Lastname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Email.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Phone.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

const now = new Date();

@Component({
    selector: 'app-course-add-eva',
    templateUrl: './course-add-eva.component.html',
    styleUrls: [
        './course-add-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CourseAddEVAComponent implements OnInit {
    // @ViewChild('fileInput') fileInput;
    @ViewChild('fileInput') fileInput: ElementRef;
    listSites = [];
	private trainingHoursOther:any;
    nativeWindow: any;
    private _serviceUrl = 'api/MainCourses';
    messageResult: string = '';
    messageError: string = '';
    studentErrorTag = false;
    studentSuccessTag = false;
    errorTagMessage: string = '';
    successTagMessage: string = '';
    private errorSites: string = '';
    private errorInstructor: string = '';
    public rowsOnPage: number = 5;
    public rowsOnPage1: number = 5;
    public filterQuery: string = "";
    public filterQuery1: string = "";
    public sortBy: string = "StudentID";
    public sortOrder: string = "asc";
    public sortBy1: string = "StudentID";
    public sortOrde1: string = "asc";
    // addcourseform: FormGroup;  
    // modelPopup1: NgbDateStruct;  
    // modelPopup2: NgbDateStruct;
    public Course = false;
    public Project = false;
    public event = false;
    public CertificateType = false;
    public CertificateTypeCol = true;
    public ATC1 = true;
    public ATC2 = true;
    public ATC3 = true;
    public ATC4 = true;
    public ATC5 = true;
    public institution = false;
    public parentLabel = false;
    public projectInstitution = false;
    public projectLogoUpload = false;
    private data;
    pickedStudent = [];
    dataStudent = [];
    public studentFound = false;
    public studentPicked = false;
    private dropdownCertificate = "";
    private ContactName: string;
    private products;
    private versions1;
    private versions2;
    private typeOf;
    private trainingHours;
    public courseEquipment = true;
    public courseFacility = true;
    public courseDiv = false;
    private evaTemplate;
    private cftTemplate;
    private disabled = false;
    private survey;
    private lastCourseId;
    private evaTemplateId;
    private certificateTemplateId;
    private partnerSelected;
    private certificateSelected;

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownSettings = {};

    selectedStudent = [];

    dropdownPartner = [];

    protected siteId: any = "";
    protected contactId: string = "";
    protected contactName: string = "";
    protected siswa: string;
    protected sites = [];
    protected instructor = [];
    protected siswaLookup = [];
    // protected dataService1: CompleterData;
    protected dataService2: CompleterData;
    // protected dataService3: CompleterData;
    protected dataService1: RemoteData;
    protected dataService3: RemoteData;

    addCourse: FormGroup; //Add Course with Partner Type AAP & Certificate Type Course
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    dateStart: string;
    dateEnd: string;
    currentYear;
    public loading = false;

    public minDate: NgbDateStruct;
    // public minDate1: NgbDateStruct;

    public useraccesdata: any;
    private selectedSite;
    private contactID;
    private coursePartner;
    private certdropdown;
    private selcertdropdown;
    public notFound = false;
    eventType;
    textSearching: string = "";
    teachingLevel: string = "";
    kurir = [];

    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsOrganization(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ORGANIZATION") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsSite(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "SITE") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }

     /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

     adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
				for(var s=0; s< userlvlarr.length; s++){
					if(s!=i){
						if (userlvlarr[s] == "ADMIN" || userlvlarr[s] == "SUPERADMIN" || userlvlarr[s] == "ORGANIZATION" || userlvlarr[s] == "SITE" || userlvlarr[s] == "DISTRIBUTOR") {
							this.trainerya = false;
						}
					}
				}
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }
 
     urlGetOrgId(): string {
         var orgarr = this.useraccesdata.OrgId.split(',');
         var orgnew = [];
         for (var i = 0; i < orgarr.length; i++) {
             orgnew.push('"' + orgarr[i] + '"');
         }
         return orgnew.toString();
     }
 
     urlGetSiteId(): string {
         var sitearr = this.useraccesdata.SiteId.split(',');
         var sitenew = [];
         for (var i = 0; i < sitearr.length; i++) {
             sitenew.push('"' + sitearr[i] + '"');
         }
         return sitenew.toString();
     }
 
     /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    constructor(private service: AppService, public http: Http, private router: Router, private formatdate: AppFormatDate, private filterGeo: AppFilterGeo,
        private session: SessionService,
        private completerService: CompleterService, private winRef: WindowRef, private parserFormatter: NgbDateParserFormatter) {

        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        this.minDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        
        // this.minDate1 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        // this.dataService1 = completerService.local(this.sites, 'SiteId', 'SiteId').descriptionField("SiteName");
        // this.dataService3 = completerService.local(this.siswaLookup, 'StudentID', 'StudentID').descriptionField("Firstname");
        // this.dataService3 = completerService.local(this.siswaLookup, 'StudentKey', 'StudentKey');

        //Untuk pencarian sites berdasarkan site id

        // let status = ['"A"', '"H"', '"I"'];
        // let siteUrl: any;

        // if (this.useraccesdata.UserLevelId == "TRAINER") {
        //     var orgArr = this.useraccesdata.OrgId.toString().split(',');
        //     var orgTmp = [];
        //     for (var i = 0; i < orgArr.length; i++) {
        //         orgTmp.push('"' + orgArr[i] + '"');
        //     }
        //     siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Org':'" + orgTmp.join(",") + "'}";
        // }
        // else {
        //     siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
        // }


        //based on role

        // if(this.checkrole()){
        //     if(this.itsDistributor()){
        //         siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Distributor':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.itsinstructor()){
        //             siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','ContactId':'" + this.useraccesdata.UserId + "'}";
        //         }else{
        //             siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Organization':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }else{
        //     siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
        // }

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Distributor':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsOrganization()) {
        //             siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Organization':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             if (this.itsSite()) {
        //                 siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','SiteId':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','ContactId':'" + this.useraccesdata.UserId + "'}";
        //             }
        //         }
        //     }
        // }
        // else {
        //     siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
        // }

        // this.dataService1 = completerService.remote(
        //     null,
        //     "Sites",
        //     "Sites");
        // this.dataService1.urlFormater(term => {
        //     return siteUrl + "/" + term;
        // });
        // this.dataService1.dataField("results");

        //Untuk pencarian siswa berdasarkan email dan student id
        this.dataService3 = completerService.remote(
            null,
            "Student",
            "Student");
        this.dataService3.urlFormater(term => {
            return `api/Student/FindStudent/` + term;
        });
        this.dataService3.dataField("results");

        this.nativeWindow = winRef.getNativeWindow();

        // this.currentYear = (new Date()).getFullYear();

        let CourseId = new FormControl('');
        let PartnerType = new FormControl('', Validators.required);
        let CertificateType = new FormControl('', Validators.required);
        let CourseTitle = new FormControl('', [Validators.required, Validators.maxLength(40)]);
        let SiteId = new FormControl('', Validators.required);
        let ContactId = new FormControl('', Validators.required);
        let Institution = new FormControl('', Validators.required);
        let StartDate = new FormControl('', Validators.required);
        let EndDate = new FormControl('', Validators.required);
        let Survey = new FormControl(this.currentYear, Validators.required);
        let Update = new FormControl(false);
        let Essentials = new FormControl(false);
        let Intermediate = new FormControl(false);
        let Advanced = new FormControl(false);
        let Customized = new FormControl(false);
        let Other = new FormControl(false);
        let Komen = new FormControl('');
        let Status = new FormControl('');
        let Product1 = new FormControl('', Validators.required);
        let Version1 = new FormControl('');
        let Product2 = new FormControl('');
        let Version2 = new FormControl('');
        let TrainingHours = new FormControl('', Validators.required);
        let InstructorLed = new FormControl('');
        let Online = new FormControl('');
        let TrainingType = new FormControl('');
        let Autodesk = new FormControl('');
        let AAP = new FormControl('');
        let ATC = new FormControl('');
        let Independent = new FormControl('');
        let IndependentOnline = new FormControl('');
        let ATCOnline = new FormControl('');
        let Other5 = new FormControl('');
        let Komen5 = new FormControl('');
        let CourseFacility = new FormControl('', Validators.required);
        let CourseEquipment = new FormControl('', Validators.required);
        // let EvaTemplate = new FormControl('', Validators.required);
        // let CertTemplate = new FormControl('', Validators.required);
        let StudentID = new FormControl('');
        let ProjectType = new FormControl('', Validators.required);
        let LocationName = new FormControl('', Validators.required);
        let EventType = new FormControl('', Validators.required);
        let LearnerType = new FormControl('', Validators.required);
        let ContactName = new FormControl('');
        let LevelTeaching = new FormControl('',Validators.required);
        let InstitutionLogo = new FormControl(null); //change to save logo into database
		let HoursTrainingOther=new FormControl(0);

        this.addCourse = new FormGroup({
            CourseId: CourseId,
            PartnerType: PartnerType,
            CertificateType: CertificateType,
            CourseTitle: CourseTitle,
            SiteId: SiteId,
            ContactId: ContactId,
            Institution: Institution,
            StartDate: StartDate,
            EndDate: EndDate,
            Survey: Survey,
            Update: Update,
            Essentials: Essentials,
            Intermediate: Intermediate,
            Advanced: Advanced,
            Customized: Customized,
            Other: Other,
            Komen: Komen,
            Status: Status,
            Product1: Product1,
            Version1: Version1,
            Product2: Product2,
            Version2: Version2,
            TrainingHours: TrainingHours,
			HoursTrainingOther:HoursTrainingOther,
            InstructorLed: InstructorLed,
            Online: Online,
            TrainingType: TrainingType,
            Autodesk: Autodesk,
            AAP: AAP,
            ATC: ATC,
            Independent: Independent,
            IndependentOnline: IndependentOnline,
            ATCOnline: ATCOnline,
            Other5: Other5,
            Komen5: Komen5,
            CourseFacility: CourseFacility,
            CourseEquipment: CourseEquipment,
            // EvaTemplate: EvaTemplate,
            // CertTemplate: CertTemplate,
            StudentID: StudentID,
            ProjectType: ProjectType,
            LocationName: LocationName,
            EventType: EventType,
            LearnerType: LearnerType,
            ContactName: ContactName,
            LevelTeaching: LevelTeaching,
            InstitutionLogo: InstitutionLogo //change to save logo into database
        });
    }

    ketikaSiswaDipilih(item) {
        this.addStudent(item['originalObject'].StudentID);
    }

    getPartnerType() {
        var partnerTemp: any;
        this.dropdownPartner = [];
        this.service.httpClientGet("api/Roles/CoursePartner", partnerTemp)
            .subscribe(result => {
                partnerTemp = result;
                this.dropdownPartner = partnerTemp;
            });
    }

    getDataSiswa() {
        this.loading = true;
        var murid: any;
        var studentName: any;
        this.service.httpClientGet("api/Student", murid)
            .subscribe(result => {
                // console.log(result)
                // var finalresult = result.replace(/\t/g, " ");
                // murid = JSON.parse(finalresult);
                var finalresult : any = result;
                if (murid.length > 0) {
                    for (let i = 0; i < murid.length; i++) {
                        if (murid[i].Firstname != null && murid[i].Firstname != '') {
                            murid[i].Firstname = this.service.decoder(murid[i].Firstname);
                        }
                        if (murid[i].Lastname != null && murid[i].Lastname != '') {
                            murid[i].Lastname = this.service.decoder(murid[i].Lastname);
                      }
                        // this.siswaLookup.push(murid[i]);
                        if (murid[i].FirstName != murid[i].Lastname) {
                            studentName = murid[i].Firstname + " " + murid[i].Lastname;
                        } else {
                            studentName = murid[i].Firstname;
                        }

                        //Kata kunci menggunakan email dan nama siswa
                        if (murid[i].Email != "" && murid[i].Email != undefined) {
                            this.siswaLookup.push({
                                StudentKey: murid[i].Email + " | " + studentName,
                                StudentName: studentName,
                                StudentID: murid[i].StudentID
                            });
                        } else {
                            this.siswaLookup.push({
                                StudentKey: studentName,
                                StudentName: studentName,
                                StudentID: murid[i].StudentID
                            });
                        }

                    }
                    this.loading = false;
                }
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });

    }

    public partnertype: any;
    selectedPartner(newvalue) {
        this.loading = true;
        this.partnerSelected = newvalue;
        if (newvalue == 58) {
            var certificate: any;
            this.dropdownCertificate = "";
            // var parent = "AAPCertificateType";
            this.service.httpClientGet("api/Roles/" + newvalue, '')
                .subscribe(result => {
                    this.partnertype = result;
                    // this.service.get('api/RoleCertificate/where/{"RoleCode":"' + this.partnertype.RoleCode + '","Parent":"' + parent + '","Status":"A"}', certificate)
                    // this.service.get('api/RoleCertificate/where/{"Parent":"' + parent + '","Status":"A"}', certificate)
                    this.service.httpClientGet('api/RoleCertificateType/AAP', certificate)
                        .subscribe((result:any) => {
                            let tmpData : any = result;
                            // this.dropdownCertificate = tmpData.sort((a,b)=>{
                            //     if(a.KeyValue > b.KeyValue)
                            //         return 1
                            //     else if(a.KeyValue < b.KeyValue)
                            //         return -1
                            //     else return 0
                            // });
                            this.dropdownCertificate = tmpData;
                        });
                    this.loading = false;
                });
            this.courseDiv = true;
            this.courseEquipment = false;
            this.courseFacility = false;
            this.CertificateType = true;
            this.CertificateTypeCol = false;
            this.institution = true;
        } else {
            this.dropdownCertificate = "";
            // localStorage.removeItem("filter");
            this.coursePartner = 'ATC';
            this.parentLabel = true;
            this.CertificateType = false;
            this.CertificateTypeCol = true;
            this.courseDiv = false;
            this.courseEquipment = true;
            this.courseFacility = true;
            this.event = false;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC5 = true;
            this.institution = false;
            this.loading = false;
        }
        this.siteId = "";
    }

    // getSite() {
    //     if (this.useraccesdata.UserLevelId == "TRAINER") {
    //         var orgArr = this.useraccesdata.OrgId.toString().split(',');
    //         var orgTmp = [];
    //         for (var i = 0; i < orgArr.length; i++) {
    //             orgTmp.push('"' + orgArr[i] + '"');
    //         }
    //         this.getSiteFromUrl("api/MainSite/SiteCourse/{'Status':'A','Org':'" + orgTmp.join(",") + "'}");
    //     }
    //     else {
    //         this.getSiteFromUrl("api/MainSite/SiteCourse/{'Status':'A'}");
    //     }
    // }

    // getSiteFromUrl(url) {
    //     var siteTemp: any;
    //     this.service.get(url, siteTemp)
    //         .subscribe(result => {
    //             var finalresult = result.replace(/\t/g, " ");
    //             siteTemp = JSON.parse(finalresult);
    //             if (siteTemp != null) {
    //                 if (siteTemp.length > 0) {
    //                     var exist = false;
    //                     for (let i = 0; i < siteTemp.length; i++) {
    //                         if (this.sites.length == 0) {
    //                             this.sites.push(siteTemp[i]);
    //                         } else {
    //                             for (let j = 0; j < this.sites.length; j++) {
    //                                 if (siteTemp[i].SiteId == this.sites[j].SiteId) {
    //                                     exist = true;
    //                                 }
    //                             }
    //                             if (exist == false && siteTemp[i].SiteId != "") {
    //                                 this.sites.push(siteTemp[i]);
    //                             }
    //                         }
    //                     }
    //                 } else {
    //                     swal(
    //                         'Information!',
    //                         'Site Data Not Found',
    //                         'error'
    //                     );
    //                 }
    //             } else {
    //                 swal(
    //                     'Information!',
    //                     'Site Data Not Found',
    //                     'error'
    //                 );
    //             }
    //         }, error => { this.service.errorserver(); });
    // }

    getInstructor(siteId) { 
        let n_siteId
       if(siteId)
           n_siteId = siteId
       else
           n_siteId = this.siteId[0].id
        var instructor: any;
        this.instructor = [];
        this.service.httpClientGet("api/MainSite/Instructor/" + n_siteId, instructor)
            .subscribe(result => {
                instructor = result;
                if (instructor.length > 0) {
                    var exist = false;
                    for (let i = 0; i < instructor.length; i++) {
                        // if (this.instructor.length == 0) {
                        //     this.instructor.push(instructor[i]);
                        // } else {
                        //     for (let j = 0; j < this.instructor.length; j++) {
                        //         if (instructor[i].InstructorId == this.instructor[j].InstructorId) {
                        //             exist = true;
                        //         }
                        //     }
                        //     if (exist == false && instructor[i].InstructorId != "") {
                        //         this.instructor.push(instructor[i]);
                        //     }
                        // }
                        this.instructor.push(instructor[i]);
                    }
                    this.dataService2 = this.completerService.local(this.instructor, 'Instructor', 'Instructor');
                }
            });
    }

    checkSelectedSite(value) {
        if (this.instructor.length != 0) {
            this.textSearching = "Please wait...";
        } else {
            this.textSearching = "No results found";
        }
    }
    onSiteIdInputFocus(){
               if (!this.partnerSelected){
                   $("#partnerTypeSelector").focus();
                   swal(
                       'Field is Required!',
                       'Please select Partner Type first.',
                       'info'
                   )	
               }
           }

    onSelected(item) {
        if (item == null) {
            this.addCourse.controls["ContactId"].reset();
            this.addCourse.controls["ContactName"].reset();
        }

        if (item != null) {
            let splitVal = (item.originalObject.Sites).split(" | ");
            var siteId = '';
            siteId = item.title;
            this.getInstructor(splitVal[0]);
            this.errorSites = "";
        }
    }

    onSelectedInstructor(item) {
        if (item == null) {
            this.addCourse.controls["ContactName"].reset();
        }

        if (item != null) {
            let splitVal = (item.originalObject.Instructor).split(" | ");
            this.ContactName = splitVal[1];
            this.addCourse.patchValue({ ContactName: splitVal[1] });
            for (let i = 0; i < this.instructor.length; i++) {
                if (this.instructor[i].Instructor === item.originalObject.Instructor) {
                    this.contactID = this.instructor[i].ContactId;
                    // this.contactId = this.instructor[i].ContactId;
                }
            }
            this.errorInstructor = "";
        }
        // else {
        //     this.errorInstructor = "Instructor Not Found..";
        // }
    }

    // checkIsInstructor(value) {
    //     let ketemu: boolean = false;
    //     if (this.instructor.length != 0) {
    //         for (let n = 0; n < this.instructor.length; n++) {
    //             if (value === this.instructor[n].Instructor) {
    //                 ketemu = true;
    //             }
    //         }

    //         if (ketemu == false) {
    //             swal(
    //                 'Field is Required!',
    //                 'Please select instructor from the selected site!',
    //                 'error'
    //             )
    //         }
    //     } else {
    //         swal(
    //             'Field is Required!',
    //             'Instructor data not found from the selected site!',
    //             'error'
    //         )
    //     }
    // }

    getProduct() {
        var productTemp: any;
        //Saya hide dulu karena pak nakarin minta nampilin semua produknya dan ngehide produk qualification sampai ada kejelasannya
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         this.service.get('api/Qualifications/Product/' + this.useraccesdata.UserId, productTemp)
        //             .subscribe(result => {
        //                 if (result == "Not Found") {
        //                     this.products = '';
        //                 } else {
        //                     productTemp = result;
        //                     console.log(productTemp);
        //                     productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        //                     this.products = JSON.parse(productTemp);
        //                 }
        //             },
        //                 error => {
        //                     this.service.errorserver();
        //                     this.products = '';
        //                 });
        //     }
        // }
        // else {
        //     // this.service.get("api/Product/GetSingleProduct", productTemp)
        //     this.service.get("api/Product", productTemp)
        //         .subscribe(result => {
        //             productTemp = result;
        //             productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        //             this.products = JSON.parse(productTemp);
        //         });
        // }


        // this.service.get("api/Product", productTemp)
        this.service.httpClientGet("api/Product/ListProductCourse", productTemp)
            .subscribe((result:any) => {
                // productTemp = result;
                // productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
                // this.products = JSON.parse(productTemp);
                this.products = result.sort((a,b)=>{
                    if(a.productName > b.productName)
                        return 1
                    else if(a.productName < b.productName)
                        return -1
                    else return 0
                });
            });
    }

    findVersion1(value) {
        if (value != null) {
            this.loading = true;
            var version: any;
            //sy hide dulu karena kata si zwe mah masih simpang siur statusnya
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         this.service.httpClientGet("api/Qualifications/Version/" + value + "/" + this.useraccesdata.UserId, version)
            //             .subscribe(result => {
            //                 version = result;
            //                 if (version.length > 0) {
            //                     let currentVersion = [];
            //                     var anyprevious = false;
            //                     //Reposisi index previous seharusnya di pilihan paling bawah
            //                     for (let i = 0; i < version.length; i++) {
            //                         if (version[i].Version != 'Previous') {
            //                             currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
            //                         } else {
            //                             anyprevious = true;
            //                         }
            //                     }

            //                     if (anyprevious) {
            //                         let lastVersion = version.find(o => o.Version === 'Previous');
            //                         if (lastVersion != undefined) {
            //                             currentVersion.splice(currentVersion.length, 0, lastVersion);
            //                         }
            //                     }

            //                     this.versions1 = currentVersion;
            //                 } else {
            //                     this.versions1 = [{ ProductVersionsId: '0', Version: "No version found" }];
            //                 }
            //             });
            //     }
            // } else {
            //     this.service.httpClientGet("api/Product/Version/" + value, version)
            //         .subscribe(result => {
            //             version = result;
            //             // console.log(version);
            //             if (version.length > 0) {
            //                 let currentVersion = [];
            //                 //Reposisi index previous seharusnya di pilihan paling bawah
            //                 for (let i = 0; i < version.length; i++) {
            //                     if (version[i].Version != 'Previous') {
            //                         currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
            //                     }
            //                 }
            //                 let lastVersion = version.find(o => o.Version === 'Previous');
            //                 if (lastVersion != undefined) {
            //                     currentVersion.splice(currentVersion.length, 0, lastVersion);
            //                 }
            //                 this.versions1 = currentVersion;
            //             } else {
            //                 this.versions1 = [{ ProductVersionsId: '0', Version: "No version found" }];
            //             }
            //         });
            // }
            // 
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    version = result;
                    // console.log(version);
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        if (lastVersion != undefined) {
                            currentVersion.splice(currentVersion.length, 0, lastVersion);
                        }
                        this.versions1 = currentVersion;
                    } else {
                        this.versions1 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
            this.loading = false;
        } else {
            this.versions1 = "";
            this.loading = false;
        }
    }

    findVersion2(value) {
        if (value != null) {
            this.loading = true;
            var version: any;
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         this.service.httpClientGet("api/Qualifications/Version/" + value + "/" + this.useraccesdata.UserId, version)
            //             .subscribe(result => {
            //                 version = result;
            //                 if (version.length > 0) {
            //                     let currentVersion = [];
            //                     var anyprevious = false;
            //                     //Reposisi index previous seharusnya di pilihan paling bawah
            //                     for (let i = 0; i < version.length; i++) {
            //                         if (version[i].Version != 'Previous') {
            //                             currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
            //                         } else {
            //                             anyprevious = true;
            //                         }
            //                     }

            //                     if (anyprevious) {
            //                         let lastVersion = version.find(o => o.Version === 'Previous');
            //                         if (lastVersion != undefined) {
            //                             currentVersion.splice(currentVersion.length, 0, lastVersion);
            //                         }
            //                     }

            //                     this.versions2 = currentVersion;
            //                 } else {
            //                     this.versions2 = [{ ProductVersionsId: '0', Version: "No version found" }];
            //                 }
            //             });
            //     }
            // } else {
            //     this.service.httpClientGet("api/Product/Version/" + value, version)
            //         .subscribe(result => {
            //             version = result;
            //             if (version.length > 0) {
            //                 let currentVersion = [];
            //                 //Reposisi index previous seharusnya di pilihan paling bawah
            //                 for (let i = 0; i < version.length; i++) {
            //                     if (version[i].Version != 'Previous') {
            //                         currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
            //                     }
            //                 }
            //                 let lastVersion = version.find(o => o.Version === 'Previous');
            //                 if (lastVersion != undefined) {
            //                     currentVersion.splice(currentVersion.length, 0, lastVersion);
            //                 }
            //                 this.versions2 = currentVersion;
            //             } else {
            //                 this.versions2 = [{ ProductVersionsId: '0', Version: "No version found" }];
            //             }
            //         });
            // }
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    version = result;
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        if (lastVersion != undefined) {
                            currentVersion.splice(currentVersion.length, 0, lastVersion);
                        }
                        this.versions2 = currentVersion;
                    } else {
                        this.versions2 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
            this.loading = false;
        } else {
            this.versions2 = "";
            this.loading = false;
        }
    }

    getDictionary(parent, status) {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var dataTemp: any;
        var arrange = [];
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
            .subscribe(result => {
                this.typeOf = result;
                this.typeOf.sort(compare);
            }, error => { this.service.errorserver(); });
    }

    getSurveyIndicator() {
        var dataTemp: any;
        var parent = "FYIndicator";
        var status = "A";
        var currentYear: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
            .subscribe(result => {
              this.survey = result;
              var today: Date = new Date();
              var activityDateTemp = today.toString();
              var activityDate = activityDateTemp.split('-');
              var month = parseInt(activityDate[1]);
              if (today.getDate() >= 1 && month >= 2) {
                currentYear = this.survey[this.survey.length - 1].Key;
              }
              else {
                currentYear = today.getFullYear()-1;
              }
              
                this.addCourse.patchValue({ Survey: currentYear });
            }, error => { this.service.errorserver(); });
    }

    getEvaTemplate() {
        var evaTemp: any;
        this.service.httpClientGet("api/EvaluationQuestion", evaTemp)
            .subscribe(result => {
                this.evaTemplate = result;
            }, error => { this.service.errorserver(); });
    }

    getCertificateTemplate() {
        var certificate: any;
        this.service.httpClientGet("api/Courses/Certificate", certificate)
            .subscribe(result => {
                this.cftTemplate = result;
            }, error => { this.service.errorserver(); });
    }

    resetDate() {
        // this.addCourse.reset({
        //     'StartDate': null,
        //     'EndDate': null
        // });
        this.addCourse.controls['EndDate'].reset();
        this.dateStart = null;
        this.dateEnd = null;
    }

    onSelectDateStart(date: NgbDateStruct) {
        if (date != null) {
            this.dateStart = this.parserFormatter.format(date);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }

    onSelectDateEnd(date: NgbDateStruct) {
      if (date != null) {
          this.dateEnd = this.parserFormatter.format(date);
          this.checkEndDate(this.dateStart, this.dateEnd);
            // this.checkDate(this.dateStart, this.dateEnd);
            // console.log(this.dateStart);
        }
    }

    // checkAvailableFY(year) {
    //     var tempYear: any;
    //     this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Key':'" + year + "'}", tempYear)
    //         .subscribe(result => {
    //             tempYear = result;
    //             if (tempYear.length == 1) {
    //                 this.addCourse.patchValue({ Survey: tempYear[tempYear.length - 1].Key });
    //             } else {
    //                 swal('Information!', 'FY Not Found', 'error');
    //             }
    //         }, error => {
    //             this.service.errorserver();
    //         });
    // }

    checkDate(dateStart, dateEnd) {
        var today: Date = new Date();
        var startCourse: Date = new Date(dateStart);
        var endCourse: Date = new Date(dateEnd);
        var data: any;
        if (dateStart != undefined && dateEnd != undefined) {
            if (endCourse.getTime() < startCourse.getTime()) {
                swal(
                    'Information!',
                    'The start date exceeds the end date',
                    'error'
                );
                this.resetDate();
            }
            // else {
            //     var lastYear = today.getFullYear() - 1;
            //     var currYear = today.getFullYear();
            //     var intervalStart: Date = new Date(lastYear + '-03-01');
            //     var intervalEnd: Date = new Date(currYear + '-02-28');
            //     if (startCourse.getTime() <= intervalStart.getTime()) {
            //         this.checkAvailableFY(lastYear);
            //     } else if ((startCourse.getTime() >= intervalStart.getTime()) && (startCourse.getTime() <= intervalEnd.getTime())) {
            //         this.checkAvailableFY(currYear);
            //     } else {
            //         this.checkAvailableFY(currYear);
            //     }
            // }
        }
    }


  checkEndDate(dateStart, dateEnd) {
    var startCourse: Date = new Date(dateStart);
    var endCourse: Date = new Date(dateEnd);
    var activityDateTemp = dateEnd.toString();
    var activityDate = activityDateTemp.split('-');
    var month = parseInt(activityDate[1]);
    if (dateEnd != undefined) {
      if (endCourse.getTime() < startCourse.getTime()) {
        swal(
          'Information!',
          'The start date exceeds the end date',
          'error'
        );
        this.resetDate();
      }
      else {
          var dataTemp: any;
          var parent = "FYIndicator";
          var status = "A";
          var currentYear: any;
          this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
            .subscribe(result => {
              this.survey = result;
              currentYear = this.survey[this.survey.length - 1].Key;
              if (endCourse.getDate() >= 1 && month >= 2) {
                if (currentYear > endCourse.getFullYear()) {
                  currentYear = endCourse.getFullYear();
                }
              }
              else {
                if (currentYear >= endCourse.getFullYear()) {
                  currentYear = endCourse.getFullYear()-1;
                }
              }
              this.addCourse.patchValue({ Survey: currentYear });
            }, error => { this.service.errorserver(); });
      }
    }
  }

  
    roleinstructor: boolean = false;
    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole();
        this.itsinstructor();
        this.itsOrganization();
        this.itsSite();
        this.itsDistributor();
		// let options = new RequestOptions({ headers: new Headers() });
		// var token = this.session.getToken();
		// options.headers.set("Authorization", 'Bearer '+ token);

        setTimeout(() => {
            if (this.adminya) {
                if (this.trainerya) {
                    this.contactId = this.useraccesdata.InstructorId;
                    this.ContactName = this.useraccesdata.ContactName;
                    this.roleinstructor = true;
                }
            }

            let status = ['"A"', '"H"', '"I"'];
            let siteUrl: any;
            if (this.adminya) {
                if(this.distributorya){
                    siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Distributor':'" + this.urlGetOrgId() + "'}";
                }else{
                    if(this.orgya){
                        siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Organization':'" + this.urlGetOrgId() + "'}";
                    }else{
                        if(this.siteya){
                            siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','SiteId':'" + this.urlGetSiteId() + "'}";
                        }else{
                            siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','ContactId':'" + this.useraccesdata.UserId + "'}";
                        }
                    }
                }
            }else {
               // siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
               siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.getPartnerSelected() + "'}";
            }
			
            this.dataService1 = this.completerService.remote(
                null,
                "Sites",
                "Sites");
				//this.dataService1.requestOptions(options);
            this.dataService1.urlFormater(term => {
                return siteUrl + "/" + term;
            });
            this.dataService1.dataField("results");
        }, 3000);

        /* call function get user level id (issue31082018)*/

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         // this.siteId = this.useraccesdata.PrimarySiteId;
        //         this.contactId = this.useraccesdata.InstructorId;
        //         this.ContactName = this.useraccesdata.ContactName;
        //         this.roleinstructor = true;
        //     }
        // }

        // this.getDataSiswa();
        this.getPartnerType();
        // this.getSite();
        this.getProduct();
        this.getSurveyIndicator();
        this.getEvaTemplate();
        this.getCertificateTemplate();
        this.getTrainingHours();
        this.addCourse.controls["Komen"].disable();
        this.addCourse.controls["Komen5"].disable();
        this.ATC1 = true;
        this.ATC2 = true;
        this.ATC3 = true;
        this.ATC4 = true;
        this.ATC5 = true;
        this.parentLabel = true;

        // Geo
        // var data = '';
        // this.service.httpClientGet("api/Geo", data)
        //     .subscribe(result => {
        //         this.data = result;
        //         this.dropdownListGeo = this.data.map((item) => {
        //             return {
        //                 id: item.geo_code,
        //                 itemName: item.geo_name
        //             }
        //         })
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });
        // this.selectedItemsGeo = [];

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        //setting dropdown
        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
           // classes: "myclass custom-class",
            disabled: false,
            //maxHeight: 120,
            badgeShowLimit: 5
        };
    }
	
           

           getSites(){
           if (this.adminya) {
               if (this.trainerya) {
                   this.contactId = this.useraccesdata.InstructorId;	
                   this.ContactName = this.useraccesdata.ContactName;	
                   this.roleinstructor = true;	
               }	
           }	
           let status = ['"A"', '"H"', '"I"'];	
           let siteUrl: any;
           let tmpD = null;	
           if (this.adminya) {	
                if(this.distributorya){
                   siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.getPartnerSelected() + "','Distributor':'" + this.urlGetOrgId() + "','EditCourse':'false'}/" + tmpD ;	
               }else{	
                   if(this.orgya){	
                       siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.getPartnerSelected() + "','Organization':'" + this.urlGetOrgId() + "','EditCourse':'false'}/" + tmpD ;	
                   }else{
                       if(this.siteya){	
                           siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.getPartnerSelected() + "','SiteId':'" + this.urlGetSiteId() + "','EditCourse':'false'}/" + tmpD ;	
                       }else{
                           siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.getPartnerSelected() + "','ContactId':'" + this.useraccesdata.UserId + "','EditCourse':'false'}/" + tmpD ;	
                       }	
                   }	
               }		
           }else {	
               siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.getPartnerSelected() + "','EditCourse':'false'}/" + tmpD ;		
           }		
           this.service.httpClientGet(siteUrl,{}).subscribe((res:any)=>{	
               this.listSites = res.results.map((el)=>{	
                   let split = el.Sites.split('|')	
                   return {	
                       id: split[0].trim(),	
                       itemName: el.Sites	
                   }	
               })	
           })	
       }	
           getPartnerSelected(): string {	
           return this.partnerSelected.toString();
    }

    getTrainingHours() {
        function compare(a, b) {
            // Use toUpperCase() to ignore character casing
            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var hour: any;
        var hour_temp = [];
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'ATCHours','Status':'A'}", hour)
            .subscribe(res => {
                hour = res;
                for (let i = 0; i < hour.length; i++) {
                    if (hour[i].KeyValue != '0 hour') {
                        hour_temp.push(hour[i]);
                    }
                }
                this.trainingHours = hour_temp.sort(compare);
            });
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var dataTemp: any;
        var arrange = [];
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
            .subscribe(result => {
                this.eventType = result;
                this.eventType.sort(compare);
            }, error => { this.service.errorserver(); });
    }

    selectedCertificate(newvalue) {
        this.certificateSelected = newvalue;
        if (newvalue == 1) {
            this.coursePartner = 'AAP Course';
            this.parentLabel = true;
            this.getDictionary("TrainingType", "A");
            this.getSurveyIndicator();
            this.getTrainingHours();
            this.institution = true;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.Course = true;
            this.Project = false;
            this.event = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = false;
            this.ATC5 = false;
        } else if (newvalue == 2) {
            this.coursePartner = 'AAP Project';
            this.getDictionary("ProjectType", "A");
            this.getSurveyIndicator();
            this.getTrainingHours();
            this.parentLabel = false;
            this.institution = false;
            this.projectInstitution = true;
            this.projectLogoUpload = true;
            this.Project = true;
            this.event = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC5 = false;
            this.Course = false;
        } else if (newvalue == 3) {
            this.coursePartner = 'AAP Event';
            this.getDictionary("LearnerType", "A");
            this.getEventType();
            this.getSurveyIndicator();
            this.parentLabel = false;
            this.institution = false;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.event = true;
            this.Project = false;
            this.Course = false;
            this.ATC1 = false;
            this.ATC2 = false;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC5 = false;
        } else {
            this.coursePartner = 'AAP Other';
            this.getDictionary("LearnerType", "A");
            this.getEventType();
            this.getSurveyIndicator();
            this.getTrainingHours();
            this.parentLabel = true;
            this.institution = false;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC5 = true;
        }
    }

    // arraygeoid=[];
    // onGeoSelect(item:any){ 
    //     this.arraygeoid.push('"'+item.id+'"');

    //     // Region
    //     var data = '';
    //     this.service.httpClientGet("api/Region/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListRegion = null;
    //         }
    //         else{
    //             this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.region_code,
    //                 itemName: item.region_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListSubRegion = null;
    //         }
    //         else{
    //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.subregion_code,
    //                 itemName: item.subregion_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.countries_code,
    //                 itemName: item.countries_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });


    // }

    // OnGeoDeSelect(item:any){
    //     var data = '';

    //     //split region
    //     let regionArrTmp = [];
    //     this.service.get("api/Region/filter/'"+item.id+"'",data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             regionArrTmp = null;
    //         }
    //         else{
    //             regionArrTmp = JSON.parse(result);
    //             for(var i=0;i<regionArrTmp.length;i++){
    //                 var index = this.selectedItemsRegion.findIndex(x => x.id == regionArrTmp[i].region_code);
    //                 if(index !== -1){
    //                     this.selectedItemsRegion.splice(index,1);
    //                 }
    //             }
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     //split sub region
    //     let subregionArrTmp = [];
    //     this.service.get("api/SubRegion/filter/'"+item.id+"'",data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             subregionArrTmp = null;
    //         }
    //         else{
    //             subregionArrTmp = JSON.parse(result);
    //             for(var i=0;i<subregionArrTmp.length;i++){
    //                 var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
    //                 if(index !== -1){
    //                     this.selectedItemsSubRegion.splice(index,1);
    //                 }
    //             }
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filter/'"+item.id+"'",data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             countriesArrTmp = null;
    //         }
    //         else{
    //             countriesArrTmp = JSON.parse(result);
    //             for(var i=0;i<countriesArrTmp.length;i++){
    //                 var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                 if(index !== -1){
    //                     this.selectedItemsCountry.splice(index,1);
    //                 }
    //             }
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     var index = this.arraygeoid.findIndex(x => x == '"'+item.id+'"');
    //     this.arraygeoid.splice(index,1);
    //     this.selectedItemsRegion.splice(index,1);
    //     this.selectedItemsSubRegion.splice(index,1);
    //     this.selectedItemsCountry.splice(index,1);

    //     if(this.arraygeoid.length > 0){
    //     // Region
    //     var data = '';
    //     this.service.get("api/Region/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListRegion = null;
    //         }
    //         else{
    //             this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                     id: item.region_code,
    //                     itemName: item.region_name
    //                 }
    //                 })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListSubRegion = null;
    //         }
    //         else{
    //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                     id: item.subregion_code,
    //                     itemName: item.subregion_name
    //                 }
    //                 })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                     id: item.countries_code,
    //                     itemName: item.countries_name
    //                 }
    //                 })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    //     }
    //     else{
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsRegion.splice(index,1);
    //     this.selectedItemsSubRegion.splice(index,1);
    //     this.selectedItemsCountry.splice(index,1);
    //     }
    // }

    // onGeoSelectAll(items: any){
    //     this.arraygeoid = [];

    //     for(var i=0;i<items.length;i++){
    //     this.arraygeoid.push('"'+items[i].id+'"');
    //     }

    //     // Region
    //     var data = '';
    //     this.service.get("api/Region/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListRegion = null;
    //         }
    //         else{
    //             this.dropdownListRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.region_code,
    //                 itemName: item.region_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListSubRegion = null;
    //         }
    //         else{
    //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.subregion_code,
    //                 itemName: item.subregion_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filter/"+this.arraygeoid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.countries_code,
    //                 itemName: item.countries_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }

    // onGeoDeSelectAll(items: any){
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    // }

    // arrayregionid=[];

    // onRegionSelect(item:any){
    //     this.arrayregionid.push('"'+item.id+'"');

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListSubRegion = null;
    //         }
    //         else{
    //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.subregion_code,
    //                 itemName: item.subregion_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.countries_code,
    //                 itemName: item.countries_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }

    // OnRegionDeSelect(item:any){

    //     //split sub region
    //     let subregionArrTmp = [];
    //     this.service.get("api/SubRegion/filterregion/'"+item.id+"'",data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             subregionArrTmp = null;
    //         }
    //         else{
    //             subregionArrTmp = JSON.parse(result);
    //             for(var i=0;i<subregionArrTmp.length;i++){
    //                 var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
    //                 if(index !== -1){
    //                     this.selectedItemsSubRegion.splice(index,1);
    //                 }
    //             }
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filterregion/'"+item.id+"'",data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             countriesArrTmp = null;
    //         }
    //         else{
    //             countriesArrTmp = JSON.parse(result);
    //             for(var i=0;i<countriesArrTmp.length;i++){
    //                 var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                 if(index !== -1){
    //                     this.selectedItemsCountry.splice(index,1);
    //                 }
    //             }
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     var index = this.arrayregionid.findIndex(x => x == '"'+item.id+'"');
    //     this.arrayregionid.splice(index,1);
    //     this.selectedItemsSubRegion.splice(index,1);
    //     this.selectedItemsCountry.splice(index,1);

    //     if(this.arrayregionid.length > 0){
    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListSubRegion = null;
    //         }
    //         else{
    //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                     id: item.subregion_code,
    //                     itemName: item.subregion_name
    //                 }
    //                 })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                     id: item.countries_code,
    //                     itemName: item.countries_name
    //                 }
    //                 })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    //     }
    //     else{
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsSubRegion.splice(index,1);
    //     this.selectedItemsCountry.splice(index,1);
    //     }
    // }
    // onRegionSelectAll(items: any){
    //     this.arrayregionid = [];

    //     for(var i=0;i<items.length;i++){
    //     this.arrayregionid.push('"'+items[i].id+'"');
    //     }

    //     // SubRegion
    //     var data = '';
    //     this.service.get("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListSubRegion = null;
    //         }
    //         else{
    //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.subregion_code,
    //                 itemName: item.subregion_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.countries_code,
    //                 itemName: item.countries_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }

    // onRegionDeSelectAll(items: any){
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];

    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    // }

    // arraysubregionid=[];

    // onSubRegionSelect(item:any){
    //     this.arraysubregionid.push('"'+item.id+'"');

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.countries_code,
    //                 itemName: item.countries_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }

    // OnSubRegionDeSelect(item:any){

    //     //split countries
    //     let countriesArrTmp = [];
    //     this.service.get("api/Countries/filtersubregion/'"+item.id+"'",data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             countriesArrTmp = null;
    //         }
    //         else{
    //             countriesArrTmp = JSON.parse(result);
    //             for(var i=0;i<countriesArrTmp.length;i++){
    //                 var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
    //                 if(index !== -1){
    //                     this.selectedItemsCountry.splice(index,1);
    //                 }
    //             }
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });

    //     var index = this.arraysubregionid.findIndex(x => x == '"'+item.id+'"');
    //     this.arraysubregionid.splice(index,1);
    //     this.selectedItemsCountry.splice(index,1);

    //     if(this.arraysubregionid.length > 0){
    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                     id: item.countries_code,
    //                     itemName: item.countries_name
    //                 }
    //                 })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    //     }
    //     else{
    //     this.dropdownListCountry = [];

    //     this.selectedItemsCountry.splice(index,1);
    //     }
    // }
    // onSubRegionSelectAll(items: any){
    //     this.arraysubregionid = [];

    //     for(var i=0;i<items.length;i++){
    //     this.arraysubregionid.push('"'+items[i].id+'"');
    //     }

    //     // Countries
    //     var data = '';
    //     this.service.get("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
    //     .subscribe(result => { 
    //         if(result=="Not found"){
    //             this.service.notfound();
    //             this.dropdownListCountry = null;
    //         }
    //         else{
    //             this.dropdownListCountry = JSON.parse(result).map((item) => {
    //                 return {
    //                 id: item.countries_code,
    //                 itemName: item.countries_name
    //                 }
    //             })
    //         } 
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }
    // onSubRegionDeSelectAll(items: any){
    //     this.dropdownListCountry = [];

    //     this.selectedItemsCountry = [];
    // }

    onCountriesSelect(item: any) {
        this.getStudentbyCountry();
    }
    OnCountriesDeSelect(item: any) {
        this.getStudentbyCountry();
    }
    onCountriesSelectAll(items: any) {
        this.getStudentbyCountry();
    }
    onCountriesDeSelectAll(items: any) {
        this.getStudentbyCountry();
    }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    getStudentbyCountry() {
        this.dataStudent = [];
        var student: any;
        if (this.selectedItemsCountry.length != 0) {
            this.loading = true;
            for (let i = 0; i < this.selectedItemsCountry.length; i++) {
                this.service.httpClientGet("api/Student/GetStudentByCountry/" + this.selectedItemsCountry[i].id, student)
                    .subscribe(result => {
                        // var finalresult = result.replace(/\t/g, "\\t")
                        // student = JSON.parse(finalresult);
                        student = result
                        if (student.length > 0) {
                            for (let j = 0; j < student.length; j++) {
                                this.dataStudent.push(student[j]);
                            }
                            this.notFound = false;
                            this.loading = false;
                        } else {
                            if (this.dataStudent.length == 0) {
                                this.notFound = true;
                            }
                            this.loading = false;
                        }
                    });
            }
            this.studentFound = true;
        } else {
            this.studentFound = false;
            // this.loading = false;
        }
    }

    refreshPickedStudent(selectedStudent) {
        var picked: any;
        this.pickedStudent = [];
        if (selectedStudent.length > 0) {
            for (let x = 0; x < selectedStudent.length; x++) {
                this.service.httpClientGet("api/Student/GetStudent/" + selectedStudent[x], picked)
                    .subscribe(result => {
                        picked = result;
                        var exist = false;
                        if (this.pickedStudent.length == 0) {
                            this.pickedStudent.push(picked);
                            this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                        } else {
                            for (let k = 0; k < this.pickedStudent.length; k++) {
                                if (picked.StudentID == this.pickedStudent[k].StudentID) {
                                    exist = true;
                                    this.studentSuccessTag = false;
                                }
                            }
                            if (exist == false && picked.StudentID != "") {
                                this.pickedStudent.push(picked);
                                this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                            }
                        }
                        console.log(this.pickedStudent)
                    }, error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                    });
            }
            this.studentPicked = true;
        } else {
            this.studentPicked = false;
        }
    }

    showAddedStudent(firstname, lastname): void {
        this.studentSuccessTag = true;
        this.successTagMessage = "You added " + this.service.decoder(firstname) + " " + this.service.decoder(lastname) + " in this course..";
        setTimeout(function () {
            this.studentSuccessTag = false;
        }.bind(this), 3000);
    }

    removeStudent(id) {
        let studentTemp = [];
        let index = this.selectedStudent.indexOf(id)
        if (index > -1) {
            this.selectedStudent.splice(index, 1);
        }

        var index2 = this.pickedStudent.findIndex(x => x.StudentID == id);
        if (index2 !== -1) {
            //Kalau pakai unshift ga akan support di IE (buat nambah array di index awal)
            this.dataStudent.unshift(this.pickedStudent[index2]);
            studentTemp = this.dataStudent;
            this.dataStudent = [];
            this.dataStudent = studentTemp;
        }

        this.refreshPickedStudent(this.selectedStudent);
    }

    addStudent(id) {
        if (this.selectedStudent.length == 0) {
            this.selectedStudent.push(id);
        } else {
            var exist = false;
            for (let i = 0; i < this.selectedStudent.length; i++) {
                if (id == this.selectedStudent[i]) {
                    exist = true;
                    this.studentErrorTag = true;
                    this.studentSuccessTag = false;
                    this.errorTagMessage = "You have choose this student..";
                    setTimeout(function () {
                        this.studentErrorTag = false;
                    }.bind(this), 3000);
                }
            }
            if (exist == false && id != "") {
                this.selectedStudent.push(id);
                this.studentErrorTag = false;
            }
        }

        var index = this.dataStudent.findIndex(x => x.StudentID == id);
        if (index !== -1) {
            this.dataStudent.splice(index, 1);
        }

        this.refreshPickedStudent(this.selectedStudent);
    }

    onChangeOtherDisable(value) {
        let new_value = ''
        this.addCourse.controls.Update.value? new_value+= 'Update, ':null
        this.addCourse.controls.Essentials.value? new_value+= 'Essentials, ':null
        this.addCourse.controls.Intermediate.value? new_value+= 'Intermediate, ':null
        this.addCourse.controls.Advanced.value? new_value+= 'Advanced, ':null
        this.addCourse.controls.Customized.value? new_value+= 'Customized, ':null
        this.addCourse.controls.Other.value? new_value+= 'Other, ':null
        this.addCourse.controls["LevelTeaching"].setValue(new_value);
        this.teachingLevel = new_value
        this.addCourse.controls['LevelTeaching'].markAsTouched();
    }

    // onChangeOther(isChecked: boolean) {
    onChangeOther(value) {
        // isChecked ? this.addCourse.controls["Komen"].enable() : this.addCourse.controls["Komen"].disable();
        let new_value = ''
        this.addCourse.controls.Update.value? new_value+= 'Update, ':null
        this.addCourse.controls.Essentials.value? new_value+= 'Essentials, ':null
        this.addCourse.controls.Intermediate.value? new_value+= 'Intermediate, ':null
        this.addCourse.controls.Advanced.value? new_value+= 'Advanced, ':null
        this.addCourse.controls.Customized.value? new_value+= 'Customized, ':null
        this.addCourse.controls.Other.value? new_value+= 'Other, ':null
        this.addCourse.controls["LevelTeaching"].setValue(new_value);
        this.teachingLevel = new_value
        if(value == false)
            this.addCourse.controls["Komen"].disable();
        else
        this.addCourse.controls["Komen"].enable();
        this.addCourse.controls['LevelTeaching'].markAsTouched();
    }

    onChangeOther2(isChecked: boolean) {
        isChecked ? this.addCourse.controls["Komen5"].enable() : this.addCourse.controls["Komen5"].disable();
    }

    getTemplateEvaId(value) {
        this.evaTemplateId = value;
    }

    evaTemplatePreview() {
        if (this.evaTemplateId == undefined || this.evaTemplateId == "") {
            swal(
                'Information!',
                'There is no survey template selected',
                'error'
            );
        } else {
            var newWindow = window.open('template-preview/survey-preview-eva/' + this.evaTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
            return false;
        }
    }

    getTemplateCertificateId(value) {
        this.certificateTemplateId = value;
    }

    certificatePreview() {
        if (this.certificateTemplateId == undefined || this.certificateTemplateId == "") {
            swal(
                'Information!',
                'There is no certificate template selected',
                'error'
            );
        } else {
            var newWindow = window.open('template-preview/certificate-preview-eva/' + this.certificateTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
            return false;
        }
    }

    // insertToCourseStudent(data) {
    //     if (this.pickedStudent.length > 0) {
    //         var student: any;
    //         for (let y = 0; y < this.pickedStudent.length; y++) {
    //             var studentTemp = {
    //                 'CourseId': this.addCourse.value.CourseId,
    //                 'StudentID': this.pickedStudent[y].StudentID,
    //                 'Status': this.addCourse.value.Status,
    //                 'CourseTaken': 0,
    //                 'SurveyTaken': 0,
    //                 'StudentName': this.pickedStudent[y].Firstname+" "+this.pickedStudent[y].Lastname,
    //                 'Email': this.pickedStudent[y].Email
    //             };
    //             let studentData = JSON.stringify(studentTemp);
    //             this.service.post("api/Courses/CourseStudent", studentData)
    //                 .subscribe(result5 => {
    //                     student = JSON.parse(result5);
    //                     if (student["code"] == '1') {
    //                         // this.service.openSuccessSwal(student["message"]);
    //                         console.log(student["message"])
    //                     } else {
    //                         swal(
    //                             'Information!',
    //                             'Error When Insert Into Course Student Table',
    //                             'error'
    //                         );
    //                         this.loading = false;
    //                     }
    //                 }, error => { this.service.errorserver(); this.loading = false; });
    //         }
    //         this.router.navigate(['/course/course-list']);
    //         this.loading = false;
    //     } else {
    //         this.router.navigate(['/course/course-list']);
    //         this.loading = false;
    //     }
    // }

    insertToCTM(data) {
        // let autodesk = (this.addCourse.value.Autodesk == "") ? false : true;
        // let aap = (this.addCourse.value.AAP == "") ? false : true;
        // let atc = (this.addCourse.value.ATC == "") ? false : true;
        // let independent = (this.addCourse.value.Independent == "") ? false : true;
        // let independentonline = (this.addCourse.value.IndependentOnline == "") ? false : true;
        // let atconline = (this.addCourse.value.ATCOnline == "") ? false : true;
        // let other5 = (this.addCourse.value.Other5 == "") ? false : true;

        var l: any;
        var ctm = {
            'CourseId': data["CourseId"],
            'Autodesk': data["Autodesk"],
            'AAP': data["AAP"],
            'ATC': data["ATC"],
            'Independent': data["Independent"],
            'IndependentOnline': data["IndependentOnline"],
            'ATCOnline': data["ATCOnline"],
            'Other5': data["Other5"],
            'Komen5': data["Komen5"],
            'Status': data["Status"]
        };
        let courseTrainingMaterial = JSON.stringify(ctm);
        this.service.httpClientPost("api/Courses/CourseTrainingMaterial", courseTrainingMaterial)
            .subscribe(result4 => {
                l = result4;
                if (l["code"] == '1') {
                    // this.insertToCourseStudent(data);
                    this.putToLocalStorage(data);
                    setTimeout(() => {
                        this.router.navigate(['/course/course-list']);
                        this.loading = false;
                    }, 500)
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Training Material Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    putToLocalStorage(data) {
        // var dataJson = JSON.parse(data)
        // console.log(dataJson);
        let cert: any;
        // let certdropdown: any;
        let selcert: any;
        // let selcertdropdown: any;

        var selectedPartner: any;
        if (data["PartnerType"] == "ATC" || data["PartnerType"] == "1") {
            selectedPartner = [{ id: "ATC", itemName: "Authorized Training Center (ATC)" }];
        } else if (data["PartnerType"] == "AAP Course" || data["PartnerType"] == "AAP Project" || data["PartnerType"] == "AAP Event" || data["PartnerType"] == "58") {
            selectedPartner = [{ id: "AAP", itemName: "Authorized Academic Partner (AAP)" }];
        } else {
            selectedPartner = [{ id: "ATC", itemName: "Authorized Training Center (ATC)" }, { id: "AAP", itemName: "Authorized Academic Partner (AAP)" }];
        }

        // if(dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event"){
        if (data["PartnerType"] == "AAP Course" || data["PartnerType"] == "AAP Project" || data["PartnerType"] == "AAP Event" || data["PartnerType"] == "58") {
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", '')
                .subscribe(res => {
                    cert = res;
                    this.certdropdown = cert.map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Key':'" + data["CertificateType"] + "'}", '')
                        .subscribe(res => {
                            selcert = res;
                            this.selcertdropdown = selcert.map((item) => {
                                return {
                                    id: item.Key,
                                    itemName: item.KeyValue
                                }
                            })
                        });
                });
        } else if (data["PartnerType"] == "ATC" || data["PartnerType"] == "1") { //issue (salah populate certificate pas edit / add atc) ASAP 25/07/2018
            this.certdropdown = [];
            this.selcertdropdown = [];
        }
        else {
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", '')
                .subscribe(res => {
                    cert = res;
                    this.certdropdown = cert.map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.selcertdropdown = cert.map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                });
        }

        setTimeout(() => {
            var params =
            {
                idCourse: data["CourseId"],
                selectPartner: selectedPartner,
                dropPartner: this.dropdownPartner.map((item) => {
                    return {
                        id: item.RoleCode,
                        itemName: item.RoleName
                    }
                }),
                dropCerti: this.certdropdown,
                selectCerti: this.selcertdropdown,
                idOrg: "",
                idSite: "",
                idInstructor: "",
                titleCourse: "",
                courseStart: data["StartDate"],
                courseEnd: data["EndDate"],
                year: "",
                sortV: "CourseId",
                sortD: "ASC"
            }

            localStorage.setItem("filter", JSON.stringify(params));
        }, 3000)
    }

    insertToCTF(data) {
        // let instructorled = (this.addCourse.value.InstructorLed == "") ? false : true;
        // let online = (this.addCourse.value.Online == "") ? false : true;

        var k: any;
        var ctf = {
            'CourseId': data["CourseId"],
            'InstructorLed': data["InstructorLed"],
            'Online': data["Online"],
            'Status': data["Status"]
        };
        // console.log(ctf);
        let courseTrainingFormat = JSON.stringify(ctf);
        this.service.httpClientPost("api/Courses/CourseTrainingFormat", courseTrainingFormat)
            .subscribe(result3 => {
                k = result3;
                if (k["code"] == '1') {
                    this.insertToCTM(data);
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Training Format Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCourseSoftware(data) {
        var j: any;
        var version1;
        var version2;
        if (data["Version1"] != null && data["Version1"] != "") {
            version1 = data["Version1"];
        } else {
            version1 = 0; //Menandakan tidak mempunyai versi, silahkan diperbaiki jika sudah ada ketetapan
        }

        if (data["Version2"] != null && data["Version2"] != "") {
            version2 = data["Version2"];
        } else {
            version2 = 0; //Menandakan tidak mempunyai versi, silahkan diperbaiki jika sudah ada ketetapan
        }

        var cs = {
            'CourseId': data["CourseId"],
            'Product1': data["Product1"],
            'Version1': version1,
            'Product2': data["Product2"],
            'Version2': version2,
            'Status': data["Status"]
        };
        let courseSoftware = JSON.stringify(cs);
        this.service.httpClientPost("api/Courses/CourseSoftware", courseSoftware)
            .subscribe(result2 => {
                j = result2;
                if (j["code"] == '1') {
                    if (this.addCourse.value.CertificateType == 2 || this.addCourse.value.CertificateType == 3) {
                        // this.insertToCourseStudent(data);
                        this.putToLocalStorage(data);
                        setTimeout(() => {
                            this.router.navigate(['/course/course-list']);
                            this.loading = false;
                        }, 500)
                    } else {
                        this.insertToCTF(data);
                    }
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Software Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCTL(data) {
        // let update = (this.addCourse.value.Update == "") ? false : true;
        // let essentials = (this.addCourse.value.Essentials == "") ? false : true;
        // let intermediate = (this.addCourse.value.Intermediate == "") ? false : true;
        // let advanced = (this.addCourse.value.Advanced == "") ? false : true;
        // let customized = (this.addCourse.value.Customized == "") ? false : true;
        // let other = (this.addCourse.value.Other == "") ? false : true;

        var i: any;
        var ctl = {
            'CourseId': data["CourseId"],
            'Update': data["Update"],
            'Essentials': data["Essentials"],
            'Intermediate': data["Intermediate"],
            'Advanced': data["Advanced"],
            'Customized': data["Customized"],
            'Other': data["Other"],
            'Komen': data["Komen"],
            'Status': data["Status"]
        };
        
        let courseTeachingLevel = JSON.stringify(ctl);
        this.service.httpClientPost("api/Courses/CourseTeachingLevel", courseTeachingLevel)
            .subscribe(result1 => {
                i = result1;
                if (i["code"] == '1') {
                    this.insertToCourseSoftware(data);
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Teaching Level Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCourse(data) {
        var x: any;
        this.service.httpClientPost("api/Courses", data)
            .subscribe(result => {
                // console.log(result)
                x = result;
                if (x["code"] == '1') {
                    this.lastCourseId = x["lastInsertId"];
                    // this.addCourse.value.CourseId = this.lastCourseId;
                    // let newDataCourse = JSON.stringify(this.addCourse.value);
                    this.kurir["CourseId"] = this.lastCourseId;
                    if (this.addCourse.value.CertificateType == 3) {
                        this.insertToCourseSoftware(this.kurir);
                    } else if (this.addCourse.value.CertificateType == 2) {
                        this.insertToCTL(this.kurir);
                    } else {
                        this.insertToCTL(this.kurir);
                    }
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    fileProject: FileList;
    fileChange(event) {
        this.fileProject = event.target.files;
        console.log(this.fileProject);
    }

    onFileChange(event) {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.addCourse.get('InstitutionLogo').setValue({
              filename: file.name,
              filetype: file.type,
              value: (<string>reader.result).split(',')[1]
            })
          };
        }
    }

    onSubmit() {
        // let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        // if(format.test(this.addCourse.value.CourseTitle))
        //     return swal('ERROR','Special character not allowed in CourseTitle','error')
        if (this.partnerSelected == 1) {
            this.addCourse.removeControl('CertificateType');
            this.addCourse.removeControl('Institution');
            this.addCourse.removeControl('TrainingType');
            this.addCourse.removeControl('ProjectType');
            this.addCourse.removeControl('EventType');
            this.addCourse.removeControl('LearnerType');
            this.addCourse.removeControl('LocationName');
            this.addCourse.controls['CourseFacility'].markAsTouched();
            this.addCourse.controls['CourseEquipment'].markAsTouched();
            // this.addCourse.controls['LevelTeaching'].markAsTouched(); /* valid active on secon save */
            this.addCourse.controls['TrainingHours'].markAsTouched();
        } else {
            if (this.certificateSelected == 1) {
                this.addCourse.removeControl('CourseFacility');
                this.addCourse.removeControl('CourseEquipment');
                this.addCourse.removeControl('ProjectType');
                this.addCourse.removeControl('EventType');
                this.addCourse.removeControl('LearnerType');
                this.addCourse.removeControl('LocationName');
                this.addCourse.controls['Institution'].markAsTouched();
                this.addCourse.controls['TrainingType'].markAsTouched();
                this.addCourse.controls['TrainingHours'].markAsTouched();
                // this.addCourse.controls['LevelTeaching'].markAsTouched(); /* valid active on secon save */
                this.addCourse.value.CourseFacility = 0;
                this.addCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 2) {
                this.addCourse.removeControl('CourseFacility');
                this.addCourse.removeControl('CourseEquipment');
                this.addCourse.removeControl('TrainingType');
                this.addCourse.removeControl('EventType');
                this.addCourse.removeControl('LearnerType');
                this.addCourse.removeControl('LocationName');
                this.addCourse.controls['Institution'].markAsTouched();
                this.addCourse.controls['ProjectType'].markAsTouched();
                this.addCourse.controls['TrainingHours'].markAsTouched();
                // this.addCourse.controls['LevelTeaching'].markAsTouched(); /* valid active on secon save */
                this.addCourse.value.CourseFacility = 0;
                this.addCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 3) {
                this.addCourse.removeControl('CourseFacility');
                this.addCourse.removeControl('CourseEquipment');
                this.addCourse.removeControl('Institution');
                this.addCourse.removeControl('TrainingType');
                this.addCourse.removeControl('ProjectType');
                this.addCourse.removeControl('TrainingHours');
                this.addCourse.removeControl('LevelTeaching');
                this.addCourse.controls['LocationName'].markAsTouched();
                this.addCourse.controls['EventType'].markAsTouched();
                this.addCourse.controls['LearnerType'].markAsTouched();
                this.addCourse.value.CourseFacility = 0;
                this.addCourse.value.CourseEquipment = 0;
            }
            this.addCourse.controls['CertificateType'].markAsTouched();
        }
		
        this.addCourse.controls['PartnerType'].markAsTouched();
        this.addCourse.controls['CourseTitle'].markAsTouched();
        this.addCourse.controls['SiteId'].markAsTouched();
        this.addCourse.controls['ContactId'].markAsTouched();
        this.addCourse.controls['StartDate'].markAsTouched();
        this.addCourse.controls['EndDate'].markAsTouched();
        this.addCourse.controls['Survey'].markAsTouched();
        this.addCourse.controls['Product1'].markAsTouched();

        // this.addCourse.controls['Version1'].markAsTouched();
        // this.addCourse.controls['Product2'].markAsTouched();
        // this.addCourse.controls['Version2'].markAsTouched();

        // Dulu ini dipakai dengan cara dipilih, sekarang sudah ditentukan berdasarkan certificate Type dan FYIndicator
        // this.addCourse.controls['EvaTemplate'].markAsTouched(); 
        // this.addCourse.controls['CertTemplate'].markAsTouched();

        /* teaching level validation active when all field is fill */
        var list = [];
        const controls = this.addCourse.controls;
		
        for (const name in controls) {
            if (controls[name].invalid) {
                list.push(name);
            }
        }
        
        if(list.length == 1 && list[0] == "LevelTeaching"){
            if (this.partnerSelected == 1) {
                this.addCourse.controls['LevelTeaching'].markAsTouched();
            } else {
                if (this.certificateSelected == 1) {
                    this.addCourse.controls['LevelTeaching'].markAsTouched();
                } else if (this.certificateSelected == 2) {
                    this.addCourse.controls['LevelTeaching'].markAsTouched();
                }
            }
        }
        /* teaching level validation active when all field is fill */
        if (this.addCourse.valid) {
            let formData = this.addCourse.value
            let htmlString = '<table class="preview-table" style="table-layout: fixed; width: 100%;">'
            Object.keys(formData).map((key)=>{
                if(formData[key] != "" && formData[key] != null && key!='CertificateType'){
					
                    let label = key.replace(/([A-Z])/g, ' $1').trim()
                    let content = formData[key]						    						
                    if(key=='PartnerType')
                        content = this.dropdownPartner.find((el)=> el.RoleId == content)['RoleName']
					if(key=='Product1')
                        label = 'Primary Product'
					if(key=='Version1')
						label = 'Primary Version'
					if(key=='Product2')
						label = 'Secondary Product'
					if(key=='Version2')
						label = 'Secondary Version'
					if(key=='Autodesk')
						label = 'Autodesk Official Courseware (any Autodesk branded material)'						
					if(key=='InstructorLed')
						label = 'Instructor-led in the classroom'					
					if(key=='Online')
						label = 'Online or e-learning'	
					if(key=='AAP')
						label='Autodesk Authors and Publishers (AAP) Program Courseware'
					if(key=='ATC')
						label='Course material developed by the ATC'
					if(key=='Independent')
						label = 'Course material developed by an independent vendor or instructor'		
					if(key=='IndependentOnline')
						label = 'Online e-learning course material developed by an independent vendor or instructor'
					if(key=='ATCOnline')
						label='Online e-learning course material developed by the ATC'
					if(key=='Komen5')
						label='Other'
                    if(key=='ContactId')
                        label = 'Instructor ID'
                    if(key=='StartDate' || key=='EndDate')
                        content = content.day+'/'+content.month+'/'+content.year
                    if(key=='CourseFacility')
                        content = formData[key] == 1 ? 'ATC Facility':'Onsite'
                    if(key=='CourseEquipment')
                        content = formData[key] == 1 ? 'ATC Computer':'Onsite'
                    if(key=='Product1')						
                        content = this.products.find((el)=> el.productId == content)['productName']
                    if(key=='Version1')						
                        content = this.versions1.find((el)=> el.ProductVersionsId == content)['Version']
                    if(key=='Product2')						
                        content = this.products.find((el)=> el.productId == content)['productName']
                    if(key=='Version2')				
                        content = this.versions2.find((el)=> el.ProductVersionsId == content)['Version']
					if(key=='TrainingHours')
						if(this.trainingHoursOther != undefined){
							content = this.trainingHoursOther +" hours";
						
						}	
                    if(key=='SiteId')
                       content = formData[key][0].id						
					if(key=='Autodesk')						
						content='Yes'
					if(key=='InstructorLed')						
						content='Yes'
					if(key=='Online')						
						content='Yes'
					if(key=='ATC')
						content='Yes'
					if(key=='AAP')
						content='Yes'
					if(key=='Independent')
						content='Yes'
					if(key=='IndependentOnline')
						content='Yes'
					if(key=='ATCOnline')
						content='Yes'
                    if(key=='InstitutionLogo')
                        content = '<img style="height:70px;width:70px;" src='+ 'data:'+formData[key].filetype+';base64,'+formData[key].value+'></img>'
                    if(key=='ProjectType')
                        content = this.typeOf.find((el)=> el.Key == content)['KeyValue']
                    if(key=='EventType')						
                    content = this.eventType.find((el)=> el.Key == content)['KeyValue']
                    if(key=='LearnerType')						
                    content = this.typeOf.find((el)=> el.Key == content)['KeyValue']
                    if(key=='TrainingType')						
                    content = this.typeOf.find((el)=> el.Key == content)['KeyValue']
					if(key!='ContactName' && key!='Other5')
						htmlString+= '<tr><th width="330" align="left" style="width:80%;"><div  style="word-wrap: break-word;white-space:normal;">'+label+'</div></th><td align="left" style="width:80%"><div  style="word-wrap: break-word;white-space:normal;">'+content+'</div></td></tr>'
                }
            })
			
            htmlString+='</table>'
            swal({
                title: 'Are you sure?',
                text: "If you wish to Add Course!",
                width: "50rem",
                html: htmlString,//'<table style="width:100%">'+
                      //  '<tr><th>Partner Type</th><td>'+this.coursePartner+'</td></tr>'+
                      //  '<tr><th>Title</th><td>'+this.addCourse.value.CourseTitle+'</td></tr>'+
                      //  '<tr><th>Facility</th><td>'+this.addCourse.value.CourseFacility+'</td></tr>'+
                      //  '<tr><th>Equipment</th><td>'+this.addCourse.value.CourseEquipment+'</td></tr>'+
                      //  '<tr><th>Site Id</th><td>'+this.addCourse.value.SiteId+'</td></tr>'+
                      //  '<tr><th>Contact Id</th><td>'+this.addCourse.value.ContactId+'</td></tr>'+
                      //  '<tr><th>StartDate</th><td>'+this.addCourse.value.StartDate+'</td></tr>'+
                      //  '<tr><th>EndDate</th><td>'+this.addCourse.value.EndDate+'</td></tr>'+
                      //  '<tr><th>Survey FYI</th><td>'+this.addCourse.value.Survey+'</td></tr>'+
                      //  '<tr><th>Product1</th><td>'+this.addCourse.value.Product1+'-'+this.addCourse.value.Version1+'</td></tr>'+
                      //  '<tr><th>Product2</th><td>'+this.addCourse.value.Product2+'</td></tr>'+
                    //'</table>',
				//'<label style="padding: 0px !important;margin:auto;position:initial;text-align:right !important;margin-right:60%;width:40%;">Course Title:'+this.addCourse.value.CourseTitle+'</label>'+
				//'<label style="padding: 0px !important;margin:auto;position:initial;text-align:left !important;margin-left:40%;width:30%;">Partner Type:'+this.coursePartner+'</label>',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, sure!'
            }).then(result => {
                if (result == true) {
                    this.kurir = [];
                    this.loading = true;
                    //let siteTemp = (this.addCourse.value.SiteId).split(" | ");
                    let contactTemp = (this.contactId).split(" | ");
                    this.contactId = contactTemp[0];
					        //this.addCourse.value.SiteId = siteTemp[0];
            
                    // this.addCourse.value.ContactId = this.contactID;
                    this.addCourse.value.ContactId = this.contactId;
                    this.addCourse.value.PartnerType = this.coursePartner;

                    this.addCourse.value.StartDate = this.formatdate.dateCalendarToYMD(this.addCourse.value.StartDate);
                    this.addCourse.value.EndDate = this.formatdate.dateCalendarToYMD(this.addCourse.value.EndDate);
                    this.addCourse.value.Status = "A";
					
					
					
					if(this.addCourse.value.TrainingHours == "Other"){
						this.addCourse.value.TrainingHours=this.trainingHoursOther;
						this.addCourse.value.HoursTrainingOther=1;
					}
						
                    // if (this.addCourse.value.Update == "") this.addCourse.value.Update = false;
                    // if (this.addCourse.value.Essentials == "") this.addCourse.value.Essentials = false;
                    // if (this.addCourse.value.Intermediate == "") this.addCourse.value.Intermediate = false;
                    // if (this.addCourse.value.Advanced == "") this.addCourse.value.Advanced = false;
                    // if (this.addCourse.value.Customized == "") this.addCourse.value.Customized = false;
                    // if (this.addCourse.value.Other == "") this.addCourse.value.Other = false;
                    
                    if (this.addCourse.value.InstructorLed == "") this.addCourse.value.InstructorLed = false;
                    if (this.addCourse.value.Online == "") this.addCourse.value.Online = false;
                    if (this.addCourse.value.Autodesk == "") this.addCourse.value.Autodesk = false;
                    if (this.addCourse.value.AAP == "") this.addCourse.value.AAP = false;
                    if (this.addCourse.value.ATC == "") this.addCourse.value.ATC = false;
                    if (this.addCourse.value.Independent == "") this.addCourse.value.Independent = false;
                    if (this.addCourse.value.IndependentOnline == "") this.addCourse.value.IndependentOnline = false;
                    if (this.addCourse.value.ATCOnline == "") this.addCourse.value.ATCOnline = false;
                    if (this.addCourse.value.Other5 == "") this.addCourse.value.Other5 = false;

                    //Audit Log
                    this.addCourse.value.cuid = this.useraccesdata.ContactName;
                    this.addCourse.value.UserId = this.useraccesdata.UserId;
                    //End Audit Log
                    //allow special character
                    this.addCourse.value.CourseTitle = this.service.encoder(this.addCourse.value.CourseTitle);
                    this.kurir = this.addCourse.value;
                    let pre_process_data = {...this.addCourse.value, SiteId:this.addCourse.value.SiteId[0].id}
                   // let data = JSON.stringify(this.addCourse.value);
                   // data = data.replace(/'/g,"\\\\'");
                   let data = JSON.stringify(pre_process_data);
                    // console.log(this.addCourse.value)

                    // console.log(JSON.parse(data));

                    // this.uploadFileProject(this.fileProject)

                    this.insertToCourse(data);
                   
                }
            });
			//.catch(swal.noop);
        } else {
            //console.log("Ada yang ga valid");
            this.loading = false;
            /*
            swal(
                'Field is Required!',
                'Please enter the Add Course form required!',
                'error'
            )
            */
        }
    }

    uploadFileProject(fileproject) {
        let fileList: FileList = fileproject;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            let headers = new Headers();
            /** No need to include Content-Type in Angular 4 */
            // headers.append('Content-Type', 'multipart/form-data');

            /* autodesk plan 10 oct */
            
            this.service.httpClientPost('api/CertificateBackground/UploadFileBackground', formData)
                .subscribe(
                    data => console.log('success'),
                    error => console.log(error)
                )
            
            /* end line autodesk plan 10 oct */

        }
    }

    /* fixed issue 20092018 - Add course> reset button is not functioning */
    resetForm(){
        /*this.addCourse.reset({
            CourseId: '',
            PartnerType: '',
            CertificateType: '',
            CourseTitle: '',
            SiteId: '',
            ContactId: '',
            Institution: '',
            StartDate: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
            EndDate: '',
            Survey: this.addCourse.value.Survey,
            Update: '',
            Essentials: '',
            Intermediate: '',
            Advanced: '',
            Customized: '',
            Other: '',
            Komen: '',
            Status: '',
            Product1: '',
            Version1: '',
            Product2: '',
            Version2: '',
            TrainingHours: '',
            InstructorLed: '',
            Online: '',
            TrainingType: '',
            Autodesk: '',
            AAP: '',
            ATC: '',
            Independent: '',
            IndependentOnline: '',
            ATCOnline: '',
            Other5: '',
            Komen5: '',
            CourseFacility: '',
            CourseEquipment: '',
            StudentID: '',
            ProjectType: '',
            LocationName: '',
            EventType: '',
            LearnerType: '',
            ContactName: '',
            LevelTeaching: '',
            InstitutionLogo: null
        });
        this.selectedPartner('');*/
		this.router.routeReuseStrategy.shouldReuseRoute = function(){return false;};

		let currentUrl = this.router.url;
		
		this.router.navigateByUrl(currentUrl)
		.then(() => {
			this.router.navigated = false;
			this.router.navigate([this.router.url]);
		});
    }
    /* end line fixed issue 20092018 - Add course> reset button is not functioning */
}
