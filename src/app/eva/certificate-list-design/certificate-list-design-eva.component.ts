import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import { AppService } from "../../shared/service/app.service";
import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';


@Pipe({ name: 'dataFilterCertificat' })
export class DataFilterCertificatPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.CertificateId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Code.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-certificate-list-design-eva',
  templateUrl: './certificate-list-design-eva.component.html',
  styleUrls: [
  './certificate-list-design-eva.component.css',
  '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class CertificateListDesignEVAComponent implements OnInit {
  private _serviceUrl = 'api/Certificate';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "Name";
  public sortOrder: string = "asc";
  public language: any;
  public languageFound = false;
  useraccessdata:any
  id: string;
  public Encode_data: any;
  public dataDetail: any;
  public loading = false;

  constructor(public session: SessionService, private service: AppService,  private router: Router) { 
    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
  }

  accesAddCertificateDesignBtn: Boolean = true;
  accesViewCertificateDesignBtn: Boolean = true;
  accesEditCertificateDesignBtn: Boolean = true;
  accesDeleteCertificateDesignBtn: Boolean = true;
  ngOnInit() {
    this.accesAddCertificateDesignBtn = this.session.checkAccessButton("certificate/certificate-add-design");
    this.accesViewCertificateDesignBtn = this.session.checkAccessButton("certificate/certificate-view-design");
    this.accesEditCertificateDesignBtn = this.session.checkAccessButton("certificate/certificate-edit-design");
    this.accesDeleteCertificateDesignBtn = this.session.checkAccessButton("certificate/certificate-delete-design");

    this.loading = true;
    this.getDataIntoTable()
  }

  getDataIntoTable(){
     //get data action
     var data = '';
     this.service.httpClientGet(this._serviceUrl, data)
     .subscribe(result => {
       if (result == "Not found") {
         this.service.notfound();
         this.data = null;
       }
       else {
        //  var dataResult = result.replace(/\t/g, " ");
        //  dataResult = dataResult.replace(/\r/g, " ");
        //  dataResult = dataResult.replace(/\n/g, " ");
        //  this.data = JSON.parse(dataResult);
        this.data = result;
       }
       this.loading = false;
     });

   }

  //delete confirm
  openConfirmsSwal(id) {
    // var index = this.data.findIndex(x => x.CertificateId == id);
    // this.service.httpClientDelete(this._serviceUrl, this.data, id, index);

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
    .then(result => {
      if (result == true) {
        this.loading = true;
        var cuid = this.useraccessdata.ContactName
        var UserId = this.useraccessdata.UserId

        var data = ''

        this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data)
        .subscribe(result => {
          let tmpData : any = result;
          this.messageResult = tmpData;
          var resource = result;
          if (resource['code'] == '1') {
            setTimeout(() => {
              this.getDataIntoTable();
              this.router.navigate(['/certificate/certificate-list-design']);
              this.loading = true;                  
            }, 1000);
          }
          else {
            swal(
              'Information!',
              "Delete Data Failed",
              'error'
              );
            this.loading = true;
          }
        },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
          this.loading = true;
        });
      }
    }).catch(swal.noop);

  }

  PreviewDesignTemplate(id) {
    $("#certificate_iframe").empty();
    var iframe = document.createElement('iframe');
    iframe.src = 'template-preview/certificate-preview-eva/' + id;
    document.getElementById("certificate_iframe").appendChild(iframe);
    // $('.iframe-certificate').css({'max-width':'870px', 'padding-top': '40px'})
    // $('iframe').css({
    //   'width': '100%',
    //   'height' : '600px'
    // })

    // var data = '';

    // //get activity data detail
    // this.service.httpClientGet(this._serviceUrl+"/"+id,data)
    //   .subscribe(result => {
    //     if(result=="Not found"){
    //         this.service.notfound();
    //         this.dataDetail = '';
    //     }
    //     else{
    //         this.dataDetail = result; 
    //         this.Encode_data = atob(this.dataDetail.HTMLDesign)
    //         let fragmentFromString = function (strHTML) {
    //           return document.createRange().createContextualFragment(strHTML);
    //         }
    //         let fragment = fragmentFromString(this.Encode_data);
    //         $('#CardCanvas').css({'display':'' })
    //         $('#CardCanvas').css({"margin-top":"50%" })
    //         document.getElementById("forPreview").appendChild(fragment);
    //         this.PrintPreview()
    //     } 

    //   },
    //   error => {
    //       this.service.errorserver();
    //   });

    // var newWindow = window.open('template-preview/certificate-preview-eva/' + id, 'mywin', 'left=270,top=30,width=837,height=700,toolbar=1,resizable=0');
    // if (window.focus()) { newWindow.focus() }
    // return false;

  }

  // @ViewChild('content') content: ElementRef;
  // public PrintPreview(){
  //   html2canvas(document.getElementById('content')).then(function(canvas) {
  //     var self = this;
  //     var doc = new jspdf('l', 'pt', 'a4');
  //     var img = canvas.toDataURL("image/png");
  //     doc.addImage(img, 'PNG', 0, 0);
  //     doc.save('test.pdf');
  //     $('#CardCanvas').css({'display':'none' })
  //     $('#content').remove();
  //   });
  // }

}
