import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateListDesignEVAComponent } from './certificate-list-design-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { DataFilterCertificatPipe } from './certificate-list-design-eva.component';
import { SessionService } from '../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const CertificateListDesignEVARoutes: Routes = [
  {
    path: '',
    component: CertificateListDesignEVAComponent,
    data: {
      breadcrumb: 'eva.manage_certificate.list_certificate_design',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificateListDesignEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CertificateListDesignEVAComponent, DataFilterCertificatPipe],
  providers: [AppService, SessionService]
})
export class CertificateListDesignEVAModule { }