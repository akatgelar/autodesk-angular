import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchStudentEVAComponent } from './search-student-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from 'app/shared/service/app.service';
import { DataFilterStudentPipe } from './search-student-eva.component';
import { SessionService } from '../../shared/service/session.service';
import { Ng2CompleterModule } from "ng2-completer";
import { LoadingModule } from 'ngx-loading';

export const SearchStudentEVARoutes: Routes = [
  {
    path: '',
    component: SearchStudentEVAComponent,
    data: {
      breadcrumb: 'eva.manage_student_info.list_student',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchStudentEVARoutes),
    SharedModule,
    Ng2CompleterModule,
    LoadingModule
  ],
  declarations: [SearchStudentEVAComponent, DataFilterStudentPipe],
  providers: [AppService, SessionService]
})
export class SearchStudentEVAModule { }

