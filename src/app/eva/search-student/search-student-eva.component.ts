import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from 'app/shared/service/app.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";

@Pipe({ name: 'dataFilterStudent' })
export class DataFilterStudentPipe {
  constructor(private service: AppService) {}
    transform(array: any[], query: string): any {
        if (query) {

            return _.filter(array, row =>
                (row.StudentID.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                // (row.Firstname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Company.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.City.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.countries_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.Phone.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Email.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));

        }
        return array;
    }
}

@Component({
    selector: 'app-search-student-eva',
    templateUrl: './search-student-eva.component.html',
    styleUrls: [
        './search-student-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SearchStudentEVAComponent implements OnInit {

    // private _serviceUrl: 'api/student';
    public data: any;
    valid = true;
    public student_data: any;
    public student_detail: any;
    messageError: string = '';
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "Firstname";
    public sortOrder: string = "asc";
    student_detail_form: FormGroup;
    useraccesdata: any;
    protected dataService3: RemoteData;

    StudentFilter:FormGroup;
    public loading = false;
    
    constructor(private completerService: CompleterService, public session: SessionService, private service: AppService, private http:HttpClient) {

        //Untuk pencarian siswa berdasarkan email, name dan student id
        this.dataService3 = completerService.remote(
            null,
            "Student",
            "Student");
        this.dataService3.urlFormater(term => {
            return `api/Student/FindStudent/` + term;
        });
        this.dataService3.dataField("results");

        let StudentId = new FormControl('');
        let StudentName = new FormControl('');
        let StudentEmail = new FormControl('');

        this.StudentFilter = new FormGroup({
            StudentId: StudentId,
            StudentName: StudentName,
            StudentEmail: StudentEmail
        });
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
        

    }

    checkrole(): boolean {
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
      for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
          return false;
        } else {
          return true;
        }
      }
    }

    itsDistributor(): boolean {
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
      for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "DISTRIBUTOR") {
          return true;
        } else {
          return false;
        }
      }
    }

    onSubmit() {
        if(!this.StudentFilter.controls.StudentId.value && !this.StudentFilter.controls.StudentName.value && !this.StudentFilter.controls.StudentEmail.value)
            return this.valid = false
        else this.valid = true
      this.loading = true;
      let TerritoryId;
      if (this.checkrole()) {
        if (this.itsDistributor()) {
          TerritoryId = this.useraccesdata.TerritoryId;
         
        } else {
          TerritoryId = "";
        }
      }
      
      this.service.httpClientGet("api/Student/where/{'SL_TerritoryId':'" + TerritoryId +"','SL_StudentId':'"+this.StudentFilter.value.StudentId+"','SL_StudentName':'"+this.StudentFilter.value.StudentName+"','SL_StudentEmail':'"+this.StudentFilter.value.StudentEmail+"'}",'')
            .subscribe(result => {
                var finalresult = result;
                if (finalresult == "Not found") {
                    this.service.notfound();
                    this.data = null;
                    this.loading = false;
                }
                else {
                    this.data = finalresult;
                    for (var i = 0; i < this.data.length; i++) {
                      if (this.data[i].Firstname != null && this.data[i].Firstname != '') {
                          this.data[i].Firstname = this.service.decoder(this.data[i].Firstname);
                      }
                      if (this.data[i].Lastname != null && this.data[i].Lastname != '') {
                        this.data[i].Lastname = this.service.decoder(this.data[i].Lastname);
                    }
                    }
                    this.loading = false;
                }
            },
            error => {
                this.service.errorserver();
                this.loading = false;
            })
    }

    ketikaSiswaDipilih(value){
        this.data = '';
        this.data = [{
            StudentID : value["originalObject"].StudentID,
            Firstname : value["originalObject"].Firstname,
            Lastname : value["originalObject"].Lastname,
            Email : value["originalObject"].Email
        }];

        //Buat object untuk filter yang dipilih
        var params =
        {
            result: value
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

    }

    accesUpdateStudentInfoBtn:Boolean=true;
    ngOnInit() {
        this.accesUpdateStudentInfoBtn = this.session.checkAccessButton("studentinfo/update-student-information");
        
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.ketikaSiswaDipilih(item.result)
        } else {
          let TerritoryId;
          if (this.checkrole()) {
            if (this.itsDistributor()) {
              TerritoryId = this.useraccesdata.TerritoryId;

            } else {
              TerritoryId = 0;
            }
          }
          this.getAllStudent(TerritoryId);
        }

    }

    getAllStudent(data) {
        //get data student
      this.loading = true;
      this.service.httpClientGet('api/Student/Select/' + data, '')
            .subscribe(result => {
                var finalresult = result;
                if (finalresult == "Not found") {
                    this.service.notfound();
                    this.data = null;
                    this.loading = false;
                }
                else {
                    this.data = finalresult;
                     for (var i = 0; i < this.data.length; i++) {
                        if (this.data[i].Firstname != null && this.data[i].Firstname != '') {
                            this.data[i].Firstname = this.service.decoder(this.data[i].Firstname);
                        }
                        if (this.data[i].Lastname != null && this.data[i].Lastname != '') {
                          this.data[i].Lastname = this.service.decoder(this.data[i].Lastname);
                      }
                      }
                    this.loading = false;
                }
            },
            error => {
                this.messageError = <any>error
                this.service.errorserver();
                this.loading = false;
            });
    }

    generatePassword(value){
        this.loading = true;
        var data = {
          "StudentID":value
        }

        /* autodesk plan 10 oct */
        // this.service.httpCLientPut('api/Auth/GeneratePassStudent',data,'');
        // this.loading = false;
        // this.ngOnInit();
        this.service.httpCLientPut('api/Auth/GeneratePassStudent',data)
        .subscribe(result => {
            var res = result;
            if(res['code'] == "1"){
              swal(
                'Information!',
                'Your password has been reset.<br/>Please check your email',
                'success'
              );
              this.loading = false;
            }else{
                swal(
                    'Error',
                    'Password reset failed',
                    'error'
                );
              this.loading = false;
            }
        }, error => {
            swal(
                'Error',
                'Password reset failed',
                'error'
            );
          this.loading = false;
        });

        
          /* end line autodesk plan 10 oct */

      }

    // FindStudent(value){
    //     var timeout:any=null;
        
    //     clearTimeout(timeout);

    //     timeout = 
    //         setTimeout(() => {
    //             console.log(value)
    //             this.service.get('api/Student/FindStudent/'+value, '')
    //                 .subscribe(result => {
    //                     var finalresult = result.replace(/\t/g, " ");
    //                     if (finalresult == "Not found") {
    //                         this.data = '';
    //                     }
    //                     else {
    //                         this.data = JSON.parse(finalresult);
    //                         console.log(this.data);
    //                     }
    //                 },
    //                 error => {
    //                     this.data = '';
    //                 });
    //         }, 3000);
    // }

    // openConfirmsSwal(id) {
    //     swal({
    //         title: 'Are you sure?',
    //         text: "This action will delete all related data",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //     }).then(result => {
    //         if (result == true) {
    //             var index = this.data.findIndex(x => x.StudentID == id);
    //             setTimeout(() => {
    //                 this.service.put('api/Student/Delete/'+id, '')
    //                     .subscribe(res=>{
    //                         console.log(res)
    //                     });
    //                 this.data.splice(index,1);                    
    //             }, 1000);
    //         }
    //     }).catch(swal.noop);
    // }

//     openConfirmsSwal(id) {
      
//       var index = this.data.findIndex(x => x.StudentID == id);
//       setTimeout(() => {
//           this.service.httpCLientPut('api/Student/Delete',id, '')
//               // .subscribe(res=>{
//               //     console.log(res)
//               // });
//           this.data.splice(index,1);                    
//       }, 1000);
  
// }
openConfirmsSwal(id) {
  swal({
      title: 'Are you sure?',
      text: "This action will delete all related data",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
  }).then(result => {
      if (result == true) {
          var index = this.data.findIndex(x => x.StudentID == id);
          setTimeout(() => {
              this.service.httpCLientPut('api/Student/Delete/'+id, '')
                  .subscribe(res=>{
                      console.log(res)
                  });
              this.data.splice(index,1);                    
          }, 1000);
      }
  }).catch(swal.noop);
}
}
