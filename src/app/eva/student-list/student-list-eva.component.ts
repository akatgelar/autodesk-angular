import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Router } from '@angular/router';

import {AppService} from "../../shared/service/app.service";

@Component({
    selector: 'app-student-list-eva',
    templateUrl: './student-list-eva.component.html',
    styleUrls: [
      './student-list-eva.component.css',
      '../../../../node_modules/c3/c3.min.css', 
      ], 
    encapsulation: ViewEncapsulation.None
})

export class StudentListEVAComponent implements OnInit {

    private _serviceUrl = 'api/Activity';
    messageResult: string = '';
    messageError: string = ''; 
    public data: any;
    public datadetail: any;
    public activitytype: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";

    // Table select add student
    rows = [];
    selected = [];
    columns: any[] = [
        { prop: 'id'} ,
        { name: 'Name' }
    ];

    addstudentform: FormGroup;


    constructor(private service:AppService, public http: Http, private router: Router) { 
        
        // Table select add student
        this.fetch((data) => {
            // this.selected = [data[2]];
            this.rows = data;
        });

        let student_id = new FormControl('', Validators.required); 
        let student_name = new FormControl('', Validators.required);
        let other_attribute1 = new FormControl('', Validators.required);
        let other_attribute2 = new FormControl('', Validators.required);

        this.addstudentform = new FormGroup({
            student_id:student_id,
            student_name:student_name,
            other_attribute1:other_attribute1,
            other_attribute2:other_attribute2
        });
    }
    
    
    ngOnInit() {}

    // Table select add student
    fetch(cb) {
        const req = new XMLHttpRequest();
        req.open('GET', `assets/data/student.json`);
    
        req.onload = () => {
          cb(JSON.parse(req.response));
        };
        req.send();
    }
    onSelect({ selected }) {
        console.log('Select Event', selected, this.selected);
    }
    onActivate(event) {
        console.log('Activate Event', event);
    }

    //cange table based on combobox
    title : string = "";
    
    // changetable(newvalue){
    //     if(newvalue != ""){
    //         this.title = newvalue;
    //         //get data action
    //         var data = '';
    //         this.service.get(this._serviceUrl+"/where/{'activity_category':'"+newvalue+"'}",data)
    //         .subscribe(result => {
    //             if(result=="Not found"){
    //                 this.service.notfound();
    //                 this.data = null;
    //             }
    //             else{
    //                 this.data = JSON.parse(result);  
    //             } 
    //         },
    //         error => {
    //             this.messageError = <any>error
    //             this.service.errorserver();
    //         });
    //     }
    //     else{
    //         this.title = "";
    //         this.data = null;
    //     }
    // }
    
    //view detail
    // viewdetail(id) {
    //     var data = '';
    //     this.service.get(this._serviceUrl+'/'+id, data)
    //     .subscribe(result => {
    //         this.datadetail = JSON.parse(result);  
    //     },
    //     error => {
    //         this.messageError = <any>error
    //         this.service.errorserver();
    //     });
    // }

    //delete confirm
    // openConfirmsSwal(id, index) {
    //     swal({
    //     title: 'Are you sure?',
    //     text: "You won't be able to revert this!",
    //     type: 'warning',
    //     showCancelButton: true,
    //     confirmButtonColor: '#3085d6',
    //     cancelButtonColor: '#d33',
    //     confirmButtonText: 'Yes, delete it!'
    //     })
    //     .then(result => {  
    //         if (result == true) {
    //             var data = '';
    //             this.service.delete(this._serviceUrl+'/'+id, data)
    //             .subscribe(result => {
    //                 var resource = JSON.parse(result);
    //                 if(resource['code'] == '1') {
    //                     this.service.openSuccessSwal(resource['name']); 
    //                     var index = this.data.findIndex(x => x.activity_id == id);
    //                     if (index !== -1) {
    //                         this.data.splice(index, 1);
    //                     }    
    //                 }
    //                 else{
    //                     swal(
    //                     'Information!',
    //                     "Delete Data Failed",
    //                     'error'
    //                     );
    //                 }
    //             },
    //             error => {
    //                 this.messageError = <any>error
    //                 this.service.errorserver();
    //             });
    //         }

    //     }).catch(swal.noop);
    // }

    //submit form add student
    submittedAddStundet: boolean;
    onSubmitAddStudent() {
        this.submittedAddStundet = true;
        this.router.navigate(['/eva/student-list']);

        //convert object to json
        let data = JSON.stringify(this.addstudentform.value);
        console.log("data input -> "+data);
    }

    // success notification add contact
    openSuccessSwalAddStudent() {
        swal({
        title: 'Success!',
        text: 'Your contact has been added!',
        type: 'success'
        }).catch(swal.noop);
    }
    
    //add row organization
    private fieldArray: Array<any> = [];
    private newAttribute: any = {};

    addFieldValue() {
        this.fieldArray.push(this.newAttribute)
        this.newAttribute = {};
    }

    deleteFieldValue(index) {
        this.fieldArray.splice(index, 1);
    }
}