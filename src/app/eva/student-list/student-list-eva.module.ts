import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentListEVAComponent } from './student-list-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

export const StudentListEVARoutes: Routes = [
    {
      path: '',
      component: StudentListEVAComponent,
      data: {
        breadcrumb: 'Student Tag',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(StudentListEVARoutes),
      SharedModule,
      NgxDatatableModule
    ],
    declarations: [StudentListEVAComponent],
    providers:[AppService]
  })
  export class StudentListEVAModule { }