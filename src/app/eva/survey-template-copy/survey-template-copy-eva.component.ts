import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterSurveyTemplate' })
export class DataFilterSurveyTemplatePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.eva_code.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.certificate_type.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.eva_title.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.year.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

var surveyJSON = {
  "pages": [
    {
      "name": "page1"
    }
  ]
}

@Component({
  selector: 'app-survey-template-copy-eva',
  templateUrl: './survey-template-copy-eva.component.html',
  styleUrls: [
    './survey-template-copy-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyTemplateCopyEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "eva_title";
  public sortOrder: string = "asc";
  edittemplatedata: FormGroup;
  addtemplatedata: FormGroup;
  evaluationData = [];

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private http: HttpClient) {

    //validation
    let EvaluationQuestionCode = new FormControl('', Validators.required);
    let CourseId = new FormControl('', Validators.required);

    this.addtemplatedata = new FormGroup({
      EvaluationQuestionCode: EvaluationQuestionCode,
      CourseId: CourseId
    });

    let EvaluationQuestionCode_update = new FormControl('', Validators.required);
    let CourseId_update = new FormControl('', Validators.required);

    this.edittemplatedata = new FormGroup({
      EvaluationQuestionCode: EvaluationQuestionCode_update,
      CourseId: CourseId_update
    });

  }

  getCertificateName(certificate_id) {
    var name: string;
    switch (certificate_id) {
      case '1':
        name = "Course";
        break;
      case '2':
        name = "Project";
        break;
      case '3':
        name = "Event";
        break;
      default:
        name = "ATC";
    }
    return name;
  }

  getDataIntoTable() {
    this.evaluationData = [];
    //get data action
    var data: any;
    this.service.httpClientGet("api/EvaluationQuestion/Copy", data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.evaluationData = [];
        }
        else {
          data = result;
          for (let j = 0; j < data.length; j++) {
            var capcus = {
              'eva_code': data[j].EvaluationQuestionCode,
              'certificate_type': this.getCertificateName(data[j].CertificateTypeId),
              'eva_title': data[j].CourseId,
              'year': data[j].Year,
              'eva_id': data[j].EvaluationQuestionId
            };
            this.evaluationData.push(capcus);
          }
        }
      },
        error => {
          this.service.errorserver();
          this.data = '';
        });
  }

  ngOnInit() {
    this.getDataIntoTable();
  }

  //delete confirm
  openConfirmsSwal(id) {
    var index = this.data.findIndex(x => x.EvaluationQuestionId == id);
    //this.service.httpClientDelete(this._serviceUrl, this.data, id, index);
    this.service.httpClientDelete(this._serviceUrl+'/'+id, this.data)
  }

  editsurvey(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.datadetail = '';
        }
        else {
          this.datadetail = result;
          this.buildform();
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
        });
  }

  buildform() {
    let EvaluationQuestionId = new FormControl(this.datadetail.EvaluationQuestionId);
    let EvaluationQuestionCode = new FormControl(this.datadetail.EvaluationQuestionCode, Validators.required);
    let CourseId = new FormControl(this.datadetail.CourseId, Validators.required);
    let CountryCode = new FormControl(this.datadetail.CountryCode);
    let EvaluationQuestionTemplate = new FormControl(this.datadetail.EvaluationQuestionTemplate);
    let EvaluationQuestionJson = new FormControl(this.datadetail.EvaluationQuestionJson);
    let CreatedDate = new FormControl(this.datadetail.CreatedDate);
    let CreatedBy = new FormControl(this.datadetail.CreatedBy);

    this.edittemplatedata = new FormGroup({
      EvaluationQuestionId: EvaluationQuestionId,
      EvaluationQuestionCode: EvaluationQuestionCode,
      CourseId: CourseId,
      CountryCode: CountryCode,
      EvaluationQuestionTemplate: EvaluationQuestionTemplate,
      EvaluationQuestionJson: EvaluationQuestionJson,
      CreatedDate: CreatedDate,
      CreatedBy: CreatedBy
    });
  }

  resetFormUpdate() {
    this.edittemplatedata.reset({
      'EvaluationQuestionCode': this.datadetail.EvaluationQuestionCode,
      'CourseId': this.datadetail.CourseId
    });
  }

  onSubmitEditSurveyTemplate() {
    this.edittemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.edittemplatedata.controls['CourseId'].markAsTouched();

    if (this.edittemplatedata.valid) {
      //convert object to json
      let data = JSON.stringify(this.edittemplatedata.value);

      //post action
      
      // var index = this.data.findIndex(x => x.EvaluationQuestionId == this.edittemplatedata.value.EvaluationQuestionId);
      // this.service.httpCLientPutModal(this._serviceUrl, this.datadetail.EvaluationQuestionId, data, index, this.data, this.edittemplatedata.value);
      
      this.service.httpCLientPut(this._serviceUrl +"/"+ this.datadetail.EvaluationQuestionId, data)
        .subscribe(result => {
          if (result['code'] == '1') {
              this.ngOnInit();
          }
          else {
            console.log("failed");
          }
        },
        error => {
          console.log("failed");
        });
    
    }
  }

  onSubmit() {

    this.addtemplatedata.controls['EvaluationQuestionCode'].markAsTouched();
    this.addtemplatedata.controls['CourseId'].markAsTouched();

    if (this.addtemplatedata.valid) {
      this.addtemplatedata.value.CountryCode = "SG";
      this.addtemplatedata.value.EvaluationQuestionTemplate = surveyJSON;
      this.addtemplatedata.value.EvaluationQuestionJson = surveyJSON;
      this.addtemplatedata.value.CreatedDate = this.formatdate.dateJStoYMD(new Date());
      this.addtemplatedata.value.CreatedBy = "admin";

      //convert object to json
      let data = JSON.stringify(this.addtemplatedata.value);

      /* autodesk plan 18 oct */

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
        .subscribe(res => {
          var result = res; 
          if (result['code'] == '1') {
            this.router.navigate(['/evaluation/add-edit-form', result['EvaluationQuestionId']]);
          }
          else {
            swal(
              'Information!',
              "Insert Data Failed",
              'error'
            );
          }
        },
        error => {
          this.service.errorserver();
        });

      /* end line autodesk plan 18 oct */

    }
  }

  resetForm() {
    this.addtemplatedata.reset();
  }

}
