import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyTemplateCopyEVAComponent } from './survey-template-copy-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { DataFilterSurveyTemplatePipe } from './survey-template-copy-eva.component';

export const SurveyTemplateCopyEVARoutes: Routes = [
  {
    path: '',
    component: SurveyTemplateCopyEVAComponent,
    data:
      {
        breadcrumb: 'menu_manage_evaluation.copy_template',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyTemplateCopyEVARoutes),
    SharedModule
  ],
  declarations: [SurveyTemplateCopyEVAComponent, DataFilterSurveyTemplatePipe],
  providers:[AppService,AppFormatDate]
})
export class SurveyTemplateCopyEVAModule { }
