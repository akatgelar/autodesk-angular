import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { DatePipe } from '@angular/common';
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";

@Component({
    selector: 'app-course-evaluation-student',
    templateUrl: './course-evaluation-student.component.html',
    styleUrls: [
        './course-evaluation-student.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CourseEvaluationComponent implements OnInit {

    messageError: string = '';
    id: string;
    dropdownPartner = [];
    datadetail = [];
    selectedStudent = [];
    private data: any;
    private versions1;
    private versions2;
    private partnerSelected;
    private certificateSelected;
    pickedStudent = [];
    private errorSites: string = '';
    private errorInstructor: string = '';
    dateStart: string;
    dateEnd: string;
    private products;
    private versions;
    private typeOf;
    private trainingHours;
    private survey;
    private evaTemplate;
    private cftTemplate;
    private _serviceUrl = 'api/Courses';
    private dropdownCertificate;
    private ContactName: string;
    private evaTemplateId;
    private certificateTemplateId;
    public courseDiv = false;
    public courseEquipment = true;
    public courseFacility = true;
    public CertificateType = false;
    public CertificateTypeCol = true;
    public Course = false;
    public Project = false;
    public event = false;
    public ATC1 = true;
    public ATC2 = true;
    public ATC3 = true;
    public ATC4 = true;
    public ATC6 = true;
    public institution = false;
    protected siteId: string;
    protected contactId: string;
    protected sites = [];
    protected instructor = [];
    protected dataService1: CompleterData;
    protected dataService2: CompleterData;
    // protected dataService3: CompleterData;
    protected dataService3: RemoteData;
    public rowsOnPage: number = 5;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public studentFound = false;
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownSettings = {};
    dataStudent = [];
    protected siswa: string;
    protected siswaLookup = [];
    editCourse: FormGroup;
    modelPopup1;
    modelPopup2;
    public projectInstitution = false;
    public projectLogoUpload = false;
    public parentLabel = false;
    public loading = false;
    studentErrorTag = false;
    studentSuccessTag = false;
    errorTagMessage: string = '';
    successTagMessage: string = '';
    public studentPicked = false;
    private evalsId;
    public showEvals = false;
    dataEvals;
    public evalsNumber;
    partnerType: string = "";

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute,
        private completerService: CompleterService, private parserFormatter: NgbDateParserFormatter, private datePipe: DatePipe, private filterGeo: AppFilterGeo) {

        this.dataService1 = completerService.local(this.sites, 'SiteId', 'SiteId').descriptionField("SiteName");
        this.dataService2 = completerService.local(this.instructor, 'ContactId', 'ContactId').descriptionField("ContactName");
        // this.dataService3 = completerService.local(this.siswaLookup, 'StudentID', 'StudentID').descriptionField("Firstname");

        this.dataService3 = completerService.remote(
            null,
            "Student",
            "Student");
        this.dataService3.urlFormater(term => {
            return `api/Student/FindStudent/` + term;
        });
        this.dataService3.dataField("results");

        let CourseId = new FormControl('');
        let PartnerType = new FormControl('');
        let CertificateType = new FormControl('');
        let CourseTitle = new FormControl('', [Validators.required, Validators.maxLength(40)]);
        let SiteId = new FormControl('', Validators.required);
        let InstructorId = new FormControl('', Validators.required);
        let Institution = new FormControl('', Validators.required);
        let StartDate = new FormControl('', Validators.required);
        let EndDate = new FormControl('', Validators.required);
        let Survey = new FormControl('', Validators.required);
        let Update = new FormControl('');
        let Essentials = new FormControl('');
        let Intermediate = new FormControl('');
        let Advanced = new FormControl('');
        let Customized = new FormControl('');
        let Other = new FormControl('');
        let Komen = new FormControl('');
        let Status = new FormControl('');
        let Product1 = new FormControl('', Validators.required);
        let Version1 = new FormControl('', Validators.required);
        let Product2 = new FormControl('', Validators.required);
        let Version2 = new FormControl('', Validators.required);
        let TrainingHours = new FormControl('', Validators.required);
        let InstructorLed = new FormControl('');
        let Online = new FormControl('');
        let TrainingType = new FormControl('');
        let Autodesk = new FormControl('');
        let AAP = new FormControl('');
        let ATC = new FormControl('');
        let Independent = new FormControl('');
        let IndependentOnline = new FormControl('');
        let ATCOnline = new FormControl('');
        let Other5 = new FormControl('');
        let Komen5 = new FormControl('');
        let CourseFacility = new FormControl('', Validators.required);
        let CourseEquipment = new FormControl('', Validators.required);
        let EvaTemplate = new FormControl('', Validators.required);
        let CertTemplate = new FormControl('', Validators.required);
        let StudentID = new FormControl('');
        let ProjectType = new FormControl('', Validators.required);
        let LocationName = new FormControl('', Validators.required);
        let EventType = new FormControl('', Validators.required);

        this.editCourse = new FormGroup({
            CourseId: CourseId,
            PartnerType: PartnerType,
            CertificateType: CertificateType,
            CourseTitle: CourseTitle,
            SiteId: SiteId,
            InstructorId: InstructorId,
            Institution: Institution,
            StartDate: StartDate,
            EndDate: EndDate,
            Survey: Survey,
            Update: Update,
            Essentials: Essentials,
            Intermediate: Intermediate,
            Advanced: Advanced,
            Customized: Customized,
            Other: Other,
            Komen: Komen,
            Status: Status,
            Product1: Product1,
            Version1: Version1,
            Product2: Product2,
            Version2: Version2,
            TrainingHours: TrainingHours,
            InstructorLed: InstructorLed,
            Online: Online,
            TrainingType: TrainingType,
            Autodesk: Autodesk,
            AAP: AAP,
            ATC: ATC,
            Independent: Independent,
            IndependentOnline: IndependentOnline,
            ATCOnline: ATCOnline,
            Other5: Other5,
            Komen5: Komen5,
            CourseFacility: CourseFacility,
            CourseEquipment: CourseEquipment,
            EvaTemplate: EvaTemplate,
            CertTemplate: CertTemplate,
            StudentID: StudentID,
            ProjectType: ProjectType,
            LocationName: LocationName,
            EventType: EventType
        });
    }

    ketikaSiswaDipilih(item) {
        this.addStudent(item['originalObject'].StudentID);
    }

    getDataSiswa() {
        var murid: any;
        this.service.httpClientGet("api/Student", murid)
            .subscribe(result => {
                murid = result;
                if (murid.length > 0) {
                    for (let i = 0; i < murid.length; i++) {
                        this.siswaLookup.push(murid[i]);
                    }
                }
            }, error => {
                this.service.errorserver();
            });

    }

    getTrainingHours(hoursTraining) {
        var hour: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'ATCHours','Status':'A'}", hour)
            .subscribe(res => {
                hour = res;
                for (let i = 0; i < hour.length; i++) {
                    if (hour[i].KeyValue === hoursTraining) {
                        this.editCourse.patchValue({ TrainingHours: hour[i].Key });
                    }
                }
            });
    }

    selectedPartner(newvalue) {
        this.partnerSelected = newvalue;
        if (this.partnerSelected == 58) {
            var certificate: any;
            var parent = "AAPCertificateType";
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
                .subscribe(result => {
                    this.dropdownCertificate = result;
                }, error => {
                    this.service.errorserver();
                });
            this.courseDiv = true;
            this.courseEquipment = false;
            this.courseFacility = false;
            this.CertificateType = true;
            this.CertificateTypeCol = false;
            this.institution = true;
        } else if (this.partnerSelected == 1) {
            this.CertificateType = false;
            this.CertificateTypeCol = true;
            this.courseDiv = false;
            this.courseEquipment = true;
            this.courseFacility = true;
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC6 = true;
            this.institution = false;
        }
    }

    selectedCertificate(newvalue, hoursTraining) {
        this.certificateSelected = newvalue;
        if (newvalue == 1) {
            this.getDictionary("TrainingType", "A");
            this.getSurveyIndicator();
            this.getTrainingHours(hoursTraining);
            this.parentLabel = true;
            this.institution = true;
            this.Course = true;
            this.Project = false;
            this.event = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = false;
            this.ATC6 = false;
        } else if (newvalue == 2) {
            this.getDictionary("ProjectType", "A");
            this.getSurveyIndicator();
            this.getTrainingHours(hoursTraining);
            this.parentLabel = false;
            this.institution = true;
            this.Project = true;
            this.event = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC6 = false;
            this.Course = false;
        } else if (newvalue == 3) {
            this.getDictionary("EventType", "A");
            this.getSurveyIndicator();
            this.parentLabel = false;
            this.institution = false;
            this.event = true;
            this.Project = false;
            this.Course = false;
            this.ATC1 = false;
            this.ATC2 = false;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC6 = false;
        } else {
            this.getDictionary("EventType", "A");
            this.getSurveyIndicator();
            this.getTrainingHours(hoursTraining);
            this.parentLabel = true;
            this.institution = false;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC6 = true;
        }
    }

    getPartnerType() {
        var partnerTemp: any;
        this.dropdownPartner = [];
        this.service.httpClientGet("api/Roles/CoursePartner", partnerTemp)
            .subscribe(result => {
                partnerTemp = result;
                this.dropdownPartner = partnerTemp;
            });
    }

    onSelectedInstructor(item) {
        if (item != null) {
            this.ContactName = item.description;
            this.errorInstructor = "";
        } else {
            this.errorInstructor = "Instructor Not Found..";
        }
    }

    getProduct() {
        var productTemp: any;
        this.service.httpClientGet("api/Product", productTemp)
            .subscribe(result => {
                // productTemp = result;
                // productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
                // this.products = JSON.parse(productTemp);
                this.products = productTemp;
            });
    }

    findVersion1(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    version = result;
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        currentVersion.splice(currentVersion.length, 0, lastVersion);
                        this.versions1 = currentVersion;
                    } else {
                        this.versions1 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
            this.loading = false;
        } else {
            this.versions1 = "";
            this.loading = false;
        }
    }

    findVersion2(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    version = result;
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        currentVersion.splice(currentVersion.length, 0, lastVersion);
                        this.versions2 = currentVersion;
                    } else {
                        this.versions2 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
            this.loading = false;
        } else {
            this.versions2 = "";
            this.loading = false;
        }
    }

    getDictionary(parent, status) {
        var dataTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
            .subscribe(result => {
                this.typeOf = result;
            }, error => { this.service.errorserver(); });
    }

    getSurveyIndicator() {
        var dataTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Status':'A'}", dataTemp)
            .subscribe(result => {
                this.survey = result;
            });
    }

    // getEvaTemplate() {
    //     var evaTemp: any;
    //     this.service.httpClientGet("api/EvaluationQuestion", evaTemp)
    //         .subscribe(result => {
    //             this.evaTemplate = result;
    //         }, error => { this.service.errorserver(); });
    // }

    // getCertificateTemplate() {
    //     var certificate: any;
    //     this.service.httpClientGet("api/Courses/Certificate", certificate)
    //         .subscribe(result => {
    //             this.cftTemplate = result;
    //         }, error => { this.service.errorserver(); });
    // }

    resetDate() {
        this.editCourse.reset({
            'StartDate': null,
            'EndDate': null
        });
        this.dateStart = null;
        this.dateEnd = null;
    }

    onSelectDateStart(date: NgbDateStruct) {
        if (date != null) {
            this.dateStart = this.parserFormatter.format(date);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }

    onSelectDateEnd(date: NgbDateStruct) {
        if (date != null) {
            this.dateEnd = this.parserFormatter.format(date);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }

    // checkAvailableFY(year) {
    //     var tempYear: any;
    //     this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Key':'" + year + "'}", tempYear)
    //         .subscribe(result => {
    //             tempYear = result;
    //             if (tempYear.length == 1) {
    //                 this.editCourse.patchValue({ Survey: tempYear[tempYear.length - 1].Key });
    //             } else {
    //                 swal('Information!', 'FY Not Found', 'error');
    //             }
    //         }, error => {
    //             this.service.errorserver();
    //         });
    // }

    // checkDate(dateStart, dateEnd) {
    //     var today: Date = new Date();
    //     var startCourse: Date = new Date(dateStart);
    //     var endCourse: Date = new Date(dateEnd);
    //     var data: any;
    //     if (dateStart != undefined && dateEnd != undefined) {
    //         if (endCourse.getTime() < startCourse.getTime()) {
    //             swal(
    //                 'Information!',
    //                 'The start date exceeds the end date',
    //                 'error'
    //             );
    //             this.resetDate();
    //         } else {
    //             var lastYear = today.getFullYear() - 1;
    //             var currYear = today.getFullYear();
    //             var intervalStart: Date = new Date(lastYear + '-03-01');
    //             var intervalEnd: Date = new Date(currYear + '-02-28');
    //             if (startCourse.getTime() <= intervalStart.getTime()) {
    //                 this.checkAvailableFY(lastYear);
    //             } else if ((startCourse.getTime() >= intervalStart.getTime()) && (startCourse.getTime() <= intervalEnd.getTime())) {
    //                 this.checkAvailableFY(currYear);
    //             } else {
    //                 this.checkAvailableFY(currYear);
    //             }
    //         }
    //     }
    // }

    onSelected(item) {
        var siteId = '';
        if (item != null) {
            siteId = item.title;
            this.getInstructor(siteId);
            this.errorSites = "";
        } else {
            this.errorSites = "Sites Not Found..";
        }
    }

    getSite() {
        var currentStatus = 'A';
        var siteTemp: any;
        this.service.httpClientGet("api/MainSite/SiteCourse/{'Status':'" + currentStatus + "'}", siteTemp)
            .subscribe(result => {
                siteTemp = result;
                if (siteTemp.length > 0) {
                    var exist = false;
                    for (let i = 0; i < siteTemp.length; i++) {
                        if (this.sites.length == 0) {
                            this.sites.push(siteTemp[i]);
                        } else {
                            for (let j = 0; j < this.sites.length; j++) {
                                if (siteTemp[i].SiteId == this.sites[j].SiteId) {
                                    exist = true;
                                }
                            }
                            if (exist == false && siteTemp[i].SiteId != "") {
                                this.sites.push(siteTemp[i]);
                            }
                        }
                    }
                } else {
                    swal(
                        'Information!',
                        'Site Data Not Found',
                        'error'
                    );
                }

            }, error => { this.service.errorserver(); });
    }

    getInstructor(siteId) {
        var instructor: any;
        this.service.httpClientGet("api/MainSite/Instructor/" + siteId, instructor)
            .subscribe(result => {
                instructor = result;
                if (instructor.length > 0) {
                    var exist = false;
                    for (let i = 0; i < instructor.length; i++) {
                        if (this.instructor.length == 0) {
                            this.instructor.push(instructor[i]);
                        } else {
                            for (let j = 0; j < this.instructor.length; j++) {
                                if (instructor[i].ContactId == this.instructor[j].ContactId) {
                                    exist = true;
                                }
                            }
                            if (exist == false && instructor[i].ContactId != "") {
                                this.instructor.push(instructor[i]);
                            }
                        }
                    }
                } else {
                    swal(
                        'Information!',
                        'Instructor With Site ID Selected Not Found',
                        'error'
                    );
                }
            }, error => { this.service.errorserver(); });
    }

    onChangeOther(isChecked: boolean) {
        isChecked ? this.editCourse.controls["Komen"].enable() : this.editCourse.controls["Komen"].disable();
    }

    onChangeOther2(isChecked: boolean) {
        isChecked ? this.editCourse.controls["Komen5"].enable() : this.editCourse.controls["Komen5"].disable();
    }

    getTemplateEvaId(value) {
        this.evaTemplateId = value;
    }

    evaTemplatePreview() {
        if (this.evaTemplateId == undefined || this.evaTemplateId == "") {
            swal(
                'Information!',
                'There is no survey template selected',
                'error'
            );
        } else {
            var newWindow = window.open('template-preview/survey-preview-eva/' + this.evaTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
            return false;
        }
    }

    getTemplateCertificateId(value) {
        this.certificateTemplateId = value;
    }

    certificatePreview() {
        if (this.certificateTemplateId == undefined || this.certificateTemplateId == "") {
            swal(
                'Information!',
                'There is no certificate template selected',
                'error'
            );
        } else {
            var newWindow = window.open('template-preview/certificate-preview-eva/' + this.certificateTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
            return false;
        }
    }

    getInstructorData(contactId, SiteId) {
        var kontak: any;
        this.service.httpClientGet("api/Courses/InstructorContact/" + contactId + "/" + SiteId, kontak)
            .subscribe(res => {
                kontak = res;
                if (kontak != null) {
                    this.editCourse.patchValue({ SiteId: kontak.SiteId });
                    this.ContactName = kontak.ContactName;
                } else {
                    this.editCourse.patchValue({ SiteId: "NOT FOUND" });
                    this.ContactName = "NOT FOUND";
                }
            }, error => {
                this.service.errorserver();
            });
    }

    setFormValue(data) {
        // console.log("Data kursus => ", data);
        //Set partner type
        let partnerTemp: any;
        if (data[0].PartnerType === 'ATC') {
            partnerTemp = 1;
        } else if (data[0].PartnerType === 'CTC') {
            partnerTemp = 61;
        } else {
            partnerTemp = 58;
        }

        this.editCourse.patchValue({ PartnerType: partnerTemp });
        this.editCourse.controls["PartnerType"].disable();
        this.selectedPartner(partnerTemp);

        //Set certificate type
        let certificate: any;
        if (partnerTemp == 58) {
            if (data[0].PartnerType === 'AAP Course') {
                certificate = 1;
            } else if (data[0].PartnerType === 'AAP Project') {
                certificate = 2;
            } else if (data[0].PartnerType === 'AAP Event') {
                certificate = 3;

            } else {
                certificate = 0;
            }
        }
        this.partnerType = data[0].PartnerType;

        if (certificate != undefined && certificate == 3) {
            this.editCourse.patchValue({ LocationName: data[0].LocationName });
        } else {
            this.selectedCertificate(certificate, data[0].HoursTraining);
        }

        //Set course title
        this.editCourse.patchValue({ CourseTitle: data[0].Name });

        //Set course facility & equipment if ATC
        this.editCourse.patchValue({ CourseFacility: data[0].ATCFacility });
        this.editCourse.patchValue({ CourseEquipment: data[0].ATCComputer });

        //Set site id, instructor id and contact name
        this.editCourse.patchValue({ SiteId: data[0].SiteId });
        this.editCourse.patchValue({ InstructorId: data[0].InstructorId });
        this.ContactName = data[0].ContactName;

        //Set course course start and completion date, survey year too
        let startDate = data[0].StartDate.split('-');
        let completionDate = data[0].CompletionDate.split('-');

        this.modelPopup1 = {
            "year": parseInt(startDate[0]),
            "month": parseInt(startDate[1]),
            "day": parseInt(startDate[2])
        };

        this.modelPopup2 = {
            "year": parseInt(completionDate[0]),
            "month": parseInt(completionDate[1]),
            "day": parseInt(completionDate[2])
        };

        this.editCourse.patchValue({ Survey: data[0].FYIndicatorKey });

        //Set institution
        this.editCourse.patchValue({ Institution: data[0].Institution });;

        //Set course teaching level
        let updateVal = (data[0].Update == '1') ? true : false;
        this.editCourse.patchValue({ Update: updateVal });

        let essensVal = (data[0].Essentials == '1') ? true : false;
        this.editCourse.patchValue({ Essentials: essensVal });

        let intermediateVal = (data[0].Intermediate == '1') ? true : false;
        this.editCourse.patchValue({ Intermediate: intermediateVal });

        let advancedVal = (data[0].Advanced == '1') ? true : false;
        this.editCourse.patchValue({ Advanced: advancedVal });

        let customVal = (data[0].Customized == '1') ? true : false;
        this.editCourse.patchValue({ Customized: customVal });

        if (data[0].ctl_Other == '1') {
            this.editCourse.patchValue({ Other: true });
            this.editCourse.controls["Komen"].enable();
            this.editCourse.patchValue({ Komen: data[0].ctl_Comment });
        } else {
            this.editCourse.patchValue({ Other: false });
            this.editCourse.controls["Komen"].disable();
        }

        //Set product and product version (primary & secondary)
        this.editCourse.patchValue({ Product1: data[0].productId });
        this.findVersion1(data[0].productId);
        this.editCourse.patchValue({ Version1: data[0].ProductVersionsId });

        this.editCourse.patchValue({ Product2: data[0].productsecondaryId });
        this.findVersion2(data[0].productsecondaryId);
        this.editCourse.patchValue({ Version2: data[0].ProductVersionsSecondaryId });

        //Set course teaching format
        let instructorLedVal = (data[0].InstructorLed == '1') ? true : false;
        this.editCourse.patchValue({ InstructorLed: instructorLedVal });

        let onlineVal = (data[0].Online == '1') ? true : false;
        this.editCourse.patchValue({ Online: onlineVal });

        //Set course teaching materials
        let autodeskVal = (data[0].Autodesk == '1') ? true : false;
        this.editCourse.patchValue({ Autodesk: autodeskVal });

        let aapVal = (data[0].ctm_AAP == '1') ? true : false;
        this.editCourse.patchValue({ AAP: aapVal });

        let atcVal = (data[0].ctm_ATC == '1') ? true : false;
        this.editCourse.patchValue({ ATC: atcVal });

        let independentVal = (data[0].Independent == '1') ? true : false;
        this.editCourse.patchValue({ Independent: independentVal });

        let independentOnlineVal = (data[0].IndependentOnline == '1') ? true : false;
        this.editCourse.patchValue({ IndependentOnline: independentOnlineVal });

        let atcOnlineVal = (data[0].ATCOnline == '1') ? true : false;
        this.editCourse.patchValue({ ATCOnline: atcOnlineVal });

        if (data[0].ctm_Other == '1') {
            this.editCourse.patchValue({ Other5: true });
            this.editCourse.controls["Komen5"].enable();
            this.editCourse.patchValue({ Komen5: data[0].ctm_Comment });
        } else {
            this.editCourse.patchValue({ Other5: false });
            this.editCourse.controls["Komen5"].disable();
        }

        // this.editCourse.patchValue({ EvaTemplate: data[0].EvaluationQuestionId });
        // this.evaTemplateId = data[0].EvaluationQuestionId;
        // this.editCourse.patchValue({ CertTemplate: data[0].CertificateId });
        // this.certificateTemplateId = data[0].CertificateId;
        this.editCourse.disable();
    }

    getStudentPicked(id) {
        var student: any;
        this.pickedStudent = [];
        this.service.httpClientGet("api/Courses/Student/" + id, student)
            .subscribe(res => {
                student = res;
                if (student.length > 0) {
                    for (let i = 0; i < student.length; i++) {
                        this.pickedStudent.push(student[i]);
                        //Karena studentnya sudah dipilih sebelumnya, maka dimasukkan ke variable selected student supaya tetap terbaca ketika masuk ke fungsi remove student
                        this.selectedStudent.push(student[i].StudentID);
                    }
                } else {
                    this.pickedStudent = [];
                    this.selectedStudent = [];
                }
                this.loading = false;
            });
    }

    getDataCourse(id) {
        var data = '';
        this.datadetail = [];
        this.service.httpClientGet("api/Courses/Detail/" + id, data)
            .subscribe(res => {
                this.data = res;
                this.datadetail.push(this.data);
                this.setFormValue(this.datadetail);
            });
    }

    // getGeo() {
    //     var data = '';
    //     this.service.httpClientGet("api/Geo", data)
    //         .subscribe(result => {
    //             this.data = result;
    //             this.dropdownListGeo = this.data.map((item) => {
    //                 return {
    //                     id: item.geo_id,
    //                     itemName: item.geo_name
    //                 }
    //             })
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    //     this.selectedItemsGeo = [];

    //     //setting dropdown
    //     this.dropdownSettings = {
    //         singleSelection: false,
    //         text: "Please Select",
    //         selectAllText: 'Select All',
    //         unSelectAllText: 'Unselect All',
    //         enableSearchFilter: true,
    //         classes: "myclass custom-class",
    //         disabled: false,
    //         maxHeight: 120
    //     };
    // }

    // onGeoSelect(item: any) {
    //     this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // OnGeoDeSelect(item: any) {
    //     this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onGeoSelectAll(items: any) {
    //     this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onRegionSelect(item: any) {
    //     this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // OnRegionDeSelect(item: any) {
    //     this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onRegionSelectAll(items: any) {
    //     this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onSubRegionSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.studentFound = false;
    // }

    // onSubRegionSelectAll(items: any) {
    //     this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    onCountriesSelect(item: any) {
        this.getStudentbyCountry();
    }
    OnCountriesDeSelect(item: any) {
        this.getStudentbyCountry();
        this.studentFound = false;
    }
    onCountriesSelectAll(item: any) {
        this.getStudentbyCountry();
    }
    onCountriesDeSelectAll(item: any) {
        this.getStudentbyCountry();
        this.studentFound = false;
    }

    getStudentbyCountry() {
        this.loading = true;
        this.dataStudent = [];
        var student: any;
        if (this.selectedItemsCountry.length != 0) {
            for (let i = 0; i < this.selectedItemsCountry.length; i++) {
                this.service.httpClientGet("api/Student/GetStudentByCountry/" + this.selectedItemsCountry[i].id, student)
                    .subscribe(result => {
                        // var finalresult = result.replace(/\t/g, " ")
                        // student = JSON.parse(finalresult);
                        student = result;
                        if (student.length > 0) {
                            for (let j = 0; j < student.length; j++) {
                                this.dataStudent.push(student[j]);
                            }
                        }
                    });
            }
            this.studentFound = true;
            this.loading = false;
        } else {
            this.studentFound = false;
            this.loading = false;
        }
    }

    addStudent(id) {
        if (this.selectedStudent.length == 0) {
            this.selectedStudent.push(id);
        } else {
            var exist = false;
            for (let i = 0; i < this.selectedStudent.length; i++) {
                if (id == this.selectedStudent[i]) {
                    exist = true;
                    this.studentErrorTag = true;
                    this.studentSuccessTag = false;
                    this.errorTagMessage = "You have choose this student..";
                    setTimeout(function () {
                        this.studentErrorTag = false;
                    }.bind(this), 3000);
                }
            }
            if (exist == false && id != "") {
                this.selectedStudent.push(id);
                this.studentErrorTag = false;
            }
        }

        var index = this.dataStudent.findIndex(x => x.StudentID == id);
        if (index !== -1) {
            this.dataStudent.splice(index, 1);
        }

        this.refreshPickedStudent(this.selectedStudent);
    }

    showAddedStudent(firstname, lastname): void {
        this.studentSuccessTag = true;
        this.successTagMessage = "You added " + this.service.decoder(firstname) + " " + this.service.decoder(lastname) + " in this course..";
        setTimeout(function () {
            this.studentSuccessTag = false;
        }.bind(this), 3000);
    }

    refreshPickedStudent(selectedStudent) {
        var picked: any;
        this.pickedStudent = [];
        if (selectedStudent.length > 0) {
            for (let x = 0; x < selectedStudent.length; x++) {
                this.service.httpClientGet("api/Student/GetStudent/" + selectedStudent[x], picked)
                    .subscribe(result => {
                        picked = result;
                        var exist = false;
                        if (this.pickedStudent.length == 0) {
                            this.pickedStudent.push(picked);
                            this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                        } else {
                            for (let k = 0; k < this.pickedStudent.length; k++) {
                                if (picked.StudentID == this.pickedStudent[k].StudentID) {
                                    exist = true;
                                    this.studentSuccessTag = false;
                                }
                            }
                            if (exist == false && picked.StudentID != "") {
                                this.pickedStudent.push(picked);
                                this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                            }
                        }
                    });
            }
            this.studentPicked = true;
        } else {
            this.studentPicked = false;
        }
    }

    removeStudent(id) {
        let studentTemp = [];
        let index = this.selectedStudent.indexOf(id)
        if (index > -1) {
            this.selectedStudent.splice(index, 1);
        }

        var index2 = this.pickedStudent.findIndex(x => x.StudentID == id);
        if (index2 !== -1) {
            //Kalau pakai unshift ga akan support di IE (buat nambah array di index awal)
            this.dataStudent.unshift(this.pickedStudent[index2]);
            studentTemp = this.dataStudent;
            this.dataStudent = [];
            this.dataStudent = studentTemp;
        }

        this.refreshPickedStudent(this.selectedStudent);
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['courseId'];
        this.evalsId = this.route.snapshot.params['evalsId'];
        this.evalsNumber = this.route.snapshot.params['evalsNumber'];

        // console.log(this.evalsId);
        this.getDataCourse(this.id);
        //ini untuk lookup student
        // this.getDataSiswa();
        //ini untuk ngambil data student sebelumnya yang sudah tersimpan di database
        this.getStudentPicked(this.id);
        // this.getSite();
        this.getPartnerType();
        this.getSurveyIndicator();
        this.getProduct();
        // this.getEvaTemplate();
        // this.getCertificateTemplate();
        // this.getGeo();
        this.parentLabel = true;

        var hourTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'ATCHours','Status':'A'}", hourTemp)
            .subscribe(res => {
                this.trainingHours = res;
            });
        this.getCountry();

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    getCountry() {
        var data: any;
        this.service.httpClientGet("api/Countries", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            });
        this.selectedItemsCountry = [];
    }

    inputStudent(id) {
        var input: any;
        var studentTemp = {
            'CourseId': this.id,
            'StudentID': id,
            'Status': this.editCourse.value.Status,
            'CourseTaken': 0,
            'SurveyTaken': 0
        };
        let studentData = JSON.stringify(studentTemp);
        this.service.httpClientPost("api/Courses/CourseStudent", studentData)
            .subscribe(resultPost => {
                input = resultPost;
                if (input["code"] != 1) swal('Information', 'Error when update into Table CourseStudent', 'error');
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    deleteStudent(id) {
        var del: any;
        this.service.httpClientDelete("api/Courses/CourseStudent/where/{'CourseId':'" + this.id + "','StudentID':'" + id + "'}", del)
            .subscribe(result => {
                del = result;
                if (del["code"] != 1) swal('Information', 'Error when update into Table CourseStudent', 'error');
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    updateCourseStudent() {
        var student: any;
        this.service.httpClientGet("api/Courses/Student/" + this.id, student)
            .subscribe(res => {
                student = res;
                if (student.length > 0) {
                    for (let m = 0; m < student.length; m++) {
                        var existInPicked = false;
                        for (let n = 0; n < this.pickedStudent.length; n++) {
                            if (student[m].StudentID == this.pickedStudent[n].StudentID) existInPicked = true;
                        }
                        if (existInPicked == false) this.deleteStudent(student[m].StudentID);
                    }

                    for (let j = 0; j < this.pickedStudent.length; j++) {
                        var existInDB = false;
                        for (let k = 0; k < student.length; k++) {
                            if (this.pickedStudent[j].StudentID == student[k].StudentID) existInDB = true;
                        }
                        if (existInDB == false) this.inputStudent(this.pickedStudent[j].StudentID);
                    }
                    swal('Information!', 'Update Data Success', 'success');
                    this.router.navigate(['/course/course-list']);
                    this.loading = false;
                } else {
                    swal(
                        'Information!',
                        'Data Student Not Found',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
                this.loading = false;
            });
    }

    updateCTM(data) {
        var l: any;
        var ctm = {
            'Autodesk': this.editCourse.value.Autodesk,
            'AAP': this.editCourse.value.AAP,
            'ATC': this.editCourse.value.ATC,
            'Independent': this.editCourse.value.Independent,
            'IndependentOnline': this.editCourse.value.IndependentOnline,
            'ATCOnline': this.editCourse.value.ATCOnline,
            'Other5': this.editCourse.value.Other5,
            'Komen5': this.editCourse.value.Komen5,
            'Status': this.editCourse.value.Status
        };
        let courseTrainingMaterial = JSON.stringify(ctm);
        this.service.httpCLientPut("api/Courses/CourseTrainingMaterial/" + this.id, courseTrainingMaterial)
            .subscribe(res5 => {
                l = res5;
                if (l["code"] == 1) {
                    this.updateCourseStudent();
                } else {
                    swal(
                        'Information',
                        'Error when update into Table CourseTrainingMaterial',
                        'error'
                    );
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    updateCTF(data) {
        var k: any;
        var ctf = {
            'InstructorLed': this.editCourse.value.InstructorLed,
            'Online': this.editCourse.value.Online,
            'Status': this.editCourse.value.Status
        };
        let courseTrainingFormat = JSON.stringify(ctf);
        this.service.httpCLientPut("api/Courses/CourseTrainingFormat/" + this.id, courseTrainingFormat)
            .subscribe(res4 => {
                k = res4;
                if (k["code"] == 1) {
                    this.updateCTM(data);
                } else {
                    swal(
                        'Information',
                        'Error when update into Table CourseTrainingFormat',
                        'error'
                    );
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    updateCourseSoftware(data) {
        var j: any;
        var cs = {
            'Product1': this.editCourse.value.Product1,
            'Version1': this.editCourse.value.Version1,
            'Product2': this.editCourse.value.Product2,
            'Version2': this.editCourse.value.Version2,
            'Status': this.editCourse.value.Status
        };
        let courseSoftware = JSON.stringify(cs);
        this.service.httpCLientPut("api/Courses/CourseSoftware/" + this.id, courseSoftware)
            .subscribe(res3 => {
                j = res3;
                if (j["code"] == 1) {
                    if (this.editCourse.value.CertificateType == 2 || this.editCourse.value.CertificateType == 3) {
                        this.updateCourseStudent();
                    } else {
                        this.updateCTF(data);
                    }
                } else {
                    swal(
                        'Information',
                        'Error when update into Table CourseSoftware',
                        'error'
                    );
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    updateCTL(data) {
        var i: any;
        var ctl = {
            'Update': this.editCourse.value.Update,
            'Essentials': this.editCourse.value.Essentials,
            'Intermediate': this.editCourse.value.Intermediate,
            'Advanced': this.editCourse.value.Advanced,
            'Customized': this.editCourse.value.Customized,
            'Other': this.editCourse.value.Other,
            'Comment': this.editCourse.value.Komen,
            'Status': this.editCourse.value.Status
        };
        let courseTeachingLevel = JSON.stringify(ctl);
        this.service.httpCLientPut("api/Courses/CourseTeachingLevel/" + this.id, courseTeachingLevel)
            .subscribe(res2 => {
                i = res2;
                if (i["code"] == 1) {
                    this.updateCourseSoftware(data);
                } else {
                    swal(
                        'Information',
                        'Error when update into Table CourseTeachingLevel',
                        'error'
                    );
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    updateCourse(data) {
        var x: any;
        this.service.httpCLientPut("api/Courses/" + this.id, data)
            .subscribe(res1 => {
                x = res1;
                if (x["code"] == 1) {
                    this.editCourse.value.CertificateType == 3 ? this.updateCourseSoftware(data) : this.updateCTL(data);
                } else {
                    swal(
                        'Information',
                        'Error when update into Table Courses',
                        'error'
                    );
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    onSubmit() {

        if (this.partnerSelected == 1) {
            this.editCourse.removeControl('CertificateType');
            this.editCourse.removeControl('Institution');
            this.editCourse.removeControl('TrainingType');
            this.editCourse.removeControl('ProjectType');
            this.editCourse.removeControl('EventType');
            this.editCourse.removeControl('LocationName');
            this.editCourse.controls['CourseFacility'].markAsTouched();
            this.editCourse.controls['CourseEquipment'].markAsTouched();
        } else {
            if (this.certificateSelected == 1) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('ProjectType');
                this.editCourse.removeControl('EventType');
                this.editCourse.removeControl('LocationName');
                this.editCourse.controls['Institution'].markAsTouched();
                this.editCourse.controls['TrainingType'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 2) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('TrainingType');
                this.editCourse.removeControl('EventType');
                this.editCourse.removeControl('LocationName');
                this.editCourse.controls['Institution'].markAsTouched();
                this.editCourse.controls['ProjectType'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 3) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('Institution');
                this.editCourse.removeControl('TrainingType');
                this.editCourse.removeControl('ProjectType');
                this.editCourse.controls['LocationName'].markAsTouched();
                this.editCourse.controls['EventType'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            }
            this.editCourse.controls['CertificateType'].markAsTouched();
        }

        this.editCourse.controls['PartnerType'].markAsTouched();
        this.editCourse.controls['CourseTitle'].markAsTouched();
        this.editCourse.controls['SiteId'].markAsTouched();
        this.editCourse.controls['ContactId'].markAsTouched();
        this.editCourse.controls['StartDate'].markAsTouched();
        this.editCourse.controls['EndDate'].markAsTouched();
        this.editCourse.controls['Survey'].markAsTouched();
        this.editCourse.controls['Product1'].markAsTouched();
        this.editCourse.controls['Version1'].markAsTouched();
        this.editCourse.controls['Product2'].markAsTouched();
        this.editCourse.controls['Version2'].markAsTouched();
        this.editCourse.controls['TrainingHours'].markAsTouched();
        this.editCourse.controls['EvaTemplate'].markAsTouched();
        this.editCourse.controls['CertTemplate'].markAsTouched();

        if (this.editCourse.valid) {
            this.loading = true;
            if (this.pickedStudent.length != 0) {
                this.editCourse.value.StartDate = this.formatdate.dateCalendarToYMD(this.editCourse.value.StartDate);
                this.editCourse.value.EndDate = this.formatdate.dateCalendarToYMD(this.editCourse.value.EndDate);
                this.editCourse.value.Status = "A";

                if (this.editCourse.value.Update == "") this.editCourse.value.Update = false;
                if (this.editCourse.value.Essentials == "") this.editCourse.value.Essentials = false;
                if (this.editCourse.value.Intermediate == "") this.editCourse.value.Intermediate = false;
                if (this.editCourse.value.Advanced == "") this.editCourse.value.Advanced = false;
                if (this.editCourse.value.Customized == "") this.editCourse.value.Customized = false;
                if (this.editCourse.value.Other == "") this.editCourse.value.Other = false;
                if (this.editCourse.value.InstructorLed == "") this.editCourse.value.InstructorLed = false;
                if (this.editCourse.value.Online == "") this.editCourse.value.Online = false;
                if (this.editCourse.value.Autodesk == "") this.editCourse.value.Autodesk = false;
                if (this.editCourse.value.AAP == "") this.editCourse.value.AAP = false;
                if (this.editCourse.value.ATC == "") this.editCourse.value.ATC = false;
                if (this.editCourse.value.Independent == "") this.editCourse.value.Independent = false;
                if (this.editCourse.value.IndependentOnline == "") this.editCourse.value.IndependentOnline = false;
                if (this.editCourse.value.ATCOnline == "") this.editCourse.value.ATCOnline = false;
                if (this.editCourse.value.Other5 == "") this.editCourse.value.Other5 = false;

                let data = JSON.stringify(this.editCourse.value);
                this.updateCourse(data);
            } else {
                swal('Information!', 'Please Tag Your Student in This Course', 'error');
                this.loading = false;
            }
        }
    }

    goBack(){
        window.history.back();
    }
}