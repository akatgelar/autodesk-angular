import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CourseUpdateEVAComponent } from './course-update-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { Ng2CompleterModule } from "ng2-completer";
import { FormsModule } from "@angular/forms";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';
import { ConvertDatePipe } from './course-update-eva.component';

export const CourseUpdateEVARoutes: Routes = [
    {
        path: '',
        component: CourseUpdateEVAComponent,
        data: {
            breadcrumb: 'eva.manage_course.add.edit_course',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CourseUpdateEVARoutes),
        SharedModule,
        Ng2CompleterModule,
        FormsModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [CourseUpdateEVAComponent, ConvertDatePipe],
    providers: [AppService, AppFormatDate, DatePipe, AppFilterGeo]
})

export class CourseUpdateEVAModule { }