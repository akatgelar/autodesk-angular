import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { DatePipe } from '@angular/common';
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
// import * as myGlobals from "../../../../node_modules/globals";
import $ from 'jquery/dist/jquery';
import { SessionService } from '../../shared/service/session.service';
import { Pipe, PipeTransform, ViewChild, Injectable, ElementRef } from "@angular/core";
import { saveAs } from 'file-saver';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
    }
}

@Component({
    selector: 'app-course-update-eva',
    templateUrl: './course-update-eva.component.html',
    styleUrls: [
    './course-update-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CourseUpdateEVAComponent implements OnInit {
    @ViewChild('fileInput') fileInput: ElementRef;
    listSites = [];
    messageError: string = '';
    id: string;
    partnerType: string = "";
    dropdownPartner = [];
    datadetail = [];
    selectedStudent = [];
	private trainingHoursOther:any;
    private data: any;
    private versions1;
    private versions2;
    private partnerSelected;
    certificateSelected: any;
    pickedStudent = [];
    studentTosendEmail = [];
    private errorSites: string = '';
    private errorInstructor: string = '';
    dateStart: string;
    dateEnd: string;
    private products;
    private versions;
    private typeOf;
    private trainingHours;
    private survey;
    private evaTemplate;
    private cftTemplate;
    private _serviceUrl = 'api/Courses';
    private dropdownCertificate;
    private ContactName: string;
    private evaTemplateId;
    private certificateTemplateId;
    public courseDiv = false;
    public courseEquipment = true;
    public courseFacility = true;
    public CertificateType = false;
    public CertificateTypeCol = true;
    public Course = false;
    public Project = false;
    public event = false;
    public ATC1 = true;
    public ATC2 = true;
    public ATC3 = true;
    public ATC4 = true;
    public ATC6 = true;
    public institution = false;
    protected siteId: any;
    protected contactId: string;
    protected CourseId: any;
    protected sites = [];
    protected instructor = [];
    // protected dataService1: CompleterData;
    protected dataService2: CompleterData;
    // protected dataService3: CompleterData;
    protected dataService1: RemoteData;
    protected dataService3: RemoteData;
    public rowsOnPage: number = 5;
    public rowsOnPage2: number = 5;
    public filterQuery: string = "";
    public filterQuery2: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public studentFound = false;
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownSettings = {};
    dataStudent = [];
    protected siswa: string;
    protected siswaLookup = [];
    editCourse: FormGroup;
    modelPopup1;
    modelPopup2;
    public projectInstitution = false;
    public projectLogoUpload = false;
    public parentLabel = false;
    public loading = false;
    studentErrorTag = false;
    studentSuccessTag = false;
    errorTagMessage: string = '';
    successTagMessage: string = '';
    public studentPicked = false;
    private evalsNumber: number = 0;
    public showEvals = false;
    dataEvals;
    public useraccesdata: any;
    private contactID;
    private coursePartner;
    private siteUrl;
    public notFound = false;
	private lastEvalDate;
    eventType;
    // hidden field
    public hiddenfield: boolean = false;
    partnerTemp: any;
    certificate = 0;
    private certdropdown;
    private selcertdropdown;
    public isAvailableLogo: boolean = false;
    imgLogoName: string = "";
    imgSrc: string = "";
    language = "35";

    constructor(private router: Router, private session: SessionService, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute,
        private completerService: CompleterService, private parserFormatter: NgbDateParserFormatter, private datePipe: DatePipe, private filterGeo: AppFilterGeo) {

        // this.dataService1 = completerService.local(this.sites, 'SiteId', 'SiteId').descriptionField("SiteName");
        // this.dataService2 = completerService.local(this.instructor, 'ContactId', 'ContactId').descriptionField("ContactName");
        // this.dataService3 = completerService.local(this.siswaLookup, 'StudentID', 'StudentID').descriptionField("Firstname");

        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        //Untuk pencarian sites berdasarkan site id
        let status = ['"A"', '"H"', '"I"'];
        if (this.useraccesdata.UserLevelId == "TRAINER") {
            var orgArr = this.useraccesdata.OrgId.toString().split(',');
            var orgTmp = [];
            for (var i = 0; i < orgArr.length; i++) {
                orgTmp.push('"' + orgArr[i] + '"');
            }
            this.siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Organization':'" + orgTmp.join(",") + "'}";
        }
        else {
            this.siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
        }

        this.dataService1 = completerService.remote(
            null,
            "Sites",
            "Sites");
        this.dataService1.urlFormater(term => {
            return this.siteUrl + "/" + term;
        });
        this.dataService1.dataField("results");

        //Untuk pencarian siswa berdasarkan email dan student id
        this.dataService3 = completerService.remote(
            null,
            "Student",
            "Student");
        this.dataService3.urlFormater(term => {
            return `api/Student/FindStudent/` + term;
        });
        this.dataService3.dataField("results");

        let CourseId = new FormControl('');
        let PartnerType = new FormControl('');
        let CertificateType = new FormControl('');
        let CourseTitle = new FormControl('', [Validators.required, Validators.maxLength(40)]);
        let SiteId = new FormControl('', Validators.required);
        let InstructorId = new FormControl('', Validators.required);
        let Institution = new FormControl('', Validators.required);
        let StartDate = new FormControl('', Validators.required);
        let EndDate = new FormControl('', Validators.required);
        let Survey = new FormControl('', Validators.required);
        let Update = new FormControl(false);
        let Essentials = new FormControl(false);
        let Intermediate = new FormControl(false);
        let Advanced = new FormControl(false);
        let Customized = new FormControl(false);
        let Other = new FormControl(false);
        let Komen = new FormControl('');
        let Status = new FormControl('');
        let Product1 = new FormControl('', Validators.required);
        let Version1 = new FormControl('', Validators.required);
        let Product2 = new FormControl('');
        let Version2 = new FormControl('');
        let TrainingHours = new FormControl('', Validators.required);
        let InstructorLed = new FormControl('');
        let Online = new FormControl('');
        let TrainingType = new FormControl('');
        let Autodesk = new FormControl('');
        let AAP = new FormControl('');
        let ATC = new FormControl('');
        let Independent = new FormControl('');
        let IndependentOnline = new FormControl('');
        let ATCOnline = new FormControl('');
        let Other5 = new FormControl('');
        let Komen5 = new FormControl('');
        let CourseFacility = new FormControl('', Validators.required);
        let CourseEquipment = new FormControl('', Validators.required);
        // let EvaTemplate = new FormControl('', Validators.required);
        // let CertTemplate = new FormControl('', Validators.required);
        let StudentID = new FormControl('');
        let ProjectType = new FormControl('', Validators.required);
        let LocationName = new FormControl('', Validators.required);
        let EventType = new FormControl('', Validators.required);
        let LearnerType = new FormControl('', Validators.required);
        let nameInstructor = new FormControl('');
        let LevelTeaching = new FormControl('',Validators.required);
        let InstitutionLogo = new FormControl(null); //change to save logo into database
		let HoursTrainingOther=new FormControl(0);

        this.editCourse = new FormGroup({
            CourseId: CourseId,
            PartnerType: PartnerType,
            CertificateType: CertificateType,
            CourseTitle: CourseTitle,
            SiteId: SiteId,
            InstructorId: InstructorId,
            Institution: Institution,
            StartDate: StartDate,
            EndDate: EndDate,
            Survey: Survey,
            Update: Update,
            Essentials: Essentials,
            Intermediate: Intermediate,
            Advanced: Advanced,
            Customized: Customized,
            Other: Other,
            Komen: Komen,
            Status: Status,
            Product1: Product1,
            Version1: Version1,
            Product2: Product2,
            Version2: Version2,
            TrainingHours: TrainingHours,
			HoursTrainingOther:HoursTrainingOther,
            InstructorLed: InstructorLed,
            Online: Online,
            TrainingType: TrainingType,
            Autodesk: Autodesk,
            AAP: AAP,
            ATC: ATC,
            Independent: Independent,
            IndependentOnline: IndependentOnline,
            ATCOnline: ATCOnline,
            Other5: Other5,
            Komen5: Komen5,
            CourseFacility: CourseFacility,
            CourseEquipment: CourseEquipment,
            // EvaTemplate: EvaTemplate,
            // CertTemplate: CertTemplate,
            StudentID: StudentID,
            ProjectType: ProjectType,
            LocationName: LocationName,
            EventType: EventType,
            LearnerType: LearnerType,
            nameInstructor: nameInstructor,
            LevelTeaching: LevelTeaching,
            InstitutionLogo: InstitutionLogo
        });
    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    ketikaSiswaDipilih(item) {
        this.addStudent(item['originalObject'].StudentID);
    }

    getDataSiswa() {
        var murid: any;
        this.service.httpClientGet("api/Student", murid)
        .subscribe(result => {
            murid = result;
            if (murid.length > 0) {
                for (let i = 0; i < murid.length; i++) {
                    this.siswaLookup.push(murid[i]);
                }
            }
        }, error => {
            this.service.errorserver();
        });

    }

    onFileChange(event) {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.editCourse.get('InstitutionLogo').setValue({
              filename: file.name,
              filetype: file.type,
              value: (<string>reader.result).split(',')[1]
            })
          };
        }
    }

    clearLogo(){
        this.isAvailableLogo = false;
        this.projectLogoUpload = true;
    }

    getTrainingHours() {
        function compare(a, b) {
            // Use toUpperCase() to ignore character casing
            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var hour: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'ATCHours','Status':'A'}", hour)
        .subscribe(res => {
            hour = res;
            this.trainingHours = hour.sort(compare);

        });
    }

    selectedPartner(newvalue) {
        this.partnerSelected = newvalue;
        if (this.partnerSelected == 58) {
            var certificate: any;
            var parent = "AAPCertificateType";
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
            .subscribe(result => {
                this.dropdownCertificate = result;
            }, error => {
                this.service.errorserver();
            });
            this.courseDiv = true;
            // this.courseEquipment = false;
            // this.courseFacility = false;
            $("#courseFacility").css("display", "none");
            $(".courseEquipment").css("display", "none");
            this.CertificateType = true;
            this.CertificateTypeCol = false;
            $(".institution").css("display", "block");
        } else if (this.partnerSelected == 1) {
            this.coursePartner = 'ATC';
            this.CertificateType = false;
            this.CertificateTypeCol = true;
            this.courseDiv = false;
            // this.courseEquipment = true;
            // this.courseFacility = true;
            $("#courseFacility").css("display", "block");
            $(".courseEquipment").css("display", "block");
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC6 = true;
            $(".institution").css("display", "none");
        }
    }

    getEventType() {
        function compare(a, b) {
            // const valueA = a.KeyValue.toUpperCase();
            // const valueB = b.KeyValue.toUpperCase();

            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var dataTemp: any;
        var arrange = [];
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'EventType','Status':'A'}", dataTemp)
        .subscribe(result => {
            this.eventType = result;
            this.eventType.sort(compare);
        }, error => { this.service.errorserver(); });
    }

    selectedCertificate(newvalue) {
        this.certificateSelected = newvalue;
        if (newvalue == 1) {
            this.coursePartner = 'AAP Course';
            this.getDictionary("TrainingType", "A");
            this.getSurveyIndicator();
            // this.getTrainingHours(hoursTraining);
            this.parentLabel = true;
            // $(".parentLabel").css("display", "block");
            this.institution = true;
            // $(".institution").css("display", "block");
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.Course = true;
            this.Project = false;
            this.event = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = false;
            this.ATC6 = false;
        } else if (newvalue == 2) {
            this.coursePartner = 'AAP Project';
            this.getDictionary("ProjectType", "A");
            this.getSurveyIndicator();
            // this.getTrainingHours(hoursTraining);
            this.parentLabel = false;
            // $(".parentLabel").css("display", "none");
            this.institution = false;
            // $(".institution").css("display", "block");
            this.projectInstitution = true;
            this.projectLogoUpload = true;
            this.Project = true;
            this.event = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC6 = false;
            this.Course = false;
        } else if (newvalue == 3) {
            this.coursePartner = 'AAP Event';
            this.getDictionary("LearnerType", "A");
            this.getEventType();
            this.getSurveyIndicator();
            this.parentLabel = false;
            // $(".parentLabel").css("display", "none");
            this.institution = false;
            // $(".institution").css("display", "none");
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.event = true;
            this.Project = false;
            this.Course = false;
            this.ATC1 = false;
            this.ATC2 = false;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC6 = false;
        } else {
            this.coursePartner = 'AAP Other';
            this.getDictionary("LearnerType", "A");
            this.getEventType();
            this.getSurveyIndicator();
            // this.getTrainingHours(hoursTraining);
            this.parentLabel = true;
            // $(".parentLabel").css("display", "block");
            this.institution = false;
            // $(".institution").css("display", "none");
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC6 = true;
        }
    }

    getPartnerType() {
        var partnerTemp: any;
        this.dropdownPartner = [];
        this.service.httpClientGet("api/Roles/CoursePartner", partnerTemp)
        .subscribe(result => {
            partnerTemp = result;
            this.dropdownPartner = partnerTemp;
            setTimeout(()=>{
                this.getSites()
            },1000)
        });
    }

    onSelected(item) {
        if (item == null) {
            this.editCourse.controls["InstructorId"].reset();
            this.editCourse.controls["nameInstructor"].reset();
        }

        if (item != null) {
            let splitVal = (item.originalObject.Sites).split(" | ");
            var siteId = '';
            siteId = item.title;
            this.getInstructor(splitVal[0]);
            this.errorSites = "";
        }
        // else {
        //     this.errorSites = "Sites Not Found..";
        // }
    }

    onSelectedInstructor(item) {
        if (item == null) {
            this.editCourse.controls["nameInstructor"].reset();
        }

        if (item != null) {
            let splitVal = (item.originalObject.Instructor).split(" | ");
            this.ContactName = splitVal[1];
            this.editCourse.patchValue({ nameInstructor: splitVal[1] });
            for (let i = 0; i < this.instructor.length; i++) {
                if (this.instructor[i].Instructor === item.originalObject.Instructor) {
                    this.contactID = this.instructor[i].ContactId;
                }
            }
            this.errorInstructor = "";
        }
        // else {
        //     this.errorInstructor = "Instructor Not Found..";
        // }
    }

    getProduct() {
        var productTemp: any;
        //sy tutup dulu karena kata zwe kualifikasi produk masih simpang siur
        // if (this.useraccesdata.UserLevelId == "TRAINER") {
        //     this.service.httpClientGet('api/Qualifications/Product/' + this.useraccesdata.UserId, productTemp)
        //         .subscribe(result => {
        //             if (result == "Not Found") {
        //                 this.products = '';
        //             } else {
        //                 productTemp = result;
        //                 productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        //                 this.products = JSON.parse(productTemp);
        //             }
        //         },
        //             error => {
        //                 this.service.errorserver();
        //                 this.products = '';
        //             });
        // } else {
        //     // this.service.get("api/Product/GetSingleProduct", productTemp)
        //     this.service.get("api/Product", productTemp)
        //         .subscribe(result => {
        //             productTemp = result;
        //             productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        //             this.products = JSON.parse(productTemp);
        //         });
        // }

        this.service.httpClientGet("api/Product", productTemp)
        .subscribe(result => {
            // productTemp = result;
            // productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
            // this.products = JSON.parse(productTemp);
            this.products = result;
        });
    }

    findVersion1(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
            .subscribe(result => {
                version = result;
                    // console.log(version);
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        if (lastVersion != undefined) {
                            currentVersion.splice(currentVersion.length, 0, lastVersion);
                        }
                        this.versions1 = currentVersion;
                    } else {
                        this.versions1 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
        } else {
            this.versions1 = "";
        }
    }

    findVersion2(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
            .subscribe(result => {
                version = result;
                if (version.length > 0) {
                    let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        if (lastVersion != undefined) {
                            currentVersion.splice(currentVersion.length, 0, lastVersion);
                        }
                        this.versions2 = currentVersion;
                    } else {
                        this.versions2 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
        } else {
            this.versions2 = "";
        }
    }

    getDictionary(parent, status) {
        var dataTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
        .subscribe(result => {
            this.typeOf = result;
        }, error => { this.service.errorserver(); });
    }

    getSurveyIndicator() {
        var dataTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Status':'A'}", dataTemp)
        .subscribe(result => {
            this.survey = result;
        });
    }

    // getEvaTemplate() {
    //     var evaTemp: any;
    //     this.service.httpClientGet("api/EvaluationQuestion", evaTemp)
    //         .subscribe(result => {
    //             this.evaTemplate = result;
    //         }, error => { this.service.errorserver(); });
    // }

    // getCertificateTemplate() {
    //     var certificate: any;
    //     this.service.httpClientGet("api/Courses/Certificate", certificate)
    //         .subscribe(result => {
    //             this.cftTemplate = result;
    //         }, error => { this.service.errorserver(); });
    // }

    resetDate() {
        this.editCourse.reset({
            'StartDate': null,
            'EndDate': null
        });
        this.dateStart = null;
        this.dateEnd = null;
    }

    onSelectDateStart(date: NgbDateStruct) {
        if (date != null) {
            this.dateStart = this.parserFormatter.format(date);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }

    onSelectDateEnd(date: NgbDateStruct) {
        if (date != null) {
          this.dateEnd = this.parserFormatter.format(date);
          this.checkEndDate(this.dateStart, this.dateEnd);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }
  checkEndDate(dateStart, dateEnd) {
    var startCourse: Date = new Date(dateStart);
    var endCourse: Date = new Date(dateEnd);
    var activityDateTemp = dateEnd.toString();
    var activityDate = activityDateTemp.split('-');
    var month = parseInt(activityDate[1]);
    if (dateEnd != undefined) {
      if (endCourse.getTime() < startCourse.getTime()) {
        swal(
          'Information!',
          'The start date exceeds the end date',
          'error'
        );
        this.resetDate();
      }
      else {
          var dataTemp: any;
          var parent = "FYIndicator";
          var status = "A";
          var currentYear: any;
          this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
            .subscribe(result => {
              this.survey = result;
              currentYear = this.survey[this.survey.length - 1].Key;
              if (endCourse.getDate() >= 1 && month >= 2) {
                if (endCourse.getFullYear() < currentYear) {
                  currentYear = endCourse.getFullYear();
                }
              }
              else {
                if (endCourse.getFullYear() <= currentYear) {
                  currentYear = endCourse.getFullYear()-1;
                }
              }
              
              this.editCourse.patchValue({ Survey: currentYear });
            }, error => { this.service.errorserver(); });
     
      }
      
    }
  }
 
    // checkAvailableFY(year) {
    //     var tempYear: any;
    //     this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Key':'" + year + "'}", tempYear)
    //         .subscribe(result => {
    //             tempYear = result;
    //             if (tempYear.length == 1) {
    //                 this.editCourse.patchValue({ Survey: tempYear[tempYear.length - 1].Key });
    //             } else {
    //                 swal('Information!', 'FY Not Found', 'error');
    //             }
    //         }, error => {
    //             this.service.errorserver();
    //         });
    // }

    // checkDate(dateStart, dateEnd) {
    //     var today: Date = new Date();
    //     var startCourse: Date = new Date(dateStart);
    //     var endCourse: Date = new Date(dateEnd);
    //     var data: any;
    //     if (dateStart != undefined && dateEnd != undefined) {
    //         if (endCourse.getTime() < startCourse.getTime()) {
    //             swal(
    //                 'Information!',
    //                 'The start date exceeds the end date',
    //                 'error'
    //             );
    //             this.resetDate();
    //         } else {
    //             var lastYear = today.getFullYear() - 1;
    //             var currYear = today.getFullYear();
    //             var intervalStart: Date = new Date(lastYear + '-03-01');
    //             var intervalEnd: Date = new Date(currYear + '-02-28');
    //             if (startCourse.getTime() <= intervalStart.getTime()) {
    //                 this.checkAvailableFY(lastYear);
    //             } else if ((startCourse.getTime() >= intervalStart.getTime()) && (startCourse.getTime() <= intervalEnd.getTime())) {
    //                 this.checkAvailableFY(currYear);
    //             } else {
    //                 this.checkAvailableFY(currYear);
    //             }
    //         }
    //     }
    // }

    getSite() {
        var currentStatus = 'A';
        var siteTemp: any = null;
        this.service.httpClientGet("api/MainSite/SiteCourse/{'Status':'" + currentStatus + "'}/"+siteTemp, '')
        .subscribe(result => {
            siteTemp = result;
            if (siteTemp.length > 0) {
                var exist = false;
                for (let i = 0; i < siteTemp.length; i++) {
                    if (this.sites.length == 0) {
                        this.sites.push(siteTemp[i]);
                    } else {
                        for (let j = 0; j < this.sites.length; j++) {
                            if (siteTemp[i].SiteId == this.sites[j].SiteId) {
                                exist = true;
                            }
                        }
                        if (exist == false && siteTemp[i].SiteId != "") {
                            this.sites.push(siteTemp[i]);
                        }
                    }
                }
            } else {
                swal(
                    'Information!',
                    'Site Data Not Found',
                    'error'
                    );
            }

        }, error => { this.service.errorserver(); });
    }

    getInstructor(siteId) {
        let n_siteId
       if(siteId)
           n_siteId = siteId
       else
           n_siteId = this.siteId[0].id
        var instructor: any;
        this.instructor = [];
        this.service.httpClientGet("api/MainSite/Instructor/" + n_siteId, instructor)
        .subscribe(result => {
            instructor = result;
            if (instructor.length > 0) {
                var exist = false;
                for (let i = 0; i < instructor.length; i++) {
                        // if (this.instructor.length == 0) {
                        //     this.instructor.push(instructor[i]);
                        // } else {
                        //     for (let j = 0; j < this.instructor.length; j++) {
                        //         if (instructor[i].ContactId == this.instructor[j].ContactId) {
                        //             exist = true;
                        //         }
                        //     }
                        //     if (exist == false && instructor[i].ContactId != "") {
                        //         this.instructor.push(instructor[i]);
                        //     }
                        // }
                        this.instructor.push(instructor[i]);
                    }
                    if (this.tmpSiteID != n_siteId)
                    {
                        this.editCourse.controls["ContactId"].reset();
                        this.editCourse.controls["nameInstructor"].reset();
                    }
                    this.dataService2 = this.completerService.local(this.instructor, 'Instructor', 'Instructor');
                }
            });
        }

           adminya:Boolean=true;
           checkroleAdmin(){
               let userlvlarr = this.useraccesdata.UserLevelId.split(',');
               for (var i = 0; i < userlvlarr.length; i++) {
                   if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                       this.adminya = false;
                   }
               }	
           }
           trainerya:Boolean=false;
           itsinstructor(){
               let userlvlarr = this.useraccesdata.UserLevelId.split(',');
               for (var i = 0; i < userlvlarr.length; i++) {
                   if (userlvlarr[i] == "TRAINER") {
                       this.trainerya = true;
                                       for(var s=0; s< userlvlarr.length; s++){
                                               if(s!=i){
                                                       if (userlvlarr[s] == "ADMIN" || userlvlarr[s] == "SUPERADMIN" || userlvlarr[s] == "ORGANIZATION" || userlvlarr[s] == "SITE" || userlvlarr[s] == "DISTRIBUTOR") {
                                                               this.trainerya = false;
                                                       }
                                               }
                                       }
                   }	
               }
           }
           distributorya:Boolean=false;
           itsDistributor(){
               let userlvlarr = this.useraccesdata.UserLevelId.split(',');
               for (var i = 0; i < userlvlarr.length; i++) {
                   if (userlvlarr[i] == "DISTRIBUTOR") {
                       this.distributorya = true;
                   }
               }
           }
           orgya:Boolean=false;
           itsOrganization(){
               let userlvlarr = this.useraccesdata.UserLevelId.split(',');
               for (var i = 0; i < userlvlarr.length; i++) {	
                   if (userlvlarr[i] == "ORGANIZATION") {
                       this.orgya = true;
                   }
               }
           }
           siteya:Boolean=false;	
           itsSite(){
               let userlvlarr = this.useraccesdata.UserLevelId.split(',');
               for (var i = 0; i < userlvlarr.length; i++) {
                   if (userlvlarr[i] == "SITE") {
                       this.siteya = true;
                   }
               }
           }
           urlGetOrgId(): string {
               var orgarr = this.useraccesdata.OrgId.split(',');	
               var orgnew = [];
               for (var i = 0; i < orgarr.length; i++) {
                   orgnew.push('"' + orgarr[i] + '"');
               }
               return orgnew.toString();
           }
           urlGetSiteId(): string {
               var sitearr = this.useraccesdata.SiteId.split(',');
               var sitenew = [];
               for (var i = 0; i < sitearr.length; i++) {
                   sitenew.push('"' + sitearr[i] + '"');
               }
               return sitenew.toString();
           }

           getSites(){
               if (this.adminya) {
                   if (this.trainerya) {	
                       this.contactId = this.useraccesdata.InstructorId;

                       this.ContactName = this.useraccesdata.ContactName;
                       this.roleinstructor = true;
                   }
               }
               let status = ['"A"', '"H"', '"I"'];
               let siteUrl: any;
               let tmpdata =null;
               if (this.adminya) {
                   if(this.distributorya){
                       siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','Distributor':'" + this.urlGetOrgId() + "'}/" + tmpdata;
                   }else{
                       if(this.orgya){
                           siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','Organization':'" + this.urlGetOrgId() + "'}/" + tmpdata;
                       }else{
                           if(this.siteya){
                               siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','SiteId':'" + this.urlGetSiteId() + "'}/" + tmpdata;
                           }else{
                               siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','ContactId':'" + this.useraccesdata.UserId + "'}/" + tmpdata;
                           }
                       }
                   }
               }else {
                   siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','EditCourse':'true'}/" + tmpdata;
               }
               this.service.httpClientGet(siteUrl,{}).subscribe((res:any)=>{
                   if(res.results.length)
                       this.listSites = res.results.map((el)=>{
                           let split = el.Sites.split('|')
                           return {
                               id: split[0].trim(),
                               itemName: el.Sites
                           }
                       })
               })
    }

    teachingLevel: string = "";
    onChangeOtherDisable(value) {
        let new_value = ''
        this.editCourse.controls.Update.value? new_value+= 'Update, ':null
        this.editCourse.controls.Essentials.value? new_value+= 'Essentials, ':null
        this.editCourse.controls.Intermediate.value? new_value+= 'Intermediate, ':null
        this.editCourse.controls.Advanced.value? new_value+= 'Advanced, ':null
        this.editCourse.controls.Customized.value? new_value+= 'Customized, ':null
        this.editCourse.controls.Other.value? new_value+= 'Other, ':null
        this.editCourse.controls["LevelTeaching"].setValue(new_value);
        this.teachingLevel = new_value
        this.editCourse.controls['LevelTeaching'].markAsTouched();
    }

    // onChangeOther(isChecked: boolean) {
    onChangeOther(value) {
        let new_value = ''
        this.editCourse.controls.Update.value? new_value+= 'Update, ':null
        this.editCourse.controls.Essentials.value? new_value+= 'Essentials, ':null
        this.editCourse.controls.Intermediate.value? new_value+= 'Intermediate, ':null
        this.editCourse.controls.Advanced.value? new_value+= 'Advanced, ':null
        this.editCourse.controls.Customized.value? new_value+= 'Customized, ':null
        this.editCourse.controls.Other.value? new_value+= 'Other, ':null
        this.editCourse.controls["LevelTeaching"].setValue(new_value);
        this.teachingLevel = new_value
        if(value == false)
            this.editCourse.controls["Komen"].disable();
        else
        this.editCourse.controls["Komen"].enable();
        this.editCourse.controls['LevelTeaching'].markAsTouched();
    }

    onChangeOther2(isChecked: boolean) {
        isChecked ? this.editCourse.controls["Komen5"].enable() : this.editCourse.controls["Komen5"].disable();
    }

    getTemplateEvaId(value) {
        this.evaTemplateId = value;
    }

    evaTemplatePreview() {
        if (this.evaTemplateId == undefined || this.evaTemplateId == "") {
            swal(
                'Information!',
                'There is no survey template selected',
                'error'
                );
        } else {
            var newWindow = window.open('template-preview/survey-preview-eva/' + this.evaTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
                return false;
        }
    }

    getTemplateCertificateId(value) {
        this.certificateTemplateId = value;
    }

    certificatePreview() {
        if (this.certificateTemplateId == undefined || this.certificateTemplateId == "") {
            swal(
                'Information!',
                'There is no certificate template selected',
                'error'
                );
        } else {
            this.loading = true;
            $("#certificate_iframe").empty();
            var iframe = document.createElement('iframe');
            iframe.src = 'template-preview/certificate-preview-eva/' + this.certificateTemplateId;
            document.getElementById("certificate_iframe").appendChild(iframe);
            // $('.iframe-certificate').css({'max-width':'870px', 'padding-top': '40px'})
            // $('iframe').css({
            //     'width': '100%',
            //     'height' : '600px'
            // })

            setTimeout(() => {
                this.loading = false;
                // $("#certificate_iframe").empty();
            }, 15000);

        }
    }

    back(){
        window.history.back()
    }
    
    getInstructorData(contactId, SiteId) {
        var kontak: any;
        this.service.httpClientGet("api/Courses/InstructorContact/" + contactId + "/" + SiteId, kontak)
        .subscribe(res => {
            kontak = res;
            if (kontak != null) {
                this.editCourse.patchValue({ SiteId: kontak.SiteId });
                this.ContactName = kontak.ContactName;
            } else {
                this.editCourse.patchValue({ SiteId: "NOT FOUND" });
                this.ContactName = "NOT FOUND";
            }
        }, error => {
            this.service.errorserver();
        });
    }

    // setFormValue(data) {
    // console.log("Data kursus => ", data);
    //Set partner type
    // let partnerTemp: any;
    // if (data[0].PartnerType === 'ATC') {
    //     partnerTemp = 1;
    // } else if (data[0].PartnerType === 'CTC') {
    //     partnerTemp = 61;
    // } else {
    //     partnerTemp = 58;
    // }

    // this.editCourse.patchValue({ PartnerType: partnerTemp });
    // this.editCourse.controls["PartnerType"].disable();
    // this.selectedPartner(partnerTemp);

    // //Set certificate type
    // let certificate: any;
    // if (partnerTemp == 58) {
    //     if (data[0].PartnerType === 'AAP Course') {
    //         certificate = 1;
    //     } else if (data[0].PartnerType === 'AAP Project') {
    //         certificate = 2;
    //     } else if (data[0].PartnerType === 'AAP Event') {
    //         certificate = 3;

    //     } else {
    //         certificate = 0;
    //     }
    //     this.selectedCertificate(certificate, data[0].HoursTraining);
    //     this.editCourse.patchValue({ CertificateType: certificate });
    // }

    // if (certificate != undefined && certificate == 3) {
    //     this.editCourse.patchValue({ LocationName: data[0].LocationName });
    // }

    // //Set course title
    // this.editCourse.patchValue({ CourseTitle: data[0].Name });

    // if (partnerTemp != 58) {
    //     //Set course facility & equipment if ATC
    //     this.editCourse.patchValue({ CourseFacility: data[0].ATCFacility });
    //     this.editCourse.patchValue({ CourseEquipment: data[0].ATCComputer });
    // }

    // //Set site id, instructor id and contact name
    // this.editCourse.patchValue({ SiteId: data[0].SiteId });
    // this.editCourse.patchValue({ InstructorId: data[0].InstructorId });
    // this.ContactName = data[0].ContactName;

    // //Set course course start and completion date, survey year too
    // let startDate = data[0].StartDate.split('-');
    // let completionDate = data[0].CompletionDate.split('-');

    // this.modelPopup1 = {
    //     "year": parseInt(startDate[0]),
    //     "month": parseInt(startDate[1]),
    //     "day": parseInt(startDate[2])
    // };

    // this.modelPopup2 = {
    //     "year": parseInt(completionDate[0]),
    //     "month": parseInt(completionDate[1]),
    //     "day": parseInt(completionDate[2])
    // };

    // this.editCourse.patchValue({ Survey: data[0].FYIndicatorKey });

    // //Set institution
    // this.editCourse.patchValue({ Institution: data[0].Institution });;

    // //Set course teaching level
    // let updateVal = (data[0].Update == '1') ? true : false;
    // this.editCourse.patchValue({ Update: updateVal });

    // let essensVal = (data[0].Essentials == '1') ? true : false;
    // this.editCourse.patchValue({ Essentials: essensVal });

    // let intermediateVal = (data[0].Intermediate == '1') ? true : false;
    // this.editCourse.patchValue({ Intermediate: intermediateVal });

    // let advancedVal = (data[0].Advanced == '1') ? true : false;
    // this.editCourse.patchValue({ Advanced: advancedVal });

    // let customVal = (data[0].Customized == '1') ? true : false;
    // this.editCourse.patchValue({ Customized: customVal });

    // if (data[0].ctl_Other == '1') {
    //     this.editCourse.patchValue({ Other: true });
    //     this.editCourse.controls["Komen"].enable();
    //     this.editCourse.patchValue({ Komen: data[0].ctl_Comment });
    // } else {
    //     this.editCourse.patchValue({ Other: false });
    //     this.editCourse.controls["Komen"].disable();
    // }

    // //Set product and product version (primary & secondary)
    // this.editCourse.patchValue({ Product1: data[0].productId });
    // this.findVersion1(data[0].productId);
    // this.editCourse.patchValue({ Version1: data[0].ProductVersionsId });

    // this.editCourse.patchValue({ Product2: data[0].productsecondaryId });
    // this.findVersion2(data[0].productsecondaryId);
    // this.editCourse.patchValue({ Version2: data[0].ProductVersionsSecondaryId });

    // //Set course teaching format
    // let instructorLedVal = (data[0].InstructorLed == '1') ? true : false;
    // this.editCourse.patchValue({ InstructorLed: instructorLedVal });

    // let onlineVal = (data[0].Online == '1') ? true : false;
    // this.editCourse.patchValue({ Online: onlineVal });

    // //Set course teaching materials
    // let autodeskVal = (data[0].Autodesk == '1') ? true : false;
    // this.editCourse.patchValue({ Autodesk: autodeskVal });

    // let aapVal = (data[0].ctm_AAP == '1') ? true : false;
    // this.editCourse.patchValue({ AAP: aapVal });

    // let atcVal = (data[0].ctm_ATC == '1') ? true : false;
    // this.editCourse.patchValue({ ATC: atcVal });

    // let independentVal = (data[0].Independent == '1') ? true : false;
    // this.editCourse.patchValue({ Independent: independentVal });

    // let independentOnlineVal = (data[0].IndependentOnline == '1') ? true : false;
    // this.editCourse.patchValue({ IndependentOnline: independentOnlineVal });

    // let atcOnlineVal = (data[0].ATCOnline == '1') ? true : false;
    // this.editCourse.patchValue({ ATCOnline: atcOnlineVal });

    // if (data[0].ctm_Other == '1') {
    //     this.editCourse.patchValue({ Other5: true });
    //     this.editCourse.controls["Komen5"].enable();
    //     this.editCourse.patchValue({ Komen5: data[0].ctm_Comment });
    // } else {
    //     this.editCourse.patchValue({ Other5: false });
    //     this.editCourse.controls["Komen5"].disable();
    // }

    // this.editCourse.patchValue({ EvaTemplate: data[0].EvaluationQuestionId });
    // this.evaTemplateId = data[0].EvaluationQuestionId;
    // this.editCourse.patchValue({ CertTemplate: data[0].CertificateId });
    // this.certificateTemplateId = data[0].CertificateId;
    // }

    getStudentPicked(id) {
        var student: any;
        this.pickedStudent = [];
        this.service.httpClientGet("api/Courses/Student/" + id, student)
        .subscribe(res => {
            student = res;
                // console.log(student);
                if (student.length > 0) {
                    for (let i = 0; i < student.length; i++) {
                        if (student[i].Firstname != null && student[i].Firstname != '') {
                            student[i].Firstname = this.service.decoder(student[i].Firstname);
                        }
                        if (student[i].Lastname != null && student[i].Lastname != '') {
                            student[i].Lastname = this.service.decoder(student[i].Lastname);
                      }

                        let status = ['Pending','Pending','Approved','Rejected']
                        var itemStatus = status[Math.floor(Math.random()*status.length)];
                        student[i].status = itemStatus
                        this.pickedStudent.push(student[i]);
                        //Karena studentnya sudah dipilih sebelumnya, maka dimasukkan ke variable selected student supaya tetap terbaca ketika masuk ke fungsi remove student
                        this.selectedStudent.push(student[i].StudentID);
                    }
                    this.studentTosendEmail = this.pickedStudent
                    this.loading = false;
                } else {
                    this.pickedStudent = [];
                    this.selectedStudent = [];
                    this.loading = false;
                }
            });
    }

    updateStudentStatus(id,status){
        this.pickedStudent = this.pickedStudent.map((el)=>{
            if(el.StudentID == id)
                return {...el,status:status}
            else return el
        })
    }

    updateStudentStatusAll(){
        console.log('All')
        swal({
            title: 'Are you sure?',
            text: "If you wish to Approve all student!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, sure!'
        }).then(result => {
            if(result)
                this.pickedStudent = this.pickedStudent.map((el)=>{
                    return {...el,status:'Approved'}
                })
        }).catch(swal.noop);
    }

    // getDataCourse(id) {
    //     var data = '';
    //     this.datadetail = [];
    //     this.service.httpClientGet("api/Courses/Detail/" + id, data)
    //         .subscribe(res => {
    //             this.data = res;
    //             //untuk set nilai site id
    //             let siteTemp: any;
    //             this.service.get(this.siteUrl + "/" + this.data.SiteId, siteTemp)
    //                 .subscribe(result => {
    //                     siteTemp = result;
    //                     siteTemp = siteTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, " ");
    //                     siteTemp = JSON.parse(siteTemp);
    //                     siteTemp = siteTemp.results[0].Sites;
    //                     this.data.SiteVal = siteTemp;
    //                     this.buildForm();
    //                 });
    //         });
    // }

    // getGeo() {
    //     var data = '';
    //     this.service.httpClientGet("api/Geo", data)
    //         .subscribe(result => {
    //             this.data = result;
    //             this.dropdownListGeo = this.data.map((item) => {
    //                 return {
    //                     id: item.geo_id,
    //                     itemName: item.geo_name
    //                 }
    //             })
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    //     this.selectedItemsGeo = [];

    //     //setting dropdown
    //     this.dropdownSettings = {
    //         singleSelection: false,
    //         text: "Please Select",
    //         selectAllText: 'Select All',
    //         unSelectAllText: 'Unselect All',
    //         enableSearchFilter: true,
    //         classes: "myclass custom-class",
    //         disabled: false,
    //         maxHeight: 120
    //     };
    // }

    // onGeoSelect(item: any) {
    //     this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // OnGeoDeSelect(item: any) {
    //     this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onGeoSelectAll(items: any) {
    //     this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onRegionSelect(item: any) {
    //     this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // OnRegionDeSelect(item: any) {
    //     this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onRegionSelectAll(items: any) {
    //     this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    // onSubRegionSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.studentFound = false;
    // }

    // onSubRegionSelectAll(items: any) {
    //     this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.studentFound = false;
    // }

    onCountriesSelect(item: any) {
        this.getStudentbyCountry();
    }
    OnCountriesDeSelect(item: any) {
        this.getStudentbyCountry();
        this.studentFound = false;
    }
    onCountriesSelectAll(item: any) {
        this.getStudentbyCountry();
    }
    onCountriesDeSelectAll(item: any) {
        this.getStudentbyCountry();
        this.studentFound = false;
    }

    getStudentbyCountry() {
        this.dataStudent = [];
        var student: any;
        if (this.selectedItemsCountry.length != 0) {
            this.loading = true;
            for (let i = 0; i < this.selectedItemsCountry.length; i++) {
                this.service.httpClientGet("api/Student/GetStudentByCountry/" + this.selectedItemsCountry[i].id, student)
                .subscribe(result => {
                    // var finalresult = result.replace(/\t/g, "\\t")
                    // student = JSON.parse(finalresult);
                    student = result;
                    if (student.length > 0) {
                        for (let j = 0; j < student.length; j++) {
                            if (student[j].Firstname != null && student[j].Firstname != '') {
                                student[j].Firstname = this.service.decoder(student[j].Firstname);
                            }
                            if (student[j].Lastname != null && student[j].Lastname != '') {
                                student[j].Lastname = this.service.decoder(student[j].Lastname);
                          }
                            this.dataStudent.push(student[j]);
                        }
                        this.notFound = false;
                        this.loading = false;
                    } else {
                        if (this.dataStudent.length == 0) {
                            this.notFound = true;
                        }
                        this.loading = false;
                    }
                });
            }
            this.studentFound = true;
        } else {
            this.studentFound = false;
        }
    }

    addStudent(id) {
        if (this.selectedStudent.length == 0) {
            this.selectedStudent.push(id);
        } else {
            var exist = false;
            for (let i = 0; i < this.selectedStudent.length; i++) {
                if (id == this.selectedStudent[i]) {
                    exist = true;
                    this.studentErrorTag = true;
                    this.studentSuccessTag = false;
                    this.errorTagMessage = "You have choose this student..";
                    setTimeout(function () {
                        this.studentErrorTag = false;
                    }.bind(this), 3000);
                }
            }
            if (exist == false && id != "") {
                this.selectedStudent.push(id);
                this.studentErrorTag = false;
            }
        }

        var index = this.dataStudent.findIndex(x => x.StudentID == id);
        if (index !== -1) {
            this.dataStudent.splice(index, 1);
        }

        this.refreshPickedStudent(this.selectedStudent);
    }

    showAddedStudent(firstname, lastname): void {
        this.studentSuccessTag = true;
        this.successTagMessage = "You added " + this.service.decoder(firstname) + " " + this.service.decoder(lastname) + " in this course..";
        setTimeout(function () {
            this.studentSuccessTag = false;
        }.bind(this), 3000);
    }

    refreshPickedStudent(selectedStudent) {
        var picked: any;
        this.pickedStudent = [];
        if (selectedStudent.length > 0) {
            for (let x = 0; x < selectedStudent.length; x++) {
                this.service.httpClientGet("api/Student/GetStudent/" + selectedStudent[x], picked)
                .subscribe(result => {
                    picked = result;
                    var exist = false;
                    if (this.pickedStudent.length == 0) {
                        this.pickedStudent.push(picked);
                        this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                    } else {
                        for (let k = 0; k < this.pickedStudent.length; k++) {
                            if (picked.StudentID == this.pickedStudent[k].StudentID) {
                                exist = true;
                                this.studentSuccessTag = false;
                            }
                        }
                        if (exist == false && picked.StudentID != "") {
                            this.pickedStudent.push(picked);
                            this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                        }
                    }
                });
            }
            this.studentPicked = true;
        } else {
            this.studentPicked = false;
        }
    }

    removeStudent(id) {
        let studentTemp = [];
        let index = this.selectedStudent.indexOf(id)
        if (index > -1) {
            this.selectedStudent.splice(index, 1);
        }

        var index2 = this.pickedStudent.findIndex(x => x.StudentID == id);
        if (index2 !== -1) {
            //Kalau pakai unshift ga akan support di IE (buat nambah array di index awal)
            this.dataStudent.unshift(this.pickedStudent[index2]);
            studentTemp = this.dataStudent;
            this.dataStudent = [];
            this.dataStudent = studentTemp;
        }

        this.refreshPickedStudent(this.selectedStudent);
    }
    roleinstructor: boolean = false;
    tmpSiteID: any;
    ngOnInit() {
        this.checkrole();
        this.checkroleAdmin();
        this.itsinstructor();
        this.itsOrganization();
        this.itsSite();
        this.itsDistributor();
        this.id = this.route.snapshot.params['id'];
        this.evalsNumber = this.route.snapshot.params['evals'];
        this.partnerType = this.route.snapshot.params['partner_type'];


    if(localStorage.getItem("languageKey") != null && localStorage.getItem("languageKey") != undefined && localStorage.getItem("languageKey") != ""){
        this.language = localStorage.getItem("languageKey");
    }
        this.loading = true;
        var data = '';
        this.datadetail = [];
        this.service.httpClientGet("api/Courses/Detail/" + this.id, data)
        .subscribe(res => {
            this.data = res;
			 
			this.lastEvalDate=this.data.EnrollmentExpireDate;
            if (this.data != 'Not Found') {
                //allow special character
                if (this.data != null) {
                    if (this.data.Name != null && this.data.Name != '') {
                        this.data.Name = this.service.decoder(this.data.Name);
                    }
                }
                    //untuk set nilai site id
                    // console.log(this.data);
                    let siteTemp: any;
                    let status = ['"A"', '"H"', '"D"', '"I"', '"T"', '"X"'];
                    // this.service.get(this.siteUrl + "/" + this.data.SiteId, siteTemp) //Diganti karena ada data lama yg site partner type statusnya TERMINATE
                    this.service.httpClientGet("api/MainSite/OldSiteCourse/{'Status':'" + status + "'}/" + this.data.SiteId, siteTemp)
                    .subscribe(result => {
                        siteTemp = result;
                        // siteTemp = siteTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, " ");
                        // siteTemp = JSON.parse(siteTemp);
						
                        siteTemp = siteTemp.results[0].Sites;
						
                        this.data.SiteVal = siteTemp;
                        /* issue 27112018 - cant edit instructor (edit course) */

                        let splitVal = (siteTemp).split(" | ");
                        this.data.SiteVal = [{ id:splitVal[0], itemName: siteTemp }];
                        this.listSites = [{ id:splitVal[0], itemName: siteTemp }]
                        this.siteId = [{ id:splitVal[0], itemName: siteTemp }]
                        this.tmpSiteID =this.data.SiteId;
                        this.CourseId = this.data.CourseId;
                        this.getInstructor(splitVal[0]);

                        /* end line issue 27112018 - cant edit instructor (edit course) */
						
                        setTimeout(() => {
                            this.getCountry();
                        }, 1000);

                        setTimeout(() => {
                            this.getPartnerType();
                            this.getSurveyIndicator();
                            this.getProduct();
                            this.getTrainingHours();
                        }, 2000);

                        setTimeout(() => {
                            this.buildForm();
                        }, 3000);

                        setTimeout(() => {
                            this.getStudentPicked(this.id);
                        }, 4000);

                        var currentDate = new Date();
                        var courseEndDate = new Date(this.data.CompletionDate);
                        let currentTime = currentDate.getTime();
                            // let endTime = courseEndDate.getTime();
                            let endTime = courseEndDate.getTime() + (24*60*60*1000); /* Able to Edit Course, even in the last day (End Date) | just 1 day */

                            if (currentTime > endTime) {
                                $(document).ready(function () {
                                    $("#editCourseForm :input").prop("disabled", true);
                                    $(".btn-danger").prop("disabled", false);
                                    $(".not-disabled").prop("disabled", false);
                                });
                            }
                        });
                } else {
                    this.loading = false;
                }
            });

        //ini udah dipindahin ke init ini lhoo
        // this.getDataCourse(this.id);

        //Ga dipake
        //ini untuk lookup student
        // this.getDataSiswa();

        //ini untuk ngambil data student sebelumnya yang sudah tersimpan di database
        // this.getStudentPicked(this.id);

        //Ga dipake
        // this.getSite();

        // this.getPartnerType();
        // this.getSurveyIndicator();
        // this.getProduct();
        // this.getTrainingHours();

        //Ga dipake
        // this.getEvaTemplate();
        // this.getCertificateTemplate();
        // this.getGeo();

        this.parentLabel = true;

        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        //untuk mengecek apakah sudah ada yang mengisi survey atau belum, jika sudah akan ditampilkan tabelnya jika belum ya tentu tidak...
        if(this.partnerType != "AAP Event"){
            if (this.evalsNumber > 0) {
                let dataEvals: any;
                this.service.httpClientGet("api/Courses/StudentEvaluation/" + this.id, dataEvals)
                .subscribe(res => {
                    dataEvals = res;
                    if (dataEvals.length != 0) {
                        for (var i = 0; i < dataEvals.length; i++) {
                            if (dataEvals[i].StudentName != null && dataEvals[i].StudentName != '') {
                                dataEvals[i].StudentName = this.service.decoder(dataEvals[i].StudentName);
                            }

                          }
                        this.dataEvals = dataEvals;
                        this.showEvals = true;
                    } else {
                        this.showEvals = false;
                    }
                });
            }
        } else{
            //untuk aap event, ambil data berdasarkan banyaknya student yang mengikuti event (Student tidak mengisi survey)
            //Berbeda dengan aap course, project, dan ATC. Student diwajibkan mengisi survey terlebih dahulu
            let aapEventData: any;
            this.service.httpClientGet("api/Courses/StudentAAPEvent/" + this.id, aapEventData)
                .subscribe(res => {
                    aapEventData = res;
                    if (aapEventData.length != 0) {
                        for (var i = 0; i < aapEventData.length; i++) {
                            if (aapEventData[i].StudentName != null && aapEventData[i].StudentName != '') {
                                aapEventData[i].StudentName = this.service.decoder(aapEventData[i].StudentName);
                            }

                          }
                        this.dataEvals = aapEventData;
                        if(aapEventData[0].isShow == "TRUE"){
                            this.showEvals = true;
                        } else{
                            this.showEvals = false;    
                        }
                    } else {
                        this.showEvals = false;
                    }
                });
        }
        

        if (!this.checkrole()) {
            this.hiddenfield = true;
        }
        else {
            this.hiddenfield = false;
        }
    }

    getCountry() {
        var data: any;
        this.service.httpClientGet("api/Countries", data)
        .subscribe(result => {
            if (result == "Not found") {
                this.service.notfound();
                this.dropdownListCountry = null;
                this.loading = false;
            }
            else {
                this.dropdownListCountry = Object.keys(result).map(function (Index) {
                    return {
                      id: result[Index].countries_code,
                      itemName: result[Index].countries_name
                    }
                  })
            }
        });
        this.selectedItemsCountry = [];
    }

    inputStudent(id) {
        var input: any;
        var studentTemp = {
            'CourseId': this.id,
            'StudentID': id,
            'Status': this.editCourse.value.Status,
            'CourseTaken': 0,
            'SurveyTaken': 0
        };
        let studentData = JSON.stringify(studentTemp);
        this.service.httpClientPost("api/Courses/CourseStudent", studentData)
        .subscribe(resultPost => {
            input = resultPost;
            if (input["code"] != '1') swal('Information', 'Error when update into Table CourseStudent', 'error');
        });
    }

    deleteStudent(id) 
    {
        swal({
            title: 'Are you sure?',
            text: "Are you sure if you wish to remove the record?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then(result => {
              if (result == true) {
                var keyword : any = {
                    CourseId: this.id,
                    StudentID: id
                   
                };
                // keyword = JSON.stringify(keyword);
                this.service.httpClientPost("api/Courses/SurveyCheck",keyword).subscribe(result => {
                    if (result["code"] == '1') { 
                        var del: any;
                        this.service.httpClientDelete("api/Courses/CourseStudent/where/{'CourseId':'" + this.id + "','StudentID':'" + id + "'}", del)
                        .subscribe(res => {
                            del =res;
                            if (del["code"] != '1') swal('Information', 'Error when update into Table CourseStudent', 'error');
                            else{
                                this.ngOnInit();
                            }
                        });
                    }
                    else{
                        swal('Information', result["message"], 'error');
                    }
                });
       
    }
    })
} 
    // updateCourseStudent() {
    //     var student: any;
    //     this.service.httpClientGet("api/Courses/Student/" + this.id, student)
    //         .subscribe(res => {
    //             student = res;
    //             if (student.length > 0) {
    //                 for (let m = 0; m < student.length; m++) {
    //                     var existInPicked = false;
    //                     for (let n = 0; n < this.pickedStudent.length; n++) {
    //                         if (student[m].StudentID == this.pickedStudent[n].StudentID) existInPicked = true;
    //                     }
    //                     //Jika ternyata student yang dipilih sebelumnya tidak ada di pilihan baru, maka dihapus di database table course student
    //                     if (existInPicked == false) this.deleteStudent(student[m].StudentID);
    //                 }

    //                 for (let j = 0; j < this.pickedStudent.length; j++) {
    //                     var existInDB = false;
    //                     for (let k = 0; k < student.length; k++) {
    //                         if (this.pickedStudent[j].StudentID == student[k].StudentID) existInDB = true;
    //                     }
    //                     //Jika ternyata student yang dipilih di pilihan baru belum ada di database, maka dimasukkan ke table course student
    //                     if (existInDB == false) this.inputStudent(this.pickedStudent[j].StudentID);
    //                 }
    //                 // swal('Information!', 'Update Data Success', 'success');
    //                 this.router.navigate(['/course/course-list']);
    //                 this.loading = false;
    //             } else {
    //                 //Jika student di kursus ini belum ada sama sekali maka akan masuk kondisi ini
    //                 for (let j = 0; j < this.pickedStudent.length; j++) {
    //                     this.inputStudent(this.pickedStudent[j].StudentID);
    //                 }
    //                 this.router.navigate(['/course/course-list']);
    //                 this.loading = false;
    //             }
    //         });
    // }

    updateCTM(data) {
        var l: any;
        var ctm = {
            'Autodesk': this.editCourse.value.Autodesk,
            'AAP': this.editCourse.value.AAP,
            'ATC': this.editCourse.value.ATC,
            'Independent': this.editCourse.value.Independent,
            'IndependentOnline': this.editCourse.value.IndependentOnline,
            'ATCOnline': this.editCourse.value.ATCOnline,
            'Other5': this.editCourse.value.Other5,
            'Komen5': this.editCourse.value.Komen5,
            'Status': this.editCourse.value.Status
        };
        let courseTrainingMaterial = JSON.stringify(ctm);
        this.service.httpCLientPut("api/Courses/CourseTrainingMaterial/" + this.id, courseTrainingMaterial)
        .subscribe(res5 => {
            l = res5;
            if (l["code"] == 1) {
                    // this.updateCourseStudent();
                    this.putToLocalStorage(data);
                    setTimeout(() => {
                        this.router.navigate(['/course/course-list']);
                        this.loading = false;
                    }, 5000);
                } else {
                    swal(
                        'Information',
                        'Error when update into Table CourseTrainingMaterial',
                        'error'
                        );
                }
            });
    }

    updateCTF(data) {
        var k: any;
        var ctf = {
            'InstructorLed': this.editCourse.value.InstructorLed,
            'Online': this.editCourse.value.Online,
            'Status': this.editCourse.value.Status
        };
        let courseTrainingFormat = JSON.stringify(ctf);
        this.service.httpCLientPut("api/Courses/CourseTrainingFormat/" + this.id, courseTrainingFormat)
        .subscribe(res4 => {
            k = res4;
            if (k["code"] == 1) {
                this.updateCTM(data);
            } else {
                swal(
                    'Information',
                    'Error when update into Table CourseTrainingFormat',
                    'error'
                    );
            }
        });
    }

    updateCourseSoftware(data) {
        var j: any;
        var cs = {
            'Product1': this.editCourse.value.Product1,
            'Version1': this.editCourse.value.Version1,
            'Product2': this.editCourse.value.Product2,
            'Version2': this.editCourse.value.Version2,
            'Status': this.editCourse.value.Status
        };
        let courseSoftware = JSON.stringify(cs);
        this.service.httpCLientPut("api/Courses/CourseSoftware/" + this.id, courseSoftware)
        .subscribe(res3 => {
            j = res3;
            if (j["code"] == '1') {
                if (this.editCourse.value.CertificateType == 2 || this.editCourse.value.CertificateType == 3) {
                        // this.updateCourseStudent();
                        this.putToLocalStorage(data);
                        setTimeout(() => {
                            this.router.navigate(['/course/course-list']);
                            this.loading = false;
                        }, 5000);
                    } else {
                        this.updateCTF(data);
                    }
                } else {
                    swal(
                        'Information',
                        'Error when update into Table CourseSoftware',
                        'error'
                        );
                }
            });
    }

    updateCTL(data) {
        var i: any;
        var ctl = {
            'Update': this.editCourse.value.Update,
            'Essentials': this.editCourse.value.Essentials,
            'Intermediate': this.editCourse.value.Intermediate,
            'Advanced': this.editCourse.value.Advanced,
            'Customized': this.editCourse.value.Customized,
            'Other': this.editCourse.value.Other,
            'Comment': this.editCourse.value.Komen,
            'Status': this.editCourse.value.Status
        };
        let courseTeachingLevel = JSON.stringify(ctl);
        this.service.httpCLientPut("api/Courses/CourseTeachingLevel/" + this.id, courseTeachingLevel)
        .subscribe(res2 => {
            i = res2;
            if (i["code"] == '1') {
                this.updateCourseSoftware(data);
            } else {
                swal(
                    'Information',
                    'Error when update into Table CourseTeachingLevel',
                    'error'
                    );
            }
        });
    }

    updateCourse(data) {
        var x: any;
        this.service.httpCLientPut("api/Courses/" + this.id, data)
        .subscribe(res1 => {
            x = res1;
            if (x["code"] == '1') {
                this.editCourse.value.CertificateType == 3 ? this.updateCourseSoftware(data) : this.updateCTL(data);
            } else {
                swal(
                    'Information',
                    'Error when update into Table Courses',
                    'error'
                    );
            }
        });
    }

    buildForm(): void {
        // console.log(this.data);

        // Set partner type
        // let partnerTemp: any;
        if (this.data.PartnerType == 'ATC') {
            this.partnerTemp = 1;
        } else if (this.data.PartnerType == 'CTC') {
            this.partnerTemp = 61;
        } else {
            this.partnerTemp = 58;
        }

        this.editCourse.controls["PartnerType"].disable();
        this.selectedPartner(this.partnerTemp);

        //Set certificate type
        if (this.partnerTemp == 58) {
            if (this.data.PartnerType == 'AAP Course') {
                this.certificate = 1;
            } else if (this.data.PartnerType == 'AAP Project') {
                this.certificate = 2;
            } else if (this.data.PartnerType == 'AAP Event') {
                this.certificate = 3;
            } else {
                this.certificate = 0;
            }
            this.selectedCertificate(this.certificate);
        }
        // this.editCourse.controls["CertificateType"].disable();
        this.ContactName = this.data.ContactName;

        //Set course course start and completion date, survey year too
        let startDate = this.data.StartDate.split('-');
        let completionDate = this.data.CompletionDate.split('-');
        this.modelPopup1 = {
            "year": parseInt(startDate[0]),
            "month": parseInt(startDate[1]),
            "day": parseInt(startDate[2])
        };

        this.modelPopup2 = {
            "year": parseInt(completionDate[0]),
            "month": parseInt(completionDate[1]),
            "day": parseInt(completionDate[2])
        };

        //Set course teaching level
        let updateVal = (this.data.Update == '1') ? true : false;
        let essensVal = (this.data.Essentials == '1') ? true : false;
        let intermediateVal = (this.data.Intermediate == '1') ? true : false;
        let advancedVal = (this.data.Advanced == '1') ? true : false;
        let customVal = (this.data.Customized == '1') ? true : false;

        let otherVal: boolean;
        if (this.data.ctl_Other == '1') {
            otherVal = true;
            this.editCourse.controls["Komen"].enable();
        } else {
            otherVal = false;
            this.editCourse.controls["Komen"].disable();
        }

        let teachinglvlval = "";
        if (this.data.Update == '1') {
            teachinglvlval = "Update";
        } else if (this.data.Essentials == '1') {
            teachinglvlval = "Essentials";
        } else if (this.data.Intermediate == '1') {
            teachinglvlval = "Intermediate";
        } else if (this.data.Advanced == '1') {
            teachinglvlval = "Advanced";
        } else if (this.data.Customized == '1') {
            teachinglvlval = "Customized";
        } else if (this.data.ctl_Other == '1') {
            teachinglvlval = "Other";
        }
        this.teachingLevel = teachinglvlval;

        //Set product and product version (primary & secondary)
        this.findVersion1(this.data.productId);
        this.findVersion2(this.data.productsecondaryId);

        //Set course teaching format
        let instructorLedVal = (this.data.InstructorLed == '1') ? true : false;
        let onlineVal = (this.data.Online == '1') ? true : false;
		
		if(this.data.HoursTrainingOther == '1'){
			var actHour=this.data.HoursTraining;
			this.data.HoursTraining='Other';
			this.trainingHoursOther=actHour;		
		}
        //Set course teaching materials
        let autodeskVal = (this.data.Autodesk == '1') ? true : false;
        let aapVal = (this.data.ctm_AAP == '1') ? true : false;
        let atcVal = (this.data.ctm_ATC == '1') ? true : false;
        let independentVal = (this.data.Independent == '1') ? true : false;
        let independentOnlineVal = (this.data.IndependentOnline == '1') ? true : false;
        let atcOnlineVal = (this.data.ATCOnline == '1') ? true : false;

        let ctmOther: boolean;
        if (this.data.ctm_Other == '1') {
            ctmOther = true;
            this.editCourse.controls["Komen5"].enable();
        } else {
            ctmOther = false;
            this.editCourse.controls["Komen5"].disable();
        }

        this.contactID = this.data.ContactId;

        let imageLogo = {};
        let img;
        if(this.data.InstitutionLogo != ""){
            imageLogo = {
                filename: this.data.LogoName,
                filetype: this.data.LogoType,
                value: this.data.InstitutionLogo
            }
            this.imgSrc =  "data:" + this.data.LogoType + ";base64," + this.data.InstitutionLogo;
            this.imgLogoName = this.data.LogoName;
            this.projectLogoUpload = false;
            this.isAvailableLogo = true;
        } else{
            imageLogo = {
                filename: "",
                filetype: "",
                value: ""
            }
        }

        // this.dataService1 = this.completerService.remote(this.siteUrl + "/" + this.data.SiteId, this.data.SiteId, null);

        let CourseId = new FormControl(this.data.CourseId);
        let PartnerType = new FormControl(this.partnerTemp);
        let CertificateType = new FormControl(this.certificate);
        let CourseTitle = new FormControl(this.data.Name, [Validators.required, Validators.maxLength(40)]);
        let SiteId = new FormControl(this.data.SiteVal, Validators.required);
        let InstructorId = new FormControl(this.data.InstructorId + " | " + this.data.ContactName, Validators.required);
        let Institution = new FormControl(this.data.Institution, Validators.required);
        let StartDate = new FormControl('', Validators.required);
        let EndDate = new FormControl('', Validators.required);
        let Survey = new FormControl(this.data.FYIndicatorKey, Validators.required);
        let Update = new FormControl(updateVal);
        let Essentials = new FormControl(essensVal);
        let Intermediate = new FormControl(intermediateVal);
        let Advanced = new FormControl(advancedVal);
        let Customized = new FormControl(customVal);
        let Other = new FormControl(otherVal);
        let Komen = new FormControl(this.data.ctl_Comment);
        let Status = new FormControl('');
        let Product1 = new FormControl(this.data.productId, Validators.required);
        let Version1 = new FormControl(this.data.ProductVersionsId, Validators.required);
        let Product2 = new FormControl(this.data.productsecondaryId);
        let Version2 = new FormControl(this.data.ProductVersionsSecondaryId);
        let TrainingHours = new FormControl(this.data.HoursTraining, Validators.required);
        let InstructorLed = new FormControl(instructorLedVal);
        let Online = new FormControl(onlineVal);
        let TrainingType = new FormControl(this.data.TrainingTypeKey);
        let Autodesk = new FormControl(autodeskVal);
        let AAP = new FormControl(aapVal);
        let ATC = new FormControl(atcVal);
        let Independent = new FormControl(independentVal);
        let IndependentOnline = new FormControl(independentOnlineVal);
        let ATCOnline = new FormControl(atcOnlineVal);
        let Other5 = new FormControl(ctmOther);
        let Komen5 = new FormControl(this.data.ctm_Comment);
        let CourseFacility = new FormControl(this.data.ATCFacility, Validators.required);
        let CourseEquipment = new FormControl(this.data.ATCComputer, Validators.required);
        // let EvaTemplate = new FormControl('', Validators.required);
        // let CertTemplate = new FormControl('', Validators.required);
        let StudentID = new FormControl('');
        let ProjectType = new FormControl(this.data.ProjectTypeKey, Validators.required);
        let LocationName = new FormControl(this.data.LocationName, Validators.required);
        let EventType = new FormControl(this.data.EventType2, Validators.required);
        let LearnerType = new FormControl(this.data.EventTypeKey, Validators.required);
        let nameInstructor = new FormControl(this.data.ContactName);
        let LevelTeaching = new FormControl(teachinglvlval,Validators.required);
        let InstitutionLogo = new FormControl(imageLogo); //change to save logo into database
	let HoursTrainingOther=new FormControl(0);
        this.editCourse = new FormGroup({
            CourseId: CourseId,
            PartnerType: PartnerType,
            CertificateType: CertificateType,
            CourseTitle: CourseTitle,
            SiteId: SiteId,
            InstructorId: InstructorId,
            Institution: Institution,
            StartDate: StartDate,
            EndDate: EndDate,
            Survey: Survey,
            Update: Update,
            Essentials: Essentials,
            Intermediate: Intermediate,
            Advanced: Advanced,
            Customized: Customized,
            Other: Other,
            Komen: Komen,
            Status: Status,
            Product1: Product1,
            Version1: Version1,
            Product2: Product2,
            Version2: Version2,
            TrainingHours: TrainingHours,
			HoursTrainingOther:HoursTrainingOther,
            InstructorLed: InstructorLed,
            Online: Online,
            TrainingType: TrainingType,
            Autodesk: Autodesk,
            AAP: AAP,
            ATC: ATC,
            Independent: Independent,
            IndependentOnline: IndependentOnline,
            ATCOnline: ATCOnline,
            Other5: Other5,
            Komen5: Komen5,
            CourseFacility: CourseFacility,
            CourseEquipment: CourseEquipment,
            // EvaTemplate: EvaTemplate,
            // CertTemplate: CertTemplate,
            StudentID: StudentID,
            ProjectType: ProjectType,
            LocationName: LocationName,
            EventType: EventType,
            LearnerType: LearnerType,
            nameInstructor: nameInstructor,
            LevelTeaching: LevelTeaching,
            InstitutionLogo: InstitutionLogo
        });
        console.log(this.editCourse)
    }

    onSubmit() {
        if (this.partnerSelected == 1) {
            this.editCourse.removeControl('CertificateType');
            this.editCourse.removeControl('Institution');
            this.editCourse.removeControl('TrainingType');
            this.editCourse.removeControl('ProjectType');
            this.editCourse.removeControl('EventType');
            this.editCourse.removeControl('LearnerType');
            this.editCourse.removeControl('LocationName');
            this.editCourse.controls['CourseFacility'].markAsTouched();
            this.editCourse.controls['CourseEquipment'].markAsTouched();
            this.editCourse.controls['LevelTeaching'].markAsTouched();
            this.editCourse.controls['TrainingHours'].markAsTouched();
        } else {
            if (this.certificateSelected == 1) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('ProjectType');
                this.editCourse.removeControl('EventType');
                this.editCourse.removeControl('LearnerType');
                this.editCourse.removeControl('LocationName');
                this.editCourse.controls['Institution'].markAsTouched();
                this.editCourse.controls['TrainingType'].markAsTouched();
                this.editCourse.controls['TrainingHours'].markAsTouched();
                this.editCourse.controls['LevelTeaching'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 2) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('TrainingType');
                this.editCourse.removeControl('EventType');
                this.editCourse.removeControl('LearnerType');
                this.editCourse.removeControl('LocationName');
                this.editCourse.controls['Institution'].markAsTouched();
                this.editCourse.controls['ProjectType'].markAsTouched();
                this.editCourse.controls['TrainingHours'].markAsTouched();
                this.editCourse.controls['LevelTeaching'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 3) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('Institution');
                this.editCourse.removeControl('TrainingType');
                this.editCourse.removeControl('ProjectType');
                this.editCourse.removeControl('TrainingHours');
                this.editCourse.removeControl('LevelTeaching');
                this.editCourse.controls['LocationName'].markAsTouched();
                this.editCourse.controls['EventType'].markAsTouched();
                this.editCourse.controls['LearnerType'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            }
            this.editCourse.controls['CertificateType'].markAsTouched();
        }
		
        // this.editCourse.controls['PartnerType'].markAsTouched();

        this.editCourse.controls['CourseTitle'].markAsTouched();
        this.editCourse.controls['SiteId'].markAsTouched();
        this.editCourse.controls['InstructorId'].markAsTouched();
        this.editCourse.controls['StartDate'].markAsTouched();
        this.editCourse.controls['EndDate'].markAsTouched();
        this.editCourse.controls['Survey'].markAsTouched();
        this.editCourse.controls['Product1'].markAsTouched();
        this.editCourse.controls['Version1'].markAsTouched();

        // this.editCourse.controls['Product2'].markAsTouched();
        // this.editCourse.controls['Version2'].markAsTouched();
        // this.editCourse.controls['EvaTemplate'].markAsTouched();
        // this.editCourse.controls['CertTemplate'].markAsTouched();

        if (this.editCourse.valid) {
			 let formData = this.editCourse.value;
			 let htmlString = '<table class="preview-table" style="table-layout: fixed; width: 100%;">';
            Object.keys(formData).map((key)=>{
                if(formData[key] != "" && formData[key] != null && key!='CertificateType'){
					
                    let label = key.replace(/([A-Z])/g, ' $1').trim()
                    let content = formData[key]						    						
                    if(key=='PartnerType')
                        content = this.dropdownPartner.find((el)=> el.RoleId == content)['RoleName']
					if(key=='Product1')
                        label = 'Primary Product'
					if(key=='Version1')
						label = 'Primary Version'
					if(key=='Product2')
						label = 'Secondary Product'
					if(key=='Version2')
						label = 'Secondary Version'
					if(key=='Autodesk')
						label = 'Autodesk Official Courseware (any Autodesk branded material)'						
					if(key=='InstructorLed')
						label = 'Instructor-led in the classroom'					
					if(key=='Online')
						label = 'Online or e-learning'	
					if(key=='AAP')
						label='Autodesk Authors and Publishers (AAP) Program Courseware'
					if(key=='ATC')
						label='Course material developed by the ATC'
					if(key=='Independent')
						label = 'Course material developed by an independent vendor or instructor'		
					if(key=='IndependentOnline')
						label = 'Online e-learning course material developed by an independent vendor or instructor'
					if(key=='ATCOnline')
						label='Online e-learning course material developed by the ATC'
					if(key=='Komen5')
						label='Other'
                    if(key=='ContactId')
                        label = 'Instructor ID'
                    if(key=='StartDate' || key=='EndDate')
                        content = content.day+'/'+content.month+'/'+content.year
                    if(key=='CourseFacility')
                        content = formData[key] == 1 ? 'ATC Facility':'Onsite'
                    if(key=='CourseEquipment')
                        content = formData[key] == 1 ? 'ATC Computer':'Onsite'
                    if(key=='Product1')						
                        content = this.products.find((el)=> el.productId == content)['productName']
                    if(key=='Version1')						
                        content = this.versions1.find((el)=> el.ProductVersionsId == content)['Version']
                    if(key=='Product2')						
                        content = this.products.find((el)=> el.productId == content)['productName']
                    if(key=='Version2')				
                        content = this.versions2.find((el)=> el.ProductVersionsId == content)['Version']
					if(key=='TrainingHours')
						if(this.trainingHoursOther != undefined){
							content = this.trainingHoursOther +" hours";
						
                        }
                    if(key=='SiteId')
                        content = formData[key][0].id						
					if(key=='Autodesk')						
						content='Yes'
					if(key=='InstructorLed')						
						content='Yes'
					if(key=='Online')						
						content='Yes'
					if(key=='ATC')
						content='Yes'
					if(key=='AAP')
						content='Yes'
					if(key=='Independent')
						content='Yes'
					if(key=='IndependentOnline')
						content='Yes'
					if(key=='ATCOnline')
						content='Yes'
                    if(key=='InstitutionLogo')
                    {
                        if (formData[key].filetype == "" || formData[key].value == "") {
                            content = 'No logo uploaded'
                        }
                        else {
                            content = '<img style="height:70px;width:70px;" src=' + 'data:' + formData[key].filetype + ';base64,' + formData[key].value + '></img>'
                        }
                    }
                    if(key=='ProjectType')
                        content = this.typeOf.find((el)=> el.Key == content)['KeyValue']
                    if(key=='EventType')						
                        content = this.eventType.find((el)=> el.Key == content)['KeyValue']
                    if(key=='LearnerType')						
                        content = this.typeOf.find((el)=> el.Key == content)['KeyValue']
                    if(key=='TrainingType')						
                        content = this.typeOf.find((el)=> el.Key == content)['KeyValue']
					if(key!='ContactName' && key!='Other5')
						htmlString+= '<tr><th width="330" align="left" style="width:80%;"><div  style="word-wrap: break-word;white-space:normal;">'+label+'</div></th><td align="left" style="width:80%"><div  style="word-wrap: break-word;white-space:normal;">'+content+'</div></td></tr>'
                }
				})
			
			
			
			htmlString+='</table>'
            swal({
                title: 'Are you sure?',
                text: "If you wish to Add Course!",
				 width: "50rem",
                html: htmlString,
                //type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, sure!'
            }).then(result => {
                if(result == true){
                    this.loading = true;
                    // let siteTemp = (this.editCourse.value.SiteId).split(" | ");
                    // this.editCourse.value.SiteId = siteTemp[0];
                    this.editCourse.value.InstructorId = this.contactID;
                    this.editCourse.value.PartnerType = this.coursePartner;

                    this.editCourse.value.StartDate = this.formatdate.dateCalendarToYMD(this.editCourse.value.StartDate);
                    this.editCourse.value.EndDate = this.formatdate.dateCalendarToYMD(this.editCourse.value.EndDate);
                    this.editCourse.value.Status = "A";

                    // this.editCourse.value.Update = false;
                    // this.editCourse.value.Essentials = false;
                    // this.editCourse.value.Intermediate = false;
                    // this.editCourse.value.Advanced = false;
                    // this.editCourse.value.Customized = false;
                    // this.editCourse.value.Other = false;
					
					if(this.editCourse.value.TrainingHours == "Other"){
						this.editCourse.value.TrainingHours=this.trainingHoursOther;
						this.editCourse.value.HoursTrainingOther=1;
					}
                    // if (this.editCourse.value.Update == "") this.editCourse.value.Update = false;
                    // if (this.editCourse.value.Essentials == "") this.editCourse.value.Essentials = false;
                    // if (this.editCourse.value.Intermediate == "") this.editCourse.value.Intermediate = false;
                    // if (this.editCourse.value.Advanced == "") this.editCourse.value.Advanced = false;
                    // if (this.editCourse.value.Customized == "") this.editCourse.value.Customized = false;
                    // if (this.editCourse.value.Other == "") this.editCourse.value.Other = false;
                    
                    if (this.editCourse.value.InstructorLed == "") this.editCourse.value.InstructorLed = false;
                    if (this.editCourse.value.Online == "") this.editCourse.value.Online = false;
                    if (this.editCourse.value.Autodesk == "") this.editCourse.value.Autodesk = false;
                    if (this.editCourse.value.AAP == "") this.editCourse.value.AAP = false;
                    if (this.editCourse.value.ATC == "") this.editCourse.value.ATC = false;
                    if (this.editCourse.value.Independent == "") this.editCourse.value.Independent = false;
                    if (this.editCourse.value.IndependentOnline == "") this.editCourse.value.IndependentOnline = false;
                    if (this.editCourse.value.ATCOnline == "") this.editCourse.value.ATCOnline = false;
                    if (this.editCourse.value.Other5 == "") this.editCourse.value.Other5 = false;

                    //Audit Log
                    this.editCourse.value.cuid = this.useraccesdata.ContactName;
                    this.editCourse.value.UserId = this.useraccesdata.UserId;
                    //End Audit Log
                    //allow special character
                    this.editCourse.value.CourseTitle = this.service.encoder(this.editCourse.value.CourseTitle);

                    // let data = JSON.stringify(this.editCourse.value);
                    let pre_process_data = {...this.editCourse.value, SiteId:this.editCourse.value.SiteId[0].id}
                    let data = JSON.stringify(pre_process_data);
                    // console.log(JSON.parse(data));
                    this.updateCourse(data);
                }
            }).catch(swal.noop);
        } else {
            console.log("Ada yang ga valid");
            this.loading = false;
            swal(
                'Field is Required!',
                'Please enter the Edit Course form required!',
                'error'
                )
        }

    }

    putToLocalStorage(data) {
        var dataJson = JSON.parse(data)
        // console.log(dataJson);
        let cert: any;
        // let certdropdown: any;
        let selcert: any;
        // let selcertdropdown: any;

        var selectedPartner: any;
        if (dataJson.PartnerType == "ATC" || dataJson.PartnerType == "1") {
            selectedPartner = [{ id: "ATC", itemName: "Authorized Training Center (ATC)" }];
        } else if (dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event" || dataJson.PartnerType == "58") {
            selectedPartner = [{ id: "AAP", itemName: "Authorized Academic Partner (AAP)" }];
        } else {
            selectedPartner = [{ id: "ATC", itemName: "Authorized Training Center (ATC)" }, { id: "AAP", itemName: "Authorized Academic Partner (AAP)" }];
        }

        // if(dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event"){
            if (dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event" || dataJson.PartnerType == "58") {
                this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", '')
                .subscribe(res => {
                    cert = res;
                    this.certdropdown = cert.map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Key':'" + dataJson.CertificateType + "'}", '')
                    .subscribe(res => {
                        selcert = res;
                        this.selcertdropdown = selcert.map((item) => {
                            return {
                                id: item.Key,
                                itemName: item.KeyValue
                            }
                        })
                    });
                });
        } else if (dataJson.PartnerType == "ATC" || dataJson.PartnerType == "1") { //issue (salah populate certificate pas edit / add atc) ASAP 25/07/2018
            this.certdropdown = [];
            this.selcertdropdown = [];
        }
        else {
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", '')
            .subscribe(res => {
                cert = res;
                this.certdropdown = cert.map((item) => {
                    return {
                        id: item.Key,
                        itemName: item.KeyValue
                    }
                });
                this.selcertdropdown = cert.map((item) => {
                    return {
                        id: item.Key,
                        itemName: item.KeyValue
                    }
                });
            });
        }

        setTimeout(() => {
            var params =
            {
                idCourse: dataJson.CourseId,
                selectPartner: selectedPartner,
                dropPartner: this.dropdownPartner.map((item) => {
                    return {
                        id: item.RoleCode,
                        itemName: item.RoleName
                    }
                }),
                dropCerti: this.certdropdown,
                selectCerti: this.selcertdropdown,
                idOrg: "",
                idSite: "",
                idInstructor: "",
                titleCourse: "",
                courseStart: dataJson.StartDate,
                courseEnd: dataJson.EndDate,
                year: "",
                sortV: "CourseId",
                sortD: "ASC"
            }

            localStorage.setItem("filter", JSON.stringify(params));
        }, 3000)
    }

    // DownloadCertificateStudent(FyId, PartnerType, CourseId, StudentId, languageId) {
    DownloadCertificateStudent(FyId, PartnerType, CourseId, StudentId) {
        this.loading = true;

        let certificateType;
        let data: any;

        var language = "35";

        if(localStorage.getItem("languageKey") != null && localStorage.getItem("languageKey") != undefined && localStorage.getItem("languageKey") != ""){
            language = localStorage.getItem("languageKey");
        }

        switch (PartnerType) {
            case "AAP Course":
            certificateType = 1;
            break;
            case "AAP Project":
            certificateType = 2;
            break;
            case "AAP Event":
            certificateType = 3;
            break;

            default:
            certificateType = 0;
            break;
        }

        // if (TrainingId != null && ProjectId == null && EventId == null) {
        //     certificateType = 1;
        // } else if (TrainingId == null && ProjectId != null && EventId == null) {
        //     certificateType = 2;
        // } else if (TrainingId == null && ProjectId == null && EventId != null) {
        //     certificateType = 3;
        // } else {
        //     certificateType = 0;
        // }

        this.service.httpClientGet("api/Certificate/where/{'CertificateTypeId':'" + certificateType + "','Year':'" + FyId + "','LanguageCertificate':'" + language + "','PartnerType':'" + this.partnerTemp + "','courseid':'" + CourseId + "','UserId':'" + this.useraccesdata.UserId + "'}", data)
        .subscribe(result => {
            if (result == "Not found" || Object.keys(result).length < 1) {
                console.log("Certificate Not Found");
                this.loading = false;
            }
            else {
                // data = result.replace(/\t'/g, "\\t");
                // data = result.replace(/\r'/g, "\\r");
                // data = result.replace(/\n'/g, "\\n");
                // data = JSON.parse(result);
                data = result;



                // var newWindow = window.open('template-preview/certificate-download-student-eva/' + data[0].CertificateId + '/' + CourseId + '/' + StudentId, 'mywin', 'left=270,top=30,width=900,height=650,toolbar=1,resizable=0');
                // if (window.focus()) { newWindow.focus() }
                //     return false;
                $("#certificate_iframe_download").empty();
                var iframe = document.createElement('iframe');
                iframe.src = 'template-preview/certificate-download-student-eva/' + data[0].CertificateId + '/' + CourseId + '/' + StudentId;
                document.getElementById("certificate_iframe_download").appendChild(iframe);
                // $('.iframe-certificate-download-admin').css({'max-width':'870px', 'padding-top': '40px'})
                // $('iframe').css({
                //     'width': '100%',
                //     'height' : '600px'
                // })
                this.loading = false;


            }
        },
        error => {
            this.service.errorserver();
        });
    }

    DownloadCertificates() {
        if (!this.dataEvals || this.dataEvals.length == 0)
            return;
        
        this.loading = true;
        var year = this.dataEvals[0].FYIndicatorKey;
        var courseId = this.dataEvals[0].CourseId;
        var studentIds = this.dataEvals.map(d => {return d.StudentId});
        var data = {
            partnerTypeId: this.partnerTemp,
            certificateTypeId: this.certificate,
            Year: year,
            languageCertificate: this.language,
            StudentIds: studentIds,
            courseId: courseId
        }
        this.service.httpClientPostWithOptions("api/Certificate/BatchDownload", data, { responseType: 'arraybuffer'})
        .subscribe(result => {
            var blob = new Blob([result], {type: "application/zip"});
            this.loading = false;
            saveAs(blob, "Certificates.zip"); 
        },
        error => {
            this.service.errorserver();
            this.loading = false;
        });
    }
}
