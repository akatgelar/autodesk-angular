import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterStudent' })
export class DataFilterStudentPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => (row.StudentID.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Firstname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Lastname.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Email.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-confirm-final-student-eva',
    templateUrl: './confirm-final-student-eva.component.html',
    styleUrls: [
        './confirm-final-student-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ConfirmFinalStudentEVAComponent implements OnInit {

    private _serviceUrl = 'api/Courses';
    data;
    id: string = '';
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    private selectedStudent = [];
    private courseTitle: string = "";
    private courseCode: string = "";

    constructor(private route: ActivatedRoute, private router: Router, private service: AppService) {

    }

    getStudent(CourseId) {
        var dataTemp: any;
        this.service.httpClientGet("api/Courses/Student/" + CourseId, dataTemp)
            .subscribe(res => {
                dataTemp = res;
                if (dataTemp.length != 0) {
                    this.data = dataTemp;
                    this.courseTitle = dataTemp[0].Name;
                    this.courseCode = dataTemp[0].CourseCode;
                } else {
                    // this.router.navigate(['/eva/course-list']);
                    swal('Information!', 'Data Student Not Found', 'error');
                }
            }, error => {
                this.service.errorserver();
            });
    }

    onChange(id: any, isChecked: boolean) {
        if (isChecked) {
            this.selectedStudent.push(id);
        } else {
            let index = this.selectedStudent.indexOf(id)
            if (index > -1) {
                this.selectedStudent.splice(index, 1);
            }
        }
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.getStudent(this.id);
    }

    saveStudentAttendance() {
        if (this.selectedStudent.length != 0) {
            var val = '';
            for (let i = 0; i < this.selectedStudent.length; i++) {
                if (this.data.length > 0) {
                    for (let j = 0; j < this.data.length; j++) {
                        if (this.selectedStudent[i] == this.data[j].StudentID) {
                            var course = {
                                'CourseTaken': '1',
                                'CourseTitle': this.courseTitle,
                                'CourseCode': this.courseCode,
                                'CourseId': this.id,
                                'FirstName': this.data[j].Firstname.charAt(0).toUpperCase() + this.data[j].Firstname.slice(1),
                                'LastName': this.data[j].Lastname.charAt(0).toUpperCase() + this.data[j].Lastname.slice(1),
                                'Email': this.data[j].Email,
                                'StudentID': this.data[j].StudentID
                            }

                            console.log(course)

                            // console.log(course);
                            // this.service.put(this._serviceUrl + "/StudentAttendance", course)
                            //     .subscribe(res => {
                            //         val = JSON.parse(res);
                            //         if (val['code'] != 1) {
                            //             swal('Information!', val['message'], 'error');
                            //         }
                            //     }, error => {
                            //         this.service.errorserver();
                            //     });
                        }
                    }
                }
            }
            // this.router.navigate(['/course/course-list']);
        } else {
            swal('Information!', 'Please choose student', 'error');
        }
    }
}
