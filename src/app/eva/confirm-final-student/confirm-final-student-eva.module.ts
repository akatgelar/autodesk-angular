import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmFinalStudentEVAComponent } from './confirm-final-student-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { DataFilterStudentPipe } from '././confirm-final-student-eva.component';

export const ConfirmFinalStudentEVARoutes: Routes = [
  {
    path: '',
    component: ConfirmFinalStudentEVAComponent,
    data: {
      breadcrumb: 'eva.manage_course.confirm_student.confirm_student',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ConfirmFinalStudentEVARoutes),
    SharedModule
  ],
  declarations: [ConfirmFinalStudentEVAComponent, DataFilterStudentPipe],
  providers: [AppService]
})
export class ConfirmFinalStudentEVAModule { }
