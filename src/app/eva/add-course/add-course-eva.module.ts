import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCourseEVAComponent } from './add-course-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const AddCourseEVARoutes: Routes = [
    {
      path: '',
      component: AddCourseEVAComponent,
      data: {
        breadcrumb: 'Course Management Tool / Add Course',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddCourseEVARoutes),
        SharedModule
    ],
    declarations: [AddCourseEVAComponent]
})
export class AddCourseEVAModule { }