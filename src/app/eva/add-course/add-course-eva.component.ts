import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";

@Component({
    selector: 'app-add-course-eva',
    templateUrl: './add-course-eva.component.html',
    styleUrls: [
        './add-course-eva.component.css',
        '../../../../node_modules/c3/c3.min.css', 
        ], 
    encapsulation: ViewEncapsulation.None
})

export class AddCourseEVAComponent implements OnInit {
    
    addcourseform: FormGroup;
        
    constructor(public http: Http) { 

        //validation
        let partnertype = new FormControl('', Validators.required);
        let coursetitle = new FormControl('', Validators.required);
        let siteid = new FormControl('', Validators.required);
        let instructorid = new FormControl('', Validators.required);

        this.addcourseform = new FormGroup({
            partnertype:partnertype,
            coursetitle:coursetitle,
            siteid:siteid,
            instructorid:instructorid
        });

    }

    ngOnInit() {

    }

    //submit form
    submitted: boolean;
    onSubmit() {
        this.submitted = true;
    }

}