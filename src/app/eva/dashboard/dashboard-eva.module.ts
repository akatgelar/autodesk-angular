import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardEVAComponent } from './dashboard-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const DashboardEVARoutes: Routes = [
  {
    path: '',
    component: DashboardEVAComponent,
    data: {
      breadcrumb: 'Dashboard',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardEVARoutes),
    SharedModule
  ],
  declarations: [DashboardEVAComponent]
})
export class DashboardEVAModule { }
