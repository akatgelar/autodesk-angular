import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-dashboard-eva',
  templateUrl: './dashboard-eva.component.html',
  styleUrls: [
    './dashboard-eva.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class DashboardEVAComponent implements OnInit {

  content = ` Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at,`;
  timeline = [
    { caption: '16 Jan', date: new Date('2014, 1, 16'), selected: true, title: 'Horizontal Timeline', content: this.content },
    { caption: '28 Feb', date: new Date('2014, 2, 28'), title: 'Event title here', content: this.content },
    { caption: '20 Mar', date: new Date('2014, 3, 20'), title: 'Event title here', content: this.content },
    { caption: '20 May', date: new Date('2014, 5, 20'), title: 'Event title here', content: this.content },
    { caption: '09 Jul', date: new Date('2014, 7, 9'), title: 'Event title here', content: this.content },
    { caption: '30 Aug', date: new Date('2014, 8, 30'), title: 'Event title here', content: this.content },
    { caption: '15 Sep', date: new Date('2014, 9, 15'), title: 'Event title here', content: this.content },
    { caption: '01 Nov', date: new Date('2014, 11, 1'), title: 'Event title here', content: this.content },
    { caption: '10 Dec', date: new Date('2014, 12, 10'), title: 'Event title here', content: this.content },
    { caption: '29 Jan', date: new Date('2015, 1, 19'), title: 'Event title here', content: this.content },
    { caption: '3 Mar', date: new Date('2015,  3,  3'), title: 'Event title here', content: this.content },
  ];

  donutChartData =  {
    chartType: 'PieChart',
    dataTable: [
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2],
      ['Watch TV', 2],
      ['Sleep', 7]
    ],
    options: {
      height: 320,
      title: 'My Daily Activities',
      pieHole: 0.4,
      colors: ['#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f']
    },
  };
 
  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      const chart = c3.generate({
        bindto: '#chart4',
        data: {
          columns: [
            ['data1', 30, 20, 50, 40, 60, 50],
            ['data2', 200, 130, 90, 240, 130, 220],
            ['data3', 300, 200, 160, 400, 250, 250],
            ['data4', 200, 130, 90, 240, 130, 220],
            ['data5', 130, 120, 150, 140, 160, 150],
            ['data6', 90, 70, 20, 50, 60, 120],
          ],
          type: 'bar',
          colors: {
            data1: '#00C292',
            data2: '#4C5667',
            data3: '#03A9F3',
            data4: '#AB8CE4',
            data5: '#a3aebd',
            data6: '#FEC107'
          },
          types: {
            data3: 'spline',
            data4: 'line',
            data6: 'area',
          },
          groups: [
            ['data1', 'data2']
          ]
        }
      });
  
    }, 1);
  }

}
