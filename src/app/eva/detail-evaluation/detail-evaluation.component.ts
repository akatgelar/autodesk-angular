import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import * as Survey from "survey-angular";
import { Injectable } from '@angular/core';
import { SessionService } from '../../shared/service/session.service';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { resource } from 'selenium-webdriver/http';

var surveyJSON = {};
var datadetail: any;

@Component({
    selector: 'app-detail-evaluation',
    templateUrl: './detail-evaluation.component.html',
    styleUrls: [
        './detail-evaluation.component.css',
        '../../../../node_modules/c3/c3.min.css',
        '../../../../node_modules/survey-angular/survey.css'
    ],
    encapsulation: ViewEncapsulation.None
})

export class DetailEvaluationComponent implements OnInit {
    private id: string;
    private evalsId: string;
    private evalsNumber: number;
    private useraccessdata;
    private _serviceUrl = "api/EvaluationQuestion";

    constructor(private route: ActivatedRoute, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    }

    ngOnInit() {
        this.id = this.route.snapshot.params['courseId'];
        this.evalsId = this.route.snapshot.params['evalsId'];
        this.evalsNumber = this.route.snapshot.params['evalsNumber'];

        let data: any;
        this.service.httpClientGet(this._serviceUrl + "/GetSurvey/" + this.evalsId + "/" + this.id, data)
            .subscribe(res => {
                // data = res;
                // data = data.replace(/\t|\n|\r|\f|\\|\/|'/g, " ");
                // data = JSON.parse(data);
                data = res;
                // console.log(data);
                surveyJSON = JSON.stringify(data.EvaluationQuestionTemplate);
                Survey.Survey.cssType = "bootstrap";
                var survey = new Survey.Model(surveyJSON);
                survey.data = data.EvaluationAnswerJson;
                //read only
                survey.mode = "display";
                // survey.onComplete.add(this.sendDataToServer);
                Survey.SurveyNG.render("surveyElement", { model: survey });
            });
    }

}
