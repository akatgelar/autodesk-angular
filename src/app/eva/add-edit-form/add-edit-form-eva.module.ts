import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditFormEVAComponent } from './add-edit-form-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import { SurveyComponent } from './survey.component';
import { SurveyEditorComponent } from './survey.editor.component';

export const AddEditFormEVARoutes: Routes = [
  {
    path: '',
    component: AddEditFormEVAComponent,
    data: {
      breadcrumb: 'Add Edit Form',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddEditFormEVARoutes),
    SharedModule
  ],
  declarations: [
    AddEditFormEVAComponent,
    SurveyComponent,
    SurveyEditorComponent
  ]
})
export class AddEditFormEVAModule { }
