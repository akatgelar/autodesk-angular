import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-add-edit-form-eva',
  templateUrl: './add-edit-form-eva.component.html',
  styleUrls: [
    './add-edit-form-eva.component.css',
    '../../../../node_modules/c3/c3.min.css'
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class AddEditFormEVAComponent implements OnInit {

  public json:any;

  constructor() { }

  ngOnInit() {
    this.json={};
  }

  onSurveySaved(survey) {
  }

}
