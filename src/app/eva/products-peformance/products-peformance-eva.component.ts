import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-products-peformance-eva',
  templateUrl: './products-peformance-eva.component.html',
  styleUrls: [
    './products-peformance-eva.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ProductsPeformanceEVAComponent implements OnInit {

  // Geo
  dropdownListGeo = [];
  selectedItemsGeo = [];
  dropdownSettingsGeo = {};

  // Region
  dropdownListRegion = [];
  selectedItemsRegion = [];
  dropdownSettingsRegion = {};

  // SubRegion
  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];
  dropdownSettingsSubRegion = {};

  // Territories
  dropdownListTerritories = [];
  selectedItemsTerritories = [];
  dropdownSettingsTerritories = {};

  // Market Type
  dropdownListMarketType = [];
  selectedItemsMarketType = [];
  dropdownSettingsMarketType = {};

  // Country
  dropdownListCountry = [];
  selectedItemsCountry = [];
  dropdownSettingsCountry = {};

  // SubCountry
  dropdownListSubCountry = [];
  selectedItemsSubCountry = [];
  dropdownSettingsSubCountry = {};

  // Partner
  dropdownListPartner = [];
  selectedItemsPartner = [];
  dropdownSettingsPartner = {};

  // Certificate
  dropdownListCertificate = [];
  selectedItemsCertificate = [];
  dropdownSettingsCertificate = {};

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
   
 
  constructor(public http: Http) { }

  ngOnInit() {
    this.http.get(`assets/data/activities.json`)
      .subscribe((data)=> {
        this.data = data.json();
      }
    );

    // Geo
    this.dropdownListGeo = [
      {"id":1,"itemName":"AMER"},
      {"id":2,"itemName":"APAC (excl. GCR)"},
      {"id":3,"itemName":"EMEA"},
      {"id":4,"itemName":"GCR"}
    ];
    this.selectedItemsGeo = [];
    this.dropdownSettingsGeo = { 
        singleSelection: false, 
        text:"All Geos",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // Region
    this.dropdownListRegion = [
      {"id":21,"itemName":"ANZ"},
      {"id":22,"itemName":"ASEAN"},
      {"id":27,"itemName":"Canada"},
      {"id":30,"itemName":"Central Europe"}
    ];
    this.selectedItemsRegion = [];
    this.dropdownSettingsRegion = {
      singleSelection: false, 
      text:"All Regions",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    };

    // Sub Region
    this.dropdownListSubRegion = [
      {"id":18,"itemName":"Africa"},
      {"id":2,"itemName":"ASEAN"},
      {"id":1,"itemName":"Australia / New Zealand"},
      {"id":21,"itemName":"Benelux"}
    ];
    this.selectedItemsSubRegion = [];
    this.dropdownSettingsSubRegion = {
      singleSelection: false, 
      text:"All SubRegions",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    };

    // Territories
    this.dropdownListTerritories = [
      {"id":3,"itemName":"ASEAN"},
      {"id":10,"itemName":"CIS"},
      {"id":1,"itemName":"Europe"},
      {"id":4,"itemName":"GCR"}
    ];
    this.selectedItemsTerritories = [];
    this.dropdownSettingsTerritories = {
      singleSelection: false, 
      text:"All Territories",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    };

    // Country
    this.dropdownListCountry = [
      {"id":1,"itemName":"Afganistan"},
      {"id":2,"itemName":"Albania"},
      {"id":3,"itemName":"Algeria"},
      {"id":4,"itemName":"Andorra"}
    ];
    this.selectedItemsCountry = [];
    this.dropdownSettingsCountry = { 
        singleSelection: false, 
        text:"Select Countries",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // SubCountry
    this.dropdownListSubCountry = [
      {"id":2912,"itemName":"‘Ajman"},
      {"id":1892,"itemName":"Abia State"},
      {"id":857,"itemName":"Abkhazia"},
      {"id":222,"itemName":"Absheron Rayon"}
    ];
    this.selectedItemsSubCountry = [];
    this.dropdownSettingsSubCountry = { 
        singleSelection: false, 
        text:"Select SubCountries",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // Market Type
    this.dropdownListMarketType = [
      {"id":2,"itemName":"Emerging Markets"},
      {"id":3,"itemName":"Mature Markets"},
      {"id":1,"itemName":"N/A"}
    ];
    this.selectedItemsMarketType = [];
    this.dropdownSettingsMarketType = { 
        singleSelection: false, 
        text:"Select Market Type",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // Partner
    this.dropdownListPartner = [
      {"id":1,"itemName":"ATC"},
      {"id":2,"itemName":"AAP"}
    ];
    this.selectedItemsPartner = [];
    this.dropdownSettingsPartner = { 
        singleSelection: false, 
        text:"Please Select",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // Certificate
    this.dropdownListCertificate = [
      {"id":1,"itemName":"Course"},
      {"id":2,"itemName":"Project"}
    ];
    this.selectedItemsCertificate = [];
    this.dropdownSettingsCertificate = { 
      singleSelection: false, 
      text:"Please Select",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    };
  }

}
