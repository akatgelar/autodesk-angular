import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsPeformanceEVAComponent } from './products-peformance-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const ProductsPeformanceEVARoutes: Routes = [
  {
    path: '',
    component: ProductsPeformanceEVAComponent,
    data: {
      breadcrumb: 'Products Performance',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsPeformanceEVARoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ProductsPeformanceEVAComponent]
})
export class ProductsPeformanceEVAModule { }
