import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {Input, Output, EventEmitter } from "@angular/core";
import * as c3 from 'c3';
declare const $: any;
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

import * as SurveyEditor from "surveyjs-editor";

@Component({
  selector: 'app-survey-template-copy-detail-eva',
  templateUrl: './survey-template-copy-detail-eva.component.html',
  styleUrls: [
    './survey-template-copy-detail-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    '../../../../node_modules/surveyjs-editor/surveyeditor.css',
    '../../../../node_modules/survey-angular/survey.css',
    //'../../../../src/assets/css/bootstrap.css',
    ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyTemplateCopyDetailEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public json:any;
  public EvaluationQuestionTemplate: any;
  public EvaluationQuestionCode: string = '';
  public CourseId: string = '';
  public CountryCode: string = '';
  public CreatedDate: any;
  public CreatedBy: string = ''; //akan diupdate ambil dari session
  public Year: string = '';

  constructor(private service:AppService, private route:ActivatedRoute, private router: Router, private formatdate:AppFormatDate){ }

  id:string;
  editor: SurveyEditor.SurveyEditor;

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];

    var data = '';
    this.json={};

    //decalarasi survey js
    let editorOptions = {
    // show the embeded survey tab. It is hidden by default
    showEmbededSurveyTab : false,
    // hide the test survey tab. It is shown by default
    showTestSurveyTab : true,
    // hide the JSON text editor tab. It is shown by default
    showJSONEditorTab : true,
    // show the "Options" button menu. It is hidden by default
    showOptions: false  };

    this.editor = new SurveyEditor.SurveyEditor('surveyEditorContainer', editorOptions);
    if(this.id != null){
        this.editor.saveSurveyFunc = this.updateSurvey;
    }
    else{
        this.editor.saveSurveyFunc = this.saveSurvey;
    }
    //end of decalarasi survey js

    //get data according to id
    this.service.httpClientGet(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        if(result=="Not found"){
            this.service.notfound();
            this.data = null;
        }
        else{
            this.data = result;
            this.json=JSON.stringify(this.data.EvaluationQuestionTemplate);
            this.editor.text=this.json;
            this.CourseId = this.data.CourseId;
            this.EvaluationQuestionCode  = this.data.EvaluationQuestionCode;
            this.CountryCode = this.data.CountryCode;
            this.Year = this.data.Year;
            this.CreatedDate =this.formatdate.dateJStoYMD(new Date());
            this.CreatedBy  = this.data.CreatedBy;
            //  console.log("id CreatedDate:"+this.CreatedDate);
        }
      },
      error => {
          this.messageError = <any>error
          this.service.errorserver();
      });
  }

  updateSurvey = () => {
    //update data
    let data = JSON.stringify({
        EvaluationQuestionId:this.id,
        EvaluationQuestionCode: this.EvaluationQuestionCode,
        CourseId:  this.CourseId ,
        CountryCode: this.CountryCode,
        Year: this.Year,
        EvaluationQuestionTemplate: this.editor.text,
        EvaluationQuestionJson: this.editor.text,
        CreatedBy:this.CreatedBy,
        CreatedDate: this.CreatedDate
      })
    this.service.httpCLientPut(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        var resource = result;
        if(resource['code'] == '1') {
          this.service.openSuccessSwal('Insert Data Success');
        }
        else{
            swal(
              'Information!',
              "Update Data Failed",
              'error'
            );
        }
        this.router.navigate(['/evaluation/survey-template-list/']);
      },
      error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
    }

    saveSurvey = () => {
      console.log("save data =>"+JSON.stringify(this.editor.text));
    }

    //view detail
    viewdetail(id) {
      var data = '';
      this.service.httpClientGet(this._serviceUrl+'/'+id, data)
      .subscribe(result => {
          this.datadetail = result;
      },
      error => {
          this.messageError = <any>error
          this.service.errorserver();
      });
    }

    //delete confirm
    openConfirmsSwal(id) {
      var index = this.data.findIndex(x => x.EvaluationQuestionId == id);
      //this.service.httpClientDelete(this._serviceUrl, this.data, id, index)
      this.service.httpClientDelete(this._serviceUrl+'/'+id, this.data)
    }

}
