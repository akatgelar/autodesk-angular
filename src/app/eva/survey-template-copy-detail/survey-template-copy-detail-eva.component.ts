import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Input, Output, EventEmitter } from "@angular/core";
import * as c3 from 'c3';
declare const $: any;
import $ from 'jquery/dist/jquery';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, Response } from "@angular/http";

import * as SurveyEditor from "./surveyjs-editor-copy";
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-survey-template-copy-detail-eva',
    templateUrl: './survey-template-copy-detail-eva.component.html',
    styleUrls: [
        './survey-template-copy-detail-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
        '../../../../node_modules/surveyjs-editor/surveyeditor.css',
        '../../../../node_modules/survey-angular/survey.css',
        // '../../../../src/assets/css/bootstrap.css',
    ],
    encapsulation: ViewEncapsulation.None
})



export class SurveyTemplateCopyDetailEVAComponent implements OnInit {

    messageResult: string = '';
    messageError: string = '';
    copytemplatedata: FormGroup;
    dropdownPartner = [];
    year;
    private _serviceUrl = 'api/EvaluationQuestion';
    public data: any;
    public datadetail: any;
    public json: any;
    public EvaluationQuestionTemplate: any;
    public CountryCode: string = '';
    public CreatedDate: any;
    public CreatedBy: string = ''; //akan diupdate ambil dari session
    public CertificateTypeId: string = '';
    private dropdownCertificate;
    private partnerSelected;
    public CertificateType = false;
    private useraccessdata;
    public shadow1 = true;
    public shadow2 = false;
    public language;
    public selectedLang: string = "";


    
    constructor(private datePipe: DatePipe, private session: SessionService, private http: HttpClient, private service: AppService, private route: ActivatedRoute, private router: Router, private formatdate: AppFormatDate) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        //validation
        let EvaluationQuestionCode = new FormControl({ value: '', disabled: true });
        let CourseId = new FormControl({ value: '', disabled: true });
        let Year = new FormControl({ value: '', disabled: true });
        let CertificateType = new FormControl({ value: '', disabled: true });
        let PartnerType = new FormControl({ value: '', disabled: true });

        this.copytemplatedata = new FormGroup({
            EvaluationQuestionCode: EvaluationQuestionCode,
            CourseId: CourseId,
            Year: Year,
            CertificateType: CertificateType,
            PartnerType: PartnerType
        });
    }

    id: string;
    editor: SurveyEditor.SurveyEditor;


    getCertificateType() {
        var certificate: any;
        var parent = "AAPCertificateType";
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "'}", certificate)
            .subscribe(result => {
                this.dropdownCertificate =result;
            }, error => {
                this.service.errorserver();
            });
    }

    getPartnerType() {
        var partner = ['1', '58'];
        var partnerTemp: any;
        this.dropdownPartner = [];
        for (let i = 0; i < partner.length; i++) {
            this.service.httpClientGet("api/Roles/" + partner[i], partnerTemp)
                .subscribe(result => {
                    partnerTemp = result;
                    this.dropdownPartner.push(partnerTemp);
                }, error => {
                    this.service.errorserver();
                });
        }
    }

    getFY() {
        var parent = "FYIndicator";
        var data = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", data)
            .subscribe(res => {
                this.year = res;
            }, error => {
                this.service.errorserver();
            });
    }

    selectedPartner(newvalue) {
        // this.partnerSelected = newvalue;
        if (newvalue == 58) {
            var certificate: any;
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", certificate)
                .subscribe(result => {
                    this.dropdownCertificate = result;
                }, error => {
                    this.service.errorserver();
                });
            this.CertificateType = true;
            this.shadow1 = false;
            this.shadow2 = true;
        } else {
            this.CertificateType = false;
            this.shadow1 = true;
            this.shadow2 = false;
        }
    }

    getLanguage(code) {
        function compare(a, b) {
            // Use toUpperCase() to ignore character casing
            const valueA = a.KeyValue.toUpperCase();
            const valueB = b.KeyValue.toUpperCase();

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var lang: any;
        this.service.httpClientGet("api/Dictionaries/LanguageCopyTemplate/"+code, lang)
            .subscribe(res => {
                lang = res;
                lang.length > 0 ? this.language = lang.sort(compare) : this.language = "";
            }, error => {
                this.language = "";
            });
    }

    ngOnInit() {
        this.getFY();
        this.getCertificateType();
        this.getPartnerType();

        this.id = this.route.snapshot.params['id'];

        var data = '';
        this.json = {};

        //decalarasi survey js
        let editorOptions = {
            // show the embeded survey tab. It is hidden by default
            showEmbededSurveyTab: false,
            // hide the test survey tab. It is shown by default
            showTestSurveyTab: true,
            // hide the JSON text editor tab. It is shown by default
            showJSONEditorTab: false,

            questionTypes: [""],

            showPropertyGrid: false,
            showOptions: false,
        };

        this.editor = new SurveyEditor.SurveyEditor('surveyEditorContainer', editorOptions);
        // onPropertyValidationCustomError: false,
        $('.svd_save_btn').html('<span data-bind="text: title">Copy Survey</span>');

        // if(this.id != null){
        this.editor.saveSurveyFunc = this.CopySurvey;
        // }
        // else{
        //     this.editor.saveSurveyFunc = this.saveSurvey;
        // }
        //end of decalarasi survey js

        //get data according to id
        this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.data = null;
                }
                else {
                    this.data = result;
                    this.getLanguage(this.data.EvaluationQuestionCode);
                    this.json = JSON.stringify(this.data.EvaluationQuestionTemplate);
                    this.editor.text = this.json;

                    //Set template data value
                    this.copytemplatedata.patchValue({ PartnerType: this.data.PartnerType });
                    if (this.data.PartnerType == 58) {
                        this.selectedPartner(this.data.PartnerType);
                        this.copytemplatedata.patchValue({ CertificateType: this.data.CertificateTypeId });
                    }
                    this.copytemplatedata.patchValue({ EvaluationQuestionCode: this.data.EvaluationQuestionCode });
                    this.copytemplatedata.patchValue({ CourseId: this.data.CourseId });
                    this.copytemplatedata.patchValue({ Year: this.data.Year });

                    // this.CourseId = this.data.CourseId;
                    // this.EvaluationQuestionCode = this.data.EvaluationQuestionCode;
                    // this.CountryCode = this.data.CountryCode;
                    // this.PartnerType = this.data.PartnerType;
                    // this.CertificateTypeId = this.data.CertificateTypeId;
                    // this.Year = this.data.Year;
                    // this.CreatedDate = this.formatdate.dateJStoYMD(new Date());
                    // this.CreatedBy = this.data.CreatedBy;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });
    }

    //submit form
    submitted: boolean;
    public loading = false;
    CopySurvey = () => {

        if (this.selectedLang == "") {
            swal("Information!", "Please select survey language...", "warning");
        } else {
            var dataCopy = JSON.stringify({
                'EvaluationQuestionCode': this.copytemplatedata.value.EvaluationQuestionCode,
                'CertificateTypeId': this.copytemplatedata.value.CertificateType,
                'PartnerType': this.copytemplatedata.value.PartnerType,
                'CourseId': this.copytemplatedata.value.CourseId,
                'CountryCode': "SG",
                'Year': this.copytemplatedata.value.Year,
                'EvaluationQuestionTemplate': this.editor.text,
                'EvaluationQuestionJson': this.editor.text,
                'LanguageID': this.selectedLang,
                'CreatedBy': this.useraccessdata.ContactName,
                'CreatedDate': new Date(),
                'cuid' : this.useraccessdata.ContactName,
                'UserId': this.useraccessdata.UserId
            });

            // console.log(JSON.parse(dataCopy));

            //post action
            this.loading = true;
            this.service.httpClientPost(this._serviceUrl, dataCopy)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

            setTimeout(() => {
                this.router.navigate(["/evaluation/survey-template-list"]);
                setTimeout(() => {
                    location.reload();
                    this.loading = false;
                }, 500)
            }, 3000)
        }
    }
}
