import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyTemplateCopyDetailEVAComponent } from './survey-template-copy-detail-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
//import { SurveyEditorComponent } from './survey.editor.component';
import { LoadingModule } from 'ngx-loading';

export const SurveyTemplateCopyDetailEVARoutes: Routes = [
  {
    path: '',
    component: SurveyTemplateCopyDetailEVAComponent,
    data: {
      breadcrumb: 'Survey Template Copy',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyTemplateCopyDetailEVARoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [
    SurveyTemplateCopyDetailEVAComponent
  ],
  providers:[AppService,AppFormatDate]
})
export class SurveyTemplateCopyDetailEVAModule { }
