import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {AppService} from "../../shared/service/app.service";

import * as Survey from "survey-angular";

var surveyJSON ={};
var datadetail: any;

function sendDataToServer(survey) {}

@Component({
  selector: 'survey-template-detail-eva',
  templateUrl: './survey-template-detail-eva.component.html',
  styleUrls: [
    './survey-template-detail-eva.component.css',
    '../../../../node_modules/c3/c3.min.css',
    '../../../../node_modules/survey-angular/survey.css',
    //'../../../../src/assets/css/bootstrap.css',
    ],
  encapsulation: ViewEncapsulation.None
})

export class SurveyTemplateDetailEVAComponent implements OnInit {

  private _serviceUrl = 'api/EvaluationQuestion';
  public data: any;
  public datadetail: any;
  public json:any;
  id:string;

  constructor(private service:AppService, private route:ActivatedRoute, private router: Router){ }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.json={};

    //get data according to id
    this.service.httpClientGet(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        if(result=="Not found"){
            this.service.notfound();
            this.data = '';
        }
        else{
          this.data = result;
          surveyJSON = JSON.stringify(this.data.EvaluationQuestionTemplate);
          Survey.Survey.cssType = "bootstrap";
          var survey = new Survey.Model(surveyJSON);
          survey.onComplete.add(sendDataToServer);
          Survey.SurveyNG.render("surveyElement", { model: survey });
        }
      },
      error => {
          this.service.errorserver();
          this.datadetail = '';
      });

    //get data according to id
    this.service.httpClientGet('api/EvaluationQuestion/'+this.id, data)
      .subscribe(result => {
        if(result=="Not found"){
          this.datadetail = '';
        }
        else{
          this.datadetail = result;
        }
      },
      error => {
          this.service.errorserver();
          this.datadetail = '';
      });
  }

}
