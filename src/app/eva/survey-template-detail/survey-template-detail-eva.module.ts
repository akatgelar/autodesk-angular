import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyTemplateDetailEVAComponent } from './survey-template-detail-eva.component';
import { SurveyTemplateListEVAComponent } from '../survey-template-list/survey-template-list-eva.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppService} from "../../shared/service/app.service";

export const SurveyTemplateDetailEVARoutes: Routes = [
  {
    path: '',
    component: SurveyTemplateDetailEVAComponent,
    data: 
      {
        breadcrumb: 'Detail Template Survey',
        icon: 'icofont-home bg-c-blue',
        status: false
      },
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SurveyTemplateDetailEVARoutes),
    SharedModule
  ],
  declarations: [
    SurveyTemplateDetailEVAComponent
  ],
  providers:[AppService]
})
export class SurveyTemplateDetailEVAModule { }


