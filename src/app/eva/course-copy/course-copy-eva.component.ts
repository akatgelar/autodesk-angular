import { Component, OnInit, ViewEncapsulation, ViewChild, Injectable, ElementRef } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { DatePipe } from '@angular/common';
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
// import * as myGlobals from "../../../../node_modules/globals";
import $ from 'jquery/dist/jquery';
import { SessionService } from '../../shared/service/session.service';

@Component({
    selector: 'app-course-copy-eva',
    templateUrl: './course-copy-eva.component.html',
    styleUrls: [
        './course-copy-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CourseCopyEVAComponent implements OnInit {
    @ViewChild('fileInput') fileInput: ElementRef;
    listSites = [];
    messageError: string = '';
    id: string;
    dropdownPartner = [];
    datadetail = [];
    selectedStudent = [];
    private data: any;
    private versions1;
    private versions2;
    private partnerSelected;
    private certificateSelected;
    pickedStudent = [];
    private errorSites: string = '';
    private errorInstructor: string = '';
    dateStart: string;
    dateEnd: string;
    private products;
    private versions;
    private typeOf;
    private trainingHours;
    private survey;
    private evaTemplate;
    private cftTemplate;
    private _serviceUrl = 'api/Courses';
    private dropdownCertificate;
    private ContactName: string;
    private evaTemplateId;
    private certificateTemplateId;
    public courseDiv = false;
    public courseEquipment = true;
    public courseFacility = true;
    public CertificateType = false;
    public CertificateTypeCol = true;
    public Course = false;
    public Project = false;
    public event = false;
    public ATC1 = true;
    public ATC2 = true;
    public ATC3 = true;
    public ATC4 = true;
    public ATC6 = true;
    public institution = false;
    protected siteId: any;
    protected contactId: string;
    protected sites = [];
    protected instructor = [];
    // protected dataService1: CompleterData;
    protected dataService2: CompleterData;
    // protected dataService3: CompleterData;
    protected dataService1: RemoteData;
    protected dataService3: RemoteData;
    public rowsOnPage: number = 5;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public rowsOnPage1: number = 5;
    public filterQuery1: string = "";
    public sortBy1: string = "StudentID";
    public sortOrde1: string = "asc";
    public studentFound = false;
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownSettings = {};
    dataStudent = [];
    protected siswa: string;
    protected siswaLookup = [];
    editCourse: FormGroup;
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    public projectInstitution = false;
    public projectLogoUpload = false;
    public parentLabel = false;
    public loading = false;
    studentErrorTag = false;
    studentSuccessTag = false;
    errorTagMessage: string = '';
    successTagMessage: string = '';
    public studentPicked = false;
    private evalsNumber: number = 0;
    public showEvals = false;
    dataEvals;
    public useraccesdata: any;
    private contactID;
    private coursePartner;
    private siteUrl;
    private lastCourseId;
    public notFound = false;
    teachingLevel: string = "";
    private certdropdown;
    private selcertdropdown;
    public isAvailableLogo: boolean = false;
    imgLogoName: string = "";
    imgSrc: string = "";
    private trainingHoursOther:any;

    constructor(private router: Router, private session: SessionService, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute,
        private completerService: CompleterService, private parserFormatter: NgbDateParserFormatter, private datePipe: DatePipe, private filterGeo: AppFilterGeo) {

        // this.dataService1 = completerService.local(this.sites, 'SiteId', 'SiteId').descriptionField("SiteName");
        // this.dataService2 = completerService.local(this.instructor, 'ContactId', 'ContactId').descriptionField("ContactName");
        // this.dataService3 = completerService.local(this.siswaLookup, 'StudentID', 'StudentID').descriptionField("Firstname");

        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        //Untuk pencarian sites berdasarkan site id
        let status = ['"A"', '"H"', '"I"'];
        if (this.useraccesdata.UserLevelId == "TRAINER") {
            var orgArr = this.useraccesdata.OrgId.toString().split(',');
            var orgTmp = [];
            for (var i = 0; i < orgArr.length; i++) {
                orgTmp.push('"' + orgArr[i] + '"');
            }
            this.siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','Organization':'" + orgTmp.join(",") + "'}";
        }
        else {
            this.siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "'}";
        }

        this.dataService1 = completerService.remote(
            null,
            "Sites",
            "Sites");
        this.dataService1.urlFormater(term => {
            return this.siteUrl + "/" + term;
        });
        this.dataService1.dataField("results");

        //Untuk pencarian siswa berdasarkan email dan student id
        this.dataService3 = completerService.remote(
            null,
            "Student",
            "Student");
        this.dataService3.urlFormater(term => {
            return `api/Student/FindStudent/` + term;
        });
        this.dataService3.dataField("results");

        let CourseId = new FormControl('');
        let PartnerType = new FormControl({ value: '', disabled: true });
        let CertificateType = new FormControl('');
        let CourseTitle = new FormControl('', [Validators.required, Validators.maxLength(40)]);
        let SiteId = new FormControl('', Validators.required);
        let ContactId = new FormControl('', Validators.required);
        let Institution = new FormControl('', Validators.required);
        let StartDate = new FormControl('', Validators.required);
        let EndDate = new FormControl('', Validators.required);
        let Survey = new FormControl({ value: '', disabled: true }, Validators.required);
        let Update = new FormControl('');
        let Essentials = new FormControl('');
        let Intermediate = new FormControl('');
        let Advanced = new FormControl('');
        let Customized = new FormControl('');
        let Other = new FormControl('');
        let Komen = new FormControl({ value: '', disabled: true });
        let Status = new FormControl('');
        let Product1 = new FormControl('', Validators.required);
        let Version1 = new FormControl('', Validators.required);
        let Product2 = new FormControl('');
        let Version2 = new FormControl('');
        let TrainingHours = new FormControl('', Validators.required);
        let InstructorLed = new FormControl('');
        let Online = new FormControl('');
        let TrainingType = new FormControl('');
        let Autodesk = new FormControl('');
        let AAP = new FormControl('');
        let ATC = new FormControl('');
        let Independent = new FormControl('');
        let IndependentOnline = new FormControl('');
        let ATCOnline = new FormControl('');
        let Other5 = new FormControl('');
        let Komen5 = new FormControl({ value: '', disabled: true });
        let CourseFacility = new FormControl('', Validators.required);
        let CourseEquipment = new FormControl('', Validators.required);
        // let EvaTemplate = new FormControl('', Validators.required);
        // let CertTemplate = new FormControl('', Validators.required);
        let StudentID = new FormControl('');
        let ProjectType = new FormControl('', Validators.required);
        let LocationName = new FormControl('', Validators.required);
        let EventType = new FormControl('', Validators.required);
        let namaInstruktur = new FormControl({ value: '', disabled: true });
        let LevelTeaching = new FormControl('', Validators.required);
        let InstitutionLogo = new FormControl(null); //change to save logo into database
        let HoursTrainingOther=new FormControl(0);

        this.editCourse = new FormGroup({
            CourseId: CourseId,
            PartnerType: PartnerType,
            CertificateType: CertificateType,
            CourseTitle: CourseTitle,
            SiteId: SiteId,
            ContactId: ContactId,
            Institution: Institution,
            StartDate: StartDate,
            EndDate: EndDate,
            Survey: Survey,
            Update: Update,
            Essentials: Essentials,
            Intermediate: Intermediate,
            Advanced: Advanced,
            Customized: Customized,
            Other: Other,
            Komen: Komen,
            Status: Status,
            Product1: Product1,
            Version1: Version1,
            Product2: Product2,
            Version2: Version2,
            TrainingHours: TrainingHours,
            InstructorLed: InstructorLed,
            Online: Online,
            TrainingType: TrainingType,
            Autodesk: Autodesk,
            AAP: AAP,
            ATC: ATC,
            Independent: Independent,
            IndependentOnline: IndependentOnline,
            ATCOnline: ATCOnline,
            Other5: Other5,
            Komen5: Komen5,
            CourseFacility: CourseFacility,
            CourseEquipment: CourseEquipment,
            // EvaTemplate: EvaTemplate,
            // CertTemplate: CertTemplate,
            StudentID: StudentID,
            ProjectType: ProjectType,
            LocationName: LocationName,
            EventType: EventType,
            namaInstruktur: namaInstruktur,
            LevelTeaching: LevelTeaching,
            InstitutionLogo: InstitutionLogo,
            HoursTrainingOther : HoursTrainingOther
        });
    }

    ketikaSiswaDipilih(item) {
        this.addStudent(item['originalObject'].StudentID);
    }

    getDataSiswa() {
        var murid: any;
        this.service.httpClientGet("api/Student", murid)
            .subscribe(result => {
                murid = result;
                if (murid.length > 0) {
                    for (let i = 0; i < murid.length; i++) {
                        this.siswaLookup.push(murid[i]);
                    }
                }
            }, error => {
                this.service.errorserver();
            });

    }

    getTrainingHours() {
        function compare(a, b) {
            // Use toUpperCase() to ignore character casing
            const valueA = parseInt(a.Key);
            const valueB = parseInt(b.Key);

            let comparison = 0;
            if (valueA > valueB) {
                comparison = 1;
            } else if (valueA < valueB) {
                comparison = -1;
            }
            return comparison;
        }

        var hour: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'ATCHours','Status':'A'}", hour)
            .subscribe(res => {
                hour = res;
                this.trainingHours = hour.sort(compare);

            });
    }

    selectedPartner(newvalue) {
        this.partnerSelected = newvalue;
        if (this.partnerSelected == 58) {
            var certificate: any;
            var parent = "AAPCertificateType";
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'A'}", certificate)
                .subscribe(result => {
                    this.dropdownCertificate = result;
                }, error => {
                    this.service.errorserver();
                });
            this.courseDiv = true;
            this.courseEquipment = false;
            this.courseFacility = false;
            // $("#courseFacility").css("display", "none");
            // $(".courseEquipment").css("display", "none");
            this.CertificateType = true;
            this.CertificateTypeCol = false;
            // $(".institution").css("display", "block");
            this.institution = true;
        } else {
            this.coursePartner = 'ATC';
            this.CertificateType = false;
            this.CertificateTypeCol = true;
            this.courseDiv = false;
            this.courseEquipment = true;
            this.courseFacility = true;
            // $("#courseFacility").css("display", "block");
            // $(".courseEquipment").css("display", "block");
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC6 = true;
            // $(".institution").css("display", "none");
            this.institution = false;
        }
    }

    selectedCertificate(newvalue) {
        this.certificateSelected = newvalue;
        if (newvalue == 1) {
            this.coursePartner = 'AAP Course';
            this.getDictionary("TrainingType", "A");
            this.getSurveyIndicator();
            // this.getTrainingHours(hoursTraining);
            this.parentLabel = true;
            this.institution = true;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            // $(".parentLabel").css("display", "block");
            // $(".institution").css("display", "block");
            this.Course = true;
            this.Project = false;
            this.event = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = false;
            this.ATC6 = false;
        } else if (newvalue == 2) {
            this.coursePartner = 'AAP Project';
            this.getDictionary("ProjectType", "A");
            this.getSurveyIndicator();
            // this.getTrainingHours(hoursTraining);
            this.parentLabel = false;
            this.institution = false;
            this.projectInstitution = true;
            this.projectLogoUpload = true;
            // $(".parentLabel").css("display", "none");
            // $(".institution").css("display", "block");
            this.Project = true;
            this.event = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC6 = false;
            this.Course = false;
        } else if (newvalue == 3) {
            this.coursePartner = 'AAP Event';
            this.getDictionary("EventType", "A");
            this.getSurveyIndicator();
            this.parentLabel = false;
            this.institution = false;
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            // $(".parentLabel").css("display", "none");
            // $(".institution").css("display", "none");
            this.event = true;
            this.Project = false;
            this.Course = false;
            this.ATC1 = false;
            this.ATC2 = false;
            this.ATC3 = false;
            this.ATC4 = false;
            this.ATC6 = false;
        } else {
            this.coursePartner = 'AAP Other';
            this.getDictionary("EventType", "A");
            this.getSurveyIndicator();
            // this.getTrainingHours(hoursTraining);
            this.parentLabel = true;
            this.institution = false;
            // $(".parentLabel").css("display", "block");
            // $(".institution").css("display", "none");
            this.projectInstitution = false;
            this.projectLogoUpload = false;
            this.event = false;
            this.Project = false;
            this.Course = false;
            this.ATC1 = true;
            this.ATC2 = true;
            this.ATC3 = true;
            this.ATC4 = true;
            this.ATC6 = true;
        }
    }

    onFileChange(event) {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.editCourse.get('InstitutionLogo').setValue({
              filename: file.name,
              filetype: file.type,
              value: (<string>reader.result).split(',')[1]
            })
          };
        }
    }

    clearLogo(){
        this.isAvailableLogo = false;
        this.projectLogoUpload = true;
    }

    getPartnerType() {
        var partnerTemp: any;
        this.dropdownPartner = [];
        this.service.httpClientGet("api/Roles/CoursePartner", partnerTemp)
            .subscribe(result => {
                partnerTemp = result;
                this.dropdownPartner = partnerTemp;
                setTimeout(()=>{
                    this.getSites()
                },1000)
            });
    }

    onSelected(item) {
        if (item == null) {
            this.editCourse.controls["ContactId"].reset();
            this.editCourse.controls["namaInstruktur"].reset();
        }

        if (item != null) {
            let splitVal = (item.originalObject.Sites).split(" | ");
            var siteId = '';
            siteId = item.title;
            this.getInstructor(splitVal[0]);
            this.errorSites = "";
        }
    }

    onSelectedInstructor(item) {
        if (item == null) {
            this.editCourse.controls["namaInstruktur"].reset();
        }

        if (item != null) {
            let splitVal = (item.originalObject.Instructor).split(" | ");
            this.ContactName = splitVal[1];
            this.editCourse.patchValue({ namaInstruktur: splitVal[1] });
            this.contactID = splitVal[0];
            // for (let i = 0; i < this.instructor.length; i++) {
            //     if (this.instructor[i].Instructor === item.originalObject.Instructor) {
            //         this.contactID = this.instructor[i].ContactId;
            //     }
            // }
            this.errorInstructor = "";
        }
        // else {
        //     this.errorInstructor = "Instructor Not Found..";
        // }
    }

    getProduct() {
        var productTemp: any;
        if (this.useraccesdata.UserLevelId == "TRAINER") {
            this.service.httpClientGet('api/Qualifications/Product/' + this.useraccesdata.UserId, productTemp)
                .subscribe(result => {
                    if (result == "Not Found") {
                        this.products = '';
                    } else {
                        // productTemp = result;
                        // productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
                        this.products = result;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.products = '';
                    });
        } else {
            // this.service.get("api/Product/GetSingleProduct", productTemp)
            this.service.httpClientGet("api/Product", productTemp)
                .subscribe(result => {
                    // productTemp = result;
                    // productTemp = productTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
                    // this.products = JSON.parse(productTemp);
                    this.products =result;
                });
        }
    }

    findVersion1(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    version = result;
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        if (lastVersion != undefined) {
                            currentVersion.splice(currentVersion.length, 0, lastVersion);
                        }
                        this.versions1 = currentVersion;

                        /*Ini hanya untuk ngehandle produk version yang status nya 'X' didapat dari data kursus yang jadul*/
                        let valVersion: any;
                        let isVersion: boolean = false;
                        for (let x = 0; x < this.versions1.length; x++) {
                            if (this.data.ProductVersionsId == this.versions1[x].ProductVersionsId) {
                                isVersion = true;
                                valVersion = this.versions1[x].ProductVersionsId;
                            }
                        }

                        if (isVersion) {
                            this.editCourse.patchValue({ Version1: valVersion });
                        } else {
                            this.editCourse.patchValue({ Version1: "" });
                        }
                        /*End of line*/

                    } else {
                        this.versions1 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
        } else {
            this.versions1 = "";
        }
    }

    findVersion2(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    version = result;
                    if (version.length > 0) {
                        let currentVersion = [];
                        //Reposisi index previous seharusnya di pilihan paling bawah
                        for (let i = 0; i < version.length; i++) {
                            if (version[i].Version != 'Previous') {
                                currentVersion.push({ ProductVersionsId: version[i].ProductVersionsId, Version: version[i].Version })
                            }
                        }
                        let lastVersion = version.find(o => o.Version === 'Previous');
                        if (lastVersion != undefined) {
                            currentVersion.splice(currentVersion.length, 0, lastVersion);
                        }
                        this.versions2 = currentVersion;

                        /*Ini hanya untuk ngehandle produk version yang status nya 'X' didapat dari data kursus yang jadul*/
                        let valVersion2: any;
                        let isVersion2: boolean = false;
                        for (let x = 0; x < this.versions2.length; x++) {
                            if (this.data.ProductVersionsSecondaryId == this.versions2[x].ProductVersionsId) {
                                isVersion2 = true;
                                valVersion2 = this.versions1[x].ProductVersionsId;
                            }
                        }

                        if (isVersion2) {
                            this.editCourse.patchValue({ Version2: valVersion2 });
                        } else {
                            this.editCourse.patchValue({ Version2: "" });
                        }
                        /*End of line*/
                    } else {
                        this.versions2 = [{ ProductVersionsId: '0', Version: "No version found" }];
                    }
                });
        } else {
            this.versions2 = "";
        }
    }

    getDictionary(parent, status) {
        var dataTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", dataTemp)
            .subscribe(result => {
                this.typeOf = result;
            }, error => { this.service.errorserver(); });
    }

    getSurveyIndicator() {
        var dataTemp: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'FYIndicator','Status':'A'}", dataTemp)
            .subscribe(result => {
                this.survey = result;
            });
    }

    onChangeOtherDisable(value) {
        this.teachingLevel = value;
        this.editCourse.controls["Komen"].disable();
        this.editCourse.controls["Komen"].reset();
    }

    resetDate() {
        this.editCourse.reset({
            'StartDate': null,
            'EndDate': null
        });
        this.dateStart = null;
        this.dateEnd = null;
    }

    onSelectDateStart(date: NgbDateStruct) {
        if (date != null) {
            this.dateStart = this.parserFormatter.format(date);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }

    onSelectDateEnd(date: NgbDateStruct) {
        if (date != null) {
            this.dateEnd = this.parserFormatter.format(date);
            // this.checkDate(this.dateStart, this.dateEnd);
        }
    }

    getSite() {
        var currentStatus = 'A';
        var siteTemp: any;
        this.service.httpClientGet("api/MainSite/SiteCourse/{'Status':'" + currentStatus + "'}", siteTemp)
            .subscribe(result => {
                siteTemp = result;
                if (siteTemp.length > 0) {
                    var exist = false;
                    for (let i = 0; i < siteTemp.length; i++) {
                        if (this.sites.length == 0) {
                            this.sites.push(siteTemp[i]);
                        } else {
                            for (let j = 0; j < this.sites.length; j++) {
                                if (siteTemp[i].SiteId == this.sites[j].SiteId) {
                                    exist = true;
                                }
                            }
                            if (exist == false && siteTemp[i].SiteId != "") {
                                this.sites.push(siteTemp[i]);
                            }
                        }
                    }
                } else {
                    swal(
                        'Information!',
                        'Site Data Not Found',
                        'error'
                    );
                }

            }, error => { this.service.errorserver(); });
    }

    getInstructor(siteId) {
        let n_siteId
        if(siteId)
            n_siteId = siteId
        else
            n_siteId = this.siteId[0].id
        var instructor: any;
        this.instructor = [];
        this.service.httpClientGet("api/MainSite/Instructor/" + n_siteId, instructor)
            .subscribe(result => {
                instructor = result;
                if (instructor.length > 0) {
                    var exist = false;
                    for (let i = 0; i < instructor.length; i++) {
                        this.instructor.push(instructor[i]);
                    }
                    if (this.tmpSiteID != n_siteId)
                    {
                        this.editCourse.controls["ContactId"].reset();
                        this.editCourse.controls["namaInstruktur"].reset();
                    }
                    this.dataService2 = this.completerService.local(this.instructor, 'Instructor', 'Instructor');
                }
            });
    }

    // onChangeOther(isChecked: boolean) {
    //     isChecked ? this.editCourse.controls["Komen"].enable() : this.editCourse.controls["Komen"].disable();
    // }
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }
    adminya:Boolean=true;
    checkroleAdmin(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }	
    }
    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
                                for(var s=0; s< userlvlarr.length; s++){
                                        if(s!=i){
                                                if (userlvlarr[s] == "ADMIN" || userlvlarr[s] == "SUPERADMIN" || userlvlarr[s] == "ORGANIZATION" || userlvlarr[s] == "SITE" || userlvlarr[s] == "DISTRIBUTOR") {
                                                        this.trainerya = false;
                                                }
                                        }
                                }
            }	
        }
    }
    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }
    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {	
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }
    siteya:Boolean=false;	
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }
    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');	
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }
    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
    getSites(){
        if (this.adminya) {
            if (this.trainerya) {	
                this.contactId = this.useraccesdata.InstructorId;

                this.ContactName = this.useraccesdata.ContactName;
                this.roleinstructor = true;
            }
        }
        let status = ['"A"', '"H"', '"I"'];
        let siteUrl: any;
        let tmpdata =null;      
        this.editCourse.value.PartnerType =this.data.RoleId;
        if (this.adminya) {
           
            if(this.distributorya){
                siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','Distributor':'" + this.urlGetOrgId() + "'}/" + tmpdata;
            }else{
                if(this.orgya){
                    siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','Organization':'" + this.urlGetOrgId() + "'}/" + tmpdata;
                }else{
                    if(this.siteya){
                        siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','SiteId':'" + this.urlGetSiteId() + "'}/" + tmpdata;
                    }else{
                        siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "','ContactId':'" + this.useraccesdata.UserId + "'}/" + tmpdata;
                    }
                }
            }
        }else {
            siteUrl = "api/MainSite/SiteCourse/{'Status':'" + status + "','PartnerTypeId':'" + this.editCourse.value.PartnerType + "'}/" + tmpdata;
        }
        this.service.httpClientGet(siteUrl,{}).subscribe((res:any)=>{
            if(res.results.length)
                this.listSites = res.results.map((el)=>{
                    let split = el.Sites.split('|')
                    return {
                        id: split[0].trim(),
                        itemName: el.Sites
                    }
                })
        })
}
    onChangeOther(value) {
        this.teachingLevel = value;
        this.editCourse.controls["Komen"].enable();
    }

    onChangeOther2(isChecked: boolean) {
        isChecked ? this.editCourse.controls["Komen5"].enable() : this.editCourse.controls["Komen5"].disable();
    }

    getTemplateEvaId(value) {
        this.evaTemplateId = value;
    }

    evaTemplatePreview() {
        if (this.evaTemplateId == undefined || this.evaTemplateId == "") {
            swal(
                'Information!',
                'There is no survey template selected',
                'error'
            );
        } else {
            var newWindow = window.open('template-preview/survey-preview-eva/' + this.evaTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
            return false;
        }
    }

    getTemplateCertificateId(value) {
        this.certificateTemplateId = value;
    }

    certificatePreview() {
        if (this.certificateTemplateId == undefined || this.certificateTemplateId == "") {
            swal(
                'Information!',
                'There is no certificate template selected',
                'error'
            );
        } else {
            var newWindow = window.open('template-preview/certificate-preview-eva/' + this.certificateTemplateId, 'mywin', 'left=350,top=100,width=1250,height=800,toolbar=1,resizable=0');
            if (window.focus()) { newWindow.focus() }
            return false;
        }
    }

    getInstructorData(contactId, SiteId) {
        var kontak: any;
        this.service.httpClientGet("api/Courses/InstructorContact/" + contactId + "/" + SiteId, kontak)
            .subscribe(res => {
                kontak = res;
                if (kontak != null) {
                    this.editCourse.patchValue({ SiteId: kontak.SiteId });
                    this.ContactName = kontak.ContactName;
                } else {
                    this.editCourse.patchValue({ SiteId: "NOT FOUND" });
                    this.ContactName = "NOT FOUND";
                }
            }, error => {
                this.service.errorserver();
            });
    }

    getStudentPicked(id) {
        var student: any;
        this.pickedStudent = [];
        this.service.httpClientGet("api/Courses/Student/" + id, student)
            .subscribe(res => {
                student = res;
                // console.log(student);
                if (student.length > 0) {
                    for (let i = 0; i < student.length; i++) {
                        this.pickedStudent.push(student[i]);
                        //Karena studentnya sudah dipilih sebelumnya, maka dimasukkan ke variable selected student supaya tetap terbaca ketika masuk ke fungsi remove student
                        this.selectedStudent.push(student[i].StudentID);
                    }
                    this.loading = false;
                } else {
                    this.pickedStudent = [];
                    this.selectedStudent = [];
                    this.loading = false;
                }
            });
    }

    onCountriesSelect(item: any) {
        this.getStudentbyCountry();
    }
    OnCountriesDeSelect(item: any) {
        this.getStudentbyCountry();
        this.studentFound = false;
    }
    onCountriesSelectAll(item: any) {
        this.getStudentbyCountry();
    }
    onCountriesDeSelectAll(item: any) {
        this.getStudentbyCountry();
        this.studentFound = false;
    }

    getStudentbyCountry() {
        this.dataStudent = [];
        var student: any;
        if (this.selectedItemsCountry.length != 0) {
            this.loading = true;
            for (let i = 0; i < this.selectedItemsCountry.length; i++) {
                this.service.httpClientGet("api/Student/GetStudentByCountry/" + this.selectedItemsCountry[i].id, student)
                    .subscribe(result => {
                        // var finalresult = result.replace(/\t/g, "\\t")
                        // student = JSON.parse(finalresult);
                        student = result;
                        if (student.length > 0) {
                            for (let j = 0; j < student.length; j++) {
                                this.dataStudent.push(student[j]);
                            }
                            this.notFound = false;
                            this.loading = false;
                        } else {
                            if (this.dataStudent.length == 0) {
                                this.notFound = true;
                            }
                            this.loading = false;
                        }
                    });
            }
            this.studentFound = true;
        } else {
            this.studentFound = false;
            // this.loading = false;
        }
    }

    addStudent(id) {
        if (this.selectedStudent.length == 0) {
            this.selectedStudent.push(id);
        } else {
            var exist = false;
            for (let i = 0; i < this.selectedStudent.length; i++) {
                if (id == this.selectedStudent[i]) {
                    exist = true;
                    this.studentErrorTag = true;
                    this.studentSuccessTag = false;
                    this.errorTagMessage = "You have choose this student..";
                    setTimeout(function () {
                        this.studentErrorTag = false;
                    }.bind(this), 3000);
                }
            }
            if (exist == false && id != "") {
                this.selectedStudent.push(id);
                this.studentErrorTag = false;
            }
        }

        var index = this.dataStudent.findIndex(x => x.StudentID == id);
        if (index !== -1) {
            this.dataStudent.splice(index, 1);
        }

        this.refreshPickedStudent(this.selectedStudent);
    }

    showAddedStudent(firstname, lastname): void {
        this.studentSuccessTag = true;
        this.successTagMessage = "You added " + this.service.decoder(firstname) + " " + this.service.decoder(lastname) + " in this course..";
        this.siswa = "";
        setTimeout(function () {
            this.studentSuccessTag = false;
        }.bind(this), 3000);
    }

    refreshPickedStudent(selectedStudent) {
        var picked: any;
        this.pickedStudent = [];
        if (selectedStudent.length > 0) {
            for (let x = 0; x < selectedStudent.length; x++) {
                this.service.httpClientGet("api/Student/GetStudent/" + selectedStudent[x], picked)
                    .subscribe(result => {
                        picked = result;
                        var exist = false;
                        if (this.pickedStudent.length == 0) {
                            this.pickedStudent.push(picked);
                            this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                        } else {
                            for (let k = 0; k < this.pickedStudent.length; k++) {
                                if (picked.StudentID == this.pickedStudent[k].StudentID) {
                                    exist = true;
                                    this.studentSuccessTag = false;
                                }
                            }
                            if (exist == false && picked.StudentID != "") {
                                this.pickedStudent.push(picked);
                                this.showAddedStudent(this.service.decoder(picked.Firstname), this.service.decoder(picked.Lastname));
                            }
                        }
                    });
            }
            this.studentPicked = true;
        } else {
            this.studentPicked = false;
        }
    }

    removeStudent(id) {
        let studentTemp = [];
        let index = this.selectedStudent.indexOf(id)
        if (index > -1) {
            this.selectedStudent.splice(index, 1);
        }

        var index2 = this.pickedStudent.findIndex(x => x.StudentID == id);
        if (index2 !== -1) {
            //Kalau pakai unshift ga akan support di IE (buat nambah array di index awal)
            this.dataStudent.unshift(this.pickedStudent[index2]);
            studentTemp = this.dataStudent;
            this.dataStudent = [];
            this.dataStudent = studentTemp;
        }

        this.refreshPickedStudent(this.selectedStudent);
    }
    roleinstructor: boolean = false;
    tmpSiteID: any;
    ngOnInit() {
        this.checkrole();
        this.checkroleAdmin();
        this.itsinstructor();
        this.itsOrganization();
        this.itsSite();
        this.itsDistributor();
        this.id = this.route.snapshot.params['id'];
        
        // let status = ['"A"', '"H"'];
        this.loading = true;
        var data = '';
        this.datadetail = [];
        this.service.httpClientGet("api/Courses/Detail/" + this.id, data)
            .subscribe(res => {
                this.data = res;

                //allow special character
                if (this.data != null) {
                    if (this.data.Name != null && this.data.Name != '') {
                        this.data.Name = this.service.decoder(this.data.Name);
                    }
                }
                //untuk set nilai site id
                let siteTemp: any;            
                let status = ['"A"', '"H"', '"D"', '"I"', '"T"', '"X"'];
                // this.service.get("api/MainSite/SiteCourse/{'Status':'" + status + "'}" + "/" + this.data.SiteId, siteTemp)
                this.service.httpClientGet("api/MainSite/OldSiteCourse/{'Status':'" + status + "'}/" + this.data.SiteId, siteTemp)
                    .subscribe(result => {
                        siteTemp = result;
                      
                        // siteTemp = siteTemp.replace(/\t|\n|\r|\f|\\|\/|'/g, " ");
                        // siteTemp = JSON.parse(siteTemp);
                        siteTemp = result;
                        siteTemp = siteTemp.results[0].Sites;
                        this.data.SiteVal = siteTemp;
                        this.data.SiteVal = [{ id:siteTemp, itemName: siteTemp }];
                        this.listSites = [{ id:siteTemp, itemName: siteTemp }]
                        this.siteId = [{ id:siteTemp, itemName: siteTemp }]
                        this.tmpSiteID =this.data.SiteId;
                        setTimeout(() => {
                            this.getCountry();
                        }, 1000);

                        setTimeout(() => {
                            this.getPartnerType();
                            this.getSurveyIndicator();
                            this.getProduct();
                            this.getTrainingHours();
                        }, 2000);

                        setTimeout(() => {
                            this.buildForm();
                            this.loading = false;
                        }, 3000);
                        // setTimeout(() => {
                        //     this.getStudentPicked(this.id);
                        // }, 4000);
                    });
            });

        //ini udah dipindahin ke init ini lhoo
        // this.getDataCourse(this.id);

        //Ga dipake
        //ini untuk lookup student
        // this.getDataSiswa();

        //ini untuk ngambil data student sebelumnya yang sudah tersimpan di database
        // this.getStudentPicked(this.id);

        //Ga dipake
        // this.getSite();

        // this.getPartnerType();
        // this.getSurveyIndicator();
        // this.getProduct();
        // this.getTrainingHours();

        //Ga dipake
        // this.getEvaTemplate();
        // this.getCertificateTemplate();
        // this.getGeo();

        this.parentLabel = true;

        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        //untuk mengecek apakah sudah ada yang mengisi survey atau belum, jika sudah akan ditampilkan tabelnya jika belum ya tentu tidak...
        if (this.evalsNumber > 0) {
            let dataEvals: any;
            this.service.httpClientGet("api/Courses/StudentEvaluation/" + this.id, dataEvals)
                .subscribe(res => {
                    dataEvals = res;
                    this.dataEvals = dataEvals;
                    this.showEvals = true;
                });
        }
    }

    getCountry() {
        var data: any;
        this.service.httpClientGet("api/Countries", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            });
        this.selectedItemsCountry = [];
    }

    inputStudent(id) {
        var input: any;
        var studentTemp = {
            'CourseId': this.id,
            'StudentID': id,
            'Status': this.editCourse.value.Status,
            'CourseTaken': 0,
            'SurveyTaken': 0
        };
        let studentData = JSON.stringify(studentTemp);
        this.service.httpClientPost("api/Courses/CourseStudent", studentData)
            .subscribe(resultPost => {
                input = resultPost;
                if (input["code"] != '1') swal('Information', 'Error when update into Table CourseStudent', 'error');
            });
    }

    deleteStudent(id) {
        var del: any;
        this.service.httpClientDelete("api/Courses/CourseStudent/where/{'CourseId':'" + this.id + "','StudentID':'" + id + "'}", del)
            .subscribe(result => {
                del = result;
                if (del["code"] != '1') swal('Information', 'Error when update into Table CourseStudent', 'error');
            });
    }

    insertToCourseStudent(data) {
        // if (this.pickedStudent.length > 0) {

        // } else {
        //     swal(
        //         'Information!',
        //         'There are no Student Picked',
        //         'error'
        //     );
        //     this.loading = false;
        // }
        var student: any;
        for (let y = 0; y < this.pickedStudent.length; y++) {
            var studentTemp = {
                'CourseId': this.editCourse.value.CourseId,
                'StudentID': this.pickedStudent[y].StudentID,
                'Status': this.editCourse.value.Status,
                'CourseTaken': 0,
                'SurveyTaken': 0
            };
            let studentData = JSON.stringify(studentTemp);
            this.service.httpClientPost("api/Courses/CourseStudent", studentData)
                .subscribe(result5 => {
                    student = result5;
                    if (student["code"] == '1') {
                        // this.service.openSuccessSwal(student["message"]);
                        console.log(student["message"])
                    } else {
                        swal(
                            'Information!',
                            'Error When Insert Into Course Student Table',
                            'error'
                        );
                    }
                }, error => { this.service.errorserver(); });
        }
        this.router.navigate(['/course/course-list']);
        this.loading = false;
    }

    putToLocalStorage(data) {
        var dataJson = JSON.parse(data)
        // console.log(dataJson);
        let cert: any;
        // let certdropdown: any;
        let selcert: any;
        // let selcertdropdown: any;
        var selectedPartner: any;
        if (dataJson.PartnerType == "ATC" || dataJson.PartnerType == "1") {
            selectedPartner = [{ id: "ATC", itemName: "Authorized Training Center (ATC)" }];
        } else if (dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event" || dataJson.PartnerType == "58") {
            selectedPartner = [{ id: "AAP", itemName: "Authorized Academic Partner (AAP)" }];
        } else {
            selectedPartner = [{ id: "ATC", itemName: "Authorized Training Center (ATC)" }, { id: "AAP", itemName: "Authorized Academic Partner (AAP)" }];
        }

        // if(dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event"){
            if (dataJson.PartnerType == "AAP Course" || dataJson.PartnerType == "AAP Project" || dataJson.PartnerType == "AAP Event" || dataJson.PartnerType == "58") {
                this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", '')
                .subscribe(res => {
                    cert = res;
                    this.certdropdown = cert.map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType','Key':'" + dataJson.CertificateType + "'}", '')
                    .subscribe(res => {
                        selcert = res;
                        this.selcertdropdown = selcert.map((item) => {
                            return {
                                id: item.Key,
                                itemName: item.KeyValue
                            }
                        })
                    });
                });
        } else if (dataJson.PartnerType == "ATC" || dataJson.PartnerType == "1") { //issue (salah populate certificate pas edit / add atc) ASAP 25/07/2018
            this.certdropdown = [];
            this.selcertdropdown = [];
        }
        else {
            this.service.httpClientGet("api/Dictionaries/where/{'Parent':'AAPCertificateType'}", '')
            .subscribe(res => {
                cert = res;
                this.certdropdown = cert.map((item) => {
                    return {
                        id: item.Key,
                        itemName: item.KeyValue
                    }
                });
                this.selcertdropdown = cert.map((item) => {
                    return {
                        id: item.Key,
                        itemName: item.KeyValue
                    }
                });
            });
        }

        setTimeout(() => {
            var params =
            {
                idCourse: dataJson.CourseId,
                selectPartner: selectedPartner,
                dropPartner: this.dropdownPartner.map((item) => {
                    return {
                        id: item.RoleCode,
                        itemName: item.RoleName
                    }
                }),
                dropCerti: this.certdropdown,
                selectCerti: this.selcertdropdown,
                idOrg: "",
                idSite: "",
                idInstructor: "",
                titleCourse: "",
                courseStart: dataJson.StartDate,
                courseEnd: dataJson.EndDate,
                year: "",
                sortV: "CourseId",
                sortD: "ASC"
            }

            localStorage.setItem("filter", JSON.stringify(params));
        }, 3000)
    }

    insertToCTM(data) {
        let autodesk = (this.editCourse.value.Autodesk == "") ? false : true;
        let aap = (this.editCourse.value.AAP == "") ? false : true;
        let atc = (this.editCourse.value.ATC == "") ? false : true;
        let independent = (this.editCourse.value.Independent == "") ? false : true;
        let independentonline = (this.editCourse.value.IndependentOnline == "") ? false : true;
        let atconline = (this.editCourse.value.ATCOnline == "") ? false : true;
        let other5 = (this.editCourse.value.Other5 == "") ? false : true;

        var l: any;
        var ctm = {
            'CourseId': this.editCourse.value.CourseId,
            'Autodesk': autodesk,
            'AAP': aap,
            'ATC': atc,
            'Independent': independent,
            'IndependentOnline': independentonline,
            'ATCOnline': atconline,
            'Other5': other5,
            'Komen5': this.editCourse.value.Komen5,
            'Status': this.editCourse.value.Status
        };
        let courseTrainingMaterial = JSON.stringify(ctm);
        this.service.httpClientPost("api/Courses/CourseTrainingMaterial", courseTrainingMaterial)
            .subscribe(result4 => {
                l =result4;
                if (l["code"] == '1') {
                    // this.insertToCourseStudent(data);
                    this.putToLocalStorage(data);
                    setTimeout(() => {
                        this.router.navigate(['/course/course-list']);
                        this.loading = false;
                    }, 5000)
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Training Material Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCTF(data) {
        let instructorled = (this.editCourse.value.InstructorLed == "") ? false : true;
        let online = (this.editCourse.value.Online == "") ? false : true;

        var k: any;
        var ctf = {
            'CourseId': this.editCourse.value.CourseId,
            'InstructorLed': instructorled,
            'Online': online,
            'Status': this.editCourse.value.Status
        };
        // console.log(ctf);
        let courseTrainingFormat = JSON.stringify(ctf);
        this.service.httpClientPost("api/Courses/CourseTrainingFormat", courseTrainingFormat)
            .subscribe(result3 => {
                k =result3;
                if (k["code"] == '1') {
                    this.insertToCTM(data);
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Training Format Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCourseSoftware(data) {
        var j: any;
        var version1;
        var version2;
        if (this.editCourse.value.Version1 != null && this.editCourse.value.Version1 != "") {
            version1 = this.editCourse.value.Version1;
        } else {
            version1 = 0; //Menandakan tidak mempunyai versi, silahkan diperbaiki jika sudah ada ketetapan
        }

        if (this.editCourse.value.Version2 != null && this.editCourse.value.Version2 != "") {
            version2 = this.editCourse.value.Version2;
        } else {
            version2 = 0; //Menandakan tidak mempunyai versi, silahkan diperbaiki jika sudah ada ketetapan
        }

        var cs = {
            'CourseId': this.editCourse.value.CourseId,
            'Product1': this.editCourse.value.Product1,
            'Version1': version1,
            'Product2': this.editCourse.value.Product2,
            'Version2': version2,
            'Status': this.editCourse.value.Status
        };
        let courseSoftware = JSON.stringify(cs);
        this.service.httpClientPost("api/Courses/CourseSoftware", courseSoftware)
            .subscribe(result2 => {
                j = result2;
                if (j["code"] == '1') {
                    if (this.editCourse.value.CertificateType == 2 || this.editCourse.value.CertificateType == 3) {
                        // this.insertToCourseStudent(data);
                        this.putToLocalStorage(data);
                        setTimeout(() => {
                            this.router.navigate(['/course/course-list']);
                            this.loading = false;
                        }, 5000)
                    } else {
                        this.insertToCTF(data);
                    }
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Software Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCTL(data) {
        let update = (this.editCourse.value.Update == "") ? false : true;
        let essentials = (this.editCourse.value.Essentials == "") ? false : true;
        let intermediate = (this.editCourse.value.Intermediate == "") ? false : true;
        let advanced = (this.editCourse.value.Advanced == "") ? false : true;
        let customized = (this.editCourse.value.Customized == "") ? false : true;
        let other = (this.editCourse.value.Other == "") ? false : true;

        var i: any;
        var ctl = {
            'CourseId': this.editCourse.value.CourseId,
            'Update': update,
            'Essentials': essentials,
            'Intermediate': intermediate,
            'Advanced': advanced,
            'Customized': customized,
            'Other': other,
            'Komen': this.editCourse.value.Komen,
            'Status': this.editCourse.value.Status
        };

        let courseTeachingLevel = JSON.stringify(ctl);
        this.service.httpClientPost("api/Courses/CourseTeachingLevel", courseTeachingLevel)
            .subscribe(result1 => {
                i = result1;
                if (i["code"] == '1') {
                    this.insertToCourseSoftware(data);
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Teaching Level Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    insertToCourse(data) {
        var x: any;
        this.service.httpClientPost("api/Courses", data)
            .subscribe(result => {
                // console.log(result)
                x = result;
                if (x["code"] == '1') {
                    this.lastCourseId = x["lastInsertId"];
                    this.editCourse.value.CourseId = this.lastCourseId;
                    let newDataCourse = JSON.stringify(this.editCourse.value);
                    if (this.editCourse.value.CertificateType == 3) {
                        this.insertToCourseSoftware(newDataCourse);
                    } else if (this.editCourse.value.CertificateType == 2) {
                        this.insertToCTL(newDataCourse);
                    } else {
                        this.insertToCTL(newDataCourse);
                    }
                } else {
                    swal(
                        'Information!',
                        'Error When Insert Into Course Table',
                        'error'
                    );
                    this.loading = false;
                }
            }, error => { this.service.errorserver(); this.loading = false; });
    }

    buildForm(): void {
        // console.log(this.data);

        // Set partner type
        let partnerTemp: any;
        if (this.data.PartnerType == 'ATC') {
            partnerTemp = 1;
        } else if (this.data.PartnerType == 'CTC') {
            partnerTemp = 61;
        } else {
            partnerTemp = 58;
        }

        // this.editCourse.controls["PartnerType"].disable();
        this.selectedPartner(partnerTemp);

        //Set certificate type
        let certificate: any;
        if (partnerTemp == 58) {
            if (this.data.PartnerType == 'AAP Course') {
                certificate = 1;
            } else if (this.data.PartnerType == 'AAP Project') {
                certificate = 2;
            } else if (this.data.PartnerType == 'AAP Event') {
                certificate = 3;
            } else {
                certificate = 0;
            }
            this.selectedCertificate(certificate);
        }
        // this.editCourse.controls["CertificateType"].disable();
        this.ContactName = this.data.ContactName;

        //Set course course start and completion date, survey year too
        // let startDate = this.data.StartDate.split('-');
        // let completionDate = this.data.CompletionDate.split('-');
        // this.modelPopup1 = {
        //     "year": parseInt(startDate[0]),
        //     "month": parseInt(startDate[1]),
        //     "day": parseInt(startDate[2])
        // };

        // this.modelPopup2 = {
        //     "year": parseInt(completionDate[0]),
        //     "month": parseInt(completionDate[1]),
        //     "day": parseInt(completionDate[2])
        // };

        //Set course teaching level
        let updateVal = (this.data.Update == '1') ? true : false;
        let essensVal = (this.data.Essentials == '1') ? true : false;
        let intermediateVal = (this.data.Intermediate == '1') ? true : false;
        let advancedVal = (this.data.Advanced == '1') ? true : false;
        let customVal = (this.data.Customized == '1') ? true : false;

        let otherVal: boolean;
        let otherNilai: boolean;
        if (this.data.ctl_Other == '1') {
            otherVal = false;
            otherNilai = true;
            // this.editCourse.controls["Komen"].enable();
        } else {
            otherVal = true;
            otherNilai = false;
            // this.editCourse.controls["Komen"].disable();
        }

        //Set product and product version (primary & secondary)
        this.findVersion1(this.data.productId);
        this.findVersion2(this.data.productsecondaryId);

        //Set course teaching format
        let instructorLedVal = (this.data.InstructorLed == '1') ? true : false;
        let onlineVal = (this.data.Online == '1') ? true : false;

        //Set course teaching materials
        let autodeskVal = (this.data.Autodesk == '1') ? true : false;
        let aapVal = (this.data.ctm_AAP == '1') ? true : false;
        let atcVal = (this.data.ctm_ATC == '1') ? true : false;
        let independentVal = (this.data.Independent == '1') ? true : false;
        let independentOnlineVal = (this.data.IndependentOnline == '1') ? true : false;
        let atcOnlineVal = (this.data.ATCOnline == '1') ? true : false;

        let ctmOther: boolean;
        let ctmOtherVal: boolean;
        if (this.data.ctm_Other == '1') {
            ctmOther = false;
            ctmOtherVal = true;
            // this.editCourse.controls["Komen5"].enable();
        } else {
            ctmOther = true;
            ctmOtherVal = false;
            // this.editCourse.controls["Komen5"].disable();
        }

        this.contactID = this.data.InstructorId;

        let teachingVal = [this.data.Update, this.data.Essentials, this.data.Intermediate, this.data.Advanced, this.data.Customized, this.data.ctl_Other];
        var teachingIndex = teachingVal.indexOf("1");
        let stringVal: string = "";
        switch (teachingIndex) {
            case 0:
                stringVal = "Update";
                break;
            case 1:
                stringVal = "Essentials";
                break;
            case 2:
                stringVal = "Intermediate";
                break;
            case 3:
                stringVal = "Advanced";
                break;
            case 4:
                stringVal = "Customized";
                break;
            case 5:
                stringVal = "Other";
                break;
        }
        this.teachingLevel = stringVal;

        // let trainVal: any;
        // let isTrain: boolean = false;
        // if (this.trainingHours.length != 0) {
        //     for (let m = 0; m < this.trainingHours.length; m++) {
        //         if (this.data.HoursTraining == this.trainingHours[m].Key) {
        //             isTrain = true;
        //             trainVal = this.trainingHours[m].Key;
        //         }
        //     }
        // }

        // if (isTrain) {
        //     trainVal = trainVal;
        // } else {
        //     trainVal = "";
        // }

        // this.dataService1 = this.completerService.remote(this.siteUrl + "/" + this.data.SiteId, this.data.SiteId, null);

        let imageLogo = {};
        if(this.data.InstitutionLogo != ""){
            imageLogo = {
                filename: this.data.LogoName,
                filetype: this.data.LogoType,
                value: this.data.InstitutionLogo
            }
            this.imgSrc = "data:image/png;base64," + this.data.InstitutionLogo;
            this.imgLogoName = this.data.LogoName;
            this.projectLogoUpload = false;
            this.isAvailableLogo = true;
        } else{
            imageLogo = {
                filename: "",
                filetype: "",
                value: ""
            }
        }

        let CourseId = new FormControl(this.data.CourseId);
        let PartnerType = new FormControl({ value: partnerTemp, disabled: true });
        let CertificateType = new FormControl(certificate);
        let CourseTitle = new FormControl(this.data.Name, [Validators.required, Validators.maxLength(40)]);
        let SiteId = new FormControl(this.data.SiteVal, Validators.required);
        this.getInstructor(this.data.SiteId);
        let ContactId = new FormControl(this.data.InstructorId + " | " + this.data.ContactName, Validators.required);
        let Institution = new FormControl(this.data.Institution, Validators.required);
        let StartDate = new FormControl('', Validators.required);
        let EndDate = new FormControl('', Validators.required);
        // let Survey = new FormControl(this.data.FYIndicatorKey, Validators.required); /* ganti jadi tahun sekarang */
        let Survey = new FormControl(new Date().getFullYear(), Validators.required);
        let Update = new FormControl(updateVal);
        let Essentials = new FormControl(essensVal);
        let Intermediate = new FormControl(intermediateVal);
        let Advanced = new FormControl(advancedVal);
        let Customized = new FormControl(customVal);
        let Other = new FormControl(otherNilai);
        let Komen = new FormControl({ value: this.data.ctl_Comment, disabled: otherVal });
        let Status = new FormControl('');
        let Product1 = new FormControl(this.data.productId, Validators.required);
        let Version1 = new FormControl('', Validators.required);
        let Product2 = new FormControl(this.data.productsecondaryId);
        let Version2 = new FormControl(this.data.ProductVersionsSecondaryId);
        let TrainingHours = new FormControl(this.data.HoursTraining, Validators.required);
        let InstructorLed = new FormControl(instructorLedVal);
        let Online = new FormControl(onlineVal);
        let TrainingType = new FormControl(this.data.TrainingTypeKey);
        let Autodesk = new FormControl(autodeskVal);
        let AAP = new FormControl(aapVal);
        let ATC = new FormControl(atcVal);
        let Independent = new FormControl(independentVal);
        let IndependentOnline = new FormControl(independentOnlineVal);
        let ATCOnline = new FormControl(atcOnlineVal);
        let Other5 = new FormControl(ctmOtherVal);
        let Komen5 = new FormControl({ value: this.data.ctm_Comment, disabled: ctmOther });
        let CourseFacility = new FormControl(this.data.ATCFacility, Validators.required);
        let CourseEquipment = new FormControl(this.data.ATCComputer, Validators.required);
        // let EvaTemplate = new FormControl('', Validators.required);
        // let CertTemplate = new FormControl('', Validators.required);
        let StudentID = new FormControl('');
        let ProjectType = new FormControl(this.data.ProjectTypeKey, Validators.required);
        let LocationName = new FormControl(this.data.LocationName, Validators.required);
        let EventType = new FormControl(this.data.EventTypeKey, Validators.required);
        let namaInstruktur = new FormControl({ value: this.data.ContactName, disabled: true });
        let LevelTeaching = new FormControl(stringVal, Validators.required);
        let InstitutionLogo = new FormControl(imageLogo); //change to save logo into database
        let HoursTrainingOther = new FormControl(0);

        this.editCourse = new FormGroup({
            CourseId: CourseId,
            PartnerType: PartnerType,
            CertificateType: CertificateType,
            CourseTitle: CourseTitle,
            SiteId: SiteId,
            ContactId: ContactId,
            Institution: Institution,
            StartDate: StartDate,
            EndDate: EndDate,
            Survey: Survey,
            Update: Update,
            Essentials: Essentials,
            Intermediate: Intermediate,
            Advanced: Advanced,
            Customized: Customized,
            Other: Other,
            Komen: Komen,
            Status: Status,
            Product1: Product1,
            Version1: Version1,
            Product2: Product2,
            Version2: Version2,
            TrainingHours: TrainingHours,
            InstructorLed: InstructorLed,
            Online: Online,
            TrainingType: TrainingType,
            Autodesk: Autodesk,
            AAP: AAP,
            ATC: ATC,
            Independent: Independent,
            IndependentOnline: IndependentOnline,
            ATCOnline: ATCOnline,
            Other5: Other5,
            Komen5: Komen5,
            CourseFacility: CourseFacility,
            CourseEquipment: CourseEquipment,
            // EvaTemplate: EvaTemplate,
            // CertTemplate: CertTemplate,
            StudentID: StudentID,
            ProjectType: ProjectType,
            LocationName: LocationName,
            EventType: EventType,
            namaInstruktur: namaInstruktur,
            LevelTeaching: LevelTeaching,
            InstitutionLogo: InstitutionLogo,
            HoursTrainingOther:HoursTrainingOther
        });
    }

    onSubmit() {
        if (this.partnerSelected == 1) {
            this.editCourse.removeControl('CertificateType');
            this.editCourse.removeControl('Institution');
            this.editCourse.removeControl('TrainingType');
            this.editCourse.removeControl('ProjectType');
            this.editCourse.removeControl('EventType');
            this.editCourse.removeControl('LocationName');
            this.editCourse.controls['CourseFacility'].markAsTouched();
            this.editCourse.controls['CourseEquipment'].markAsTouched();
            this.editCourse.controls['TrainingHours'].markAsTouched();
            this.editCourse.controls['LevelTeaching'].markAsTouched();
        } else {
            if (this.certificateSelected == 1) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('ProjectType');
                this.editCourse.removeControl('EventType');
                this.editCourse.removeControl('LocationName');
                this.editCourse.controls['Institution'].markAsTouched();
                this.editCourse.controls['TrainingType'].markAsTouched();
                this.editCourse.controls['TrainingHours'].markAsTouched();
                this.editCourse.controls['LevelTeaching'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 2) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('TrainingType');
                this.editCourse.removeControl('EventType');
                this.editCourse.removeControl('LocationName');
                this.editCourse.controls['Institution'].markAsTouched();
                this.editCourse.controls['ProjectType'].markAsTouched();
                this.editCourse.controls['TrainingHours'].markAsTouched();
                this.editCourse.controls['LevelTeaching'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            } else if (this.certificateSelected == 3) {
                this.editCourse.removeControl('CourseFacility');
                this.editCourse.removeControl('CourseEquipment');
                this.editCourse.removeControl('Institution');
                this.editCourse.removeControl('TrainingType');
                this.editCourse.removeControl('ProjectType');
                this.editCourse.removeControl('TrainingHours');
                this.editCourse.removeControl('LevelTeaching');
                this.editCourse.controls['LocationName'].markAsTouched();
                this.editCourse.controls['EventType'].markAsTouched();
                this.editCourse.value.CourseFacility = 0;
                this.editCourse.value.CourseEquipment = 0;
            }
            this.editCourse.controls['CertificateType'].markAsTouched();
        }

        this.editCourse.controls['PartnerType'].markAsTouched();
        this.editCourse.controls['CourseTitle'].markAsTouched();
        this.editCourse.controls['SiteId'].markAsTouched();
        this.editCourse.controls['ContactId'].markAsTouched();
        this.editCourse.controls['StartDate'].markAsTouched();
        this.editCourse.controls['EndDate'].markAsTouched();
        this.editCourse.controls['Survey'].markAsTouched();
        this.editCourse.controls['Product1'].markAsTouched();

        // this.editCourse.controls['Version1'].markAsTouched();
        // this.editCourse.controls['Product2'].markAsTouched();
        // this.editCourse.controls['Version2'].markAsTouched();
        // this.editCourse.controls['EvaTemplate'].markAsTouched();
        // this.editCourse.controls['CertTemplate'].markAsTouched();

        if (this.editCourse.valid) {
            swal({
                title: 'Are you sure?',
                text: "If you wish to Copy Course!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, sure!'
            }).then(result => {
                if (result == true) {
                    this.loading = true;
                    // let siteTemp = (this.editCourse.value.SiteId).split(" | ");

                    // this.editCourse.value.SiteId = siteTemp[0];
                    this.editCourse.value.ContactId = this.contactID;
                    this.editCourse.value.PartnerType = this.coursePartner;

                    this.editCourse.value.StartDate = this.formatdate.dateCalendarToYMD(this.editCourse.value.StartDate);
                    this.editCourse.value.EndDate = this.formatdate.dateCalendarToYMD(this.editCourse.value.EndDate);
                    this.editCourse.value.Status = "A";

                    this.editCourse.value.cuid = this.useraccesdata.ContactName;
                    this.editCourse.value.UserId = this.useraccesdata.UserId;

                    this.editCourse.value.Update = false;
                    this.editCourse.value.Essentials = false;
                    this.editCourse.value.Intermediate = false;
                    this.editCourse.value.Advanced = false;
                    this.editCourse.value.Customized = false;
                    this.editCourse.value.Other = false;

                    if (this.teachingLevel == "Update") {
                        this.editCourse.value.Update = true;
                    } else if (this.teachingLevel == "Essentials") {
                        this.editCourse.value.Essentials = true;
                    } else if (this.teachingLevel == "Intermediate") {
                        this.editCourse.value.Intermediate = true;
                    } else if (this.teachingLevel == "Advanced") {
                        this.editCourse.value.Advanced = true;
                    } else if (this.teachingLevel == "Customized") {
                        this.editCourse.value.Customized = true;
                    } else if (this.teachingLevel == "Other") {
                        this.editCourse.value.Other = true;
                    }

                    // if (this.editCourse.value.Update == "") this.editCourse.value.Update = false;
                    // if (this.editCourse.value.Essentials == "") this.editCourse.value.Essentials = false;
                    // if (this.editCourse.value.Intermediate == "") this.editCourse.value.Intermediate = false;
                    // if (this.editCourse.value.Advanced == "") this.editCourse.value.Advanced = false;
                    // if (this.editCourse.value.Customized == "") this.editCourse.value.Customized = false;
                    // if (this.editCourse.value.Other == "") this.editCourse.value.Other = false;

                    if (this.editCourse.value.InstructorLed == "") this.editCourse.value.InstructorLed = false;
                    if (this.editCourse.value.Online == "") this.editCourse.value.Online = false;
                    if (this.editCourse.value.Autodesk == "") this.editCourse.value.Autodesk = false;
                    if (this.editCourse.value.AAP == "") this.editCourse.value.AAP = false;
                    if (this.editCourse.value.ATC == "") this.editCourse.value.ATC = false;
                    if (this.editCourse.value.Independent == "") this.editCourse.value.Independent = false;
                    if (this.editCourse.value.IndependentOnline == "") this.editCourse.value.IndependentOnline = false;
                    if (this.editCourse.value.ATCOnline == "") this.editCourse.value.ATCOnline = false;
                    if (this.editCourse.value.Other5 == "") this.editCourse.value.Other5 = false;
                    if(this.editCourse.value.TrainingHours == "Other"){
						this.editCourse.value.TrainingHours=this.trainingHoursOther;
						this.editCourse.value.HoursTrainingOther=1;
                    }
                    //allow special character
                    this.editCourse.value.CourseTitle = this.service.encoder(this.editCourse.value.CourseTitle);
                    let tmpSite :string=this.editCourse.value.SiteId[0].id.toString(); 
                    if(tmpSite.includes("|")) this.tmpSiteID = this.data.SiteId;     
                    else this.tmpSiteID = this.editCourse.value.SiteId[0].id;
                    let pre_process_data = {...this.editCourse.value, SiteId:this.tmpSiteID}
                    //let data = JSON.stringify(this.editCourse.value);
                    // console.log(JSON.parse(data));
                    let data = JSON.stringify(pre_process_data);
                    this.insertToCourse(data);
                }
            }).catch(swal.noop);
        } else {
            console.log("Ada yang ga valid");
            this.loading = false;
            swal(
                'Field is Required!',
                'Please enter the Add Course form required!',
                'error'
            )
        }
    }
}