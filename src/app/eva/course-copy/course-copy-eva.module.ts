import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CourseCopyEVAComponent } from './course-copy-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { Ng2CompleterModule } from "ng2-completer";
import { FormsModule } from "@angular/forms";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const CourseCopyEVARoutes: Routes = [
    {
        path: '',
        component: CourseCopyEVAComponent,
        data: {
            breadcrumb: 'Copy Course',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CourseCopyEVARoutes),
        SharedModule,
        Ng2CompleterModule,
        FormsModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [CourseCopyEVAComponent],
    providers: [AppService, AppFormatDate, AppFilterGeo]
})

export class CourseCopyEVAModule { }