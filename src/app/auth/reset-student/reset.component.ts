import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';
import { PasswordValidators } from 'ngx-validators';
import { Observable } from "rxjs/Observable";
import { Http, Headers, Response } from "@angular/http";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html'
})
export class ResetComponent implements OnInit {


  public loading = false;
  private _serviceUrl = 'api/Auth/ChangePasswordStudent';
  public data: any;
  changepasswordform: FormGroup;
  public useraccesdata: any;
  public form: FormGroup;
  id: string;
  public ActiveLang: string = "";


  constructor(private _http: Http, private translate: TranslateService, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level  
    let password = new FormControl('', [Validators.required, Validators.minLength(6), Validators.compose([
      PasswordValidators.digitCharacterRule(1),
      PasswordValidators.specialCharacterRule(1)])]);
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({
      password: password,
      rpassword: rpassword
    });

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }

  }

  login() {
    var login_val_json =
    {
      Email: this.Email,
      Password: this.changepasswordform.value.password
    }

    /* autodesk plan 10 oct */

    this.service.httpClientPost("api/auth/LoginStudent", login_val_json)
      .subscribe(result => {
        this.changepasswordform.reset();
        var resource = result;
        if (resource['code'] == '1') {
          this.session.logIn(resource["jwt"], JSON.stringify(resource['user']));

          let useracces = this.session.getData();
          this.useraccesdata = JSON.parse(useracces);

          var level2 = this.useraccesdata.UserLevelId.split(',');

          if (level2[0] == "ADMIN" || level2[0] == "ORGANIZATION" || level2[0] == "DISTRIBUTOR" || level2[0] == "TRAINER" || level2[0] == "STUDENT") {
            this.router.navigate(['/dashboard-' + level2[0].toLowerCase() + '']);
            this.loading = false;
          } else if (level2[0] == "SITE") {
            this.router.navigate(['/dashboard-organization']);
            this.loading = false;
          } else if (level2[0] == "SUPERADMIN" || level2[0] == "APPROVER_1" || level2[0] == "APPROVER_2") {
            this.router.navigate(['/dashboard-admin']);
            this.loading = false;
          } else {
            this.router.navigate(['/no-role-page']);
            this.loading = false;
          }
        }
      },
      error => {
        swal(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */
  }

  post(data): Observable<string> {
    return this._http.post(
      "api/auth/LoginStudent",
      data,
      { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
    )
      .map((response: Response) => {
        return response.text();
      });
  }

  Email: string = "";
  ngOnInit() {
    localStorage.setItem("stop","false");
    this.id = this.route.snapshot.params['code'];

    //get activity data detail
    let data = "";
    this.service.httpClientGet("api/Auth/ResetPasswordStudent/" + this.id, data)
      .subscribe(result => {
        this.loading = false;
        var resource = result;
        if (resource['code'] == '1') {
          //this.service.openSuccessSwal(resource['message']); 
          this.Email = resource['Email'];
        }
        else {
          swal(
            'Information!',
            resource['message'],
            'error'
          );
          this.router.navigate(['/forget-password-student']);
        }

      },
        error => {
          this.loading = false;
          this.service.errorserver();
        });

  }

  onSubmit() {
    this.loading = true;
    this.changepasswordform.value.ResetPasswordCode = this.id;
    this.changepasswordform.value.Password = this.changepasswordform.value.password;

    let data = JSON.stringify(this.changepasswordform.value);
    this.service.httpClientPost(this._serviceUrl, data)
      .subscribe(result => {

        this.loading = false;
        var resource = result;
        if (resource['code'] == '1') {
          // this.service.openSuccessSwal(resource['message']); 
          this.login();
        }
        else {
          swal(
            'Information!',
            resource['message'],
            'error'
          );
        }
      },
        error => {
          this.loading = false;
          this.service.errorserver();
        });

    //  this.router.navigate(['/login-student']);
  }
}
