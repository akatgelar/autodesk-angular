import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { SessionService } from '../../shared/service/session.service';

import { TranslateService } from "@ngx-translate/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";


@Component({
  selector: 'app-terminate-account',
  templateUrl: './terminate-account.component.html',
  encapsulation: ViewEncapsulation.None
})

export class TerminateAccountComponent implements OnInit {

  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  windowWidth: number;

  public loading = false;
  private _serviceUrl = 'api/Auth/TerminateAccount';
  public useraccesdata: any;
  terminatedform: FormGroup;
  public datadetail: any;
  terminateStatus: string = "";

  listlanguage: any;
  listlanguage1 = [];
  public ActiveLang: string = "";

  constructor(private router: Router, private _http: Http, private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private translate: TranslateService, private http: HttpClient) {
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let WarningMessage = new FormControl(false);

    this.terminatedform = new FormGroup({
      WarningMessage: WarningMessage
    });

    // Languages
    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    /* autodesk plan 10 oct */

    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}","")
      .subscribe(result => {
        translate.langs = [];
        var resTemp: any;
        resTemp = result;
        if (resTemp.length != 0) {
          for (let i = 0; i < resTemp.length; i++) {
            if (this.listlanguage1.length == 0) {
              this.listlanguage1.push(resTemp[i]);
            } else {
              for (let k = 0; k < this.listlanguage1.length; k++) {
                var exist = false;
                if (resTemp[i].Key == this.listlanguage1[k].Key) {
                  exist = true;
                }
              }
              if (exist == false) {
                this.listlanguage1.push(resTemp[i]);
              }
            }
          }
        }
        translate.addLangs(this.listlanguage1.sort(compare));
      });

    /* end line autodesk plan 10 oct */

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }
  }

  loadingdata=false;
  ngOnInit() {
    localStorage.setItem("stop","false");
    this.loadingdata=true;
    this.service.httpClientGet('api/Student/GetStudent/' + this.useraccesdata.UserId, '')
      .subscribe(result => {
        if (result == "Not Found") {
          this.datadetail = '';
          this.loadingdata=false;
        }
        else {
          this.datadetail = result;
          if (this.datadetail.Firstname != null && this.datadetail.Firstname != '') {
            this.datadetail.Firstname = this.service.decoder(this.datadetail.Firstname);
        }
        if (this.datadetail.Lastname != null && this.datadetail.Lastname != '') {
          this.datadetail.Lastname = this.service.decoder(this.datadetail.Lastname);
      }
          this.terminateStatus = this.datadetail.Status
          this.loadingdata=false;
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
          this.loadingdata=false;
        });
  }

  setLanguage(value) {
    this.translate.use(value);
    localStorage.setItem("language", value);
    this.ActiveLang = value;
    location.reload();
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onSubmit() {
    this.terminatedform.controls['WarningMessage'].markAsTouched();
    if (this.terminatedform.value.WarningMessage) {
      this.loading = true;
      let data =
        {
          "StudentId": this.useraccesdata.UserId,
          "StudentName": this.service.encoder(this.useraccesdata.StudentName)
        }

      /* autodesk plan 10 oct */

      // post action
      this.service.httpClientPost(this._serviceUrl,data)
        .subscribe(result => {
          var resource = result;
          this.loading = false;
          if (resource['code'] == '1') {
            swal(
              'Information!',
              resource['message'],
              'success'
            );
            this.router.navigate(['/dashboard-student']);
            this.loading = false;
          }
          else {
            swal(
              'Information!',
              resource['message'],
              'error'
            );
          }
        },
        error => {
          this.loading = false;
          this.service.errorserver();
        });

      /* end line autodesk plan 10 oct */
    }
  }

}
