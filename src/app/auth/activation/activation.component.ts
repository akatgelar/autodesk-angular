import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { PasswordValidators } from 'ngx-validators';
import { Http, Headers, Response } from "@angular/http";
import { SessionService } from '../../shared/service/session.service';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html'
})
export class ActivationComponent implements OnInit {
  private _serviceUrl = '';
  public data: any;
  changepasswordform: FormGroup;
  link: string = "";
  id: string = "";
  curentpassword: string = "";
  activationpage: boolean = false;
  public ActiveLang: string = "";
  public loading = false;

  constructor(public session: SessionService, private translate: TranslateService, private _http: Http, private service: AppService, private route: ActivatedRoute, private router: Router) {
    let password = new FormControl('', [Validators.required, Validators.minLength(6), Validators.compose([
      PasswordValidators.digitCharacterRule(1),
      PasswordValidators.specialCharacterRule(1)])]);
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({
      password: password,
      rpassword: rpassword
    });

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }
  }

  loginurl: string = ""
  linksplit: any;
  ngOnInit() {
    this.link = this.route.snapshot.params['id'];
    this.linksplit = this.link.split('-');

    if (this.linksplit[0] == "contact" || this.linksplit[0] == "student") {
      this.service.httpClientGet("api/auth/ActivationCheck/" + this.linksplit[0] + "/" + this.linksplit[1], '')
        .subscribe(result => {
          var data = result;
          if (result == "Not Found" || result == "{}") {
            this.service.httpClientDelete("api/auth/ExpiredActivation/" + this.linksplit[0] + '/' + this.linksplit[1], '');
            this.router.navigate(["/forbidden"]);
          }
          else {
            this.activationpage = true;
            var urlfirstlogin = ""
            if (this.linksplit[0] == "contact") {
              this._serviceUrl = 'api/ContactAll';
              this.id = data["ContactId"];
              urlfirstlogin = "api/MainContact/FirstLogin";
              this.loginurl = "Login"
            } else {
              this._serviceUrl = 'api/Student';
              this.id = data["StudentID"];
              urlfirstlogin = "api/MainContact/FirstLoginStudent";
              this.loginurl = "LoginStudent"
            }

            this.curentpassword = data["password"];

            this.service.httpClientGet(urlfirstlogin + "/" + this.id, "")
              .subscribe(result => {
                var tmp = result;
                if (tmp != null) {
                  this.Email = tmp["EmailAddress"];
                }
              },
              error => {
                console.log("failed load email")
              })
          }
        },
        error => {
          this.service.httpClientDelete("api/auth/ExpiredActivation/" + this.linksplit[0] + '/' + this.linksplit[1], '');
          this.router.navigate(["/forbidden"]);
        });
    } else {
      this.service.httpClientDelete("api/auth/ExpiredActivation/" + this.linksplit[0] + '/' + this.linksplit[1], '');
      this.router.navigate(["/forbidden"]);
    }
  }

  useraccesdata: any;
  Email: string = "";
  onSubmit() {
    this.changepasswordform.controls['password'].markAsTouched();
    this.changepasswordform.controls['rpassword'].markAsTouched();

    if (this.changepasswordform.valid) {
      this.loading = true;

      this.changepasswordform.value.currentpassword = this.curentpassword;

      let data = JSON.stringify(this.changepasswordform.value);
      
      /* autodesk plan 10 oct */

      this.service.httpCLientPut(this._serviceUrl + '/ChangePassword/'+this.id,data)
        .subscribe((data) => {
            let res = data;
            if (res['code'] == '1') {

              this.login();
            }
            else {
              swal({
                title: 'Information!',
                text: res['message'],
                type: 'error'
              }).catch(swal.noop);
              this.loading = false;
            }
        },
        error => {
          swal(
            'Authentication!',
            'Can\'t connect to server.',
            'error'
          );
          this.loading = false;
        });

      /* end line autodesk plan 10 oct */
    }
  }

  login() {
    var login_val_json =
    {
      Email: this.Email,
      Password: this.changepasswordform.value.password,
language:35
    }

    /* autodesk plan 10 oct */

    this.service.httpClientPost("api/auth/" + this.loginurl,login_val_json)
      .subscribe((data) => {
        let res = data;
        if (res['code'] == '1') {
          this.session.logIn(res["jwt"], JSON.stringify(res['user']));

          let useracces = this.session.getData();
          this.useraccesdata = JSON.parse(useracces);

          var level2 = this.useraccesdata.UserLevelId.split(',');

          if (level2[0] == "ADMIN" || level2[0] == "ORGANIZATION" || level2[0] == "DISTRIBUTOR" || level2[0] == "TRAINER" || level2[0] == "STUDENT") {
            this.router.navigate(['/dashboard-' + level2[0].toLowerCase() + '']);
            this.loading = false;
          } else if (level2[0] == "SITE") {
            this.router.navigate(['/dashboard-organization']);
            this.loading = false;
          } else if (level2[0] == "Instructor Read Only") {
            this.router.navigate(['/dashboard-trainer']);
            this.loading = false;
          } else if (level2[0] == "SUPERADMIN" || level2[0] == "APPROVER_1" || level2[0] == "APPROVER_2") {
            this.router.navigate(['/dashboard-admin']);
            this.loading = false;
          } else {
            this.router.navigate(['/no-role-page']);
            this.loading = false;
          }
        }
      },
      error => {
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */
  }
}
