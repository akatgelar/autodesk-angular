import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Under13Component } from './under13.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";



export const Under13Routes: Routes = [
  {
    path: '',
    component: Under13Component,
    data: {
      breadcrumb: 'Student Register',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(Under13Routes),
    SharedModule,
    Ng2CompleterModule
  ],
  declarations: [Under13Component],
  providers: [AppService, AppFormatDate]
})
export class Under13Module { }
