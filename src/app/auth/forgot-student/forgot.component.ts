import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";

import { TranslateService } from "@ngx-translate/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: [
    './forgot.component.css'
  ],
})
export class ForgotComponent implements OnInit {

  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  windowWidth: number;

  public loading = false;
  private _serviceUrl = 'api/Auth/ForgetPasswordStudent';
  public data: any = null;
  forgetpasswordform: FormGroup;
  public useraccesdata: any;
  forgotform: string = "";
  forgetusernameform: FormGroup;
  public datacountry: any;
  validDateBirth: Boolean = true;

  public minDate: NgbDateStruct;
  public maxDate: NgbDateStruct;

  listlanguage: any;
  listlanguage1 = [];
  public ActiveLang: string = "";

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService, private formatdate: AppFormatDate, private translate: TranslateService, private http: HttpClient) {

    this.minDate = { year: new Date().getFullYear() - 68, month: new Date().getMonth() + 1, day: new Date().getDate() };
    this.maxDate = { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() };

    let Emails = new FormControl('', Validators.required);

    this.forgetpasswordform = new FormGroup({
      Email: Emails
    });

    let Firstname = new FormControl('', Validators.required);
    let Lastname = new FormControl('', Validators.required);
    let CountryID = new FormControl('', Validators.required);
    //let DateBirth = new FormControl('', Validators.required);

    this.forgetusernameform = new FormGroup({
      Firstname: Firstname,
      Lastname: Lastname,
      CountryID: CountryID,
     // DateBirth: DateBirth
    });

    // Languages
    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    /* autodesk plan 10 oct */

    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}","")
      .subscribe(result => {
        translate.langs = [];
        var resTemp: any;
        resTemp = result;
        if (resTemp.length != 0) {
          for (let i = 0; i < resTemp.length; i++) {
            if (this.listlanguage1.length == 0) {
              this.listlanguage1.push(resTemp[i]);
            } else {
              for (let k = 0; k < this.listlanguage1.length; k++) {
                var exist = false;
                if (resTemp[i].Key == this.listlanguage1[k].Key) {
                  exist = true;
                }
              }
              if (exist == false) {
                this.listlanguage1.push(resTemp[i]);
              }
            }
          }
        }
        translate.addLangs(this.listlanguage1.sort(compare));
      });

    /* end line autodesk plan 10 oct */

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }

  }

  ngOnInit() {
    localStorage.setItem("stop","false");
    var data = '';
    this.service.httpClientGet('api/Countries', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datacountry = '';
        }
        else {
          this.datacountry = result;
        }
      },
        error => {
          this.service.errorserver();
          this.datacountry = '';
        });
  }

  setLanguage(value) {
    this.translate.use(value);
    localStorage.setItem("language", value);
    this.ActiveLang = value;
    location.reload();
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onSubmitPassword() {
    this.forgetpasswordform.controls['Email'].markAsTouched();
    if (this.forgetpasswordform.valid) {
      this.loading = true;
      let data = JSON.stringify(this.forgetpasswordform.value);
      this.service.httpClientPost(this._serviceUrl, data)
        .subscribe(result => {
          this.loading = false;
          var resource = result;
          if (resource['code'] == '1') {
            this.service.openSuccessSwal(resource['message']);
          }
          else {
            swal(
              'Information!',
              resource['message'],
              'error'
            );
          }
        },
          error => {
            this.loading = false;
            this.service.errorserver();
          });
    }
  }

  onSubmitUsername() {
    this.forgetusernameform.controls['Firstname'].markAsTouched();
    this.forgetusernameform.controls['Lastname'].markAsTouched();
    this.forgetusernameform.controls['CountryID'].markAsTouched();
  //  this.forgetusernameform.controls['DateBirth'].markAsTouched();
  //  this.forgetusernameform.value.DateBirth = this.formatdate.dateCalendarToYMD(this.forgetusernameform.value.DateBirth);
    if (this.forgetusernameform.valid) {
      this.loading = true;
     
      var keyword : any = {
        Firstname: this.forgetusernameform.value.Firstname,
        Lastname: this.forgetusernameform.value.Lastname,
        CountryID: this.forgetusernameform.value.CountryID,
       
    };
    keyword = JSON.stringify(keyword);
      console.log("api/Student/where/{'Firstname':'" + this.forgetusernameform.value.Firstname +
        "','Lastname':'" + this.forgetusernameform.value.Lastname +
        "','CountryID':'" + this.forgetusernameform.value.CountryID + "'}")
        // "','DateBirth':'" + this.forgetusernameform.value.DateBirth + "'}")
         // "','DateBirth':'" + this.forgetusernameform.value.DateBirth + "'}", '')
      // this.service.httpClientPost("api/Student/where/{'Firstname':'" + this.forgetusernameform.value.Firstname +
      //   "','Lastname':'" + this.forgetusernameform.value.Lastname +
      //   "','CountryID':'" + this.forgetusernameform.value.CountryID + "'}", '')
      this.service.httpClientPost("api/Student/ForgotPwdStudent",keyword)
        .subscribe(result => {
          if (result == "Not Found") {
            this.service.notfound();
            this.data = '';
            this.loading = false;
          }
          else {
            this.data = result;
            if(this.data.length > 1){
              this.data = null;
              swal(
                'Information!',
                "There are multiple usernames based on the details provided. Please check with your training center for help.",
                'success'
              );
            }
            this.loading = false;
          }
        },
          error => {
            this.service.errorserver();
            this.data = '';
            this.loading = false;
          });
    }
  }

  radioBtn(value) {
    this.forgotform = value;
  }
}
