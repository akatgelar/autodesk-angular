import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentComponent } from './student.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';



export const StudentRoutes: Routes = [
  {
    path: '',
    component: StudentComponent,
    data: {
      breadcrumb: 'Student ',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(StudentRoutes),
    SharedModule
  ],
  declarations: [StudentComponent],
  providers: [AppService]
})
export class StudentModule { }
