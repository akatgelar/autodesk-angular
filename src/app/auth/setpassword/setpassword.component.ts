import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { PasswordValidators } from 'ngx-validators';
import { Observable } from "rxjs/Observable";
import { Http, Headers, Response } from "@angular/http";
import { SessionService } from '../../shared/service/session.service';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html'
})
export class SetPasswordComponent implements OnInit {

  private _serviceUrl = 'api/ContactAll';
  public data: any;
  changepasswordform: FormGroup;
  id: String = "";
  password: String = "";
  contactid: String = "";
  siteid: String = "";
  Status: String = "";
  public ActiveLang: string = "";

  public loading = false;

  constructor(public session: SessionService, private translate: TranslateService, private service: AppService, private _http: Http, private route: ActivatedRoute, private router: Router) {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.id = params['id'] || "";
      this.password = params['p'] || "";
      this.contactid = params['c'] || "";
      this.siteid = params['s'] || "";
      this.Status = params['Status'] || "";
    });

    let currentpassword = new FormControl(this.password, Validators.required);
    let password = new FormControl('', [Validators.required, CustomValidators.notEqualTo(currentpassword), Validators.minLength(6), Validators.compose([
      PasswordValidators.digitCharacterRule(1),
      PasswordValidators.specialCharacterRule(1)])]);
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({
      password: password,
      rpassword: rpassword,
      currentpassword: currentpassword
    });

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }

  }

  ngOnInit() {
    localStorage.setItem("stop","false");
    var url = "";
    if (this.Status == 'Contact') {
      url = 'api/MainContact/FirstLogin/';
    }
    else if(this.Status == 'Student'){
      url = 'api/MainContact/FirstLoginStudent/';
    }
    this.service.httpClientGet(url + this.id, "")
      .subscribe(result => {
        var tmp = result;
        if (tmp != null) {
          this.Email = tmp["EmailAddress"];
        }
      },
        error => {
          console.log("failed load email")
        })

  }

  onSubmit() {
    this.changepasswordform.controls['password'].markAsTouched();
    this.changepasswordform.controls['rpassword'].markAsTouched();
    this.changepasswordform.controls['currentpassword'].markAsTouched();

    if (this.changepasswordform.valid) {
      this.loading = true;
      let data = JSON.stringify(this.changepasswordform.value);
      if (this.Status == 'Contact') {
        if (this.contactid != "" && this.siteid != "") {

          // this.service.httpCLientPutPassword(this._serviceUrl + '/ChangePassword', this.id, data, '/manage/contact/detail-contact,' + this.contactid + ',' + this.siteid, '/set-password');
          // this.loading = false;

          var routingtrue = '/manage/contact/detail-contact,' + this.contactid + ',' + this.siteid;
          var routingtruearr = routingtrue.split(',');

          this.service.httpCLientPut(this._serviceUrl + '/ChangePassword/' + this.id, data)
            .subscribe(res => {
              var result = res;
              if (result['code'] == '1') {
                if (routingtruearr.length > 1) {
                  this.router.navigate([routingtruearr[0], routingtruearr[1], routingtruearr[2]]);
                  this.loading = false;
                } else {
                  var routelogoutrole = routingtrue.split('-');
                  this.router.navigate([routelogoutrole[0]], { queryParams: { id: this.id, role: routelogoutrole[1] } });
                  this.loading = false;
                }
              }
              else {
                console.log("failed");
                this.loading = false;
              }
            },
            error => {
              console.log("failed");
              this.loading = false;
            });

        } else {

          /* autodesk plan 10 oct */

          this.service.httpCLientPut(this._serviceUrl + '/ChangePassword/' + this.id, data)
            .subscribe(result => {
              var resultJson =result;
              if (resultJson['code'] == '1') {
                this.login();
              }
              else {
                swal({
                  title: 'Information!',
                  text: resultJson['message'],
                  type: 'error'
                }).catch(swal.noop);
                this.loading = false;
              }
            },
            error => {
              swal(
                'Authentication!',
                'Can\'t connect to server.',
                'error'
              );
              this.loading = false;
            });

          /* end line autodesk plan 10 oct */

        }
      }
      else if (this.Status == 'Student') {
        this.service.httpCLientPut('api/Student/ChangePassword/' + this.id, data)
          .subscribe(result => {
            var resultJson = result;
            if (resultJson['code'] == '1') {
              this.loginStudent();
            }
            else {
              swal({
                title: 'Information!',
                text: resultJson['message'],
                type: 'error'
              }).catch(swal.noop);
              this.loading = false;
            }
          },
          error => {
            swal(
              'Authentication!',
              'Can\'t connect to server.',
              'error'
            );
            this.loading = false;
          });

      }
     
      
    }
  }

  post(data): Observable<string> {
    return this._http.post(
      "api/auth/login",
      data,
      { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
    )
      .map((response: Response) => {
        return response.text();
      });
  }

  put(data): Observable<string> {
    return this._http.put(
      this._serviceUrl + '/ChangePassword/' + this.id,
      data,
      { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
    )
      .map((response: Response) => {
        return response.text();
      });
  }

  Email: string = "";
  useraccesdata: any;
  login() {
    var login_val_json =
    {
      Email: this.Email,
      Password: this.changepasswordform.value.password
    }

    /* autodesk plan 10 oct */

    this.service.httpClientPost("api/auth/login", login_val_json)
      .subscribe(result => {
        this.changepasswordform.reset();
        var resource = result;
        if (resource['code'] == '1') {
          this.session.logIn(resource["jwt"], JSON.stringify(resource['user']));

          let useracces = this.session.getData();
          this.useraccesdata = JSON.parse(useracces);

          var level2 = this.useraccesdata.UserLevelId.split(',');

          if (level2[0] == "ADMIN" || level2[0] == "ORGANIZATION" || level2[0] == "DISTRIBUTOR" || level2[0] == "TRAINER" || level2[0] == "STUDENT") {
            this.router.navigate(['/dashboard-' + level2[0].toLowerCase() + '']);
            this.loading = false;
          } else if (level2[0] == "SITE") {
            this.router.navigate(['/dashboard-organization']);
            this.loading = false;
          }else if (level2[0] == "Instructor Read Only") {
            this.router.navigate(['/dashboard-trainer']);
            this.loading = false;
          } else if (level2[0] == "SUPERADMIN" || level2[0] == "APPROVER_1" || level2[0] == "APPROVER_2") {
            this.router.navigate(['/dashboard-admin']);
            this.loading = false;
          } else {
            this.router.navigate(['/no-role-page']);
            this.loading = false;
          }
        }
      },
      error => {
        swal(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */

  }

  loginStudent() {
    var login_val_json =
      {
        Email: this.Email,
        Password: this.changepasswordform.value.password
      }

    /* autodesk plan 10 oct */

    this.service.httpClientPost("api/auth/LoginStudent", login_val_json)
      .subscribe(result => {
        this.changepasswordform.reset();
        var resource = result;
        if (resource['code'] == '1') {
          this.session.logIn(resource["jwt"], JSON.stringify(resource['user']));

          let useracces = this.session.getData();
          this.useraccesdata = JSON.parse(useracces);

          if (this.useraccesdata.UserLevelId == "STUDENT") {
            this.router.navigate(['/dashboard-student']);
            this.loading = false;
          } else {
            this.router.navigate(['/no-role-page']);
            this.loading = false;
          }
        }
      },
      error => {
        swal(
          'Authentication!',
          'Can\'t connect to server.',
          'error'
        );
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */

  }

}
