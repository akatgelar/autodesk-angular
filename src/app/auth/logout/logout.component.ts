// import { Component, OnInit, ViewEncapsulation } from '@angular/core';
// import { SessionService } from '../../shared/service/session.service';
// import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';
// import { HttpClient,HttpHeaders } from '@angular/common/http';
// import {Http, Headers, Response} from "@angular/http";
// import swal from 'sweetalert2';

// @Component({
//   selector: 'app-root',
//   template: '<router-outlet><spinner></spinner></router-outlet>'
// })
// export class LogoutComponent  {
//    constructor(public session:SessionService,  private router: Router, private _http: Http, private http: HttpClient)
//    // constructor()
//    {

//    		// this.session.logOut();
// 		// this.router.navigate(["/authentication/login/epdb"]);
// 		console.log("logout");
// 	}

// }

import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router, ActivatedRoute } from "@angular/router";
import { Http, Headers, Response } from "@angular/http";
import { SessionService } from "../../shared/service/session.service";

import { Observable } from "rxjs/Observable";
import { AppService } from "../../shared/service/app.service";

@Component({
  selector: "app-logout",
  templateUrl: "./logout.component.html"
})
export class LogoutComponent implements OnInit {
  id: String = "";
  role: String = "";
  useraccesdata: any;
  public loading = false;

  constructor(
    public session: SessionService,
    private router: Router,
    private route: ActivatedRoute,
    private service: AppService
  ) {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.id = params["id"] || "";
      this.role = params["role"] || "";
    });

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
  }

  ngOnInit() {
    localStorage.setItem("stop","false");
    this.loading = true;

    setTimeout(() => {
      if (this.useraccesdata != null) {
        this.service.httpClientPost("api/Auth/CountLogin/" + this.role + "/" + this.useraccesdata.UserId,"")
		.subscribe(result => {
				
				}, error => {
					this.service.errorserver();
				});
      } else {
        this.service.httpClientPost("api/Auth/CountLogin/" + this.role + "/" + this.id, "")
		.subscribe(result => {
				console.log("logout");
				}, error => {
					this.service.errorserver();
				});
      }
    }, 1000)

    setTimeout(() => {
      localStorage.removeItem("language");
      this.session.logOut();
    }, 2000)

    setTimeout(() => {
      if(this.useraccesdata != null) {
        if (this.useraccesdata.UserLevelId != "STUDENT") {
          this.router.navigate(["/login"]);
        }else{
          this.router.navigate(["/login-student"]);
        }
      }else{
        if(this.role != "student"){
          this.router.navigate(["/login"]);
        }else{
          this.router.navigate(["/login-student"]);
        }
      }
      
    }, 3000)

    if(localStorage.getItem("firstLoad") == "true"){
      localStorage.removeItem("firstLoad");
      location.reload();
    }

    setTimeout(() => {
      this.loading = false;
    }, 3500)
    
  }
}
