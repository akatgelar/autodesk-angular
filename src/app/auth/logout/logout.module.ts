import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutComponent } from './logout.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { SessionService } from '../../shared/service/session.service';   
import {AppService} from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
 
export const LogoutRoutes: Routes = [
  {
    path: '',
    component: LogoutComponent,
    data: {
      breadcrumb: 'Logout ',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LogoutRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [LogoutComponent],
  providers:[AppService]
})
export class LogoutModule { }
