import { Component, OnInit, ViewEncapsulation, Testability } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { PasswordValidators } from 'ngx-validators';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { state, style, transition, animate, trigger, AUTO_STYLE } from "@angular/animations";
import { TranslateService } from "@ngx-translate/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { elementsFromPoint } from '@swimlane/ngx-datatable/release/utils';

@Component({
  selector: 'app-studentregister',
  templateUrl: './studentregister.component.html',
  animations: [
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None
})

export class StudentRegisterComponent implements OnInit {

  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  windowWidth: number;

  public loading = false;

  private _serviceUrl = 'api/Auth/Register';
  registerstudent: FormGroup;
  public data: any;
  public datadetail: any;
  public datacountry: any;
  messageResult: string = '';
  messageError: string = '';
  // modelPopup1: NgbDateStruct = {year: new Date().getFullYear() - 26, month: new Date().getMonth(), day: new Date().getDate()}; 
  public minDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDateEGD: NgbDateStruct;
  language: any;

  listlanguage: any;
  listlanguage1 = [];
  public ActiveLang: string = "";
  public ifStudent = false;
  // public isExist = true;
  // public isExpired = true;
  public isFinal = false;
  protected courseId: string = "";
  private selectedCourseId: string;
  protected dataService1: RemoteData;
  public disableVal = false;
  public showSelected = false;
  CourseSelected: string = "";
  obj: any;

  modelPopup0: NgbDateStruct;
  modelPopup1: NgbDateStruct;

  validterm: boolean = true;

  constructor(private router: Router, private _http: Http,
    private service: AppService, private formatdate: AppFormatDate, private translate: TranslateService, private http: HttpClient, private completerService: CompleterService) {

    this.minDate = { year: new Date().getFullYear() - 68, month: new Date().getMonth() + 1, day: new Date().getDate() };
    this.maxDate = { year: new Date().getFullYear()-16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1 };
    this.minDateEGD = { year: new Date().getFullYear(), month: new Date().getMonth()+ 1, day: new Date().getDate() };
    this.dataService1 = completerService.remote(null, "Keyword", "Keyword");
    this.dataService1.urlFormater(term => {
      return "api/StudentsCourses/CheckCourse/" + term;
    });
    this.dataService1.dataField("results");

    //validation
    let FirstName = new FormControl('', Validators.required);
    let LastName = new FormControl('', Validators.required);
    let EmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let EmailAddress2 = new FormControl('', [CustomValidators.notEqualTo(EmailAddress)]);
    let Country = new FormControl('', Validators.required);
    let LanguageID = new FormControl('', Validators.required);
    let DateBirth = new FormControl('');
    let EGD = new FormControl('');
    let Term1 = new FormControl('');
    let Term2 = new FormControl('');

    this.registerstudent = new FormGroup({
      FirstName: FirstName,
      LastName: LastName,
      EmailAddress: EmailAddress,
      EmailAddress2: EmailAddress2,
      Country: Country,
      DateBirth: DateBirth,
      EGD: EGD,
      LanguageID: LanguageID,
      Term1:Term1,
      Term2:Term2
    });

    // pemilihan bahasa
    let scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    /* autodesk plan 10 oct */

    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}","")
      .subscribe(result => {
        translate.langs = [];
        var resTemp: any;
        resTemp = result;
        if (resTemp.length != 0) {
          for (let i = 0; i < resTemp.length; i++) {
            if (this.listlanguage1.length == 0) {
              this.listlanguage1.push(resTemp[i]);
            } else {
              for (let k = 0; k < this.listlanguage1.length; k++) {
                var exist = false;
                if (resTemp[i].Key == this.listlanguage1[k].Key) {
                  exist = true;
                }
              }
              if (exist == false) {
                this.listlanguage1.push(resTemp[i]);
              }
            }
          }
        }
        translate.addLangs(this.listlanguage1.sort(compare));
      });

    /* end line autodesk plan 10 oct */

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }
  }

  ngOnInit() {
    localStorage.setItem("stop","false");
    if (!(localStorage.getItem("filter") === null)) {
      var item = JSON.parse(localStorage.getItem("filter"));
      this.registerstudent.patchValue({ FirstName: item.FirstName });
      this.registerstudent.patchValue({ LastName: item.LastName });
      this.registerstudent.patchValue({ EmailAddress: item.EmailAddress });
      this.registerstudent.patchValue({ EmailAddress2: item.EmailAddress2 });

      if (item.Country == undefined) {
        this.registerstudent.value.Country = '';
      }
      else {
        this.registerstudent.patchValue({ Country: item.Country });
      }

      if (item.LanguageID == undefined) {
        this.registerstudent.value.LanguageID = '';
      }
      else {
        this.registerstudent.patchValue({ LanguageID: item.LanguageID });
      }

      if (item.EGD != undefined) {
        this.modelPopup0 = {
          "year": item.EGD.year,
          "month": item.EGD.month,
          "day": item.EGD.day
        };
        this.registerstudent.patchValue({ EGD: this.modelPopup0 });
      }

      if (item.DateBirth != undefined) {
        this.modelPopup1 = {
          "year": item.DateBirth.year,
          "month": item.DateBirth.month,
          "day": item.DateBirth.day
        };
        this.registerstudent.patchValue({ DateBirth: this.modelPopup1 });
      }

     // this.checkEmailDuplicate(item.EmailAddress,16);

    }

    var data = '';
    this.service.httpClientGet('api/Countries', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datacountry = '';
        }
        else {
          this.datacountry = result;
        }
      },
        error => {
          this.service.errorserver();
          this.datacountry = '';
        });

    var language = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", language)
      .subscribe(result => {
        this.language = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  // pemilihan bahasa
  setLanguage(value) {
    this.translate.use(value);
    localStorage.setItem("language", value);
    this.ActiveLang = value;
    location.reload();
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  validDateBirth: Boolean = true;
  validEGD: Boolean = true;
  date_birth: string = "";
  onSubmit() {
    let format = /[!$%^&*+\-=\[\]{};:\\|.<>\/?]/
    if(format.test(this.registerstudent.value.FirstName) || format.test(this.registerstudent.value.LastName))
        return swal('ERROR','Special character not allowed in CourseTitle','error')
    if (this.registerstudent.value.EGD == undefined) {
      this.registerstudent.value.EGD = "";//(this.ifStudent) ? { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() } : { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() };
      if (this.ifStudent) {
        this.validEGD = false;
      }
    } else {
      this.registerstudent.value.EGD = (this.ifStudent) ? this.registerstudent.value.EGD : { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() };
      this.validEGD = true;
    }
    if (this.registerstudent.value.EGD != "") {
      this.registerstudent.value.EGD = this.formatdate.dateCalendarToYMD(this.registerstudent.value.EGD);
    }
    

    if (this.registerstudent.value.DateBirth == undefined) {
      this.registerstudent.value.DateBirth ="";//(this.ifStudent) ? { year: new Date().getFullYear(), month: new Date().getMonth(), day: new Date().getDate() } : { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1 };
    } else {
      this.registerstudent.value.DateBirth = (this.ifStudent) ? this.registerstudent.value.DateBirth : { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1 };
    }
    if (this.registerstudent.value.DateBirth != "") {
      this.date_birth = this.formatdate.dateCalendarToYMD(this.registerstudent.value.DateBirth);
    }
    if (this.ifStudent) { this.registerstudent.value.UserLevelId = 'Student';}
    else{this.registerstudent.value.UserLevelId = 'Professional';}

    if (this.validTerm) {
      this.loading = true;
     
      this.registerstudent.value.Email = this.registerstudent.value.EmailAddress;
      this.registerstudent.value.Country = this.registerstudent.value.Country;
      this.registerstudent.value.DateBirth = this.date_birth;
      this.registerstudent.value.TermAndCondition = "1";
      this.registerstudent.value.FirstName = this.service.encoder(this.registerstudent.value.FirstName);
      this.registerstudent.value.LastName =this.service.encoder(this.registerstudent.value.LastName);
      //convert object to json
      let data = JSON.stringify(this.registerstudent.value);
      
      this.postRegister(data);
      this.localStorage();
    }else{
      swal(
        // 'Warning!',
        // 'Please accept term and condition if you want register!',
        // 'error'
        'Warning!',
        'By selecting NO to receive survey requests, you will not have the ability to complete a survey or receive a certificate of course completion or certificate of event participation. Students must complete and submit their survey before receiving a certificate.',
        'warning'
      )
    }
  }

  localStorage() {
    //Buat object untuk filter yang dipilih
    var params =
    {
      FirstName:this.registerstudent.value.FirstName,
      LastName: this.registerstudent.value.LastName,
      EmailAddress: this.registerstudent.value.EmailAddress,
      EmailAddress2: this.registerstudent.value.EmailAddress2,
      Country: this.registerstudent.value.Country,
      LanguageID: this.registerstudent.value.LanguageID,
      EGD: this.modelPopup0,
      DateBirth: this.modelPopup1
    }

    //Masukan object filter ke local storage
    localStorage.setItem("filter", JSON.stringify(params));
  }

  onChange(isChecked: boolean) {
    this.ifStudent = (isChecked) ? true : false;

    if (this.ifStudent) {
      //validation
      let FirstName = new FormControl(this.registerstudent.value.FirstName, Validators.required);
      let LastName = new FormControl(this.registerstudent.value.LastName, Validators.required);
      let EmailAddress = new FormControl(this.registerstudent.value.EmailAddress, [Validators.required, Validators.email]);
      let EmailAddress2 = new FormControl(this.registerstudent.value.EmailAddress2, [CustomValidators.notEqualTo(EmailAddress)]);
      let Country = new FormControl(this.registerstudent.value.Country, Validators.required);
      let LanguageID = new FormControl(this.registerstudent.value.LanguageID, Validators.required);
      let DateBirth = new FormControl(this.registerstudent.value.DateBirth, Validators.required);
      let EGD = new FormControl(this.registerstudent.value.EGD, Validators.required);
      let Term1 = new FormControl(this.registerstudent.value.Term1);
      let Term2 = new FormControl(this.registerstudent.value.Term2);

      this.registerstudent = new FormGroup({
        FirstName: FirstName,
        LastName: LastName,
        EmailAddress: EmailAddress,
        EmailAddress2: EmailAddress2,
        Country: Country,
        DateBirth: DateBirth,
        EGD: EGD,
        LanguageID: LanguageID,
        Term1:Term1,
        Term2:Term2
      });
    } else {
      //validation
      let FirstName = new FormControl(this.registerstudent.value.FirstName, Validators.required);
      let LastName = new FormControl(this.registerstudent.value.LastName, Validators.required);
      let EmailAddress = new FormControl(this.registerstudent.value.EmailAddress, [Validators.required, Validators.email]);
      let EmailAddress2 = new FormControl(this.registerstudent.value.EmailAddress2, [CustomValidators.notEqualTo(EmailAddress)]);
      let Country = new FormControl(this.registerstudent.value.Country, Validators.required);
      let LanguageID = new FormControl(this.registerstudent.value.LanguageID, Validators.required);
      let Term1 = new FormControl(this.registerstudent.value.Term1);
      let Term2 = new FormControl(this.registerstudent.value.Term2);

      this.registerstudent = new FormGroup({
        FirstName: FirstName,
        LastName: LastName,
        EmailAddress: EmailAddress,
        EmailAddress2: EmailAddress2,
        Country: Country,
        LanguageID: LanguageID,
        Term1:Term1,
        Term2:Term2
      });
    }
  }

  post(data): Observable<string> {
    return this._http.post(
      this._serviceUrl,
      data,
      { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
    )
      .map((response: Response) => {
        return response.text();
      });
  }

  //check email registered
  validemail: Boolean = true;
  public emaildata: any;
  public loading_1 = false;
  checkEmailDuplicate(value,event) {
	  const checkEmailUrl = "api/Auth/EmailExist/?studentEmail=";
    if (value.includes("@") && (event.keyCode < 37 || event.keyCode > 40)) {
      this.loading_1 = true;
      this.service.httpClientGet(checkEmailUrl + value, '')
        .subscribe(result => {
          this.emaildata = result;
          if (this.emaildata.isDuplicate == true) {
            this.validemail = false;
          }
          else if (this.emaildata.isDuplicate == false) {
            this.validemail = true;
          }
          this.loading_1 = false;
        },
        error => {
          this.service.errorserver();
          this.loading_1 = false;
        });
    }
  }

  infoStudent() {
    swal(
      'Information!',
      '"Student" means an individual person, 13 years or older, enrolled as a student at a Qualified Educational Institution.',
      'info'
    )
  }

  /* autodesk plan 10 oct */

  postRegister(data) {
    this.service.httpClientPost(this._serviceUrl,data)
      .subscribe(result => {
        let tmpData : any = result;
        this.messageResult = tmpData;
        var resource = result;
        this.loading = false;
        if (resource['code'] == '1') {
          swal(
            'Information!',
            resource['message'],
            'success'
          );
          this.router.navigate(['/login-student']);
        }
        else {
          swal(
            'Information!',
            resource['message'],
            'error'
          );
        }
      },
      error => {
        this.loading = false;
        //console.log("failed");
      });
  }

  /* end line autodesk plan 10 oct */

  validTerm:boolean=false;
  term1value:string="";
  term2value:string="";
  onChangeTerm1(value){
    if(value == "0"){
      swal(
        'Warning!',
        'By selecting NO to receive survey requests, you will not have the ability to complete a survey or receive a certificate of course completion or certificate of event participation. Students must complete and submit their survey before receiving a certificate.',
        'warning'
      )
    }
    this.term1value = value;

    if(this.term1value == "1" && this.term2value == "1"){
      this.validTerm = true;
    }else{
      this.validTerm = false;
    }
  }

  onChangeTerm2(value){
    this.term2value = value;

    if(this.term1value == "1" && this.term2value == "1"){
      this.validTerm = true;
    }else{
      this.validTerm = false;
    }
  }

  hideStage(stage){
    let format = /[!$%^&*+\-=\[\]{};:\\|.<>\/?]/
    if(format.test(this.registerstudent.value.FirstName) || format.test(this.registerstudent.value.LastName))
        return swal('ERROR','Special character not allowed in Name','error')
    if(this.ifStudent){
      this.registerstudent.controls['FirstName'].markAsTouched();
      this.registerstudent.controls['LastName'].markAsTouched();
      this.registerstudent.controls['EmailAddress'].markAsTouched();
      this.registerstudent.controls['EmailAddress2'].markAsTouched();
      this.registerstudent.controls['Country'].markAsTouched();
      this.registerstudent.controls['LanguageID'].markAsTouched();
      this.registerstudent.controls['EGD'].markAsTouched();
      this.registerstudent.controls['DateBirth'].markAsTouched();
    }else{
      this.registerstudent.controls['FirstName'].markAsTouched();
      this.registerstudent.controls['LastName'].markAsTouched();
      this.registerstudent.controls['EmailAddress'].markAsTouched();
      this.registerstudent.controls['EmailAddress2'].markAsTouched();
      this.registerstudent.controls['Country'].markAsTouched();
      this.registerstudent.controls['LanguageID'].markAsTouched();
    }

    if(this.registerstudent.valid && this.validemail){
      $('#stage'+stage).removeClass('active');
      $('#stage-'+stage+'-step').addClass('done');
    }else{
      swal(
        'Field is Required!',
        'Please fill all register information form!',
        'error'
      )
    }
  }

  showStage(stage) {
    let format = /[!$%^&*+\-=\[\]{};:\\|.<>\/?]/
    if(format.test(this.registerstudent.value.FirstName) || format.test(this.registerstudent.value.LastName))
        return swal('ERROR','Special character not allowed in Name','error')
    if(this.ifStudent){
      this.registerstudent.controls['FirstName'].markAsTouched();
      this.registerstudent.controls['LastName'].markAsTouched();
      this.registerstudent.controls['EmailAddress'].markAsTouched();
      this.registerstudent.controls['EmailAddress2'].markAsTouched();
      this.registerstudent.controls['Country'].markAsTouched();
      this.registerstudent.controls['LanguageID'].markAsTouched();
      this.registerstudent.controls['EGD'].markAsTouched();
      this.registerstudent.controls['DateBirth'].markAsTouched();
    }else{
      this.registerstudent.controls['FirstName'].markAsTouched();
      this.registerstudent.controls['LastName'].markAsTouched();
      this.registerstudent.controls['EmailAddress'].markAsTouched();
      this.registerstudent.controls['EmailAddress2'].markAsTouched();
      this.registerstudent.controls['Country'].markAsTouched();
      this.registerstudent.controls['LanguageID'].markAsTouched();
    }

    if(this.registerstudent.valid && this.validemail){
      $('#stage'+stage).addClass('active');
      $('#stage-'+stage+'-step').addClass('active');
    }else{
      swal(
        'Field is Required!',
        'Please fill all register information form!',
        'error'
      )
    }
  }

}
