import { Routes } from '@angular/router';

import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
// import { AdminLayoutEIMComponent } from './layouts/admin-eim/admin-layout.component';
// import { AdminLayoutEVAComponent } from './layouts/admin-eva/admin-layout.component';
// import { AdminLayoutEPDBComponent } from './layouts/admin-epdb/admin-layout.component';
// import { AdminLayoutAutodeskAdminComponent } from './layouts/admin-autodesk-admin/admin-layout.component';
// import { AdminLayoutAutodeskUserComponent } from './layouts/admin-autodesk-user/admin-layout.component';
// import { AdminLayoutDistributorComponent } from './layouts/admin-distributor/admin-layout.component';
// import { AdminLayoutPartnerAdminComponent } from './layouts/admin-partner-admin/admin-layout.component';
// import { AdminLayoutPartnerUserComponent } from './layouts/admin-partner-user/admin-layout.component';
// import { AdminLayoutStudentComponent } from './layouts/admin-student/admin-layout.component';
import { AdminLayoutNewComponent } from './layouts/admin-new/admin-layout.component';
import {LocatorComponent} from './locator/locator.component';
import { DataManagementComponent } from './data-management/data-management.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'student',
    redirectTo: 'student/login',
    pathMatch: 'full'
  },
  {
     path: 'locator',
     canActivate: [LocatorComponent],
     component: LocatorComponent,
     data: {
       externalUrl: 'https://refeducation.autodesk.com/locator'
     }
 },
 
  {
    path: 'dashboard',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'dashboard-admin',
        loadChildren: './dashboard/admin/dashboard-autodesk-admin.module#DashboardAutodeskAdminModule'
      },
      {
        path: 'dashboard-distributor',
        loadChildren: './dashboard/distributor/dashboard-distributor.module#DashboardDistributorModule'
      },
      {
        path: 'dashboard-organization',
        loadChildren: './dashboard/organization/dashboard-epdb.module#DashboardEPDBModule'
      },
      // {
      //   path: 'dashboard-site',
      //   loadChildren: './dashboard/site/dashboard-site.module#DashboardSiteModule'
      // },
      {
        path: 'dashboard-trainer',
        loadChildren: './dashboard/trainer/dashboard-eva.module#DashboardEVAModule'
      },
      {
        path: 'dashboard-student',
        loadChildren: './dashboard/student/dashboard-student.module#DashboardStudentModule'
      },
    ]
  },
  
  // Dashboard

  // {
  //   path: 'dashboard',
  //   component: AdminLayoutNewComponent,
  //   loadChildren: './dashboard/admin/dashboard-autodesk-admin.module#DashboardAutodeskAdminModule'
  // },

  {
    path: 'dashboard-admin',
    component: AdminLayoutNewComponent,
    loadChildren: './dashboard/admin/dashboard-autodesk-admin.module#DashboardAutodeskAdminModule'
  },
  {
    path: 'dashboard-distributor',
    component: AdminLayoutNewComponent,
    loadChildren: './dashboard/distributor/dashboard-distributor.module#DashboardDistributorModule'
  },
  {
    path: 'dashboard-organization',
    component: AdminLayoutNewComponent,
    loadChildren: './dashboard/organization/dashboard-epdb.module#DashboardEPDBModule'
  },
  {
    path: 'dashboard-trainer',
    component: AdminLayoutNewComponent,
    loadChildren: './dashboard/trainer/dashboard-eva.module#DashboardEVAModule'
  },
  {
    path: 'dashboard-student',
    component: AdminLayoutNewComponent,
    loadChildren: './dashboard/student/dashboard-student.module#DashboardStudentModule'
  },

  // Activites
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'activities/activities-list',
        loadChildren: './epdb/activities/activities-list/activities-list-epdb.module#ActivitiesListEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'activities/activities-add',
        loadChildren: './epdb/activities/activities-add/activities-add-epdb.module#ActivitiesAddEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'activities/activities-update/:id',
        loadChildren: './epdb/activities/activities-update/activities-update-epdb.module#ActivitiesUpdateEPDBModule',
      }
    ]
  },

  // Product
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'products/list-category',
        loadChildren: './epdb/products/products-category-list/products-category-list-epdb.module#ProductsCategoryListEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'products/add-category',
        loadChildren: './epdb/products/products-category-add/products-category-add-epdb.module#ProductsCategoryAddEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'products/products-category-update/:id',
        loadChildren: './epdb/products/products-category-update/products-category-update-epdb.module#ProductsCategoryUpdateEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'products/products-list',
        loadChildren: './epdb/products/products-list/products-list-epdb.module#ProductsListEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'products/products-add',
        loadChildren: './epdb/products/products-add/products-add-epdb.module#ProductsAddEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'products/products-update/:id/:version',
        loadChildren: './epdb/products/products-update/products-update-epdb.module#ProductsUpdateEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'sku/add-sku',
		    loadChildren: './epdb/sku/master-sku-add/master-sku-add.module#MasterSkuAddModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'sku/list-sku',
        loadChildren: './epdb/sku/sku-list/sku-list-epdb.module#ListSKUEPDBModule'
		//loadChildren: './epdb/sku/master-sku-add/master-sku-add.module#MasterSkuAddModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'sku/update-sku/:id',
        loadChildren: './epdb/sku/sku-update/sku-update-epdb.module#UpdateSKUEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
		  
        path: 'sku/pricing-sku-add/:id',
        loadChildren: './epdb/sku/sku-add-pricing/sku-add-epdb.module#AddSKUEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'sku/master-sku-edit/:id',
        loadChildren: './epdb/sku/master-sku-edit/master-sku-edit.module#MasterSkuEditModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'glossary/glossary-list',
        loadChildren: './epdb/glosarry/glosarry-list/glosarry-list-epdb.module#GlosarryListEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'glossary/glossary-add',
        loadChildren: './epdb/glosarry/glosarry-add/glosarry-add-epdb.module#GlosarryAddEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'glossary/glossary-update/:id',
        loadChildren: './epdb/glosarry/glosarry-update/glosarry-update-epdb.module#GlosarryUpdateEPDBModule',
      }
    ]
  },

  // Country
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/geo-list',
        loadChildren: './epdb/country/geo-list/geo-list-epdb.module#GeoListEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/geo-add',
        loadChildren: './epdb/country/geo-add/geo-add-epdb.module#GeoAddEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/geo-update/:id',
        loadChildren: './epdb/country/geo-update/geo-update-epdb.module#GeoUpdateEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/territory-list',
        loadChildren: './epdb/country/territory-list/territory-list-epdb.module#TerritoryListEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/territory-add',
        loadChildren: './epdb/country/territory-add/territory-add-epdb.module#TerritoryAddEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/territory-update/:id',
        loadChildren: './epdb/country/territory-update/territory-update-epdb.module#TerritoryUpdateEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/region-list',
        loadChildren: './epdb/country/region-list/region-list-epdb.module#RegionListEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/region-add',
        loadChildren: './epdb/country/region-add/region-add-epdb.module#RegionAddEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/region-update/:id',
        loadChildren: './epdb/country/region-update/region-update-epdb.module#RegionUpdateEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/sub-region-list',
        loadChildren: './epdb/country/sub-region-list/sub-region-list-epdb.module#SubRegionListEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/sub-region-add',
        loadChildren: './epdb/country/sub-region-add/sub-region-add-epdb.module#SubRegionAddEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/sub-region-update/:id',
        loadChildren: './epdb/country/sub-region-update/sub-region-update-epdb.module#SubRegionUpdateEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/countries-list',
        loadChildren: './epdb/country/countries-list/countries-list-epdb.module#CountriesListEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/countries-add',
        loadChildren: './epdb/country/countries-add/countries-add-epdb.module#CountriesAddEPDBModule',
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'country/countries-update/:id',
        loadChildren: './epdb/country/countries-update/countries-update-epdb.module#CountriesUpdateEPDBModule',
      }
    ]
  },

  // Market Type
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'market-type/market-type-list',
        loadChildren: './epdb/market-type/market-type-list/market-type-list-epdb.module#MarketTypeListEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'market-type/market-type-add',
        loadChildren: './epdb/market-type/market-type-add/market-type-add-epdb.module#MarketTypeAddEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'market-type/market-type-update/:id',
        loadChildren: './epdb/market-type/market-type-update/market-type-update-epdb.module#MarketTypeUpdateEPDBModule',
      }
    ]
  },

  // Variables
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'variables/variables-list',
        loadChildren: './epdb/variables/variables-list/variables-list-epdb.module#VariablesListEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'variables/variables-add',
        loadChildren: './epdb/variables/variables-add/variables-add-epdb.module#VariablesAddEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'variables/variables-update/:id/:category',
        loadChildren: './epdb/variables/variables-update/variables-update-epdb.module#VariablesUpdateEPDBModule',
      }
    ]
  },

  // Partner Type
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'partner-type/list-sub-partner-type',
        loadChildren: './epdb/partner-type/list-sub-partner-type/list-sub-partner-type.module#ListSubPartnertTypeEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'partner-type/add-sub-partner-type',
        loadChildren: './epdb/partner-type/add-sub-partner-type/add-sub-partner-type.module#AddSubPartnertTypeEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'partner-type/update-sub-partner-type/:id',
        loadChildren: './epdb/partner-type/update-sub-partner-type/update-sub-partner-type.module#UpdateSubPartnertTypeEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'partner-type/list-partner-type',
        loadChildren: './epdb/partner-type/list-partner-type/list-partner-type.module#ListPartnertTypeEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'partner-type/add-partner-type',
        loadChildren: './epdb/partner-type/add-partner-type/add-partner-type.module#AddPartnertTypeEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'partner-type/update-partner-type/:id',
        loadChildren: './epdb/partner-type/update-partner-type/update-partner-type.module#UpdatePartnertTypeEPDBModule'
      }
    ]
  },

  // Distributor
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'distributor/add-distributor',
        loadChildren: './epdb/distributor/add-distributor/add-distributor-epdb.module#AddDistributorEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'distributor/add-master-distributor',
        loadChildren: './epdb/distributor/add-master-distributor/add-master-distributor-epdb.module#AddMasterDistributorEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'distributor/add-contact-distributor',
        loadChildren: './epdb/distributor/add-contact-distributor/add-contact-distributor-epdb.module#AddContactDistributorEPDBModule'
      }
    ]
  },

  // Currency
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'currency',
        loadChildren: './epdb/currency/currency-epdb.module#CurrencyEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'currency/currency-conversion-edit/:id',
        loadChildren: './epdb/currency-conversion-edit/currency-conversion-edit-epdb.module#CurrencyConversionEditEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'currency/currency-edit/:id',
        loadChildren: './epdb/currency-edit/currency-edit-epdb.module#CurrencyEditEPDBModule'
      }
    ]
  },

  // Setup Language
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'language/list-language',
        loadChildren: './epdb/language/list-language/list-language-epdb.module#ListLanguageEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'language/setup-language/:language',
        loadChildren: './epdb/language/setup-language/setup-language-epdb.module#SetupLanguageEPDBModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'language/manage-label',
        loadChildren: './epdb/language/manage-label/manage-label-epdb.module#ManageLabelEPDBModule'
      }
    ]
  },

  // Email Body
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'email/list-email-body',
        loadChildren: './epdb/list-email-body/list-email-body.module#ListEmailBodyModule'
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'email/add-edit-email',
        loadChildren: './epdb/add-edit-email-body/add-edit-email-body.module#AddEditEmailModule'
      }
    ]
  },

  // {
  //   path: 'admin',
  //   component: AdminLayoutNewComponent,
  //   children: [
  //     {
  //       path: 'activities/activities-list',
  //       loadChildren: './epdb/activities/activities-list/activities-list-epdb.module#ActivitiesListEPDBModule',
  //     },
  //     {
  //       path: 'activities/activities-add',
  //       loadChildren: './epdb/activities/activities-add/activities-add-epdb.module#ActivitiesAddEPDBModule',
  //     },
  //     {
  //       path: 'activities/activities-detail/:id',
  //       loadChildren: './epdb/activities/activities-detail/activities-detail-epdb.module#ActivitiesDetailEPDBModule',
  //     },
  //     {
  //       path: 'activities/activities-update/:id',
  //       loadChildren: './epdb/activities/activities-update/activities-update-epdb.module#ActivitiesUpdateEPDBModule',
  //       data: {
  //         breadcrumb: 'Update Activities '
  //       }
  //     },

  //     {
  //       path: 'invoice-type/invoice-type-list',
  //       loadChildren: './epdb/invoice-type/invoice-type-list/invoice-type-list-epdb.module#InvoiceTypeListEPDBModule',
  //     },
  //     {
  //       path: 'invoice-type/invoice-type-add',
  //       loadChildren: './epdb/invoice-type/invoice-type-add/invoice-type-add-epdb.module#InvoiceTypeAddEPDBModule',
  //     },
  //     {
  //       path: 'invoice-type/invoice-type-update/:id',
  //       loadChildren: './epdb/invoice-type/invoice-type-update/invoice-type-update-epdb.module#InvoiceTypeUpdateEPDBModule',
  //     },

  //     {
  //       path: 'line-item-summaries/line-item-summaries-list',
  //       loadChildren: './epdb/line-item-summaries/line-item-summaries-list/line-item-summaries-list-epdb.module#LineItemSummariesListEPDBModule',
  //     },
  //     {
  //       path: 'line-item-summaries/line-item-summaries-add',
  //       loadChildren: './epdb/line-item-summaries/line-item-summaries-add/line-item-summaries-add-epdb.module#LineItemSummariesAddEPDBModule',
  //     },
  //     {
  //       path: 'line-item-summaries/line-item-summaries-update/:id',
  //       loadChildren: './epdb/line-item-summaries/line-item-summaries-update/line-item-summaries-update-epdb.module#LineItemSummariesUpdateEPDBModule',
  //     },

  //     {
  //       path: 'products/list-category',
  //       loadChildren: './epdb/products/products-category-list/products-category-list-epdb.module#ProductsCategoryListEPDBModule',
  //     },
  //     {
  //       path: 'products/add-category',
  //       loadChildren: './epdb/products/products-category-add/products-category-add-epdb.module#ProductsCategoryAddEPDBModule',
  //     },
  //     {
  //       path: 'products/products-category-update/:id',
  //       loadChildren: './epdb/products/products-category-update/products-category-update-epdb.module#ProductsCategoryUpdateEPDBModule',
  //     },
  //     {
  //       path: 'products/products-list',
  //       loadChildren: './epdb/products/products-list/products-list-epdb.module#ProductsListEPDBModule',
  //     },
  //     {
  //       path: 'products/products-add',
  //       loadChildren: './epdb/products/products-add/products-add-epdb.module#ProductsAddEPDBModule',
  //     },
  //     {
  //       path: 'products/products-update/:id',
  //       loadChildren: './epdb/products/products-update/products-update-epdb.module#ProductsUpdateEPDBModule',
  //     },

  //     {
  //       path: 'sku/add-sku',
  //       loadChildren: './epdb/sku/sku-add/sku-add-epdb.module#AddSKUEPDBModule'
  //     },
  //     {
  //       path: 'sku/list-sku',
  //       loadChildren: './epdb/sku/sku-list/sku-list-epdb.module#ListSKUEPDBModule'
  //     },
  //     {
  //       path: 'sku/update-sku/:id',
  //       loadChildren: './epdb/sku/sku-update/sku-update-epdb.module#UpdateSKUEPDBModule'
  //     },



  //     {
  //       path: 'glossary/glossary-list',
  //       loadChildren: './epdb/glosarry/glosarry-list/glosarry-list-epdb.module#GlosarryListEPDBModule'
  //     },
  //     {
  //       path: 'glossary/glossary-add',
  //       loadChildren: './epdb/glosarry/glosarry-add/glosarry-add-epdb.module#GlosarryAddEPDBModule'
  //     },
  //     {
  //       path: 'glossary/glossary-update/:id',
  //       loadChildren: './epdb/glosarry/glosarry-update/glosarry-update-epdb.module#GlosarryUpdateEPDBModule',
  //     },
  //     {
  //       path: 'glossary/glossary-detail/:id',
  //       loadChildren: './epdb/glosarry/glosarry-detail/glosarry-detail-epdb.module#GlosarryDetailEPDBModule',
  //     },
  //     {
  //       path: 'glossary/glossary',
  //       loadChildren: './epdb/glossary/glossary-epdb.module#GlossaryEPDBModule',
  //     },

  //     {
  //       path: 'country/country-list',
  //       loadChildren: './epdb/country/country-list/country-epdb.module#CountryEPDBModule'
  //     },
  //     {
  //       path: 'country/geo-list',
  //       loadChildren: './epdb/country/geo-list/geo-list-epdb.module#GeoListEPDBModule'
  //     },
  //     {
  //       path: 'country/geo-add',
  //       loadChildren: './epdb/country/geo-add/geo-add-epdb.module#GeoAddEPDBModule'
  //     },
  //     {
  //       path: 'country/geo-update/:id',
  //       loadChildren: './epdb/country/geo-update/geo-update-epdb.module#GeoUpdateEPDBModule'
  //     },
  //     {
  //       path: 'country/geo-detail/:id',
  //       loadChildren: './epdb/country/geo-detail/geo-detail-epdb.module#GeoDetailEPDBModule'
  //     },
  //     {
  //       path: 'country/territory-list',
  //       loadChildren: './epdb/country/territory-list/territory-list-epdb.module#TerritoryListEPDBModule'
  //     },
  //     {
  //       path: 'country/territory-add',
  //       loadChildren: './epdb/country/territory-add/territory-add-epdb.module#TerritoryAddEPDBModule'
  //     },
  //     {
  //       path: 'country/territory-update/:id',
  //       loadChildren: './epdb/country/territory-update/territory-update-epdb.module#TerritoryUpdateEPDBModule'
  //     },
  //     {
  //       path: 'country/territory-detail/:id',
  //       loadChildren: './epdb/country/territory-detail/territory-detail-epdb.module#TerritoryDetailEPDBModule'
  //     },

  //     {
  //       path: 'country/region-list',
  //       loadChildren: './epdb/country/region-list/region-list-epdb.module#RegionListEPDBModule',
  //     },
  //     {
  //       path: 'country/region-add',
  //       loadChildren: './epdb/country/region-add/region-add-epdb.module#RegionAddEPDBModule',
  //     },
  //     {
  //       path: 'country/region-update/:id',
  //       loadChildren: './epdb/country/region-update/region-update-epdb.module#RegionUpdateEPDBModule',
  //     },
  //     {
  //       path: 'country/region-detail/:id',
  //       loadChildren: './epdb/country/region-detail/region-detail-epdb.module#RegionDetailEPDBModule',
  //     },
  //     {
  //       path: 'country/sub-region-list',
  //       loadChildren: './epdb/country/sub-region-list/sub-region-list-epdb.module#SubRegionListEPDBModule',
  //     },
  //     {
  //       path: 'country/sub-region-add',
  //       loadChildren: './epdb/country/sub-region-add/sub-region-add-epdb.module#SubRegionAddEPDBModule',
  //     },
  //     {
  //       path: 'country/sub-region-update/:id',
  //       loadChildren: './epdb/country/sub-region-update/sub-region-update-epdb.module#SubRegionUpdateEPDBModule',
  //     },
  //     {
  //       path: 'country/countries-list',
  //       loadChildren: './epdb/country/countries-list/countries-list-epdb.module#CountriesListEPDBModule',
  //     },
  //     {
  //       path: 'country/countries-add',
  //       loadChildren: './epdb/country/countries-add/countries-add-epdb.module#CountriesAddEPDBModule',
  //     },
  //     {
  //       path: 'country/countries-update/:id',
  //       loadChildren: './epdb/country/countries-update/countries-update-epdb.module#CountriesUpdateEPDBModule',
  //     },
  //     {
  //       path: 'country/countries-detail/:id',
  //       loadChildren: './epdb/country/countries-detail/countries-detail-epdb.module#CountriesDetailEPDBModule',
  //     },

  //     {
  //       path: 'territory/territory-list',
  //       loadChildren: './epdb/territory/territory-list/territory-list-epdb.module#TerritoryListEPDBModule'
  //     },
  //     {
  //       path: 'territory/territory-add',
  //       loadChildren: './epdb/territory/territory-add/territory-add-epdb.module#TerritoryAddEPDBModule'
  //     },
  //     {
  //       path: 'territory/territory-update',
  //       loadChildren: './epdb/territory/territory-update/territory-update-epdb.module#TerritoryUpdateEPDBModule',
  //     },


  //     {
  //       path: 'market-type/market-type-list',
  //       loadChildren: './epdb/market-type/market-type-list/market-type-list-epdb.module#MarketTypeListEPDBModule'
  //     },
  //     {
  //       path: 'market-type/market-type-add',
  //       loadChildren: './epdb/market-type/market-type-add/market-type-add-epdb.module#MarketTypeAddEPDBModule'
  //     },
  //     {
  //       path: 'market-type/market-type-update/:id',
  //       loadChildren: './epdb/market-type/market-type-update/market-type-update-epdb.module#MarketTypeUpdateEPDBModule',
  //     },

  //     {
  //       path: 'site-services/site-services-list',
  //       loadChildren: './epdb/site-services/site-services-list/site-services-list-epdb.module#SiteServicesListEPDBModule'
  //     },
  //     {
  //       path: 'site-services/site-services-add',
  //       loadChildren: './epdb/site-services/site-services-add/site-services-add-epdb.module#SiteServicesAddEPDBModule'
  //     },
  //     {
  //       path: 'site-services/site-services-update/:id',
  //       loadChildren: './epdb/site-services/site-services-update/site-services-update-epdb.module#SiteServicesUpdateEPDBModule',
  //     },
  //     {
  //       path: 'site-services/site-services-detail/:id',
  //       loadChildren: './epdb/site-services/site-services-detail/site-services-detail-epdb.module#SiteServicesDetailEPDBModule',
  //     },

  //     {
  //       path: 'variables/variables-list',
  //       loadChildren: './epdb/variables/variables-list/variables-list-epdb.module#VariablesListEPDBModule'
  //     },
  //     {
  //       path: 'variables/variables-add',
  //       loadChildren: './epdb/variables/variables-add/variables-add-epdb.module#VariablesAddEPDBModule'
  //     },
  //     {
  //       path: 'variables/variables-update/:id/:category',
  //       loadChildren: './epdb/variables/variables-update/variables-update-epdb.module#VariablesUpdateEPDBModule',
  //     },
  //     {
  //       path: 'variables/variables-detail/:id',
  //       loadChildren: './epdb/variables/variables-detail/variables-detail-epdb.module#VariablesDetailEPDBModule',
  //     },


  //     {
  //       path: 'partner-type/list-sub-partner-type',
  //       loadChildren: './epdb/partner-type/list-sub-partner-type/list-sub-partner-type.module#ListSubPartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/add-sub-partner-type',
  //       loadChildren: './epdb/partner-type/add-sub-partner-type/add-sub-partner-type.module#AddSubPartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/update-sub-partner-type/:id',
  //       loadChildren: './epdb/partner-type/update-sub-partner-type/update-sub-partner-type.module#UpdateSubPartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/list-partner-type',
  //       loadChildren: './epdb/partner-type/list-partner-type/list-partner-type.module#ListPartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/add-partner-type',
  //       loadChildren: './epdb/partner-type/add-partner-type/add-partner-type.module#AddPartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/update-partner-type/:id',
  //       loadChildren: './epdb/partner-type/update-partner-type/update-partner-type.module#UpdatePartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/detail-sub-partner-type/:id',
  //       loadChildren: './epdb/partner-type/detail-sub-partner-type/detail-sub-partner-type.module#DetailSubPartnertTypeEPDBModule'
  //     },
  //     {
  //       path: 'partner-type/detail-partner-type/:id',
  //       loadChildren: './epdb/partner-type/detail-partner-type/detail-partner-type.module#DetailPartnertTypeEPDBModule'
  //     },

  //     //distributor
  //     {
  //       path: 'distributor/list-distributor',
  //       loadChildren: './epdb/distributor/list-distributor/list-distributor-epdb.module#ListDistributorEPDBModule'
  //     },
  //     {
  //       path: 'distributor/add-distributor',
  //       loadChildren: './epdb/distributor/add-distributor/add-distributor-epdb.module#AddDistributorEPDBModule'
  //     },
  //     {
  //       path: 'distributor/add-master-distributor',
  //       loadChildren: './epdb/distributor/add-master-distributor/add-master-distributor-epdb.module#AddMasterDistributorEPDBModule'
  //     },
  //     {
  //       path: 'distributor/add-contact-distributor',
  //       loadChildren: './epdb/distributor/add-contact-distributor/add-contact-distributor-epdb.module#AddContactDistributorEPDBModule'
  //     },

  //     {
  //       path: 'setup-language/:language',
  //       loadChildren: './epdb/setup-language/setup-language-epdb.module#SetupLanguageEPDBModule'
  //     },
  //     {
  //       path: 'currency',
  //       loadChildren: './epdb/currency/currency-epdb.module#CurrencyEPDBModule'
  //     },
  //     {
  //       path: 'currency-conversion-edit/:id',
  //       loadChildren: './epdb/currency-conversion-edit/currency-conversion-edit-epdb.module#CurrencyConversionEditEPDBModule'
  //     },
  //     {
  //       path: 'currency-edit/:id',
  //       loadChildren: './epdb/currency-edit/currency-edit-epdb.module#CurrencyEditEPDBModule'
  //     },

  //     //aek tambah route list language
  //     {
  //       path: 'language/list-language',
  //       loadChildren: './epdb/language/list-language/list-language-epdb.module#ListLanguageEPDBModule'
  //     },
  //     {
  //       path: 'language/add-language',
  //       loadChildren: './epdb/language/add-language/add-language-epdb.module#AddLanguageEPDBModule'
  //     },
  //     {
  //       path: 'language/setup-language/:language',
  //       loadChildren: './epdb/language/setup-language/setup-language-epdb.module#SetupLanguageEPDBModule'
  //     },

  //     {
  //       path: 'email/list-email-body',
  //       loadChildren: './epdb/list-email-body/list-email-body.module#ListEmailBodyModule'
  //     },
  //     {
  //       path: 'email/add-edit-email',
  //       loadChildren: './epdb/add-edit-email-body/add-edit-email-body.module#AddEditEmailModule'
  //     }
  //   ]
  // },

  // Organization
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'organization/add-organization',
        loadChildren: './epdb/add-organization/add-organization-epdb.module#AddOrganizationEPDBModule'
      }
    ]
  },
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'organization/search-organization',
        loadChildren: './epdb/search-organization/search-organization-epdb.module#SearchOrganizationEPDBModule'
      }
    ]
  },
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'organization/detail-organization/:id',
        loadChildren: './epdb/detail-organization/detail-organization-epdb.module#DetailOrganizationEPDBModule'
      },
      {
        path: 'organization/edit-organization/:id/:orgId',
        loadChildren: './epdb/edit-organization/edit-organization-epdb.module#EditOrganizationEPDBModule'
      },
      {
        path: 'organization/add-invoice/:id',
        loadChildren: './epdb/add-invoice/add-invoice-epdb.module#AddInvoiceEPDBModule'
      },
      {
        path: 'organization/update-invoice/:id',
        loadChildren: './epdb/update-invoice/update-invoice-epdb.module#UpdateInvoiceEPDBModule'
      },
      {
        path: 'organization/add-academic-project/:id',
        loadChildren: './epdb/add-academic-project/add-academic-project-epdb.module#AddAcademicProjectEPDBModule'
      },
      {
        path: 'organization/add-orgjournalentries/:id',
        loadChildren: './epdb/add-orgjournalentries/add-orgjournalentries-epdb.module#AddOrgJournalEntriesEPDBModule'
      },
      {
        path: 'organization/edit-orgjournalentries/:id',
        loadChildren: './epdb/edit-orgjournalentries/edit-orgjournalentries-epdb.module#EditOrgJournalEntriesEPDBModule'
      },
      {
        path: 'organization/edit-orgattributes',
        loadChildren: './epdb/edit-orgattributes/edit-orgattributes-epdb.module#EditOrgAttributesEPDBModule'
      },
      {
        path: 'organization/edit-orgattachments',
        loadChildren: './epdb/edit-orgattachments/edit-orgattachments-epdb.module#EditOrgAttachmentsEPDBModule'
      },
      {
        path: 'organization/pdb-history',
        loadChildren: './epdb/pdb-history/pdb-history-epdb.module#PDBHistoryEPDBModule'
      },
      {
        path: 'organization/learning-history',
        loadChildren: './epdb/learning-history/learning-history-epdb.module#LearningHistoryEPDBModule'
      },
      {
        path: 'organization/certifications',
        loadChildren: './epdb/certifications/certifications-epdb.module#CertificationsEPDBModule'
      },
      {
        path: 'organization/specializations',
        loadChildren: './epdb/specializations/specializations-epdb.module#SpecializationsEPDBModule'
      },
    ]
  },

  // Site
  {
    path: '',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'agreement',
		    loadChildren: './epdb/agreement/agreement.module#AgreementModule'
      }
    ]
  },
  {
    path: '',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'sqlquery',
		    loadChildren: './epdb/sqlquery/sqlquery.module#SqlqueryModule'
      }
    ]
  },
  {
    path: '',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'merge',
		    loadChildren: './epdb/merge-function/merge-function.module#MergeFunctionModule'
      }
    ]
  },
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'site/search-site',
        loadChildren: './epdb/search-site/search-site-epdb.module#SearchSiteEPDBModule'
      }
    ]
  },
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'site/add-partner-type/:id',
        loadChildren: './epdb/add-partner-type/add-partner-type-epdb.module#AddPartnerTypeEPDBModule'
      },
      {
        path: 'site/update-partner-type/:id',
        loadChildren: './epdb/update-partner-type/update-partner-type-epdb.module#UpdatePartnerTypeEPDBModule'
      },
      {
        path: 'site/add-site/:id',
        loadChildren: './epdb/add-site/add-site-epdb.module#AddSiteEPDBModule'
      },
      {
        path: 'site/detail-site/:id',
        loadChildren: './epdb/detail-site/detail-site-epdb.module#DetailSiteEPDBModule'
      },
      {
        path: 'site/edit-site/:id',
        loadChildren: './epdb/edit-site/edit-site-epdb.module#EditSiteEPDBModule'
      },
      {
        path: 'site/search-siebel/:id',
        loadChildren: './epdb/search-siebel/search-siebel-epdb.module#SearchSiebelEPDBModule'
      },
      {
        path: 'site/add-site-accreditation',
        loadChildren: './epdb/add-site-accreditation/add-site-accreditation-epdb.module#AddSiteAccrEPDBModule'
      },
      {
        path: 'site/add-site-academic-programs/:id',
        loadChildren: './epdb/add-academic-programs/add-academic-programs-epdb.module#AddAcademicProgramsEPDBModule'
      },
      {
        path: 'site/edit-site-academic-programs/:id',
        loadChildren: './epdb/edit-academic-programs/edit-academic-programs-epdb.module#EditAcademicProgramsEPDBModule'
      },
      {
        path: 'site/update-site-accreditation',
        loadChildren: './epdb/update-site-accreditation/update-site-accreditation-epdb.module#UpdateSiteAccrEPDBModule'
      },
      {
        path: 'site/add-site-journal',
        loadChildren: './epdb/add-site-journal/add-site-journal-epdb.module#AddSiteJournalEPDBModule'
      },
      {
        path: 'site/update-site-journal',
        loadChildren: './epdb/update-site-journal/update-site-journal-epdb.module#UpdateSiteJournalEPDBModule'
      },
      {
        path: 'site/add-academic-project',
        loadChildren: './epdb/add-academic-project/add-academic-project-epdb.module#AddAcademicProjectEPDBModule'
      },
      {
        path: 'site/update-academic-project',
        loadChildren: './epdb/update-academic-project/update-academic-project-epdb.module#UpdateAcademicProjectEPDBModule'
      },

      /* issue 17102018 - Add crud site attribute on detail site */

      {
        path: 'site/add-site-attribute',
        loadChildren: './epdb/add-site-attribute/add-site-attribute-epdb.module#AddSiteAttributeEPDBModule'
      },
      {
        path: 'site/update-site-attribute',
        loadChildren: './epdb/update-site-attribute/update-site-attribute-epdb.module#UpdateSiteAttributeEPDBModule'
      },

      /* end line issue 17102018 - Add crud site attribute on detail site */

    ]
  },

  // Contact
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'contact/add-new-contact',
        loadChildren: './epdb/add-new-contact/add-new-contact-epdb.module#AddNewContactEPDBModule'
      }
    ]
  },
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'contact/search-contact',
        loadChildren: './epdb/search-contact/search-contact-epdb.module#SearchContactEPDBModule'
      }
    ]
  },
  {
    path: 'manage',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'contact/site-affiliations/:id/:siteid/:orgid',
        loadChildren: './epdb/site-affiliations/site-affiliations-epdb.module#SiteAffiliationsEPDBModule'
      },
      {
        path: 'contact/detail-contact/:id/:siteid',
        loadChildren: './epdb/detail-contact/detail-contact-epdb.module#DetailContactEPDBModule'
      },
      {
        path: 'contact/add-contact/:id',
        loadChildren: './epdb/add-contact/add-contact-epdb.module#AddContactEPDBModule'
      },
      {
        path: 'contact/add-contact',
        loadChildren: './epdb/add-contact/add-contact-epdb.module#AddContactEPDBModule'
      },
      {
        path: 'contact/edit-contact/:id/:siteId',
        loadChildren: './epdb/edit-contact/edit-contact-epdb.module#EditContactEPDBModule'
      },
      {
        path: 'search-general',
        loadChildren: './epdb/search-general/search-general-epdb.module#SearchGeneralEPDBModule',
      },
      {
        path: 'contact/add-contact-qualifications',
        loadChildren: './epdb/add-contact-qualifications/add-contact-qualifications-epdb.module#AddContactQualificationsEPDBModule',
      },
      {
        path: 'contact/edit-contact-qualifications/:id',
        loadChildren: './epdb/edit-contact-qualifications/edit-contact-qualifications-epdb.module#EditContactQualificationsEPDBModule',
      },
      {
        path: 'contact/add-contact-journal',
        loadChildren: './epdb/add-contact-journal/add-contact-journal-epdb.module#AddContactJournalEPDBModule',
      },
      {
        path: 'contact/edit-contact-journal/:id',
        loadChildren: './epdb/edit-contact-journal/edit-contact-journal-epdb.module#EditContactJournalEPDBModule',
      },

      /* */

      {
        path: 'contact/add-contact-attribute',
        loadChildren: './epdb/add-contact-attribute/add-contact-attribute-epdb.module#AddContactAttributeEPDBModule',
      },
      {
        path: 'contact/edit-contact-attribute/:id',
        loadChildren: './epdb/edit-contact-attribute/edit-contact-attribute-epdb.module#EditContactAttributeEPDBModule',
      },

      /* */

      // other
      {
        path: 'aci',
        loadChildren: './epdb/aci/aci-eim.module#ACIEIMModule'
      }
    ]
  },

  // {
  //   path: 'manage',
  //   component: AdminLayoutNewComponent,
  //   children: [
  //     // organization
  //     {
  //       path: 'organization/add-organization',
  //       loadChildren: './epdb/add-organization/add-organization-epdb.module#AddOrganizationEPDBModule'
  //     },
  //     {
  //       path: 'organization/search-organization',
  //       loadChildren: './epdb/search-organization/search-organization-epdb.module#SearchOrganizationEPDBModule'
  //     },
  //     {
  //       path: 'organization/detail-organization/:id',
  //       loadChildren: './epdb/detail-organization/detail-organization-epdb.module#DetailOrganizationEPDBModule'
  //     },
  //     {
  //       path: 'organization/edit-organization/:id',
  //       loadChildren: './epdb/edit-organization/edit-organization-epdb.module#EditOrganizationEPDBModule'
  //     },
  //     {
  //       path: 'organization/add-invoice/:id',
  //       loadChildren: './epdb/add-invoice/add-invoice-epdb.module#AddInvoiceEPDBModule'
  //     },
  //     {
  //       path: 'organization/update-invoice/:id',
  //       loadChildren: './epdb/update-invoice/update-invoice-epdb.module#UpdateInvoiceEPDBModule'
  //     },
  //     {
  //       path: 'organization/add-academic-project/:id',
  //       loadChildren: './epdb/add-academic-project/add-academic-project-epdb.module#AddAcademicProjectEPDBModule'
  //     },
  //     {
  //       path: 'organization/add-orgjournalentries/:id',
  //       loadChildren: './epdb/add-orgjournalentries/add-orgjournalentries-epdb.module#AddOrgJournalEntriesEPDBModule'
  //     },
  //     {
  //       path: 'organization/edit-orgjournalentries/:id',
  //       loadChildren: './epdb/edit-orgjournalentries/edit-orgjournalentries-epdb.module#EditOrgJournalEntriesEPDBModule'
  //     },
  //     {
  //       path: 'organization/edit-orgattributes',
  //       loadChildren: './epdb/edit-orgattributes/edit-orgattributes-epdb.module#EditOrgAttributesEPDBModule'
  //     },
  //     {
  //       path: 'organization/edit-orgattachments',
  //       loadChildren: './epdb/edit-orgattachments/edit-orgattachments-epdb.module#EditOrgAttachmentsEPDBModule'
  //     },
  //     {
  //       path: 'organization/pdb-history',
  //       loadChildren: './epdb/pdb-history/pdb-history-epdb.module#PDBHistoryEPDBModule'
  //     },
  //     {
  //       path: 'organization/learning-history',
  //       loadChildren: './epdb/learning-history/learning-history-epdb.module#LearningHistoryEPDBModule'
  //     },
  //     {
  //       path: 'organization/certifications',
  //       loadChildren: './epdb/certifications/certifications-epdb.module#CertificationsEPDBModule'
  //     },
  //     {
  //       path: 'organization/specializations',
  //       loadChildren: './epdb/specializations/specializations-epdb.module#SpecializationsEPDBModule'
  //     },

  //     // site
  //     {
  //       path: 'site/add-partner-type/:id',
  //       loadChildren: './epdb/add-partner-type/add-partner-type-epdb.module#AddPartnerTypeEPDBModule'
  //     }, {
  //       path: 'site/update-partner-type/:id',
  //       loadChildren: './epdb/update-partner-type/update-partner-type-epdb.module#UpdatePartnerTypeEPDBModule'
  //     },
  //     {
  //       path: 'site/add-site/:id',
  //       loadChildren: './epdb/add-site/add-site-epdb.module#AddSiteEPDBModule'
  //     },
  //     {
  //       path: 'site/detail-site/:id',
  //       loadChildren: './epdb/detail-site/detail-site-epdb.module#DetailSiteEPDBModule'
  //     },
  //     {
  //       path: 'site/search-site',
  //       loadChildren: './epdb/search-site/search-site-epdb.module#SearchSiteEPDBModule'
  //     },
  //     {
  //       path: 'site/edit-site/:id',
  //       loadChildren: './epdb/edit-site/edit-site-epdb.module#EditSiteEPDBModule'
  //     },
  //     {
  //       path: 'site/search-siebel/:id',
  //       loadChildren: './epdb/search-siebel/search-siebel-epdb.module#SearchSiebelEPDBModule'
  //     },
  //     {
  //       path: 'site/add-site-accreditation',
  //       loadChildren: './epdb/add-site-accreditation/add-site-accreditation-epdb.module#AddSiteAccrEPDBModule'
  //     },
  //     {
  //       path: 'site/add-site-academic-programs/:id',
  //       loadChildren: './epdb/add-academic-programs/add-academic-programs-epdb.module#AddAcademicProgramsEPDBModule'
  //     },
  //     {
  //       path: 'site/edit-site-academic-programs/:id',
  //       loadChildren: './epdb/edit-academic-programs/edit-academic-programs-epdb.module#EditAcademicProgramsEPDBModule'
  //     },
  //     {
  //       path: 'site/update-site-accreditation',
  //       loadChildren: './epdb/update-site-accreditation/update-site-accreditation-epdb.module#UpdateSiteAccrEPDBModule'
  //     },
  //     {
  //       path: 'site/add-site-journal',
  //       loadChildren: './epdb/add-site-journal/add-site-journal-epdb.module#AddSiteJournalEPDBModule'
  //     },
  //     {
  //       path: 'site/update-site-journal',
  //       loadChildren: './epdb/update-site-journal/update-site-journal-epdb.module#UpdateSiteJournalEPDBModule'
  //     },
  //     {
  //       path: 'site/add-academic-project',
  //       loadChildren: './epdb/add-academic-project/add-academic-project-epdb.module#AddAcademicProjectEPDBModule'
  //     },
  //     {
  //       path: 'site/update-academic-project',
  //       loadChildren: './epdb/update-academic-project/update-academic-project-epdb.module#UpdateAcademicProjectEPDBModule'
  //     },

  //     // contact
  //     {
  //       path: 'contact/site-affiliations/:id/:siteid/:orgid',
  //       loadChildren: './epdb/site-affiliations/site-affiliations-epdb.module#SiteAffiliationsEPDBModule'
  //     },
  //     {
  //       path: 'contact/detail-contact/:id/:siteid',
  //       loadChildren: './epdb/detail-contact/detail-contact-epdb.module#DetailContactEPDBModule'
  //     },
  //     {
  //       path: 'contact/add-contact/:id',
  //       loadChildren: './epdb/add-contact/add-contact-epdb.module#AddContactEPDBModule'
  //     },
  //     {
  //       path: 'contact/search-contact',
  //       loadChildren: './epdb/search-contact/search-contact-epdb.module#SearchContactEPDBModule'
  //     },
  //     {
  //       path: 'contact/add-contact',
  //       loadChildren: './epdb/add-contact/add-contact-epdb.module#AddContactEPDBModule'
  //     },
  //     {
  //       path: 'contact/add-new-contact',
  //       loadChildren: './epdb/add-new-contact/add-new-contact-epdb.module#AddNewContactEPDBModule'
  //     },
  //     {
  //       path: 'contact/edit-contact/:id/:siteId',
  //       loadChildren: './epdb/edit-contact/edit-contact-epdb.module#EditContactEPDBModule'
  //     },
  //     {
  //       path: 'contact/search-general',
  //       loadChildren: './epdb/search-general/search-general-epdb.module#SearchGeneralEPDBModule',
  //     },
  //     {
  //       path: 'contact/add-contact-qualifications',
  //       loadChildren: './epdb/add-contact-qualifications/add-contact-qualifications-epdb.module#AddContactQualificationsEPDBModule',
  //     },
  //     {
  //       path: 'contact/edit-contact-qualifications/:id',
  //       loadChildren: './epdb/edit-contact-qualifications/edit-contact-qualifications-epdb.module#EditContactQualificationsEPDBModule',
  //     },
  //     {
  //       path: 'contact/add-contact-journal',
  //       loadChildren: './epdb/add-contact-journal/add-contact-journal-epdb.module#AddContactJournalEPDBModule',
  //     },
  //     {
  //       path: 'contact/edit-contact-journal/:id',
  //       loadChildren: './epdb/edit-contact-journal/edit-contact-journal-epdb.module#EditContactJournalEPDBModule',
  //     },

  //     // other
  //     {
  //       path: 'aci',
  //       loadChildren: './epdb/aci/aci-eim.module#ACIEIMModule'
  //     },
  //   ]
  // },

  {
    path: 'evaluation',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'list-of-the-form',
        loadChildren: './eva/list-of-the-form/list-of-the-form-eva.module#ListOfTheFormEVAModule'
      },
      // {
      //   path: 'add-edit-form',
      //   loadChildren: './eva/survey-template-add/add-edit-form-eva.module#AddEditFormEVAModule'
      // },
      {
        path: 'add-edit-form/:id/:code',
        loadChildren: './eva/survey-template-add/add-edit-form-eva.module#AddEditFormEVAModule'
      },
      {
        path: 'survey-template-list',
        loadChildren: './eva/survey-template-list/survey-template-list-eva.module#SurveyTemplateListEVAModule'
      },
      {
        path: 'survey-template-copy',
        loadChildren: './eva/survey-template-copy/survey-template-copy-eva.module#SurveyTemplateCopyEVAModule'
      },
      {
        path: 'survey-template-copy-detail/:id',
        loadChildren: './eva/survey-template-copy-detail/survey-template-copy-detail-eva.module#SurveyTemplateCopyDetailEVAModule'
      },
      {
        path: 'survey-template-detail/:id',
        loadChildren: './eva/survey-template-detail/survey-template-detail-eva.module#SurveyTemplateDetailEVAModule'
      },
      {
        path: 'add-edit-templatedata',
        loadChildren: './eva/survey-templatedata-add/add-edit-templatedata-eva.module#AddEditTemplateDataEVAModule'
      },
      {
        path: 'update-templatedata/:id',
        loadChildren: './eva/survey-templatedata-update/update-templatedata-eva.module#UpdateTemplateDataEVAModule'
      },
      {
        path: 'survey-answer-list',
        loadChildren: './eva/survey-answer-list/survey-answer-list-eva.module#SurveyAnswerListEVAModule'
      },
      {
        path: 'survey-answer-add',
        loadChildren: './eva/survey-answer-add/survey-answer-add-eva.module#SurveyAnswerAddEVAModule'
      },
      {
        path: 'survey-answer-add/:id',
        loadChildren: './eva/survey-answer-add/survey-answer-add-eva.module#SurveyAnswerAddEVAModule'
      },
    ]
  },

  {
    path: 'course',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'search-courses',
        loadChildren: './eva/search-courses/search-courses-eva.module#SearchCourseEVAModule'
      },
      {
        path: 'add-course',
        loadChildren: './eva/add-course/add-course-eva.module#AddCourseEVAModule'
      },
      {
        path: 'course-list',
        loadChildren: './eva/course-list/course-list-eva.module#CourseListEVAModule',
      },
      {
        path: 'course-add',
        loadChildren: './eva/course-add/course-add-eva.module#CourseAddEVAModule',
      },
      {
        path: 'course-update/:id/:evals/:partner_type',
        loadChildren: './eva/course-update/course-update-eva.module#CourseUpdateEVAModule',
      },
      {
        path: 'course-copy/:id',
        loadChildren: './eva/course-copy/course-copy-eva.module#CourseCopyEVAModule',
      },
      {
        path: 'confirm-final-student/:id',
        loadChildren: './eva/confirm-final-student/confirm-final-student-eva.module#ConfirmFinalStudentEVAModule',
      },
      {
        path: 'evaluation-student/:evalsId/:courseId/:evalsNumber',
        loadChildren: './eva/course-evaluation-student/course-evaluation-student.module#CourseEvaluationStudent',
      },
      {
        path: 'detail-evaluation/:evalsId/:courseId/:evalsNumber',
        loadChildren: './eva/detail-evaluation/detail-evaluation.module#DetailEvaluationModule',
      }
    ]
  },

  {
    path: 'studentinfo',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'search-student',
        loadChildren: './eva/search-student/search-student-eva.module#SearchStudentEVAModule'
      },
      {
        path: 'terminate-student',
        loadChildren: './eva/terminate-student/terminate-student-eva.module#TerminateStudentEVAModule'
      },
      {
        path: 'update-student-information/:id',
        loadChildren: './eva/update-student-information/update-student-information-eva.module#UpdateStudentInformationEVAModule'
      },
      {
        path: 'student-list',
        loadChildren: './eva/student-list/student-list-eva.module#StudentListEVAModule'
      },
      {
        path: 'confirm-final-student/:id',
        loadChildren: './eva/confirm-final-student/confirm-final-student-eva.module#ConfirmFinalStudentEVAModule',
      },
    ]
  },

  {
    path: 'certificate',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'certificate-list-background',
        loadChildren: './eva/certificate-list-background/certificate-list-background-eva.module#CertificateListBackgroundEVAModule'
      },
      {
        path: 'certificate-add-background',
        loadChildren: './eva/certificate-add-background/certificate-add-background-eva.module#CertificateAddBackgroundEVAModule'
      },
      {
        path: 'certificate-list-design',
        loadChildren: './eva/certificate-list-design/certificate-list-design-eva.module#CertificateListDesignEVAModule'
      },
      {
        path: 'certificate-add-design',
        loadChildren: './eva/certificate-add-design/certificate-add-design-eva.module#CertificateAddDesignEVAModule'
      },
      {
        path: 'certificate-edit-design/:id',
        loadChildren: './eva/certificate-edit-design/certificate-edit-design-eva.module#CertificateEditDesignEVAModule'
      }
    ]
  },

  {
    path: 'student/my-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/my-course/my-course-student.module#MyCourseStudentModule'
  },
  {
    path: 'student/search-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/search-course/search-course-student.module#SearchCourseStudentModule'
  },
  {
    path: 'student/all-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/all-course/all-course-student.module#AllCourseStudentModule'
  },
  {
    path: 'student/next-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/next-course/next-course-student.module#NextCourseStudentModule'
  },
  {
    path: 'student/history-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/history-course/history-course-student.module#HistoryCourseStudentModule'
  },
  {
    path: 'student/detail-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/detail-course/detail-course-student.module#DetailCourseStudentModule'
  },
  {
    path: 'student/answer-evaluation-form',
    component: AdminLayoutNewComponent,
    loadChildren: './student/answer-evaluation-form/answer-evaluation-form-student.module#AnswerEvalutionFormStudentModule'
  },
  {
    path: 'student/download-certificate',
    component: AdminLayoutNewComponent,
    loadChildren: './student/download-certificate/download-certificate-student.module#DownloadCertificateStudentModule'
  },
  {
    path: 'student/survey-course',
    component: AdminLayoutNewComponent,
    loadChildren: './student/survey-answer/survey-answer-student.module#SurveyAnswerStudentModule'
  },
  {
    path: 'student/student-profile',
    component: AdminLayoutNewComponent,
    loadChildren: './student/profile/profile-student.module#ProfileStudentModule'
  },
  {
    path: 'student/change-password',
    component: AdminLayoutNewComponent,
    loadChildren: './student/change-password/change-password-student.module#ChangePasswordStudentModule'
  },

  // {
  //   path: 'student',
  //   component: AdminLayoutNewComponent,
  //   children: [
  //     {
  //       path: 'my-course',
  //       loadChildren: './student/my-course/my-course-student.module#MyCourseStudentModule',
  //     },
  //     {
  //       path: 'next-course',
  //       loadChildren: './student/next-course/next-course-student.module#NextCourseStudentModule',
  //     },
  //     {
  //       path: 'history-course',
  //       loadChildren: './student/history-course/history-course-student.module#HistoryCourseStudentModule',
  //     },
  //     {
  //       path: 'detail-course',
  //       loadChildren: './student/detail-course/detail-course-student.module#DetailCourseStudentModule',
  //     },
  //     {
  //       path: 'answer-evaluation-form',
  //       loadChildren: './student/answer-evaluation-form/answer-evaluation-form-student.module#AnswerEvalutionFormStudentModule',
  //     },
  //     {
  //       path: 'download-certificate',
  //       loadChildren: './student/download-certificate/download-certificate-student.module#DownloadCertificateStudentModule',
  //     },
  //     {
  //       path: 'survey-course',
  //       loadChildren: './student/survey-answer/survey-answer-student.module#SurveyAnswerStudentModule',
  //     },
  //     {
  //       path: 'student-profile',
  //       loadChildren: './student/profile/profile-student.module#ProfileStudentModule',
  //     },
  //     {
  //       path: 'change-password',
  //       loadChildren: './student/change-password/change-password-student.module#ChangePasswordStudentModule',
  //     }
  //   ]
  // },

  {
    path: 'role',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'list-access',
        loadChildren: './epdb/role-access/list-access/list-access-epdb.module#ListAccessEPDBModule'
      },
      {
        path: 'add-role',
        loadChildren: './epdb/role-access/add-role/add-role-epdb.module#AddRoleEPDBModule'
      },
      {
        path: 'edit-role/:id',
        loadChildren: './epdb/role-access/edit-role/edit-role-epdb.module#EditRoleEPDBModule'
      }
    ]
  },

  {
    path: 'report',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'organization-report',
        loadChildren: './epdb/report/organization-report/organization-report-epdb.module#OrganizationReportEPDBModule',
      },
      {
        path: 'sites-report',
        loadChildren: './epdb/report/sites-report/sites-report-epdb.module#SitesReportEPDBModule',
      },
      {
        path: 'contact-report',
        loadChildren: './epdb/report/contact-report/contact-report-epdb.module#ContactReportEPDBModule',
      },
      {
        path:'instructor-request',
        loadChildren : './epdb/report/instructor-request-list/instructor-request-list-epdb.module#InstructorRequestListEPDBModule'
      },
      // {
      //   path: 'email-list-generator',
      //   loadChildren: './epdb/report/email-list-generator/email-list-generator-epdb.module#EmailListGeneratorEPDBModule',
      // },
      // {
      //   path: 'show-security-roles',
      //   loadChildren: './epdb/report/show-security-roles/show-security-roles-epdb.module#ShowSecurityRolesEPDBModule',
      // },
      {
        path: 'beta-report-builder',
        loadChildren: './epdb/report/beta-report-builder/beta-report-builder-epdb.module#BetaReportBuilderEPDBModule',
      },
      {
        path: 'organization-journal-entry-report',
        loadChildren: './epdb/report/organization-journal-entry-report/organization-journal-entry-report-epdb.module#OrganizationJournalEntryReportEPDBModule'
      },
      {
        path: 'site-journal-entry-report',
        loadChildren: './epdb/report/site-journal-entry-report/site-journal-entry-report-epdb.module#SiteJournalEntryReportEPDBModule'
      },
      {
        path: 'invoice-report',
        loadChildren: './epdb/report/invoice-report/invoice-report-epdb.module#InvoiceReportEPDBModule'
      },
      {
        path: 'invoice-report-new',
        loadChildren: './epdb/report/invoice-report-new/invoice-report-new.module#InvoiceReportNewEPDBModule'
      },
      {
        path: 'qualifications-by-contact-report',
        loadChildren: './epdb/report/qualifications-by-contact-report/qualifications-by-contact-report-epdb.module#QualificationByContactReportEPDBModule'
      },
      {
        path: 'contact-attributes-report',
        loadChildren: './epdb/report/contact-attributes-report/contact-attributes-report-epdb.module#ContactAttributeReportEPDBModule'
      },
      {
        path: 'site-attributes-report',
        loadChildren: './epdb/report/site-attributes-report/site-attributes-report-epdb.module#SiteAttributeReportEPDBModule'
      },
      {
        path: 'ATC-AAP-accreditation-counts-report',
        loadChildren: './epdb/report/ATC-AAP-accreditation-counts-report/ATC-AAP-accreditation-counts-report-epdb.module#ATCAAPAccreditationCountsReportEPDBModule'
      },
      {
        path: 'site-accreditations',
        loadChildren: './epdb/report/site-accreditations-report/site-accreditations-report.module#SAREPDBModule'
      },
      {
        path: 'database-integrity-report',
        loadChildren: './epdb/report/database-integrity-report/database-integrity-report.module#DatabaseIntegrityModule'
      },
      {
        path: 'history-report',
        loadChildren: './epdb/report/history-report/history-report-epdb.module#HistoryReportEPDBModule',
      }
    ]
  },
  {
	path: 'instructor-report',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'instructor-list',
        loadChildren: './instructor-report/instructor-list/instructor-list.module#InstructorListModule'
      }
	  ]
  },
  {
    path: 'report-eva',
    component: AdminLayoutNewComponent,
    children: [
      {
        path: 'organization-report-eva',
        loadChildren: './eva/report/organization-report-eva/organization-report-eva.module#OrganizationReportEVAModule'
      },
      {
        path: 'performance-overview-eva',
        loadChildren: './eva/report/performance-overview-eva/performance-overview-eva.module#PerformanceOverviewEVAModule'
      },
      {
        path: 'instructor-performance-eva',
        loadChildren: './eva/report/instructor-performance-eva/instructor-performance-eva.module#InstructorPerformanceEVAModule'
      },
      {
        path: 'evaluation-responses',
        loadChildren: './eva/report/evaluation-responses/evaluation-responses.module#EvaluationResponsesEVAModule'
      },
      {
        path: 'student-search-eva',
        loadChildren: './eva/report/student-search-eva/student-search-eva.module#StudentSearchEVAModule'
      },
      {
        path: 'download-site-eva',
        loadChildren: './eva/report/download-site-eva/download-site-eva.module#DownloadSiteEVAModule'
      },
      {
        path: 'channel-performance-eva',
        loadChildren: './eva/report/channel-performance-eva/channel-performance-eva.module#ChannelPerformanceEVAModule'
      },
      //add product trained report dari google spreadsheet
      {
        path: 'product-trained-eva',
        loadChildren: './eva/report/product-trained/product-trained.module#ProductTrainedEVAModule'
      },
      //add product trained statistic report (New)
      {
        path: 'product-trained-statistic',
        loadChildren: './eva/report/product-statistic/product-statistic.module#ProductStatisticEVAModule'
      },
      {
        path: 'submissions-by-language-eva',
        loadChildren: './eva/report/submissions-by-language-eva/submissions-by-language-eva.module#SubmissionLanguageEVAModule'
      },
      {
        path: 'audit',
        loadChildren: './eva/report/audit/audit.module#AuditEVAModule'
      },
      {
        path: 'post-evaluation-responses',
        loadChildren: './eva/report/post-evaluation-responses/post-evaluation-responses.module#PostEvaluationResponseEVAModule'
      },
      {
        path: 'comparison-report',
        loadChildren: './eva/report/comparison-report/comparison-report.module#ComparisonReportEVAModule'
      },
      {
        path: 'instructor-course',
        loadChildren: './eva/report/instructor-course/instructor-course.module#InstructorCourseEVAModule'
      },
      {
        path: 'instructor-student/:SiteId/:InstructorId/:CompletionDate',
        loadChildren: './eva/report/instructor-student/instructor-student.module#InstructorStudentEVAModule'
      },
      {
        path: 'student-course/:EvaluationAnswerId/:InstructorId',
        loadChildren: './eva/report/student-course/student-course.module#StudentCourseEVAModule'
      },
      {
        path: 'evaluation-edition/:eva_code',
        loadChildren: './eva/report/evaluation-edition/evaluation-edition.module#EvaluationEditionEVAModule'
      },
      {
        path: 'site-profile',
        loadChildren: './eva/report/site-profile/site-profile.module#SiteProfileEVAModule'
      },
      {
        path: 'other-answer',
        loadChildren: './eva/report/other-answer/other-answer.module#OtherAnswerEVAModule'
      }
    ]
  },

  {
    path: 'profile',
    component: AdminLayoutNewComponent,
    loadChildren: './eim/profile/profile-eim.module#ProfileEIMModule'
  },
  {
    path: 'change-password',
    component: AdminLayoutNewComponent,
    loadChildren: './eim/change-password/change-password-eim.module#ChangePasswordEIMModule'
  },
  {
    path: 'change-profile',
    component: AdminLayoutNewComponent,
    loadChildren: './epdb/change-user-information/change-user-information-epdb.module#ChangeUserInformationEPDBModule'
  },
  {
    path: 'change-email/:id',
    component: AdminLayoutNewComponent,
    loadChildren: './eim/change-email/change-email-eim.module#ChangeEmailEIMModule'
  },

  {
    path: 'template-preview',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'survey-preview-eva/:id',
        loadChildren: './template-preview/survey-preview/survey-preview-eva.module#SurveyPreviewEVAModule',
      },
      {
        path: 'certificate-preview-eva/:id',
        loadChildren: './template-preview/certificate-preview/certificate-preview-eva.module#CertificatePreviewEVAModule',
      },
      {
        path: 'certificate-download-student-eva/:id/:courseid/:studentid',
        loadChildren: './template-preview/certificate-download-student/certificate-download-student-eva.module#CertificateDownloadStudentEVAModule',
      },
      {
        path: 'after-certificate-download',
        loadChildren: './template-preview/after-certificate-download-student/after-certificate-download-student-eva.module#AfterCertificateDownloadStudentEVAModule',
      },
    ]
  },

  {
    path: 'login',
    component: AuthLayoutComponent,
    loadChildren: './auth/login/admin.module#AdminModule'
  },
  {
    path: 'login-student',
    component: AuthLayoutComponent,
    loadChildren: './auth/login-student/student.module#StudentModule'
  },
  {
    path: 'logout',
    component: AuthLayoutComponent,
    loadChildren: './auth/logout/logout.module#LogoutModule'
  },
  {
    path: 'register',
    component: AuthLayoutComponent,
    loadChildren: './auth/register/studentregister.module#StudentRegisterModule'
  },
  {
    path: 'forget-password',
    component: AuthLayoutComponent,
    loadChildren: './auth/forgot/forgot.module#ForgotModule'
  },
  {
    path: 'reset-password/:code',
    component: AuthLayoutComponent,
    loadChildren: './auth/reset/reset.module#ResetModule'
  },
  {
    path: 'forget-password-student',
    component: AuthLayoutComponent,
    loadChildren: './auth/forgot-student/forgot.module#ForgotModule'
  },
  {
    path: 'reset-password-student/:code',
    component: AuthLayoutComponent,
    loadChildren: './auth/reset-student/reset.module#ResetModule'
  },
  {
    path: 'forbidden',
    component: AuthLayoutComponent,
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  },
  {
    path: 'error',
    component: AuthLayoutComponent,
    loadChildren: './error/error.module#ErrorModule'
  },
  {
    path: 'set-password',
    component: AuthLayoutComponent,
    loadChildren: './auth/setpassword/setpassword.module#SetPasswordModule'
  },
  {
    path: 'terminate-account',
    component: AuthLayoutComponent,
    loadChildren: './auth/terminate-account/terminate-account.module#TerminateAccountModule'
  },
  {
    path: 'activation/:id',
    component: AuthLayoutComponent,
    loadChildren: './auth/activation/activation.module#ActivationModule'
  },
  {
    path: 'warningMessage',
    component: AuthLayoutComponent,
    loadChildren: './auth/under13/under13.module#Under13Module'
  },
  {
    path: 'usertraining',
    component: AuthLayoutComponent,
    loadChildren: './maintenance/usertraining/usertraining.module#UserTrainingModule'
  },
  {
    path: 'help-me',
    component: AuthLayoutComponent,
    loadChildren: './maintenance/help/help.module#HelpModule'
  },
  {
    path: 'no-role-page',
    component: AuthLayoutComponent,
    loadChildren: './maintenance/norolepage/help.module#HelpModule'
  },
  {
    path: 'inactive-user',
    component: AuthLayoutComponent,
    loadChildren: './maintenance/inactive-user/inactive-user.module#InactiveUserModule'
  },
  {
    path: 'datamanagement-report',
    component: AdminLayoutNewComponent,
    loadChildren: './data-management/data-management.module#DataMagementModule'
  },
  // {
  //   path: '',
  //   component: AuthLayoutComponent,
  //   children: [
  //     {
  //       path: 'login',
  //       loadChildren: './auth/login/admin.module#AdminModule'
  //     },
  //     {
  //       path: 'login-student',
  //       loadChildren: './auth/login-student/student.module#StudentModule',
  //     },
  //     {
  //       path: 'logout',
  //       loadChildren: './auth/logout/logout.module#LogoutModule'
  //     },
  //     {
  //       path: 'register',
  //       loadChildren: './auth/register/studentregister.module#StudentRegisterModule',
  //     },
  //     {
  //       path: 'forget-password',
  //       loadChildren: './auth/forgot/forgot.module#ForgotModule',
  //     },
  //     {
  //       path: 'reset-password/:code',
  //       loadChildren: './auth/reset/reset.module#ResetModule',
  //     },
  //     {
  //       path: 'forget-password-student',
  //       loadChildren: './auth/forgot-student/forgot.module#ForgotModule',
  //     },
  //     {
  //       path: 'reset-password-student/:code',
  //       loadChildren: './auth/reset-student/reset.module#ResetModule',
  //     },
  //     {
  //       path: 'forbidden',
  //       loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  //     },
  //     {
  //       path: 'error',
  //       loadChildren: './error/error.module#ErrorModule'
  //     },
  //     {
  //       path: 'set-password',
  //       loadChildren: './auth/setpassword/setpassword.module#SetPasswordModule',
  //     },
  //     {
  //       path: 'terminate-account',
  //       loadChildren: './auth/terminate-account/terminate-account.module#TerminateAccountModule',
  //     },
  //   ]
  // },
  
  {
    path: '**',
    redirectTo: 'error/404'
  }
   
];
