import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from "@angular/common/http";

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe){}
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0,10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => (row.CourseId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.StartDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteAddress1.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterStatus' })
export class DataFilterStatusPipe {
    transform(array: any[], query: string): any {
        if (query == "Active") {
            return _.filter(array, row => 
                (row.SurveyTaken.indexOf("0") > -1) &&
                (row.activesurveylink.indexOf("Active") > -1));
        }
        else if(query == "Expired" || query == "Pending"){
            return _.filter(array, row => 
                (row.activesurveylink.indexOf(query) > -1));
        }else if(query == "Completed"){
            return _.filter(array, row => 
                (row.SurveyTaken.indexOf("1") > -1));
        }
        return array;
    }
}


@Component({
    selector: 'app-dashboard-student',
    templateUrl: './dashboard-student.component.html',
    styleUrls: [
        './dashboard-student.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardStudentComponent implements OnInit {

    private _serviceUrl = 'api/StudentsCourses';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    public activelinksurvey = [];
    public useraccesdata: any;
    public dataStudent: any;
    public filterStatus : string ="All";

    constructor(private _http: Http,private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
        this.getcourse(this.useraccesdata.UserId);
    }


    public mycourse: any;
    public countMyCourse: number = 0;
    public historycourse: any;
    public countHistoryCourse: number = 0;
    public nextcourse: any;
    public countNextCourse: number = 0;
    public countAllCourse: number = 0;

    getCertificateType(TrainingId, ProjectId, EventId) {

        let certificateType;

        if (TrainingId != "" && ProjectId == "" && EventId == "") {
            certificateType = 1;
        } else if (TrainingId == "" && ProjectId != "" && EventId == "") {
            certificateType = 2;
        } else if (TrainingId == "" && ProjectId == "" && EventId != "") {
            certificateType = 3;
        } else {
            certificateType = 0;
        }

        return certificateType;
    }

    takeSurvey(FyId, TrainingId, ProjectId, EventId, CourseId) {
        var access = Object.keys(this.useraccesdata)[5];
         var name = Object.keys(this.useraccesdata[access])[2];

        var data = JSON.stringify({
            'StudentName': this.useraccesdata[access][name],
            'CertificateType': this.getCertificateType(TrainingId, ProjectId, EventId),
            'CourseId': CourseId,
            'StudentID': this.useraccesdata.UserId,
            'FyId': FyId,
            'LanguageID': this.useraccesdata.LanguageID
        });

        this.router.navigate(['/student/survey-course'], { queryParams: { args: data } });
    }
    getallcourse(studentid) {
        var data :any;
        this.service.httpClientGet("api/StudentsCourses/allcourse/" + studentid , data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.data = '';
                }
                else {
                    this.data = result;
                    this.countAllCourse = this.data.length;
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                });
    }
    getmycourse(studentid){
        var data :any;
        this.service.httpClientGet(this._serviceUrl + "/mycourse/" + studentid , data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.mycourse = '';
                }
                else {
                    this.mycourse = result;
                    this.countMyCourse = this.mycourse.length;
                }
            },
                error => {
                    this.service.errorserver();
                    this.mycourse = '';
                });

    }
    gethistorycourse(studentid){
        var data :any;
        this.service.httpClientGet(this._serviceUrl + "/historycourse/" + studentid , data)
        .subscribe(result => {
            if (result == "Not found") {
                this.service.notfound();
                this.historycourse = '';
            }
            else {
                this.historycourse = result;
                this.countHistoryCourse = this.historycourse.length;
            }
        },
            error => {
                this.service.errorserver();
                this.historycourse = '';
            });
    }
    getnextcourse(studentid){
        var data :any;
    this.service.httpClientGet(this._serviceUrl + "/nextcourse/{'" + studentid +"'}", data)
    .subscribe(result => {
        if (result == "Not found") {
            this.service.notfound();
            this.nextcourse = '';
        }
        else {
            this.nextcourse = result;
            this.countNextCourse = this.nextcourse.length;
        }
    },
        error => {
            this.service.errorserver();
            this.nextcourse = '';
        });
    }
    getcourse(studentid) {
        setTimeout(() => {
            this.getallcourse(studentid);
        }, 1000);

        setTimeout(() => {
            this.getmycourse(studentid);
        }, 2000);

        setTimeout(() => {
            this.gethistorycourse(studentid);
        }, 3000);

        setTimeout(() => {
            this.getnextcourse(studentid);
        }, 4000);


        
       

      
    }

    detailCourse(id) {
        if (id != "") {
            var data :any;
            var detail: any;
            this.service.httpClientGet("api/Courses/Detail/" + id, data)
                .subscribe(res => {
                    detail = res;
                    if (detail.CourseId != "") {
                        this.datadetail = detail;
                    } else {
                        swal(
                            'Information!',
                            "Data Not Found",
                            'error'
                        );
                    }
                }, error => {
                    this.service.errorserver();
                });
        } else {
            swal(
                'Information!',
                "Data Not Found",
                'error'
            );
        }
    }

}
