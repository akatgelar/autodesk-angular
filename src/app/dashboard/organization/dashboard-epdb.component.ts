import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";

@Component({
    selector: 'app-dashboard-epdb',
    templateUrl: './dashboard-epdb.component.html',
    styleUrls: [
        './dashboard-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardEPDBComponent implements OnInit {

    doughChartCoreProductsAgainstAllCourses: any;
    doughChartTotalEvaluationSurveyByMonth: any;
    doughChartPerformanceSite: any;
    tableChartDataTopInstructor: any;
    messageError: string = '';
    private _serviceUrlPerformanceSite = 'api/DashboardPerformanceSite/null';
    private _serviceUrlCoursesDeliveredBySite = 'api/DashboardCoursesDeliveredBySite';
    private _serviceUrlMonthEvaluationSurveyByGeo = 'api/DashboardMonthEvaluationSurveyByGeo';
    private _serviceUrlEvaluationSurveyByCoreProducts = 'api/DashboardEvaluationByCoreProducts';
    private _serviceUrlTop10PerformanceInstructor = 'api/DashboardTop10PerformanceInstructor';

    dataCoursesDelivered = [];
    dataCoreProductsAgainstAllCourses = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    dataTotalEvaluationSurveyByMonthByGeo = [];
    DataPerformanceSite = [];
    DataTop10PerformanceIntructor = [];

    /*Bar chart*/
    //Month Courses
    type_MonthCoursesByPartner = 'bar';
    data_MonthCoursesByPartner = {}
    options_MonthCoursesByPartner = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };

    //Month Eva
    type_MonthEvaluationSurver = 'bar';
    data_MonthEvaluationSurver = {}
    options_MonthEvaluationSurver = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };


    //Total Eva
    type_TotalEvalutionTaken = 'bar';
    data_TotalEvalutionTaken = {}
    options_TotalEvalutionTaken = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };

    public loading = false;
    public loading_1 = false;
    public loading_2 = false;
    public loading_3 = false;
    public loading_4 = false;
    public loading_5 = false;
    public loading_6 = false;
    show1 :boolean = true;
    show2 :boolean = true;
    show3 :boolean = true;
    show4 :boolean = true;
    show5 :boolean = true;
    show6 :boolean = true;
    show7 :boolean = true;
    /*Bar chart End*/


    useraccesdata: any;
    constructor(private completerService: CompleterService,private _http: Http, private service: AppService, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    /* populate data issue role */
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true;
            }
        }
    }
    
    itsinstructor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsOrganization(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsDistributor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    itsSite(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                return true;
            } else {
                return false;
            }
        }
    }
    /* populate data issue role */

    adminkan: boolean = false;
    tampilin: boolean = true;
    protected dataService: RemoteData;
    OrgIdNew: string = "";
    ngOnInit() {
        //add chose org to see site admin dashboard
        if (!this.checkrole()) {
            this.adminkan = true;
            this.tampilin = false;

            this.dataService = this.completerService.remote(
                null,
                "Organization",
                "Organization");
            this.dataService.urlFormater(term => {
                return `api/MainOrganization/FindOrganizationSiteAdmin/` + term;
            });
            this.dataService.dataField("results");
        }

        if (this.tampilin) {
            this.OrgIdNew = this.urlGetOrgId();
            setTimeout(() => {
                this.getTop10PerformanceInstructor();
            }, 1000);

            setTimeout(() => {
                this.getEvaluationByMonth();
            }, 2000);

            setTimeout(() => {
                this.getMonthPerformanceSite();
            }, 3000);

            setTimeout(() => {
                this.getCoursesDelivered();
            }, 4000);

            setTimeout(() => {
                this.getParticipatedEvaSurveyCoreProduct();
            }, 5000);

            setTimeout(() => {
                this.getMonthEvaluationSurvey();
            }, 6000);
        }
    }

    PickedOrganization(data) {
        this.tampilin = true;
        this.OrgIdNew = '"' + data["originalObject"].OrgId + '"';

        setTimeout(() => {
            this.getTop10PerformanceInstructor();
        }, 1000);

        setTimeout(() => {
            this.getEvaluationByMonth();
        }, 2000);

        setTimeout(() => {
            this.getMonthPerformanceSite();
        }, 3000);

        setTimeout(() => {
            this.getCoursesDelivered();
        }, 4000);

        setTimeout(() => {
            this.getParticipatedEvaSurveyCoreProduct();
        }, 5000);

        setTimeout(() => {
            this.getMonthEvaluationSurvey();
        }, 6000);
    }

    data_MonthEvaluationSurverNew: any = "";
    getEvaluationByMonth() {
        var data: any;
        var orgid = "";
        var siteid = "";
        this.loading_1 = true;
        if(this.checkrole()){
            if(!this.itsDistributor()){
                if(!this.itsOrganization()){
                    if(this.itsSite()){
                        siteid = this.urlGetSiteId();
                    }
                }
            }
        }
        $("#morris-bar-chart-month").empty();

       // orgid = this.OrgIdNew;

        // this.data_MonthEvaluationSurverNew = [
        //     {
        //         PartnerType: 'ATC',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Course',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Project',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Event',
        //         Total: 0
        //     },
        // ]
        var keyword : any = {
            OrgId:this.OrgIdNew,
            siteid:siteid
           
        };
        keyword = JSON.stringify(keyword);
        //this.service.httpClientGet("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
        // this.service.httpClientGet("api/DashboardMonthEvaluationSurvey/whereorg/{'OrgId':'" + this.OrgIdNew + "','siteid':'"+siteid+"'}", '')
        this.service.httpClientPost("api/DashboardMonthEvaluationSurvey/whereorg", keyword)
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show2 = false;
                }
                else {
                    data = result;

                    // for (var i in data) {
                    //     if (data[i].PartnerType == 'ATC') {
                    //         this.data_MonthEvaluationSurverNew[0].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    //     else if (data[i].PartnerType == 'AAP Course') {
                    //         this.data_MonthEvaluationSurverNew[1].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    //     else if (data[i].PartnerType == 'AAP Project') {
                    //         this.data_MonthEvaluationSurverNew[2].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    //     else if (data[i].PartnerType == 'AAP Event') {
                    //         this.data_MonthEvaluationSurverNew[3].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    // }
                    
                    // var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    // Morris.Bar({
                    //     element: 'morris-bar-chart-month',
                    //     data: this.data_MonthEvaluationSurverNew,
                    //     xkey: 'PartnerType',
                    //     ykeys: ['Total'],
                    //     labels: ['Total'],
                    //     barColors: function (row) {
                    //         return barColor[row.x];
                    //     },
                    //     hideHover: 'auto',
                    //     gridLineColor: '#eef0f2',
                    //     resize: true,
                    //     xLabelAngle: 25
                    // });
                    
                    this.data_MonthEvaluationSurverNew = [];
                    for (var i in data) {
                        if(Number(i) < 10) {
                            this.data_MonthEvaluationSurverNew.push({
                                'ATC': data[i].SiteId,
                                'Total': Number(data[i].TotalSurveyTaken),
                            });
                            tmpCount += Number(data[i].TotalSurveyTaken);
                        }
                        
                    }
                    Morris.Bar({
                        element: 'morris-bar-chart-month',
                        data: this.data_MonthEvaluationSurverNew,
                        xkey: 'ATC',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelMargin: 0,
                        xLabelAngle: 43
                    });
                }
                this.loading_1 = false;
                if(tmpCount == 0){
                    this.show2 = false;
                }
                else{
                    this.show2 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_1 = false;
                    this.show2 = false;
                });
    }

    // baru site performance buat api nya
    getMonthPerformanceSite() {
        var data = '';
        this.loading_2 = true;
        var orgid = "";
        var siteid="";
        if(this.checkrole()){
            if(!this.itsDistributor()){
                if(!this.itsOrganization()){
                    if(this.itsSite()){
                        siteid = this.urlGetSiteId();
                    }
                }
            }
        }
        $('#id-month-site-performance').empty(); // biar gak numpuk pas nyari lagi

        orgid = this.OrgIdNew;

        var dataFilter = //change to post
        {
            "OrgId": orgid,
            "siteid":siteid
        }

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost( //change to post
            "api/DashboardSiteAdmin/SitePerformanceor",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show3 = false;
                }
                else {
                    var dataResult = result; //change to post
                    var PerformanceSite = dataResult;
                    this.DataPerformanceSite = []
                    for (var i in PerformanceSite) {
                        this.DataPerformanceSite.push({
                            'ATC': PerformanceSite[i].geo_name,
                            'Total': Number(PerformanceSite[i].TotalPerformanceSite),
                        });
                        tmpCount += Number(PerformanceSite[i].TotalPerformanceSite);
                    }
                    Morris.Bar({
                        element: 'id-month-site-performance',
                        data: this.DataPerformanceSite,
                        xkey: 'ATC',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelMargin: 0,
                        xLabelAngle: 35
                    });
                }
                if(tmpCount== 0){
                    this.show3 = false;
                }
                else{
                    this.show3 = true;
                }
                this.loading_2 = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading_2 = false;
                    this.show3 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    getTop10PerformanceInstructor() {
        var orgid = "";
        var siteid = ""; /* add kondisi api khusus siteadmin */

        this.loading = true;

        orgid = this.OrgIdNew;

        var data = '';

        /* add kondisi api khusus siteadmin */

        if(this.checkrole()){
            if(!this.itsDistributor()){
                if(!this.itsOrganization()){
                    if(this.itsSite()){
                        siteid = this.urlGetSiteId();
                    }
                }
            }
        }

        /* add kondisi api khusus siteadmin */

        var dataFilter = //change to post
        {
            "OrgId": orgid,
            "SiteId": siteid
        }

        this.DataTop10PerformanceIntructor = [];

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost( //change to post
            "api/DashboardSiteAdmin/top10instructor",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show1 = false;
                }
                else {
                    var dataResult = result; //change to post
                    let Top10PerformanceIntructor: any = dataResult;

                    this.DataTop10PerformanceIntructor.push(['Contact Id', 'Contact Name', 'Course Taken']);
                    for (var i in Top10PerformanceIntructor) {
                        this.DataTop10PerformanceIntructor.push([Top10PerformanceIntructor[i].ContactId, Top10PerformanceIntructor[i].ContactName, Number(Top10PerformanceIntructor[i].TotalCourseDeliveredByIntructor)]);
                        tmpCount += Number(Top10PerformanceIntructor[i].TotalCourseDeliveredByIntructor);
                    }

                    /* issue dashboard blank */

                    // if(Top10PerformanceIntructor.length < 1){
                    //     this.DataTop10PerformanceIntructor.push(['','No record found',''])
                    // }

                    /* end line issue dashboard blank */

                    this.tableChartDataTopInstructor = {
                        chartType: 'Table',
                        dataTable: this.DataTop10PerformanceIntructor,
                        options: {
                            height: 340,
                            title: 'Countries',
                            allowHtml: true
                        }
                    };
                    if(tmpCount == 0 || Top10PerformanceIntructor.length < 1){
                        this.show1 = false;
                    }
                    else{
                        this.show1 = true;
                    }
                }
                
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.show1 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    dataCoursesDeliveredNew = [];
    getCoursesDelivered() {
        var orgid = "";
        this.loading_3 = true;
        var siteid = "";
        $('#morris-bar-chart-partner').empty() //empty chart biar gak numpuk
        orgid = this.OrgIdNew;
        if(this.checkrole()){
            if(!this.itsDistributor()){
                if(!this.itsOrganization()){
                    if(this.itsSite()){
                        siteid = this.urlGetSiteId();
                    }
                }
            }
        }
        // this.dataCoursesDeliveredNew = [
        //     {
        //         PartnerType: 'ATC',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Course',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Project',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Event',
        //         Total: 0
        //     },
        // ]

        var dataFilter = //change to post
        {
            "OrgId": orgid,
            "siteid":siteid
        }

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost( //change to post
            "api/DashboardSiteAdmin/CoursesByMonth",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show4 = false;
                }
                else {
                    var dataResult : any = result; //change to post
                    var CoursesDelivered = dataResult;

                    // for (var i in CoursesDelivered) {
                    //     if (CoursesDelivered[i].PartnerType == 'ATC') {
                    //         this.dataCoursesDeliveredNew[0].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //         tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //     }
                    //     else if (CoursesDelivered[i].PartnerType == 'AAP Course') {
                    //         this.dataCoursesDeliveredNew[1].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //         tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //     }
                    //     else if (CoursesDelivered[i].PartnerType == 'AAP Project') {
                    //         this.dataCoursesDeliveredNew[2].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //         tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //     }
                    //     else if (CoursesDelivered[i].PartnerType == 'AAP Event') {
                    //         this.dataCoursesDeliveredNew[3].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //         tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                    //     }
                    // }
                    this.dataCoursesDeliveredNew = [];
                    for (var i in dataResult) {
                        if(Number(i) < 10) {
                            this.dataCoursesDeliveredNew.push({
                                'ATC': dataResult[i].SiteId,
                                'Total': Number(dataResult[i].TotalCourseMonthByPartnerType),
                            });
                            tmpCount += Number(dataResult[i].TotalCourseMonthByPartnerType);
                        }
                        
                    }
                    
                    Morris.Bar({
                        element: 'morris-bar-chart-partner',
                        data: this.dataCoursesDeliveredNew,
                        xkey: 'ATC',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 43
                    });
                }
                
                this.loading_3 = false;
                if(tmpCount == 0){
                    this.show4 = false;
                }
                else{
                    this.show4 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_3 = false;
                    this.show4 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    getMonthEvaluationSurvey() {
        var data: any;
       // var orgid = "";
        var siteid = "";
        this.loading_4 = true;

        $('#morris-bar-chart-month2').empty(); //empty chart biar gak numpuk
        if(this.checkrole()){
            if(!this.itsDistributor()){
                if(!this.itsOrganization()){
                    if(this.itsSite()){
                        siteid = this.urlGetSiteId();
                    }
                }
            }
        }
      //  orgid = this.OrgIdNew;
        this.data_MonthEvaluationSurverNew=[];
        // this.data_MonthEvaluationSurverNew = [
        //     {
        //         PartnerType: 'ATC',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Course',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Project',
        //         Total: 0
        //     },
        //     {
        //         PartnerType: 'AAP - Event',
        //         Total: 0
        //     },
        // ]
        var keyword : any = {
            OrgId:this.OrgIdNew,
            siteid:siteid
           
        };
        keyword = JSON.stringify(keyword);
        // this.service.httpClientGet("api/DashboardMonthEvaluationSurvey/total/where/{'OrgId':'" + orgid + "','siteid':'"+siteid+"'}", '')
        this.service.httpClientPost("api/DashboardMonthEvaluationSurvey/total/where", keyword)
            .subscribe(result => {
               let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show7 = false;
                }
                else {
                    data = result;

                    // for (var i in data) {
                    //     if (data[i].PartnerType == 'ATC') {
                    //         this.data_MonthEvaluationSurverNew[0].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    //     else if (data[i].PartnerType == 'AAP Course') {
                    //         this.data_MonthEvaluationSurverNew[1].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    //     else if (data[i].PartnerType == 'AAP Project') {
                    //         this.data_MonthEvaluationSurverNew[2].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    //     else if (data[i].PartnerType == 'AAP Event') {
                    //         this.data_MonthEvaluationSurverNew[3].Total = Number(data[i].TotalSurveyTaken);
                    //         tmpCount += Number(data[i].TotalSurveyTaken);
                    //     }
                    // }

                    // var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    // Morris.Bar({
                    //     element: 'morris-bar-chart-month2',
                    //     data: this.data_MonthEvaluationSurverNew,
                    //     xkey: 'PartnerType',
                    //     ykeys: ['Total'],
                    //     labels: ['Total'],
                    //     barColors: function (row) {
                    //         return barColor[row.x];
                    //     },
                    //     hideHover: 'auto',
                    //     gridLineColor: '#eef0f2',
                    //     resize: true,
                    //     xLabelAngle: 25
                    // });
                    for (var i in data) {
                        if(Number(i) < 10) {
                            this.data_MonthEvaluationSurverNew.push({
                                'ATC': data[i].SiteId,
                                'Total': Number(data[i].TotalSurveyTaken),
                            });
                            tmpCount += Number(data[i].TotalSurveyTaken);
                        }
                        
                    }
                    Morris.Bar({
                        element: 'morris-bar-chart-month2',
                        data: this.data_MonthEvaluationSurverNew,
                        xkey: 'ATC',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelMargin: 0,
                        xLabelAngle: 43
                    });
                }
                this.loading_4 = false;
                if(tmpCount == 0){
                    this.show7 = false;
                }
                else{
                    this.show7 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_4 = false;
                    this.show7 = false;
                });
    }

    dataATCParticipatedEvaSurvey = [];
    dataAAPParticipatedEvaSurvey = [];
    doughATCParticipatedEvaSurvey: any;
    doughAAPParticipatedEvaSurvey: any;
    re : any;
    getParticipatedEvaSurveyCoreProduct() {
        var orgid = "";
        var siteid = "";
        this.loading_6 = true;
        orgid = this.OrgIdNew;
        if(this.checkrole()){
            if(!this.itsDistributor()){
                if(!this.itsOrganization()){
                    if(this.itsSite()){
                        siteid = this.urlGetSiteId();
                    }
                }
            }
        }
        var dataFilter =
        {
            "OrgId": orgid,
            "siteid":siteid
        }

        this.dataAAPParticipatedEvaSurvey = [];

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost( //change to post
            "api/DashboardSiteAdmin/CoreProductAAP",
            dataFilter
        )
            .subscribe(result => {
                if (result == "Not found"|| result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show6 = false;
                }
                else {
                    var dataResult = result; //change to post

                    var dataParse = dataResult;
                    this.dataAAPParticipatedEvaSurvey.push(['Product Name', 'Total']);

                    for (var i in dataParse) {
                        if(dataParse[i].productName == 'All non core Product' && dataParse[i].TotalCourseEvaCoreProduct !='0' ){
                        this.dataAAPParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);}
                        else if(dataParse[i].productName != 'All non core Product'){
                            this.dataAAPParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                        }
                    }
                    
                    /* issue dashboard blank */

                    // if (dataParse[0].TotalCourseEvaCoreProduct == "0") {
                    //     this.dataAAPParticipatedEvaSurvey.push(['No record found', 1])
                    // }
                    /* end line issue dashboard blank */
                    this.doughAAPParticipatedEvaSurvey = {
                        chartType: 'PieChart',
                        dataTable: this.dataAAPParticipatedEvaSurvey,
                        
                        options: {
                            height: 360,
                            title: 'AAP',
                            pieHole: 0.4,
                            sliceVisibilityThreshold :0,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ], 
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                            
                        },
                        
                        
                    }; 
                    
    
                }
                this.loading_6 = false;
                if(this.dataAAPParticipatedEvaSurvey.length <= 1){
                    this.show6 = false;
                }
                else{
                    this.show6 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_6 = false;
                    this.show6 = false;
                });
        /* new post for autodesk plan 17 oct */

        this.loading_5 = true;
        // setTimeout(() => {
            var dataFilter =
            {
                "OrgId": orgid,
                "siteid":siteid
            }

            /* new post for autodesk plan 17 oct */
            this.dataATCParticipatedEvaSurvey = [];
            this.service.httpClientPost( //change to post
                "api/DashboardSiteAdmin/CoreProductATC",
                dataFilter
            ).subscribe(result => {
                    if (result == "Not found" || result == null || result == ' ') { //change to post
                        this.service.notfound();
                        this.show5 = false;
                    }
                    else {
                        var dataResult = result; //change to post
                        var dataParse = dataResult;
                        this.dataATCParticipatedEvaSurvey.push(['Product Name', 'Total']);

                        // for (var i in dataParse) {
                        //     this.dataATCParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                        // }
                        for (var i in dataParse) {
                            if(dataParse[i].productName == 'All non core Product' && dataParse[i].TotalCourseEvaCoreProduct !='0' ){
                            this.dataATCParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);}
                            else if(dataParse[i].productName != 'All non core Product'){
                                this.dataATCParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                            }
                        }
                        /* issue dashboard blank */

                        // if (dataParse[0].TotalCourseEvaCoreProduct == "0") {
                        //     this.dataATCParticipatedEvaSurvey.push(['No record found', 1])
                        // }

                        /* end line issue dashboard blank */

                        this.doughATCParticipatedEvaSurvey = {
                            chartType: 'PieChart',
                            dataTable: this.dataATCParticipatedEvaSurvey,
                            options: {
                                height: 360,
                                title: 'ATC',
                                pieHole: 0.4,
                                sliceVisibilityThreshold :0,
                                colors: [
                                    '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                                ],
                            },
                            lang: {
                            noData: "No Data Available",
                        },
                        noData: {
                            style: {
                                fontWeight: 'bold',
                                fontSize: '15px',
                                color: '#303030'
                            }
                        }
                        };
                    }
                    this.loading_5 = false;
                    if(this.dataATCParticipatedEvaSurvey.length <= 1){
                        this.show5 = false;
                    }
                    else{
                        this.show5 = true;
                    }
                },
                    error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                        this.loading_5 = false;
                        this.show5 = false;
                    });
            /* new post for autodesk plan 17 oct */
        // }, 3000);
    }
}
