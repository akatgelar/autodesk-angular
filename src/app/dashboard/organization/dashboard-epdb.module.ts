import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardEPDBComponent } from './dashboard-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { AppTranslateLanguage } from '../../shared/translate-language/app.translateLanguage';
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";

export const DashboardEPDBRoutes: Routes = [
  {
    path: '',
    component: DashboardEPDBComponent,
    data: {
      breadcrumb: 'menu_dashboard.dashboard_organization',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardEPDBRoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [DashboardEPDBComponent]
})
export class DashboardEPDBModule { }
