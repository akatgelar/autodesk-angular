import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';


@Component({
    selector: 'app-dashboard-autodesk-admin',
    templateUrl: './dashboard-autodesk-admin.component.html',
    styleUrls: [
        './dashboard-autodesk-admin.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardAutodeskAdminComponent implements OnInit {

    lineChartOptions: any;
    lineChartData: any;
    discreteBarOptions: any;
    doughChartPartnerMonth: any;
    doughChartActivePartner: any;
    doughChartActiveInstructure: any;
    doughtChartCourseMonth: any;
    donutChartData: any
    doughChartCoreProductsAgainstAllCourses: any;
    doughCoreProductsAgainstAllCourses: any
    doughATCParticipatedEvaSurvey: any
    doughAAPParticipatedEvaSurvey: any
    MEETTHEIRANNUALREQUIREMENTS: any

    messageError: string = '';

    dataMEETTHEIRANNUALREQUIREMENTS = []
    dataPartnerMonth = [];
    dataActivePartner = [];
    dataActiveInstructors = [];
    dataCoursesDelivered: any
    dataCoreProductsAgainstAllCourses = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    dataCoursesDeliveredByPartners = [];
    dataMonthEvaluationSurvey: any;

    dataATCParticipatedEvaSurvey = []
    dataAAPParticipatedEvaSurvey = []
    show1 : boolean = true;
    show2 : boolean = true;
    show3 : boolean = true;
    show4 : boolean = true;
    show5 : boolean = true;
    show6 : boolean = true;
    show7 : boolean = true;
    show8 : boolean = true;
    show9 : boolean = true;
    show10 : boolean = true;
    show11 : boolean = true;
    show12 : boolean = true;
    type1 = 'horizontalBar';
    data1 = {};
    data2 = {};

    
    optionsBar = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                ticks: {
                    min: 0,
                    stepSize: 100,
                    beginAtZero: true
                }
            }]
        }
    };
    public loading = false;
    public loading_1 = false;
    public loading_2 = false;
    public loading_3 = false;
    public loading_4 = false;
    public loading_5 = false;
    public loading_6 = false;
    public loading_7 = false;
    public loading_8 = false;
    public loading_9 = false;
    public loading_10 = false;
    public loading_11 = false;
    useraccesdata: any;
    constructor(private service: AppService, private session: SessionService) {
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
       
    }

    result: string = "";
    hoverPercent() {
        this.result = "%";
    }

    unhoverPercent() {
        this.result = "";
    }

    dataCoursesDeliveredNew = [];
    getCoursesDelivered() {
        var orgid = "";
        this.loading_8 = true;
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        this.dataCoursesDeliveredNew = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        this.service.httpClientGet("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show2 = false;
                }
                else {
                    var CoursesDelivered = result;

                    for (var i in CoursesDelivered) {
                        if (CoursesDelivered[i].PartnerType == 'ATC') {
                            this.dataCoursesDeliveredNew[0].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Course') {
                            this.dataCoursesDeliveredNew[1].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Project') {
                            this.dataCoursesDeliveredNew[2].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Event') {
                            this.dataCoursesDeliveredNew[3].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseMonthByPartnerType);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-partner',
                        data: this.dataCoursesDeliveredNew,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_8 = false;
                if(tmpCount ==0){
                    this.show2 = false;
                }
                else{
                    this.show2 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_8 = false;
                    this.show2 = false;
                });
    }

    dataTotalCourseDelivered = [];
    getTotalCoursesDelivered() {
        var orgid = "";
        this.loading_9 = true;
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        this.dataTotalCourseDelivered = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        this.service.httpClientGet("api/DashboardCoursesByMonth/totalCourseByPartner/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show5 = false;
                }
                else {
                    var CoursesDelivered = result;

                    for (var i in CoursesDelivered) {
                        if (CoursesDelivered[i].PartnerType == 'ATC') {
                            this.dataTotalCourseDelivered[0].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Course') {
                            this.dataTotalCourseDelivered[1].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Project') {
                            this.dataTotalCourseDelivered[2].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Event') {
                            this.dataTotalCourseDelivered[3].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType);
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-partner2',
                        data: this.dataTotalCourseDelivered,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_9 = false;
                if(tmpCount ==0){
                    this.show5 = false;
                }
                else{
                    this.show5 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_9 = false;
                    this.show5 = false;
                });
    }

    //Active Instructor
    type_ActiveInstructor = 'horizontalBar';
    data_ActiveInstructor = {}
    options_ActiveInstructor = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };

    getActiveInstructors() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        this.loading = true;
        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        // for (var k = 0; k < roleid.length; k++) {
        this.service.httpClientGet("api/DashboardActiveInstructors/All/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show1 = false;
                }
                else {
                    data = result;
                    // console.log(data);
                    teritorryarr = [];
                    var partners_1 = [];
                    var partners_2 = [];
                    var partners_3 = [];
                    var aap = [];
                    var atc = [];
                    var mtp = [];
                    var temp = [];
                    /*limit 10 biar gak kebanyakan country nya (jadi rarengkep)*/

                    var length = data.length;

                    // if (length > 10) {
                    //     length = 10;
                    // }

                    /*end limit 10 biar gak kebanyakan country nya (jadi rarengkep)*/
                    // for (var i = 0; i < data.length; i++) {
                    //     if (orgid != "") {
                    //         teritorryarr.push(data[i].countries_name);
                    //     } else {
                    //         teritorryarr.push(data[i].Territory_Name);
                    //     }
                    // }


                    for (var i = 0; i < data.length; i++) {
                        teritorryarr.push(data[i].Territory_Name);
                    }

                    var uniq = [];
                    for (let n = 0; n < teritorryarr.length; n++) {
                        if (uniq.indexOf(teritorryarr[n]) == -1) {
                            uniq.push(teritorryarr[n]);
                        }
                    }

                    // console.log(uniq);
                    var teri_anyar = [];

                    for (let u = 0; u < length; u++) {
                        if (uniq[u] != undefined) {
                            teri_anyar.push(uniq[u]);
                        }
                    }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == "ATC") {
                            partners_1.push(+data[i].TotalActiveInstructor);
                            atc.push({
                                label: data[i].RoleCode,
                                data: partners_1,
                                backgroundColor: data[i].BackgroundColor
                            });
                            tmpCount += data[i].TotalActiveInstructor;
                        }
                    }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == "AAP") {
                            partners_2.push(+data[i].TotalActiveInstructor);
                            aap.push({
                                label: data[i].RoleCode,
                                data: partners_2,
                                backgroundColor: data[i].BackgroundColor
                            });
                            tmpCount += data[i].TotalActiveInstructor;
                        }
                    }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == "MTP") {
             
                            if (temp.includes(data[i].Territory_Name) === true )  {
                                var d = temp.indexOf(data[i].Territory_Name)
                                partners_3[d] = data[i].TotalActiveInstructor;
                            } 
                            else{
                                temp.push(data[i].Territory_Name);
                                partners_3.push(+data[i].TotalActiveInstructor);
                            }
                            tmpCount += data[i].TotalActiveInstructor;
                         }
                         else{

                                if (temp.includes(data[i].Territory_Name) === false)  {
                                    temp.push(data[i].Territory_Name);
                                    partners_3.push(+0);
                            }
                            
                         }

                         mtp.push({
                            label: 'MTP',
                            data: partners_3,
                            backgroundColor: "#1AB244"
                         })
                        
                   }

                    var jek = [];
                    if (atc.length != 0) {
                        jek.push(atc[0]);
                    }

                    if (aap.length != 0) {
                        jek.push(aap[0]);
                    }

                    if (mtp.length != 0) {
                        jek.push(mtp[0]);
                    }

                    // dataset.push({
                    //     label: ["ATC","AAP","MTP"],
                    //     data: partners,
                    //     backgroundColor: data[0].BackgroundColor
                    // })
                    this.data_ActiveInstructor = {
                        labels: teri_anyar,
                        datasets: jek
                    }
if(tmpCount ==0){
    this.show1 = false;
}
else{
    this.show1 = true;
}
                    // console.log(this.data_ActiveInstructor);
                }
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                    this.show1 = false;
                });
        // }

        // this.service.get("api/DashboardActiveInstructors/where/{'OrgId':'" + orgid + "'}", '')
        //   .subscribe(result => {
        //     if (result == "Not found") {
        //       this.service.notfound();
        //     }
        //     else {
        //       data = JSON.parse(result);
        //       var partnersATC = [];
        //       var partnersAAP = [];
        //       var partnersCTC = [];
        //       var countriesName = []
        //       var backgroundColorATC = []
        //       var backgroundColorAAP = []
        //       var backgroundColorMTP = []
        //       for (var i = 0; i < data.length; i++) {
        //         if (data[i].RoleCode == 'ATC') {
        //          partnersATC.push(data[i].TotalActiveInstructor);
        //          partnersAAP.push(0);
        //          partnersCTC.push(0)
        //         }
        //         else if (data[i].RoleCode == 'AAP') {
        //          partnersATC.push(0);
        //          partnersAAP.push(data[i].TotalActiveInstructor);
        //          partnersCTC.push(0)
        //         } else if (data[i].RoleCode == 'CTC') {
        //          partnersATC.push(0);
        //          partnersAAP.push(0);
        //          partnersCTC.push(data[i].TotalActiveInstructor)
        //         } else {
        //          partnersATC.push(0);
        //          partnersAAP.push(0);
        //          partnersCTC.push(0)
        //         }

        //         countriesName.push(data[i].Label);
        //         backgroundColorATC.push("#AD1D28")
        //         backgroundColorAAP.push("#DEBB27")
        //         backgroundColorMTP.push("#1AB244")

        //        // Set Into Chart
        //        this.data_ActiveInstructor = {
        //          labels: countriesName,
        //          datasets: [{
        //            label: 'ATC',
        //            data: partnersATC,
        //            backgroundColor: backgroundColorATC
        //          },
        //          {
        //            label: 'AAP',
        //            data: partnersAAP,
        //            backgroundColor: backgroundColorAAP
        //          },
        //          {
        //            label: 'MTP',
        //            data: partnersCTC,
        //            backgroundColor: backgroundColorMTP
        //          }]
        //        }
        //       }
        //     }
        //   },
        //   error => {
        //     this.service.errorserver();
        //   });
    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    ngOnInit() {      
        setTimeout(() => {
            this.getSiteMeetTheirAnnualReqATC(); //query nya masih di limit hanya tahun ini
        }, 1000);
        setTimeout(() => {
            this.getSitesParticipatedInEvaSurvey();
        }, 2000);
        setTimeout(() => {
            this.getParticipatedEvaSurveyCoreProduct();
        }, 3000);
        setTimeout(() => {
            this.getNewPartnerByMonth();
            this.getCoursesDelivered();
            this.totalNewPartner();
        }, 4000);
        setTimeout(() => {
            this.getEvaluationByMonth();
            this.getActiveInstructors();
            this.getTotalCoursesDelivered();
        }, 5000);
        setTimeout(() => {
            this.getSiteMeetTheirAnnualReqAAP(); 
        }, 6000);
    }

    // getNewPartnerByMonth() {
    //     var data: any;
    //     var roleid = ["60", "58", "1"];
    //     var teritorryarr = [];
    //     var dataset = [];
    //     this.loading_5 = true;
    //     var orgid = "";
    //     if (this.checkrole()) {
    //         orgid = this.urlGetOrgId();
    //     }

    //    for (var k = 0; k < roleid.length; k++) {
    //        this.service.httpClientGet("api/DashboardPartner/All/where/{'OrgId':'" + orgid + "','RoleId':'" + roleid[k] + "'}", '')
    //              .subscribe(result => {
    //                 let tmpCount : any= 0; 
    //                 if (result == "Not found" || result == null || result == ' ') {
    //                     this.service.notfound();
    //                     this.show10 = false;
    //                 }
    //                 else {
    //                     data = result;

    //                     teritorryarr = [];
    //                     var partners = [];

    //                     for (var i = 0; i < data.length; i++) {
    //                         partners.push(+data[i].CountPartners);
    //                         teritorryarr.push(data[i].Territory_Name);
    //                         tmpCount += data[i].CountPartners;
    //                     }

    //                     dataset.push({
    //                         label: data[0].RoleCode,
    //                         data: partners,
    //                         backgroundColor: data[0].BackgroundColor
    //                     })

    //                     this.data1 = {
    //                         labels: teritorryarr,
    //                         datasets: dataset
    //                     }
    //                 }
    //                 this.loading_5 = false;
    //                 if(tmpCount ==0){
    //                     this.show10 = false;
    //                 }
    //                 else{
    //                     this.show10 = true;
    //                 }
    //             },
    //                 error => {
    //                     this.service.errorserver();
    //                     this.loading_5 = false;
    //                     this.show10 = false;
    //                 });
    //     }
    // }

    getNewPartnerByMonth() {
        var data: any;
        var teritorryarr = [];
        var dataset = [];
        this.loading_5 = true;
        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

      this.service.httpClientGet("api/DashboardPartner/whereNew/{'OrgId':'" + orgid + "'}", '')
                .subscribe(result => {
                    let tmpCount : any= 0; 
                    if (result == "Not found" || result == null || result == ' ') {
                        this.service.notfound();
                        this.show10 = false;
                    }
                    else {
                        data = result;

                        for (var i = 0; i < data.length; i++) {
                            tmpCount += data[i].ATC;
                        }
                        var tmpData = data,
                        config = {
                            element: 'totalPartnerNew',
                            data: tmpData,
                            xkey: 'Territory_Name',
                            ykeys: ['ATC', 'AAPMTP','CTC'],
                            labels: ['ATC','AAP & MTP', 'CTC'],
                            fillOpacity: 0.6,
                            hideHover: 'auto',
                            behaveLikeLine: true,
                            resize: true,
                            barColors:['#AD1D28','#DEBB27','#1AB244'],
                            pointFillColors:['#ffffff'],
                            pointStrokeColors: ['black'],
                            lineColors:['gray','red'],
                            stacked : true,
                            xLabelAngle: 90,
 
                        }
                        config.element = 'totalPartnerNew';
                        config.stacked = true;
                        Morris.Bar(config);
                    }
                    if(tmpCount ==0){
                        this.show10 = false;
                    }
                    else{
                        this.show10 = true;
                    }
                    this.loading_5 = false;
                   
                },
                    error => {
                        this.service.errorserver();
                        this.loading_5 = false;
                        this.show10 = false;
                    });

    }

    //Total New Partner by Month
    totalNewPartner() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        this.loading_10 = true;
        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        for (var k = 0; k < roleid.length; k++) {
            this.service.httpClientGet("api/DashboardPartner/Month/where/{'OrgId':'" + orgid + "','RoleId':'" + roleid[k] + "'}", '')
                .subscribe(result => {
                    let tmpCount : any= 0; 
                    if (result == "Not found" || result == null || result == ' ') {
                        this.service.notfound();
                        this.show9 = false;
                    }
                    else {
                        data = result;
                        teritorryarr = [];
                        var partners = [];

                        for (var i = 0; i < data.length; i++) {
                            partners.push(+data[i].CountPartners);
                            teritorryarr.push(data[i].Territory_Name);
                            tmpCount += data[i].CountPartners;
                        }

                        var empty = 0;
                        for (let x = 0; x < partners.length; x++) {
                            if (partners[x] == 0) {
                                empty += 1;
                            }
                        }

                        if (empty == 16) {
                            partners = [];
                        }

                        dataset.push({
                            label: data[0].RoleCode,
                            data: partners,
                            backgroundColor: data[0].BackgroundColor
                        })

                        this.data2 = {
                            labels: teritorryarr,
                            datasets: dataset
                        }
                    }
                    this.loading_10 = false;
                    if(tmpCount ==0){
                        this.show9 = false;
                    }
                    else{
                        this.show9 = true;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.loading_10 = false;
                        this.show9 = false;
                    });
        }
    }

    getParticipatedEvaSurveyCoreProduct() {
        var orgid = "";
        this.loading_4 = true;
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        // this.service.get("api/DCPAAC/AAP/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientGet("api/DashboardTmpCoreProductAAP", '')
            .subscribe(result => {
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show8 = false;
                }
                else {
                    // var dataResult = result.replace(/\t/g, " ");
                    // dataResult = dataResult.replace(/\r/g, " ");
                    // dataResult = dataResult.replace(/\n/g, " ");
                    var dataResult  = result;
                    var dataParse =dataResult;
                    this.dataAAPParticipatedEvaSurvey.push(['Product Name', 'Total']);

                    for (var i in dataParse) {
                        this.dataAAPParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                    }

                    // if (dataParse == "") {
                    //     this.dataAAPParticipatedEvaSurvey.push([' ', 1])
                    // }

                    this.doughAAPParticipatedEvaSurvey = {
                        chartType: 'PieChart',
                        dataTable: this.dataAAPParticipatedEvaSurvey,
                        options: {
                            height: 360,
                            title: 'AAP',
                            pieHole: 0.4,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ],
                            sliceVisibilityThreshold :0,
                        },
                    };
                }
                this.loading_4 = false;
                if(this.dataAAPParticipatedEvaSurvey.length <=1){
                    this.show8 = false;
                }
                else{
                    this.show8 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_4 = false;
                    this.show8 = false;
                });

        this.loading_3 = true;
        setTimeout(() => {
            // this.service.get("api/DCPAAC/ATC/where/{'OrgId':'" + orgid + "'}", '')
            this.service.httpClientGet("api/DashboardTmpCoreProductATC", '')
                .subscribe(result => {
                    if (result == "Not found" || result == null || result == ' ') {
                        this.service.notfound();
                        this.show7 = false;
                    }
                    else {
                        var dataResult = result

                        var dataParse = dataResult;
                        this.dataATCParticipatedEvaSurvey.push(['Product Name', 'Total']);

                        for (var i in dataParse) {
                            this.dataATCParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                        }

                        // if (dataParse == "") {
                        //     this.dataATCParticipatedEvaSurvey.push([' ', 1])
                        // }

                        this.doughATCParticipatedEvaSurvey = {
                            chartType: 'PieChart',
                            dataTable: this.dataATCParticipatedEvaSurvey,
                            options: {
                                height: 360,
                                title: 'ATC',
                                pieHole: 0.4,
                                colors: [
                                    '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                                ],
                                sliceVisibilityThreshold :0,
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                            },
                        };
                    }
                    this.loading_3 = false;
                    if(this.dataATCParticipatedEvaSurvey.length <=1){
                        this.show7 = false;
                    }
                    else{
                        this.show7 = true;
                    }
                },
                    error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                        this.loading_3 = false;
                        this.show7 = false;
                    });
        }, 3000);
    }

    dataSiteMeetTheirAnnualReq = {};
    getSiteMeetTheirAnnualReqATC() {
        var data: any;
        var meetnotmeet = ">=,<,0";
        var teritorryarr = [];
        var dataset = [];
        var NotMeet = [];
        var Meet = [];
        var Total = [];
        //isu di grup AD 23-07-2018 disuruh tambah loading di setiap chart
        this.loading = true;

        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }
        this.service.httpClientGet("api/DashboardSitesMeetAnnualReq/where/{'OrgId':'" + orgid + "','MeetNotMeet':'" + meetnotmeet + "'}", '')
        .subscribe(result => {

        //});
        // for (var k = 0; k < meetnotmeet.length; k++) {
        //     this.service.httpClientGet("api/DashboardSitesMeetAnnualReq/where/{'OrgId':'" + orgid + "','MeetNotMeet':'" + meetnotmeet[k] + "'}", '')
        //         .subscribe(result => {
                    let tmpCount : any= 0; 
                    if (result == "Not found" || result == null || result == ' ') {
                        this.service.notfound();
                        this.show3 = false;
                    }
                    else {
                        data = result;
                        teritorryarr = [];
                        var label = [];
                        var partners = [];
                        var backgroundColor = [];

                        for (var i = 0; i < data.length; i++) {
                            partners.push(+data[i].MeetCount);
                            NotMeet.push(+data[i].NotMeetCount);
                            teritorryarr.push(data[i].Territory_Name);
                            tmpCount += data[i].MeetCount + data[i].NotMeetCount;

                        }
                        Meet.push({
                            label: 'Site Meet',
                            data: partners,
                            backgroundColor: "#1AB244"
                         })
                         Total.push({
                            label: 'Site Not Meet',
                            data: NotMeet,
                            backgroundColor: "#AD1D28"
                         })
                    var jek = [];
                    if (Total.length != 0) {
                        jek.push(Total[0]);
                    }

                    if (Meet.length != 0) {
                        jek.push(Meet[0]);
                    }


                        dataset.push({
                            label: label,
                            data: jek,
                            backgroundColor: ["#AD1D28", "#DEBB27"]
                        })
                        var formatY = function (y) {
                            return '$'+y;
                        }
                    var formatX = function (x) {
                            return x.src.y;
                        }
                        this.dataSiteMeetTheirAnnualReq = {
                           
                            // dataset: data,
                            // xkey: 'Territory_Name',
                            // ykeys: ['MeetCount', 'NotMeetCount'],
                            // //labels: ['Total Income', 'Total Outcome'],
                            // fillOpacity: 0.6,
                            // hideHover: 'auto',
                            // stacked: true,
                            // resize: true,
                            // pointFillColors:['#ffffff'],
                            // pointStrokeColors: ['black'],
                            // barColors:['blue','green','orange'],
                            // yLabelFormat:formatY,
                            // xLabelFormat: formatX,
                            // hoverCallback: function (index, options, content, row) {
                            //   return 'custom 1';


                            // }


                            // labels: teritorryarr,
                            // datasets: jek,
                            // xkey: jek,
                            // ykeys:teritorryarr,
                            // options: {
                            //     scales: {
                            //         xAxes: [{
                            //             stacked: true
                            //         }],
                            //         yAxes: [{ stacked: true }]
                            //     },
                            //     // scales: {
                            //     //   xAxes: [{ stacked: true }],
                            //     //   yAxes: [{ stacked: true }]
                            //     // }
                            //   }
                           
                            //stacked: true
                            
                        }
                       
                       // var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                        var tmpData = data,
                        config = {
                            element: 'sitemeetATC',
                            data: tmpData,
                            xkey: 'Territory_Name',
                            ykeys: ['perSiteMeet', 'perSiteNotMeet'],
                            labels: ['Site Meet'+'(%)', 'Site Does not Meet'+'(%)'],
                            fillOpacity: 0.6,
                            hideHover: 'auto',
                            behaveLikeLine: true,
                            resize: true,
                            barColors:['#AD1D28','#DEBB27'],
                            pointFillColors:['#ffffff'],
                            pointStrokeColors: ['black'],
                            lineColors:['gray','red'],
                           stacked : true,
                            //stacked100: true,
                            xLabelAngle: 90,
                           
                                
                               
                        //     data: data,
                        //     xkey: ['MeetCount', 'NotMeetCount'],
                        //     ykeys:  teritorryarr,
                        //    // labels: ['MeetCount', 'NotMeetCount'],
                        //     barColors: function (row) {
                        //         return barColor[row.x];
                        //     },
                        //     fillOpacity: 0.6,
                        //     hideHover: 'auto',
                        //     stacked: true,
                        //     resize: true,
                        //     pointFillColors:['#ffffff'],
                        //     pointStrokeColors: ['black'],
                           
                            // yLabelFormat:formatY,
                            // xLabelFormat: formatX,
                            
                        }
                        config.element = 'sitemeetATC';
                        config.stacked = true;
                        Morris.Bar(config);
                    }
                    
                    if(tmpCount ==0){
                        this.show3 = false;
                    }
                    else{
                        this.show3 = true;
                    }
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                        this.show3 = false;
                    });
        // }
    }
    getSiteMeetTheirAnnualReqAAP() {
        var data: any;
        var meetnotmeet = ">=,<,0";
        this.loading_11 = true;

        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }
        this.service.httpClientGet("api/DashboardSitesMeetAnnualReq/whereAAP/{'OrgId':'" + orgid + "','MeetNotMeet':'" + meetnotmeet + "'}", '')
        .subscribe(result => {
                    let tmpCount : any= 0; 
                    if (result == "Not found" || result == null || result == ' ') {
                        this.service.notfound();
                        this.show12 = false;
                    }
                    else {
                        data = result;
                        for (var i = 0; i < data.length; i++) {
                            tmpCount += data[i].TotalCount + data[i].MeetCount + data[i].NotMeetCount;
                        }
                        var tmpData = data,
                        config = {
                            element: 'sitemeetAAP',
                            data: tmpData,
                            xkey: 'Territory_Name',
                            ykeys: ['perSiteMeet', 'perSiteNotMeet'],
                            labels: ['Site Meet'+'(%)', 'Site Does not Meet'+'(%)'],
                            fillOpacity: 0.6,
                            hideHover: 'auto',
                            behaveLikeLine: true,
                            resize: true,
                            barColors:['#AD1D28','#DEBB27'],
                            pointFillColors:['#ffffff'],
                            pointStrokeColors: ['black'],
                            lineColors:['gray','red'],
                           stacked : true,
                            //stacked100: true,
                            xLabelAngle: 90,
 
                        }
                        config.element = 'sitemeetAAP';
                        config.stacked = true;
                        Morris.Bar(config);
                    }
                    if(tmpCount ==0){
                        this.show12 = false;
                    }
                    else{
                        this.show12 = true;
                    }
                    this.loading_11 = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading_11 = false;
                        this.show12 = false;
                    });
        // }
    }
    dataSitesParticipatedInEvaSurvey = [];
    dataSitesParticipatedInEvaSurvey2 = [];
    doughSitesInEvaSurvey: any;
    doughSitesInEvaSurvey2: any;
    getSitesParticipatedInEvaSurvey() {
        var orgid = "";
        this.loading_2 = true;
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        this.service.httpClientGet("api/DashboardSitesInEvaSurvey/AAP/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {;
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show6 = false;
                }
                else {
                   
                    var dataParse :any = result;
                    for (var i = 0; i < dataParse.length; i++) {
                        tmpCount += dataParse[i].TotalSitesEval + dataParse[i].NonCount;
                    }
                    var tmpData = dataParse,
                    config = {
                        element: 'SitesParticipatedAAP',
                        data: tmpData,
                        xkey: 'Territory_Name',
                        ykeys: ['TotalSitesEval', 'NonCount'],
                        labels: ['Sites Participated', 'Sites Do not Participate'],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        barColors:['#AD1D28','#DEBB27'],
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:['gray','red'],
                        stacked : true,
                        //stacked100: true,
                        xLabelAngle: 90,

                    }
                    config.element = 'SitesParticipatedAAP';
                    config.stacked = true;
                    Morris.Bar(config);
                    this.loading_2 = false;
                }
                if(tmpCount ==0){
                    this.show6 = false;
                }
                else{
                    this.show6 = true;
                }
                //     this.dataSitesParticipatedInEvaSurvey.push(['Product Name', 'Total']);

                //     this.dataSitesParticipatedInEvaSurvey.push(["Site Participate", Number(dataParse.TotalSitesEval)]);
                //     this.dataSitesParticipatedInEvaSurvey.push(["Site Non Participate", Number(dataParse.TotalSitesUnEval)]);

                //     this.doughSitesInEvaSurvey = {
                //         chartType: 'PieChart',
                //         dataTable: this.dataSitesParticipatedInEvaSurvey,
                //         options: {
                //             height: 360,
                //             title: 'AAP',
                //             pieHole: 0.4
                //         },
                //         sliceVisibilityThreshold :0,
                //             lang: {
                //                 noData: "No Data Available",
                //             },
                //             noData: {
                //                 style: {
                //                     fontWeight: 'bold',
                //                     fontSize: '15px',
                //                 }
                //             }
                //     };
                // }
                // this.loading_2 = false;
                // if(this.dataSitesParticipatedInEvaSurvey.length <=1){
                //     this.show6 = false;
                // }
                // else{
                //     this.show6 = true;
                // }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_2 = false;
                    this.show6 = false;
                });

        this.loading_1 = true;
        this.service.httpClientGet("api/DashboardSitesInEvaSurvey/ATC/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show4 = false;
                }
                else {
                    var dataParse :any = result;
                    for (var i = 0; i < dataParse.length; i++) {
                        tmpCount += dataParse[i].TotalSitesEval + dataParse[i].NonCount;
                    }
                    var tmpData = dataParse,
                    config = {
                        element: 'SitesParticipatedATC',
                        data: tmpData,
                        xkey: 'Territory_Name',
                        ykeys: ['TotalSitesEval', 'NonCount'],
                        labels: ['Sites Participated', 'Sites Do not Participate'],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        barColors:['#AD1D28','#DEBB27'],
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:['gray','red'],
                       stacked : true,
                        //stacked100: true,
                        xLabelAngle: 90,

                    }
                    config.element = 'SitesParticipatedATC';
                    config.stacked = true;
                    Morris.Bar(config);
                }
                if(tmpCount ==0){
                    this.show4 = false;
                }
                else{
                    this.show4 = true;
                }
                this.loading_1 = false;
                    // this.dataSitesParticipatedInEvaSurvey2.push(['Product Name', 'Total']);

                    // this.dataSitesParticipatedInEvaSurvey2.push(["Site Participate", Number(dataParse.TotalSitesEval)]);
                    // this.dataSitesParticipatedInEvaSurvey2.push(["Site Non Participate", Number(dataParse.TotalSitesUnEval)]);

                    // this.doughSitesInEvaSurvey2 = {
                    //     chartType: 'PieChart',
                    //     dataTable: this.dataSitesParticipatedInEvaSurvey2,
                    //     options: {
                    //         height: 360,
                    //         title: 'ATC',
                    //         pieHole: 0.4
                    //     },
                    //     sliceVisibilityThreshold :0,
                    //         lang: {
                    //             noData: "No Data Available",
                    //         },
                    //         noData: {
                    //             style: {
                    //                 fontWeight: 'bold',
                    //                 fontSize: '15px',
                    //             }
                    //         }
                    // };
                // }
                // this.loading_1 = false;
                // if(this.dataSitesParticipatedInEvaSurvey2.length <=1){
                //     this.show4 = false;
                // }
                // else{
                //     this.show4 = true;
                // }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_1 = false;
                    this.show4 = false;
                });
    }


    dataEvaluationMonth = [];
    getEvaluationByMonth() {
        var orgid = "";
        this.loading_6 = true;
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        this.dataEvaluationMonth = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        this.service.httpClientGet("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show11 = false;
                }
                else {
                    var EvaByMonth : any = result;

                    for (var i in EvaByMonth) {
                        if (EvaByMonth[i].PartnerType == 'ATC') {
                            this.dataEvaluationMonth[0].Total = Number(EvaByMonth[i].TotalSurveyTaken);
                            tmpCount +=Number(EvaByMonth[i].TotalSurveyTaken);
                        }
                        else if (EvaByMonth[i].PartnerType == 'AAP Course') {
                            this.dataEvaluationMonth[1].Total = Number(EvaByMonth[i].TotalSurveyTaken);
                            tmpCount +=Number(EvaByMonth[i].TotalSurveyTaken);
                        }
                        else if (EvaByMonth[i].PartnerType == 'AAP Project') {
                            this.dataEvaluationMonth[2].Total = Number(EvaByMonth[i].TotalSurveyTaken);
                            tmpCount +=Number(EvaByMonth[i].TotalSurveyTaken);
                        }
                        else if (EvaByMonth[i].PartnerType == 'AAP Event') {
                            this.dataEvaluationMonth[3].Total = Number(EvaByMonth[i].TotalSurveyTaken);
                            tmpCount +=Number(EvaByMonth[i].TotalSurveyTaken);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];

                    Morris.Bar({
                        element: 'id-evaluation-by-month',
                        data: this.dataEvaluationMonth,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_6 = false;
                if(tmpCount ==0){
                    this.show11 = false;
                }
                else{
                    this.show11 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_6 = false;
                    this.show11 = false;
                });
    }

    // getActiveInstructors() {
    //     var orgid = "";
    //     if (this.checkrole()) {
    //         orgid = this.urlGetOrgId();
    //     }
    //     this.service.get(this._serviceUrlActiveInstructors, '')
    //         .subscribe(result => {
    //             if (result == "Not found") {
    //                 this.service.notfound();
    //             }
    //             else {
    //                 var ActiveInstructors = JSON.parse(result);
    //                 this.dataActiveInstructors.push(['Intructure', 'Total']);

    //                 for (var i in ActiveInstructors) {
    //                     this.dataActiveInstructors.push([ActiveInstructors[i].Territory_Name, Number(ActiveInstructors[i].TotalActiveInstructor)]);
    //                 }


    //                     this.doughChartActiveInstructure = {
    //                         chartType: 'BarChart',
    //                         dataTable: this.dataActiveInstructors,
    //                         options: {
    //                             legend: { position: 'none' },
    //                             height: 300,
    //                             title: 'Total Active Intructors',
    //                             chartArea: { width: '50%' },
    //                             isStacked: false,
    //                             hAxis: {
    //                                 title: 'Value',
    //                                 minValue: 0,
    //                             },
    //                             vAxis: {
    //                                 title: 'Partner Type'
    //                             },
    //                             colors: ['#2ecc71', '#5faee3']
    //                         },
    //                     };


    //             }
    //         },
    //         error => {
    //             this.messageError = <any>error
    //             this.service.errorserver();
    //         });
    // }

    // getCoursesByMonth() {
    //     var orgid = "";
    //     if (this.checkrole()) {
    //       orgid = this.urlGetOrgId();
    //     }

    //     this.dataCoursesDelivered = [
    //         {
    //             PartnerType: 'ATC',
    //             Total: 0
    //         },
    //         {
    //             PartnerType: 'AAP - Course',
    //             Total: 0
    //         },
    //         {
    //             PartnerType: 'AAP - Project',
    //             Total: 0
    //         },
    //         {
    //             PartnerType: 'AAP - Event',
    //             Total: 0
    //         },
    //     ]

    //     this.service.get("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
    //       .subscribe(result => {
    //         if (result == "Not found") {
    //           this.service.notfound();

    //         }
    //         else {
    //             var CoursesDelivered = JSON.parse(result);

    //             for (var i in CoursesDelivered) {
    //                 if(CoursesDelivered[i].PartnerType == 'ATC'){
    //                     this.dataCoursesDelivered[0].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //                 }
    //                 else if(CoursesDelivered[i].PartnerType == 'AAP Course'){
    //                     this.dataCoursesDelivered[1].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //                 }
    //                 else if(CoursesDelivered[i].PartnerType == 'AAP Project'){
    //                     this.dataCoursesDelivered[2].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //                 }
    //                 else if(CoursesDelivered[i].PartnerType == 'AAP Event'){
    //                     this.dataCoursesDelivered[3].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //                 }
    //             }

    //             var barColor = ["#AD1D28","#DEBB27","#fec04c","#1AB244"];
    //             Morris.Bar({
    //                 element: 'morris-bar-chart',
    //                 data: this.dataCoursesDelivered,
    //                 xkey: 'PartnerType',
    //                 ykeys: ['Total'],
    //                 labels: ['Total'],
    //                 barColors: function (row) {
    //                     return barColor[row.x];
    //                 },
    //                 hideHover: 'auto',
    //                 gridLineColor: '#eef0f2',
    //                 resize: true
    //             });

    //         }
    //       },
    //       error => {
    //         this.messageError = <any>error
    //         this.service.errorserver();
    //       });
    // }


}
