import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardAutodeskAdminComponent } from './dashboard-autodesk-admin.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';

export const DashboardAutodeskAdminRoutes: Routes = [
  {
    path: '',
    component: DashboardAutodeskAdminComponent,
    data: {
      breadcrumb: 'menu_dashboard.dashboard_admin',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardAutodeskAdminRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [DashboardAutodeskAdminComponent]
})
export class DashboardAutodeskAdminModule { }
