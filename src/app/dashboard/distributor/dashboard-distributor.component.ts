import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { AppService } from "../../shared/service/app.service";

@Component({
    selector: 'app-dashboard-distributor',
    templateUrl: './dashboard-distributor.component.html',
    styleUrls: [
        './dashboard-distributor.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardDistributorComponent implements OnInit {
    messageError: string = '';
    donutChartData: any
    doughChartPartnerGeo: any
    doughChartInstructorGeo: any
    doughChartCourseMonth: any
    doughChartPartnerParticipated: any
    doughChartEvaluationMonth: any
    doughChartPerformanceSite: any;

    dataCoursesByMonth = []
    dataEvaluationMonth = []
    dataActivePartner = []
    dataActiveInstructors = [];
    dataCoursesDeliveredByPartners = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    DataPerformanceSite = [];

    dataCoursesDelivered: any;
    show1 : boolean = true;
    show2 : boolean = true;
    show3 : boolean = true;
    show4 : boolean = true;
    show5 : boolean = true;
    show6 : boolean = true;
    show7 : boolean = true;
    show8 : boolean = true;
    show9 : boolean = true;
    show10 : boolean = true;
    show11 : boolean = true;
    private _serviceUrlCoursesByMonth = 'api/DashboardCoursesDelivered';

    private _serviceUrlPerformanceSite = 'api/DashboardPerformanceSite/null';
    private _serviceUrlActiveInstructors = 'api/DashboardActiveInstructors';
    private _serviceUrlCoursesDeliveredByPartners = 'api/DashboardCoursesDeliveredByPartners';

    public loading = false;
    public loading_1 = false;
    public loading_2 = false;
    public loading_3 = false;
    public loading_4 = false;
    public loading_5 = false;
    public loading_6 = false;
    public loading_7 = false;
    public loading_8 = false;
    public loading_9 = false;
    public loading_11 = false;
    public tipeuting: string = "";

    content = ` Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at,`;
    timeline = [
        { caption: '16 Jan', date: new Date('2014, 1, 16'), selected: true, title: 'Horizontal Timeline', content: this.content },
        { caption: '28 Feb', date: new Date('2014, 2, 28'), title: 'Event title here', content: this.content },
        { caption: '20 Mar', date: new Date('2014, 3, 20'), title: 'Event title here', content: this.content },
        { caption: '20 May', date: new Date('2014, 5, 20'), title: 'Event title here', content: this.content },
        { caption: '09 Jul', date: new Date('2014, 7, 9'), title: 'Event title here', content: this.content },
        { caption: '30 Aug', date: new Date('2014, 8, 30'), title: 'Event title here', content: this.content },
        { caption: '15 Sep', date: new Date('2014, 9, 15'), title: 'Event title here', content: this.content },
        { caption: '01 Nov', date: new Date('2014, 11, 1'), title: 'Event title here', content: this.content },
        { caption: '10 Dec', date: new Date('2014, 12, 10'), title: 'Event title here', content: this.content },
        { caption: '29 Jan', date: new Date('2015, 1, 19'), title: 'Event title here', content: this.content },
        { caption: '3 Mar', date: new Date('2015,  3,  3'), title: 'Event title here', content: this.content },
    ];

    /*Bar chart Start*/
    type_NewPartnerByMont = 'horizontalBar';
    data_NewPartnerByMont = {}
    options_NewPartnerByMont = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };


    //Active Partner
    type_ActivePartner = 'horizontalBar';
    data_ActivePartner = {};
    options_ActivePartner = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };


    //Active Instructor
    type_ActiveInstructor = 'horizontalBar';
    data_ActiveInstructor = {}
    options_ActiveInstructor = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom'
        }
    };


    /*Bar chart End*/
    useraccesdata: any;
    constructor(private completerService: CompleterService,private _http: Http,private service: AppService, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    //Get Role Access
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }
    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }
    //Get Role Access

    //add chose org to see distributor dashboard
    PickedOrganization(data){
        this.tampilin=true;
        this.OrgIdNew = '"'+data["originalObject"].OrgId+'"';
        
        setTimeout(() => {
            this.getActiveInstructors();
        }, 1000);

        setTimeout(() => {
            this.getParticipatedEvaSurveyCoreProduct();
        }, 2000);

        setTimeout(() => {
            this.getTotalCoursesDelivered();
        }, 3000);

        setTimeout(() => {
            this.getSitesParticipatedInEvaSurvey();
        }, 4000);

        setTimeout(() => {
            this.getActivePartner();
        }, 5000);

        setTimeout(() => {
            this.getCoursesDelivered();
        }, 6000);

        setTimeout(() => {
            this.getTotalEvaluationSurvey();
        }, 7000);

        setTimeout(() => {
            this.getSiteMeetTheirAnnualReq();
        }, 8000);
        setTimeout(() => {
            this.getSiteMeetTheirAnnualReqAAP();
        }, 9000);
    }

    adminkan:boolean=false;
    tampilin:boolean=true;
    protected dataService: RemoteData;
    OrgIdNew:string="";
    ngOnInit() {

        //add chose org to see distributor dashboard
        if(!this.checkrole()){
            this.adminkan=true;
            this.tampilin=false;

            this.dataService = this.completerService.remote(
                null,
                "Organization",
                "Organization");
              this.dataService.urlFormater(term => {
                  return `api/MainOrganization/FindOrganizationDistributor/`+ term;
              });
              this.dataService.dataField("results");
        }

        if(this.tampilin){
            this.OrgIdNew = this.urlGetOrgId();
            setTimeout(() => {
                this.getActiveInstructors();
            }, 1000);
    
            setTimeout(() => {
                this.getParticipatedEvaSurveyCoreProduct();
            }, 2000);
    
            setTimeout(() => {
                this.getTotalCoursesDelivered();
            }, 3000);
    
            setTimeout(() => {
                this.getSitesParticipatedInEvaSurvey();
            }, 4000);
    
            setTimeout(() => {
                this.getActivePartner();
            }, 5000);
    
            setTimeout(() => {
                this.getCoursesDelivered();
            }, 6000);
    
            setTimeout(() => {
                this.getTotalEvaluationSurvey();
            }, 7000);
    
            setTimeout(() => {
                this.getSiteMeetTheirAnnualReq();
            }, 8000);
            setTimeout(() => {
                this.getSiteMeetTheirAnnualReqAAP();
            }, 9000);
        }


        // setTimeout(() => {
        //   this.getMonthPerformanceSite();
        //   this.getCoursesDelivered();
        //   this.getEvaluationByMonth();
        // }, 5000);


        // this.getPartnerByMonth();
        // this.getCoursesByMonth();
        // this.getActivePartner();
        // this.getEvaluationByMonth()
        // this.setTimeout();
    }

    getTotalEvaluationSurvey() {
        var data: any;
        var orgid = "";
        this.loading_9 = true;
        
        $("#morris-bar-chart-month2").empty(); //kosongin biar gak numpuk
        
        orgid = this.OrgIdNew;
  
        this.data_MonthEvaluationSurverNew = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        var dataFilter =
        {
            "OrgId": orgid
        }

        /* new post for autodesk plan 17 oct */
        // this.service.get("api/DashboardMonthEvaluationSurvey/total/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/TotalEvaSurvey",
            dataFilter
        )
            .subscribe((result) => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show10 = false;
                }
                else {
                    var dataResult = result; //change to post
                    data = dataResult;

                    for (var i in data) {
                        if (data[i].PartnerType == 'ATC') {
                            this.data_MonthEvaluationSurverNew[0].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount +=  Number(data[i].TotalSurveyTaken);
                        }
                        else if (data[i].PartnerType == 'AAP Course') {
                            this.data_MonthEvaluationSurverNew[1].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount +=  Number(data[i].TotalSurveyTaken);
                        }
                        else if (data[i].PartnerType == 'AAP Project') {
                            this.data_MonthEvaluationSurverNew[2].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount +=  Number(data[i].TotalSurveyTaken);
                        }
                        else if (data[i].PartnerType == 'AAP Event') {
                            this.data_MonthEvaluationSurverNew[3].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount +=  Number(data[i].TotalSurveyTaken);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-month2',
                        data: this.data_MonthEvaluationSurverNew,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_9 = false;
                if(tmpCount ==0){
                    this.show10 = false;
                }
                else{
                    this.show10 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_9 = false;
                    this.show10 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    dataTotalCourseDelivered = [];
    getTotalCoursesDelivered() {
        var orgid = "";
        this.loading_8 = true;
        
        $("#morris-bar-chart-partner2").empty(); //kosongin biar gak numpuk
        
        orgid = this.OrgIdNew;

        var dataFilter =
        {
            "OrgId": orgid
        }

        this.dataTotalCourseDelivered = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        /* new post for autodesk plan 17 oct */
        // this.service.get("api/DashboardCoursesByMonth/totalCourseByPartner/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/CourseDeliveredByPartner",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show9 = false;
                }
                else {
                    var dataResult = result; //change to post
                    var CoursesDelivered = dataResult;

                    for (var i in CoursesDelivered) {
                        if (CoursesDelivered[i].PartnerType == 'ATC') {
                            this.dataTotalCourseDelivered[0].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Course') {
                            this.dataTotalCourseDelivered[1].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Project') {
                            this.dataTotalCourseDelivered[2].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Event') {
                            this.dataTotalCourseDelivered[3].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-partner2',
                        data: this.dataTotalCourseDelivered,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_8 = false;
                if(tmpCount ==0){
                    this.show9 = false;
                }
                else{
                    this.show9 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_8 = false;
                    this.show9 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    // baru site performance buat api nya
    getMonthPerformanceSite() {
        var data = '';

        var orgid = "";
        
        
        orgid = this.OrgIdNew;

        this._serviceUrlPerformanceSite.replace("null", orgid);

        this.service.httpClientGet(this._serviceUrlPerformanceSite, data)
            .subscribe(result => {
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                }
                else {
                    var PerformanceSite = result;
                    this.DataPerformanceSite.push(['Task', 'Hours per Day']);
                    for (var i in PerformanceSite) {
                        this.DataPerformanceSite.push([PerformanceSite[i].geo_name, Number(PerformanceSite[i].TotalPerformanceSite)]);
                    }
                    this.doughChartPerformanceSite = {
                        chartType: 'PieChart',
                        dataTable: this.DataPerformanceSite,
                        options: {
                            height: 360,
                            title: 'Evaluation Survey Taken By Month',
                            pieHole: 0.4,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ],
                            sliceVisibilityThreshold :0,
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                        },
                    };
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    getActiveInstructors() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        this.loading = true;
        var orgid = "";
        
        
        orgid = this.OrgIdNew;

        // for (var k = 0; k < roleid.length; k++) {
        this.service.httpClientGet("api/DashboardActiveInstructors/All/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show1 = false;
                }
                else {
                    data = result;
                    // console.log(data);
                    teritorryarr = [];
                    var partners_1 = [];
                    var partners_2 = [];
                    var partners_3 = [];
                    var aap = [];
                    var atc = [];
                    var mtp = [];

                    /*limit 10 biar gak kebanyakan country nya (jadi rarengkep)*/

                    var length = data.length;

                    if (length > 10) {
                        length = 10;
                    }

                    /*end limit 10 biar gak kebanyakan country nya (jadi rarengkep)*/
                    // for (var i = 0; i < data.length; i++) {
                    //     if (orgid != "") {
                    //         teritorryarr.push(data[i].countries_name);
                    //     } else {
                    //         teritorryarr.push(data[i].Territory_Name);
                    //     }
                    // }


                    for (var i = 0; i < data.length; i++) {
                        teritorryarr.push(data[i].Territory_Name);
                    }

                    var uniq = [];
                    for (let n = 0; n < teritorryarr.length; n++) {
                        if (uniq.indexOf(teritorryarr[n]) == -1) {
                            uniq.push(teritorryarr[n]);
                        }
                    }
                    // console.log(uniq);
                    var teri_anyar = [];

                    for (let u = 0; u < length; u++) {
                        if (uniq[u] != undefined) {
                            teri_anyar.push(uniq[u]);
                        }
                    }
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == "ATC") {
                            partners_1.push(+data[i].TotalActiveInstructor);
                            atc.push({
                                label: data[i].RoleCode,
                                data: partners_1,
                                backgroundColor: data[i].BackgroundColor
                            });
                            tmpCount += data[i].TotalActiveInstructor;
                        }
                    }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == "AAP") {
                            partners_2.push(+data[i].TotalActiveInstructor);
                            aap.push({
                                label: data[i].RoleCode,
                                data: partners_2,
                                backgroundColor: data[i].BackgroundColor
                            });
                            tmpCount += data[i].TotalActiveInstructor;
                        }
                    }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == "MTP") {
                            partners_3.push(+data[i].TotalActiveInstructor);
                            mtp.push({
                                label: data[i].RoleCode,
                                data: partners_3,
                                backgroundColor: data[i].BackgroundColor
                            });
                            tmpCount += data[i].TotalActiveInstructor;
                        }
                    }

                    var jek = [];
                    if (atc.length != 0) {
                        jek.push(atc[0]);
                    }

                    if (aap.length != 0) {
                        jek.push(aap[0]);
                    }

                    if (mtp.length != 0) {
                        jek.push(mtp[0]);
                    }

                    // dataset.push({
                    //     label: ["ATC","AAP","MTP"],
                    //     data: partners,
                    //     backgroundColor: data[0].BackgroundColor
                    // })

                    this.data_ActiveInstructor = {
                        labels: teri_anyar,
                        datasets: jek
                    }

                    // console.log(this.data_ActiveInstructor);
                }
                this.loading = false;
                if(tmpCount ==0){
                    this.show1 = false;
                }
                else{
                    this.show1 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                    this.show1 = false;
                });
        // }

        // this.service.get("api/DashboardActiveInstructors/where/{'OrgId':'" + orgid + "'}", '')
        //   .subscribe(result => {
        //     if (result == "Not found") {
        //       this.service.notfound();
        //     }
        //     else {
        //       data = JSON.parse(result);
        //       var partnersATC = [];
        //       var partnersAAP = [];
        //       var partnersCTC = [];
        //       var countriesName = []
        //       var backgroundColorATC = []
        //       var backgroundColorAAP = []
        //       var backgroundColorMTP = []
        //       for (var i = 0; i < data.length; i++) {
        //         if (data[i].RoleCode == 'ATC') {
        //          partnersATC.push(data[i].TotalActiveInstructor);
        //          partnersAAP.push(0);
        //          partnersCTC.push(0)
        //         }
        //         else if (data[i].RoleCode == 'AAP') {
        //          partnersATC.push(0);
        //          partnersAAP.push(data[i].TotalActiveInstructor);
        //          partnersCTC.push(0)
        //         } else if (data[i].RoleCode == 'CTC') {
        //          partnersATC.push(0);
        //          partnersAAP.push(0);
        //          partnersCTC.push(data[i].TotalActiveInstructor)
        //         } else {
        //          partnersATC.push(0);
        //          partnersAAP.push(0);
        //          partnersCTC.push(0)
        //         }

        //         countriesName.push(data[i].Label);
        //         backgroundColorATC.push("#AD1D28")
        //         backgroundColorAAP.push("#DEBB27")
        //         backgroundColorMTP.push("#1AB244")

        //        // Set Into Chart
        //        this.data_ActiveInstructor = {
        //          labels: countriesName,
        //          datasets: [{
        //            label: 'ATC',
        //            data: partnersATC,
        //            backgroundColor: backgroundColorATC
        //          },
        //          {
        //            label: 'AAP',
        //            data: partnersAAP,
        //            backgroundColor: backgroundColorAAP
        //          },
        //          {
        //            label: 'MTP',
        //            data: partnersCTC,
        //            backgroundColor: backgroundColorMTP
        //          }]
        //        }
        //       }
        //     }
        //   },
        //   error => {
        //     this.service.errorserver();
        //   });
    }

    dataATCParticipatedEvaSurvey = [];
    dataAAPParticipatedEvaSurvey = [];
    doughATCParticipatedEvaSurvey: any;
    doughAAPParticipatedEvaSurvey: any;
    getParticipatedEvaSurveyCoreProduct() {
        var orgid = "";
        this.loading_2 = true;
        
        
        orgid = this.OrgIdNew;

        var dataFilter =
        {
            "OrgId": orgid
        }
        
        /* new post for autodesk plan 17 oct */
        this.dataAAPParticipatedEvaSurvey = [];
        // this.service.get("api/DCPAAC/AAP/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/CoreProductAAP",
            dataFilter
        )
            .subscribe(result => {
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show3 = false;
                }
                else {
                    var dataResult = result; //change to post

                    var dataParse = dataResult;
                    this.dataAAPParticipatedEvaSurvey.push(['Product Name', 'Total']);

                    for (var i in dataParse) {
                        this.dataAAPParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                    }

                    // if (dataParse == "") {
                    //     this.dataAAPParticipatedEvaSurvey.push([' ', 1])
                    // }

                    this.doughAAPParticipatedEvaSurvey = {
                        chartType: 'PieChart',
                        dataTable: this.dataAAPParticipatedEvaSurvey,
                        options: {
                            height: 360,
                            title: 'AAP',
                            pieHole: 0.4,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ],
                            sliceVisibilityThreshold :0,
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                        },
                    };
                }
                this.loading_2 = false;
                if(this.dataAAPParticipatedEvaSurvey.length <=1){
                    this.show3 = false;
                }
                else{
                    this.show3 = true;
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_2 = false;
                    this.show3 = false;
                });
        /* new post for autodesk plan 17 oct */

        this.loading_1 = true;
        this.dataATCParticipatedEvaSurvey = [];
        setTimeout(() => {
            /* new post for autodesk plan 17 oct */
            // this.service.get("api/DCPAAC/ATC/where/{'OrgId':'" + orgid + "'}", '')
            this.service.httpClientPost( //change to post
                "api/DashboardDistributor/CoreProductATC",
                dataFilter
            )
                .subscribe(result => {
                    if (result == "Not found" || result == null || result == ' ') { //change to post
                        this.service.notfound();
                        this.show2 = false;
                    }
                    else {
                        var dataResult = result; //change to post

                        var dataParse = dataResult;
                        this.dataATCParticipatedEvaSurvey.push(['Product Name', 'Total']);

                        for (var i in dataParse) {
                            this.dataATCParticipatedEvaSurvey.push([dataParse[i].productName, Number(dataParse[i].TotalCourseEvaCoreProduct)]);
                        }

                        // if (dataParse == "") {
                        //     this.dataATCParticipatedEvaSurvey.push([' ', 1])
                        // }

                        this.doughATCParticipatedEvaSurvey = {
                            chartType: 'PieChart',
                            dataTable: this.dataATCParticipatedEvaSurvey,
                            options: {
                                height: 360,
                                title: 'ATC',
                                pieHole: 0.4,
                                colors: [
                                    '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                                ],
                                sliceVisibilityThreshold :0,
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                            },
                        };
                    }
                    this.loading_1 = false;
                    if(this.dataATCParticipatedEvaSurvey.length <=1){
                        this.show2 = false;
                    }
                    else{
                        this.show2 = true;
                    }
                },
                    error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                        this.loading_1 = false;
                        this.show2 = false;
                    });
            /* new post for autodesk plan 17 oct */
        }, 3000);
    }

    type1 = "horizontalBar";

    optionsBar = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                ticks: {
                    min: 0,
                    stepSize: 100
                }
            }]
        }
    };

    dataSiteMeetTheirAnnualReq = {};
    getSiteMeetTheirAnnualReq() {
        var data: any;
        var meetnotmeet = ">=,<,0";
        var teritorryarr = [];
        var dataset = [];
        var counter: number = 0;
        this.loading_3 = true;
        var orgid = "";
        
        orgid = this.OrgIdNew;

        //for (var k = 0; k < meetnotmeet.length; k++) {
            // this.service.get("api/DashboardSitesMeetAnnualReq/where/{'OrgId':'" + orgid + "','MeetNotMeet':'" + meetnotmeet[k] + "'}", '')
            //     .subscribe(result => {
            //         if (result == "Not found") {
            //             this.service.notfound();
            //         }
            //         else {

            var dataFilter = //change to post
            {
                "OrgId": orgid,
                "MeetNotMeet":meetnotmeet
               // "MeetNotMeet":meetnotmeet[k]
            }

            /* new post for autodesk plan 17 oct */
            this.service.httpClientPost( //change to post
                "api/DashboardDistributor/AnualReq",
                dataFilter
            )
                .subscribe(result => {
                    let tmpCount : any= 0;
                    if (result == "Not found" || result == null || result == ' ') { //change to post
                        this.service.notfound();
                        this.show4 = false;
                    }
                    else {
                        var dataResult = result; //change to post
                        data = dataResult;
                        for (var i = 0; i < data.length; i++) {
                            tmpCount += data[i].TotalCount + data[i].perSiteMeet + data[i].perSiteNotMeet;
                        }
                        var tmpData = data,
                        config = {
                            element: 'sitemeetATC',
                            data: tmpData,
                            xkey: 'Territory_Name',
                            ykeys: ['perSiteMeet', 'perSiteNotMeet'],
                            labels: ['Site Meet'+'(%)', 'Site Does not Meet'+'(%)'],
                            fillOpacity: 0.6,
                            hideHover: 'auto',
                            behaveLikeLine: true,
                            resize: true,
                            barColors:['#AD1D28','#DEBB27'],
                            pointFillColors:['#ffffff'],
                            pointStrokeColors: ['black'],
                            lineColors:['gray','red'],
                           stacked : true,
                            //stacked100: true,
                            //xLabelAngle: 90,
 
                        }
                        config.element = 'sitemeetATC';
                        config.stacked = true;
                        Morris.Bar(config);
                    }
                        // teritorryarr = [];
                        // var partners = [];

                        // for (var i = 0; i < data.length; i++) {
                        //     partners.push(+data[i].Count);
                        //     if (+data[i].Count > 0) {
                        //         teritorryarr.push(data[i].Territory_Name);
                        //     }
                        //     tmpCount += data[i].Count;
                        // }

                        // if(data.length > 0){
                        //     dataset.push({
                        //         label: data[0].Label,
                        //         data: partners,
                        //         backgroundColor: data[0].BackgroundColor
                                
                        //     })
                        //     this.dataSiteMeetTheirAnnualReq = {
                        //         labels: teritorryarr,
                        //         datasets: dataset,
                        //         stacked :  true,
                        //         xkey: 'y',
                        //         ykeys: ['a', 'b' , 'c'],
                        //     }
                        // }
                        // else{
                        //     this.dataSiteMeetTheirAnnualReq = {
                        //         labels: "",
                        //         datasets: []
                        //     }
                       // }


                        // counter += this.dataSiteMeetTheirAnnualReq["labels"].length;
                        // if(counter <= 4){
                        //     this.tipeuting = "Bar";
                        // } else{
                        //     this.tipeuting = "horizontalBar";
                        // }
                        // console.log(this.dataSiteMeetTheirAnnualReq["labels"].length);
                    //}
                    this.loading_3 = false;
                    if(tmpCount ==0){
                        this.show4 = false;
                    }
                    else{
                        this.show4 = true;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.loading_3 = false;
                        this.show4 = false;
                    });
            /* new post for autodesk plan 17 oct */
        //}
        this.loading_3 = false;
    }
    getSiteMeetTheirAnnualReqAAP() {
        var data: any;
        var meetnotmeet = ">=,<,0";
        this.loading_11 = true;
        var orgid = "";
        orgid = this.OrgIdNew;

            var dataFilter = //change to post
            {
                "OrgId": orgid,
                "MeetNotMeet":meetnotmeet
            }

            /* new post for autodesk plan 17 oct */
            this.service.httpClientPost( //change to post
                "api/DashboardDistributor/AnualReqAAP",
                dataFilter
            )
                .subscribe(result => {
                    let tmpCount : any= 0;
                    if (result == "Not found" || result == null || result == ' ') { //change to post
                        this.service.notfound();
                        this.show4 = false;
                    }
                    else {
                        var dataResult = result; //change to post
                        data = dataResult;
                        for (var i = 0; i < data.length; i++) {
                            tmpCount += data[i].TotalCount + data[i].perSiteMeet + data[i].perSiteNotMeet;
                        }
                        var tmpData = data,
                        config = {
                            element: 'sitemeetAAP',
                            data: tmpData,
                            xkey: 'Territory_Name',
                            ykeys: ['perSiteMeet', 'perSiteNotMeet'],
                            labels: ['Site Meet'+'(%)', 'Site Does not Meet'+'(%)'],
                            fillOpacity: 0.6,
                            hideHover: 'auto',
                            behaveLikeLine: true,
                            resize: true,
                            barColors:['#AD1D28','#DEBB27'],
                            pointFillColors:['#ffffff'],
                            pointStrokeColors: ['black'],
                            lineColors:['gray','red'],
                            stacked : true,
                            //xLabelAngle: 90,
 
                        }
                        config.element = 'sitemeetAAP';
                        config.stacked = true;
                        Morris.Bar(config);
                    }
  
                    this.loading_11 = false;
                    if(tmpCount ==0){
                        this.show11 = false;
                    }
                    else{
                        this.show11 = true;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.loading_11 = false;
                        this.show11 = false;
                    });
            /* new post for autodesk plan 17 oct */
        
        this.loading_11 = false;
    }
    dataCoursesDeliveredNew: any;
    getCoursesDelivered() {
        var orgid = "";
        this.loading_7 = true;
        
        $("#morris-bar-chart-partner").empty(); //kosongin biar gak numpuk
        
        orgid = this.OrgIdNew;

        var dataFilter =
        {
            "OrgId": orgid
        }

        this.dataCoursesDeliveredNew = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        /* new post for autodesk plan 17 oct */
        // this.service.get("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/CourseDeliveredByPartnerMonth",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0; 
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show8 = false;
                }
                else {
                    var dataResult = result; //change to post
                    var CoursesDelivered = dataResult;

                    for (var i in CoursesDelivered) {
                        if (CoursesDelivered[i].PartnerType == 'ATC') {
                            this.dataCoursesDeliveredNew[0].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Course') {
                            this.dataCoursesDeliveredNew[1].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Project') {
                            this.dataCoursesDeliveredNew[2].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Event') {
                            this.dataCoursesDeliveredNew[3].Total = Number(CoursesDelivered[i].course);
                            tmpCount += Number(CoursesDelivered[i].course);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-partner',
                        data: this.dataCoursesDeliveredNew,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_7 = false;
                if(tmpCount == 0){
                    this.show8 = false;
                }
                else{
                    this.show8 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_7 = false;
                    this.show8 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    data1 = {};
    getActivePartner() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        this.loading_6 = true;
        var orgid = "";
        
        
        orgid = this.OrgIdNew;

        for (var k = 0; k < roleid.length; k++) {
            this.service.httpClientGet("api/DashboardDistributor/ActivePartner/{'OrgId':'" + orgid + "','RoleId':'" + roleid[k] + "'}", '')
                .subscribe(result => {
                    let tmpCount : any= 0;
                    if (result == "Not found" || result == null || result == ' ') {
                        this.service.notfound();
                        this.show7 = false;
                    }
                    else {
                        data = result;
                        teritorryarr = [];
                        var partners = [];

                        for (var i = 0; i < data.length; i++) {
                            partners.push(+data[i].CountPartners);
                            teritorryarr.push(data[i].Territory_Name);
                            tmpCount += data[i].CountPartners;
                        }

                        if(data.length > 0){
                            dataset.push({
                                label: data[0].RoleCode,
                                data: partners,
                                backgroundColor: data[0].BackgroundColor
                            })
    
                            this.data1 = {
                                labels: teritorryarr,
                                datasets: dataset
                            }
                        }
                        // else{
                        //     this.data1 = {
                        //         labels: "",
                        //         datasets: []
                        //     }
                        // }
                    }
                
                    this.loading_6 = false;
                    if(tmpCount ==0){
                        this.show7 = false;
                    }
                    else{
                        this.show7 = true;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.loading_6 = false;
                        this.show7 = false;
                    });
        }
        this.loading_6 = false;
    }

    data_MonthEvaluationSurverNew: any = "";
    getEvaluationByMonth() {
        // start NEW PARTNERS BY MONTH
        var data: any;
        var orgid = "";

        
        
        orgid = this.OrgIdNew;

        this.data_MonthEvaluationSurverNew = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        this.service.httpClientGet("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                }
                else {
                    data = result;

                    for (var i in data) {
                        if (data[i].PartnerType == 'ATC') {
                            this.data_MonthEvaluationSurverNew[0].Total = Number(data[i].TotalSurveyTaken)
                        }
                        else if (data[i].PartnerType == 'AAP Course') {
                            this.data_MonthEvaluationSurverNew[1].Total = Number(data[i].TotalSurveyTaken)
                        }
                        else if (data[i].PartnerType == 'AAP Project') {
                            this.data_MonthEvaluationSurverNew[2].Total = Number(data[i].TotalSurveyTaken)
                        }
                        else if (data[i].PartnerType == 'AAP Event') {
                            this.data_MonthEvaluationSurverNew[3].Total = Number(data[i].TotalSurveyTaken)
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-month',
                        data: this.data_MonthEvaluationSurverNew,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });
    }

    dataSitesParticipatedInEvaSurvey = [];
    dataSitesParticipatedInEvaSurvey2 = [];
    doughSitesInEvaSurvey: any;
    doughSitesInEvaSurvey2: any;
    getSitesParticipatedInEvaSurvey() {
        var orgid = "";
        this.loading_5 = true;
        
        
        orgid = this.OrgIdNew;

        var dataFilter =
        {
            "OrgId": orgid
        }

        
        /* new post for autodesk plan 17 oct */
        this.dataSitesParticipatedInEvaSurvey = [];
        // this.service.get("api/DashboardSitesInEvaSurvey/AAP/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/SitesParticipatedAAP",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show6 = false;
                }
                else {
                    var dataResult :any= result; //change to post
                    for (var i = 0; i < dataResult.length; i++) {
                        tmpCount += dataResult[i].TotalSitesEval + dataResult[i].NonCount;
                    }
                    var tmpData = dataResult,
                    config = {
                        element: 'SitesParticipatedAAP',
                        data: tmpData,
                        xkey: 'Territory_Name',
                        ykeys: ['TotalSitesEval', 'NonCount'],
                        labels: ['Sites Participated', 'Sites Do not Participate'],
                        fillOpacity: 0.2,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        barColors:['#AD1D28','#DEBB27'],
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:['gray','red'],
                       stacked : true,
                        //stacked100: true,
                       // xLabelAngle: 90,

                    }
                    config.element = 'SitesParticipatedAAP';
                    config.stacked = true;
                    Morris.Bar(config);
                    this.loading_5 = false;
                }
                if(tmpCount ==0){
                    this.show6 = false;
                }
                else{
                    this.show6 = true;
                }
                //     var totalSiteEva = 0;
                //     var totalSiteUnEva = 0;

                //     if (dataResult != "") {
                //         var dataParse = dataResult;
                //         totalSiteEva = Number(dataParse[0].TotalSitesEval);
                //         totalSiteUnEva = Number(dataParse[1].TotalSitesEval);
                //     }

                //     this.dataSitesParticipatedInEvaSurvey.push(['Site Participate', 'Total']);

                //     this.dataSitesParticipatedInEvaSurvey.push(["Site Participate", totalSiteEva]);
                //     this.dataSitesParticipatedInEvaSurvey.push(["Site Non Participate", totalSiteUnEva]);

                //     this.doughSitesInEvaSurvey = {
                //         chartType: 'PieChart',
                //         dataTable: this.dataSitesParticipatedInEvaSurvey,
                //         options: {
                //             height: 360,
                //             title: 'AAP',
                //             pieHole: 0.4
                //         },
                //         sliceVisibilityThreshold :0,
                //             lang: {
                //                 noData: "No Data Available",
                //             },
                //             noData: {
                //                 style: {
                //                     fontWeight: 'bold',
                //                     fontSize: '15px',
                //                 }
                //             }
                //     };
                // }
                // this.loading_5 = false;
                // if(this.dataSitesParticipatedInEvaSurvey.length <=1){
                //     this.show6 = false;
                // }
                // else{
                //     this.show6 = true;
                // }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_5 = false;
                    this.show6 = false;
                });
        /* new post for autodesk plan 17 oct */

        
        /* new post for autodesk plan 17 oct */
        this.loading_4 = true;
        this.dataSitesParticipatedInEvaSurvey2 = [];
        // this.service.get("api/DashboardSitesInEvaSurvey/ATC/where/{'OrgId':'" + orgid + "'}", '')
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/SitesParticipatedATC",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show5 = false;
                }
                else {
                    var dataResult:any = result; //change to post
                    for (var i = 0; i < dataResult.length; i++) {
                        tmpCount += dataResult[i].TotalSitesEval+dataResult[i].NonCount;
                    }
                    var tmpData = dataResult,
                    config = {
                        element: 'SitesParticipatedATC',
                        data: tmpData,
                        xkey: 'Territory_Name',
                        ykeys: ['TotalSitesEval', 'NonCount'],
                        labels: ['Sites Participated', 'Sites Do not Participate'],
                        fillOpacity: 0.2,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        barColors:['#AD1D28','#DEBB27'],
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:['gray','red'],
                       stacked : true,
                        //stacked100: true,
                        //xLabelAngle: 90,

                    }
                    config.element = 'SitesParticipatedATC';
                    config.stacked = true;
                    Morris.Bar(config);
                }
                if(tmpCount ==0){
                    this.show5 = false;
                }
                else{
                    this.show5 = true;
                }
                this.loading_4 = false;
                //     var totalSiteEva = 0;
                //     var totalSiteUnEva = 0; 

                //     if (dataResult != "") {
                //         var dataParse = dataResult;
                //         totalSiteEva = Number(dataParse[0].TotalSitesEval);
                //         totalSiteUnEva = Number(dataParse[1].TotalSitesEval);
                //     }

                //     this.dataSitesParticipatedInEvaSurvey2.push(['Site Participate', 'Total']);

                //     this.dataSitesParticipatedInEvaSurvey2.push(["Site Participate", totalSiteEva]);
                //     this.dataSitesParticipatedInEvaSurvey2.push(["Site Non Participate", totalSiteUnEva]);

                //     this.doughSitesInEvaSurvey2 = {
                //         chartType: 'PieChart',
                //         dataTable: this.dataSitesParticipatedInEvaSurvey2,
                //         options: {
                //             height: 360,
                //             title: 'ATC',
                //             pieHole: 0.4
                //         },
                //         sliceVisibilityThreshold :0,
                //             lang: {
                //                 noData: "No Data Available",
                //             },
                //             noData: {
                //                 style: {
                //                     fontWeight: 'bold',
                //                     fontSize: '15px',
                //                 }
                //             }
                //     };
                // }
                // this.loading_4 = false;
                // if(this.dataSitesParticipatedInEvaSurvey2.length <=1){
                //     this.show5 = false;
                // }
                // else{
                //     this.show5 = true;
                // }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                    this.loading_4 = false;
                    this.show5 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    // getPartnerByMonth() {
    //   // start NEW PARTNERS BY MONTH
    //   var data: any;
    //   var roleid = ["60", "58", "1"];
    //   var teritorryarr = [];
    //   var dataset = [];

    //   var orgid = "";
    //   if (this.checkrole()) {
    //     orgid = this.urlGetOrgId();
    //   }

    //   for (var k = 0; k < roleid.length; k++) {
    //     this.service.get("api/DashboardPartner/Month/where/{'OrgId':'" + orgid + "','RoleId':'" + roleid[k] + "'}", '')
    //       .subscribe(result => {
    //         if (result == "Not found") {
    //           this.service.notfound();
    //         }
    //         else {
    //           data = JSON.parse(result);
    //           teritorryarr = [];
    //           var partners = [];
    //           for (var i = 0; i < data.length; i++) {
    //             partners.push(data[i].CountPartners);
    //             teritorryarr.push(data[i].Territory_Name);
    //           }
    //           dataset.push({
    //             label: data[0].RoleCode,
    //             data: partners,
    //             backgroundColor: data[0].BackgroundColor
    //           })
    //         }
    //       },
    //       error => {
    //         this.service.errorserver();
    //       });
    //   }
    //   setTimeout(() => {
    //     this.data_NewPartnerByMont = {
    //       labels: teritorryarr,
    //       datasets: dataset
    //     }
    //   }, 5000);
    // }

    // getCoursesByMonth() {
    //   var orgid = "";
    //   if (this.checkrole()) {
    //     orgid = this.urlGetOrgId();
    //   }

    //   this.dataCoursesDelivered = [
    //     {
    //         PartnerType: 'ATC',
    //         Total: 0
    //     },
    //     {
    //         PartnerType: 'AAP - Course',
    //         Total: 0
    //     },
    //     {
    //         PartnerType: 'AAP - Project',
    //         Total: 0
    //     },
    //     {
    //         PartnerType: 'AAP - Event',
    //         Total: 0
    //     },
    //   ]

    //   this.service.get("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
    //     .subscribe(result => {
    //       if (result == "Not found") {
    //         this.service.notfound();
    //       }
    //       else {
    //         var CoursesDelivered = JSON.parse(result);

    //         for (var i in CoursesDelivered) {
    //           if(CoursesDelivered[i].PartnerType == 'ATC'){
    //               this.dataCoursesDelivered[0].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //           }
    //           else if(CoursesDelivered[i].PartnerType == 'AAP Course'){
    //               this.dataCoursesDelivered[1].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //           }
    //           else if(CoursesDelivered[i].PartnerType == 'AAP Project'){
    //               this.dataCoursesDelivered[2].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //           }
    //           else if(CoursesDelivered[i].PartnerType == 'AAP Event'){
    //               this.dataCoursesDelivered[3].Total = Number(CoursesDelivered[i].TotalCourseMonthByPartnerType)
    //           }
    //         }

    //         var barColor = ["#AD1D28","#DEBB27","#fec04c","#1AB244"];
    //         Morris.Bar({
    //           element: 'morris-bar-chart',
    //           data: this.dataCoursesDelivered,
    //           xkey: 'PartnerType',
    //           ykeys: ['Total'],
    //           labels: ['Total'],
    //           barColors: function (row) {
    //             return barColor[row.x];
    //           },
    //           hideHover: 'auto',
    //           gridLineColor: '#eef0f2',
    //           resize: true,
    //           xLabelMargin: 10
    //         });
    //       }
    //     },
    //     error => {
    //       this.messageError = <any>error
    //       this.service.errorserver();
    //     });
    // }

    // getActivePartner() {
    //   // start NEW PARTNERS BY MONTH
    //   var data: any;
    //   var roleid = ["60", "58", "1"];
    //   var teritorryarr = [];
    //   var dataset = [];

    //   var orgid = "";
    //   if (this.checkrole()) {
    //     orgid = this.urlGetOrgId();
    //   }

    //   this.service.get("api/DashboardActivePartner/where/{'DistributorOrgId':'" + orgid + "'}", '')
    //     .subscribe(result => {
    //       if (result == "Not found") {
    //         this.service.notfound();
    //       }
    //       else {
    //         data = JSON.parse(result);
    //         teritorryarr = [];
    //         var partnersATC = [];
    //         var partnersAAP = [];
    //         var partnersCTC = [];
    //         var countriesName = []
    //         var backgroundColorATC = []
    //         var backgroundColorAAP = []
    //         var backgroundColorMTP = []

    //         for (var i = 0; i < data.length; i++) {
    //           if (data[i].RoleCode == 'ATC') {
    //             partnersATC.push(data[i].TotalPartnerThisMonth);
    //             partnersAAP.push(0);
    //             partnersCTC.push(0)
    //           }

    //           else if (data[i].RoleCode == 'AAP') {
    //             partnersATC.push(0);
    //             partnersAAP.push(data[i].TotalPartnerThisMonth);
    //             partnersCTC.push(0)

    //           } else if (data[i].RoleCode == 'CTC') {
    //             partnersATC.push(0);
    //             partnersAAP.push(0);
    //             partnersCTC.push(data[i].TotalPartnerThisMonth)
    //           }

    //           countriesName.push(data[i].countries_name);
    //           backgroundColorATC.push("#AD1D28")
    //           backgroundColorAAP.push("#DEBB27")
    //           backgroundColorMTP.push("#1AB244")
    //           this.data_ActivePartner = {
    //             labels: countriesName,
    //             datasets: [{
    //               label: 'ATC',
    //               data: partnersATC,
    //               backgroundColor: backgroundColorATC
    //             },
    //             {
    //               label: 'AAP',
    //               data: partnersAAP,
    //               backgroundColor: backgroundColorAAP
    //             },
    //             {
    //               label: 'MTP',
    //               data: partnersCTC,
    //               backgroundColor: backgroundColorMTP
    //             }]
    //           }
    //         }
    //       }
    //     },
    //     error => {
    //       this.service.errorserver();
    //     });
    // }

    // getEvaluationByMonth() {
    //   var orgid = "";
    //   if (this.checkrole()) {
    //     orgid = this.urlGetOrgId();
    //   }

    //   this.dataEvaluationMonth = [
    //     {
    //         PartnerType: 'ATC',
    //         Total: 0
    //     },
    //     {
    //         PartnerType: 'AAP - Course',
    //         Total: 0
    //     },
    //     {
    //         PartnerType: 'AAP - Project',
    //         Total: 0
    //     },
    //     {
    //         PartnerType: 'AAP - Event',
    //         Total: 0
    //     },
    //   ]

    //   this.service.get("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
    //     .subscribe(result => {
    //       if (result == "Not found") {
    //         this.service.notfound();

    //       }
    //       else {
    //         var EvaByMonth = JSON.parse(result);

    //         for (var i in EvaByMonth) {
    //           if(EvaByMonth[i].PartnerType == 'ATC'){
    //               this.dataEvaluationMonth[0].Total = Number(EvaByMonth[i].TotalSurveyTaken)
    //           }
    //           else if(EvaByMonth[i].PartnerType == 'AAP Course'){
    //               this.dataEvaluationMonth[1].Total = Number(EvaByMonth[i].TotalSurveyTaken)
    //           }
    //           else if(EvaByMonth[i].PartnerType == 'AAP Project'){
    //               this.dataEvaluationMonth[2].Total = Number(EvaByMonth[i].TotalSurveyTaken)
    //           }
    //           else if(EvaByMonth[i].PartnerType == 'AAP Event'){
    //               this.dataEvaluationMonth[3].Total = Number(EvaByMonth[i].TotalSurveyTaken)
    //           }
    //         }

    //         var barColor = ["#AD1D28","#DEBB27","#fec04c","#1AB244"];

    //         Morris.Bar({
    //           element: 'id-evaluation-by-month',
    //           data: this.dataEvaluationMonth,
    //           xkey: 'PartnerType',
    //           ykeys: ['Total'],
    //           labels: ['Total'],
    //           barColors: function (row) {
    //             return barColor[row.x];
    //           },
    //           hideHover: 'auto',
    //           gridLineColor: '#eef0f2',
    //           resize: true,
    //           xLabelMargin: 10
    //         });



    //       }
    //     },
    //     error => {
    //       this.messageError = <any>error
    //       this.service.errorserver();
    //     });
    // }

    // private _serviceUrlPartnerSurvey = "api/DashboardPartnerSurvey";
    // dataPartnerSurvey = [];
    // countPartner: number = 0;
    // countPartnerString: String = "";
    // getPartnerSurvey() {
    //   //get data action
    //   var data = '';
    //   this.service.get(this._serviceUrlPartnerSurvey, data)
    //     .subscribe(result => {
    //       if (result == "Not found") {
    //         this.dataPartnerSurvey = ['Task', 'Hours per Day'];
    //         this.service.notfound();
    //       }
    //       else {
    //         var PartnerSurvey = JSON.parse(result);
    //         this.dataPartnerSurvey.push(['Task', 'Hours per Day']);
    //         for (var i in PartnerSurvey) {
    //           this.dataPartnerSurvey.push([PartnerSurvey[i].RoleName, Number(PartnerSurvey[i].TotalPartnersOnSurvey)]);
    //           this.countPartner = +PartnerSurvey[i].TotalPartnersOnSurvey + this.countPartner;
    //         }
    //         this.countPartnerString = this.countPartner.toString();
    //       }
    //     },
    //     error => {
    //       this.dataPartnerSurvey = ['Task', 'Hours per Day'];
    //       this.service.errorserver();
    //     });
    // }

    // hoverPercent() {
    //   this.countPartnerString = this.countPartner.toString() + "%";
    // }

    // unhoverPercent() {
    //   this.countPartnerString = this.countPartner.toString();
    // }

    // getCoursesDeliveredByPartners() {
    //   //get data action
    //   var data = '';
    //   this.service.get(this._serviceUrlCoursesDeliveredByPartners, data)
    //     .subscribe(result => {
    //       if (result == "Not found") {
    //         this.service.notfound();
    //       }
    //       else {
    //         var CoursesDeliveredByPartners = JSON.parse(result);
    //         for (var i in CoursesDeliveredByPartners) {
    //           this.dataCoursesDeliveredByPartners.push([CoursesDeliveredByPartners[i].KeyValue, Number(CoursesDeliveredByPartners[i].TotalCourseMonthByPartnerType)]);
    //           this.dataKeyValueCoursesDeliveredByPartners.push([CoursesDeliveredByPartners[i].KeyValue])
    //         }
    //       }
    //     },
    //     error => {
    //       this.messageError = <any>error
    //       this.service.errorserver();
    //     });
    // }


    // setTimeout() {
    //   setTimeout(() => {
    //     const chart = c3.generate({
    //       bindto: '#chart4',
    //       data: {
    //         columns: this.dataCoursesDeliveredByPartners,
    //         xkey: ['Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017'],
    //         type: 'bar',
    //         colors: {
    //           data1: '#03A9F3',
    //           data2: '#FEC107',
    //         },
    //         groups: this.dataKeyValueCoursesDeliveredByPartners
    //       }
    //     });
    //     $('.resource-barchart1').sparkline([5, 6, 9, 7, 8, 4, 6], {
    //       type: 'bar',
    //       barWidth: '6px',
    //       height: '32px',
    //       barColor: '#1abc9c',
    //       tooltipClassname: 'abc'
    //     });

    //     $('.resource-barchart2').sparkline([6, 4, 8, 7, 9, 6, 5], {
    //       type: 'bar',
    //       barWidth: '6px',
    //       height: '32px',
    //       barColor: '#1abc9c',
    //       tooltipClassname: 'abc'
    //     });
    //   }, 2000);

    //   setTimeout(() => {
    //     // this.doughChartPartnerGeo = {
    //     //   chartType: 'PieChart',
    //     //   dataTable: this.dataActivePartner,
    //     //   options: {
    //     //     height: 360,
    //     //     title: 'New Partners by Month',
    //     //     pieHole: 0.4,
    //     //     colors: [
    //     //       '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
    //     //     ]
    //     //   },
    //     // };
    //     this.doughChartInstructorGeo = {
    //       chartType: 'PieChart',
    //       dataTable: this.dataActiveInstructors,
    //       options: {
    //         height: 360,
    //         title: 'New Partners by Month',
    //         pieHole: 0.4,
    //         colors: [
    //           '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
    //         ]
    //       },
    //     };
    //     this.doughChartCourseMonth = {
    //       chartType: 'PieChart',
    //       dataTable: [
    //         ['Task', 'Hours per Day'],
    //         ['NULL', 1]

    //       ],
    //       options: {
    //         height: 360,
    //         title: 'New Partners by Month',
    //         pieHole: 0.4,
    //         colors: [
    //           '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
    //         ]
    //       },
    //     };
    //     this.doughChartPartnerParticipated = {
    //       chartType: 'PieChart',
    //       dataTable: this.dataPartnerSurvey,
    //       options: {
    //         height: 360,
    //         title: 'New Partners by Month',
    //         pieHole: 0.4,
    //         colors: [
    //           '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
    //         ]
    //       },
    //     };
    //     this.doughChartEvaluationMonth = {
    //       chartType: 'PieChart',
    //       dataTable: [
    //         ['Task', 'Hours per Day'],
    //         ['NULL', 1]

    //       ],
    //       options: {
    //         height: 360,
    //         title: 'New Partners by Month',
    //         pieHole: 0.4,
    //         colors: [
    //           '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
    //         ]
    //       },
    //     };

    //     this.donutChartData = {
    //       chartType: 'PieChart',
    //       dataTable: [
    //         ['Task', 'Hours per Day'],
    //         ['NULL', 1]

    //       ],
    //       options: {
    //         height: 320,
    //         title: 'My Daily Activities',
    //         pieHole: 0.4,
    //         colors: ['#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f']
    //       },
    //     };
    //   }, 1000)
    // }
    //     

}
