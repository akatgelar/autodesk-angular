import { Component, OnInit, ViewEncapsulation,HostListener } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { Http, Headers, Response } from "@angular/http";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-dashboard-eva',
    templateUrl: './dashboard-eva.component.html',
    styleUrls: [
        './dashboard-eva.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardEVAComponent implements OnInit {

    doughChartCoreProductsAgainstAllCourses: any;
    doughChartTotalEvaluationSurveyByMonth: any;
    doughChartPerformanceSite: any;
    tableChartDataTopInstructor: any;
    closeResult: string;
    private evalModel:any;
    private courseModel:any;
    private studentModel:any; 
    private popUpModel:any;    

    private _serviceUrlPerformanceSite = 'api/DashboardPerformanceSite/null';
    private _serviceUrlCoursesDeliveredBySite = 'api/DashboardCoursesDeliveredBySite';
    private _serviceUrlMonthEvaluationSurveyByGeo = 'api/DashboardMonthEvaluationSurveyByGeo';
    private _serviceUrlEvaluationSurveyByCoreProducts = 'api/DashboardEvaluationByCoreProducts';
    private _serviceUrlTop10PerformanceInstructor = 'api/DashboardTop10PerformanceInstructor';
    private _serviceUrlCardInfo='api/DashboardActiveInstructors/Top5Info'

    public loading = false;
    public loading_1 = false;
    public loading_2 = false;
    public loading_3 = false;
    public loading_4 = false;

    dataCoursesDelivered = [];
    dataCoreProductsAgainstAllCourses = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    dataTotalEvaluationSurveyByMonthByGeo = [];
    DataPerformanceSite = [];
    DataTop10PerformanceIntructor = [];

    useraccesdata: any;

    show1 :boolean = true;
    show2 :boolean = true;
    show3 :boolean = true;
    show4 :boolean = true;
    show5 :boolean = true;

  constructor(private completerService: CompleterService, private _http: Http, private service: AppService, private session: SessionService, private modalService: NgbModal, private router: Router) {
        //get user level
        let useracces = this.session.getData();
		//console.log(useracces);
        this.useraccesdata = JSON.parse(useracces);
		//console.log(this.useraccesdata);
    }

    adminkan: boolean = false;
    tampilin: boolean = true;
    protected dataService: RemoteData;
    OrgIdNew: string = "";
    ngOnInit() {
        //add chose org to see trainer dashboard
        if (!this.checkrole()) {
            this.adminkan = true;
            this.tampilin = false;

            this.dataService = this.completerService.remote(
                null,
                "Organization",
                "Organization");
            this.dataService.urlFormater(term => {
                return `api/MainOrganization/FindOrganizationSiteAdmin/` + term; //nembak api nya disamakan karena sama2 saja untuk ngedapetin orgid
            });
            this.dataService.dataField("results");
        }

        if (this.tampilin) {
            this.OrgIdNew = this.urlGetOrgId();

            setTimeout(() => {
                this.getMonthPerformanceSite();
            }, 1000);

            setTimeout(() => {
                this.getTop10PerformanceInstructor();
            }, 2000);

            setTimeout(() => {
                this.getCoreProductsAgainstAllCourses();
            }, 3000);

            setTimeout(() => {
                this.getMonthEvaluationSurvey();
            }, 4000);

            setTimeout(() => {
                this.getCoursesDelivered();
            }, 5000);
      }
      if (this.useraccesdata.Access.UserLevelId === 'TRAINER') {
        this.getTop5(this.useraccesdata.UserId);
      }

    }

    PickedOrganization(data) {
        this.tampilin = true;
        this.OrgIdNew = '"' + data["originalObject"].OrgId + '"';

        setTimeout(() => {
            this.getMonthPerformanceSite();
        }, 1000);

        setTimeout(() => {
            this.getTop10PerformanceInstructor();
        }, 2000);

        setTimeout(() => {
            this.getCoreProductsAgainstAllCourses();
        }, 3000);

        setTimeout(() => {
            this.getMonthEvaluationSurvey();
        }, 4000);

        setTimeout(() => {
            this.getCoursesDelivered();
        }, 5000);
    }

    // baru site performance buat api nya
    getMonthPerformanceSite() {
        var data = '';
        this.loading = true;
        var orgid = "";

        $('#id-month-site-performance').empty(); // biar gak numpuk pas nyari lagi

        orgid = this.OrgIdNew;

        var dataFilter = //change to post
        {
            "OrgId": orgid,
            "siteid": ''
        }
        
        /* new post for autodesk plan 17 oct */
        this.DataPerformanceSite = [];
        this.service.httpClientPost( //change to post
            "api/DashboardSiteAdmin/SitePerformance",
            dataFilter
        )
            .subscribe(result => {
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show1 = false;
                }
                else {
                    var dataResult = result; //change to post
                    var PerformanceSite = dataResult;
                    this.DataPerformanceSite.push(['Task', 'Hours per Day']);
                    for (var i in PerformanceSite) {
                        this.DataPerformanceSite.push([PerformanceSite[i].geo_name, Number(PerformanceSite[i].TotalPerformanceSite)]);
                    }
                    
                    /* issue dashboard blank */

                    // if (PerformanceSite[0].TotalPerformanceSite == "0" && PerformanceSite[1].TotalPerformanceSite == "0" && PerformanceSite[2].TotalPerformanceSite == "0" && PerformanceSite[3].TotalPerformanceSite == "0") {
                    //     this.DataPerformanceSite.push(['No record found', 1])
                    // }

                    /* end line issue dashboard blank */

                    this.doughChartPerformanceSite = {
                        chartType: 'PieChart',
                        dataTable: this.DataPerformanceSite,
                        options: {
                            height: 360,
                            title: 'Sites Performance By Month',
                            pieHole: 0.4,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ],
                            sliceVisibilityThreshold :0,
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                        },
                    };
                }
                this.loading = false;
                if(this.DataPerformanceSite.length <=1){
                    this.show1 = false;
                }
                else{
                    this.show1 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                    this.show1 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    getTop10PerformanceInstructor() {
        var orgid = "";
        this.loading_1 = true;
        
        orgid = this.OrgIdNew;

        var dataFilter = //change to post
        {
            "OrgId": orgid
        }

        this.DataTop10PerformanceIntructor = [];

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost( //change to post
            "api/DashboardSiteAdmin/top10instructor",
            dataFilter
        )
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show2 = false;
                }
                else {
                    var dataResult = result; //change to post
                    let Top10PerformanceIntructor : any= dataResult;
                    this.DataTop10PerformanceIntructor.push(['Contact Id', 'Contact Name', 'Course Taken']);
                    for (var i in Top10PerformanceIntructor) {
                        this.DataTop10PerformanceIntructor.push([Top10PerformanceIntructor[i].ContactId, Top10PerformanceIntructor[i].ContactName, parseInt(Top10PerformanceIntructor[i].TotalCourseDeliveredByIntructor)]);
                        tmpCount += parseInt(Top10PerformanceIntructor[i].TotalCourseDeliveredByIntructor);
                    }
                    /* issue dashboard blank */

                    // if(Top10PerformanceIntructor.length < 1){
                    //     this.DataTop10PerformanceIntructor.push(['','No record found',''])
                    // }

                    /* end line issue dashboard blank */

                    this.tableChartDataTopInstructor = {
                        chartType: 'Table',
                        dataTable: this.DataTop10PerformanceIntructor,
                        options: {
                            height: 340,
                            title: 'Countries',
                            allowHtml: true
                        }
                    };
                }
                this.loading_1 = false;
                if(tmpCount ==0){
                    this.show2 = false;
                }
                else{
                    this.show2 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_1 = false;
                    this.show2 = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    // getCoursesDelivered(){
    //     var data = '';
    //     this.service.get(this._serviceUrlCoursesDeliveredBySite,data)
    //     .subscribe(result => {
    //         if(result=="Not found"){
    //             this.service.notfound();
    //         }
    //         else{
    //             var CoursesDelivered = JSON.parse(result);
    //             this.dataCoursesDelivered.push( ['Task', 'Hours per Day']);
    //             for(var i in CoursesDelivered) {
    //                 this.dataCoursesDelivered.push([CoursesDelivered[i].SiteName, Number(CoursesDelivered[i].TotalCourseDeliveredByPartners)]);
    //             }
    //             const chart = c3.generate({
    //                 bindto: '#chart4',
    //                 scaleShowLabels: false,
    //                 data: {
    //                 columns: this.dataCoursesDelivered,
    //                 xkey: ['Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017'],
    //                 type: 'bar',
    //                 colors: {
    //                     data1: '#03A9F3',
    //                     data2: '#FEC107', 
    //                 }, 
    //                 groups: this.dataKeyValueCoursesDeliveredByPartners
    //                 }
    //             });
    //         }
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }

    // getMonthEvaluationSurvey(){
    //     var data = '';
    //     this.service.get(this._serviceUrlMonthEvaluationSurveyByGeo,data)
    //     .subscribe(result => {
    //         if(result=="Not found"){
    //             this.service.notfound();
    //         }
    //         else{
    //             var TotalEvaluationSurveyByMonth = JSON.parse(result);
    //             this.dataTotalEvaluationSurveyByMonthByGeo.push( ['Task', 'Hours per Day']);
    //             for(var i in TotalEvaluationSurveyByMonth) {
    //                 this.dataTotalEvaluationSurveyByMonthByGeo.push([TotalEvaluationSurveyByMonth[i].geo_name, Number(TotalEvaluationSurveyByMonth[i].TotalMOnthlySurveyTaken)]);
    //             }
    //             this.doughChartTotalEvaluationSurveyByMonth =  {
    //                 chartType: 'PieChart',
    //                 dataTable: this.dataTotalEvaluationSurveyByMonthByGeo,
    //                 options: {
    //                 height: 360,
    //                 title: 'Evaluation Survey Taken By Month',
    //                 pieHole: 0.4,
    //                 colors: [
    //                     '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
    //                     ]
    //                 },
    //             };
    //         }
    //     },
    //     error => {
    //         this.service.errorserver();
    //     });
    // }

    dataCoursesDeliveredNew = [];
    getCoursesDelivered() {
        var orgid = "";
        this.loading_2 = true;

        orgid = this.OrgIdNew;

        $('#morris-bar-chart-partner').empty(); // biar gak numpuk pas nyari lagi

        this.dataCoursesDeliveredNew = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        this.service.httpClientGet("api/DashboardCoursesByMonth/totalCourseByPartner/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show3 = false;
                }
                else {
                    var CoursesDelivered = result;

                    for (var i in CoursesDelivered) {
                        if (CoursesDelivered[i].PartnerType == 'ATC') {
                            this.dataCoursesDeliveredNew[0].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType)
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Course') {
                            this.dataCoursesDeliveredNew[1].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType)
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Project') {
                            this.dataCoursesDeliveredNew[2].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType)
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                        else if (CoursesDelivered[i].PartnerType == 'AAP Event') {
                            this.dataCoursesDeliveredNew[3].Total = Number(CoursesDelivered[i].TotalCourseByPartnerType)
                            tmpCount += Number(CoursesDelivered[i].TotalCourseByPartnerType);
                        }
                    }

                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-partner',
                        data: this.dataCoursesDeliveredNew,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_2 = false;
                if(tmpCount == 0){
                    this.show3 = false;
                }
                else{
                    this.show3 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_2 = false;
                    this.show3 = false;
                });
    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
		// console.log(userlvlarr);
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    data_MonthEvaluationSurverNew: any = "";
    getMonthEvaluationSurvey() {
        var data: any;
        var orgid = "";
        this.loading_3 = true;

        orgid = this.OrgIdNew;

        $('#morris-bar-chart-month').empty(); // biar gak numpuk pas nyari lagi

        this.data_MonthEvaluationSurverNew = [
            {
                PartnerType: 'ATC',
                Total: 0
            },
            {
                PartnerType: 'AAP - Course',
                Total: 0
            },
            {
                PartnerType: 'AAP - Project',
                Total: 0
            },
            {
                PartnerType: 'AAP - Event',
                Total: 0
            },
        ]

        this.service.httpClientGet("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                let tmpCount : any= 0;
                if (result == "Not found" || result == null || result == ' ') {
                    this.service.notfound();
                    this.show4 = false;
                }
                else {
                    data = result;

                    for (var i in data) {
                        if (data[i].PartnerType == 'ATC') {
                            this.data_MonthEvaluationSurverNew[0].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount += Number(data[i].TotalSurveyTaken);
                        }
                        else if (data[i].PartnerType == 'AAP Course') {
                            this.data_MonthEvaluationSurverNew[1].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount += Number(data[i].TotalSurveyTaken);
                        }
                        else if (data[i].PartnerType == 'AAP Project') {
                            this.data_MonthEvaluationSurverNew[2].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount += Number(data[i].TotalSurveyTaken);
                        }
                        else if (data[i].PartnerType == 'AAP Event') {
                            this.data_MonthEvaluationSurverNew[3].Total = Number(data[i].TotalSurveyTaken);
                            tmpCount += Number(data[i].TotalSurveyTaken);
                        }
                    }
                    
                    var barColor = ["#AD1D28", "#DEBB27", "#fec04c", "#1AB244"];
                    Morris.Bar({
                        element: 'morris-bar-chart-month',
                        data: this.data_MonthEvaluationSurverNew,
                        xkey: 'PartnerType',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: function (row) {
                            return barColor[row.x];
                        },
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true,
                        xLabelAngle: 25
                    });
                }
                this.loading_3 = false;
                if(tmpCount ==0){
                    this.show4 = false;
                }
                else{
                    this.show4 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_3 = false;
                    this.show4 = false;
                });
    }

    getCoreProductsAgainstAllCourses() {
        var data = '';
        this.loading_4 = true;
        var orgid = this.OrgIdNew;

        var dataFilter =
        {
            "OrgId": orgid
        }
        
        /* new post for autodesk plan 17 oct */
        this.dataCoreProductsAgainstAllCourses = [];
        this.service.httpClientPost( //change to post
            "api/DashboardDistributor/EvaTakenByCoreProduct",
            dataFilter
        )
            .subscribe(result => {
                if (result == "Not found" || result == null || result == ' ') { //change to post
                    this.service.notfound();
                    this.show5 = false;
                }
                else {
                    var dataResult = result; //change to post
                    var CoreProductsAgainstAllCourses = dataResult;
                    this.dataCoreProductsAgainstAllCourses.push(['Task', 'Hours per Day']);
                    // for (var i in CoreProductsAgainstAllCourses) {
                    //     this.dataCoreProductsAgainstAllCourses.push([CoreProductsAgainstAllCourses[i].productName, Number(CoreProductsAgainstAllCourses[i].TotalSurveyTaken)]);
                    // }
                    for (var i in CoreProductsAgainstAllCourses) {
                        if(CoreProductsAgainstAllCourses[i].productName == 'All non core Product' && CoreProductsAgainstAllCourses[i].TotalSurveyTaken !='0' ){
                        this.dataCoreProductsAgainstAllCourses.push([CoreProductsAgainstAllCourses[i].productName, Number(CoreProductsAgainstAllCourses[i].TotalSurveyTaken)]);}
                        else if(CoreProductsAgainstAllCourses[i].productName != 'All non core Product'){
                            this.dataCoreProductsAgainstAllCourses.push([CoreProductsAgainstAllCourses[i].productName, Number(CoreProductsAgainstAllCourses[i].TotalSurveyTaken)]);
                        }
                    }
                    /* issue dashboard blank */

                    // if (CoreProductsAgainstAllCourses[0].TotalSurveyTaken == "0") {
                    //     this.dataCoreProductsAgainstAllCourses.push(['No record found', 1])
                    // }

                    /* end line issue dashboard blank */

                    this.doughChartCoreProductsAgainstAllCourses = {
                        chartType: 'PieChart',
                        dataTable: this.dataCoreProductsAgainstAllCourses,
                        options: {
                            height: 360,
                            title: 'Evaluation Survey Taken By Core Products',
                            pieHole: 0.4,
                            sliceVisibilityThreshold :0,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ],
                            lang: {
                                noData: "No Data Available",
                            },
                            noData: {
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '15px',
                                }
                            }
                        },
                    };
                }
                this.loading_4 = false;
                if(this.dataCoreProductsAgainstAllCourses.length <=1){
                    this.show5 = false;
                }
                else{
                    this.show5 = true;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading_4 = false;
                    this.show5 = false;
                });
        /* new post for autodesk plan 17 oct */
    }
    @HostListener('click') onMouseClick() {
    }
    open(content,Id) {
      if(Id==='evalEnd'){
        this.popUpModel=null;
	      //this.evalModel=[{'Id':'1','Name':'One','Date':'12'}];
        this.popUpModel = {
          'Title': 'TOP 5 Course Evaluation End Date This Week', 'List': this.evalModel
        };
       
      }
      if(Id==='courseEnd'){
        this.popUpModel=null;
	      //this.courseModel=[{'Id':'C1','Name':'COne','Date':'12'}];
	      this.popUpModel={'Title':'TOP 5 Course End Date This Week','List':this.courseModel};
      }
      if(Id==='enrol'){
        this.popUpModel=null;
	      //this.studentModel=[{'Id':'S1','Name':'SOne','Date':'12'}];
	      this.popUpModel={'Title':'TOP 5 Students Enrol This Week','List':this.studentModel};
      }
        this.modalService.open(content); 

    }

  getTop5(UserId) {
    var data = '';
    this.service.httpClientGet(this._serviceUrlCardInfo + '/' + UserId, data)
      .subscribe(result => {
        if (result == "Not found" || result == null || result == ' ') {
          this.service.notfound();
        }
        else {
          var respondData : any = result;
          this.courseModel = respondData.CourseEndThisWeekViewModels;
          this.evalModel = respondData.EvalEndThisWeekViewModels;
          this.studentModel = respondData.StudentEnrolThisWeekViewModels;
        //   console.log(respondData);
        }
      },
      error => {
        this.service.errorserver();
      });

  }
  confirmEdit(courseId) {
    // console.log(courseId);
   
    this.router.navigate(['/course/course-update', courseId,null,null,null]);
  }
}
