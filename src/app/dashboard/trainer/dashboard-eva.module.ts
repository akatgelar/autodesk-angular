import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardEVAComponent } from './dashboard-eva.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";

export const DashboardEVARoutes: Routes = [
  {
    path: '',
    component: DashboardEVAComponent,
    data: {
      breadcrumb: 'menu_dashboard.dashboard_trainer',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardEVARoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [DashboardEVAComponent]
})
export class DashboardEVAModule { }
