import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteServicesListEPDBComponent } from './site-services-list-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

import { DataFilterSiteServicePipe } from './site-services-list-epdb.component';

export const SiteServicesListEPDBRoutes: Routes = [
  {
    path: '',
    component: SiteServicesListEPDBComponent,
    data: {
      breadcrumb: 'List Site Services',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SiteServicesListEPDBRoutes),
    SharedModule
  ],
  declarations: [SiteServicesListEPDBComponent, DataFilterSiteServicePipe],
  providers:[AppService]
})
export class SiteServicesListEPDBModule { }
