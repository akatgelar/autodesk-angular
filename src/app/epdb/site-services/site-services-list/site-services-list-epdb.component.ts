import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../../shared/service/app.service";

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({ name: 'dataFilterSiteService' })
export class DataFilterSiteServicePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
                (row.site_service_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.site_service_name.toLowerCase().indexOf(query.toLowerCase()) > -1));
        } 
        return array;
    }
}

@Component({
  selector: 'app-site-list-services-epdb',
  templateUrl: './site-services-list-epdb.component.html',
  styleUrls: [
    './site-services-list-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SiteServicesListEPDBComponent implements OnInit {

  private _serviceUrl = 'api/SiteService';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
   
  constructor(private service:AppService) { }

  ngOnInit() {
    
    //get data action
    var data = '';
    this.service.httpClientGet(this._serviceUrl,data)
    .subscribe(result => {
        this.data = result; 
    },
    error => {
        this.service.errorserver();
    });
    
  }

  //view detail
  viewdetail(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+id, data)
    .subscribe(result => {
        this.datadetail = result;  
    },
    error => {
        this.service.errorserver();
    });
  }

  //delete confirm
  openConfirmsSwal(id) {
    var index = this.data.findIndex(x => x.site_service_id == id);
    //this.service.httpClientDelete(this._serviceUrl,this.data, id, index);
    this.service.httpClientDelete(this._serviceUrl+'/'+id,this.data);
  }
}