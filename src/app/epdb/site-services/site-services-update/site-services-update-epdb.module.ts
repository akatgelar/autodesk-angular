import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteServicesUpdateEPDBComponent } from './site-services-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const SiteServicesUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: SiteServicesUpdateEPDBComponent,
    data: {
      breadcrumb: 'Update Site Services',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SiteServicesUpdateEPDBRoutes),
    SharedModule
  ],
  declarations: [SiteServicesUpdateEPDBComponent],
  providers:[AppService]
})
export class SiteServicesUpdateEPDBModule { }
