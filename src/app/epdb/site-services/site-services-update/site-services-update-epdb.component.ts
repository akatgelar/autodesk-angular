import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-site-update-services-epdb',
  templateUrl: './site-services-update-epdb.component.html',
  styleUrls: [
    './site-services-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SiteServicesUpdateEPDBComponent implements OnInit {

  private _serviceUrl = 'api/SiteService';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  updatesiteservice: FormGroup;
  id:string;

  constructor(private service:AppService, private route:ActivatedRoute, private router: Router) { 

    let site_service_id = new FormControl();
    let site_service_code = new FormControl();
    let site_service_name = new FormControl();
    let description = new FormControl();

    this.updatesiteservice = new FormGroup({
      site_service_id:site_service_id,
      site_service_code:site_service_code,
      site_service_name:site_service_name,
      description:description
    });

  }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        if(result=="Not found"){
            this.service.notfound();
        }
        else{
            this.data = result; 
            this.buildForm();
        } 
      },
      error => {
          this.service.errorserver();
      });

  }

  //build form update
  buildForm(): void {
    
    let site_service_id = new FormControl(this.data.site_service_id);
    let site_service_code = new FormControl(this.data.site_service_code, Validators.required);
    let site_service_name = new FormControl(this.data.site_service_name, Validators.required);
    let description = new FormControl(this.data.description, Validators.required);

    this.updatesiteservice = new FormGroup({
      site_service_id:site_service_id,
      site_service_code:site_service_code,
      site_service_name:site_service_name,
      description:description
    });

  }

  //submit form
  onSubmit(){

    this.updatesiteservice.controls['site_service_code'].markAsTouched();
    this.updatesiteservice.controls['site_service_name'].markAsTouched();
    this.updatesiteservice.controls['description'].markAsTouched();

    if(this.updatesiteservice.valid){
      
      //data and get id
      let data = JSON.stringify(this.updatesiteservice.value);
  
      //update action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id,data)
        .subscribe(res=>{
          console.log(res)
        });
  
      //redirect
      this.router.navigate(['/admin/site-services/site-services-list']);
    }
  }

  resetForm(){
    this.updatesiteservice.reset({
      'site_service_code':this.data.site_service_code,
      'site_service_name':this.data.site_service_name,
      'description':this.data.description
    });
  }
  
}