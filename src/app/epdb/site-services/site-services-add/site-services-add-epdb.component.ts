import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-site-add-services-epdb',
  templateUrl: './site-services-add-epdb.component.html',
  styleUrls: [
    './site-services-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SiteServicesAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/SiteService';
  messageResult: string = '';
  messageError: string = '';
  addsiteservice: FormGroup;

  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate) { 
    
    //validation
    let site_service_code = new FormControl('', Validators.required);
    let site_service_name = new FormControl('', Validators.required);
    let description = new FormControl('', Validators.required);

    this.addsiteservice = new FormGroup({
      site_service_code:site_service_code,
      site_service_name:site_service_name,
      description:description
    });

  }

  ngOnInit() {}

  //submit form
  submitted: boolean;
  onSubmit() {
    this.addsiteservice.controls['site_service_code'].markAsTouched();
    this.addsiteservice.controls['site_service_name'].markAsTouched();
    this.addsiteservice.controls['description'].markAsTouched();

    if(this.addsiteservice.valid){
      
      //convert object to json
      let data = JSON.stringify(this.addsiteservice.value); 
  
      //post action
      this.service.httpClientPost(this._serviceUrl,data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
  
      //redirect
      this.router.navigate(['/admin/site-services/site-services-list']);
    }    
  }

  resetForm(){
    this.addsiteservice.reset({
      
    });
  }
}