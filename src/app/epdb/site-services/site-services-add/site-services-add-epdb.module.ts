import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteServicesAddEPDBComponent } from './site-services-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const SiteServicesAddEPDBRoutes: Routes = [
  {
    path: '',
    component: SiteServicesAddEPDBComponent,
    data: {
      breadcrumb: 'Add Site Services',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SiteServicesAddEPDBRoutes),
    SharedModule
  ],
  declarations: [SiteServicesAddEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class SiteServicesAddEPDBModule { }
