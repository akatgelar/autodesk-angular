import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteServicesDetailEPDBComponent } from './site-services-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const SiteServicesDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: SiteServicesDetailEPDBComponent,
    data: {
      breadcrumb: 'Detail Site Services',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SiteServicesDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [SiteServicesDetailEPDBComponent],
  providers:[AppService]
})
export class SiteServicesDetailEPDBModule { }
