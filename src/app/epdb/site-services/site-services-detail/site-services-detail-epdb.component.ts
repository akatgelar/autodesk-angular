import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-site-detail-services-epdb',
  templateUrl: './site-services-detail-epdb.component.html',
  styleUrls: [
    './site-services-detail-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SiteServicesDetailEPDBComponent implements OnInit {

  private _serviceUrl = 'api/SiteService';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  updatesiteservice: FormGroup;
  id:string;
  public datadetail:any

  constructor(private service:AppService, private route:ActivatedRoute, private router: Router) { 

    let site_service_id = new FormControl();
    let site_service_code = new FormControl();
    let site_service_name = new FormControl();
    let description = new FormControl();

    this.updatesiteservice = new FormGroup({
      site_service_id:site_service_id,
      site_service_code:site_service_code,
      site_service_name:site_service_name,
      description:description
    });

  }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    var data = '';
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result;  
    },
    error => {
        this.service.errorserver();
    });

  }



  
}