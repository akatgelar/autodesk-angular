import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ATCAAPAccreditationCountsReportEPDBComponent } from './ATC-AAP-accreditation-counts-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ATCAAPAccreditationCountsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: ATCAAPAccreditationCountsReportEPDBComponent,
    data: {
      breadcrumb: 'ATC/AAP Accreditation Counts Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ATCAAPAccreditationCountsReportEPDBRoutes),
    SharedModule
  ],
  declarations: [ATCAAPAccreditationCountsReportEPDBComponent]
})
export class ATCAAPAccreditationCountsReportEPDBModule { }
