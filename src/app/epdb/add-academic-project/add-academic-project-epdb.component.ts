import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-add-academic-project-epdb',
    templateUrl: './add-academic-project-epdb.component.html',
    styleUrls: [
        './add-academic-project-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddAcademicProjectEPDBComponent implements OnInit {

    private _serviceUrl = '';
    academicprojectform: FormGroup;
    messageResult: string = '';
    messageError: string = '';
    public activitytype: any;
    public datadetail: any;
    modelPopup1: NgbDateStruct;
    public listProject;
    listProduct = [];
    public listCountry;
    private completionDate;
    private SiteId;
    private SiteIdInt;
    private useraccessdata;
    private countryCode;
    private versions;
    public loading = false;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute,
        private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        let projecttype = new FormControl('', Validators.required);
        let completiondate = new FormControl('', Validators.required);
        let numbereducator = new FormControl('', Validators.required);
        let numberstudent = new FormControl('', Validators.required);
        let product = new FormControl('', Validators.required);
        let institution = new FormControl('', Validators.required);
        let countryinstitution = new FormControl('', Validators.required);
        let noteproject = new FormControl('');
        // let productVersion = new FormControl('');

        this.academicprojectform = new FormGroup({
            projecttype: projecttype,
            completiondate: completiondate,
            numbereducator: numbereducator,
            numberstudent: numberstudent,
            product: product,
            institution: institution,
            countryinstitution: countryinstitution,
            noteproject: noteproject,
            // productVersion: productVersion
        });
    }

    getProjectType() {
        var data: any;
        var parent = "ProjectType";
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "'}", data)
            .subscribe(res => {
                data = res;
                data.length != 0 ? this.listProject = data : this.listProject = [];
            }, error => {
                this.service.errorserver();
            });
    }

    getProduct() {
        var data: any;
        this.listProduct = [];
        this.service.httpClientGet("api/Product", data)
            .subscribe(res => {
                data = res;
                if (data.length != 0) {
                    for (let i = 0; i < data.length; i++) {
                        var product = {
                            'productId': data[i].productId,
                            'productName': data[i].productName
                        };
                        this.listProduct.push(product);
                    }
                }
            }, error => {
                this.service.errorserver();
            });
    }

    findVersion(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    this.versions = result;
                }, error => { this.service.errorserver(); });
        } else {
            this.versions = "";
        }
    }

    getCountry() {
        var data: any;
        this.service.httpClientGet("api/Countries", data)
            .subscribe(res => {
                data = res;
                data.length != 0 ? this.listCountry = data : this.listCountry = [];
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.completionDate = this.parserFormatter.format(date);
        } else {
            this.completionDate = "";
        }
    }

    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || '';
            this.SiteIdInt = params['SiteIdInt'] || 0;
        });

        this.getProjectType();
        this.getProduct();
        this.getCountry();
    }

    onSubmitAcademicProject() {

        this.academicprojectform.controls["projecttype"].markAsTouched();
        this.academicprojectform.controls["completiondate"].markAsTouched();
        this.academicprojectform.controls["numbereducator"].markAsTouched();
        this.academicprojectform.controls["numberstudent"].markAsTouched();
        this.academicprojectform.controls["product"].markAsTouched();
        this.academicprojectform.controls["institution"].markAsTouched();
        this.academicprojectform.controls["countryinstitution"].markAsTouched();

        if (this.academicprojectform.valid) {
            this.loading = true;
            var dataProject = {
                'SiteId': this.SiteId,
                'Usage': this.academicprojectform.value.projecttype,
                'CompletionDate': this.completionDate,
                'TargetEducator': this.academicprojectform.value.numbereducator,
                'TargetStudent': this.academicprojectform.value.numberstudent,
                'productId': this.academicprojectform.value.product,
                // 'productVersion':this.academicprojectform.value.productVersion,
                'Institution': this.academicprojectform.value.institution,
                'CountryCode': this.academicprojectform.value.countryinstitution,
                'Comment': this.academicprojectform.value.noteproject,
                'DateAdded': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                'AddedBy': this.useraccessdata.ContactName
            };
            this.service.httpClientPost("api/AcademicTargetProgram/Project", dataProject)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
            this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
            this.loading = false;
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
    }

    resetForm() {
        this.academicprojectform.reset();
    }
}