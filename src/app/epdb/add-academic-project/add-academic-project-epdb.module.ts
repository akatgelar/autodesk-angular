import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAcademicProjectEPDBComponent } from './add-academic-project-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddAcademicProjectEPDBRoutes: Routes = [
  {
    path: '',
    component: AddAcademicProjectEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.site.academic_project.add_academic_project',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddAcademicProjectEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [AddAcademicProjectEPDBComponent],
  providers: [AppService, AppFormatDate],
})
export class AddAcademicProjectEPDBModule { }