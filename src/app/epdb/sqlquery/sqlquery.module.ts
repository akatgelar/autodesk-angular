import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { SqlqueryComponent } from './sqlquery.component';
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const SKUEPDBRoutes: Routes = [
  {
    path: '',
    component: SqlqueryComponent,
    data: {
      breadcrumb: 'SQL Query',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LoadingModule,
    RouterModule.forChild(SKUEPDBRoutes)
  ],
  declarations: [SqlqueryComponent],
  providers:[AppService]
})

export class SqlqueryModule { }
