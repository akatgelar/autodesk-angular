import { Component, OnInit } from '@angular/core';
import { AppService } from "../../shared/service/app.service";
import swal from 'sweetalert2';

@Component({
  selector: 'app-sqlquery',
  templateUrl: './sqlquery.component.html',
  styleUrls: ['./sqlquery.component.css']
})
export class SqlqueryComponent implements OnInit {

  loading = false
  sqlQuery = ""
  otp = ""
  tables = []
  selectedTable = ""
  headers = []
  data = []
  
  constructor(private service:AppService) {
  }

  ngOnInit() {
    this.loading = true
    this.service.httpClientGet('/api/SqlAdmin/GetTables',{}).subscribe((res:any)=>{
      this.loading = false
      this.tables = res.data
    })
  }

  selectTable(name){
    if(this.selectedTable==name)
      this.selectedTable = ''
    else
      this.selectedTable = name
  }

  generateToken(){
    this.loading = true
    this.service.httpClientPost('api/SqlAdmin/GenerateOTPToken',{}).subscribe((res)=>{
      this.loading = false
      swal('One Time Password Genereated','Check your email for one time password','success')
    })
  }

  query(){
    this.loading = true
    let email = JSON.parse(localStorage.getItem('autodesk-data')).Email
    this.service.httpClientPost('api/SqlAdmin/ExecuteQuery',{
      Query: this.sqlQuery,
	    EmailAddress: email,
	    SercurityCode: this.otp
    }).subscribe((res:any)=>{
      this.loading = false
      let data = res.data.result
      if(data[0]){
        this.headers = Object.keys(data[0])
        this.data = data
      }else{
        this.headers = []
        this.data = []
      }
    })
  }
}
