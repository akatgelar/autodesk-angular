import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

const now = new Date();

@Component({
  selector: 'app-add-contact-qualifications-epdb',
  templateUrl: './add-contact-qualifications-epdb.component.html',
  styleUrls: [
    './add-contact-qualifications-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class AddContactQualificationsEPDBComponent implements OnInit {
 
  private _serviceUrl = 'api/Qualifications';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  public datasite:any;
  contactqualificationform: FormGroup;
  public orgentriestype:any;
  public useraccesdata:any;
  public datayearcurrency:any;
  public programtype:any;
  public productcategory:any;
  public product:any;
  public typequalifications:any;
  public loading = false;

  modelPopup: NgbDateStruct;
  
  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private formatdate: AppFormatDate, private session: SessionService) { //, private htmlEntityService: htmlentityService
    
    //get user level
    this.modelPopup = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
    
    let AutodeskProduct = new FormControl('', Validators.required);
    let QualificationType = new FormControl('', Validators.required);
    let QualificationDate = new FormControl('', Validators.required);
    let Version = new FormControl('');
    let Comments = new FormControl('');
    this.contactqualificationform = new FormGroup({
      AutodeskProduct:AutodeskProduct,
      QualificationType:QualificationType,
      QualificationDate:QualificationDate,
      Comments:Comments,
      Version:Version
    });

  }


  id:string="";
  ContactId:string="";
  SiteId:string="";
  ContactIdInt:string='';
  ngOnInit() {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
        this.ContactId = params['ContactId'] || '';
        this.SiteId = params['SiteId'] || ''; 
        this.ContactIdInt = params['ContactIdInt'] || ''; 
    });

    var data = '';
    this.service.httpClientGet("api/Product/qualification/ProductFamilies", data)
      .subscribe(res => {
       // data = res.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
     let  tmpData = res;
        if (res == "Not found") {
          this.service.notfound();
          this.productcategory = '';
        } else {
          this.productcategory = tmpData;
          this.getProductList(this.productcategory);
        }
      }, error => { 
        this.service.errorserver(); 
        this.productcategory = '';
      }
    );

    this.service.httpClientGet('api/Currency/where/{"Parent":"QualificationTypes","Status":"A"}', data)
    .subscribe((result:any) => {
      if (result == "Not found") {
        this.typequalifications = '';
      }else{
        this.typequalifications = result.sort((a,b)=>{
          if(a.KeyValue > b.KeyValue)
            return 1
          else if(a.KeyValue < b.KeyValue)
            return -1
          else return 0
        });
      }
    },
    error => {
        this.service.errorserver();
        this.typequalifications = '';
    });
    
  }

  dropdownListProduct=[];
  dropdownListProductCategory=[];  
  getProductList(data){
    for(var i =0; i<data.length;i++){
      this.dropdownListProductCategory.push(data[i].familyId); 
    }
    for(var i =0; i<this.dropdownListProductCategory.length;i++){
      this.service.httpClientGet("api/Product/qualification/where/{'familyId':'" + this.dropdownListProductCategory[i] + "','Status':'A','VersionStatus':'A'}", '')
      .subscribe(result => {
        // data = result.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        this.product = result;
        this.dropdownListProduct.push([this.product]); 
      },
      error => {
          this.service.errorserver();
      });
    }
  }

  versions:any;
  findVersion(value) {
    if (value != null) {
        var version: any;
        this.service.httpClientGet("api/Product/Version/" + value, version)
            .subscribe(result => {
              //var data = result.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
               // this.versions = JSON.parse(data);
               this.versions = result;
            }, error => { this.service.errorserver(); });
    } else {
        this.versions = "";
    }
  }

  //submit
  onSubmit(){

    this.contactqualificationform.controls['AutodeskProduct'].markAsTouched();
    this.contactqualificationform.controls['QualificationType'].markAsTouched();
    this.contactqualificationform.controls['QualificationDate'].markAsTouched();
    // this.contactqualificationform.controls['Version'].markAsTouched();
    //this.contactqualificationform.controls['FYIndicator'].markAsTouched();
    
    if(this.contactqualificationform.valid){
      this.loading = true;
      this.contactqualificationform.value.QualificationDate = this.formatdate.dateCalendarToYMD(this.contactqualificationform.value.QualificationDate);
      this.contactqualificationform.value.AddedBy = this.useraccesdata.ContactName;
      this.contactqualificationform.value.ContactId = this.ContactId;

      this.contactqualificationform.value.cuid = this.useraccesdata.ContactName;
      this.contactqualificationform.value.UserId = this.useraccesdata.UserId;
     //this.contactqualificationform.value.Comments = this.htmlEntityService.encoder(this.contactqualificationform.value.Comments);
      //convert object to json
      let data = JSON.stringify(this.contactqualificationform.value);
  
      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      setTimeout(() => {
        //redirect
        this.router.navigate(['/manage/contact/detail-contact',this.ContactIdInt,this.SiteId]);
        this.loading = false;
      }, 1000)
    }
  }

  go_Back_Bro() {
    this.router.navigate(['manage/contact/detail-contact/', this.ContactIdInt, this.SiteId]);
  }

  //reset form
  resetForm() {
    this.contactqualificationform.reset({
      'AutodeskProduct': '',
      'QualificationType': '',
      'QualificationDate': '',
      'Comments': '',
      'Version': ''
    });
  }
}
