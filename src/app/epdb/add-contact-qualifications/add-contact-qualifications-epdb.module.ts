import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactQualificationsEPDBComponent } from './add-contact-qualifications-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddContactQualificationsEPDBRoutes: Routes = [
  {
    path: '',
    component: AddContactQualificationsEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.partner_type_spec.add_qualification',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddContactQualificationsEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddContactQualificationsEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class AddContactQualificationsEPDBModule { }
