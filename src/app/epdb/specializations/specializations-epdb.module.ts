import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecializationsEPDBComponent } from './specializations-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const SpecializationsEPDBRoutes: Routes = [
  {
    path: '',
    component: SpecializationsEPDBComponent,
    data: {
      breadcrumb: 'Learning History',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SpecializationsEPDBRoutes),
    SharedModule
  ],
  declarations: [SpecializationsEPDBComponent]
})
export class SpecializationsEPDBModule { }
