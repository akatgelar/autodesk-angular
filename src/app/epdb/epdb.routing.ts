import {Routes} from '@angular/router';

export const EPDBRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard-epdb.module#DashboardEPDBModule',
        data: {
          breadcrumb: 'Dashboard'
        }
      }, 
      {
        path: 'add-organization',
        loadChildren: './add-organization/add-organization-epdb.module#AddOrganizationEPDBModule',
        data: {
          breadcrumb: 'Add new Organization'
        }
      }, 
      {
        path: 'search-organization',
        loadChildren: './search-organization/search-organization-epdb.module#SearchOrganizationEPDBModule',
        data: {
          breadcrumb: 'Search for an Organization'
        }
      },  
      {
        path: 'search-site',
        loadChildren: './search-site/search-site-epdb.module#SearchSiteEPDBModule',
        data: {
          breadcrumb: 'Search for an Site'
        }
      },  
      {
        path: 'search-contact',
        loadChildren: './search-contact/search-contact-epdb.module#SearchContactEPDBModule',
        data: {
          breadcrumb: 'Search for an Contact'
        }
      },  
      {
        path: 'view-user-information',
        loadChildren: './view-user-information/view-user-information-epdb.module#ViewUserInformationEPDBModule',
        data: {
          breadcrumb: 'View User Information'
        }
      },   
      {
        path: 'change-user-information',
        loadChildren: './change-user-information/change-user-information-epdb.module#ChangeUserInformationEPDBModule',
        data: {
          breadcrumb: 'Change User Information'
        }
      },   
      {
        path: 'administration',
        loadChildren: './administration/administration-epdb.module#AdministrationEPDBModule',
        data: {
          breadcrumb: 'Administration'
        }
      },    
      {
        path: 'activities',
        children: [
          {
            path: 'activities-list',
            loadChildren: './activities/activities-list/activities-list-epdb.module#ActivitiesListEPDBModule',
            data: {
              breadcrumb: 'List Activities '
            }
          },   
          {
            path: 'activities-add',
            loadChildren: './activities/activities-add/activities-add-epdb.module#ActivitiesAddEPDBModule',
            data: {
              breadcrumb: 'Add New Activities '
            }
          }, 
        ]
      }, 
      
    ]
  }
];


