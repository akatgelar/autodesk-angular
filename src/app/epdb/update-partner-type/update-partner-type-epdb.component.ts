import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';  

@Component({
    selector: 'app-update-partner-type-epdb',
    templateUrl: './update-partner-type-epdb.component.html',
    styleUrls: [
      './update-partner-type-epdb.component.css',
      '../../../../node_modules/c3/c3.min.css', 
      ], 
    encapsulation: ViewEncapsulation.None
})

export class UpdatePartnerTypeEPDBComponent implements OnInit {

    private _serviceUrl = 'api/SitePartnerType';
    messageError: string = '';
    updatepartnertypeform: FormGroup; 
    public datadetail:any;
    id:string='';
    public partnertype:any;
    public partnertypestatus:any;
    public datasite:any;
    public loading = false;

    useraccesdata:any;

    constructor(public session:SessionService, private router: Router, private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute) {

         //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let PartnerType = new FormControl('',Validators.required);
        let Status = new FormControl('',Validators.required);
        let CSN = new FormControl('');
        let ExternalId = new FormControl('');

        this.updatepartnertypeform = new FormGroup({
            PartnerType:PartnerType,
            Status:Status,
            CSN:CSN,
            ExternalId:ExternalId
        });
    }

    ngOnInit() {
        
        //get data partner type
        this.id = this.route.snapshot.params['id'];
        var data = '';
        this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
            .subscribe(result => {
                if(result == "Not Found"){
                    this.service.notfound();
                    this.datadetail = '';
                }
                else{
                    this.datadetail = result;
                    this.buildForm();
                }
            },
            error => {
                this.service.errorserver();
                this.datadetail = '';
            }
        );

        //get partner type
        
        //this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}',data)   
        this.service.httpClientGet('api/Roles/NotShowAddedPartnerType/' + this.id, data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.partnertype = '';
            }
            else{
                this.partnertype = result;
               
            } 
        },
        error => {
            this.service.errorserver();
            this.partnertype = '';
        });

        //get partner type status
        this.service.httpClientGet('api/Roles/Status',data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.partnertypestatus = '';
            }
            else{
                this.partnertypestatus = result;
            } 
        },
        error => {
            this.service.errorserver();
            this.partnertypestatus = '';
        });

    }

    //build form update
    buildForm(): void {
        let PartnerType = new FormControl(this.datadetail.RoleId ,Validators.required);
        let Status = new FormControl(this.datadetail.Status ,Validators.required);
        let CSN = new FormControl(this.datadetail.CSN);
        let ExternalId = new FormControl(this.datadetail.ExternalId);

        this.updatepartnertypeform = new FormGroup({
            PartnerType:PartnerType,
            Status:Status,
            CSN:CSN,
            ExternalId:ExternalId
        });
    }

    // update form
    onSubmit(){
        
        this.loading = true;
        //convert object to json

        this.updatepartnertypeform.value.cuid = this.useraccesdata.ContactName;
        this.updatepartnertypeform.value.UserId = this.useraccesdata.UserId;

        let data = JSON.stringify(this.updatepartnertypeform.value);

        //action put
        this.service.httpCLientPut(this._serviceUrl+'/'+this.id,data)
            .subscribe(res=>{
                console.log(res)
            });

        setTimeout(() => {

            //get partner type
            this.service.httpClientGet("api/MainSite/where/{'SiteId': '"+this.datadetail.SiteId+"'}",data)
              .subscribe(result => { 
                if(result=="Not found"){
                    this.service.notfound();
                    this.datasite = '';
                }
                else{
                    this.datasite = result;
                    this.service.httpClientGet('api/MainSite/partnertype/' + this.datasite[0].SiteIdInt, data);
                } 
              },
              error => {
                  this.service.errorserver();
                  this.datasite = '';
              });
    
            //redirect
            this.router.navigate(['/manage/site/detail-site/',this.datadetail.SiteIdInt]);
            this.loading = false;

        }, 1000)

    }
}