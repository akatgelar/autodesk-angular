import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdatePartnerTypeEPDBComponent } from './update-partner-type-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const UpdatePartnerTypeEPDBRoutes: Routes = [
    {
        path: '',
        component: UpdatePartnerTypeEPDBComponent,
        data: {
            breadcrumb: 'epdb.admin.partner_type.edit_partner_type',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UpdatePartnerTypeEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [UpdatePartnerTypeEPDBComponent],
    providers: [AppService, AppFormatDate]
})

export class UpdatePartnerTypeEPDBModule { }