import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SessionService } from '../../shared/service/session.service';
import { Router } from '@angular/router';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterOrganization' })
export class DataFilterOrganizationPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.OrgId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.OrgName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        } else {
            return "No Partner Type"
        }
    }
}

@Component({
    selector: 'app-search-organization-epdb',
    templateUrl: './search-organization-epdb.component.html',
    styleUrls: [
        './search-organization-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SearchOrganizationEPDBComponent implements OnInit {

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownListSubCountry = [];
    selectedItemsSubCountry = [];
    dropdownSettings = {};
    dropdownListSubPartnerType =[];
    selectedItemsSubPartnerType = [];
    dropdownListTerritories = [];
    selectedItemsTerritories = [];
    dropdownListState = [];
    selectedItemsState = [];



    private _serviceUrl = 'api/MainOrganization';
    public data: any = '';
    public partnertype: any;
    public subpartnertype: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "";
    public sortOrder: string = "asc";
    filterorgform: FormGroup;
    public loading = false;
    useraccesdata: any;
    public datastate: any;
    constructor(private _http: Http, private router: Router, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let OrgId = new FormControl('');
        let OrgName = new FormControl('');
        let PartnerType = new FormControl('');
        let SubPartnerType = new FormControl('');
        let RegisteredStateProvince = new FormControl('');

        this.filterorgform = new FormGroup({
            OrgId: OrgId,
            OrgName: OrgName,
            PartnerType: PartnerType,
            SubPartnerType: SubPartnerType,
            RegisteredStateProvince: RegisteredStateProvince
        });

    }

    /* populate data issue role distributor */
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true;
            }
        }
    }

    itsinstructor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsDistributor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
    /* populate data issue role distributor */

    territorydefault: string = "";
    orgarr: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        var  OrgId = null; var Distributor = null;
        if (this.checkrole()) {

            if (this.itsinstructor()) {
                OrgId = this.urlGetOrgId();
               url = "api/Territory/wherenew/'OrgId'";
            } else {
                if (this.itsDistributor()) {
                    Distributor = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'Distributor'";
                } else {
                    OrgId = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'OrgId'";
                }
            }
            
        }
        var keyword : any = {
            OrgId: OrgId,
            Distributor: Distributor
           
        };
        keyword = JSON.stringify(keyword);
        /* populate data issue role distributor */

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }

                    if (this.checkrole()) {
                        var itsdistributor = false;
                        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
                        for (var i = 0; i < userlvlarr.length; i++) {
                            if (userlvlarr[i] == "DISTRIBUTOR") {
                                itsdistributor = true;
                            }
                        }
                        if (itsdistributor) {
                            var territoryarr = [];
                            for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                                territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                            }
                            this.territorydefault = territoryarr.toString();
                        } else {
                            this.orgarr = this.urlGetOrgId();
                        }
                    }

                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsTerritories = [];
    }


    allregion = [];
    allsubregion = [];
    allcountries = [];
    ngOnInit() {
        //partner type
        var data = '';
        this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.partnertype = '';
                }
                else {
                    this.partnertype = result.sort((a,b)=>{
                        if(a.RoleName > b.RoleName)
                            return 1
                        else if(a.RoleName < b.RoleName)
                            return -1
                        else return 0
                    })
                }
            },
                error => {
                    this.service.errorserver();
                    this.partnertype = '';
                });







        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.filterorgform.patchValue({ OrgId: item.OrgId });
            this.filterorgform.patchValue({ OrgName: item.OrgName });
            this.filterorgform.patchValue({ PartnerType: item.Partner });
            this.selectedItemsPartnerTypeStatus = item.PartnerStatus;
            this.dropdownListPartnerTypeStatus = item.dropdownStatus;
            this.selectedItemsGeo = item.Geo;
            this.dropdownListGeo = item.dropdownGeo;
            this.selectedItemsTerritories = item.Territory;
            this.dropdownListTerritories = item.dropdownTerritory;
            this.selectedItemsCountry = item.Country;
            this.dropdownListCountry = item.dropdownCountry;
            this.selectedItemsSubPartnerType = item.subpartnertype;
            this.dropdownListSubPartnerType = item.dropdownsubpartnertype;
            this.selectedItemsState = item.state;
            this.dropdownListState = item.dropdownstate;
            this.filterorgform.patchValue({ RegisteredStateProvince: item.StateProvince });
            if(JSON.parse(localStorage.getItem("filter")).OrgName != undefined)
                this.onSubmit();
        } else {
            this.getTerritory();

            // Partner Type Status
            var data = '';
            this.service.httpClientGet('api/Roles/Status', data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListPartnerTypeStatus = null;
                    }
                    else {
                        this.dropdownListPartnerTypeStatus = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].Key,
                              itemName: result[Index].KeyValue
                            }
                          })
                       this.selectedItemsPartnerTypeStatus = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].Key,
                              itemName: result[Index].KeyValue
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsPartnerTypeStatus = [];

            // Geo
            /* populate data issue role distributor */
            var url = "api/Geo/SelectAdmin";
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }
            var  DistributorGeo = null; var OrgIdGeo = null;
            if (this.checkrole()) {
                if (this.itsinstructor()) {
                    OrgIdGeo = this.urlGetOrgId();
                   url = "api/Territory/wherenew/'OrgIdGeo'";
                } else {
                    if (this.itsDistributor()) {
                        DistributorGeo = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'DistributorGeo'";
                    } else {
                        OrgIdGeo = this.urlGetOrgId();
                   url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
                
            }
            var keyword : any = {
                DistributorGeo: DistributorGeo,
                OrgIdGeo: OrgIdGeo,
               
            };
            keyword = JSON.stringify(keyword);
            data = '';
            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe((result:any) => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListGeo = null;
                    }
                    else {
                        this.dropdownListGeo = result.sort((a,b)=>{
                            if(a.geo_name>b.geo_name)
                                return 1
                            else if(a.geo_name<b.geo_name)
                                return -1
                            else return 0
                        }).map(function (el) {
                            return {
                              id: el.geo_code,
                              itemName: el.geo_name
                            }
                          })
                        this.selectedItemsGeo = result.map(function (el) {
                            return {
                              id: el.geo_code,
                              itemName: el.geo_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsGeo = [];

            // Countries
            /* populate data issue role distributor */
            var url2 = "api/Countries/SelectAdmin";
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }
            var  CountryOrgId = null; var DistributorCountry = null;
            if (this.checkrole()) {  
                if (this.itsinstructor()) {
                    CountryOrgId = this.urlGetOrgId();
                     url2 = "api/Territory/wherenew/'CountryOrgId'";
                } else {
                    if (this.itsDistributor()) {
                        DistributorCountry = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'DistributorCountry'";
                    } else {
                        CountryOrgId = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
                
            }
            var keyword : any = {
                CountryOrgId: CountryOrgId,
                DistributorCountry: DistributorCountry,
            };
            keyword = JSON.stringify(keyword);
            data = '';
            this.service.httpClientPost(url2, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                    this.allcountries = this.dropdownListCountry;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsCountry = [];
        }

        //setting dropdown
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelect(item: any) { }
    OnItemDeSelect(item: any) { }
    onSelectAll(items: any) { }
    onDeSelectAll(items: any) { }

    arraygeoid = [];
    arrayterritory = [];
    arraycountry = [];

    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }
         var  DistTerr = null;
            if (this.itsDistributor()) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';

            /* populate data issue role distributor */
           // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
            var url = "api/Countries/filterByGeoByTerritory";
            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }

            var  DistTerr = null;
            if (this.itsDistributor()) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        // Countries
        var data = '';

        /* populate data issue role distributor */
     //   var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
            var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
        if (this.itsDistributor()) {
            DistTerr = this.urlGetOrgId();
           url = "api/Countries/DistTerr";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistTerr:DistTerr,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    
  

    onGeoSelect(item: any) {
        //task malam sabtu 27/07/2018
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';

        /* populate data issue role distributor */
     //   var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        var  DistGeo = null;
            if (this.itsDistributor()) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });

    }

    onCountrySelect(item: any) {
        //task malam sabtu 27/07/2018
        this.selectedItemsState= [];
       /// this.selectedItemsTerritories = [];

        var tmpCountry = "''"
        this.arraycountry = [];
        if (this.selectedItemsCountry.length > 0) {
           // for (var i = 0; i < this.selectedItemsCountry.length; i++) {
           //     this.arraycountry.push('"' + this.selectedItemsCountry[i].id + '"');
           // }
            this.arraycountry.push('"' + this.selectedItemsCountry[0].id + '"');
            tmpCountry = this.arraycountry.toString()
        }

        var data = '';
        this.service.httpClientGet('api/State/whereState/' + tmpCountry + '', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListState = null;
                }
                else {
                    this.dropdownListState = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].StateID,
                          itemName: result[Index].StateName
                        }
                      })
                   this.dropdownListState = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].StateID,
                          itemName: result[Index].StateName
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsState = [];
      


    
     //   this.getState(tmpCountry)

        

    }

    onPartnerSelect(item) {
        
        var url = "api/Roles/wheresub/{RoleCode:'" + item + "'}" ;      
  // subpartner type
  var data = '';
  this.service.httpClientGet(url, data)
      .subscribe((result:any) => {
          if (result == "Not found") {
              this.service.notfound();
              this.dropdownListSubPartnerType = null;
          }
          else {
              this.dropdownListSubPartnerType = result.sort((a,b)=>{
                if(a.RoleName > b.RoleName)
                    return 1
                else if(a.RoleName < b.RoleName)
                    return -1
                else return 0
              }).map(function (el) {
                  return {
                    id: el.RoleId,
                    itemName: el.RoleName
                  }
                })
             this.selectedItemsSubPartnerType = result.map(function (el) {
                  return {
                    id: el.RoleId,
                    itemName: el.RoleName
                  }
                })
          }
      },
          error => {
              this.service.errorserver();
          });
  this.selectedItemsSubPartnerType = [];




    }



    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';

        /* populate data issue role distributor */
     //   var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);
        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';

        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
        this.service.httpClientPost(url, keyword)   
        .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }
   
    onCountriesSelect(item: any) {
        // let country_temp: any;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
         this.onCountrySelect(item);

    }
    OnCountriesDeSelect(item: any) { 
        this.onCountrySelect(item);
    }
    onCountriesSelectAll(items: any) { 
        this.onCountrySelect(items);
    }
    onCountriesDeSelectAll(items: any) { 
        this.onCountrySelect(items);
    }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    //submit form
    onSubmit() {
        this.loading = true;
        //jangan pakai clear, kalau engga ntar kehapus semua yg ada di local storage
        localStorage.removeItem("filter");
        var data = '';
        var geoarr = [];
        var regionarr = [];
        var subregionarr = [];
        var countryarr = [];
        var partnertypestatus = [];
        var subpartnertype =[];
        var state =[];
        var territoryarr = []
        this.data = null;
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countryarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnertypestatus.push("'" + this.selectedItemsPartnerTypeStatus[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
            territoryarr.push("'" + this.selectedItemsTerritories[i].id + "'");
        }
        for (var i = 0; i < this.selectedItemsSubPartnerType.length; i++) {
            subpartnertype.push("'" + this.selectedItemsSubPartnerType[i].id + "'");
        }
        for (var i = 0; i < this.selectedItemsState.length; i++) {
            state.push("'" + this.selectedItemsState[i].id + "'");
        }

        //Buat object untuk filter yang dipilih
        var params =
        {
            OrgId: this.filterorgform.value.OrgId,
            OrgName: this.filterorgform.value.OrgName,
            Partner: this.filterorgform.value.PartnerType,
           // SubPartnerType: this.filterorgform.value.SubPartnerType,
            PartnerStatus: this.selectedItemsPartnerTypeStatus,
            dropdownStatus: this.dropdownListPartnerTypeStatus,
            subpartnertype: this.selectedItemsSubPartnerType,
            dropdownsubpartnertype: this.dropdownListSubPartnerType,
            Geo: this.selectedItemsGeo,
            dropdownGeo: this.dropdownListGeo,
            Territory: this.selectedItemsTerritories,
            dropdownTerritory: this.dropdownListTerritories,
            Country: this.selectedItemsCountry,
            dropdownCountry: this.dropdownListCountry,
            state: this.selectedItemsState,
            dropdownstate: this.dropdownListState,
            StateProvince: this.filterorgform.value.RegisteredStateProvince

        };

        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
        var dataFilter =
        {
            Organization: this.orgarr,
            // UserRoleTerritory: this.territorydefault,
            OrgId: this.filterorgform.value.OrgId,
            OrgName: this.filterorgform.value.OrgName,
            OrgNameSpecial: this.service.encoder(this.filterorgform.value.OrgName?this.filterorgform.value.OrgName:''),
          //  RegisteredStateProvince: this.filterorgform.value.RegisteredStateProvince,
            RegisteredCountryCode: countryarr.toString(),
            PartnerType: this.filterorgform.value.PartnerType,
          //  SubPartnerType: this.filterorgform.value.SubPartnerType,
            PartnerTypeStatus: partnertypestatus.toString(),          
            SubPartnerType: subpartnertype.toString(),
            Geo: geoarr.toString(),
            Territory: territoryarr.toString(),
            RegisteredStateProvince: state.toString()
        }
        var roleFilter = {};
        if (this.checkrole()) {
            if (this.itsDistributor()) {
                roleFilter = {
                    "Distributor": this.urlGetOrgId()
                }
            } else {
                roleFilter = {
                    "SiteAdmin": this.urlGetOrgId()
                }
            }
        }
        dataFilter = Object.assign(dataFilter, roleFilter);
        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost(
            this._serviceUrl + "/FilterOrg",
            dataFilter
        )
            .subscribe(result => {
                this.loading = false;
                // var finalresult = result.replace(/\t/g, " ");
                var finalresult :any = result;
                if (finalresult == "Not Found") {
                    this.service.notfound();
                    this.data = '';
                }
                else {
                    this.data = finalresult.map((el)=>{
                        let pt = []
                        if(el.PartnerType.indexOf('ATC')>-1){
                            if(el.PartnerType.indexOf('ATC(Active)')>-1)
                                pt.push('ATC(Active)')
                            else pt.push('ATC(Inactive)')
                        }
                        if(el.PartnerType.indexOf('AAP')>-1){
                            if(el.PartnerType.indexOf('AAP(Active)')>-1)
                                pt.push('AAP(Active)')
                            else pt.push('AAP(Inactive)')
                        }
                        if(el.PartnerType.indexOf('CTC')>-1){
                            if(el.PartnerType.indexOf('CTC(Active)')>-1)
                                pt.push('CTC(Active)')
                            else pt.push('CTC(Inactive)')
                        }
                        if(el.PartnerType.indexOf('MTP')>-1){
                            if(el.PartnerType.indexOf('MTP(Active)')>-1)
                                pt.push('MTP(Active)')
                            else pt.push('MTP(Inactive)')
                        }
                        if(el.PartnerType.indexOf('MED')>-1){
                            if(el.PartnerType.indexOf('MED(Active)')>-1)
                                pt.push('MED(Active)')
                            else pt.push('MED(Inactive)')
                        }
                        if(el.PartnerType.indexOf('VCP')>-1){
                            if(el.PartnerType.indexOf('VCP(Active)')>-1)
                                pt.push('VCP(Active)')
                            else pt.push('VCP(Inactive)')
                        }
                        if(el.PartnerType.indexOf('RED')>-1){
                            if(el.PartnerType.indexOf('RED(Active)')>-1)
                                pt.push('RED(Active)')
                            else pt.push('RED(Inactive)')
                        }
                        //return {...el,PartnerType: el.PartnerType.split(',')}
                        return {...el,PartnerType: pt}
                    });
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
        /* new post for autodesk plan 17 oct */
    }

    stopquery() {
        this.data = '';
    }
}
