import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchOrganizationEPDBComponent } from './search-organization-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";

import { DataFilterOrganizationPipe, ReplacePipe } from './search-organization-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const SearchOrganizationEPDBRoutes: Routes = [
  {
    path: '',
    component: SearchOrganizationEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.search.search_organization',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchOrganizationEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [SearchOrganizationEPDBComponent, DataFilterOrganizationPipe, ReplacePipe],
  providers: [AppService, AppFilterGeo]
})
export class SearchOrganizationEPDBModule { }
