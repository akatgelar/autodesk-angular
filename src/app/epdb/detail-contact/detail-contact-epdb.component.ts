import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { FileUploader } from "ng2-file-upload";
const URL = 'https://evening-anchorage-3159.herokuapp.comapi/';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe){}
    transform(date: string): any {
      return this.datepipe.transform(new Date(date.substr(0,10)), 'dd-MMMM-yyyy');
    }
}

@Pipe({
  name: 'dateFormatPipe',
})
export class dateFormatPipe implements PipeTransform {
  transform(value: string) {
     var datePipe = new DatePipe("en-US");
      value = datePipe.transform(value, 'yMMMMd');
      return value;
  }
}

@Pipe({ name: 'dataFilterAffiliatedSites' })
export class DataFilterAffiliatedSitesPipe {
  transform(array: any[], query: string): any {
    // console.log(query);
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteAddress1.toLowerCase().indexOf(query.toLowerCase()) > -1)
        // (row.Status.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

// Qualification
@Pipe({ name: 'dataFilterQ' })
export class DataFilterQPipe {
  transform(array: any[], query: string): any {
    // console.log(query);
    if (query) {
      return _.filter(array, row =>
        (row.productName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.QualificationType.toLowerCase().indexOf(query.toLowerCase()) > -1)
        // (row.QualificationDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        // (row.Comments.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

// Contact Journal Entries
@Pipe({ name: 'dataFilterCJE' })
export class DataFilterCJEPipe {
  transform(array: any[], query: string): any {
    // console.log(query);
    if (query) {
      return _.filter(array, row =>
        // (row.AddedBy.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityName.toLowerCase().indexOf(query.toLowerCase()) > -1)
        // (row.ActivityDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        // (row.Notes.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Component({
  selector: 'app-detail-contact-epdb',
  templateUrl: './detail-contact-epdb.component.html',
  styleUrls: [
    './detail-contact-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class DetailContactEPDBComponent implements OnInit {

  private _serviceUrl = 'api/MainContact';

  public rowsOnPage1: number = 10;
  public filterQuery1: string = "";
  public sortBy1: string = "SiteName";
  public sortOrder1: string = "asc";
  private validationError: Boolean = false;
  private validationMsg: string ="";

  // Qualification
  public rowsOnPage2: number = 10;
  public filterQuery2: string = "";
  public sortBy2: string = "productName";
  public sortOrder2: string = "asc";

  // Contact
  public rowsOnPage3: number = 10;
  public filterQuery3: string = "";
  public sortBy3: string = "ActivityName";
  public sortOrder3: string = "asc";

  // Contact Atrr
  public rowsOnPage4: number = 10;
  public filterQuery4: string = "";
  public sortBy4: string = "";
  public sortOrder4: string = "asc";

  messageError: string = '';
  public datadetail: any;
  public data: any;
  public datacontact: any;
  link: string = "";
  public loading = false;

  Status = [];

  // Training Contact
  contactqualificationform: FormGroup;
  contactjournalentrytypeform: FormGroup;
  contactattributeform: FormGroup;
  contactmigrationanomalieform: FormGroup;
  contactattachmentform: FormGroup;

  // Sales contact
  contactsecurityroleform: FormGroup;
  varcontactattribute: FormGroup;
  varcontactjournalentriesform: FormGroup;

  id: string;
  siteid: string;

  modelPopup1: NgbDateStruct;
  modelPopup2: NgbDateStruct;

  uploader: FileUploader = new FileUploader({
    url: URL,
    isHTML5: true
  });
  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;

  private isActive:Boolean= false ;
  private statusActivate: string ='';
  private TerminateReason: string ='';
  modalReference: NgbModalRef;
  public contactQualifications: any;
  useraccesdata:any;

  constructor(private http: HttpClient, private session: SessionService, private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate,private modalService: NgbModal) {
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
  }

  accessEditContactBtn:Boolean=true;
  accessSiteAffiliationBtn:Boolean=true;
  accessAddContactQualificationBtn:Boolean=true;
  accessEditContactQualificationBtn:Boolean=true;
  accessAddContactJournalBtn:Boolean=true;
  accessEditContactJournalBtn:Boolean=true;

  ngOnInit() {
    this.Status = [
      { id: 1, name: "Active" },
      { id: 2, name: "Disabled" }
  ];

    this.accessEditContactBtn = this.session.checkAccessButton("manage/contact/edit-contact");
    this.accessSiteAffiliationBtn = this.session.checkAccessButton("manage/contact/site-affiliations");
    this.accessAddContactQualificationBtn = this.session.checkAccessButton("manage/contact/add-contact-qualifications");
    this.accessEditContactQualificationBtn = this.session.checkAccessButton("manage/contact/edit-contact-qualifications");
    this.accessAddContactJournalBtn = this.session.checkAccessButton("manage/contact/add-contact-journal");
    this.accessEditContactJournalBtn = this.session.checkAccessButton("manage/contact/edit-contact-journal");

    this.loading = true;
    //get data contact
    this.id = this.route.snapshot.params['id'];
    this.siteid = this.route.snapshot.params['siteid'];
    var data = '';

    this.service.httpClientGet(this._serviceUrl + '/DetailContact/' + this.id + '/' + this.siteid, data)
      .subscribe(result => {
        // console.log(result);
        if (result == "Not Found") {
          this.datadetail = '';
          this.loading = false;
        } else {
          this.datadetail = result;
          if(this.datadetail.Status === 'A'){
              this.isActive = true;
          }else{
            this.isActive = false;
          }
          if (this.datadetail != null) {
            if (this.datadetail.Comments != null && this.datadetail.Comments != "") {
              this.datadetail.Comments = this.datadetail.Comments.replace("&#10;", " ");
              this.datadetail.Comments = this.service.decoder(this.datadetail.Comments);
            }
            if (this.datadetail.ContactName != null && this.datadetail.ContactName != "") {
              this.datadetail.ContactName = this.service.decoder(this.datadetail.ContactName);
            }
            if (this.datadetail.Department != null && this.datadetail.Department != "") {
              this.datadetail.v = this.service.decoder(this.datadetail.Department);
            }
            if (this.datadetail.EmailAddress != null && this.datadetail.EmailAddress != "") {
              this.datadetail.EmailAddress = this.service.decoder(this.datadetail.EmailAddress);
            }
            if (this.datadetail.LastLogin != null && this.datadetail.LastLogin != "") {
              this.datadetail.LastLogin = this.service.decoder(this.datadetail.LastLogin);
            }
            if (this.datadetail.RoleName != null && this.datadetail.RoleName != "") {
              this.datadetail.RoleName = this.service.decoder(this.datadetail.RoleName);
            }
            if (this.datadetail.Designation != null && this.datadetail.Designation != "") {
              this.datadetail.Designation = this.service.decoder(this.datadetail.Designation);
            }
          }
          // console.log(this.datadetail);
          this.link = this.datadetail.ContactIdInt + "," + this.siteid;

          setTimeout(() => {
            this.getContactQualifications(this.datadetail.ContactId);
          }, 500)

          setTimeout(() => {
            this.getContactJournal();
          }, 1000)

          setTimeout(() => {
            this.getsitecontact(this.datadetail.OrganizationId);
          }, 1500)

          /* issue 17102018 - add back contact attribute */

          setTimeout(() => {
            this.getContactAttr(this.datadetail.ContactId);
          }, 2000)

          /* end line issue 17102018 - add back contact attribute */

        }
      },
      error => {
        this.service.errorserver();
        this.datadetail = '';
        this.loading = false;
      });
  }

  /* issue 17102018 - add back contact attribute */

  contactAtt:any;
  getContactAttr(id){
    var activityType = "Contact Attribute";
    this.service.httpClientGet("api/OrganizationJournalEntries/ActivityJournal/" + id + "/" + activityType, '')
      .subscribe(res => {
        if (res == "Not Found") {
          this.contactAtt = '';
        } else {
          //console.log(res);
          this.contactAtt = res;
          if (this.contactAtt != null) {
            for (var i = 0; i < this.contactAtt.length; i++) {
              if (this.contactAtt[i].Notes != null && this.contactAtt[i].Notes != '') {
                this.contactAtt[i].Notes = this.contactAtt[i].Notes.replace("&#10;", " ");
                this.contactAtt[i].Notes = this.service.decoder(this.contactAtt[i].Notes);
              }
            }
          }
        }
      }, error => {
        this.contactAtt = '';
      });
  }

  /* end line issue 17102018 - add back contact attribute */

  getContactQualifications(id) {
    var data = '';
    this.service.httpClientGet('api/Qualifications/contact/' + id, data)
      .subscribe(result => {
        if (result == "Not Found") {
          this.contactQualifications = '';
        } else {
          this.contactQualifications = result;

          if (this.contactQualifications != null) {
            for (var i = 0; i < this.contactQualifications.length; i++) {
              if (this.contactQualifications[i].Comments != null && this.contactQualifications[i].Comments != '') {
                this.contactQualifications[i].Comments = this.contactQualifications[i].Comments.replace("&#10;", " ");
                this.contactQualifications[i].Comments = this.service.decoder(this.contactQualifications[i].Comments);
              }
            }
          }
        }
      },
        error => {
          this.service.errorserver();
          this.contactQualifications = '';
        });
  }

  back(){
    window.history.back()
  }
  
  urlGetOrgId(): string {
    var orgarr = this.useraccesdata.OrgId.split(',');
    var orgnew = [];
    for (var i = 0; i < orgarr.length; i++) {
        orgnew.push('"' + orgarr[i] + '"');
    }
    return orgnew.toString();
  }

  checkrole(): boolean {
  let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
            return false;
        } else {
            return true;
        }
    }
  }

  affiliatesite = [];
  getsitecontact(orgid) {
    this.loading = true;
    var data = "";

    /* populate based role (distributor) */
    var url = 'api/MainSite/Affiliate/' + this.id + '/' + orgid;
    if(this.checkrole()){
      url = 'api/MainSite/AffiliateDistributor/' + this.id + '/' + orgid + '/' + this.urlGetOrgId();
    }

    this.service.httpClientGet(url, data) /* populate based role (distributor) */
      .subscribe(result => {
        if (result == null) {
          this.service.notfound();
          this.data = '';
          this.loading = false;
        }
        else {
          this.data = result;
          for (var i = 0; i < this.data.length; i++) {
            if (this.data[i].CheckedSites == true) {
              this.affiliatesite.push(this.data[i]);
            }
          }
          this.loading = false;
        }
      },
      error => {
        this.service.errorserver();
        this.data = '';
        this.loading = false;
      });
  }

  contactJournal : any;
  getContactJournal() {

    /* get contactid - 10102018 */

    this.service.httpClientGet("api/ContactAll/ContactIdInt/" + this.id, '')
    .subscribe(res => {
        let data:any = res;

        var activityType = "Contact Journal Entry";
        this.contactJournal = [];
        this.service.httpClientGet("api/OrganizationJournalEntries/ActivityJournal/" +  data.ContactId + "/" + activityType, '')
          .subscribe(res => {
            this.contactJournal = res;
            if (this.contactJournal != null) {
              for (var i = 0; i < this.contactJournal.length; i++) {
                if (this.contactJournal[i].Notes != null && this.contactJournal[i].Notes != '') {
                  this.contactJournal[i].Notes = this.contactJournal[i].Notes.replace("&#10;", " ");
                  this.contactJournal[i].Notes = this.service.decoder(this.contactJournal[i].Notes);
                }
              }
            }
          }, error => {
            this.service.errorserver();
          });

    }, error => {
        this.service.errorserver();
    });

    /* end line get contactid - 10102018 */
  }

  routeaffiliate(ContactIdInt, SiteId, OrganizationId) {
    //redirect
    this.router.navigate(['/manage/contact/site-affiliations', ContactIdInt, SiteId, OrganizationId], { queryParams: { link: "contact", code: this.link } });
  }

  addContactQualificationsRoute() {
    this.router.navigate(['/manage/contact/add-contact-qualifications'], { queryParams: { ContactIdInt: this.id, ContactId: this.datadetail.ContactId, SiteId: this.siteid } });
  }

  editQualificationRoute(id) {
    this.router.navigate(['/manage/contact/edit-contact-qualifications', id], { queryParams: { ContactId: this.id, SiteId: this.siteid } });
  }

  addContactJournalRoute() {
    this.router.navigate(['/manage/contact/add-contact-journal'], { queryParams: { ContactId: this.id, SiteId: this.siteid } });
  }

  editContactJournalRoute(id) {
    this.router.navigate(['/manage/contact/edit-contact-journal', id], { queryParams: { ContactId: this.id, SiteId: this.siteid } });
  }

  /* issue 17102018 - add back contact attribute */

  addContactAttribute(){
    this.router.navigate(['/manage/contact/add-contact-attribute'], { queryParams: { ContactId: this.id, SiteId: this.siteid } });
  }
  editContactAtt(id) {
    this.router.navigate(['/manage/contact/edit-contact-attribute', id], { queryParams: { ContactId: this.id, SiteId: this.siteid } });
  }

  /* end line issue 17102018 - add back contact attribute */


  ChangeStatus(){
    var ststus;
    var modelData;
    this.validationMsg = "";
    this.validationError = false;

    if(this.TerminateReason != ''){     
      modelData ={
        Status:this.isActive,
        Reason:this.TerminateReason
      }
      this.service.httpCLientPut('api/ContactAll/ChangeUserStatus/'+ this.datadetail.ContactId, JSON.stringify(modelData))
      .subscribe(result => {
        this.JoinAndClose();
        var res = result;
        if(res['code'] == true){
          if(this.isActive ){
            ststus ="A";
          }else{
            ststus ="I";
          }
          this.datadetail.Status = ststus;
          this.datadetail.TerminationReason = this.TerminateReason;
          swal(
            'Information!',
            res['message'],
            'success'
          );
          
          this.loading = false;
          this.TerminateReason= '';
        }else{
          
          this.loading = false;
          swal(
            'Information!',
            res['message'],
            'error'
          );
        }
      }, error => {
        this.JoinAndClose();
        this.TerminateReason= '';
        console.log("failed");
        this.loading = false;
      });
    }else{
      this.validationError = true;
      this.validationMsg = "Please Fill in the reason for disabling the user";
    }
  }
  open(content,item) {
    if(this.isActive){
      this.statusActivate = "Activate"
    }else{
      this.statusActivate = "Deactivate"
    }
    this.modalReference = this.modalService.open(content);
  }

  JoinAndClose() {
    this.modalReference.close();
    }

  generatePassword(value){
    this.loading = true;
    var data = {
      "ContactId":value,
      Userid : this.useraccesdata.Userid,
      cuid : this.useraccesdata.ContactName
    }

    /* autodesk plan 10 oct */

    this.service.httpCLientPut('api/Auth/GeneratePass',data)
      .subscribe(result => {
        var res = result;
        if(res['code'] == "1"){
          swal(
            'Information!',
            res['message'],
            'success'
          );
          this.loading = false;
        }else{
          console.log("failed");
          this.loading = false;
        }
      }, error => {
        console.log("failed");
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */
  }

  generatePasswordPopUp(value){
    this.loading = true;
    var data = {
      "ContactId":value,
      Userid : this.useraccesdata.UserId,
      cuid : this.useraccesdata.ContactName
    }

    /* autodesk plan 10 oct */

    this.service.httpCLientPut('api/Auth/GeneratePassPopUp',data)
      .subscribe(result => {
        var res = result;
        if(res['code'] == "1"){
          swal(
            'Information!',
            res['message'],
            'success'
          );
          this.loading = false;
        }else{
          console.log("failed");
          this.loading = false;
        }
      }, error => {
        console.log("failed");
        this.loading = false;
      });

    /* end line autodesk plan 10 oct */
  }

  /* change request 28082018 - delete contact */

  deleteContact(value){
    swal({
      title: 'Are you sure?',
      text: "This action will delete all related data!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
        if (result == true) {
          this.loading = true;
          this.service.httpCLientPut('api/ContactAll/DeleteContact/'+value,'')
            .subscribe(result => {
              var res =result;
              if(res['code'] == "1"){
                swal(
                  'Information!',
                  res['message'],
                  'success'
                );
                this.router.navigate(['/manage/contact/search-contact']);
                this.loading = false;
              }else{
                swal(
                  'Information!',
                  res['message'],
                  'success'
                );
                this.loading = false;
              }
            }, error => {
              this.loading = false;
            });
        }
    }).catch(swal.noop);
  }

  /* change request 28082018 - delete contact */

  /* issue 17102018 - add delete function */

  deleteQualification(id){
    swal({
      title: 'Are you sure?',
      text: "This action will delete all related data!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
        if (result == true) {
          this.service.httpClientDelete('api/Qualifications/' + id + '/' + this.useraccesdata.ContactName + '/' + this.useraccesdata.UserId + '/' + this.datadetail.ContactId,'')
            .subscribe(result => {
              var res = result;
              if(res['code'] == "1"){
                this.ngOnInit();
              }else{
                console.log("failed");
              }
            }, error => {
              console.log("failed");
            });
        }
    }).catch(swal.noop);
  }

  deleteJournalAndAttribute(id){
    swal({
      title: 'Are you sure?',
      text: "This action will delete all related data!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
        if (result == true) {
          this.service.httpClientDelete('api/OrganizationJournalEntries/' + id + '/' + this.useraccesdata.ContactName + '/' + this.useraccesdata.UserId, '')
            .subscribe(result => {
              var res = result;
              if(res['code'] == "1"){
                this.ngOnInit();
              }else{
                console.log("failed");
              }
            }, error => {
              console.log("failed");
            });
        }
    }).catch(swal.noop);
  }

  /* end line issue 17102018 - add delete function */

/* add delete function - 31 oct */
  deleteContactbyID(id){
    swal({
      title: 'Are you sure?',
      text: "This action will delete all related data!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result == true) {
        this.loading = true;
       var data =
       {
        id: id,
        UserId:this.useraccesdata.UserId,
        cuid :this.useraccesdata.ContactName
       }
       let delData = JSON.stringify(data);
        this.service.httpClientPost("api/MainContact/Delete",delData)
          .subscribe(result => {
            var res = result;
            if(res['code'] == "1"){
              swal(
                'Information!',
                res['message'],
                'success'
              );
              this.router.navigate(['/manage/contact/search-contact']);
              this.loading = false;
            }else{
              swal(
                'Information!',
                res['message'],
                'success'
              );
              this.loading = false;
            }
          }, error => {
            this.loading = false;
          });
      }
  }).catch(swal.noop);
}

}
