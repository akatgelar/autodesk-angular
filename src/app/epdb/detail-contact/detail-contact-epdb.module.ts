import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailContactEPDBComponent } from './detail-contact-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { DataFilterAffiliatedSitesPipe, DataFilterQPipe, DataFilterCJEPipe, ConvertDatePipe, dateFormatPipe } from './detail-contact-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';

export const DetailContactEPDBRoutes: Routes = [
  {
    path: '',
    component: DetailContactEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.detail.detail_contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailContactEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [DetailContactEPDBComponent, DataFilterAffiliatedSitesPipe, DataFilterQPipe, DataFilterCJEPipe, ConvertDatePipe, dateFormatPipe],
  providers: [AppService, AppFormatDate, SessionService,DatePipe]
})
export class DetailContactEPDBModule { }
