import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

import {FileUploader} from "ng2-file-upload";
const URL = 'https://evening-anchorage-3159.herokuapp.comapi/';

@Component({
  selector: 'app-edit-orgattachments-epdb',
  templateUrl: './edit-orgattachments-epdb.component.html',
  styleUrls: [
    './edit-orgattachments-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class EditOrgAttachmentsEPDBComponent implements OnInit {

  uploader: FileUploader = new FileUploader({
    url: URL,
    isHTML5: true
  });
  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      
  
    }, 1);
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

}