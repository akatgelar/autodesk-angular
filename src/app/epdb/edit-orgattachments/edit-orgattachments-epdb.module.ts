import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditOrgAttachmentsEPDBComponent } from './edit-orgattachments-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const EditOrgAttachmentsEPDBRoutes: Routes = [
  {
    path: '',
    component: EditOrgAttachmentsEPDBComponent,
    data: {
      breadcrumb: 'Learning History',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditOrgAttachmentsEPDBRoutes),
    SharedModule
  ],
  declarations: [EditOrgAttachmentsEPDBComponent]
})
export class EditOrgAttachmentsEPDBModule { }
