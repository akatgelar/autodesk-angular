import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DetailSiteEPDBComponent } from './detail-site-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";

import { ReplacePipe, DataFilterContactPipe, DataFilterPartnerTypePipe, DataFilterSiteAccreditationPipe, DataFilterContactQualificationPipe, DataFilterSJEPipe, DataFilterAProgPipe, DataFilterATargPipe, DataFilterAProjPipe, ConvertDatePipe, DataFilterAttrPipe } from './detail-site-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { SessionService } from '../../shared/service/session.service';

export const DetailSiteEPDBRoutes: Routes = [
  {
    path: '',
    component: DetailSiteEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.site.detail.detail_site',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailSiteEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations:
    [
      ReplacePipe, DetailSiteEPDBComponent, DataFilterContactPipe, DataFilterPartnerTypePipe, DataFilterSiteAccreditationPipe, DataFilterContactQualificationPipe, DataFilterSJEPipe, DataFilterAProgPipe, DataFilterATargPipe, DataFilterAProjPipe, ConvertDatePipe, DataFilterAttrPipe
    ],
  providers: [AppService, AppFormatDate, SessionService, DatePipe]
})
export class DetailSiteEPDBModule { }
