import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { DatePipe } from '@angular/common';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      let newValue = value.replace(/,/g, "<br/>")
      return `${newValue}`;
    }
  }
}

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
  constructor(private datepipe: DatePipe) { }
  transform(date: string): any {
    return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
  }
}

// Contact
@Pipe({ name: 'dataFilterContact' })
export class DataFilterContactPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.InstructorId.toLowerCase().indexOf(query.toLowerCase()) > -1) || //fixed issue 20092018 - Detail site, contact table, grid view search with instructor id is not working. Site id: AP0355
        (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.EmailAddress.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.CountSites.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Partner Type
@Pipe({ name: 'dataFilterPartnerType' })
export class DataFilterPartnerTypePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.CSN.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.RoleName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ParamValue.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Site Accreditation
@Pipe({ name: 'dataFilterSiteAccreditation' })
export class DataFilterSiteAccreditationPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.AddedBy.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Notes.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Contact Qualification
@Pipe({ name: 'dataFilterContactQualification' })
export class DataFilterContactQualificationPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.AutodeskProduct.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Site Journal Entries
@Pipe({ name: 'dataFilterSJE' })
export class DataFilterSJEPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.AddedBy.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Notes.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Academic Program
@Pipe({ name: 'dataFilterAProg' })
export class DataFilterAProgPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Usage.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetEducator.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetStudent.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetInstitution.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Academic Target
@Pipe({ name: 'dataFilterATarg' })
export class DataFilterATargPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Usage.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.FYIndicator.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetEducator.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetStudent.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetInstitution.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Academic Project
@Pipe({ name: 'dataFilterAProj' })
export class DataFilterAProjPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.CompletionDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetEducator.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.TargetStudent.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.productName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

/* issue 17102018 - add back site attribute */

// Site Journal Entries
@Pipe({ name: 'dataFilterAttr' })
export class DataFilterAttrPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.AddedBy.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ActivityDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Notes.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

/* end line issue 17102018 - add back site attribute */

@Component({
  selector: 'app-detail-site-epdb',
  templateUrl: './detail-site-epdb.component.html',
  styleUrls: [
    './detail-site-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class DetailSiteEPDBComponent implements OnInit {
  @ViewChild('fileUploader') fileUploader
  private _serviceUrl = 'api/MainSite';
  private attachments = []
  // Contact
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "ContactName";
  public sortOrder: string = "asc";

  // Partner Type
  public rowsOnPagePT: number = 10;
  public filterQueryPT: string = "";
  public sortByPT: string = "RoleName";
  public sortOrderPT: string = "asc";

  // Site Accreditation
  public rowsOnPageSA: number = 10;
  public filterQuerySA: string = "";
  public sortBySA: string = "name";
  public sortOrderSA: string = "desc";

  // Contact Qualification
  public rowsOnPageCQ: number = 10;
  public filterQueryCQ: string = "";
  public sortByCQ: string = "name";
  public sortOrderCQ: string = "desc";

  // Site Journal Entries
  public rowsOnPageSJE: number = 10;
  public filterQuerySJE: string = "";
  public sortBySJE: string = "name";
  public sortOrderSJE: string = "desc";

  // Academic Program
  public rowsOnPageAP: number = 10;
  public filterQueryAP: string = "";
  public sortByAP: string = "name";
  public sortOrderAP: string = "desc";

  // Academic Target
  public rowsOnPageAT: number = 10;
  public filterQueryAT: string = "";
  public sortByAT: string = "name";
  public sortOrderAT: string = "desc";

  // Academic Project
  public rowsOnPageAProj: number = 10;
  public filterQueryAProj: string = "";
  public sortByAProj: string = "name";
  public sortOrderAProj: string = "desc";

  // Site Attribute
  public rowsOnPageAttr: number = 10;
  public filterQueryAttr: string = "";
  public sortByAttr: string = "";
  public sortOrderAttr: string = "desc";

  messageError: string = '';
  public datadetail: any;
  public orgid: any;
  public data: any;
  public datacontact: any;
  id: string = "";
  contactcount: number = 0;
  public partnertype: any;
  link: string = "";
  public contactQualifications: any;
  private SiteId;
  siteAccr: any;
  siteJournal: any;
  academicProject: any;
  private iniSiteId;
  public loading = false;
  useraccessdata:any;

  constructor(private session: SessionService, private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate, private datePipe: DatePipe) { //, private htmlEntityService: htmlentityService

    let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

  }

  getSiteAccreditation(id) {
    var data: any;
    var activityType = "Site Accreditation";
    this.siteAccr = [];
    this.service.httpClientGet("api/OrganizationJournalEntries/ActivityJournal/" + id + "/" + activityType, data)
      .subscribe(res => {
        this.siteAccr = res;
      }, error => {
        this.service.errorserver();
      });
  }

  getAcademicProject(id) {
    var data: any;
    this.academicProject = [];
    this.service.httpClientGet('api/AcademicTargetProgram/AcademicProject/' + id, data)
      .subscribe(result => {
        this.academicProject = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  back(){
    window.history.back()
  }
  
  getSiteJournal(id) {
    var data: any;
    var activityType = "Site Journal Entry";
    this.siteJournal = [];
    this.service.httpClientGet("api/OrganizationJournalEntries/ActivityJournal/" + id + "/" + activityType, data)
      .subscribe(res => {
        this.siteJournal = res;
      }, error => {
        this.service.errorserver();
      });
  }

  /* issue 17102018 - add back site attribute */

  siteAttribute:any;
  getSiteAttribute(id) {
    var data: any;
    var activityType = "Site Attribute";
    this.service.httpClientGet("api/OrganizationJournalEntries/ActivityJournal/" + id + "/" + activityType, data)
      .subscribe(res => {console.log(res)
        this.siteAttribute = res;
      }, error => {
        this.service.errorserver();
      });
  }

  /* end line issue 17102018 - add back site attribute */

  accessEditSiteBtn: Boolean = true;
  accessAddContactBtn: Boolean = true;
  accessSiteAffiliationBtn: Boolean = true;
  accessAddPartnerTypeBtn: Boolean = true;
  accessUpdatePartnerTypeBtn: Boolean = true;
  accessAddSiteAccreditationBtn: Boolean = true;
  accessUpdateSiteAccreditationBtn: Boolean = true;
  accessAddSiteJournalBtn: Boolean = true;
  accessUpdateSiteJournalBtn: Boolean = true;
  accessAddAcademicProgramBtn: Boolean = true;
  accessEditAcademicProgramBtn: Boolean = true;
  accessAddAcademicTargetBtn: Boolean = true;
  accessEditAcademicTargetBtn: Boolean = true;
  accessAddAcademicProjectBtn: Boolean = true;
  accessEditAcademicProjectBtn: Boolean = true;
  ngOnInit() {
    this.accessEditSiteBtn = this.session.checkAccessButton("manage/site/edit-site");
    this.accessAddContactBtn = this.session.checkAccessButton("manage/contact/add-contact");
    this.accessSiteAffiliationBtn = this.session.checkAccessButton("manage/contact/site-affiliations");
    this.accessAddPartnerTypeBtn = this.session.checkAccessButton("manage/site/add-partner-type");
    this.accessUpdatePartnerTypeBtn = this.session.checkAccessButton("manage/site/update-partner-type");
    this.accessAddSiteAccreditationBtn = this.session.checkAccessButton("manage/site/add-site-accreditation");
    this.accessUpdateSiteAccreditationBtn = this.session.checkAccessButton("manage/site/update-site-accreditation");
    this.accessAddSiteJournalBtn = this.session.checkAccessButton("manage/site/add-site-journal");
    this.accessUpdateSiteJournalBtn = this.session.checkAccessButton("manage/site/update-site-journal");
    this.accessAddAcademicProgramBtn = this.session.checkAccessButton("manage/site/add-site-academic-programs");
    this.accessEditAcademicProgramBtn = this.session.checkAccessButton("manage/site/edit-site-academic-programs");
    this.accessAddAcademicTargetBtn = this.session.checkAccessButton("manage/site/add-site-academic-programs");
    this.accessEditAcademicTargetBtn = this.session.checkAccessButton("manage/site/edit-site-academic-programs");
    this.accessAddAcademicProjectBtn = this.session.checkAccessButton("manage/site/add-academic-project");
    this.accessEditAcademicProjectBtn = this.session.checkAccessButton("manage/site/update-academic-project");

    this.loading = true;
    //get data site
    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
      .subscribe(result => {
        if (result == "Not found" || result == null) {
          this.datadetail = '';
          this.loading = false;
        } else {
          this.datadetail =result;
          if (this.datadetail != null) {
            if (this.datadetail.AdminNotes != null && this.datadetail.AdminNotes != '') {
              this.datadetail.AdminNotes = this.datadetail.AdminNotes.replace("&#10;", " ");
              this.datadetail.AdminNotes = this.service.decoder(this.datadetail.AdminNotes);
            }
            if (this.datadetail.SiteCity != null && this.datadetail.SiteCity != '') {
              this.datadetail.SiteCity = this.service.decoder(this.datadetail.SiteCity);
            }
            if (this.datadetail.SitePostalCode != null && this.datadetail.SitePostalCode != '') {
              this.datadetail.SitePostalCode = this.service.decoder(this.datadetail.SitePostalCode);
            }
            if (this.datadetail.MailingCity != null && this.datadetail.MailingCity != '') {
              this.datadetail.MailingCity = this.service.decoder(this.datadetail.MailingCity);
            }
            if (this.datadetail.MailingPostalCode != null && this.datadetail.MailingPostalCode != '') {
              this.datadetail.MailingPostalCode = this.service.decoder(this.datadetail.MailingPostalCode);
            }
            if (this.datadetail.ShippingCity != null && this.datadetail.ShippingCity != '') {
              this.datadetail.ShippingCity = this.service.decoder(this.datadetail.ShippingCity);
            }
            if (this.datadetail.ShippingPostalCode != null && this.datadetail.ShippingPostalCode != '') {
              this.datadetail.ShippingPostalCode = this.service.decoder(this.datadetail.ShippingPostalCode);
            }
            if (this.datadetail.ATCDirectorFirstName != null && this.datadetail.ATCDirectorFirstName != '') {
              this.datadetail.ATCDirectorFirstName = this.service.decoder(this.datadetail.ATCDirectorFirstName);
            }
            if (this.datadetail.ATCDirectorLastName != null && this.datadetail.ATCDirectorLastName != '') {
              this.datadetail.ATCDirectorLastName = this.service.decoder(this.datadetail.ATCDirectorLastName);
            }
            if (this.datadetail.LegalContactFirstName != null && this.datadetail.LegalContactFirstName != '') {
              this.datadetail.LegalContactFirstName = this.service.decoder(this.datadetail.LegalContactFirstName);
            }
            if (this.datadetail.LegalContactLastName != null && this.datadetail.LegalContactLastName != '') {
              this.datadetail.LegalContactLastName = this.service.decoder(this.datadetail.LegalContactLastName);
            }
            if (this.datadetail.BillingContactFirstName != null && this.datadetail.BillingContactFirstName != '') {
              this.datadetail.BillingContactFirstName = this.service.decoder(this.datadetail.BillingContactFirstName);
            }
            if (this.datadetail.BillingContactLastName != null && this.datadetail.BillingContactLastName != '') {
              this.datadetail.BillingContactLastName = this.service.decoder(this.datadetail.BillingContactLastName);
            }
            if (this.datadetail.SiteWebAddress != null && this.datadetail.SiteWebAddress != '') {
              this.datadetail.SiteWebAddress = this.service.decoder(this.datadetail.SiteWebAddress);
            }
            if (this.datadetail.DateAdded != null && this.datadetail.DateAdded != '') {
              this.datadetail.DateAdded = this.service.decoder(this.datadetail.DateAdded);
            }
          }
          this.attachments =  this.datadetail.Files || []
          this.link = this.datadetail.SiteIdInt;
          this.SiteId = this.datadetail.SiteId;
          this.iniSiteId = this.datadetail.SiteId;
          this.getOrg(this.datadetail.OrgId);
			
          setTimeout(() => {
            this.getPartnerType();
          }, 500)

          setTimeout(() => {
            this.getSiteAccreditation(this.SiteId);
          }, 1000)

          setTimeout(() => {
            this.getContactQualification(this.datadetail.SiteId);
          }, 1500)

          setTimeout(() => {
            this.getSiteJournal(this.SiteId);
          }, 2000)

          // setTimeout(() => {
          //   this.getAcademicPrograms(this.datadetail.SiteId);
          // }, 2500)

          // setTimeout(() => {
          //   this.getAcademicTargets(this.datadetail.SiteId);
          // }, 3000)

          // setTimeout(() => {
          //   this.getAcademicProject(this.SiteId);
          // }, 3500)

          setTimeout(() => {
            this.getContact(this.datadetail.SiteId);
          }, 4000)

          /* issue 17102018 - add back site attribute */

          setTimeout(() => {
            this.getSiteAttribute(this.SiteId)
          }, 3000)

          /* end line issue 17102018 - add back site attribute */
           this.loading = false;
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
          this.loading = false;
        });
  }

  getPartnerType() {
    this.service.httpClientGet(this._serviceUrl + '/partnertype/' + this.id, '')
      .subscribe(result => {
        if (result == "Not Found") {
          this.partnertype = '';
        } else {
          this.partnertype =  result;
        }
      },
        error => {
          this.service.errorserver();
          this.partnertype = '';
        });
  }

  getContactQualification(siteid) {
    let qc: any;
    this.contactQualifications = [];
    this.service.httpClientGet('api/MainContact/ContactQualifications/' + this.id, qc)
      .subscribe(result => {
        qc = result;
        // qc = qc.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        // qc = JSON.parse(qc);
        
        if (qc == "Not found") {
          this.contactQualifications = '';
        } else {
          this.contactQualifications = qc;
          
        }
      },
        error => {
          this.service.errorserver();
          this.contactQualifications = '';
        });
  }

  public academicPrograms: any;
  getAcademicPrograms(siteid) {
    this.service.httpClientGet('api/AcademicTargetProgram/where/{"SiteId":"' + siteid + '","AcademicType":"AcademicPrograms"}', '')
      .subscribe(result => {
        if (result == "Not found") {
          this.academicPrograms = '';
        } else {
          this.academicPrograms = result;
        }
      },
        error => {
          this.service.errorserver();
          this.academicPrograms = '';
        });
  }

  public academicTargets: any;
  getAcademicTargets(siteid) {
    this.service.httpClientGet('api/AcademicTargetProgram/where/{"SiteId":"' + siteid + '","AcademicType":"AcademicTargets"}', '')
      .subscribe(result => {
        if (result == "Not found") {
          this.academicTargets = '';
        } else {
          this.academicTargets = result;
        }
      },
        error => {
          this.service.errorserver();
          this.academicTargets = '';
        });
  }

  routeaffiliate(ContactIdInt, SiteId, OrganizationId) {
    //redirect
    this.router.navigate(['/manage/contact/site-affiliations', ContactIdInt, SiteId, OrganizationId], { queryParams: { link: "site", code: this.link } });
  }

  getContact(id) {
    this.loading = true;
    //get data contact
    var data = '';
    let datatmp: any;
    this.service.httpClientGet("api/MainContact/getcontactwithcountsitesforsites/" + id, data)
      .subscribe(result => {
        if (result == "Not found"|| result == null) {
          this.data = '';
          this.loading = false;
        } else {
          this.data = result;
          this.contactcount = this.data.length;
          this.loading = false;

          // datatmp = result;
          // this.data = [];
          // for (var i = 0; i < datatmp.length; i++) {
          //   if (datatmp[i].ThisSite > 0) {
          //     this.data.push(datatmp[i]);
          //   }
          // }
          // if (this.data != undefined) {
          //   this.countcontact(this.data);
          // } else {
          //   this.data = ''
          //   this.loading = false;
          // }
        }
      },
        error => {
          this.service.errorserver();
          this.data = '';
          this.loading = false;
        });
  }

  getOrg(id) {
    //get data org
    var data = '';
    this.service.httpClientGet('api/MainOrganization/where/{"OrgId":"' + id + '"}', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.orgid = '';
        } else {
          this.orgid = result;
        }
      },
        error => {
          this.service.errorserver();
          this.orgid = '';
        });
  }

  addSiteAccr() {
    this.router.navigate(['/manage/site/add-site-accreditation'], { queryParams: { SiteId: this.SiteId, SiteIdInt: this.id } });
  }

  updateDisiniAjah(journalId) { //update site accreditation
    this.router.navigate(['/manage/site/update-site-accreditation'], { queryParams: { JournalId: journalId, SiteIdInt: this.id } });
  }

  addSiteJournal() {
    this.router.navigate(['/manage/site/add-site-journal'], { queryParams: { SiteId: this.SiteId, SiteIdInt: this.id } });
  }

  updateDisiniYuk(journalId) {
    this.router.navigate(['/manage/site/update-site-journal'], { queryParams: { JournalId: journalId, SiteIdInt: this.id } });
  }

  addAcademicProject() {
    this.router.navigate(['/manage/site/add-academic-project'], { queryParams: { SiteId: this.SiteId, SiteIdInt: this.id } });
  }

  updateAcademicProject(projectId) {
    this.router.navigate(['/manage/site/update-academic-project'], { queryParams: { ProjectId: projectId, SiteIdInt: this.id } });
  }

  // countcontact(contact) {
  //   for (var i = 0; i < contact.length; i++) {
  //     if (contact[i].ThisSite > 0) {
  //       this.contactcount++;
  //     }
  //   }
  //   this.loading = false;
  // }

  academicprogramroute() {
    this.router.navigate(['/manage/site/add-site-academic-programs', this.id], { queryParams: { AcademicType: "AcademicPrograms" } })
  }
  academictargetroute() {
    this.router.navigate(['/manage/site/add-site-academic-programs', this.id], { queryParams: { AcademicType: "AcademicTargets" } })
  }
  editacademicprogramroute(id) {
    this.router.navigate(['/manage/site/edit-site-academic-programs', id], { queryParams: { AcademicType: "AcademicPrograms", SiteIdInt: this.id } })
  }
  editacademictargetroute(id) {
    this.router.navigate(['/manage/site/edit-site-academic-programs', id], { queryParams: { AcademicType: "AcademicTargets", SiteIdInt: this.id } })
  }

  // openConfirmsSwal(id, index) {
  //   swal({
  //     title: 'Are you sure?',
  //     text: "You won't be able to revert this!",
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, delete it!'
  //   })
  //     .then(result => {
  //       if (result == true) {

  //       }
  //     }).catch(swal.noop);
  // }

  /* issue 17102018 - add back site attribute */

  addSiteAttr() {
    this.router.navigate(['/manage/site/add-site-attribute'], { queryParams: { SiteId: this.SiteId, SiteIdInt: this.id } });
  }

  updateSiteAttr(journalId) {
    this.router.navigate(['/manage/site/update-site-attribute'], { queryParams: { JournalId: journalId, SiteIdInt: this.id } });
  }

  /* end line issue 17102018 - add back site attribute */

  openConfirmsSwal(id) {
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      })
        .then(result => {
          if (result == true) {
            var cuid = this.useraccessdata.ContactName
            var UserId = this.useraccessdata.UserId
            var data = '';
            
            this.service.httpClientDelete('api/OrganizationJournalEntries/' + id + '/' + cuid + '/' + UserId, data)
              .subscribe(result => {
                var resource = result;
                if (resource['code'] == '1') {
                  this.ngOnInit()
                }
                else {
                  swal(
                    'Information!',
                    "Delete Data Failed",
                    'error'
                  );
                }
              },
              error => {
                this.service.errorserver();
              });
          }
        }).catch(swal.noop);
  }
  
  deleteSitebyID(id){
    let isAdmin = JSON.parse(localStorage.getItem('autodesk-data')).UserLevelId.indexOf("SUPERADMIN")>-1
    if (!this.fileUploader.valid() && !isAdmin)
      return swal('No File Found','Please add atleast 1 file','error')
    let fileData = []
	if( this.fileUploader.files.length>0){
		
    this.fileUploader.files.map((file)=>{
        let reader = new FileReader()
        let name = file.name
        reader.onload = ()=>{
            fileData.push({ name:name, data: reader.result })
        }
        reader.readAsDataURL(file);
    })
	}
    swal({
      title: 'Are you sure?',
      text: "This action will delete all related data!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result == true) {
        this.loading = true;

       var data =
       {
        id: id,
        files:fileData,     
        cuid: this.useraccessdata.ContactName,
        UserId: this.useraccessdata.UserId
       }
       let delData = JSON.stringify(data);
       var deletedSite=JSON.parse(localStorage.getItem("filter"));
        this.service.httpClientPost("api/MainSite/Delete",delData)
          .subscribe(result => {
            var res = result;
     
   // var deletedSite : any=localStorage.getItem("filter");
			if(deletedSite!=null && deletedSite.SiteId!="")
			{
				
				deletedSite.SiteId=res['SiteId'];
				localStorage.setItem("filter",deletedSite);
			}
			
            if(res['code'] == "1"){
              swal(
                'Information!',
                res['message'],
                'success'
              );
              this.router.navigate(['/manage/site/search-site']);
              this.loading = false;
            }else{
              swal(
                'Information!',
                res['message'],
                'success'
              );
              this.loading = false;
            }
          }, error => {
            this.loading = false;
          });
      }
  }).catch(swal.noop);
}
}
