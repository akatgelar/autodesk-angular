import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from '@angular/forms';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { SessionService } from '../../shared/service/session.service';

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        }
    }
}

@Component({
    selector: 'app-invoices-report-epdb',
    templateUrl: './invoice-report-epdb.component.html',
    styleUrls: [
        './invoice-report-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class InvoiceReportEPDBComponent implements OnInit {

    dropdownListCheckInvoice = [];
    selectedItemsCheckInvoice = [];

    dropdownListPartner = [];
    selectedItemsPartner = [];

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListFieldOrganization = [];
    selectedItemsFieldOrganization = [];

    dropdownListFieldInvoice = [];
    selectedItemsFieldInvoice = [];

    dropdownListFieldShow2 = [];
    selectedItemsFieldShow2 = [];

    dropdownSettings = {};

    public data: any;
    public partnertype: any;
    public partnertypestatus: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "OrganizationId";
    public sortOrder: string = "asc";
    public dataFound = false;
    searchInvoice: FormGroup;
    public loading = false;

    public datayearcurrency: any;

    //Validation Select 2
    public showValidMarketType = false;
    public showValidPartnerType = false;
    public showValidPartnerStatus = false;
    //  public showValidTerritory = false;
    //  public showValidGeo = false
    public showValidCountry = false;

    useraccesdata: any;

    constructor(private _http: Http, private session: SessionService, private service: AppService, private filterGeo: AppFilterGeo, private formatdate: AppFormatDate) {

        let invoiceNumber = new FormControl('');
        let FinancialYear = new FormControl('');

        this.searchInvoice = new FormGroup({
            invoiceNumber: invoiceNumber,
            FinancialYear: FinancialYear
        });

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    resetForm() {
        this.searchInvoice.reset();
        this.dataFound = false;
        this.dropdownListCheckInvoice = [];
        this.selectedItemsCheckInvoice = [];
        this.dropdownListPartner = [];
        this.selectedItemsPartner = [];
        this.dropdownListPartnerTypeStatus = [];
        this.selectedItemsPartnerTypeStatus = [];
        this.dropdownListMarketType = [];
        this.selectedItemsMarketType = [];
        this.dropdownListTerritories = [];
        this.selectedItemsTerritories = [];
        this.dropdownListGeo = [];
        this.selectedItemsGeo = [];
        this.dropdownListRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dropdownListFieldOrganization = [];
        this.selectedItemsFieldOrganization = [];
        this.dropdownListFieldInvoice = [];
        this.selectedItemsFieldInvoice = [];
        this.dropdownListFieldShow2 = [];
        this.selectedItemsFieldShow2 = [];
    }

    stopquery() {
        this.reportInvoice = "";
        this.listkey = null;
    }

    modelPopup1: any = null;
    modelPopup2: any = null;
    poreciveddatedate: boolean = false;
    present() {
        this.poreciveddatedate = true;
    }

    notpresent() {
        this.poreciveddatedate = false;
        this.modelPopup1 = null;
        this.modelPopup2 = null;
    }

    /*fix issue excel no 263.Show data based role*/

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /*end fix issue excel no 263.Show data based role*/

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya: Boolean = true;
    checkrole() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya: Boolean = false;
    itsinstructor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya: Boolean = false;
    itsOrganization() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya: Boolean = false;
    itsSite() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya: Boolean = false;
    itsDistributor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    reportInvoice: any = "";
    listkey: any;
    onSubmit() {
        let invoices: any;

        let partnertypearr = [];
        for (var i = 0; i < this.selectedItemsPartner.length; i++) {
            partnertypearr.push("'" + this.selectedItemsPartner[i].id + "'");
        }

        let partnertypestatusarr = [];
        for (var i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnertypestatusarr.push("'" + this.selectedItemsPartnerTypeStatus[i].id + "'");
        }

        let markettypearr = [];
        for (var i = 0; i < this.selectedItemsMarketType.length; i++) {
            markettypearr.push("'" + this.selectedItemsMarketType[i].id + "'");
        }

        let geoarr = [];
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }

        let regionarr = [];
        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
        }

        let subregionarr = [];
        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
        }

        let countriesarr = [];
        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countriesarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }

        let territoryarr = [];
        for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
            territoryarr.push("'" + this.selectedItemsTerritories[i].id + "'");
        }

        let anotherfieldarr = [];
        for (var i = 0; i < this.selectedItemsFieldShow2.length; i++) {
            anotherfieldarr.push(this.selectedItemsFieldShow2[i].id);
        }

        let orgfieldarr = [];
        for (var i = 0; i < this.selectedItemsFieldOrganization.length; i++) {
            orgfieldarr.push(this.selectedItemsFieldOrganization[i].id);
        }

        let invoicefieldarr = [];
        for (var i = 0; i < this.selectedItemsFieldInvoice.length; i++) {
            invoicefieldarr.push(this.selectedItemsFieldInvoice[i].id);
        }

        let startdate = "";
        let enddate = "";
        if (this.modelPopup1 != null || this.modelPopup2 != null) {
            startdate = this.formatdate.dateCalendarToYMD(this.modelPopup1);
            enddate = this.formatdate.dateCalendarToYMD(this.modelPopup2);
        }

        var params =
        {
            invoiceNumber: this.searchInvoice.value.invoiceNumber,
            Year: this.searchInvoice.value.FinancialYear,
            selectPartner: this.selectedItemsPartner,
            dropdownPartner: this.dropdownListPartner,
            selectPartnerStatus: this.selectedItemsPartnerTypeStatus,
            dropdownPartnerStatus: this.dropdownListPartnerTypeStatus,
            selectMarketType: this.selectedItemsMarketType,
            dropdownMarketType: this.dropdownListMarketType,
            selectTerritory: this.selectedItemsTerritories,
            dropdownTerritory: this.dropdownListTerritories,
            selectGeo: this.selectedItemsGeo,
            dropdownGeo: this.dropdownListGeo,
            selectCountry: this.selectedItemsCountry,
            dropdownCountry: this.dropdownListCountry,
            selectFieldOrg: this.selectedItemsFieldOrganization,
            dropdownFieldOrg: this.dropdownListFieldOrganization,
            selectFieldShow: this.selectedItemsFieldShow2,
            dropdownFieldShow: this.dropdownListFieldShow2,
            selectFieldInvoice: this.selectedItemsFieldInvoice,
            dropdownFieldInvoice: this.dropdownListFieldInvoice
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        if (
            this.selectedItemsPartner.length == 0 ||
            this.selectedItemsMarketType.length == 0 ||
            // this.selectedItemsTerritories.length == 0 || 
            // this.selectedItemsGeo.length == 0 ||
            this.selectedItemsCountry.length == 0 ||
            this.selectedItemsPartnerTypeStatus.length == 0

        ) {
            swal(
                'Field is Required!',
                'Please enter the Invoice Report form required!',
                'error'
            )

            if (this.selectedItemsPartner.length == 0) {
                this.showValidPartnerType = true;
            } else {
                this.showValidPartnerType = false;
            }
            if (this.selectedItemsMarketType.length == 0) {
                this.showValidMarketType = true;
            } else {
                this.showValidMarketType = false;
            }
            // if (this.selectedItemsTerritories.length == 0) {
            //     this.showValidTerritory = true;
            // }else{
            //     this.showValidTerritory = false;
            // }
            // if(this.selectedItemsGeo.length == 0){
            //     this.showValidGeo = true;
            // }else{
            //     this.showValidGeo = false;
            // }
            if (this.selectedItemsCountry.length == 0) {
                this.showValidCountry = true;
            } else {
                this.showValidCountry = false;
            }
            if (this.selectedItemsPartnerTypeStatus.length == 0) {
                this.showValidPartnerStatus = true
            } else {
                this.showValidPartnerStatus = false
            }

        }
        else {
            this.loading = true;
            this.showValidPartnerType = false;
            this.showValidMarketType = false;
            // this.showValidTerritory = false;
            // this.showValidGeo = false;
            this.showValidCountry = false;
            this.showValidPartnerStatus = false

            // 'api/MainInvoices/reportInvoices/{"fieldOrg":"' + orgfieldarr.toString() + '","anotherField":"' + anotherfieldarr.toString() + '","invoicesField":"' + invoicefieldarr.toString() + '","filterByInvoicesNumber":"' + this.searchInvoice.value.invoiceNumber + '",' +
            //     '"filterByPOReceivedDate":"' + startdate + ',' + enddate + '","filterByPartnerType":"' + partnertypearr.toString() + '","filterByPartnerTypeStatus":"' + partnertypestatusarr.toString() + '","filterByMarketType":"' + markettypearr.toString() + '","filterByGeo":"' + geoarr.toString() + '",' +
            //     '"filterByRegion":"' + regionarr.toString() + '","filterBySubRegion":"' + subregionarr.toString() + '","filterByTerritory":"' + territoryarr.toString() + '","filterByCountries":"' + countriesarr.toString() + '","FinancialYear":"' + this.searchInvoice.value.FinancialYear + '"}'           

            /*fix issue excel no 263.Show data based role*/

            var orgid = "";
            var role = "";
            
            // if (this.checkrole()) {
            //     orgid = this.urlGetOrgId();
            //     if (this.itsDistributor()) {
            //         role = "ORGANIZATION";
            //     } else {
            //         role = "DISTRIBUTOR";
            //     }
            // }

            /*end fix issue excel no 263.Show data based role*/

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            if (this.adminya) {
                orgid = this.urlGetOrgId();
                if (this.distributorya) {
                    role = "DISTRIBUTOR";
                } else {
                    role = "ORGANIZATION";
                }
            }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            var keyword = {
                fieldOrg: orgfieldarr.toString(),
                anotherField: anotherfieldarr.toString(),
                invoicesField: invoicefieldarr.toString(),
                filterByInvoicesNumber: this.searchInvoice.value.invoiceNumber,
                filterByPOReceivedDate: startdate + "," + enddate,
                filterByPartnerType: partnertypearr.toString(),
                filterByPartnerTypeStatus: partnertypestatusarr.toString(),
                filterByMarketType: markettypearr.toString(),
                filterByGeo: geoarr.toString(),
                filterByRegion: regionarr.toString(),
                filterBySubRegion: subregionarr.toString(),
                filterByTerritory: territoryarr.toString(),
                filterByCountries: countriesarr.toString(),
                FinancialYear: this.searchInvoice.value.FinancialYear,
                OrgId: orgid, //fix issue excel no 263.Show data based role
                Role: role //fix issue excel no 263.Show data based role
            };

            this.reportInvoice = null;
            this.listkey = null;
            /* new post for autodesk plan 18 oct */
            this.service.httpClientPost("api/MainInvoices/reportInvoices", keyword)
                .subscribe(res => {
                    invoices = res;
                    this.listkey = [];
                    if (invoices.length > 0) {
                        for (let key in invoices[0]) {
                            this.listkey.push(key);
                        }
                        this.reportInvoice = invoices;
                        this.dataFound = true;
                        this.loading = false;
                    } else {
                        this.reportInvoice = "";
                        this.dataFound = true;
                        this.loading = false;
                    }
                }, error => {
                    this.data = true;
                    this.loading = false;
                });
            /* new post for autodesk plan 18 oct */
        }

    }

    preselected(value) {
        if (value == "1") {
            this.selectedItemsFieldOrganization =
                [
                    {
                        id: "o.OrgName",
                        itemName: "OrgName"
                    }
                ];
            this.selectedItemsFieldInvoice =
                [
                    {
                        id: "i.InvoiceId",
                        itemName: "Invoice Id"
                    },
                    {
                        id: "i.TotalPaymentAmount",
                        itemName: "Total Payment Amount"
                    },
                ];
        }
        else {
            this.selectedItemsFieldOrganization =
                [
                    {
                        id: "o.OrgName",
                        itemName: "OrgName"
                    },
                    {
                        id: "o.TaxExempt",
                        itemName: "TaxExempt"
                    },
                    {
                        id: "o.VATNumber",
                        itemName: "VATNumber"
                    },
                    {
                        id: "o.InvoicingDepartment",
                        itemName: "InvoicingDepartment"
                    },
                    {
                        id: "o.InvoicingAddress1",
                        itemName: "InvoicingAddress1"
                    },
                    {
                        id: "o.InvoicingAddress3",
                        itemName: "InvoicingAddress3"
                    },
                    {
                        id: "o.InvoicingCity",
                        itemName: "InvoicingCity"
                    },
                    {
                        id: "o.InvoicingStateProvince",
                        itemName: "InvoicingStateProvince"
                    },
                    {
                        id: "o.InvoicingPostalCode",
                        itemName: "InvoicingPostalCode"
                    },
                    {
                        id: "o.InvoicingCountryCode",
                        itemName: "InvoicingCountryCode"
                    },
                    {
                        id: "o.BillingContactFirstName",
                        itemName: "BillingContactFirstName"
                    },
                    {
                        id: "o.BillingContactLastName",
                        itemName: "BillingContactLastName"
                    },
                    {
                        id: "o.BillingContactEmailAddress",
                        itemName: "BillingContactEmailAddress"
                    },
                    {
                        id: "o.BillingContactTelephone",
                        itemName: "BillingContactTelephone"
                    }
                ];
            this.selectedItemsFieldInvoice =
                [
                    {
                        id: "i.InvoiceId",
                        itemName: "Invoice Id"
                    },
                    {
                        id: "i.InvoiceNumber",
                        itemName: "Invoice Number"
                    },
                    {
                        id: "i.InvoiceDescription",
                        itemName: "Invoice Description"
                    },
                    {
                        id: "i.TotalInvoicedAmount",
                        itemName: "Total Invoiced Amount"
                    },
                    {
                        id: "i.InvoicedCurrency",
                        itemName: "Invoiced Currency"
                    },
                    {
                        id: "i.TotalPaymentAmount",
                        itemName: "Total Payment Amount"
                    }
                ];
        }
    }

    ExportExcel() {
        // var date = this.service.formatDate();
        var fileName = "InvoiceRpt.xls";
        var ws = XLSX.utils.json_to_sheet(this.reportInvoice);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileName);
    }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    getPartnerType() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Roles/where/{'RoleType': 'Company'}", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListPartner = null;
                }
                else {
                    this.dropdownListPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
            
                    var dataParse : any = result
                    for (let i = 0; i < dataParse.length; i++) {
                        if (dataParse[i].RoleName == "Authorized Academic Partner (AAP)") {
                            this.selectedItemsPartner.push({ 'id': dataParse[i].RoleId, 'itemName': dataParse[i].RoleName });
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsPartner = [];
        this.getPartnerTypeStatus();
    }

    getPartnerTypeStatus() {
        var data: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'SiteStatus','Status':'A'}", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.selectedItemsPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsPartnerTypeStatus = [];
        this.getMarketType();
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                            this.selectedItemsMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsMarketType = [];
    }

    territorydefault: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        /* populate data issue role distributor */

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'Distributor'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteId'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteId";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            Distributor: DistributorGeo,
            SiteId: siteRes,
            OrgId: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.adminya) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        // get Geo
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Geo";
        // } else {
        //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        // }
        var url = "api/Geo/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                   
                    this.dropdownListGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })

                    this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    getOrgFields() {
        var data: any;
        this.service.httpClientGet("api/MainOrganization/getColumns", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListFieldOrganization = data.map((item) => {
                        return {
                            id: "o." + item.Field,
                            itemName: item.Field
                        }
                    });
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].Field == "OrgId" || data[i].Field == "OrgName" || data[i].Field == "RegisteredCountryCode" || data[i].Field == "ATCDirectorEmailAddress") {
                            this.selectedItemsFieldOrganization.push({ 'id': "o." + data[i].Field, 'itemName': data[i].Field });
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsFieldOrganization = [];
    }

    getInvoicesFields() {
        var field = [
            { "id": "i.InvoiceId", "itemName": "Invoice Id" },
            { "id": "i.InvoiceNumber", "itemName": "Invoice Number" },
            { "id": "i.InvoiceDescription", "itemName": "Invoice Description" },
            { "id": "i.POReceivedDate", "itemName": "PO Received Date" },
            { "id": "i.TotalInvoicedAmount", "itemName": "Total Invoiced Amount" },
            { "id": "d.KeyValue AS InvoicedCurrency", "itemName": "Invoiced Currency" },
            { "id": "i.TotalPaymentAmount", "itemName": "Total Payment Amount" },
            { "id": "i.DateAdded", "itemName": "Date Added" },
            { "id": "i.AddedBy", "itemName": "Added By" },
            { "id": "i.DateLastAdmin", "itemName": "Date Last Admin" },
            { "id": "i.LastAdminBy", "itemName": "Last Admin By" },
            { "id": "i.NumberOfLicenses", "itemName": "Number Of Licenses" },
            { "id": "i.FinancialYear", "itemName": "Financial Year" },
            { "id": "sku.SKUDescription", "itemName": "SKU Description" },
            { "id": "(select GROUP_CONCAT(SiteId) from Main_organization o JOIN Invoices i ON o.OrgId = i.OrgId JOIN InvoicesSites isite ON i.InvoiceId = isite.InvoiceId where isite.`Status` = 'A') as SiteIds", "itemName": "Site Id" }
        ];

        for (let n = 0; n < field.length; n++) {
            this.dropdownListFieldInvoice.push(field[n]);
        }

        this.selectedItemsFieldInvoice = [];
    }

    getAnotherFields() {
        var field = [
            { "id": "m.MarketType", "itemName": "Market Type" },
            { "id": "r.RoleName AS PartnerType", "itemName": "Partner Type" },
            { "id": "sr.Status", "itemName": "Partner Type Status" },
            { "id": "sr.CSN", "itemName": "Partner Type CSN" },
            { "id": "sr.ITS_ID", "itemName": "ITS Site ID" },
            { "id": "sr.UParent_CSN", "itemName": "Ultimate Parent CSN" },
            { "id": "rg.geo_name", "itemName": "Geo" },
            { "id": "rr.region_name", "itemName": "Region" },
            { "id": "rsr.subregion_name", "itemName": "SubRegion" },
            { "id": "t.Territory_Name", "itemName": "Territory" },
            { "id": "ja.ActivityName", "itemName": "Organization Journal Entries" },
            { "id": "j.ActivityDate", "itemName": "Organization Journal Entries Date" },
            { "id": "sku.Currency", "itemName": "Currency" }
        ];
        for (let n = 0; n < field.length; n++) {
            this.dropdownListFieldShow2.push(field[n]);
        }
        this.selectedItemsFieldShow2 = [];
    }

    allregion = [];
    allsubregion = [];
    allcountries = [];
    ngOnInit() {

        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        this.loading = true;
        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.searchInvoice.patchValue({ invoiceNumber: item.invoiceNumber });
            this.searchInvoice.patchValue({ FinancialYear: item.Year });
            this.selectedItemsPartner = item.selectPartner;
            this.dropdownListPartner = item.dropdownPartner;
            this.selectedItemsPartnerTypeStatus = item.selectPartnerStatus;
            this.dropdownListPartnerTypeStatus = item.dropdownPartnerStatus;
            this.selectedItemsMarketType = item.selectMarketType;
            this.dropdownListMarketType = item.dropdownMarketType;
            this.selectedItemsTerritories = item.selectTerritory;
            this.dropdownListTerritories = item.dropdownTerritory;
            this.selectedItemsGeo = item.selectGeo;
            this.dropdownListGeo = item.dropdownGeo;
            this.selectedItemsCountry = item.selectCountry;
            this.dropdownListCountry = item.dropdownCountry;
            this.selectedItemsFieldOrganization = item.selectFieldOrg;
            this.dropdownListFieldOrganization = item.dropdownFieldOrg;
            this.selectedItemsFieldShow2 = item.selectFieldShow;
            this.dropdownListFieldShow2 = item.dropdownFieldShow;
            this.selectedItemsFieldInvoice = item.selectFieldInvoice;
            this.dropdownListFieldInvoice = item.dropdownFieldInvoice;
            this.onSubmit();
        }
        else {

            this.getPartnerType();
            this.getTerritory();
            this.getGeo();
            this.getOrgFields();
            this.getAnotherFields();
            this.getInvoicesFields();

            // Region
            /*var data = '';
            this.service.get("api/Region", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListRegion = null;
                    }
                    else {
                        this.dropdownListRegion = JSON.parse(result).map((item) => {
                            return {
                                id: item.region_code,
                                itemName: item.region_name
                            }
                        })
                    }
                    this.allregion = this.dropdownListRegion;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsRegion = [];

            // Sub Region
            data = '';
            this.service.get("api/SubRegion", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = JSON.parse(result).map((item) => {
                            return {
                                id: item.subregion_code,
                                itemName: item.subregion_name
                            }
                        })
                    }
                    this.allsubregion = this.dropdownListSubRegion;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsSubRegion = [];*/

            // Countries
            /* populate data issue role distributor */
            var url2 = "api/Countries/SelectAdmin";

            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.orgya) {
            //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             if (this.siteya) {
            //                 url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //             } else {
            //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //             }
            //         }
            //     }
            // }
            var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistributorCountry = this.urlGetOrgId();
                     url2 = "api/Territory/wherenew/'DistributorCountry'";
                } else {
                    if (this.orgya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                    } else {
                        if (this.siteya) {
                            OnlyCountryOrgId = this.urlGetOrgId();
                            url2 = "api/Territory/wherenew/'OnlyCountryOrgId";
                        } else {
                            CountryOrgId= this.urlGetOrgId();
                            url2 = "api/Territory/wherenew/'CountryOrgId'";
                        }
                    }
                }
                
            }
            var keyword : any = {
                DistributorCountry: DistributorCountry,
                OnlyCountryOrgId: OnlyCountryOrgId,
                CountryOrgId: CountryOrgId,
               
            };
            keyword = JSON.stringify(keyword);
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            var data = '';
            this.service.httpClientPost(url2, keyword)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                    this.allcountries = this.dropdownListCountry;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsCountry = [];

        }

        data = '';
        this.service.httpClientGet('api/Currency/where/{"Parent":"FYIndicator","Status":"A"}', data)
            .subscribe(result => {
                this.datayearcurrency = result;
                this.searchInvoice.patchValue({ FinancialYear: this.datayearcurrency[this.datayearcurrency.length - 1].Key });
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    arrayterritory = [];

    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';

        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';

            /* populate data issue role distributor */
           // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
            var url = "api/Countries/filterByGeoByTerritory";
            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //     } else {
            //         if (this.orgya) {
            //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //         } else {
            //             if (this.siteya) {
            //             url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //         } else {
            //             url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //         }
            //     }
            // }
            var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistTerr = this.urlGetOrgId();
                   url = "api/Countries/DistTerr";
                } else {
                    if (this.orgya) {
                        OrgSiteTerr = this.urlGetOrgId();
                        url = "api/Countries/OrgSiteTerr";
                    } else {
                        if (this.siteya) {
                            OnlyCountryOrgId = this.urlGetOrgId();
                            url = "api/Territory/wherenew/'OnlyCountryOrgId";
                        } else {
                            CountryOrgId= this.urlGetOrgId();
                            url = "api/Territory/wherenew/'CountryOrgId'";
                        }
                    }
                }
                
            }
            var keyword : any = {
                CtmpGeo:tmpGeo,
                CDistTerr:DistTerr,
                COrgSiteTerr:OrgSiteTerr,
                OnlyCountryOrgId: OnlyCountryOrgId,
                CountryOrgId: CountryOrgId,
                CtmpTerritory:tmpTerritory
               
            };
            keyword = JSON.stringify(keyword);
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            this.service.httpClientGet(url, data) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        // Countries
        var data = '';

        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

       // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
       this.service.httpClientPost(url, keyword) 
       .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {

        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp = result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = this.allsubregion;
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        this.showValidCountry = false;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }
    OnCountriesDeSelect(item: any) { }
    onCountriesSelectAll(items: any) {
        this.showValidCountry = false;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }
    onCountriesDeSelectAll(items: any) { }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }
}