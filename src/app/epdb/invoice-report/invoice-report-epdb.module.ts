import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceReportEPDBComponent,ReplacePipe } from './invoice-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
// import { DataFilterReport } from './organization-report-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const InvoiceReportEPDBRoutes: Routes = [
    {
        path: '',
        component: InvoiceReportEPDBComponent,
        data: {
            breadcrumb: 'menu_report_epdb.invoice_report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(InvoiceReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule,
      LoadingModule
    ],
    declarations: [InvoiceReportEPDBComponent,ReplacePipe],
    providers: [AppService, AppFilterGeo,AppFormatDate]
  })
  export class InvoiceReportEPDBModule { }