import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";

@Component({
  selector: 'app-report-epdb',
  templateUrl: './report-epdb.component.html',
  styleUrls: [
    './report-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ReportEPDBComponent implements OnInit {

  dropdownListGeo = [];
  selectedItemsGeo = [];
  dropdownListRegion = [];
  selectedItemsRegion = [];
  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];
  dropdownListCountry = [];
  selectedItemsCountry = [];
  dropdownListSubCountry = [];
  selectedItemsSubCountry = [];

  dropdownListTerritories = [];
  selectedItemsTerritories = [];
  
  dropdownSettings  = {};

  private _serviceUrl        = 'api/MainSite';
  messageResult     : string = '';
  messageError      : string = '';
  adddistributor    : FormGroup;
  public distributor: any;
  public data;
  public data_1;
  public data_2;
  public rowsOnPage : number = 10;
  public filterQuery: string = "";
  public sortBy     : string = "type";
  public sortOrder  : string = "asc";
  public foundCountry        = false;
  public foundSites          = false;
  public geo        : string = "";
  public region     : string = "";
  public subRegion  : string = "";
  public countryName: string = "";
  public countryCode: string = "";
  public siteID     : any;
  public sites      : any;
  public siteNames  : any;
  public masterDistributor   = false;
  public regionalDistributor = false;
  public contacts            = false;
  public dataSite   : any;

  constructor(private router: Router, private service:AppService, private filterGeo:AppFilterGeo) { 
    
    let SiteID           = new FormControl('');
    let CountryCode      = new FormControl('');


  }

  ngOnInit() {
    
    // Geo
    var data = '';
    this.service.httpClientGet("api/Geo",data)
    .subscribe(result => { 
      this.data = result;
      this.dropdownListGeo = this.data.map((item) => {
        return {
          id: item.geo_code,
          itemName: item.geo_name
        }
      }) 
    },
    error => {
        this.service.errorserver();
    });
    this.selectedItemsGeo = [];

    // Territory
    var data = '';
    this.service.httpClientGet("api/Geo", data)
    .subscribe(result => {
      this.dropdownListTerritories = this.data.map((item) => {
        return {
          id: item.geo_code,
          itemName: item.geo_name
        }
      })
    },
    error => {
      this.service.errorserver();
    });
    this.selectedItemsTerritories = [];

    //setting dropdown
    this.dropdownSettings = { 
      singleSelection   : false, 
      text              : "Please Select",
      selectAllText     : 'Select All',
      unSelectAllText   : 'UnSelect All',
      enableSearchFilter: true,
      classes           : "myclass custom-class",
      disabled          : false
    };
    // this.getData1();
    // this.getData2();
  }

  onGeoSelect(item:any){
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion=[];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }
  OnGeoDeSelect(item:any){
    this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.selectedItemsSubCountry = [];
    this.dropdownListSubRegion=[];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }
  onGeoSelectAll(items: any){
    this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo,this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion=[];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }
  onGeoDeSelectAll(items: any){
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.selectedItemsSubCountry = [];
    this.dropdownListRegion=[];
    this.dropdownListSubRegion=[];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }

  onRegionSelect(item:any){
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }
  OnRegionDeSelect(item:any){
    this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.selectedItemsSubCountry = [];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }
  onRegionSelectAll(items: any){
    this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }
  onRegionDeSelectAll(items: any){
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.selectedItemsSubCountry = [];
    this.dropdownListSubRegion=[];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }

  onSubRegionSelect(item:any){
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.dropdownListSubCountry=[];
  }
  OnSubRegionDeSelect(item:any){
    this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.selectedItemsSubCountry = [];
    this.dropdownListSubCountry=[];
  }
  onSubRegionSelectAll(items: any){
    this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.dropdownListSubCountry=[];
  }
  onSubRegionDeSelectAll(items: any){
    this.selectedItemsCountry = [];
    this.selectedItemsSubCountry = [];
    this.dropdownListCountry=[];
    this.dropdownListSubCountry=[];
  }

  onCountriesSelect(item:any){
    this.filterGeo.filterCountriesOnSelect(item.id, this.dropdownListSubCountry)
    this.selectedItemsSubCountry = [];
  }
  OnCountriesDeSelect(item:any){
    this.filterGeo.filterCountriesOnDeSelect(item.id, this.dropdownListSubCountry)
    this.selectedItemsSubCountry = [];
  }
  onCountriesSelectAll(items: any){
    this.filterGeo.filterCountriesOnSelectAll(this.selectedItemsCountry, this.dropdownListSubCountry)
    this.selectedItemsSubCountry = [];
  }
  onCountriesDeSelectAll(items: any){
    this.selectedItemsSubCountry = [];
    this.dropdownListSubCountry=[];
  }

  onSubCountriesSelect(item:any){}
  OnSubCountriesDeSelect(item:any){}
  onSubCountriesSelectAll(item:any){}
  onSubCountriesDeSelectAll(item:any){}

  onTerritoriesSelect(item:any){}
  OnTerritoriesDeSelect(item:any){}
  onTerritoriesSelectAll(item:any){}
  onTerritoriesDeSelectAll(item:any){}

  findSites(){
    let arr    : any;
    let i      : any;
    var siteID = '';
    var sites  = '';

    arr = [
      this.selectedItemsGeo[0]["itemName"],
      this.selectedItemsRegion[0]["itemName"],
      this.selectedItemsSubRegion[0]["itemName"],
      this.selectedItemsCountry[0]["itemName"],
      this.selectedItemsCountry[0]["id"],
      this.selectedItemsTerritories[0]["itemNameT"]
    ];
    this.geo         = arr[0];
    this.region      = arr[1];
    this.subRegion   = arr[2];
    this.countryName = arr[3];
    this.countryCode = arr[4];
    this.foundCountry= true;
    this.foundSites  = true;

    this.service.httpClientGet("api/SiteCountryDistributor/where/{'CountryCode':'"+this.countryCode+"'}",siteID)
    .subscribe(result => {
        this.siteID = result;
        this.siteID.forEach(item =>{
          this.getSite(item.SiteID);
        });
        this.masterDistributor   = true;
        this.regionalDistributor = true;
        this.contacts            = true;
    },
    error => {
        this.service.errorserver();
    });
  }

  getSite(id){
    var sites = '';
    let dataSite = [];
    this.service.httpClientGet("api/MainSite/where/{'SiteId':'"+id+"'}",sites)
    .subscribe(result => {
        this.sites = result;
        if(this.sites.length != 0){
          dataSite.push(this.sites);
        }
    },
    error => {
        this.service.errorserver();
    });
  }

  getCountry(){
    this.countryCode = this.selectedItemsCountry[0]["id"];
    this.countryName = this.selectedItemsCountry[0]["itemName"];

    var siteNames = '';
    this.service.httpClientGet("api/MainSite",siteNames)
    .subscribe(result => {
        this.siteNames = result;
    },
    error => {
        this.service.errorserver();
    });
    this.siteID = "";
  }
  
  selectedSite(newvalue){
    this.siteID = newvalue;
  }

  getSitebyCountry(){
    this.countryCode = this.selectedItemsCountry[0]["id"];
    var siteID = '';
    this.service.httpClientGet("api/SiteCountryDistributor/where/{'CountryCode':'"+this.countryCode+"'}",siteID)
    .subscribe(result => {
        this.siteID = result;
        this.siteID.forEach(item =>{
          this.getSite(item.SiteID);
        });
    },
    error => {
        this.service.errorserver();
    });
  }
}
