import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportEPDBComponent } from './report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {AppTranslateLanguage} from '../../shared/translate-language/app.translateLanguage';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
 
export const ReportEPDBRoutes: Routes = [
  {
    path: '',
    component: ReportEPDBComponent,
    data: {
      breadcrumb: 'Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ReportEPDBComponent],
  providers:[AppService, AppFilterGeo]
})
export class ReportEPDBModule { }
