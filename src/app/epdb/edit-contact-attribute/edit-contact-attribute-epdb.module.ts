import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditContactAttributeEPDBComponent } from './edit-contact-attribute-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const EditContactAttributeEPDBRoutes: Routes = [
    {
        path: '',
        component: EditContactAttributeEPDBComponent,
        data: {
            breadcrumb: 'Edit Contact Attribute',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(EditContactAttributeEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [EditContactAttributeEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class EditContactAttributeEPDBModule { }
