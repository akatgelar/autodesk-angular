import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-login-history-report-epdb',
  templateUrl: './login-history-report-epdb.component.html',
  styleUrls: [
    './login-history-report-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class LoginHistoryReportEPDBComponent implements OnInit {

  dropdownListGeo = [];
  selectedItemsGeo = [];
  dropdownSettingsGeo = {};

  dropdownListRegion = [];
  selectedItemsRegion = [];
  dropdownSettingsRegion = {};

  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];
  dropdownSettingsSubRegion = {};

  dropdownListCountry = [];
  selectedItemsCountry = [];
  dropdownSettingsCountry = {};

  dropdownListSubCountry = [];
  selectedItemsSubCountry = [];
  dropdownSettingsSubCountry = {};
  
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
   
 
  constructor(public http: Http) { }

  ngOnInit() {
    this.http.get(`assets/data/activities.json`)
      .subscribe((data)=> {
        this.data = data.json();
      });

    // Geo
    this.dropdownListGeo = [
      {"id":1,"itemName":"AMER"},
      {"id":2,"itemName":"APAC (excl. GCR)"},
      {"id":3,"itemName":"EMEA"},
      {"id":4,"itemName":"GCR"}
    ];
    this.selectedItemsGeo = [];
    this.dropdownSettingsGeo = { 
        singleSelection: false, 
        text:"Please Select",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // Region
    this.dropdownListRegion = [
        {"id":1,"itemName":"ANZ"},
        {"id":2,"itemName":"ASEAN"},
        {"id":3,"itemName":"Canada"},
        {"id":4,"itemName":"Central Europe"}
    ];
    this.selectedItemsRegion = [];
    this.dropdownSettingsRegion = { 
        singleSelection: false, 
        text:"Please Select",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // SubRegion
    this.dropdownListSubRegion = [
        {"id":1,"itemName":"Africa"},
        {"id":2,"itemName":"ASEAN"},
        {"id":3,"itemName":"Australia / New Zealand"},
        {"id":4,"itemName":"Central Europe"}
    ];
    this.selectedItemsSubRegion = [];
    this.dropdownSettingsSubRegion = { 
        singleSelection: false, 
        text:"Please Select",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // Country
    this.dropdownListCountry = [
      {"id":1,"itemName":"Afganistan"},
      {"id":2,"itemName":"Albania"},
      {"id":3,"itemName":"Algeria"},
      {"id":4,"itemName":"Andorra"}
    ];
    this.selectedItemsCountry = [];
    this.dropdownSettingsCountry = { 
        singleSelection: false, 
        text:"Please Select",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };

    // SubCountry
    this.dropdownListSubCountry = [
        {"id":1,"itemName":"Ajman"},
        {"id":2,"itemName":"Abia State"},
        {"id":3,"itemName":"Abkhazia"},
        {"id":4,"itemName":"Absheron Rayon"}
    ];
    this.selectedItemsSubCountry = [];
    this.dropdownSettingsSubCountry = { 
        singleSelection: false, 
        text:"Please Select",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        classes:"myclass custom-class"
    };
  }

  onItemSelect(item:any){
    console.log(item);
    console.log(this.selectedItemsGeo);
    console.log(this.selectedItemsRegion);
    console.log(this.selectedItemsSubRegion);
    console.log(this.selectedItemsCountry);
    console.log(this.selectedItemsSubCountry);
  }
  OnItemDeSelect(item:any){
      console.log(item);
      console.log(this.selectedItemsGeo);
      console.log(this.selectedItemsRegion);
      console.log(this.selectedItemsSubRegion);
      console.log(this.selectedItemsCountry);
      console.log(this.selectedItemsSubCountry);
  }
  onSelectAll(items: any){
      console.log(items);
  }
  onDeSelectAll(items: any){
      console.log(items);
  }
}