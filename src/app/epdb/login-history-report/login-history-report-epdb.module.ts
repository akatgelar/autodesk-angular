import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginHistoryReportEPDBComponent } from './login-history-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const LoginHistoryReportEPDBRoutes: Routes = [
  {
    path: '',
    component: LoginHistoryReportEPDBComponent,
    data: {
      breadcrumb: 'Login History Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginHistoryReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [LoginHistoryReportEPDBComponent]
})
export class LoginHistoryReportEPDBModule { }
