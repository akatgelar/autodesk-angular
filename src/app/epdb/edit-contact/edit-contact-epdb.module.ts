import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditContactEPDBComponent } from './edit-contact-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { Ng2CompleterModule } from "ng2-completer";

export const EditContactEPDBRoutes: Routes = [
  {
    path: '',
    component: EditContactEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.add_edit.edit_contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditContactEPDBRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule,
    Ng2CompleterModule
  ],
  declarations: [EditContactEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class EditContactEPDBModule { }