import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";

@Component({
  selector: 'app-edit-contact-epdb',
  templateUrl: './edit-contact-epdb.component.html',
  styleUrls: [
    './edit-contact-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css'
  ],
  encapsulation: ViewEncapsulation.None
})

export class EditContactEPDBComponent implements OnInit {

  private _serviceUrl = 'api/MainContact';
  messageError: string = '';
  editcontact: FormGroup;
  public datadetail: any;
  honorific;
  language;
  primaryindustry;
  roleAccess;
  private id;
  public isSelectAll=false;
  public selCounter=0;
  private siteid;
  public timezones: any;
  public useraccesdata: any;
  public loading = false;
  dropdownListRoleAcces = [];
  selectedItemsRoleAcces = [];
  dropdownSettingsRoleAcces = {};
  dropdownSettingsRoleAccesone = {};
  dropdownSettingsRoleAccestwo = {};
  // dropdownListOrganization = [];
  // selectedItemsOrganization = [];
  dropdownSettings = {};
  protected dataService: RemoteData;
  Organization: string = ""
  allChecked: boolean = true;

  constructor(private completerService: CompleterService, private router: Router, private service: AppService, private session: SessionService, private formatdate: AppFormatDate, private route: ActivatedRoute) {//, private htmlEntityService:htmlentityService

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let ContactName = new FormControl('');
    /* fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
    let FirstName = new FormControl('', Validators.required);
    let LastName = new FormControl('', Validators.required);
    let EmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let EmailAddress2 = new FormControl('', [CustomValidators.notEqualTo(EmailAddress)]);
    let DoNotEmail = new FormControl('');
    let UserLevelId = new FormControl('');
    let PrimaryRoleId = new FormControl('');
    let PrimaryIndustryId = new FormControl('');
    let Telephone1 = new FormControl('');
    let Telephone2 = new FormControl('');
    let Mobile1 = new FormControl('');
    let Mobile2 = new FormControl('');
    let PrimaryLanguage = new FormControl('');
    let SecondaryLanguage = new FormControl('');
    let Timezone = new FormControl('', Validators.required);
    /* end line fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
    let Comments = new FormControl('');
    let InstructorId = new FormControl('');
    let Honorific = new FormControl('');
    let ATCRole = new FormControl('');
    let Designation = new FormControl('');

    this.editcontact = new FormGroup({
      ContactName: ContactName,
      FirstName: FirstName,
      LastName: LastName,
      EmailAddress: EmailAddress,
      EmailAddress2: EmailAddress2,
      DoNotEmail: DoNotEmail,
      UserLevelId: UserLevelId,
      PrimaryRoleId: PrimaryRoleId,
      PrimaryIndustryId: PrimaryIndustryId,
      Telephone1: Telephone1,
      Telephone2: Telephone2,
      Mobile1: Mobile1,
      Mobile2: Mobile2,
      PrimaryLanguage: PrimaryLanguage,
      SecondaryLanguage: SecondaryLanguage,
      Timezone: Timezone,
      Honorific: Honorific,
      Comments: Comments,
      ATCRole: ATCRole,
      Designation: Designation
    });
  }


  //Jadi kalau user milih instruktur, otomatis checkbox "is an instructor" harus ke centang
  onItemSelectRoleAcces(item: any) { 
    if(item.id == "TRAINER" || item.id == "Instructor Read Only"){
		if(this.isSelectAll){
				this.selCounter=this.selCounter-1;
      }
      this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
      this.editcontact.patchValue({ ATCRole: true });
    }
    else {
      this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
    }
  }

  OnItemDeSelectRoleAcces(item: any) { 
   if(this.isSelectAll){
		if(item.id == "TRAINER"){
          this.selCounter = this.selCounter + 1;
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
			if(this.selCounter == 2){
				this.editcontact.patchValue({ ATCRole: false });
			}
		}
		if(item.id == "Instructor Read Only"){
			this.selCounter=this.selCounter+1;
			if(this.selCounter == 2){
				this.editcontact.patchValue({ ATCRole: false });
			}
		}		
   } else {
     if (this.selectedItemsRoleAcces[0].id == "TRAINER") {
       this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
     }
     else {
       this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
     }
		this.editcontact.patchValue({ ATCRole: false });
	}
  }

  onSelectAllRoleAcces(items: any) { 
  this.isSelectAll=true;
    for(let i = 0; i < items.length; i++){
      if(items[i].id == "TRAINER" || items[i].id == "Instructor Read Only"){
        this.editcontact.patchValue({ ATCRole: true });
      }
    }
  }

  onDeSelectAllRoleAcces(items: any) { 
    this.editcontact.patchValue({ ATCRole: false });
  }

  getRoleAccess(url, userlvl) {
    var data = '';
    this.service.httpClientGet(url, data)
      .subscribe((result:any) => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListRoleAcces = null;
        }
        else {
          this.dropdownListRoleAcces = result.sort((a,b)=>{
            if(a.Name > b.Name)
              return 1
            else if(a.Name < b.Name)
              return -1
            else return 0
          }).map(function (el) {
            return {
              id: el.UserLevelId,
              itemName: el.Name
            }
          })
        }
      },
        error => {
          this.service.errorserver();
        });
  if (userlvl != null && userlvl != "") {
      let lvlarr = userlvl.split(',');
      for (var i = 0; i < lvlarr.length; i++) {
        var dataUserLvl: any;
        this.service.httpClientGet('api/UserLevel/' + lvlarr[i], data)
          .subscribe(result => {
            dataUserLvl = result;
            if (result == "Not found") {
              this.service.notfound();
              this.selectedItemsRoleAcces = [];
            }
            else {
              if (dataUserLvl.UserLevelId == "TRAINER") {
                this.editcontact.patchValue({ ATCRole: true });
                this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
              }
              this.selectedItemsRoleAcces.push(
                  {
                    'id': dataUserLvl.UserLevelId,
                    'itemName': dataUserLvl.Name
                  }
                );       
            }
           
            
          },
            error => {
              this.service.errorserver();
          });
        if (this.selectedItemsRoleAcces.length < 2) {
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
        }
      }
      //this.selectedItemsRoleAcces=[];
    }
    else {
      this.selectedItemsRoleAcces = [];
    }

  }

  public jobrole: any;
  getJobRole() {

    this.service.httpClientGet('api/Roles/GetJobRole', '')
      .subscribe((result:any) => {
        if (result == "Not found") {
          this.service.notfound();
          this.jobrole = '';
        }
        else {
          this.jobrole = result.sort((a,b)=>{
            if(a.RoleName > b.RoleName)
              return 1
            else if(a.RoleName < b.RoleName)
              return -1
            else return 0
          });
        }
      },
        error => {
          this.service.errorserver();
          this.jobrole = '';
        });

  }

  getPrimaryIndustry() {
    var primaryindustry = '';
    this.service.httpClientGet("api/Industries/", primaryindustry)
      .subscribe(result => {
        this.primaryindustry = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const valueA = a.KeyValue.toUpperCase();
    const valueB = b.KeyValue.toUpperCase();

    let comparison = 0;
    if (valueA > valueB) {
      comparison = 1;
    } else if (valueA < valueB) {
      comparison = -1;
    }
    return comparison;
  }

  getLanguages() {
    var language = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", language)
      .subscribe(result => {
        var tmpData : any = result;
        this.language = tmpData.sort(this.compare);
      },
        error => {
          this.service.errorserver();
        });
  }

  getHonorfic() {
    var honorific = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Salutation','Status':'A'}", honorific)
      .subscribe(result => {
        this.honorific = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  /* populate data issue role distributor */

  checkrole(): boolean {
  let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
            return false;
        } else {
            return true;
        }
    }
  }

  itsOrganization(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ORGANIZATION") {
            return true;
        } else {
            return false;
        }
    }
  }

  itsDistributor(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "DISTRIBUTOR") {
            return true;
        } else {
            return false;
        }
    }
  }

  /* populate data issue role distributor */

  getDetailContact(id, siteId) {
    var data: any;
    this.service.httpClientGet(this._serviceUrl + '/DetailContact/' + id + '/' + siteId, data)
      .subscribe(result => {
        if (result == "Not Found") {
          this.datadetail = '';
        } else {
          this.datadetail = result;
          if (this.datadetail != null) {
            if (this.datadetail.Comments != null && this.datadetail.Comments != "") {
              this.datadetail.Comments = this.service.decoder(this.datadetail.Comments);
            }
            if (this.datadetail.ContactName != null && this.datadetail.ContactName != "") {
              this.datadetail.ContactName = this.service.decoder(this.datadetail.ContactName);
            }
            if (this.datadetail.FirstName != null && this.datadetail.FirstName != "") {
              this.datadetail.FirstName = this.service.decoder(this.datadetail.FirstName);
            }
            if (this.datadetail.LastName != null && this.datadetail.LastName != "") {
              this.datadetail.LastName = this.service.decoder(this.datadetail.LastName);
            }
            if (this.datadetail.Department != null && this.datadetail.Department != "") {
              this.datadetail.Department = this.service.decoder(this.datadetail.Department);
            }
            if (this.datadetail.EmailAddress != null && this.datadetail.EmailAddress != "") {
              this.datadetail.EmailAddress = this.service.decoder(this.datadetail.EmailAddress);
            }
            if (this.datadetail.RoleName != null && this.datadetail.RoleName != "") {
              this.datadetail.RoleName = this.service.decoder(this.datadetail.RoleName);
            }
            if (this.datadetail.Designation != null && this.datadetail.Designation != "") {
              this.datadetail.Designation = this.service.decoder(this.datadetail.Designation);
            }
            
          }
          let userlvlarr = this.useraccesdata.UserLevelId.split(',');
          let url: any;

          //get user level
          
          // for (var i = 0; i < userlvlarr.length; i++) {
          //   if (userlvlarr[i] == "DISTRIBUTOR") {
          //     url = 'api/UserLevel/WithoutStudent/Distributor';
          //   }
          //   else if (userlvlarr[i] == "ORGANIZATION") {
          //     url = 'api/UserLevel/WithoutStudent/Org';
          //   }
          //   else if (userlvlarr[i] == "SITE") {
          //     url = 'api/UserLevel/WithoutStudent/Site';
          //   }
          //   else if (userlvlarr[i] == "ADMIN") {
          //     url = 'api/UserLevel/WithoutStudent/Admin';
          //   }
          //   else {
          //     url = 'api/UserLevel/WithoutStudent/null';
          //   }
          // }

          /* get role data based role user */

          if(this.checkrole()){
            if(this.itsDistributor()){
                url = 'api/UserLevel/WithoutStudent/Distributor'
            }else{
              if(this.itsOrganization()){
                url = 'api/UserLevel/WithoutStudent/Org'
              }else{
                url = 'api/UserLevel/WithoutStudent/Site'
              }
            }
          }else {
            url = 'api/UserLevel/WithoutStudent/null'
          }

          /* get role data based role user */

          this.getRoleAccess(url, this.datadetail.UserLevelId);
          // this.selectedItemsOrganization = [{id: this.datadetail.OrganizationId,itemName: this.datadetail.OrgName}];
          this.getSiteAffiliate(this.datadetail.ContactIdInt, this.datadetail.OrganizationId);
          this.Organization = this.datadetail.OrgName;
          this.primarysiteid_value = this.datadetail.PrimarySiteId;
          this.getJobRole();
          this.buildForm();
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
        });
  }

  getTimeZone() {
    var data = '';
    this.service.httpClientGet("api/TimeZones", data)
      .subscribe(result => {
        this.timezones = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  ngOnInit() {

    //get data edit contact
    this.id = this.route.snapshot.params['id'];
    this.siteid = this.route.snapshot.params['siteId'];
    this.getDetailContact(this.id, this.siteid);
    this.getHonorfic();
    this.getLanguages();
    this.getPrimaryIndustry();
    this.getTimeZone();

      this.dropdownSettingsRoleAcces = {
      singleSelection: false,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true
    };
    //setting dropdown for select one
    this.dropdownSettingsRoleAccesone = {
      singleSelection: false,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true,
      limitSelection: 1
    };
    //setting dropdown for select two
    this.dropdownSettingsRoleAccestwo = {
      singleSelection: false,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true,
      limitSelection: 2
    };
    //setting dropdown
    this.dropdownSettings = {
      singleSelection: true,
      text: "Please Select",
      enableSearchFilter: true,
      classes: "myclass custom-class",
      // disabled: false,1
      badgeShowLimit: 5,
      maxHeight: 120,
      showCheckbox: false,
      closeOnSelect: true
    };

    let userlvlarr = this.useraccesdata.UserLevelId.split(',');

    // Organization
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "ORGANIZATION" || userlvlarr[i] == "DISTRIBUTOR" || userlvlarr[i] == "SITE") {
        var orgArr = this.useraccesdata.OrgId.toString().split(',');
        var orgTmp = [];
        for (var i = 0; i < orgArr.length; i++) {
          orgTmp.push("'" + orgArr[i] + "'");
        }
        // this.getDataOrg('api/MainOrganization/AddContactOrg/' + this.useraccesdata.UserLevelId + '/' + orgTmp.join(","));
      }
      // else {
      // this.getDataOrg('api/MainOrganization/AddContact');
      // }
    }


  }

  //submit form
  submitted: boolean;
  validorg:boolean=true;
  affiliatevalid:boolean=true;
  validPri = true;
  validRoleAcc = true;
  onSubmit() {
    /* fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
    if(this.Organization == ""){
      this.validorg = false;
    }else{
      this.validorg = true;
    }
    if(this.editcontact.value.PrimaryRoleId){
      this.validPri = true;
    }else{
      this.validPri = false;
    }
    // if(this.editcontact.value.selectedItemsRoleAcces){
    //   this.validRoleAcc = true;
    // }else{
    //   this.validRoleAcc = false;
    // }
    // var anyaffiliate=false;
    // for(var i=0;i<this.sitesarray.length;i++){
    //   if(this.sitesarray[i][1]){
    //     anyaffiliate=true;
    //   }
    // }

    // if(anyaffiliate){
    //   this.affiliatevalid = true;
    // }else{
    //   this.affiliatevalid = false;
    // }

    let roleaccessarr = [];
    for (var i = 0; i < this.selectedItemsRoleAcces.length; i++) {
      roleaccessarr.push(this.selectedItemsRoleAcces[i].id)
    }

    if(roleaccessarr.length > 0){
      this.validRoleAcc = true;
    }else{
      this.validRoleAcc = false;
    }

    this.editcontact.controls['FirstName'].markAsTouched();
    this.editcontact.controls['LastName'].markAsTouched();
    this.editcontact.controls['EmailAddress'].markAsTouched();
    this.editcontact.controls['EmailAddress2'].markAsTouched();
    this.editcontact.controls['Timezone'].markAsTouched();
    /* endline fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
    if (this.editcontact.valid && this.primarysiteidselect && this.validemail && this.validorg && this.validPri && this.validRoleAcc) { /* fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
      swal({
        title: 'Are you sure?',
        text: "If you wish to Edit Contact!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          this.submitted = true;

          if (this.editcontact.value.DoNotEmail == true) {
            this.editcontact.value.DoNotEmail = "Y";
          } else {
            this.editcontact.value.DoNotEmail = "N";
          }

          // let roleaccessarr = [];
          // for (var i = 0; i < this.selectedItemsRoleAcces.length; i++) {
          //   roleaccessarr.push(this.selectedItemsRoleAcces[i].id)
          // }
          
          var dataUpdate = {
            'Honorific': this.editcontact.value.Honorific,
            'FirstName': this.service.encoder(this.editcontact.value.FirstName),
            'LastName': this.service.encoder(this.editcontact.value.LastName),
            'EmailAddress': this.editcontact.value.EmailAddress,
            'EmailAddress2': this.editcontact.value.EmailAddress2,
            'UserLevelId': roleaccessarr.join(','),
            'PrimaryRoleId': this.editcontact.value.PrimaryRoleId,
            'PrimaryIndustryId': this.editcontact.value.PrimaryIndustryId,
            'Timezone': this.editcontact.value.Timezone,
            'Mobile1': this.editcontact.value.Mobile1,
            'Mobile2': this.editcontact.value.Mobile2,
            'Telephone1': this.editcontact.value.Telephone1,
            'Telephone2': this.editcontact.value.Telephone2,
            'PrimaryLanguage': this.editcontact.value.PrimaryLanguage,
            'SecondaryLanguage': this.editcontact.value.SecondaryLanguage,
            'LastAdminBy': this.useraccesdata.ContactName,
            'Comments': this.service.encoder(this.editcontact.value.Comments),//this.editcontact.value.Comments,
            'ContactName': this.service.encoder(this.editcontact.value.FirstName) + " " + this.service.encoder(this.editcontact.value.LastName),
            'DoNotEmail': this.editcontact.value.DoNotEmail,
            'IsInstructor': this.editcontact.value.ATCRole,
            'InstructorId': this.datadetail.InstructorId,
            'ContactId': this.datadetail.ContactId,
            'cuid': this.useraccesdata.ContactName,
            'UserId': this.useraccesdata.UserId,
            'Designation': this.service.encoder(this.editcontact.value.Designation)
          };

          //convert object to json
          let data = JSON.stringify(dataUpdate);

          data = data.replace(/'/g,"\\\\'"); /* issue 27092018 - handling crud with char (') inside */
          
          for (var i = 0; i < this.sitesarray.length; i++) {
            if (this.sitesarray[i][1] == true) {
              this.status[i] = "A";
            } else {
              this.status[i] = "X";
            }
          }

          var dataafiliate =
          {
            PrimarySiteId: this.primarysiteid_value,
            SiteIdArr: this.sitesarray,
            ContactIdInt: this.datadetail.ContactIdInt,
            Status: this.status,
            AddedBy: this.useraccesdata.ContactName,
            cuid:this.useraccesdata.ContactName,
            UserId:this.useraccesdata.UserId
          }

          let dataSubmitAfiliate = JSON.stringify(dataafiliate);

          // Fix can not call api detail-contact when primarysiteid_value null
          let _siteId = this.primarysiteid_value;
          if (!_siteId) _siteId = this.siteid;
          
          // this.service.httpCLientPutNew(this._serviceUrl, this.datadetail.ContactIdInt, data, this._serviceUrl + '/DetailContact/' + this.id + '/' + this.siteid);

          this.service.httpClientPost(this._serviceUrl +"/"+ this.datadetail.ContactIdInt, data)
            .subscribe(res => {
              var result = res;
              if (result['code'] == '1') {
                //this.service.httpClientGet(this._serviceUrl + '/DetailContact/' + this.id + '/' + this.siteid,'');
                
                //update site on contact action
                this.service.httpClientPost('api/SiteContactLinks', dataSubmitAfiliate)
                .subscribe(result => {                    
                    this.router.navigate(['/manage/contact/detail-contact', this.id, _siteId]);
                    this.loading = false;
                  }, error => {
                    this.service.errorserver();
                    this.router.navigate(['/manage/contact/detail-contact', this.id, _siteId]);
                    this.loading = false;
                  });
              }
              else {
                console.log("failed");
                this.router.navigate(['/manage/contact/detail-contact', this.id, _siteId]);
                this.loading = false;
              }
            },
            error => {
              console.log("failed");
              this.router.navigate(['/manage/contact/detail-contact', this.id, _siteId]);
              this.loading = false;
            });
          // setTimeout(() => {
          //   this.router.navigate(['/manage/contact/detail-contact', this.id, _siteId]);
          //   this.loading = false;
          // }, 5000)
        }
      }).catch(swal.noop);
    }
    else{
      swal(
        'Information!',
        'Please Fill The Form',
        'error'
      )
    }
  }

  buildForm(): void {

    var sendemail = false;
    if (this.datadetail.DoNotEmail == "Y") {
      sendemail = true;
    }

    let primaryjob = ""
    if (this.datadetail.PrimaryRoleId != 0 && this.datadetail.PrimaryRoleId != null) {
      primaryjob = this.datadetail.PrimaryRoleId;
    }

    let Honorific = new FormControl(this.datadetail.Salutation);
    let ContactName = new FormControl(this.datadetail.ContactName);
    /* fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
    let FirstName = new FormControl(this.datadetail.FirstName, Validators.required);
    let LastName = new FormControl(this.datadetail.LastName, Validators.required);
    let EmailAddress = new FormControl(this.datadetail.EmailAddress, [Validators.required, Validators.email]);
    let EmailAddress2 = new FormControl(this.datadetail.EmailAddress2, [CustomValidators.notEqualTo(EmailAddress)]);
    let DoNotEmail = new FormControl(sendemail);
    let UserLevelId = new FormControl(this.datadetail.UserLevelId);
    let PrimaryRoleId = new FormControl(primaryjob);
    let PrimaryIndustryId = new FormControl(this.datadetail.PrimaryIndustryId);
    let Telephone1 = new FormControl(this.datadetail.Telephone1);
    let Telephone2 = new FormControl(this.datadetail.Telephone2);
    let Mobile1 = new FormControl(this.datadetail.Mobile1);
    let Mobile2 = new FormControl(this.datadetail.Mobile2);
    let PrimaryLanguage = new FormControl(this.datadetail.PrimaryLanguage);
    let SecondaryLanguage = new FormControl(this.datadetail.SecondaryLanguage);
    let Timezone = new FormControl(this.datadetail.valueTimeZone, Validators.required);
    /* end line fixed issue 20092018 - Edit contact page> Even if the mandatory fields are left blank the user is allowed to save the contact */
    let Comments = new FormControl(this.datadetail.Comments);
    let ATCRole = new FormControl('');
    let Designation = new FormControl(this.datadetail.Designation);

    this.editcontact = new FormGroup({
      ContactName: ContactName,
      FirstName: FirstName,
      LastName: LastName,
      EmailAddress: EmailAddress,
      EmailAddress2: EmailAddress2,
      DoNotEmail: DoNotEmail,
      UserLevelId: UserLevelId,
      PrimaryRoleId: PrimaryRoleId,
      PrimaryIndustryId: PrimaryIndustryId,
      Telephone1: Telephone1,
      Telephone2: Telephone2,
      Mobile1: Mobile1,
      Mobile2: Mobile2,
      PrimaryLanguage: PrimaryLanguage,
      SecondaryLanguage: SecondaryLanguage,
      Timezone: Timezone,
      Comments: Comments,
      Honorific: Honorific,
      ATCRole: ATCRole,
      Designation: Designation
    });
  }

  //reset form
  resetForm() {
    this.editcontact.reset({
      'FirstName': this.datadetail.FirstName,
      'LastName': this.datadetail.LastName,
      'EmailAddress': this.datadetail.EmailAddress,
      'EmailAddress2': this.datadetail.EmailAddress2,
      'PrimaryRoleId': this.datadetail.PrimaryRoleId,
      'PrimaryIndustryId': this.datadetail.PrimaryIndustryId,
      'Timezone': this.datadetail.Timezone,
      'Telephone1': this.datadetail.Telephone1,
      'Telephone2': this.datadetail.Telephone2,
      'PrimaryLanguage': this.datadetail.PrimaryLanguage,
      'SecondaryLanguage': this.datadetail.SecondaryLanguage,
      'Comments': this.datadetail.Comments,
      'Designation': this.datadetail.Designation
    });
  }

  // getDataOrg(url) {
  //   this.service.get(url, '')
  //     .subscribe(result => {
  //       var finalresult = result.replace(/\t/g, " ")
  //       if (finalresult == "Not found") {
  //         this.service.notfound();
  //         this.dropdownListOrganization = null;
  //       }
  //       else {
  //         this.dropdownListOrganization = JSON.parse(finalresult).map((item) => {
  //           return {
  //             id: item.OrganizationId,
  //             itemName: item.OrgName
  //           }
  //         })
  //       }
  //     },
  //     error => {
  //       this.service.errorserver();
  //     });
  //   this.selectedItemsOrganization = [];
  // }

  datasite = []; /* just show affiliate site checked */
  sitesarray = [];
  status = [];
  // onItemSelect(item: any) {
  //   this.sitesarray = [];
  //   this.status = [];

  //   if(item.id != this.datadetail.OrganizationId){
  //     this.primarysiteidselect= false;
  //     var data = '';
  //     this.service.httpClientGet('api/MainSite/Affiliate/addnew' + '/' + item.id, data)
  //       .subscribe(result => {
  //         if (result == "Not found") {
  //           this.service.notfound();
  //           this.datasite = '';
  //         }
  //         else {
  //           this.datasite = result;
  //           this.sitesarray = [];
  //           for (var i = 0; i < this.datasite.length; i++) {
  //             this.sitesarray.push([this.datasite[i].SiteId, true, "InsertNew"]);
  //             this.status.push("A");
  //           }
  //           for (var i = 0; i < this.siteAfiliateTmp.length; i++) {
  //             this.sitesarray.push([this.siteAfiliateTmp[i].SiteId, false, this.siteAfiliateTmp[i].SiteContactLinkId]);
  //             this.status.push("X");
  //           }
  //         }
  //       },
  //       error => {
  //         this.service.errorserver();
  //         this.datasite = '';
  //       });
  //   }else{
  //     this.getSiteAffiliate(this.datadetail.ContactIdInt,this.datadetail.OrganizationId);
  //     this.primarysiteid_value = this.datadetail.PrimarySiteId;
  //     this.primarysiteidselect = true;
  //   }
  // }
  
   changeItsInstructor(value){
    if(value){
      for(let i = 0; i < this.dropdownListRoleAcces.length; i++){
        if(this.dropdownListRoleAcces[i].id == "TRAINER"){
          this.selectedItemsRoleAcces.push({
            id: this.dropdownListRoleAcces[i].id,
            itemName: this.dropdownListRoleAcces[i].itemName
          })
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
          break;
        }
        this.selectedItemsRoleAcces = [];
      }
    } else{
      for(let j = 0; j < this.selectedItemsRoleAcces.length; j++){
        if (this.selectedItemsRoleAcces[j].id == "TRAINER") {
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
          this.selectedItemsRoleAcces.splice(j, 1);
        }
        else {
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
        }
      }
    }
  }
    

  sitechecked(site) {
    var sitearr = [];
    for (var i = 0; i < site.length; i++) {
      if (site[i].CheckedSites == true) {
        this.sitesarray.push([site[i].SiteId, true, site[i].SiteContactLinkId]);
      } else {
        this.sitesarray.push([site[i].SiteId, false, site[i].SiteContactLinkId]);
      }

      sitearr.push("'" + site[i].SiteId + "'");
    }

    for (var i = 0; i < this.sitesarray.length; i++) {
      this.status.push("A");
    }

    this.dataService = this.completerService.remote(
      null,
      "Organization",
      "Organization");
    this.dataService.urlFormater(term => {
      return `api/MainOrganization/FindOrganizationEditAffiliate/` + this.useraccesdata.UserLevelId + `/` + sitearr.join(",") + `/` + term;
    });
    this.dataService.dataField("results");
  }

  urlGetOrgId(): string {
    var orgarr = this.useraccesdata.OrgId.split(',');
    var orgnew = [];
    for (var i = 0; i < orgarr.length; i++) {
        orgnew.push('"' + orgarr[i] + '"');
    }
    return orgnew.toString();
  }

  siteAfiliateTmp: any;
  getSiteAffiliate(contactidint, orgid) {
    var data = '';
    /* populate based role (distributor) */
    var url = 'api/MainSite/AffiliateSite/' + contactidint + '/' + orgid;
    if(this.checkrole()){
      url = 'api/MainSite/AffiliateDistributor/' + contactidint + '/' + orgid + '/' + this.urlGetOrgId();
    }
    
    this.service.httpClientGet(url, data) /* populate based role (distributor) */
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datasite = []; /* just show affiliate site checked */
        }
        else {
          // this.datasite = result;
          this.siteAfiliateTmp = result;
          
          /* just show affiliate site checked */
          let datatmp:any = result;
          for(var i=0;i<datatmp.length;i++){
          //  if(datatmp[i].CheckedSites){
              this.datasite.push({
                CheckedSites:datatmp[i].CheckedSites,
                OrgId:datatmp[i].OrgId,
                SiteAddress1:datatmp[i].SiteAddress1,
                SiteContactLinkId:datatmp[i].SiteContactLinkId,
                SiteCountryCode:datatmp[i].SiteCountryCode,
                SiteId:datatmp[i].SiteId,
                SiteIdInt:datatmp[i].SiteIdInt,
                SiteName:this.service.decoder(datatmp[i].SiteName),
                Status:datatmp[i].Status,
                geo_name:datatmp[i].geo_name,
                PartnerStatusDetail:datatmp[i].PartnerStatusDetail
              })
           // }
          }
          for (var i = 0; i < datatmp.length; i++) {
            if (!datatmp[i].CheckedSites) {
              this.allChecked = false;
              break;
            }
            
          }
          /* just show affiliate site checked */

          this.sitechecked(this.datasite);
        }
      },
        error => {
          this.service.errorserver();
          this.datasite = []; /* just show affiliate site checked */
        });
  }

  selectSite(value, sites) {
    if (value.target.checked) {
      for (var i = 0; i < this.sitesarray.length; i++) {
        if (this.sitesarray[i][0] == sites) {
          this.sitesarray[i][1] = true;
        }
      }
    }
    else {
      for (var i = 0; i < this.sitesarray.length; i++) {
        if (this.sitesarray[i][0] == sites) {
          this.sitesarray[i][1] = false;
        }
      }
    }
  }

  primarysiteidselect: boolean = true;
  primarysiteid_value: string = "";
  onCheckboxChangeFn(value) {
    this.primarysiteid_value = value;
    this.primarysiteidselect = true;
  }

  OrganizationPicked: boolean = false;
  PickedOrganization(data) {
    this.OrganizationPicked = true;
    var dataOrg = {
      id: data["originalObject"].OrganizationId,
      itemName: data["originalObject"].OrgName
    };
    this.onItemSelect(dataOrg);
  }

  onItemSelect(item: any) {
    let datasitetmp: any;
    var data = '';
    this.service.httpClientGet('api/MainSite/Affiliate/addnew' + '/' + item.id, data)
      .subscribe(result => {
        //var finalresult = result.replace(/\t/g, " ");
        var finalresult : any = result
        if (finalresult == "Not found") {
          this.service.notfound();
          datasitetmp = '';
        }
        else {
          datasitetmp = finalresult;
          for (var i = 0; i < datasitetmp.length; i++) {
            this.sitesarray.push([datasitetmp[i].SiteId, true, "InsertNew"]);
            this.status.push("A");
            this.datasite.push({
              SiteId: datasitetmp[i].SiteId,
              CheckedSites: datasitetmp[i].CheckedSites,
              OrgId: datasitetmp[i].OrgId,
              Status: datasitetmp[i].Status,
              SiteIdInt: datasitetmp[i].SiteIdInt,
              SiteName: datasitetmp[i].SiteName,
              SiteCountryCode: datasitetmp[i].SiteCountryCode,
            })
          }
        }
      },
        error => {
          this.service.errorserver();
          datasitetmp = '';
        });
  }

  //check email registered
  validemail: Boolean = true;
  public emaildata: any;
  checkEmailDuplicate(value) {
    if (value != '' && value != null && value != this.datadetail.EmailAddress) {
      this.service.httpClientGet("api/ContactAll/" + value, '')
        .subscribe(result => {
          this.emaildata = result;
          if (this.emaildata.EmailAddress != null) {
            this.validemail = false;
          }
          else if (this.emaildata.EmailAddress == null) {
            this.validemail = true;
          }
        },
        error => {
          this.service.errorserver();
        });
    }
    else {
      this.validemail = true;
    }
  }

 checkAll(value) {
   var chkSiteID = document.getElementsByTagName('input');
   
   if (value.target.checked) {
     for (var i = 2; i < chkSiteID.length; i++) {
        if (chkSiteID[i].type == 'checkbox') {
         chkSiteID[i].checked = true;
        }
      }
     for (var i = 0; i < this.sitesarray.length; i++) {
         this.sitesarray[i][1] = true;
     }
   }
   else {
     for (var i = 2; i < chkSiteID.length; i++) {
       if (chkSiteID[i].type == 'checkbox') {
         chkSiteID[i].checked = false;
       }
     }
     for (var i = 0; i < this.sitesarray.length; i++) {
       this.sitesarray[i][1] = false;
     }
     
   }
    
  }
}
