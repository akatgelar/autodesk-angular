import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteJournalEntryReportEPDBComponent } from './site-journal-entry-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const SiteJournalEntryReportEPDBRoutes: Routes = [
    {
        path: '',
        component: SiteJournalEntryReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Site Journal Entry Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(SiteJournalEntryReportEPDBRoutes),
      SharedModule
    ],
    declarations: [SiteJournalEntryReportEPDBComponent]
  })
  export class SiteJournalEntryReportEPDBModule { }