import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteAttributeReportEPDBComponent } from './site-attributes-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const SiteAttributeReportEPDBRoutes: Routes = [
    {
        path: '',
        component: SiteAttributeReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Site Attributes Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(SiteAttributeReportEPDBRoutes),
      SharedModule
    ],
    declarations: [SiteAttributeReportEPDBComponent]
  })
  export class SiteAttributeReportEPDBModule { }