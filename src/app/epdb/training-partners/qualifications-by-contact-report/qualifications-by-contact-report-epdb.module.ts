import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QualificationByContactReportEPDBComponent } from './qualifications-by-contact-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const QualificationByContactReportEPDBRoutes: Routes = [
    {
        path: '',
        component: QualificationByContactReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Qualification By Contact Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(QualificationByContactReportEPDBRoutes),
      SharedModule
    ],
    declarations: [QualificationByContactReportEPDBComponent]
  })
  export class QualificationByContactReportEPDBModule { }