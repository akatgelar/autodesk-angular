import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactJournalEntryReportEPDBComponent } from './contact-journal-entry-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const ContactJournalEntryReportEPDBRoutes: Routes = [
    {
        path: '',
        component: ContactJournalEntryReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Contact Journal Entry Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(ContactJournalEntryReportEPDBRoutes),
      SharedModule
    ],
    declarations: [ContactJournalEntryReportEPDBComponent]
  })
  export class ContactJournalEntryReportEPDBModule { }