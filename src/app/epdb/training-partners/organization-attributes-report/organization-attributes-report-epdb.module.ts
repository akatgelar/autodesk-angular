import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationAttributeReportEPDBComponent } from './organization-attributes-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const OrganizationAttributeReportEPDBRoutes: Routes = [
    {
        path: '',
        component: OrganizationAttributeReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Organization Attributes Report  ',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(OrganizationAttributeReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [OrganizationAttributeReportEPDBComponent]
  })
  export class OrganizationAttributeReportEPDBModule { }