import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactAttributeReportEPDBComponent } from './contact-attributes-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const ContactAttributeReportEPDBRoutes: Routes = [
    {
        path: '',
        component: ContactAttributeReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Contact Attributes Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(ContactAttributeReportEPDBRoutes),
      SharedModule
    ],
    declarations: [ContactAttributeReportEPDBComponent]
  })
  export class ContactAttributeReportEPDBModule { }