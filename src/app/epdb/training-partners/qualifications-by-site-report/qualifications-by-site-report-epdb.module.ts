import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QualificationBySiteReportEPDBComponent } from './qualifications-by-site-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const QualificationBySiteReportEPDBRoutes: Routes = [
    {
        path: '',
        component: QualificationBySiteReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Qualifications by Site Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(QualificationBySiteReportEPDBRoutes),
      SharedModule
    ],
    declarations: [QualificationBySiteReportEPDBComponent]
  })
  export class QualificationBySiteReportEPDBModule { }