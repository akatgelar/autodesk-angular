import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ATCInvoiceReportEPDBComponent } from './atc-invoices-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const ATCInvoiceReportEPDBRoutes: Routes = [
    {
        path: '',
        component: ATCInvoiceReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / ATC Invoices Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(ATCInvoiceReportEPDBRoutes),
      SharedModule
    ],
    declarations: [ATCInvoiceReportEPDBComponent]
  })
  export class ATCInvoiceReportEPDBModule { }