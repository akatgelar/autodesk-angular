import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteAccreditationReportEPDBComponent } from './site-accreditations-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const SiteAccreditationReportEPDBRoutes: Routes = [
    {
        path: '',
        component: SiteAccreditationReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Site Accreditaions Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(SiteAccreditationReportEPDBRoutes),
      SharedModule
    ],
    declarations: [SiteAccreditationReportEPDBComponent]
  })
  export class SiteAccreditationReportEPDBModule { }