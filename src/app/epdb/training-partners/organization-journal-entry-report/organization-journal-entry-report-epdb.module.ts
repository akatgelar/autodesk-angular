import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationJournalEntryReportEPDBComponent } from './organization-journal-entry-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const OrganizationJournalEntryReportEPDBRoutes: Routes = [
    {
        path: '',
        component: OrganizationJournalEntryReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Organization Journal Entry Report  ',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(OrganizationJournalEntryReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [OrganizationJournalEntryReportEPDBComponent]
  })
  export class OrganizationJournalEntryReportEPDBModule { }