import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SerialNumberReportEPDBComponent } from './serial-numbers-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const SerialNumberReportEPDBRoutes: Routes = [
    {
        path: '',
        component: SerialNumberReportEPDBComponent,
        data: {
            breadcrumb: 'Training Partners / Serial Numbers Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(SerialNumberReportEPDBRoutes),
      SharedModule
    ],
    declarations: [SerialNumberReportEPDBComponent]
  })
  export class SerialNumberReportEPDBModule { }
