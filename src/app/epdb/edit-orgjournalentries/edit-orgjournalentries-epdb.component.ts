import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-orgjournalentries-epdb',
  templateUrl: './edit-orgjournalentries-epdb.component.html',
  styleUrls: [
    './edit-orgjournalentries-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class EditOrgJournalEntriesEPDBComponent implements OnInit {

  private _serviceUrl = 'api/OrganizationJournalEntries';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public dataorg: any;
  updateorgjournalentries: FormGroup;
  orgid: string = '';
  public useraccesdata: any;
  public loading = false;
  private activityDate;
  modelPopup1: NgbDateStruct;
  public orgentriestype: any;

  constructor(private session: SessionService, private service: AppService, private route: ActivatedRoute, private router: Router,
    private parserFormatter: NgbDateParserFormatter, private datePipe: DatePipe, private formatdate: AppFormatDate) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let DateAdded = new FormControl();
    let AddedBy = new FormControl();
    let ActivityId = new FormControl();
    let ActivityDate = new FormControl();
    let Notes  = new FormControl();
    let DateLastAdmin = new FormControl();
    let LastAdminBy = new FormControl();

    this.updateorgjournalentries = new FormGroup({
      AddedDateAddedBy: DateAdded,
      AddedBy: AddedBy,
      ActivityId: ActivityId,
      ActivityDate: ActivityDate,
      Notes:Notes,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy
    });
  }

  getOrgId(id) {
    //get org id
    var data = '';
    this.service.httpClientGet('api/MainOrganization/where/{"OrgId": "' + id + '"}', data)
      .subscribe(result => {
        this.dataorg =result;
        this.orgid = this.dataorg[0].OrganizationId;
     
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
  }

  id: string = "";
  getJournalEntries() {
    this.id = this.route.snapshot.params['id'];

    var data: any;
    this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.data = result;
          if (this.data != null) {
            if (this.data.Notes != null && this.data.Notes != "") {
              this.data.Notes = this.service.decoder(this.data.Notes);
            }
          }
          this.buildForm();
          this.getOrgId(this.data.ParentId);
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });

    this.service.httpClientGet("api/JournalActivities/where/{'ActivityType': 'Organization Journal Entry'}", data)
      .subscribe(result => {
        if (result == "Not found") {
          this.orgentriestype = '';
        } else {
          this.orgentriestype = result;
        }
      },
        error => {
          this.service.errorserver();
          this.orgentriestype = '';
        });
  }

  onSelectDate(date: NgbDateStruct) {
    if (date != null) {
      this.activityDate = this.parserFormatter.format(date);
    } else {
      this.activityDate = "";
    }
  }

  ngOnInit() {
    this.getJournalEntries();
  }

  //submit form
  // submitted: boolean;
  onSubmit() {
    // this.submitted = true;
    // this.updateorgjournalentries.value.LastAdminBy = this.useraccesdata.ContactName
    // let data = JSON.stringify(this.updateorgjournalentries.value);
    // if (this.updateorgjournalentries.valid) {

    //   this.loading = true;
    //   this.service.put(this._serviceUrl + "/" + this.id, data)
    //     .subscribe(result => {
    //       var resource = JSON.parse(result);
    //       if (resource['code'] == '1') {
    //         this.service.openSuccessSwal(resource['name']);
    //       }
    //       else {
    //         swal(
    //           'Information!',
    //           "Update Data Failed",
    //           'error'
    //         );
    //       }
    //       this.router.navigate(['/manage/organization/detail-organization', this.dataorg[0].OrganizationId]);
    //       this.loading = false;
    //     },
    //       error => {
    //         this.messageError = <any>error
    //         this.service.errorserver();
    //         this.loading = false;
    //       });
    // }

    this.updateorgjournalentries.controls["ActivityId"].markAsTouched();
    this.updateorgjournalentries.controls["ActivityDate"].markAsTouched();
    this.updateorgjournalentries.controls["Notes"].markAsTouched();
	var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
    if (this.updateorgjournalentries.valid) {
      this.loading = true;
      var dataJournal = {
        'ParentId': this.data.ParentId,
        'DateLastAdmin': dateFormat,
		//this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
        'LastAdminBy': this.useraccesdata.ContactName,
        'ActivityId': this.updateorgjournalentries.value.ActivityId,
        'Notes': this.service.encoder(this.updateorgjournalentries.value.Notes),//this.htmlEntityService.encoder(this.updateorgjournalentries.value.Notes),
        'ActivityDate': this.formatdate.dateCalendarToYMD(this.updateorgjournalentries.value.ActivityDate),

        'cuid' : this.useraccesdata.ContactName,
        'UserId' : this.useraccesdata.UserId,
        'History' : 'Organizaation Journal Entries'
      };
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, dataJournal)
        .subscribe(res=>{
          console.log(res)
        });
      setTimeout(() => {

        this.router.navigate(['/manage/organization/detail-organization', this.dataorg[0].OrganizationId]);
        this.loading = false;
      }, 1000)
    }

  }

  //build form update
  buildForm(): void {
    var activityDateTemp = new Date(this.data.ActivityDate);
	//(this.data.ActivityDate).substr(0, 8).replace(/\b0/g, '');
	
	var date=activityDateTemp.getDate();
	var month=activityDateTemp.getMonth() + 1;
	var year=activityDateTemp.getFullYear();
	
   var activityDate=activityDateTemp; //= activityDateTemp.split('/');
	
    if (year > 2000) {
      this.modelPopup1 = {
        "year": year,
        "month": month,
        "day": date
      };
    }else{
      this.modelPopup1 = null;
    }
    
    // var ActivityDateConvert = this.formatdate.dateYMDToCalendar(this.data.ActivityDate);

    let DateAdded = new FormControl(this.data.DateAdded);
    let AddedBy = new FormControl(this.data.AddedBy);
    let ActivityId = new FormControl(this.data.ActivityId, Validators.required);
    let ActivityDate = new FormControl(activityDate, Validators.required);
    let Notes  = new FormControl(this.data.Notes);
    let DateLastAdmin = new FormControl(this.data.DateLastAdmin);
    let LastAdminBy = new FormControl(this.data.LastAdminBy);

    this.updateorgjournalentries = new FormGroup({
      AddedDateAddedBy: DateAdded,
      AddedBy: AddedBy,
      ActivityId: ActivityId,
      ActivityDate: ActivityDate,
      Notes:Notes,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy
    });
  }
}
