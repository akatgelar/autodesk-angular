import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditOrgJournalEntriesEPDBComponent } from './edit-orgjournalentries-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const EditOrgJournalEntriesEPDBRoutes: Routes = [
  {
    path: '',
    component: EditOrgJournalEntriesEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.add_org_journal_entries.edit_org_journal_entries',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditOrgJournalEntriesEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [EditOrgJournalEntriesEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class EditOrgJournalEntriesEPDBModule { }
