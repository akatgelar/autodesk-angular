import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';

import {AppService} from "../../../shared/service/app.service";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

import { Router, ActivatedRoute } from '@angular/router';

import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-edit-role-epdb',
    templateUrl: './edit-role-epdb.component.html',
    styleUrls: [
      './edit-role-epdb.component.css',
      '../../../../../node_modules/c3/c3.min.css',
      ],
    encapsulation: ViewEncapsulation.None
})

export class EditRoleEPDBComponent implements OnInit {

    private _serviceUrl = 'api/AccessRole';
    messageResult: string = '';
    messageError: string = '';
    private data;
    role;
    dataRole;
    public datadetail: any;

    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    addRole : FormGroup;
    updateRole : FormGroup;
    public RoleName;

    id:any
    useraccesdata:any;

    constructor(private session: SessionService, private router: Router, private route:ActivatedRoute, private service:AppService, private formatdate:AppFormatDate) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        //validation
        let Name = new FormControl('', Validators.required);
        let Description = new FormControl('', Validators.required);

        this.addRole = new FormGroup({
            Name: Name,
            Description: Description
        });

        let Name_update = new FormControl('');
        let Description_update = new FormControl('');
        this.updateRole = new FormGroup({
            Name:Name_update,
            Description:Description_update
        });

    }

    ngOnInit(){
        this.id = this.route.snapshot.params['id'];
        var dataRole = '';
        this.service.httpClientGet("api/AccessRole/"+this.id,dataRole)
        .subscribe(result => {
            this.dataRole = result;
            this.RoleName = this.dataRole.Name;
            this.buildFormAddRole();
        },
        error => {
            this.service.errorserver();
        });
    }

    buildFormAddRole(): void{

        let Name = new FormControl(this.dataRole.Name, Validators.required);
        let Description = new FormControl(this.dataRole.Description, Validators.required);

        this.updateRole = new FormGroup({
            Name: Name,
            Description: Description
        });
    }

    resetFormUpdate(){
        this.updateRole.reset({
          'Name':this.dataRole.Name,
          'Description':this.dataRole.Description
        });
      }

      
    onUpdate() {

        //form data
        this.updateRole.value.cuid = this.useraccesdata.ContactName;
        this.updateRole.value.UserId = this.useraccesdata.UserId;

        let data = JSON.stringify(this.updateRole.value);

        //put action
        this.service.httpCLientPut("api/AccessRole/"+this.dataRole.AccessRoleId, data)
        .subscribe(result=>{
            this.role = result;
            this.router.navigate(['/role/add-role']);
          },
          error => {
              this.service.errorserver();
          });
    }


    openConfirmsSwal(id, index) {
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        })
        .then(result => {
            if (result == true) {
                var data = '';
                this.service.httpClientDelete("api/AccessRole/"+id, data)
                .subscribe(value => {
                    var resource = value;
                    this.service.openSuccessSwal(resource['message']);
                    var index = this.role.findIndex(x => x.AccessRoleId == id);
                    if (index !== -1) {
                        this.role.splice(index, 1);
                    }
                },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });
            }

        }).catch(swal.noop);
    }

    resetForm() {
      this.addRole.reset({
        'Name': '',
        'Description': ''
      });
    }
}
