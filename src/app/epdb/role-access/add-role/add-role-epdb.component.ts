import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http, Response, HttpModule} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import {AppService} from "../../../shared/service/app.service";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterRole' })
export class DataFilterRolePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.AccessRoleId.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
        (row.Name.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
        (row.Description.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
    selector: 'app-add-role-epdb',
    templateUrl: './add-role-epdb.component.html',
    styleUrls: [
      './add-role-epdb.component.css',
      '../../../../../node_modules/c3/c3.min.css',
      ],
    encapsulation: ViewEncapsulation.None
})

export class AddRoleEPDBComponent implements OnInit {

    private _serviceUrl = 'api/AccessRole';
    messageResult: string = '';
    messageError: string = '';
    private data;
    role;
    dataRole;
    public datadetail: any;

    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "AccessRoleId";
    public sortOrder: string = "asc";
    addRole : FormGroup;
    updateRole : FormGroup;
    public loading = false;
    
    useraccesdata:any;

    constructor(private session: SessionService, private router: Router, private service:AppService, private formatdate:AppFormatDate, private _http: Http) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        //validation
        let Name = new FormControl('', Validators.required);
        let Description = new FormControl('', Validators.required);
        let Access = new FormControl(false);
        let Write = new FormControl(false);
        let Delete = new FormControl(false);

        this.addRole = new FormGroup({
            Name: Name,
            Description: Description,
            Access: Access,
            Write: Write,
            Delete: Delete
        });

        let Name_update = new FormControl('');
        let Description_update = new FormControl('');
        this.updateRole = new FormGroup({
            Name:Name_update,
            Description:Description_update
        });

    }

    ngOnInit(){
        this.getRole();
    }

    getRole() {
        var role = '';
        this.service.httpClientGet("api/AccessRole",role)
        .subscribe(result => {
            this.role = result;
        },
        error => {
            this.service.errorserver();
        });
    }

    success:Boolean=false;
    onSubmit() {

        this.addRole.controls['Name'].markAsTouched();
        this.addRole.controls['Description'].markAsTouched();

        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.addRole.value.Name))
            return swal('ERROR','Special character not allowed in role name','error')
        if(format.test(this.addRole.value.Description))
            return swal('ERROR','Special character not allowed in role description','error')

        if(this.addRole.valid) {
            this.loading = true;

            this.addRole.value.cuid = this.useraccesdata.ContactName;
            this.addRole.value.UserId = this.useraccesdata.UserId;

            let data = JSON.stringify(this.addRole.value);

            this.service.httpClientPost(this._serviceUrl,data)
            .subscribe(value=>{
                if(value != null || value == "Not Found"){
                    this.role = value;
                    this.resetForm();
                    this.success = true;
                    setTimeout(() => {
                        this.success = false;
                    }, 3000)
                }
            },
            error => {
                this.service.errorserver();
            });

            var listAccess: any;
            this.service.httpClientGet("api/Language/RoleAccess",listAccess)
              .subscribe(res => {
                  listAccess = res;
                  console.log(listAccess);
                  var string_access = [];
                  for(let i = 0; i < listAccess.length; i++){
                      if(this.addRole.value.Access == true && listAccess[i].read_url != "xxx"){
                          var splitReadUrl = listAccess[i].read_url.split("#");
                          for(let j = 0; j < splitReadUrl.length; j++){
                              var string_read = "`" + splitReadUrl[j] + "` = '11111'"; 
                              string_access.push(string_read);
                          }
                      }

                      if(this.addRole.value.Write == true && listAccess[i].write_url != "xxx"){
                          var splitWriteUrl = listAccess[i].write_url.split("#");
                          for(let k = 0; k < splitWriteUrl.length; k++){
                              var string_write = "`" + splitWriteUrl[k] + "` = '11111'";
                              string_access.push(string_write);
                          }
                      }

                      if(this.addRole.value.Delete == true && listAccess[i].delete_url != "xxx"){
                          var string_delete = "`" + listAccess[i].delete_url + "` = '11111'";
                          string_access.push(string_delete);
                      }
                  }
                  var updateMenu = {Kolom: string_access.join(",")}
                  // console.log(updateMenu);
                  this.service.httpCLientPut("api/UserLevel/"+this.addRole.value.Name,updateMenu)
                    .subscribe(res=>{
                        console.log(res)
                    });
              });
              this.loading = false;
        }
    }

    onUpdate() {

        //form data
        let data = JSON.stringify(this.updateRole.value);

        //put action
        this.service.httpCLientPut("api/AccessRole/"+this.dataRole.AccessRoleId, data)
        .subscribe(result=>{
            this.role = result;
          },
          error => {
              this.service.errorserver();
          });
    }

    viewRoleDetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl+'/'+id, data)
        .subscribe(result => {
            if(result=="Not found"){
              this.datadetail = '';
            }else{
              this.datadetail = result;
            }
          },
          error => {
              this.service.errorserver();
              this.datadetail = '';
          });
    }

    getEditRole(id) {
        var dataRole = '';
        this.service.httpClientGet("api/AccessRole/"+id,dataRole)
        .subscribe(result => {
            this.dataRole = result;
            this.buildFormAddRole();
        },
        error => {
            this.service.errorserver();
        });
    }

    buildFormAddRole(): void{

        let Name = new FormControl(this.dataRole.Name, Validators.required);
        let Description = new FormControl(this.dataRole.Description, Validators.required);

        this.updateRole = new FormGroup({
            Name: Name,
            Description: Description
        });
    }

    resetFormUpdate(){
        this.updateRole.reset({
          'Name':this.dataRole.Name,
          'Description':this.dataRole.Description
        });
      }

    openConfirmsSwal(id, index) {
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        })
        .then(result => {
            if (result == true) {
                var data = '';
                this.service.httpClientDelete("api/AccessRole/"+id, data)
                .subscribe(value => {
                    var resource = value;
                    this.service.openSuccessSwal(resource['message']);
                    var index = this.role.findIndex(x => x.AccessRoleId == id);
                    if (index !== -1) {
                        this.role.splice(index, 1);
                    }
                },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });
            }

        }).catch(swal.noop);
    }

    resetForm() {
      this.addRole.reset({
        'Name': '',
        'Description': ''
      });
    }
}
