import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import * as c3 from "c3";
import { Http, Response, HttpModule } from "@angular/http";
declare const $: any;
declare var Morris: any;
import "../../../../assets/echart/echarts-all.js";
import swal from "sweetalert2";
import { Router } from "@angular/router";
import { AppService } from "../../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

// @Pipe({ name: 'keys', pure: false })
// export class KeysPipe implements PipeTransform {
//     transform(value: any, args: any[] = null): any {
//         return Object.keys(value)//.map(key => value[key]);
//     }
// }

@Component({
  selector: "app-list-access-epdb",
  templateUrl: "./list-access-epdb.component.html",
  styleUrls: [
    "./list-access-epdb.component.css",
    "../../../../../node_modules/c3/c3.min.css"
  ],
  encapsulation: ViewEncapsulation.None
})
export class ListAccessEPDBComponent implements OnInit {
  private _serviceUrl = "api/AccessRoleMenu";
  messageResult: string = "";
  messageError: string = "";
  public data: any;
  rolemenuform: FormGroup;

  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  public rolelist: any;
  private Name = "";
  public menulist: any;
  selectedMenuId = [];
  listAccess;
  urlAccess;
  listKey = [];
  private userLevelId;
  public loading = false;

  constructor(
    private router: Router,
    private service: AppService,
    private _http: Http,
    public session: SessionService
  ) {
    // declaration form
    let AccessRoleId = new FormControl("", Validators.required);
    let AccessMenuId = new FormControl("");
    let Status = new FormControl("");

    this.rolemenuform = new FormGroup({
      AccessRoleId: AccessRoleId,
      AccessMenuId: AccessMenuId,
      Status: Status
    });
  }

  accesAddBtn: Boolean = true;
  ngOnInit() {

    //Untuk set filter terakhir hasil pencarian
    if (!(localStorage.getItem("filter") === null)) {
      var item = JSON.parse(localStorage.getItem("filter"));
      this.Name = item.name;
      this.getAccess(this.Name);
    }

    //get data role
    this.listAccess = "";
    this.getRoleList();

    // get data acces menu
    // this.getMenuList();
    this.getListofAccess();

    this.accesAddBtn = this.session.checkAccessButton("role/add-role");
  }

  selectedAccess(isChecked: boolean, value) {
    var split_url = value.split("#");
    var change_value: any;
    var string_access = [];
    var access: string;

    isChecked ? change_value = "11111" : change_value = "00000";

    for (let i = 0; i < split_url.length; i++) {
      access = "`" + split_url[i] + "` = '" + change_value + "'";
      string_access.push(access);
    }
    var updateMenu = { Kolom: string_access.join(",") }

    // console.log(updateMenu)

    this.service.httpCLientPut("api/UserLevel/"+this.userLevelId, updateMenu)
      .subscribe(res=>{
        console.log(res);
        this.getAccess(this.userLevelId);
      });
  }

  getAccess(value) {
    this.loading = true;
    this.userLevelId = value;
    var data: any;
    var roles: any;
    this.urlAccess = [];
    this.listAccess = "";

    this.service
      .httpClientGet("api/UserLevel/" + this.userLevelId, data)
      .subscribe(
        res => {
          data = res;
          // console.log(data);
          if (data != null) {
            this.service.httpClientGet("api/Language/RoleAccess", roles)
              .subscribe(res => {
                roles = res;
                this.listAccess = roles;
                // console.log(this.listAccess);
                for (let i = 0; i < this.listAccess.length; i++) {

                  //Get access value for read
                  if (this.listAccess[i].read_url != "xxx") {
                    var read_access = [];
                    var splitReadUrl = this.listAccess[i].read_url.split("#");
                    for (let j = 0; j < splitReadUrl.length; j++) {
                      read_access.push(data[splitReadUrl[j]]);
                    }
                    read_access.indexOf("00000") > -1 == true ? this.listAccess[i].read_value = "00000" : this.listAccess[i].read_value = "11111";
                  }

                  //Get access value for write
                  if (this.listAccess[i].write_url != "xxx") {
                    var write_access = [];
                    var splitWriteUrl = this.listAccess[i].write_url.split("#");
                    for (let k = 0; k < splitWriteUrl.length; k++) {
                      write_access.push(data[splitWriteUrl[k]]);
                    }
                    write_access.indexOf("00000") > -1 == true ? this.listAccess[i].write_value = "00000" : this.listAccess[i].write_value = "11111";
                  }

                  //Get access value for delete
                  if (this.listAccess[i].delete_url != "xxx") {
                    data[this.listAccess[i].delete_url] == "00000" ? this.listAccess[i].delete_value = "00000" : this.listAccess[i].delete_value = "11111";
                  }

                }
                this.loading = false;
                // console.log(this.listAccess);
              });
          }
        }, error => {
          this.service.errorserver();
          this.loading = false;
        }
      );

    //Buat object untuk filter yang dipilih
    var params =
      {
        name: this.Name
      }
    //Masukan object filter ke local storage
    localStorage.setItem("filter", JSON.stringify(params));
  }

  getListofAccess() {
    this.listKey = [];
    var data: any;
    this.service.httpClientGet("api/UserLevel/ListAccess", data).subscribe(
      result => {
        data = result;
        if (data.length != 0) {
          for (let key in data[0]) {
            if (key != "ID" && key != "UserLevelId" && key != "Name") {
              this.listKey.push(key);
            }
          }
        }
      },
      error => {
        this.service.errorserver();
      }
    );
  }

  //Output : Superadmin, Admin, Student, etc
  getRoleList() {
    this.loading = true;
    var data = "";
    this.service.httpClientGet("api/AccessRole", data).subscribe(
      (result:any) => {
        if (result == "Not found") {
          this.service.notfound();
          this.loading = false;
        } else {
          this.rolelist = result.sort((a,b)=>{
            if(a.Description > b.Description)
              return 1
            else if(a.Description < b.Description)
              return -1
            else return 0
          });
          this.loading = false;
        }
      },
      error => {
        this.service.errorserver();
        this.loading = false;
      }
    );
  }

  getMenuList() {
    var menu = "";
    this.service.httpClientGet("api/AccessMenu", menu).subscribe(
      result => {
        if (result == "Not found") {
          this.service.notfound();
        } else {
          this.menulist = result;
        }
      },
      error => {
        this.service.errorserver();
      }
    );
  }

  onChange(id: any, isChecked: boolean) {
    this.loading = true;
    if (isChecked) {
      this.selectedMenuId.push(id);
    } else {
      let index = this.selectedMenuId.indexOf(id);
      if (index > -1) {
        this.selectedMenuId.splice(index, 1);
      }
    }
  }

  onSubmit() {
    this.rolemenuform.controls["AccessRoleId"].markAsTouched();

    if (this.rolemenuform.valid) {
      this.rolemenuform.value.Status = "A";

      //convert object to json
      let data = JSON.stringify(this.rolemenuform.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
		 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});


      //redirect
      this.router.navigate(["/role/list-access"]);
    }
  }
}
