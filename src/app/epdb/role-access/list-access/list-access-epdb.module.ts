import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListAccessEPDBComponent } from './list-access-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { AppTranslateLanguage } from '../../../shared/translate-language/app.translateLanguage';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
// import { KeysPipe } from './list-access-epdb.component';

export const ListAccessEPDBRoutes: Routes = [
  {
    path: '',
    component: ListAccessEPDBComponent,
    data: {
      breadcrumb: 'epdb.role.list_access',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListAccessEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ListAccessEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class ListAccessEPDBModule { }