import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from "../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { CompleterService, CompleterData } from "ng2-completer";
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";

@Component({
  selector: 'app-aci-eim',
  templateUrl: './aci-eim.component.html',
  styleUrls: [
    './aci-eim.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ACIEIMComponent implements OnInit {

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  private productStatus;
  dropdownCountry = [];
  selectedItemsCountry = [];
  dropdownProduct = [];
  selectedItemsProduct = [];
  dropdownApplTerritory = [];
  selectedItemsAppTerr = [];

  dropdownSettings = {};
  protected submitBy: string;
  protected modifBy: string;
  protected rejBy: string;
  protected cName = [];
  protected dataService1: CompleterData;
  private dataACI;
  SubmissionDate: string;
  RejectedDate: string;
  ModifiedDate: string;
  productId;
  siteId;
  countryId;
  searchingValue: FormGroup;
  modelPopup1: NgbDateStruct;
  modelPopup2: NgbDateStruct;
  modelPopup3: NgbDateStruct;

  constructor(public http: Http, private service: AppService, private completerService: CompleterService, private parserFormatter: NgbDateParserFormatter) {
    this.dataService1 = completerService.local(this.cName, 'ContactName', 'ContactName');

    let Product = new FormControl('');
    let ApplicationStatus = new FormControl('');
    let ApplicantTerritory = new FormControl('');
    let ApplicantCountry = new FormControl('');
    let SubmittedOn = new FormControl('');
    let SubmittedBy = new FormControl('');
    let ModifOn = new FormControl('');
    let ModifBy = new FormControl('');
    let ApproveOrRejectOn = new FormControl('');
    let ApproveOrRejectBy = new FormControl('');

    this.searchingValue = new FormGroup({
      Product: Product,
      ApplicationStatus: ApplicationStatus,
      ApplicantTerritory: ApplicantTerritory,
      ApplicantCountry: ApplicantCountry,
      SubmittedOn: SubmittedOn,
      SubmittedBy: SubmittedBy,
      ModifOn: ModifOn,
      ModifBy: ModifBy,
      ApproveOrRejectOn: ApproveOrRejectOn,
      ApproveOrRejectBy: ApproveOrRejectBy
    });
  }

  resetForm() {
    this.searchingValue.reset({
      'ApplicationStatus': '',
      'ApplicantCountry': '',
      'SubmittedBy': '',
      'ModifBy': '',
    });
    this.selectedItemsProduct = [];
    this.selectedItemsAppTerr = [];
    this.selectedItemsCountry = [];
  }

  getCountryCode() {
    if (this.selectedItemsCountry.length != 0) {
      var country: any;
      this.service.httpClientGet("api/Countries2/" + this.selectedItemsCountry[0].id, country)
        .subscribe(res1 => {
          country = res1;
          if (Object.keys(country).length !== 0 && country.constructor === Object) {
            this.getContacts(country.CountryCode);
          } else {
            swal('Information!', 'Country code not found', 'error');
          }
        }, error => {
          this.service.errorserver();
        });
    }
  }

  getContacts(countryCode) {
    var contact: any;
    this.service.httpClientGet("api/MainContact/ContactAll/{'CountryCode':'" + countryCode + "'}", contact)
      .subscribe(res2 => {
        contact = res2;
        if (contact.length > 0) {
          for (let i = 0; i < contact.length; i++) {
            this.cName.push(contact[i]);
          }
        } else {
          this.cName = [];
        }
      }, error => {
        this.service.errorserver();
      });
  }

  getProduct() {
    var productTemp: any;
    var family: string = "ACI";
    this.service.httpClientGet("api/Product/FindProduct/where/{'familyId':'" + family + "'}", productTemp)
      .subscribe(result => {
        productTemp = result;
        if (productTemp.length != 0) {
          this.dropdownProduct = productTemp.map((item) => {
            return {
              id: item.productId,
              itemName: item.productName
            }
          });
        }
      }, error => { this.service.errorserver(); });
    this.selectedItemsProduct = [];
  }

  getProductStatus() {
    var status: any;
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'ProductStatus'}", status)
      .subscribe(res => {
        status = res;
        status.length != 0 ? this.productStatus = status : this.productStatus = "";
      }, error => {
        this.service.errorserver();
      });
  }

  getCountry() {
    var country: any;
    this.service.httpClientGet("api/Countries2", country)
      .subscribe(res => {
        country = res;
        if (country.length != 0) {
          this.dropdownCountry = country.map((item) => {
            return {
              id: item.CountryId,
              itemName: item.Country
            }
          });
        }
      }, error => {
        this.service.errorserver();
      });
    this.selectedItemsCountry = [];
  }

  getApplicantTerritory() {
    var data: any;
    this.service.httpClientGet("api/MainSite/ApplicantTerritory", data)
      .subscribe(res => {
        data = res;
        if (data.length != 0) {
          this.dropdownApplTerritory = data.map((item) => {
            return {
              id: item.SiteId,
              itemName: item.SiteId + " - " + item.OrgName + " - " + item.CountryCode
            }
          });
        }
      }, error => {
        this.service.errorserver();
      });
    this.selectedItemsAppTerr = [];
  }

  ngOnInit() {
    this.getProduct();
    this.getProductStatus();
    this.data = "";
    this.getCountry();
    this.getApplicantTerritory();

    //setting dropdown for Country
    this.dropdownSettings = {
      singleSelection: true,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      // limitSelection: 1,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true
    };
  }

  onSelectDateModifOn(date: NgbDateStruct) {
    if (date != null) {
      this.ModifiedDate = this.parserFormatter.format(date);
    }
  }

  onSelectRejectOn(date: NgbDateStruct) {
    if (date != null) {
      this.RejectedDate = this.parserFormatter.format(date);
    }
  }

  onSelectSubmitOn(date: NgbDateStruct) {
    if (date != null) {
      this.SubmissionDate = this.parserFormatter.format(date);
    }
  }

  findApp() {
    if (this.searchingValue.value.SubmittedOn != null) {
      this.SubmissionDate = this.parserFormatter.format(this.searchingValue.value.SubmittedOn);
    } else {
      this.SubmissionDate = "";
    }

    if (this.searchingValue.value.ModifOn != null) {
      this.ModifiedDate = this.parserFormatter.format(this.searchingValue.value.ModifOn);
    } else {
      this.ModifiedDate = "";
    }

    if (this.searchingValue.value.ApproveOrRejectOn != null) {
      this.RejectedDate = this.parserFormatter.format(this.searchingValue.value.ApproveOrRejectOn);
    } else {
      this.RejectedDate = "";
    }

    if (this.selectedItemsCountry.length != 0) {
      this.countryId = this.selectedItemsCountry[0].id;
    } else {
      this.countryId = "";
    }

    if (this.selectedItemsProduct.length != 0) {
      this.productId = this.selectedItemsProduct[0].id;
    } else {
      this.productId = "";
    }

    if (this.selectedItemsAppTerr.length != 0) {
      this.siteId = this.selectedItemsAppTerr[0].id;
    } else {
      this.siteId = "";
    }

    var hasil: any;
    this.service.httpClientGet("api/ContactAll/ACI/where/{'ProductId':'" + this.productId + "','SiteId':'" + this.siteId +
      "','SubmissionDate':'" + this.SubmissionDate + "','CountryId':'" + this.countryId + "','RejectedDate':'" + this.RejectedDate +
      "','ModifiedDate':'" + this.ModifiedDate + "'}", hasil)
      .subscribe(result => {
        hasil = result;
        if (hasil != null) {
          this.dataACI = hasil;
        } else {
          this.dataACI = '';
        }
      }, error => {
        this.service.errorserver();
      });
  }

  onCountrySelect(item: any) {
    this.getCountryCode();
  }

  OnCountryDeSelect(item: any) {
    this.getCountryCode();
  }

  onProductSelect(item: any) { }
  OnProductDeSelect(item: any) { }

  onApplTerrSelect(item: any) { }
  OnApplTerrDeSelect(item: any) { }

  onSubmitBy(item) {
    item != null ? console.log(item) : console.log("false");
  }

  onModifBy(item) {
    item != null ? console.log(item) : console.log("false");
  }

  onRejectBy(item) {
    item != null ? console.log(item) : console.log("false");
  }

}
