import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ACIEIMComponent } from './aci-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { Ng2CompleterModule } from "ng2-completer";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { FormsModule } from "@angular/forms";

export const ACIEIMRoutes: Routes = [
  {
    path: '',
    component: ACIEIMComponent,
    data: {
      breadcrumb: 'ACI Application Search',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ACIEIMRoutes),
    SharedModule,
    AngularMultiSelectModule,
    Ng2CompleterModule,
    FormsModule
  ],
  declarations: [ACIEIMComponent],
  providers: [AppService, AppFormatDate]
})
export class ACIEIMModule { }
