import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAcademicProgramsEPDBComponent } from './add-academic-programs-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddAcademicProgramsEPDBRoutes: Routes = [
  {
    path: '',
    component: AddAcademicProgramsEPDBComponent,
    data: {
      breadcrumb: 'Add Academic Program / Target',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddAcademicProgramsEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddAcademicProgramsEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class AddAcademicProgramsEPDBModule { }
