import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

@Component({
  selector: 'app-add-academic-programs-epdb',
  templateUrl: './add-academic-programs-epdb.component.html',
  styleUrls: [
    './add-academic-programs-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddAcademicProgramsEPDBComponent implements OnInit {

  private _serviceUrl = 'api/AcademicTargetProgram';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datasite: any;
  academicprogramform: FormGroup;
  public orgentriestype: any;
  public useraccesdata: any;
  public datayearcurrency: any;
  public programtype: any;
  public title;
  public loading = false;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let ProgramType = new FormControl('', Validators.required);
    let TargetEducator = new FormControl('', Validators.required);
    let TargetStudent = new FormControl('', Validators.required);
    let TargetInstitution = new FormControl('', Validators.required);
    let CommentNotes = new FormControl('');
    let FYIndicator = new FormControl('');

    this.academicprogramform = new FormGroup({
      ProgramType: ProgramType,
      TargetEducator: TargetEducator,
      TargetStudent: TargetStudent,
      TargetInstitution: TargetInstitution,
      Comment: CommentNotes,
      FYIndicator: FYIndicator
    });

  }

  id: string = "";
  AcademicType: string = "";
  ngOnInit() {

    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.AcademicType = params['AcademicType'] || '';
    });

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet('api/MainSite/' + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datasite = '';
        }
        else {
          this.datasite = result;
        }
      },
        error => {
          this.service.errorserver();
          this.datasite = '';
        });

    this.service.httpClientGet('api/Currency/where/{"Parent":"FYIndicator","Status":"A"}', data)
      .subscribe(result => {
        this.datayearcurrency = result;
      },
        error => {
          this.service.errorserver();
        });

    if (this.AcademicType == "AcademicPrograms") {
      this.title = "Academic Program";
      this.service.httpClientGet('api/Currency/where/{"Parent":"AcademicPrograms","Status":"A"}', data)
        .subscribe(result => {
          this.programtype = result;
        },
          error => {
            this.service.errorserver();
          });
    } else {
      this.title = "Academic Target";
      this.service.httpClientGet('api/Currency/where/{"Parent":"AcademicTargets","Status":"A"}', data)
        .subscribe(result => {
          this.programtype = result;
        },
          error => {
            this.service.errorserver();
          });
    }

  }

  go_Back_Bro() {
    this.router.navigate(['manage/site/detail-site/', this.id]);
  }

  //submit
  onSubmit() {

    this.academicprogramform.controls['ProgramType'].markAsTouched();
    this.academicprogramform.controls['TargetEducator'].markAsTouched();
    this.academicprogramform.controls['TargetStudent'].markAsTouched();
    this.academicprogramform.controls['TargetInstitution'].markAsTouched();
    //this.academicprogramform.controls['FYIndicator'].markAsTouched();

    if (this.academicprogramform.valid) {
      this.loading = true;
      this.academicprogramform.value.SiteId = this.datasite.SiteId
      this.academicprogramform.value.AddedBy = this.useraccesdata.ContactName;
      if (this.AcademicType == "AcademicPrograms") {
        this.academicprogramform.value.AcademicType = "AcademicPrograms";
      } else {
        this.academicprogramform.value.AcademicType = "AcademicTargets";
      }

      //convert object to json
      let data = JSON.stringify(this.academicprogramform.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data) 
	  .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

      //redirect
      this.router.navigate(['/manage/site/detail-site', this.datasite.SiteIdInt]);
      this.loading = false;
    }
  }

  resetForm(){
    this.academicprogramform.reset();
  }
}
