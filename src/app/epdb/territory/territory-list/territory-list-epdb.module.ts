import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryListEPDBComponent } from './territory-list-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const TerritoryListEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryListEPDBComponent,
    data: {
      breadcrumb: 'Territory List',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryListEPDBRoutes),
    SharedModule
  ],
  declarations: [TerritoryListEPDBComponent]
})
export class TerritoryListEPDBModule { }
