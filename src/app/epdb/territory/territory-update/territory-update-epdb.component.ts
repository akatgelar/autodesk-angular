import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';

@Component({
  selector: 'app-territory-update-epdb',
  templateUrl: './territory-update-epdb.component.html',
  styleUrls: [
    './territory-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class TerritoryUpdateEPDBComponent implements OnInit {

  //selected table
  rows = [];
  selected = [];

  constructor(public http: Http) { 
    //selected table
    this.fetch((data) => {
      this.rows = data;
    });
  }

  ngOnInit() {}

  //delete confirm
  openConfirmsSwal() {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      swal(
          'Deleted!',
          'Your file has been deleted.',
          'success'
      )
    }).catch(swal.noop);
  }

  //success notification
  openSuccessSwal() {
    swal({
      title: 'Success!',
      text: 'Territory has been changed!',
      type: 'success'
    }).catch(swal.noop);
  }

  //selected table
  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/activities.json`);
    req.onload = () => {
      cb(JSON.parse(req.response));
    };
    req.send();
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {}

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }

}