import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryUpdateEPDBComponent } from './territory-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const TerritoryUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryUpdateEPDBComponent,
    data: {
      breadcrumb: 'Update Territory',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryUpdateEPDBRoutes),
    SharedModule
  ],
  declarations: [TerritoryUpdateEPDBComponent]
})
export class TerritoryUpdateEPDBModule { }
