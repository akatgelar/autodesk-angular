import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';

@Component({
  selector: 'app-territory-add-epdb',
  templateUrl: './territory-add-epdb.component.html',
  styleUrls: [
    './territory-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class TerritoryAddEPDBComponent implements OnInit {

  addterritory: FormGroup;

  constructor() { 

    //validation
    let territoryname = new FormControl('', Validators.required);

    this.addterritory = new FormGroup({
      territoryname:territoryname
    });

  }

  ngOnInit() {
    setTimeout(() => {
      
  
    }, 1);
  }

  //submit form
  submitted: boolean;
  onSubmit() {
    this.submitted = true;
  }

  //success notification
  openSuccessSwal() {
    swal({
      title: 'Success!',
      text: 'Territory has been added!',
      type: 'success'
    }).catch(swal.noop);
  }

}