import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryAddEPDBComponent } from './territory-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

export const TerritoryAddEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryAddEPDBComponent,
    data: {
      breadcrumb: 'Add New Territory',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryAddEPDBRoutes),
    SharedModule
  ],
  declarations: [TerritoryAddEPDBComponent]
})
export class TerritoryAddEPDBModule { }
