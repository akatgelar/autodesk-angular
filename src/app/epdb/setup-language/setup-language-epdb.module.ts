import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupLanguageEPDBComponent } from './setup-language-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import { DataFilterActivitiesPipe } from './setup-language-epdb.component';

export const SetupLanguageEPDBRoutes: Routes = [
  {
    path: '',
    component: SetupLanguageEPDBComponent,
    data: {
      breadcrumb: 'Setup Language',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SetupLanguageEPDBRoutes),
    SharedModule
  ],
  declarations: [SetupLanguageEPDBComponent,DataFilterActivitiesPipe],
  providers:[AppService]
})
export class SetupLanguageEPDBModule { }
