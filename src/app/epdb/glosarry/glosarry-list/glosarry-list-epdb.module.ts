import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlosarryListEPDBComponent } from './glosarry-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterGlosarryPipe } from './glosarry-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const GlosarryListEPDBRoutes: Routes = [
  {
    path: '',
    component: GlosarryListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.glosarry.glosarry_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GlosarryListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [GlosarryListEPDBComponent, DataFilterGlosarryPipe],
  providers: [AppService, SessionService]
})
export class GlosarryListEPDBModule { }
