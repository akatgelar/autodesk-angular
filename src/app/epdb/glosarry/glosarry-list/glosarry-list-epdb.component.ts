import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterGlosarry' })
export class DataFilterGlosarryPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.glossary_term.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-glosarry-list-epdb',
  templateUrl: './glosarry-list-epdb.component.html',
  styleUrls: [
    './glosarry-list-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class GlosarryListEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Glossary';
  public data: any;
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "glossary_term";
  public sortOrder: string = "asc";
  useraccessdata: any;
  messageResult: string = '';
  messageError: string = '';
  public loading = false;

  constructor(public session: SessionService, private service: AppService) {
    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
  }

  accesAddBtn: Boolean = true;
  accesUpdateBtn: Boolean = true;
  accesDeleteBtn: Boolean = true;
  ngOnInit() {
    this.getGlossary();
    this.getGlossary();

    this.accesAddBtn = this.session.checkAccessButton("admin/glossary/glossary-add");
    this.accesUpdateBtn = this.session.checkAccessButton("admin/glossary/glossary-update");
    this.accesDeleteBtn = this.session.checkAccessButton("admin/glossary/glossary-delete");
  }

  getGlossary() {
    this.loading = true;
    //get data action
    var data = '';
    this.service.httpClientGet(this._serviceUrl, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = '';
          this.loading = false;
        }
        else {
          this.data = result;
          this.loading = false;
        }
      },
        error => {
          this.service.errorserver();
          this.data = '';
          this.loading = false;
        });
  }

  //view detail
  viewdetail(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.datadetail = '';
        } else {
          this.datadetail = result;
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
        });
  }

  //delete confirm
  openConfirmsSwal(id) {
    // this.loading = true;
    //     var cuid = this.useraccessdata.ContactName;
    //     var UserId = this.useraccessdata.UserId;
    //     var data = '';
    //     this.service.httpClientDelete(this._serviceUrl+ '/' +id+ '/' +cuid ,data, UserId, 0);
    //         this.getGlossary();
    //           setTimeout(function () {
    //             this.router.navigate(['/admin/glossary/glossary-list']);
    //             this.loading = false;
    //           }, 1000);
    // var index = this.data.findIndex(x => x.glossary_id == id);
    // setTimeout(() => {
    //   this.service.httpClientDelete(this._serviceUrl, this.data, id, index);      
    // }, 1000);
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result == true) {
        this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;

        var data = '';

        this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data)
          .subscribe(result => {
            let tmpData : any = result;
            this.messageResult = tmpData;
            var resource = result;
            if (resource['code'] == '1') {
              this.getGlossary();
              setTimeout(function () {
                this.router.navigate(['/admin/glossary/glossary-list']);
                this.loading = false;
              }, 1000);
            }
            else {
              swal(
                'Information!',
                "Delete Data Failed",
                'error'
              );
              this.loading = false;
            }
          },
            error => {
              this.messageError = <any>error
              this.service.errorserver();
              this.loading = false;
            });
      }
    }).catch(swal.noop);
  }
}