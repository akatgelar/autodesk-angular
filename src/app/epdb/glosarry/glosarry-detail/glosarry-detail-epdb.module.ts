import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlosarryDetailEPDBComponent } from './glosarry-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const GlosarryDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: GlosarryDetailEPDBComponent,
    data: {
      breadcrumb: 'Detail Glosarry',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GlosarryDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [GlosarryDetailEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class GlosarryDetailEPDBModule { }
