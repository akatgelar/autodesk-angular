import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-glosarry-detail-epdb',
  templateUrl: './glosarry-detail-epdb.component.html',
  styleUrls: [
    './glosarry-detail-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
    ],
  encapsulation: ViewEncapsulation.None
})

export class GlosarryDetailEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Glossary';
  public data: any;
  public datadetail:any;
  updateterm: FormGroup;

  constructor(private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute, private router: Router) {

    let glossary_id = new FormControl('');
    let glossary_term = new FormControl('');
    let description = new FormControl('');

    this.updateterm = new FormGroup({
      glossary_id:glossary_id,
      glossary_term:glossary_term,
      description:description
    });

  }

  id:string;
  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl+"/"+this.id,data)
      .subscribe(result => {
        if(result=="Not found"){
            this.service.notfound();
            this.datadetail = '';
        }
        else{
            this.datadetail = result;
        }
      },
      error => {
        this.service.errorserver();
        this.data = '';
      });

  }

}
