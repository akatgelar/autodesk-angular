import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-glosarry-update-epdb',
  templateUrl: './glosarry-update-epdb.component.html',
  styleUrls: [
    './glosarry-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class GlosarryUpdateEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Glossary';
  public data: any;
  updateterm: FormGroup;
  public loading = false;
  public useraccesdata: any;

  constructor(private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let glossary_id = new FormControl('');
    let glossary_term = new FormControl('');
    let description = new FormControl('');

    this.updateterm = new FormGroup({
      glossary_id: glossary_id,
      glossary_term: glossary_term,
      description: description
    });

  }

  id: string;
  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = '';
        }
        else {
          this.data = result;
          this.buildForm();
        }
      },
        error => {
          this.service.errorserver();
          this.data = '';
        });

  }

  //build form update
  buildForm(): void {

    let glossary_id = new FormControl(this.data.glossary_id);
    let glossary_term = new FormControl(this.data.glossary_term, Validators.required);
    let description = new FormControl(this.data.description, Validators.required);

    this.updateterm = new FormGroup({
      glossary_id: glossary_id,
      glossary_term: glossary_term,
      description: description
    });
  }

  //submit form
  submitted: boolean;
  onSubmit() {

    this.updateterm.controls['glossary_term'].markAsTouched();
    this.updateterm.controls['description'].markAsTouched();

    if (this.updateterm.valid) {
      this.loading = true;
      this.updateterm.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.updateterm.value.mdate = this.formatdate.dateJStoYMD(new Date());
      this.updateterm.value.cuid = this.useraccesdata.ContactName;
      this.updateterm.value.muid = this.useraccesdata.ContactName;
      this.updateterm.value.UserId = this.useraccesdata.UserId;
      //convert data form to json
      let data = JSON.stringify(this.updateterm.value);

      //update action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
      setTimeout(() => {
        // redirect
        this.router.navigate(['/admin/glossary/glossary-list']);
        this.loading = false;
      }, 1000);
      // this.service.httpCLientPut(this._serviceUrl,this.id, data);
       
      // setTimeout(() => {
      //   // redirect
      //   this.router.navigate(['/admin/glossary/glossary-list']);
      //   this.loading = false;
      // }, 1000);
    }

    // if (this.updateterm.valid) {
    //   swal({
    //     title: 'Are you sure?',
    //     text: "If you wish to Update the record!",
    //     type: 'warning',
    //     showCancelButton: true,
    //     confirmButtonColor: '#3085d6',
    //     cancelButtonColor: '#d33',
    //     confirmButtonText: 'Yes, sure!'
    //   }).then(result => {
    //     if (result == true) {
    //       this.loading = true;
    //       this.updateterm.value.cdate = this.formatdate.dateJStoYMD(new Date());
    //       this.updateterm.value.mdate = this.formatdate.dateJStoYMD(new Date());
    //       //convert data form to json
    //       let data = JSON.stringify(this.updateterm.value);

    //       //update action
    //       this.service.httpCLientPu(this._serviceUrl, this.id, data);
    //       setTimeout(() => {
    //         // redirect
    //         this.router.navigate(['/admin/glossary/glossary-list']);
    //         this.loading = false;
    //       }, 1000)
    //     }
    //   }).catch(swal.noop);
    // }
    // else {
    //   swal(
    //     'Field is Required!',
    //     'Please enter form is required :)',
    //     'error'
    //   )
    // }
  }
  resetForm() {
    this.updateterm.reset({
      'glossary_term': this.data.glossary_term,
      'description': this.data.description
    });
  }
}
