import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-glosarry-add-epdb',
  templateUrl: './glosarry-add-epdb.component.html',
  styleUrls: [
    './glosarry-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class GlosarryAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Glossary';
  messageResult: string = '';
  messageError: string = '';
  addterm: FormGroup;
  public loading = false;
  public useraccesdata: any;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let glossary_term = new FormControl('', Validators.required);
    let description = new FormControl('', Validators.required);

    this.addterm = new FormGroup({
      glossary_term: glossary_term,
      description: description
    });

  }

  ngOnInit() { }

  //submit form
  submitted: boolean;
  onSubmit() {

    this.addterm.controls['glossary_term'].markAsTouched();
    this.addterm.controls['description'].markAsTouched();
    let format = /[ !@#$%^&*+\-=\[\]{};':\\|,.<>\/?]/;
    if(format.test(this.addterm.value.description))
      return swal('ERROR','Special character not allowed in description','error')
    let temp_string = this.addterm.value.description
    for(let i = 0; i<temp_string.length; i++){
      let k = temp_string.charAt(i)

    }
    this.submitted = true;

    if (this.addterm.valid) {
      this.loading = true;
      this.addterm.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.addterm.value.mdate = this.formatdate.dateJStoYMD(new Date());
      // this.addterm.value.cuid = "admin";
      this.addterm.value.muid = this.useraccesdata.ContactName;
      
      this.addterm.value.cuid = this.useraccesdata.ContactName;
      this.addterm.value.UserId = this.useraccesdata.UserId;

      //convert object to json
      let data = JSON.stringify(this.addterm.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/glossary/glossary-list']);
        this.loading = false;
      }, 1000);
    }

    // if (this.addterm.valid) {
    //   swal({
    //     title: 'Are you sure?',
    //     text: "If you wish to Add the record!",
    //     type: 'warning',
    //     showCancelButton: true,
    //     confirmButtonColor: '#3085d6',
    //     cancelButtonColor: '#d33',
    //     confirmButtonText: 'Yes, sure!'
    //   }).then(result => {
    //     if (result == true) {
    //       this.loading = true;
    //       this.addterm.value.cdate = this.formatdate.dateJStoYMD(new Date());
    //       this.addterm.value.mdate = this.formatdate.dateJStoYMD(new Date());
    //       this.addterm.value.cuid = "admin";
    //       this.addterm.value.muid = "admin";

    //       //convert object to json
    //       let data = JSON.stringify(this.addterm.value);

    //       //post action
    //       this.service.httpClientPos(this._serviceUrl, data);
    //       setTimeout(() => {
    //         //redirect
    //         this.router.navigate(['/admin/glossary/glossary-list']);
    //         this.loading = false;
    //       }, 1000);
    //     }
    //   }).catch(swal.noop);
    // }
    // else {
    //   swal(
    //     'Field is Required!',
    //     'Please enter form is required :)',
    //     'error'
    //   )
    // }
  }

  resetForm() {
    this.addterm.reset();
  }
}
