import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgreementComponent } from './agreement.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AapAgreementTemplateComponent } from './aap-agreement-template/aap-agreement-template.component';
import { AtcAgreementTemplateComponent } from './atc-agreement-template/atc-agreement-template.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ExecuteComponent } from './execute/execute.component';
import { ApproveComponent } from './approve/approve.component';
import { LoadingModule } from 'ngx-loading';

export const SKUEPDBRoutes: Routes = [
  {
    path: '',
    component: AgreementComponent,
    data: {
      breadcrumb: 'Agreement List',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: 'execute/:id/:partnerType',
    component: ExecuteComponent,
    data: {
      breadcrumb: 'Agreement Singature Process',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: 'approve/:id/:partnerType',
    component: ApproveComponent,
    data: {
      breadcrumb: 'Agreement Singature Process',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SKUEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [AgreementComponent, AapAgreementTemplateComponent, AtcAgreementTemplateComponent, ExecuteComponent, ApproveComponent],
  providers:[AppService]
})

export class AgreementModule { }
