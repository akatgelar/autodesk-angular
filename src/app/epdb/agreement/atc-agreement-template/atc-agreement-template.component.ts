import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-atc-agreement-template',
  templateUrl: './atc-agreement-template.component.html',
  styleUrls: ['./atc-agreement-template.component.css']
})
export class AtcAgreementTemplateComponent implements OnInit {

  @Input('data') data
  constructor() { }

  ngOnInit() {
  }

}
