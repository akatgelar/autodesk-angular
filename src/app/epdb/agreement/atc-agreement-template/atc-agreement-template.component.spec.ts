import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtcAgreementTemplateComponent } from './atc-agreement-template.component';

describe('AtcAgreementTemplateComponent', () => {
  let component: AtcAgreementTemplateComponent;
  let fixture: ComponentFixture<AtcAgreementTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtcAgreementTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtcAgreementTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
