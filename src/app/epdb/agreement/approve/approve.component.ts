import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import * as kendo from '@progress/kendo-drawing';
import { AppService } from "../../../shared/service/app.service";
declare const $: any;

@Component({
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: ['./approve.component.css']
})
export class ApproveComponent implements OnInit {

  data = {
    remarkHistories:[],
    orgId: '<<OrgID#>>',
    legalName: ' «Company Name»',
    address: '«Add1» «Add2» «Add3» «City» «Region» «PostalCode» «Country»',
    submissionPerson: '<<Executed By>>',
    title: '<<Title>>',
    partnerType:""
  }
  constructor(private router: ActivatedRoute, private service:AppService) { }

  exportOptions = {
    margin: {
      top: '1in',
      right: '0.8in',
      bottom: '1in',
      left: '0.8in',
    },
    paperSize: 'a4',
    multiPage: true,
    forcePageBreak: '.page-break',
    scale: 0.65
  };

  form = {
    AgreementMasterId:null,
    UserId:null,
    ExportFile:null
  }
  remark = ""
  partnerType = ""
  isSuperAdmin = false
  isOrgAdmin = false

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('autodesk-data'))
    this.isSuperAdmin = user.UserLevelId.indexOf('SUPERADMIN')>-1?true:false
    this.isOrgAdmin = user.UserLevelId.indexOf('ORGANIZATION')>-1?true:false
    this.form.UserId = user.UserId
    this.router.params.subscribe((params)=>{
      let id = params['id']
      this.partnerType = params['partnerType']
      this.form.AgreementMasterId = id
      this.service.httpClientGet('api/Agreements/GetAgreementDetail?agreementMasterId='+id,{}).subscribe((res: any)=>{
        this.data = res.data;
      })
    })
  }

  approve(){
    let isATC = this.partnerType.indexOf('ATC')>-1
    let form = {...this.form,ExportFile: isATC?document.getElementById('ATC').innerHTML:document.getElementById('AAP').innerHTML }
    this.service.httpClientPost('api/agreements/ApproveAgreement',JSON.stringify(form)).subscribe((res: any)=>{
      let json = res;
      if(json.code == 200)
        swal('SUCCESS','Approved by Autodesk','success').then(()=>{
          window.history.back();
        })
      else
        swal('ERROR',json.message,'error')
    })
  }

  export(){
    this.exportAAP()
    this.exportATC()
  }

  exportATC = () => {
    var doc = $(".atc-agreement-content");

    if(!!doc && !!doc[0]) {
      kendo.drawDOM(doc[0], this.exportOptions).then(root => {
        kendo.pdf.saveAs(root, "DRAFT Blue-Cube Autodesk Authorized Training Center (ATC) Agreement FY20 4836-8884-0781, 1 MMB.pdf");
      });
    }
  }

  exportAAP = () => {
    var doc = $(".aap-agreement-content");

    if(!!doc && !!doc[0]) {
      kendo.drawDOM(doc[0], this.exportOptions).then(root => {
        kendo.pdf.saveAs(root, "DRAFT Blue-Cube Autodesk Authorized Academic Partner (AAP) Agreement FY20 4849-3995-2974, 1 MMB.pdf");
      });
    }
  }

  resubmit(){
    let form = {...this.form, Remark:this.remark}
    this.service.httpClientPost('api/agreements/RejectAgreement',JSON.stringify(form)).subscribe((res: any)=>{
      if(res.code==200)
        swal('SUCCESS','Agreement Resubmited','success').then(()=>{
          window.history.back();
        })
      else
        swal('ERROR',res.message,'error')
    })
    
  }

}
