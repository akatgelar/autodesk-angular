import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";

@Component({
  selector: 'app-execute',
  templateUrl: './execute.component.html',
  styleUrls: ['./execute.component.css']
})
export class ExecuteComponent implements OnInit {

  step = 1;
  partnerType = ""
  data:any = {}
  detail:any = {}
  agree2 = false;
  agree3 = false;
  form = {
    AgreementMasterId:"",
    OrgId: "",
    JobTitle:"",
    FirstName:"",
	  LastName:"",
    CheckTC:false,
    UserId:""
  }
  constructor(private router: ActivatedRoute, private service:AppService) { }

  ngOnInit() {
    let user_data = JSON.parse(localStorage.getItem('autodesk-data'));
    this.form.UserId = user_data.UserId
    this.router.params.subscribe((params)=>{
      let id = params['id']
      this.partnerType = params['partnerType']
      this.form.AgreementMasterId = id
      this.service.httpClientGet('api/Agreements/OrganizationInfo?agreementMasterId='+id,{}).subscribe((res: any)=>{
        this.data = res.data
        if(res.status == 'Approved' || res.status=='Pending')
          swal('Error','This agreement is already Executed','error').then(()=>{
            window.history.back();
          })
      })
      this.service.httpClientGet('api/Agreements/GetAgreementDetail?agreementMasterId='+id,{}).subscribe((res: any)=>{
        this.form.OrgId = res.data.orgId
        this.detail = res.data
      })
    })
  }

  validate(){
    let valid = true
    if(!this.form.JobTitle || !this.form.FirstName || !this.form.LastName){
        swal('ERROR','Please fill all field','error')
        valid = false
    }
    else if(!this.form.CheckTC){
        swal('ERROR','You did not check the checkbox indicating that you accept the Agreement','error')
        valid = false
    }
    else if(!this.agree2){
      swal('ERROR','You did not check the checkbox indicating that you reviewed the Program Guide yet','error')
      valid = false
    }
    return valid
  }

  confirm(){
    if(this.validate())
      this.step = 2
  }

  execute(){
    if(this.validate()){
      if(!this.agree3)
        swal('ERROR','You did not check the checkbox indicating that you accept the Agreement','error')
      else
        this.service.httpClientPost('api/Agreements/ExecuteAgreement',this.form).subscribe((res: any)=>{
          let json = res;
          if(json.code!=200)
            swal('Error',json.message,'error')
          else
            swal('SUCCESS','Agreement Signed','success').then(()=>{
              window.history.back();
            })
        },(err)=>{
          swal('Error',err.message,'error')
        })
    }
  }

}
