import { Component, OnInit } from '@angular/core';
import * as kendo from '@progress/kendo-drawing';
import { AppService } from "../../shared/service/app.service";
declare const $: any;
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.css']
})
export class AgreementComponent implements OnInit {

  loading = false;
  orgName = '';
  data = []
  currentData = {};
  fys = []
  partners = []
  territories = []
  countries = []
  statuses = []
  geos = []
  filter = {
    "FY":"",
    "Organization": "",
    "OrgId": "",
    "OrgName": "",
    "RegisteredStateProvince": "",
    "RegisteredCountryCode": [],
    "PartnerType": "",
    "PartnerTypeStatus": [],
    "Geo": [],
    "Territory": [],
    "UserId":""
  }
  dropdownSettings = {
    singleSelection: false,
    text: "Please Select",
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    enableSearchFilter: true,
    classes: "myclass custom-class",
    disabled: false,
    maxHeight: 120,
    badgeShowLimit: 5
  };
  exportOptions = {
    margin: {
      top: '1in',
      right: '0.8in',
      bottom: '1in',
      left: '0.8in',
    },
    paperSize: 'a4',
    multiPage: true,
    forcePageBreak: '.page-break',
    scale: 0.65
  };
  temp = {
    remarkHistories:[],
    orgId: '<<OrgID#>>',
    legalName: ' «Company Name»',
    address: '«Add1» «Add2» «Add3» «City» «Region» «PostalCode» «Country»',
    submissionPerson: '<<Executed By>>',
    title: '<<Title>>',
    partnerType:""
  }

  isSuperAdmin = false
  constructor(private service: AppService) { }

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('autodesk-data'))
    this.isSuperAdmin = user.UserLevelId.indexOf('SUPERADMIN')>-1?true:false
    this.filter.UserId = user.UserId
    if(!this.isSuperAdmin){
      let orgId = JSON.parse(localStorage.getItem('autodesk-data')).OrgId
      if(orgId[0]==',')
        orgId = orgId.slice(1)
      let userId = JSON.parse(localStorage.getItem('autodesk-data')).UserId
      this.service.httpClientGet('api/Agreements/GetOrgName?orgId='+orgId+'&userId='+userId,{}).subscribe((res:any)=>{
        this.orgName = res.orgName
      })
    }
    this.service.httpClientGet('api/Roles/Status',{}).subscribe((res:any)=>{
      this.statuses = res.map((item)=>{
        return {
          id: item.Key,
          itemName: item.KeyValue
        }
      })
      this.filter.PartnerTypeStatus = res.map((item)=>{
        return {
          id: item.Key,
          itemName: item.KeyValue
        }
      })
    })
    this.service.httpClientGet('api/Geo',{}).subscribe((res:any)=>{
      this.geos = res.map((item)=>{
        return {
          id: item.geo_code,
          itemName: item.geo_name
        }
      })
      this.filter.Geo = res.map((item)=>{
        return {
          id: item.geo_code,
          itemName: item.geo_name
        }
      })
    })
    this.service.httpClientGet('api/agreements/GetListPartnerType',{}).subscribe((res:any)=>{
      this.partners = res.filter((el)=>{
        return el.RoleName.indexOf('ATC') >-1 || el.RoleName.indexOf('AAP') >-1
      })
    })
    this.service.httpClientGet('api/Territory',{}).subscribe((res:any)=>{
      this.territories = res.map((item)=>{
        return {
          id: item.TerritoryId,
          itemName: item.Territory_Name
        }
      })
      this.filter.Territory = res.map((item)=>{
        return {
          id: item.TerritoryId,
          itemName: item.Territory_Name
        }
      })
    })
    this.service.httpClientGet('api/Agreements/GetListFY',{}).subscribe((res:any)=>{
      this.fys = res
    })
    setTimeout(()=>{
      this.searchCountry()
    },2000)
  }

  searchCountry(){
    let geo_ids:any = this.filter.Geo.map((item)=> '"'+item.id+'"')
    geo_ids = geo_ids.length?geo_ids.toString():''
    let ter_ids:any = this.filter.Territory.map((item)=>'"'+item.id+'"')
    ter_ids = ter_ids.length?ter_ids.toString():''
    var keyword : any = {
      CtmpGeo:geo_ids,
      CtmpTerritory:ter_ids
     
  };
  keyword = JSON.stringify(keyword);
    // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + geo_ids + "/" + ter_ids,{}).subscribe((res:any)=>{
      this.service.httpClientPost("api/Countries/filterByGeoByTerritory",keyword).subscribe((res:any)=>{
      this.countries = res.map((item)=>{
        return {
          id: item.countries_code,
          itemName: item.countries_name
        }
      })
      this.filter.RegisteredCountryCode = []
    })
  }

  search(){
    this.loading = true
    let process_filter = {...this.filter}
    process_filter.RegisteredCountryCode = process_filter.RegisteredCountryCode.map((item)=>item.id)
    process_filter.PartnerTypeStatus = process_filter.PartnerTypeStatus.map((item)=>item.id)
    process_filter.Geo = process_filter.Geo.map((item)=>item.id)
    process_filter.Territory = process_filter.Territory.map((item)=>item.id)
    this.service.httpClientPost('api/agreements/GetListMaster',process_filter).subscribe((res:any)=>{
      this.data = res.data
      this.loading = false
    })
  }

  exportATC = () => {
    var doc = $(".atc-agreement-content");

    if(!!doc && !!doc[0]) {
      kendo.drawDOM(doc[0], this.exportOptions).then(root => {
        kendo.pdf.saveAs(root, "DRAFT Blue-Cube Autodesk Authorized Training Center (ATC) Agreement FY20 4836-8884-0781, 1 MMB.pdf");
        this.temp = {
          remarkHistories:[],
          orgId: '<<OrgID#>>',
          legalName: ' «Company Name»',
          address: '«Add1» «Add2» «Add3» «City» «Region» «PostalCode» «Country»',
          submissionPerson: '<<Executed By>>',
          title: '<<Title>>',
          partnerType:""
        }
      });
    }
  }

  exportAAP = () => {
    var doc = $(".aap-agreement-content");

    if(!!doc && !!doc[0]) {
      kendo.drawDOM(doc[0], this.exportOptions).then(root => {
        kendo.pdf.saveAs(root, "DRAFT Blue-Cube Autodesk Authorized Academic Partner (AAP) Agreement FY20 4849-3995-2974, 1 MMB.pdf");
        this.temp = {
          remarkHistories:[],
          orgId: '<<OrgID#>>',
          legalName: ' «Company Name»',
          address: '«Add1» «Add2» «Add3» «City» «Region» «PostalCode» «Country»',
          submissionPerson: '<<Executed By>>',
          title: '<<Title>>',
          partnerType:""
        }
      });
    }
  }

  export(partnerType,d){
    if(d.status=='Approved')
      this.service.httpClientGet('api/Agreements/GetAgreementDetail?agreementMasterId='+d.agreementMasterId,{}).subscribe((res: any)=>{
        this.temp = res.data;
        setTimeout(()=>{
          if(partnerType.indexOf('ATC')>-1)
          this.exportATC()
          if(partnerType.indexOf('AAP')>-1)
            this.exportAAP()
        },500)
      })
    else{
      if(partnerType.indexOf('ATC')>-1)
        this.exportATC()
      if(partnerType.indexOf('AAP')>-1)
        this.exportAAP()
    }
  }

  ExportExcel() {  
    var fileName = "feemanagement.xls";
    var ws = XLSX.utils.json_to_sheet(this.data.map((d)=>{
      let type = ''
      if(d.partnerType.indexOf('ATC')>-1)
        type = 'Authorized Training Center (ATC)'
      if(d.partnerType.indexOf('AAP')>-1)
        type = 'Authorized Academic Partner (AAP)'
      return {
        'Program Year':d.fyindicatorKey,
        'Territory':d.territoryName,
        'OrgID':d.orgId,
        'Org Name':d.orgName,
        'Program Type': type,
        'Status':d.status,
        'Approved by':d.approveBy
      }
    }));
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Org Info');
    XLSX.writeFile(wb, fileName);
  }
  
}
