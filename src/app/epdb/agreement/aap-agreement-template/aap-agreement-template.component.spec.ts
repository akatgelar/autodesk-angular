import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AapAgreementTemplateComponent } from './aap-agreement-template.component';

describe('AapAgreementTemplateComponent', () => {
  let component: AapAgreementTemplateComponent;
  let fixture: ComponentFixture<AapAgreementTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AapAgreementTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AapAgreementTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
