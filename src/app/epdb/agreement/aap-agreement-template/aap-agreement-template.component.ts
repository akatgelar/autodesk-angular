import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-aap-agreement-template',
  templateUrl: './aap-agreement-template.component.html',
  styleUrls: ['./aap-agreement-template.component.css']
})
export class AapAgreementTemplateComponent implements OnInit {

  @Input('data') data
  constructor() { }

  ngOnInit() {
  }

}
