import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowAttachmentsReportEPDBComponent } from './show-attachments-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ShowAttachmentsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: ShowAttachmentsReportEPDBComponent,
    data: {
      breadcrumb: 'Show Attachments Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ShowAttachmentsReportEPDBRoutes),
    SharedModule
  ],
  declarations: [ShowAttachmentsReportEPDBComponent]
})
export class ShowAttachmentsReportEPDBModule { }
