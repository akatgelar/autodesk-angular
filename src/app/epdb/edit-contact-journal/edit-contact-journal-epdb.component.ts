import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-edit-contact-site-journal-epdb',
    templateUrl: './edit-contact-journal-epdb.component.html',
    styleUrls: [
        './edit-contact-journal-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class EditContactJournalEPDBComponent implements OnInit {
    private SiteId;
    private ContactId;
    public journal;
    private activityDate;
    private useraccessdata;
    editContactJournal: FormGroup;
    modelPopup1: NgbDateStruct;
    id: string = "";
    private _serviceUrl = "api/OrganizationJournalEntries";
    public datadetail: any;
    public loading = false;

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate,
      private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        let ActivityId = new FormControl('');
        let Notes = new FormControl('');
        let ActivityDate = new FormControl('');

        this.editContactJournal = new FormGroup({
            ActivityId: ActivityId,
            Notes: Notes,
            ActivityDate: ActivityDate
        });
    }

    getJournalActivities(ContactId) {
        this.id = this.route.snapshot.params['id'];
        this.service.httpClientGet(this._serviceUrl + '/' + this.id, '')
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.datadetail = '';
                }
                else {
                    this.datadetail = result;
                    if (this.datadetail != null) {
                      if (this.datadetail.Notes != null && this.datadetail.Notes != "") {
                        this.datadetail.Notes = this.datadetail.Notes.replace("&#10;", " ");
                        this.datadetail.Notes = this.service.decoder(this.datadetail.Notes);
                      } 
                    }
                    this.buildForm();
                }
            },
                error => {
                    this.service.errorserver();
                    this.datadetail = '';
                });

        var data: any;
        var activityType = "Contact Journal Entry";
        this.service.httpClientGet("api/MainSite/JournalActivities/" + ContactId + "/" + activityType, data)
            .subscribe(res => {
                data = res;
                data.length == 0 ? this.journal = [] : this.journal = data;
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.activityDate = this.parserFormatter.format(date);
        } else {
            this.activityDate = "";
        }
    }

    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || '';
            this.ContactId = params['ContactId'] || 0;
        });
        this.getJournalActivities(this.SiteId);
    }

    onSubmit() {

        this.editContactJournal.controls["ActivityId"].markAsTouched();
        this.editContactJournal.controls["ActivityDate"].markAsTouched();
        this.editContactJournal.controls["Notes"].markAsTouched();
var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.editContactJournal.valid) {
            this.loading = true;
            var dataJournal = {
                'ParentId': this.datadetail.ParentId,
                 'DateLastAdmin': dateFormat,
				//this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                'LastAdminBy': this.useraccessdata.ContactName,
                'ActivityId': this.editContactJournal.value.ActivityId,
                'ActivityDate': this.formatdate.dateCalendarToYMD(this.editContactJournal.value.ActivityDate),
                'Notes': this.editContactJournal.value.Notes,//this.htmlEntityService.encoder(this.editContactJournal.value.Notes),
                'History':'Contact Journal Entries',
                'cuid':this.useraccessdata.ContactName,
                'UserId':this.useraccessdata.UserId
            };
            this.service.httpCLientPut(this._serviceUrl+'/'+this.id, dataJournal)
                .subscribe(res=>{
                    
                });
            setTimeout(() => {
                this.router.navigate(['manage/contact/detail-contact/', this.ContactId, this.SiteId]);
                this.loading = false;
            }, 1000)
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/contact/detail-contact/', this.ContactId, this.SiteId]);
    }

    resetForm() {
        this.editContactJournal.reset({
            'ActivityId': this.datadetail.ActivityId,
            'ActivityDate': this.datadetail.ActivityDate,
            'Notes': this.datadetail.Notes
        });
    }

    buildForm(): void {

        var activityDateTemp = (this.datadetail.ActivityDate).substr(0, 10).replace(/\b0/g, '');
        var activityDate = activityDateTemp.split('/');
        this.modelPopup1 = {
            "year": parseInt(activityDate[2]),
            "month": parseInt(activityDate[1]),
            "day": parseInt(activityDate[0])
        };

        // var ActivityDateConvert = this.formatdate.dateYMDToCalendar(this.datadetail.ActivityDate);

        let ActivityId = new FormControl(this.datadetail.ActivityId, Validators.required);
        let Notes = new FormControl(this.datadetail.Notes, Validators.required);
        let ActivityDate = new FormControl(activityDate, Validators.required);

        this.editContactJournal = new FormGroup({
            ActivityId: ActivityId,
            Notes: Notes,
            ActivityDate: ActivityDate
        });
    }
}
