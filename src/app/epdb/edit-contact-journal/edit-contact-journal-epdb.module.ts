import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditContactJournalEPDBComponent } from './edit-contact-journal-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const EditContactJournalEPDBRoutes: Routes = [
    {
        path: '',
        component: EditContactJournalEPDBComponent,
        data: {
            breadcrumb: 'epdb.manage.contact.partner_type_spec.edit_contact_journal_entries',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(EditContactJournalEPDBRoutes),
        SharedModule,
        // AngularMultiSelectModule
        LoadingModule
    ],
    declarations: [EditContactJournalEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class EditContactJournalEPDBModule { }
