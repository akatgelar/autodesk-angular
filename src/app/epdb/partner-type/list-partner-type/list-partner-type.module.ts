import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPartnerTypeEPDBComponent } from './list-partner-type.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterPartnerTypePipe } from './list-partner-type.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const ListPartnerTypeEPDBRoutes: Routes = [
  {
    path: '',
    component: ListPartnerTypeEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.partner_type.list_partner_type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListPartnerTypeEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ListPartnerTypeEPDBComponent, DataFilterPartnerTypePipe],
  providers: [AppService, SessionService]
})
export class ListPartnertTypeEPDBModule { }
