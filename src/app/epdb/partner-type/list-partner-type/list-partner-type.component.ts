import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import {AppService} from "../../../shared/service/app.service";
import {FormGroup, FormControl, Validators, FormArray} from "@angular/forms";
import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterPartnerType' })
export class DataFilterPartnerTypePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
                (row.RoleName.toLowerCase().indexOf(query.toLowerCase()) > -1));
        } 
        return array;
    }
}

@Component({
  selector: 'app-list-partner-type',
  templateUrl: './list-partner-type.component.html',
  styleUrls: [
    './list-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ListPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Roles';
  messageResult: string = '';
  messageError: string = ''; 
  useraccessdata: any;
  public data: any;
  public datadetail: any;
  public subpartnertype: any;
  public certificatetype: any;
  public datadetailroleparams:any;
  public datadetailrolecertificate:any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "RoleName";
  public sortOrder: string = "asc";
  addpartner: FormGroup;
  editpartner:FormGroup;
  addroleparams:FormGroup;
  addrolecertificate:FormGroup;
  CertificateArr=[];
  private loading = false;

  constructor(public session: SessionService, private service:AppService) { 

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);

    // //validation
    // let RoleCode = new FormControl('', Validators.required);
    // let RoleName = new FormControl('', Validators.required);
    // let SiebelName = new FormControl('');
    // let FrameworkName = new FormControl('');
    // let CRBApprovedName = new FormControl('');
    // let Description = new FormControl('', Validators.required);
    // let RoleType = new FormControl('');
    // let RoleSubType = new FormControl('', Validators.required);
    // let Certificate = new FormControl('');
    // let CertificateType = new FormControl('');
    // let CertificateName = new FormControl('');
    // let Status = new FormControl('');
    // let ParamName = new FormControl('');
    // let ParamValue = new FormControl('');

    // this.addpartner = new FormGroup({
    //   RoleCode:RoleCode,
    //   RoleName:RoleName,
    //   SiebelName:SiebelName,
    //   FrameworkName:FrameworkName,
    //   CRBApprovedName:CRBApprovedName,
    //   Description:Description,
    //   RoleType:RoleType,
    //   RoleSubType:RoleSubType
    // });

    // this.addrolecertificate = new FormGroup({
    //   RoleCode:RoleCode,
    //   CertificateType:CertificateType,
    //   CertificateName:CertificateName,
    //   Status:Status
    // });

    // this.addroleparams = new FormGroup({
    //   RoleCode:RoleCode,
    //   ParamName:ParamName,
    //   ParamValue:ParamValue,
    //   Status:Status
    // });

    // let RoleCode_update = new FormControl();
    // let RoleName_update = new FormControl();
    // let SiebelName_update = new FormControl();
    // let FrameworkName_update = new FormControl();
    // let CRBApprovedName_update = new FormControl();
    // let Description_update = new FormControl();
    // let RoleType_update = new FormControl();
    // let RoleSubType_update = new FormControl();

    // this.editpartner = new FormGroup({
    //   RoleCode:RoleCode_update,
    //   RoleName:RoleName_update,
    //   SiebelName:SiebelName_update,
    //   FrameworkName:FrameworkName_update,
    //   CRBApprovedName:CRBApprovedName_update,
    //   Description:Description_update,
    //   RoleType:RoleType_update,
    //   RoleSubType:RoleSubType_update
    // });

  }

  accesAddBtn:Boolean=true;
  accesUpdateBtn:Boolean=true;
  accesDeleteBtn:Boolean=true;
  ngOnInit() {
    this.getAll();

    //get data certificate type
    // var data = '';
    // this.service.httpClientGet('api/RoleCertificateType',data)
    // .subscribe(result => { 
    //     if(result=="Not found"){
    //         this.service.notfound();
    //     }
    //     else{
    //         this.certificatetype = result; 
    //     } 
    // },
    // error => {
    //     this.service.errorserver();
    // });

    // this.service.httpClientGet('api/RoleSubType','')
    // .subscribe(result => {  
    //     if(result=="Not found"){
    //         this.service.notfound();
    //     }
    //     else{
    //         this.subpartnertype = result; 
    //     } 
    // },
    // error => {
    //     this.service.errorserver();
    // });

    this.accesAddBtn = this.session.checkAccessButton("admin/partner-type/add-partner-type");
    this.accesUpdateBtn = this.session.checkAccessButton("admin/partner-type/update-partner-type");
    this.accesDeleteBtn = this.session.checkAccessButton("admin/partner-type/delete-partner-type");
  }

  getAll(){
    this.loading = true;
    //get data sub partner type
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/where/{"RoleType": "Company"}',data)
    .subscribe(result => {  
        if(result=="Not found"){
            this.service.notfound();
            this.loading = false;
        }
        else{
            this.data = result; 
            this.loading = false;
        } 
    },
    error => {
        this.service.errorserver();
        this.loading = false;
    });
  }

  //view detail
  // viewdetail(id) {
  //   var data = '';
  //   this.service.httpClientGet(this._serviceUrl+'/'+id, data)
  //   .subscribe(result => { 
  //       this.datadetail = result; 
  //       this.buildForm(); 
  //       this.getRoleParams(this.datadetail.RoleCode);
  //       this.getRoleCertificate(this.datadetail.RoleCode);
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // getRoleParams(code){
  //   var data = '';
  //   this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"'+code+'"}', data)
  //   .subscribe(result => { 
  //       this.datadetailroleparams = result; 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // getRoleCertificate(code){
  //   var data = '';
  //   this.service.httpClientGet('api/RoleCertificate/where/{"RoleCode":"'+code+'"}', data)
  //   .subscribe(result => { 
  //       this.datadetailrolecertificate = result; 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // selectCertificate(value,certificate){
  //   if (value.target.checked) {
  //     this.CertificateArr.push(certificate);
  //   } 
  //   else {
  //     this.CertificateArr.splice(this.CertificateArr.indexOf(certificate), 1);
  //   }
  // }

  // //submit add
  // onSubmit(){

  //   this.addpartner.controls['RoleCode'].markAsTouched();
  //   this.addpartner.controls['RoleName'].markAsTouched();
  //   this.addpartner.controls['Description'].markAsTouched();
  //   this.addpartner.controls['RoleSubType'].markAsTouched();

  //   if(this.addpartner.valid) {
      
  //     let datarolecertificate=[];
  
  //     //add roles (partner type)
  //     this.addpartner.value.RoleType = "Company";
  //     let dataroles = JSON.stringify(this.addpartner.value);
  //     this.service.httpClientPos(this._serviceUrl,dataroles);
      
  //     //add role certificate
  //     for(var i=0; i<this.CertificateArr.length;i++){
  //       this.addrolecertificate.value.RoleCode = this.addpartner.value.RoleCode;
  //       this.addrolecertificate.value.CertificateType = this.CertificateArr[i];
  //       this.addrolecertificate.value.CertificateName = "CertificateType";
  //       this.addrolecertificate.value.Status = "A";
  //       datarolecertificate[i] = JSON.stringify(this.addrolecertificate.value);
  //     }
  //     if(datarolecertificate.length > 0){
  //       this.service.httpClientPostArray('api/RoleCertificate',datarolecertificate);
  //     }
      
  //     //add role params
  //     this.addroleparams.value.ParamName = "SubType";
  //     this.addroleparams.value.Status = "A";
  //     this.addroleparams.value.ParamValue = this.addpartner.value.RoleSubType;
  //     this.addroleparams.value.RoleCode = this.addroleparams.value.RoleCode;
  //     let dataroleparams = JSON.stringify(this.addroleparams.value);
  //     this.service.httpClientPos('api/RoleParams',dataroleparams)
  
  //     this.getAll();
  //   }
  // }
  // resetFormAdd(){
  //   this.addpartner.reset({
  //     'RoleSubType':'',
  //   });
  // }

  //delete confirm
  openConfirmsSwal(id) {
    // var index = this.data.findIndex(x => x.RoleId == id);
    // this.service.httpClientDelete(this._serviceUrl,this.data,id,index);
    // this.loading = true;
    // var cuid = this.useraccessdata.ContactName;
    // var UserId = this.useraccessdata.UserId;

    // var data = '';
    // this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data,'','');
    // setTimeout(() => {
    //   this.ngOnInit();
    // }, 1000);
    // this.loading = false;

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result == true) {
        this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;

        var data = '';

        this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data)
          .subscribe(result => {
            let tmpData : any = result;
            this.messageResult = tmpData;
            var resource = result;
            if (resource['code'] == '1') {
              setTimeout(() => {
                this.ngOnInit();
              }, 1000);
            }
            else {
              swal(
                'Information!',
                "Delete Data Failed",
                'error'
              );
              this.loading = false;
            }
          },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
            this.loading = false;
          });
      }
    }).catch(swal.noop);
  }

  // onSubmitUpdate(){

  //   this.editpartner.controls['RoleCode'].markAsTouched();
  //   this.editpartner.controls['RoleName'].markAsTouched();
  //   this.editpartner.controls['RoleSubType'].markAsTouched();

  //   if(this.editpartner.valid){
      
  //     //form data
  //     let data = JSON.stringify(this.editpartner.value);
      
  //     //put action
  //     this.service.httpCLientPu(this._serviceUrl,this.datadetail.RoleId,data)

  //   }  
  // }

  // //build form update
  // buildForm(): void {

  //   let RoleId_update = new FormControl(this.datadetail.RoleId);
  //   let RoleCode_update = new FormControl(this.datadetail.RoleCode, Validators.required);
  //   let RoleName_update = new FormControl(this.datadetail.RoleName, Validators.required);
  //   let SiebelName_update = new FormControl(this.datadetail.SiebelName);
  //   let FrameworkName_update = new FormControl(this.datadetail.FrameworkName);
  //   let CRBApprovedName_update = new FormControl(this.datadetail.CRBApprovedName);
  //   let Description_update = new FormControl(this.datadetail.Description);
  //   let RoleType_update = new FormControl(this.datadetail.RoleType);
  //   let RoleSubType_update = new FormControl(this.datadetail.RoleSubType, Validators.required);

  //   this.editpartner = new FormGroup({
  //     RoleId:RoleId_update,
  //     RoleCode:RoleCode_update,
  //     RoleName:RoleName_update,
  //     SiebelName:SiebelName_update,
  //     FrameworkName:FrameworkName_update,
  //     CRBApprovedName:CRBApprovedName_update,
  //     Description:Description_update,
  //     RoleType:RoleType_update,
  //     RoleSubType:RoleSubType_update
  //   });
  // }
  // resetFormUpdate(){
  //   this.editpartner.reset({
  //     'RoleCode':this.datadetail.RoleCode,
  //     'RoleName':this.datadetail.RoleName,
  //     'RoleSubType':this.datadetail.RoleSubType
  //   });
  // }
}
