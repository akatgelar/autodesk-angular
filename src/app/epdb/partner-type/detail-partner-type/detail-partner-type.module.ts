import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DetailPartnerTypeEPDBComponent } from './detail-partner-type.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {AppService} from "../../../shared/service/app.service";

export const DetailPartnerTypeEPDBRoutes: Routes = [
  {
    path: '',
    component: DetailPartnerTypeEPDBComponent,
    data: {
      breadcrumb: 'Detail Partner Type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailPartnerTypeEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [DetailPartnerTypeEPDBComponent],
  providers:[AppService]
})
export class DetailPartnertTypeEPDBModule { }
