import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../../shared/service/app.service";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-partner-type',
  templateUrl: './detail-partner-type.component.html',
  styleUrls: [
    './detail-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class DetailPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Roles';
  public datadetail: any;
  public subpartnertype: any;
  public certificatetype: any;
  public datadetailroleparams: any;
  public datadetailrolecertificate: any;
  editpartner: FormGroup;
  CertificateArr = [];
  id: string = '';
  dropdownSubPartner = [];
  selectedSubPartner = [];
  dropdownCertificateType = [];
  selectedCertificateType = [];
  dropdownSettings = {};

  constructor(private router: Router, private service: AppService, private route: ActivatedRoute) {

    let RoleCode = new FormControl('', Validators.required);
    let RoleName = new FormControl('', Validators.required);
    let SiebelName = new FormControl('');
    let FrameworkName = new FormControl('');
    let CRBApprovedName = new FormControl('');
    let Description = new FormControl('', Validators.required);
    let RoleType = new FormControl('');
    let RoleSubType = new FormControl('');
    let Certificate = new FormControl('');
    let CertificateType = new FormControl('');
    let CertificateName = new FormControl('');
    let Status = new FormControl('');
    let ParamName = new FormControl('');
    let ParamValue = new FormControl('');

    this.editpartner = new FormGroup({
      RoleCode: RoleCode,
      RoleName: RoleName,
      SiebelName: SiebelName,
      FrameworkName: FrameworkName,
      CRBApprovedName: CRBApprovedName,
      Description: Description,
      RoleType: RoleType,
      RoleSubType: RoleSubType
    });

  }

  getRoleCertificateType() {
    //get data certificate type
    var data = '';
    this.service.httpClientGet('api/RoleCertificateType', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.certificatetype = result;
          this.dropdownCertificateType = this.certificatetype.map((item) => {
            return {
              id: item.KeyValue,
              itemName: item.KeyValue
            }
          });
        }
      },
      error => {
        this.service.errorserver();
      });
  }

  getRoleSubType() {
    var data = '';
    this.service.httpClientGet('api/RoleSubType', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.subpartnertype = result;
          this.dropdownSubPartner = this.subpartnertype.map((item) => {
            return {
              id: item.RoleSubType,
              itemName: item.RoleSubType
            }
          });
        }
      },
      error => {
        this.service.errorserver();
      });
  }

  ngOnInit() {
    this.getRoleCertificateType();
    this.getRoleSubType();

    //setting dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false
    };

    var data = '';
    this.id = this.route.snapshot.params['id'];
    this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
      .subscribe(result => {
        this.datadetail = result;
        this.buildForm();
        this.getRoleParams(this.datadetail.RoleCode);
        this.getRoleCertificate(this.datadetail.RoleCode);
      },
        error => {
          this.service.errorserver();
        });
  }

  arrsubpartnertypetmp=[];
  arrsubpartnertype=[];
  getRoleParams(code) {
    var data = '';
    var temp: any;
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"' + code + '"}', data)
      .subscribe(result => {
        this.datadetailroleparams = Object.keys(result).map(function (Index) {
          return {
            id: result[Index].ParamValue,
            itemName: result[Index].ParamValue,
            RoleParamId:result[Index].RoleParamId
          }
        });
        this.selectedSubPartner = this.datadetailroleparams;
        for(var i=0;i<this.selectedSubPartner.length;i++){
          this.arrsubpartnertypetmp.push([this.datadetailroleparams[i].id,"exist",this.datadetailroleparams[i].RoleParamId]);
          this.arrsubpartnertype.push([this.datadetailroleparams[i].id,"exist",this.datadetailroleparams[i].RoleParamId]);
        }
      },
      error => {
        this.service.errorserver();
      });
  }

  onSubPartnerSelect(item: any) {
    var index = this.arrsubpartnertype.findIndex(x => x[0] == item.id);
    if(index == -1){
      this.arrsubpartnertype.push([item.id,"insert"]);
    }else{
      this.arrsubpartnertype[index][1] = "exist";
    }
  }
  OnSubPartnerDeSelect(item: any) {
    var indextmp = this.arrsubpartnertypetmp.findIndex(x => x[0] == item.id);
    var index = this.arrsubpartnertype.findIndex(x => x[0] == item.id);
    if(index != -1 && indextmp != -1){
      this.arrsubpartnertype[index][1] = "delete";
    } 
    else if(index != -1 && indextmp == -1){
      this.arrsubpartnertype.splice(index,1);
    }
  }
  onSubPartnerSelectAll(items: any) { 
    this.arrsubpartnertype = [];
    
    for(var i=0;i<items.length;i++){
      this.arrsubpartnertype.push([items[i].id,"exist",this.datadetailroleparams[i].RoleParamId]);
    }

    for(var i=0;i<items.length;i++){
      var indextmp = this.arrsubpartnertypetmp.findIndex(x => x[0] == items[i].id);
      var index = this.arrsubpartnertype.findIndex(x => x[0] == items[i].id);
      if(index != -1 && indextmp == -1){
        this.arrsubpartnertype[index][1] = "insert";
        this.arrsubpartnertype[index][2] = "";
      }
    }
  }
  onSubPartnerDeSelectAll(items: any) { 
    this.arrsubpartnertype = [];
    
    for(var i=0;i<this.datadetailroleparams.length;i++){
      this.arrsubpartnertype.push([this.datadetailroleparams[i].id,"deleted",this.datadetailroleparams[i].RoleParamId]);
    }

    for(var i=0;i<this.datadetailroleparams.length;i++){
      var indextmp = this.arrsubpartnertypetmp.findIndex(x => x[0] == this.datadetailroleparams[i].id);
      var index = this.arrsubpartnertype.findIndex(x => x[0] == this.datadetailroleparams[i].id);
      if(index != -1 && indextmp == -1){
        this.arrsubpartnertype.splice(index,1);
      }
    }
  }

  arrrolecertificatetmp=[];
  arrrolecertificate=[];
  getRoleCertificate(code) {
    var data = '';
    console.log('api/RoleCertificate/where/{"RoleCode":"' + code + '"}')
    this.service.httpClientGet('api/RoleCertificate/where/{"RoleCode":"' + code + '"}', data)
      .subscribe(result => {
        this.datadetailrolecertificate = Object.keys(result).map(function (Index) {
          return {
            id: result[Index].KeyValue,
            itemName: result[Index].KeyValue,
            RoleCertificateId:result[Index].KeyValue
          }
        });
        this.selectedCertificateType = this.datadetailrolecertificate;
        for(var i=0;i<this.selectedCertificateType.length;i++){
          this.arrrolecertificatetmp.push([this.datadetailrolecertificate[i].id,"exist",this.datadetailrolecertificate[i].RoleCertificateId]);
          this.arrrolecertificate.push([this.datadetailrolecertificate[i].id,"exist",this.datadetailrolecertificate[i].RoleCertificateId]);
        }
      },
      error => {
        this.service.errorserver();
      });
  }

  onCertificateTypeSelect(item: any) { 
    var index = this.arrrolecertificate.findIndex(x => x[0] == item.id);
    if(index == -1){
      this.arrrolecertificate.push([item.id,"insert"]);
    }else{
      this.arrrolecertificate[index][1] = "exist";
    }
  }
  OnCertificateTypeDeSelect(item: any) { 
    var indextmp = this.arrrolecertificatetmp.findIndex(x => x[0] == item.id);
    var index = this.arrrolecertificate.findIndex(x => x[0] == item.id);
    if(index != -1 && indextmp != -1){
      this.arrrolecertificate[index][1] = "delete";
    } 
    else if(index != -1 && indextmp == -1){
      this.arrrolecertificate.splice(index,1);
    }
  }
  onCertificateTypeSelectAll(items: any) { 
    this.arrrolecertificate = [];
    
    for(var i=0;i<items.length;i++){
      this.arrrolecertificate.push([items[i].id,"exist",this.datadetailrolecertificate[i].RoleParamId]);
    }

    for(var i=0;i<items.length;i++){
      var indextmp = this.arrrolecertificatetmp.findIndex(x => x[0] == items[i].id);
      var index = this.arrrolecertificate.findIndex(x => x[0] == items[i].id);
      if(index != -1 && indextmp == -1){
        this.arrrolecertificate[index][1] = "insert";
        this.arrrolecertificate[index][2] = "";
      }
    }
  }
  onCertificateTypeDeSelectAll(item: any) { 
    this.arrrolecertificate = [];
    
    for(var i=0;i<this.datadetailrolecertificate.length;i++){
      this.arrrolecertificate.push([this.datadetailrolecertificate[i].id,"deleted",this.datadetailrolecertificate[i].RoleParamId]);
    }

    for(var i=0;i<this.datadetailrolecertificate.length;i++){
      var indextmp = this.arrrolecertificatetmp.findIndex(x => x[0] == this.datadetailrolecertificate[i].id);
      var index = this.arrrolecertificate.findIndex(x => x[0] == this.datadetailrolecertificate[i].id);
      if(index != -1 && indextmp == -1){
        this.arrrolecertificate.splice(index,1);
      }
    }
  }

  onSubmitUpdate() {

    this.editpartner.controls['RoleCode'].markAsTouched();
    this.editpartner.controls['RoleName'].markAsTouched();
    this.editpartner.controls['Description'].markAsTouched();

    if (this.editpartner.valid) {

      //form data
      this.editpartner.value.SubPartnerType=this.arrsubpartnertype;
      this.editpartner.value.RoleCertificate=this.arrrolecertificate;
  
      let data = JSON.stringify(this.editpartner.value);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
      //this.service.httpCLientPut(this._serviceUrl,this.id, data);


      //redirect
      this.router.navigate(['/admin/partner-type/list-partner-type']);

    }
  }

  //build form update
  buildForm(): void {

    let RoleId = new FormControl(this.datadetail.RoleId);
    let RoleCode = new FormControl(this.datadetail.RoleCode, Validators.required);
    let RoleName = new FormControl(this.datadetail.RoleName, Validators.required);
    let SiebelName = new FormControl(this.datadetail.SiebelName);
    let FrameworkName = new FormControl(this.datadetail.FrameworkName);
    let CRBApprovedName = new FormControl(this.datadetail.CRBApprovedName);
    let Description = new FormControl(this.datadetail.Description, Validators.required);
    let RoleType = new FormControl(this.datadetail.RoleType);
    let RoleSubType = new FormControl(this.datadetail.RoleSubType);

    this.editpartner = new FormGroup({
      RoleId: RoleId,
      RoleCode: RoleCode,
      RoleName: RoleName,
      SiebelName: SiebelName,
      FrameworkName: FrameworkName,
      CRBApprovedName: CRBApprovedName,
      Description: Description,
      RoleType: RoleType,
      RoleSubType: RoleSubType
    });
  }

  resetFormUpdate() {
    this.editpartner.reset({
      'RoleCode': this.datadetail.RoleCode,
      'RoleName': this.datadetail.RoleName
    });
  }
}
