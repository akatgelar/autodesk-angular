import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListSubPartnerTypeEPDBComponent } from './list-sub-partner-type.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterSubPartnerTypePipe } from './list-sub-partner-type.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const ListSubPartnerTypeEPDBRoutes: Routes = [
  {
    path: '',
    component: ListSubPartnerTypeEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.sub_partner_type.list_sub_partner_type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListSubPartnerTypeEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ListSubPartnerTypeEPDBComponent, DataFilterSubPartnerTypePipe],
  providers: [AppService, SessionService]
})
export class ListSubPartnertTypeEPDBModule { }
