import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterSubPartnerType' })
export class DataFilterSubPartnerTypePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.ParamValue.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-list-sub-partner-type',
  templateUrl: './list-sub-partner-type.component.html',
  styleUrls: [
    './list-sub-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ListSubPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/RoleSubType';
  messageResult: string = '';
  messageError: string = '';
  public data: any = '';
  public datadetail: any;
  public subpartnertype: any;
  public datarolesubtype: any;
  public datapartnertype: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "ParamValue";
  public sortOrder: string = "asc";
  addsubpartner: FormGroup;
  editsubpartner: FormGroup;
  public partnertype: any;
  public dataFound = false;
  useraccessdata: any;
  private loading = false;

  constructor(public session: SessionService, private service: AppService, private route: ActivatedRoute) {

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);

    //validation addsubpartner
    let RoleSubType = new FormControl('', Validators.required);

    this.addsubpartner = new FormGroup({
      RoleSubType: RoleSubType
    });

    //validation editsubpartner
    let RoleSubType_update = new FormControl('');

    this.editsubpartner = new FormGroup({
      RoleSubType: RoleSubType_update
    });

  }


  category: string = "";
  accesAddBtn: Boolean = true;
  accesUpdateBtn: Boolean = true;
  accesDeleteBtn: Boolean = true;
  ngOnInit() {
    // var sub: any;
    // sub = this.route.queryParams.subscribe(params => {
    //     this.category = params['category'] || "";
    // });

    // if (this.category != "" && this.category != undefined) {
    //     this.getAllSubPartner(this.category);
    // }

    //Untuk set filter terakhir hasil pencarian
    if (!(localStorage.getItem("filter") === null)) {
      var item = JSON.parse(localStorage.getItem("filter"));
      this.category = item.kategorySub;
      this.getAllSubPartner(this.category);
    }

    this.loading = true;
    var data = '';
    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
      .subscribe((result:any) => {
        if (result == "Not found") {
          this.service.notfound();
          this.partnertype = '';
          this.loading = false;
        }
        else {
          this.partnertype = result.sort((a,b)=> {
            if(a.RoleName > b.RoleName)
              return 1
            else if(a.RoleName < b.RoleName)
              return -1
            else return 0
          });
          this.loading = false;
        }
      },
      error => {
        this.service.errorserver();
        this.partnertype = '';
        this.loading = false;
      });

    this.accesAddBtn = this.session.checkAccessButton("admin/partner-type/add-sub-partner-type");
    this.accesUpdateBtn = this.session.checkAccessButton("admin/partner-type/update-sub-partner-type");
    this.accesDeleteBtn = this.session.checkAccessButton("admin/partner-type/delete-sub-partner-type");
  }

  getAllSubPartner(value) {
    this.loading = true;
    //get data sub partner type
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/where/{"RoleCode":"' + value + '"}', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dataFound = false;
          this.loading = false;
        }
        else {
          this.data = result;
          this.dataFound = true;
          this.loading = false;
        }
      },
      error => {
        this.service.errorserver();
        this.loading = false;
      });

    //Buat object untuk filter yang dipilih
    var params =
      {
        kategorySub: this.category
      }
    //Masukan object filter ke local storage
    localStorage.setItem("filter", JSON.stringify(params));
  }

  //view detail
  viewdetail(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        this.datadetail = result;
        this.buildForm();
      },
      error => {
        this.service.errorserver();
      });
  }

  //Insert Data Partner Sub Type
  onSubmit() {

    this.addsubpartner.controls['RoleSubType'].markAsTouched();

    if (this.addsubpartner.valid) {

      //data form
      let data = JSON.stringify(this.addsubpartner.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
        .subscribe(res => {
          console.log(res);
        });

      setTimeout(() => {
        this.ngOnInit();
      }, 1000)

    }

  }

  //delete confirm
  openConfirmsSwal(id) {
    // var index = this.data.findIndex(x => x.RoleParamId == id);
    // this.service.httpClientDelete(this._serviceUrl, this.data, id, index);

      // this.loading = true;
      // var cuid = this.useraccessdata.ContactName;
      // var UserId = this.useraccessdata.UserId;

      // var data = '';
      // this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data,'','');
      // setTimeout(() => {
      //   this.ngOnInit();
      // }, 1000);
      // this.loading = false;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result == true) {
        this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;

        var data = '';

        this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data)
          .subscribe(result => {
            let tmpData : any = result;
            this.messageResult = tmpData;
            var resource = result;
            if (resource['code'] == '1') {
              setTimeout(() => {
                this.ngOnInit();
              }, 1000);
            }
            else {
              swal(
                'Information!',
                "Delete Data Failed",
                'error'
              );
              this.loading = false;
            }
          },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
            this.loading = false;
          });
      }
    }).catch(swal.noop);
  }

  // onSubmitUpdate() {

  //   this.editsubpartner.controls['RoleSubType'].markAsTouched();

  //   if (this.editsubpartner.valid) {

  //     //data form
  //     let data = JSON.stringify(this.editsubpartner.value);

  //     //put action
  //     var index = this.data.findIndex(x => x.RoleSubTypeId == this.datadetail.RoleSubTypeId);
  //     this.service.httpCLientPutModal(this._serviceUrl, this.datadetail.RoleSubTypeId, data, index, this.data, this.editsubpartner.value);

  //   }
  // }

  //build form update
  buildForm(): void {

    let RoleSubTypeId_update = new FormControl(this.datadetail.RoleSubTypeId);
    let RoleSubType_update = new FormControl(this.datadetail.RoleSubType, Validators.required);

    this.editsubpartner = new FormGroup({
      RoleSubType: RoleSubType_update,
      RoleSubTypeId: RoleSubTypeId_update
    });
  }

  resetFormAdd() {
    this.addsubpartner.reset();
  }

  resetFormUpdate() {
    this.editsubpartner.reset({
      'RoleSubType': this.datadetail.RoleSubType,
    });
  }
}
