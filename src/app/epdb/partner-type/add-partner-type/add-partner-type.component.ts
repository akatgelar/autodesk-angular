import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-add-partner-type',
  templateUrl: './add-partner-type.component.html',
  styleUrls: [
    './add-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Roles';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datadetail: any;
  public subpartnertype: any;
  public certificatetype: any;
  public datadetailroleparams: any;
  public datadetailrolecertificate: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  addpartner: FormGroup;
  addroleparams: FormGroup;
  addrolecertificate: FormGroup;
  CertificateArr = [];
  public useraccesdata: any;

  dropdownSubPartner = [];
  selectedSubPartner = [];
  dropdownCertificateType = [];
  selectedCertificateType = [];
  dropdownSettings = {};
  public loading = false;

  constructor(private router: Router, private service: AppService, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let RoleCode = new FormControl('', Validators.required);
    let RoleName = new FormControl('', Validators.required);
    let SiebelName = new FormControl('');
    let FrameworkName = new FormControl('');
    let CRBApprovedName = new FormControl('');
    let Description = new FormControl('', Validators.required);
    let RoleType = new FormControl('');
    let RoleSubType = new FormControl('');
    let Certificate = new FormControl('');
    let CertificateType = new FormControl('');
    let CertificateName = new FormControl('');
    let Status = new FormControl('');
    let ParamName = new FormControl('');
    let ParamValue = new FormControl('');

    this.addpartner = new FormGroup({
      RoleCode: RoleCode,
      RoleName: RoleName,
      SiebelName: SiebelName,
      FrameworkName: FrameworkName,
      CRBApprovedName: CRBApprovedName,
      Description: Description,
      RoleType: RoleType,
      RoleSubType: RoleSubType
    });

    this.addrolecertificate = new FormGroup({
      RoleCode: RoleCode,
      CertificateType: CertificateType,
      CertificateName: CertificateName,
      Status: Status
    });

    this.addroleparams = new FormGroup({
      RoleCode: RoleCode,
      ParamName: ParamName,
      ParamValue: ParamValue,
      Status: Status
    });

  }

  getRoleCertificateType() {
    //get data certificate type
    var data = '';
    this.service.httpClientGet('api/RoleCertificateType/AAP', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.certificatetype = result;
          this.dropdownCertificateType = this.certificatetype.map((item) => {
            return {
              id: item.KeyValue,
              itemName: item.KeyValue
            }
          });
        }
      },
      error => {
        this.service.errorserver();
      });
  }

  // getRoleSubType() {
  //   var data = '';
  //   this.service.httpClientGet('api/RoleSubType', data)
  //     .subscribe(result => {
  //       if (result == "Not found") {
  //         this.service.notfound();
  //       }
  //       else {
  //         this.subpartnertype = result;
  //         this.dropdownSubPartner = this.subpartnertype.map((item) => {
  //           return {
  //             id: item.RoleSubTypeId,
  //             itemName: item.RoleSubType
  //           }
  //         });
  //       }
  //     },
  //       error => {
  //         this.service.errorserver();
  //       });
  // }

  
  validcode:Boolean=true;
  public codedata:any;
  mustBeUnique(value){
    if(value!='' && value!=null){
      this.service.httpClientGet("api/Roles/CheckCode/"+value, '')
      .subscribe(result => {
        this.codedata = result;
        if(this.codedata.RoleCode != null){
          this.validcode = false;
        }
        else if(this.codedata.RoleCode == null){
          this.validcode = true;
        }
      },
      error => {
        this.service.errorserver();
      });
    }
    else{
      this.validcode = true;
    }
  }

  ngOnInit() {
    this.getRoleCertificateType();
    // this.getRoleSubType();
    //setting dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false
    };
  }

  onSubPartnerSelect(item: any) { }
  OnSubPartnerDeSelect(item: any) { }
  onSubPartnerSelectAll(item: any) { }
  onSubPartnerDeSelectAll(item: any) { }

  onCertificateTypeSelect(item: any) { }
  OnCertificateTypeDeSelect(item: any) { }
  onCertificateTypeSelectAll(item: any) { }
  onCertificateTypeDeSelectAll(item: any) { }

  //submit add
  onSubmit() {

    this.addpartner.controls['RoleCode'].markAsTouched();
    this.addpartner.controls['RoleName'].markAsTouched();
    this.addpartner.controls['Description'].markAsTouched();

    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.addpartner.controls['RoleCode'].value))
      return swal('ERROR','Special character not allowed in partner code','error')
    if(format.test(this.addpartner.value.RoleName))
      return swal('ERROR','Special character not allowed in partner name','error')
    if(format.test(this.addpartner.value.Description))
      return swal('ERROR','Special character not allowed in description','error')
    if (this.addpartner.valid && this.validcode) {

      this.loading = true;
      let datarolecertificate = [];

      //add roles (partner type)
      this.addpartner.value.RoleType = "Company";
      this.addpartner.value.muid = this.useraccesdata.ContactName; 
      this.addpartner.value.cuid = this.useraccesdata.ContactName;
      this.addpartner.value.UserId = this.useraccesdata.UserId;
      let dataroles = JSON.stringify(this.addpartner.value);
      this.service.httpClientPost(this._serviceUrl, dataroles)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      setTimeout(() => {

        //add role certificate
        for (var i = 0; i < this.selectedCertificateType.length; i++) {
          this.addrolecertificate.value.RoleCode = this.addpartner.value.RoleCode;
          this.addrolecertificate.value.CertificateType = this.selectedCertificateType[i].id;
          this.addrolecertificate.value.CertificateName = "CertificateType";
          this.addrolecertificate.value.Status = "A";
          datarolecertificate[i] = JSON.stringify(this.addrolecertificate.value);  
        }

        if (datarolecertificate.length > 0) {
          for (var i = 0; i < datarolecertificate.length; i++) {
            this.service.httpClientPost('api/RoleCertificate', datarolecertificate[i])
              .subscribe(res => {
                console.log(res);
              });
          }
        }
        
        //redirect
        this.router.navigate(['/admin/partner-type/list-partner-type']);
        this.loading = false;

      }, 1000)
    }
  }

  resetFormAdd() {
    this.addpartner.reset({
      'RoleSubType': '',
    });
  }

}
