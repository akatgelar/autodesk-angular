import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSubPartnerTypeEPDBComponent } from './add-sub-partner-type.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";

import { AppService } from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const AddSubPartnerTypeEPDBRoutes: Routes = [
  {
    path: '',
    component: AddSubPartnerTypeEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.sub_partner_type.add_new_sub_partner_type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddSubPartnerTypeEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddSubPartnerTypeEPDBComponent],
  providers: [AppService]
})
export class AddSubPartnertTypeEPDBModule { }
