import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import {AppService} from "../../../shared/service/app.service";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-add-sub-partner-type',
  templateUrl: './add-sub-partner-type.component.html',
  styleUrls: [
    './add-sub-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class AddSubPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/RoleSubType';
  addsubpartner:FormGroup;
  public partnertype:any;
  public loading = false;
  public useraccesdata: any;

  constructor(private router: Router,private service:AppService, private session: SessionService) { 

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation addsubpartner
    let RoleSubType = new FormControl('', Validators.required);
    let PartnerType = new FormControl('', Validators.required);

    this.addsubpartner = new FormGroup({
        RoleSubType:RoleSubType,
        PartnerType:PartnerType
    });

  }

  ngOnInit() { 
    var data = '';
    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}',data)
    .subscribe((result:any) => { 
        if(result=="Not found"){
            this.service.notfound();
            this.partnertype = ''; 
        }
        else{
            this.partnertype = result.sort((a,b)=>{
              if(a.RoleName > b.RoleName)
                return 1
              if(a.RoleName < b.RoleName)
                return -1
              else
                return 0
            }); 
        } 
    },
    error => {
        this.service.errorserver();
        this.partnertype = ''; 
    });
  }

  //Insert Data Partner Sub Type
  onSubmit(){

    this.addsubpartner.controls['RoleSubType'].markAsTouched();
    this.addsubpartner.controls['PartnerType'].markAsTouched();

    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.addsubpartner.value.RoleSubType))
      return swal('ERROR','Special character not allowed in sub partner type name','error')
    if(this.addsubpartner.valid){
      
      this.loading = true;
      this.addsubpartner.value.muid = this.useraccesdata.ContactName; 
      this.addsubpartner.value.cuid = this.useraccesdata.ContactName;
      this.addsubpartner.value.UserId = this.useraccesdata.UserId;

      //data form
      let data = JSON.stringify(this.addsubpartner.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/partner-type/list-sub-partner-type'], { queryParams: { category: this.addsubpartner.value.PartnerType } });
        this.loading = false;
      }, 1000)
    }

  }

  resetFormAdd(){
    this.addsubpartner.reset();
  }
}
