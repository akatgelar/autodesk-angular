import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-detail-sub-partner-type',
  templateUrl: './detail-sub-partner-type.component.html',
  styleUrls: [
    './detail-sub-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class DetailSubPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/RoleSubType';
  public datadetail: any;
  editsubpartner:FormGroup;
  id:string='';

  constructor(private router: Router, private service:AppService, private route:ActivatedRoute) { 

    //validation editsubpartner
    let RoleSubType_update = new FormControl('');

    this.editsubpartner = new FormGroup({
        RoleSubType:RoleSubType_update
    });

  }

  ngOnInit() {
    var data = '';
    this.id = this.route.snapshot.params['id'];
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result; 
        this.buildForm(); 
    },
    error => {
        this.service.errorserver();
    });
  }

  onSubmitUpdate(){

    this.editsubpartner.controls['RoleSubType'].markAsTouched();
    
    if(this.editsubpartner.valid){

      //data form
      let data = JSON.stringify(this.editsubpartner.value);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
      //this.service.httpCLientPut(this._serviceUrl,this.id, data);
      //redirect
      this.router.navigate(['/admin/partner-type/list-sub-partner-type']);
    }
  }

  //build form update
  buildForm(): void {

    let RoleSubTypeId_update = new FormControl(this.datadetail.RoleSubTypeId);
    let RoleSubType_update = new FormControl(this.datadetail.RoleSubType,Validators.required);

    this.editsubpartner = new FormGroup({
        RoleSubType:RoleSubType_update,
        RoleSubTypeId:RoleSubTypeId_update
    });
  }

  resetFormUpdate(){
    this.editsubpartner.reset({
      'RoleSubType':this.datadetail.RoleSubType,
    });
  }
}
