import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailSubPartnerTypeEPDBComponent } from './detail-sub-partner-type.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const DetailSubPartnerTypeEPDBRoutes: Routes = [
  {
    path: '',
    component: DetailSubPartnerTypeEPDBComponent,
    data: {
      breadcrumb: 'Detail Sub Partner Type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailSubPartnerTypeEPDBRoutes),
    SharedModule
  ],
  declarations: [DetailSubPartnerTypeEPDBComponent],
  providers:[AppService]
})
export class DetailSubPartnertTypeEPDBModule { }
