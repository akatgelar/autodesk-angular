import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-update-sub-partner-type',
  templateUrl: './update-sub-partner-type.component.html',
  styleUrls: [
    './update-sub-partner-type.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class UpdateSubPartnerTypeEPDBComponent implements OnInit {

  private _serviceUrl = 'api/RoleSubType';
  public datadetail: any;
  editsubpartner:FormGroup;
  id:string='';
  public partnertype:any;
  public loading = false;
  public useraccesdata: any;

  constructor(private router: Router, private service:AppService, private route:ActivatedRoute, private session: SessionService) { 

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation editsubpartner
    let RoleSubType = new FormControl('');
    let PartnerType = new FormControl('');

    this.editsubpartner = new FormGroup({
        RoleSubType:RoleSubType,
        PartnerType:PartnerType
    });

  }

  ngOnInit() {
    var data = '';
    this.id = this.route.snapshot.params['id'];
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result; 
        this.buildForm(); 
    },
    error => {
        this.service.errorserver();
    });

    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.partnertype = ''; 
        }
        else{
            this.partnertype = result; 
        } 
    },
    error => {
        this.service.errorserver();
        this.partnertype = ''; 
    });
  }

  onSubmitUpdate(){

    this.editsubpartner.controls['RoleSubType'].markAsTouched();
    this.editsubpartner.controls['PartnerType'].markAsTouched();
    
    if(this.editsubpartner.valid){

      this.loading = true;
      this.editsubpartner.value.muid = this.useraccesdata.ContactName; 
      this.editsubpartner.value.cuid = this.useraccesdata.ContactName;
      this.editsubpartner.value.UserId = this.useraccesdata.UserId;
      //data form
      let data = JSON.stringify(this.editsubpartner.value);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
     // this.service.httpCLientPut(this._serviceUrl,this.id, data);

      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/partner-type/list-sub-partner-type'], { queryParams: { category: this.editsubpartner.value.PartnerType } });
        this.loading = false;
      }, 1000)
    }
  }

  //build form update
  buildForm(): void {

    let PartnerType = new FormControl(this.datadetail.RoleCode,Validators.required);
    let RoleSubType = new FormControl(this.datadetail.ParamValue,Validators.required);

    this.editsubpartner = new FormGroup({
        RoleSubType:RoleSubType,
        PartnerType:PartnerType
    });
  }

  resetFormUpdate(){
    this.editsubpartner.reset({
      'RoleSubType':this.datadetail.RoleSubType,
      'PartnerType':this.datadetail.RoleCode,
    });
  }
}
