import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateSubPartnerTypeEPDBComponent } from './update-sub-partner-type.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";

import { AppService } from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const UpdateSubPartnerTypeEPDBRoutes: Routes = [
  {
    path: '',
    component: UpdateSubPartnerTypeEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.sub_partner_type.edit_sub_partner_type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateSubPartnerTypeEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [UpdateSubPartnerTypeEPDBComponent],
  providers: [AppService]
})
export class UpdateSubPartnertTypeEPDBModule { }
