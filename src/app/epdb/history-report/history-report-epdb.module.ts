import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryReportEPDBComponent } from './history-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const HistoryReportEPDBRoutes: Routes = [
  {
    path: '',
    component: HistoryReportEPDBComponent,
    data: {
      breadcrumb: 'History Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HistoryReportEPDBRoutes),
    SharedModule
  ],
  declarations: [HistoryReportEPDBComponent]
})
export class HistoryReportEPDBModule { }
