import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ATCLocator2012ReportALPHAEPDBComponent } from './ATC-locator-2012-report-ALPHA-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const ATCLocator2012ReportALPHAEPDBRoutes: Routes = [
  {
    path: '',
    component: ATCLocator2012ReportALPHAEPDBComponent,
    data: {
      breadcrumb: 'ATC Locator 2012 Report ALPHA',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ATCLocator2012ReportALPHAEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ATCLocator2012ReportALPHAEPDBComponent]
})
export class ATCLocator2012ReportALPHAEPDBModule { }
