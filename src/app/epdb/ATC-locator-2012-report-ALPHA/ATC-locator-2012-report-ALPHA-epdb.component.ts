import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-ATC-locator-2012-report-ALPHA-epdb',
  templateUrl: './ATC-locator-2012-report-ALPHA-epdb.component.html',
  styleUrls: [
    './ATC-locator-2012-report-ALPHA-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ATCLocator2012ReportALPHAEPDBComponent implements OnInit {

  dropdownListGeo = [];
  selectedItemsGeo = [];
  dropdownSettingsGeo = {};

  dropdownListRegion = [];
  selectedItemsRegion = [];
  dropdownSettingsRegion = {};
  
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
   
 
  constructor(public http: Http) { }

  ngOnInit() {
    this.http.get(`assets/data/activities.json`)
      .subscribe((data)=> {
        this.data = data.json();
      });

      // Geo
      this.dropdownListGeo = [
        {"id":1,"itemName":"AMER"},
        {"id":2,"itemName":"APAC (excl. GCR)"},
        {"id":3,"itemName":"EMEA"},
        {"id":4,"itemName":"GCR"}
      ];
      this.selectedItemsGeo = [];
      this.dropdownSettingsGeo = { 
          singleSelection: false, 
          text:"Please Select",
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"myclass custom-class"
      };

      // Region
      this.dropdownListRegion = [
          {"id":1,"itemName":"ANZ"},
          {"id":2,"itemName":"ASEAN"},
          {"id":3,"itemName":"Canada"},
          {"id":4,"itemName":"Central Europe"}
      ];
      this.selectedItemsRegion = [];
      this.dropdownSettingsRegion = { 
          singleSelection: false, 
          text:"Please Select",
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"myclass custom-class"
      };
  }

}