import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactEPDBComponent } from './add-contact-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

import { DataFilterAddContactPipe } from './add-contact-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const AddContactEPDBRoutes: Routes = [
  {
    path: '',
    component: AddContactEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.add_edit.add_new_contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddContactEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddContactEPDBComponent, DataFilterAddContactPipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class AddContactEPDBModule { }
