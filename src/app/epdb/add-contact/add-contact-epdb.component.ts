import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';  

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterAddContact' })
export class DataFilterAddContactPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => 
                (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
  selector: 'app-add-contact-epdb',
  templateUrl: './add-contact-epdb.component.html',
  styleUrls: [
    './add-contact-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class AddContactEPDBComponent implements OnInit {

  private _serviceUrl = 'api/MainContact';
  addcontact: FormGroup; 
  public datadetail:any;
  updateaffiliations:FormGroup;
  public dataorg:any;
  public data:any;
  public datacontact:any;
  
  public rowsOnPage: number = 5;
  public filterQuery: string = "";
  public sortBy: string = "name";
  public sortOrder: string = "desc";
  
  public rowsOnPage1: number = 10;
  public filterQuery1: string = "";
  public sortBy1: string = "name";
  public sortOrder1: string = "desc";

  contactsarray=[];
  public userlevel:any;
  public partnertype:any;
  filtercontact:FormGroup;
  email:string="";
  siteid:string="";
  id:string="";
  public loading = false;


  public useraccesdata:any;
  
 
  constructor(private _http: Http, public session:SessionService,private router: Router, private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute) { 

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let ContactId = new FormControl('');
    let FirstName = new FormControl('');
    let LastName = new FormControl('');
    // let EmailAddress = new FormControl('', [Validators.required,Validators.email]);
    let EmailAddress = new FormControl('',Validators.required);
    let DateAdded = new FormControl('');
    let AddedBy = new FormControl('');
    let PartnerType = new FormControl('');

    this.filtercontact = new FormGroup({
      ContactId:ContactId,  
      FirstName:FirstName,  
      LastName:LastName,   
      EmailAddress:EmailAddress,
      PartnerType: PartnerType
    })

    let ContactAffiliate = new FormControl('');
    let SiteId = new FormControl('');
    
    this.updateaffiliations = new FormGroup({
      ContactAffiliate:ContactAffiliate,
      SiteId:SiteId
    });

  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    //get data site
    var data = '';
    this.service.httpClientGet('api/MainSite/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result;
        if (this.datadetail.SiteName != null && this.datadetail.SiteName != '') {
          this.datadetail.SiteName = this.service.decoder(this.datadetail.SiteName);
        }
        this.getOrganization(this.datadetail.OrgId);
        this.siteid=this.datadetail.SiteId;
    },
    error => {
        this.service.errorserver();
    });

    data='';
    this.service.httpClientGet('api/MainSite/AffiliateContact/'+this.id,data)
    .subscribe(result => { 
      if(result==null){
          this.data = '';
      }
      else{
          this.data = result;
          if(this.data != null){
            for (var i = 0; i < this.data.length; i++) {
              if (this.data[i].ContactName != null && this.data[i].ContactName != '') {
                  this.data[i].ContactName = this.service.decoder(this.data[i].ContactName);
              }
            }
          }
          this.contactchecked(this.data);
      } 
    },
    error => {
        this.service.errorserver();
        this.data = '';
    });

    //partner type
    var data = '';
    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.partnertype = null;
        }
        else{
            this.partnertype = result
        } 
    },
    error => {
        this.service.errorserver();
    });
  }

  choosesite:Boolean=false;
  affiliatebasedsite(){
    this.choosesite = true;
    this.datacontact = null;
  }

  getOrganization(id){
    //get data organization
    var data = '';
    this.service.httpClientGet('api/MainOrganization/where/{"OrgId":"'+id+'"}', data)
    .subscribe(result => {
        this.dataorg = result;
        if(this.dataorg != null){
          this.dataorg[0].OrgName = this.service.decoder(this.dataorg[0].OrgName);
        }
    },
    error => {
        this.service.errorserver();
    });
  }

  openaddnewcontact(){
    this.router.navigate(['/manage/contact/add-new-contact'], { 
      queryParams: 
      { 
        email: this.email, 
        siteid: this.siteid, 
        siteidint: this.id,
        firstname: this.firstname,
        lastname: this.lastname
      } 
    });
  }

  /* populate data issue role distributor */
  checkrole(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
        return false;
      } else {
        return true;
      }
    }
  }

  itsDistributor(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "DISTRIBUTOR") {
        return true;
      } else {
        return false;
      }
    }
  }

  urlGetOrgId(): string {
    var orgarr = this.useraccesdata.OrgId.split(',');
    var orgnew = [];
    for (var i = 0; i < orgarr.length; i++) {
      orgnew.push('"' + orgarr[i] + '"');
    }
    return orgnew.toString();
  }
  /* populate data issue role distributor */

  //submit form filter
  firstname:string="";
  lastname:string="";
  onSubmit() {
    this.choosesite = false;

    this.filtercontact.controls['EmailAddress'].markAsTouched();

    if(this.filtercontact.valid){
      this.loading = true;

      this.firstname = this.filtercontact.value.FirstName;
      this.lastname = this.filtercontact.value.LastName;
      this.email = this.filtercontact.value.EmailAddress;
      
      var dataFilter = 
        {
          ContactId:this.filtercontact.value.ContactId,
          FirstName:this.filtercontact.value.FirstName,
          LastName:this.filtercontact.value.LastName,
          EmailAddress:this.filtercontact.value.EmailAddress,
          PartnerType:this.filtercontact.value.PartnerType,
          // OrgId:this.datadetail.OrgId
          SiteId:this.siteid /* add contact affiliate */
        };

      /* add contact affiliate (distributor)*/
      var roleFilter = {};
      if (this.checkrole()) {
        if (this.itsDistributor()) {
            roleFilter = {
              "Distributor": this.urlGetOrgId()
            }
        }
      }

      dataFilter = Object.assign(dataFilter, roleFilter);
      /* add contact affiliate (distributor)*/

      /* autodesk plan 10 oct */

      this.service.httpClientPost(this._serviceUrl+"/FilterContactNew", JSON.stringify(dataFilter).replace(/'/g,"\\\\'"))
        .subscribe(result => {
            // var finalresult = result.replace(/\t/g, " ");
            // finalresult.replace(/\u/g, "u");
            let finalresult : any = result;
            if (finalresult == "Not found") {
                this.service.notfound();
                this.datacontact = '';
                this.loading = false;
            }
            else {
                this.datacontact = finalresult;
                this.loading = false;
            }
        },
        error => {
            this.service.errorserver();
            this.datacontact = '';
            this.loading = false;
        });

      /* end line autodesk plan 10 oct */

    }
  }

  //submit form
  onSubmitAdd() {

    if(this.addcontact.valid){
      this.loading = true;
      this.addcontact.value.SiteId = this.datadetail.SiteId;
      this.addcontact.value.DateAdded = this.formatdate.dateJStoYMD(new Date());
      this.addcontact.value.AddedBy = "Admin";
      this.addcontact.value.ContactName = this.service.encoder(this.filtercontact.value.FirstName)+" "+this.service.encoder(this.filtercontact.value.LastName);
      this.addcontact.value.FirstName = this.service.encoder(this.filtercontact.value.FirstName);
      this.addcontact.value.LastName = this.service.encoder(this.filtercontact.value.LastName);

      //convert object to json
      let data = JSON.stringify(this.addcontact.value);

      // console.log(data)

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});


      //redirect
      this.router.navigate(['/manage/site/detail-site',this.datadetail.SiteIdInt]);
      this.loading = false;

    }
  }

  contactchecked(contact){
    // console.log(contact)
    for(var i=0; i<contact.length;i++){
      if(contact[i].CheckedContact == true){
        this.contactsarray.push([contact[i].ContactId,true,contact[i].SiteContactLinkId,contact[i].InsertNewSite]);
      }else{
        this.contactsarray.push([contact[i].ContactId,false,contact[i].SiteContactLinkId,contact[i].InsertNewSite]);
      }
    }
  }

  selectContact(value,contacts){
    if (value.target.checked) {
      for(var i=0; i<this.contactsarray.length;i++){
        if(this.contactsarray[i][0] == contacts){
          this.contactsarray[i][1] = true;
        }
      }
    } 
    else {
      for(var i=0; i<this.contactsarray.length;i++){
        if(this.contactsarray[i][0] == contacts){
          this.contactsarray[i][1] = false;
        }
      }
    }
    // console.log(this.contactsarray)
  }

  addcontactaffiliate(){
    //form data
    this.updateaffiliations.value.ContactAffiliate = this.contactsarray;
    this.updateaffiliations.value.SiteId = this.datadetail.SiteId;
    this.updateaffiliations.value.AddedBy = this.useraccesdata.ContactName;

    //convert json
    let data = JSON.stringify(this.updateaffiliations.value);
    // console.log(data)

    //action update
    this.service.httpCLientPut('api/SiteContactLinks/update',data)
      .subscribe(res=>{
        console.log(res)
      });

    //redirect
    this.router.navigate(['/manage/site/detail-site',this.datadetail.SiteIdInt]);

  }

  affiliate(value,detailcontact,siteid){
    //form data
    this.updateaffiliations.value.ContactAffiliate = value;
    this.updateaffiliations.value.SiteId = this.datadetail.SiteId;
    this.updateaffiliations.value.AddedBy = this.useraccesdata.ContactName;

    this.updateaffiliations.value.cuid = this.useraccesdata.ContactName;
    this.updateaffiliations.value.UserId = this.useraccesdata.UserId;

    //convert json
    let data = JSON.stringify(this.updateaffiliations.value);

    //action update
    this.service.httpClientPost('api/SiteContactLinks/Affiliate',data) 
	.subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});


    //redirect
    this.router.navigate(['/manage/contact/detail-contact',detailcontact,siteid]);
  }

  hideaffiliatesite(){
    this.choosesite=false;
  }
}
