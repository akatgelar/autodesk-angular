import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditOrgAttributesEPDBComponent } from './edit-orgattributes-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

export const EditOrgAttributesEPDBRoutes: Routes = [
  {
    path: '',
    component: EditOrgAttributesEPDBComponent,
    data: {
      breadcrumb: 'Manage Databases',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditOrgAttributesEPDBRoutes),
    SharedModule
  ],
  declarations: [EditOrgAttributesEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class EditOrgAttributesEPDBModule { }
