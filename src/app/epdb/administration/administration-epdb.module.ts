import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationEPDBComponent } from './administration-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const AdministrationEPDBRoutes: Routes = [
  {
    path: '',
    component: AdministrationEPDBComponent,
    data: {
      breadcrumb: 'Administration',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdministrationEPDBRoutes),
    SharedModule
  ],
  declarations: [AdministrationEPDBComponent]
})
export class AdministrationEPDBModule { }
