import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceTypeAddEPDBComponent } from './invoice-type-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

export const InvoiceTypeAddEPDBRoutes: Routes = [
  {
    path: '',
    component: InvoiceTypeAddEPDBComponent,
    data: {
      breadcrumb: 'Add New InvoiceType',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(InvoiceTypeAddEPDBRoutes),
    SharedModule
  ],
  declarations: [InvoiceTypeAddEPDBComponent],
  providers:[AppService, AppFormatDate],
})
export class InvoiceTypeAddEPDBModule { }
