import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import { Router } from '@angular/router';
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

@Component({
  selector: 'app-invoice-type-add-epdb',
  templateUrl: './invoice-type-add-epdb.component.html',
  styleUrls: [
    './invoice-type-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class InvoiceTypeAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/InvoiceType';
  myForm: FormGroup;
  messageResult: string = '';
  messageError: string = ''; 
  public activitytype: any;
    
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate) { 

    //validation
    let invoice_type = new FormControl('', Validators.required);
    let activity_type = new FormControl('', Validators.required);
    let invoiced_amount = new FormControl('', Validators.required);
    let invoiced_currency = new FormControl('', Validators.required);
    let contract_start_date = new FormControl('', Validators.required);
    let contract_end_date = new FormControl('', Validators.required);
    let description = new FormControl('', Validators.required);
    let status = new FormControl('', Validators.required);


    this.myForm = new FormGroup({
      invoice_type:invoice_type,
      activity_type:activity_type,
      invoiced_amount:invoiced_amount,
      invoiced_currency:invoiced_currency,
      contract_start_date: contract_start_date,
      contract_end_date:contract_end_date,
      description:description,
      status:status
    });

  }

  ngOnInit() { 

    //get data Activity Type
    var data = '';
    this.service.httpClientGet('api/ActivityType',data)
    .subscribe(result => {
      if(result=="Not found"){
          this.service.notfound();
          this.activitytype = null;
      }
      else{
          this.activitytype = result; 
      }  
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

  }

  modelPopup1: NgbDateStruct;  
  modelPopup2: NgbDateStruct; 

  //submit form
  submitted: boolean;
  onSubmit() {
    this.submitted = true;

    let login_val = this.myForm.value; 
    login_val.contract_start_date = this.formatdate.dateCalendarToYMD(login_val.contract_start_date); 
    login_val.contract_end_date = this.formatdate.dateCalendarToYMD(login_val.contract_end_date);  
    login_val.cdate = this.formatdate.dateJStoYMD(new Date());
    login_val.mdate = this.formatdate.dateJStoYMD(new Date());
    login_val.cuid = "admin";
    login_val.muid = "admin";

    let data = JSON.stringify(login_val); 

    this.service.httpClientPost(this._serviceUrl, data)
    .subscribe(result => {
      let tmpData : any = result;
        this.messageResult = tmpData;  
        var resource = result; 
        if(resource['code'] == '1') {
          this.service.openSuccessSwal(resource['name']); 
          this.router.navigate(['/epdb/invoice-type/invoice-type-list']);
        }
        else{
          swal(
            'Information!',
            "Insert Data Failed",
            'error'
          );
        }
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  } 
}
