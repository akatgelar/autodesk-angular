import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-products-category-update-epdb',
    templateUrl: './products-category-update-epdb.component.html',
    styleUrls: [
        './products-category-update-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProductsCategoryUpdateEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Product';
    public dataproductcategory: any;
    editproductcategoryform: FormGroup;
    id: string = '';
    public loading = false;
    public useraccesdata: any;

    constructor(private route: ActivatedRoute, private router: Router, private service: AppService, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let familyId = new FormControl('', Validators.required);
        let familyName = new FormControl('', Validators.required);
        let Status = new FormControl('');

        this.editproductcategoryform = new FormGroup({
            familyId: familyId,
            familyName: familyName,
            Status: Status
        });

    }

    ngOnInit() {

        var data = '';
        this.id = this.route.snapshot.params['id'];
        this.service.httpClientGet('api/Product/ProductFamilies/' + this.id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dataproductcategory = '';
                }
                else {
                    this.dataproductcategory = result;
                    this.editproductcategoryform.patchValue({
                        familyId: this.dataproductcategory.familyId,
                        familyName: this.dataproductcategory.familyName
                    });
                }
            },
                error => {
                    this.service.errorserver();
                    this.dataproductcategory = '';
                });

    }

    onSubmitProductCategoryUpdate() {
        this.editproductcategoryform.controls['familyId'].markAsTouched();
        this.editproductcategoryform.controls['familyName'].markAsTouched();

        if (this.editproductcategoryform.valid) {
            this.loading = true;
            this.editproductcategoryform.value.Status = "A";
            this.editproductcategoryform.value.cuid = this.useraccesdata.ContactName;
            this.editproductcategoryform.value.muid = this.useraccesdata.ContactName;
            this.editproductcategoryform.value.UserId = this.useraccesdata.UserId;
            //convert object to json
            let data = JSON.stringify(this.editproductcategoryform.value);
            this.service.httpCLientPut('api/Product/ProductFamilies/'+this.id, data)
            .subscribe(res=>{
                console.log(res)
            });
           // this.service.httpCLientPut('api/Product/ProductFamilies',this.id, data);
            setTimeout(() => {
                //redirect
                this.router.navigate(['/admin/products/list-category']);
                this.loading = false;
            }, 1000);


        }
    }

    resetFormUpdate() {
        this.editproductcategoryform.reset({
            'familyId': this.dataproductcategory.familyId,
            'familyName': this.dataproductcategory.familyName
        });
    }

}
