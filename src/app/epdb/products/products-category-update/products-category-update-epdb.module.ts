import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsCategoryUpdateEPDBComponent } from './products-category-update-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const ProductsCategoryUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: ProductsCategoryUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.product.edit_category',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsCategoryUpdateEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ProductsCategoryUpdateEPDBComponent],
  providers: [AppService]
})
export class ProductsCategoryUpdateEPDBModule { }
