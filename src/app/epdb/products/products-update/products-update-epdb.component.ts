import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-products-update-epdb',
  templateUrl: './products-update-epdb.component.html',
  styleUrls: [
    './products-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ProductsUpdateEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Product';
  updateproduct: FormGroup;
  public data: any;
  public productcategory: any;
  private year;
  public loading = false;
  dropdownListCategory = [];
  selectedItemsCategory = [];
  dropdownSettings = {};
  public showValid = false;
  public useraccesdata: any;
  category = [];
  version;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let productId = new FormControl('');
    let productVersionsId = new FormControl('');
    let productName = new FormControl('', Validators.required);
    let familyId = new FormControl('');
    // let year = new FormControl('');
    let Status = new FormControl('',Validators.required);
    let version = new FormControl('', Validators.required);

    this.updateproduct = new FormGroup({
      productId: productId,
      productVersionsId: productVersionsId,
      productName: productName,
      familyId: familyId,
      // year: year,
      Status: Status,
      version: version
    });

  }

  getFY() {
    var year = '';
    var parent = "FYIndicator";
    var status = "A";
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", year)
      .subscribe(res => {
        this.year = res;
      }, error => {
        this.service.errorserver();
      });
  }

  id: string;
  ngOnInit() {
    this.getFY();
    this.id = this.route.snapshot.params['id'];
    this.version = this.route.snapshot.params['version'];

    let data: any;
    this.service.httpClientGet(this._serviceUrl + "/where/{'ProductId':'" + this.id + "','VersionId':'" + this.version + "'}", data)
      .subscribe(result => {
        // data = result.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
        // data = JSON.parse(data);
        data = result;
        // console.log(data);
        if (data == "Not found") {
          this.service.notfound();
          this.data = '';
        }
        else {
          this.data = data;
          this.buildForm();
        }
      },
        error => {
          this.service.errorserver();
          this.data = '';
        });

    //get data Product Category
    var data1: any;
    this.service.httpClientGet('api/Product/Family', data1)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListCategory = [];
        }
        else {
          this.productcategory = result;
          this.dropdownListCategory = this.productcategory.map((item) => {
            return {
              id: item.familyId,
              itemName: item.familyId
            }
          });
        }
      });
    this.selectedItemsCategory = [];
  }

  onProductCategorySelect(item: any) { this.showValid = false; }
  OnProductCategoryDeSelect(item: any) { this.showValid = false; }
  onProductCategorySelectAll(item: any) { this.showValid = false; }
  onProductCategoryDeSelectAll(item: any) { }

  buildForm(): void {
    let productName = new FormControl(this.data[0].productName, Validators.required);
    let version = new FormControl(this.data[0].Version, Validators.required);
    let Status = new FormControl(this.data[0].Status, Validators.required);
    let productVersionsId = new FormControl(this.data[0].ProductVersionsId);
    let productId = new FormControl(this.data[0].productId);

    this.category = this.data[0].familyId.split(",");
    for (let i = 0; i < this.category.length; i++) {
      this.selectedItemsCategory.push({ id: this.category[i], itemName: this.category[i] });
    }

    this.updateproduct = new FormGroup({
      productId: productId,
      productName: productName,
      version: version,
      Status: Status,
      productVersionsId: productVersionsId
    });
  }

  //submit form
  onSubmit() {
    this.updateproduct.controls['productName'].markAsTouched();
    this.updateproduct.controls['version'].markAsTouched();
    this.updateproduct.controls['Status'].markAsTouched();

    if (this.updateproduct.valid) {
      if (this.selectedItemsCategory.length >= 1) {
        this.loading = true;
        this.updateproduct.value.cuid = this.useraccesdata.ContactName;
        this.updateproduct.value.muid = this.useraccesdata.ContactName;
        this.updateproduct.value.UserId = this.useraccesdata.UserId;

        let selectedTemp = [];
        let n = 0;
        do {
          selectedTemp.push(this.selectedItemsCategory[n].id);
          n += 1;
        } while (n < this.selectedItemsCategory.length);

        //Jika kategori produk sebelumnya tidak ada di hasil pilihan kategori produk yang baru, maka dihapus
        let deleted: any;
        for (let i = 0; i < this.category.length; i++) {
          if (selectedTemp.indexOf(this.category[i]) > -1 == false) {
            //this.service.httpClientDelete(this._serviceUrl + "/DeleteFamilies/" + this.id + "/" + this.category[i], deleted,'','');
            this.service.httpClientDelete(this._serviceUrl + "/DeleteFamilies/" + this.id + "/" + this.category[i], deleted)
              .subscribe(res => {
                deleted = res;
              });
          }
        }

        //Jika hasil pilihan kategori produk yang baru tidak ada di kategori produk sebelumnya, maka ditambahkan
        for (let j = 0; j < selectedTemp.length; j++) {
          if (this.category.indexOf(selectedTemp[j]) > -1 == false) {
            this.updateproduct.value.productId = this.id;
            this.updateproduct.value.familyId = selectedTemp[j];
            let dataFamily = JSON.stringify(this.updateproduct.value);
            this.service.httpClientPost(this._serviceUrl + "/IntoProductFamilies", dataFamily)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
          }
        }

        let updated = JSON.stringify(this.updateproduct.value);

        this.service.httpCLientPut(this._serviceUrl+'/'+this.id, updated)
          .subscribe(res=>{
            console.log(res)
          });

        setTimeout(() => {
          //redirect
          this.router.navigate(['/admin/products/products-list'], { queryParams: { familyId: this.updateproduct.value.familyId, year: this.updateproduct.value.year } });
          this.loading = false;
        }, 1000)
        // this.service.httpCLientPut(this._serviceUrl,this.id, updated);
        // setTimeout(() => {
        //   //redirect
        //   this.router.navigate(['/admin/products/products-list'], { queryParams: { familyId: this.updateproduct.value.familyId, year: this.updateproduct.value.year } });
        //   this.loading = false;
        // }, 1000)
      } else {
        this.showValid = true;
      }
    }
  }

  resetForm() {
    this.updateproduct.reset({
      'productName': this.data.productName,
      'familyId': this.data.familyId
    });
  }
}
