import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-products-category-add-epdb',
    templateUrl: './products-category-add-epdb.component.html',
    styleUrls: [
        './products-category-add-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProductsCategoryAddEPDBComponent implements OnInit {

    public productcategory: any;
    addproductcategoryform: FormGroup;
    public loading = false;
    public useraccesdata: any;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let familyId = new FormControl('', Validators.required);
        let familyName = new FormControl('', Validators.required);
        let Status = new FormControl('');

        this.addproductcategoryform = new FormGroup({
            familyId: familyId,
            familyName: familyName,
            Status: Status
        });

    }

    ngOnInit() { }

    onSubmitProductCategory() {

        this.addproductcategoryform.controls['familyId'].markAsTouched();
        this.addproductcategoryform.controls['familyName'].markAsTouched();

        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.addproductcategoryform.value.familyId))
            return swal('ERROR','Special character not allowed in category id','error')
        if(format.test(this.addproductcategoryform.value.familyName))
            return swal('ERROR','Special character not allowed in category name','error')
        if (this.addproductcategoryform.valid) {
            this.loading = true;
            this.addproductcategoryform.value.Status = "A";

            this.addproductcategoryform.value.cuid = this.useraccesdata.ContactName;
            this.addproductcategoryform.value.UserId = this.useraccesdata.UserId;

            //convert object to json
            let data = JSON.stringify(this.addproductcategoryform.value);

            //post action
            this.service.httpClientPost('api/Product/ProductFamilies', data)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});


            //redirect
            this.router.navigate(['/admin/products/list-category']);
            this.loading = false;

        }
    }

    resetFormAdd() {
        this.addproductcategoryform.reset({
            'familyId': '',
            'familyName': ''
        });
    }

}
