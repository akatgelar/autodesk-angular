import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsCategoryAddEPDBComponent } from './products-category-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const ProductsCategoryAddEPDBRoutes: Routes = [
  {
    path: '',
    component: ProductsCategoryAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.product.add_new_category',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsCategoryAddEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ProductsCategoryAddEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class ProductsCategoryAddEPDBModule { }
