import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ProductsListEPDBComponent } from './products-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterProductPipe } from './products-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const ProductsListEPDBRoutes: Routes = [
  {
    path: '',
    component: ProductsListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.product.product_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ProductsListEPDBComponent, DataFilterProductPipe],
  providers: [AppService, AppFormatDate, DatePipe, SessionService]
})
export class ProductsListEPDBModule { }
