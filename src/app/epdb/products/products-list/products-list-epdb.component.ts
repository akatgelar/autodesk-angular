import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DatePipe } from '@angular/common';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterProduct' })
export class DataFilterProductPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.productName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) || 
                (row.Version.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-products-list-epdb',
    templateUrl: './products-list-epdb.component.html',
    styleUrls: [
        './products-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProductsListEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Product';
    public productcategory: any;
    public data: any;
    public datadetail: any;
    public dataproductcategory: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "productName";
    public sortOrder: string = "asc";
    private year;
    yearArray = [];
    private familyId = "";
    public categoryFound = false;
    useraccessdata: any;
    messageResult: string = '';
    messageError: string = '';
    public loading = false;

    constructor(public session: SessionService, private router: Router, private service: AppService,
        private formatdate: AppFormatDate, private route: ActivatedRoute, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {
        // this.year = "";
        // this.yearArray = [];
        // var today = this.datePipe.transform(new Date(), "dd-MM-yyyy");
        // var dateTemp = today.split('-');
        // var year = parseInt(dateTemp[2]);
        // for (let i = year - 5; i < year + 5; i++) {
        //     this.yearArray.push(i);
        // }

        // var sub: any;
        // sub = this.route.queryParams.subscribe(params => {
        //     this.familyId = params['familyId'] || "";
        //     // this.year = +params['year'] || "";
        // });

        // if (this.familyId != "") {
        //     // this.changetable(this.year);
        //     this.getProductFamily();
        //     this.changetable(this.familyId);
        // } else {
        //     this.getProductFamily();
        // }
        // 

        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.familyId = item.idKategori;
            this.changetable(this.familyId);
        }
        else {
            //Diganti karena product jadinya bisa multiple category
            this.data = null;
            this.getAllProduct();
        }
        this.getProductFamily();

        this.accesAddBtn = this.session.checkAccessButton("admin/products/products-add");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/products/products-update");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/products/products-delete");
    }

    getAllProduct() {
        let products: any;
        this.service.httpClientGet("api/Product/ListProduct", products)
            .subscribe(res => {
                // products = res.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
                // products = JSON.parse(products);
                products =res;
                // console.log(products);
                if (products.length != 0) {
                    this.data = products;
                } else {
                    this.data = null;
                }
            });
    }

    getProductFamily() {
        this.loading = true;
        var data = '';
        this.service.httpClientGet('api/Product/Family', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.productcategory = '';
                    this.loading = false;
                }
                else {
                    this.productcategory = result;
                    this.loading = false;
                }
            },
            error => {
                this.service.errorserver();
                this.productcategory = '';
                this.loading = false;
            });
    }

    // familySelected(val) {
    // val != "" && val != undefined ? this.familyId = val : this.familyId = "";
    // this.categoryFound = false;
    // }

    //action change table based on combobox
    changetable(newvalue) {
        this.loading = true;
        // var data = '';
        // this.service.httpClientGet(this._serviceUrl + "/where/{'familyId':'" + this.familyId + "','Year':'" + newvalue + "'}", data)
        //     .subscribe(result => {
        //         this.data = result;
        //         this.categoryFound = true;
        //     },
        //     error => {
        //         this.service.errorserver();
        //     });
        
        if (newvalue != "") {
            var data: any;
            this.service.httpClientGet(this._serviceUrl + "/where/{'familyId':'" + newvalue + "'}", data)
                .subscribe(result => {
                    // data = result.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
                    // this.data = JSON.parse(data);
                    this.data = result;
                    this.categoryFound = true;
                    this.loading = false;
                });
        } else {
            this.getAllProduct();
            this.loading = false;
        }

        //Buat object untuk filter yang dipilih
        var params =
            {
                idKategori: this.familyId
            }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.datadetail = '';
                }
                else {
                    this.datadetail = result;
                }
            },
            error => {
                this.service.errorserver();
                this.datadetail = '';
            });
    }

    //delete confirm
    openConfirmsSwal(id, productVersionId) {
        let del: any;
        var index = this.data.findIndex(x => x.ProductVersionsId == productVersionId);
        // this.service.httpClientDelete(this._serviceUrl, this.data, id, index);
        // this.loading = true;
        // var cuid = this.useraccessdata.ContactName;
        // var UserId = this.useraccessdata.UserId;
        // this.service.httpClientDelete(this._serviceUrl + "/" + id + "/" + productVersionId + '/' + cuid + '/' + UserId, del,'','');
        // setTimeout(() => {
        //     if (index !== -1) {
        //         this.data.splice(index, 1);
        //         this.changetable(this.familyId);
        //         this.loading = false;
        //     }                                
        // }, 1000); 
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
                this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;
                this.service.httpClientDelete(this._serviceUrl + "/" + id + "/" + productVersionId + '/' + cuid + '/' + UserId, del)
                    .subscribe(result => {
                        var resource = result;
                        if (resource['code'] == '1') {
                            setTimeout(() => {
                                if (index !== -1) {
                                    this.data.splice(index, 1);
                                    this.changetable(this.familyId);
                                    this.loading = false;
                                }                                
                            }, 1000);
                        }
                    });
            }
        }).catch(swal.noop);
    }

}
