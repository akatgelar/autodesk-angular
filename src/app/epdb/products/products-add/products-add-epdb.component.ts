import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { SessionService } from '../../../shared/service/session.service';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-products-add-epdb',
  templateUrl: './products-add-epdb.component.html',
  styleUrls: [
    './products-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ProductsAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Product';
  addproduct: FormGroup;
  private productcategory;
  private year;
  public loading = false;
  public showValid = false;
  dropdownListCategory = [];
  selectedItemsCategory = [];
  dropdownSettings = {};
  available: boolean;
  productID;
  public useraccesdata: any;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let productName = new FormControl('', Validators.required);
    let familyId = new FormControl('', Validators.required);
    // let year = new FormControl('', Validators.required);
    let version = new FormControl('', Validators.required);
    let Status = new FormControl('',Validators.required);

    this.addproduct = new FormGroup({
      productName: productName,
      familyId: familyId,
      version: version,
      Status: Status
    });

  }

  getFY() {
    var year = '';
    var parent = "FYIndicator";
    var status = "A";
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", year)
      .subscribe(res => {
        this.year = res;
      }, error => {
        this.service.errorserver();
      });
  }

  checkAvailable(value) {
    let data: any;
    this.service.httpClientGet(this._serviceUrl + "/CheckAvailable/" + value, data) //hanya untuk mengecek apakah product name sudah ada atau tidak di table products
      .subscribe(res => {
        data = res;
        if (data.jumlah != '0') {
          this.available = true;
          this.productID = data.productId;
        } else {
          this.available = false;
        }
      });
  }

  getCategory() {
    //get data Product Category
    var data: any;
    this.service.httpClientGet('api/Product/Family', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListCategory = [];
        }
        else {
          this.productcategory = result;
          this.dropdownListCategory = this.productcategory.map((item) => {
            return {
              id: item.familyId,
              itemName: item.familyId
            }
          });
        }
      });
    this.selectedItemsCategory = [];
  }

  ngOnInit() {
    // this.getFY();
    this.getCategory();
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };
  }

  onCategorySelect(item: any) { this.showValid = false; }

  OnCategoryDeSelect(item: any) { }

  onCategorySelectAll(items: any) { this.showValid = false; }

  onCategoryDeSelectAll(items: any) { }

  //submit form
  submitted: boolean;
  onSubmit() {
    this.addproduct.controls['productName'].markAsTouched();
    this.addproduct.controls['familyId'].markAsTouched();
    this.addproduct.controls['version'].markAsTouched();
    this.addproduct.controls['Status'].markAsTouched();

    this.submitted = true;

    if (this.addproduct.valid) {
      if (this.selectedItemsCategory.length != 0) {
        this.loading = true;

        let temp = [];
        for (let i = 0; i < this.selectedItemsCategory.length; i++) {
          temp.push(this.selectedItemsCategory[i].id);
        }

        this.addproduct.value.familyId = temp.join(",");
        
        this.addproduct.value.cuid = this.useraccesdata.ContactName;
        this.addproduct.value.UserId = this.useraccesdata.UserId;

        let data = JSON.stringify(this.addproduct.value);
        if (this.available == false) { //cek apakah produk name ada di table products atau tidak
          this.service.httpClientPost(this._serviceUrl, data)
			.subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});		  //jika tidak ada maka insert ke table products, product families, dan product version

        } else { //jika ternyata produk name ada/terdapat di table products
          let dataVersi = ({
            productId: this.productID,
            familyId: temp.join(","),
            version: this.addproduct.value.version,
            cuid: this.addproduct.value.cuid,
            UserId: this.useraccesdata.UserId,
            Status: this.addproduct.value.Status
          });

          let fam_temp: any;
          let ver_temp: any;
          this.service.httpClientGet(this._serviceUrl + "/CheckAvailableFamily/" + this.productID,fam_temp) //cek terlebih dahulu familyId nya ada atau tidak
            .subscribe(res1 => {
              fam_temp = res1;
              if(fam_temp.length == 0){ //jika familyId tidak ada di table product families
                this.service.httpClientPost(this._serviceUrl + "/AddIntoPF", JSON.stringify(dataVersi))
				 .subscribe(result => {
					console.log("success");
				}, error => {
					this.service.errorserver();
				});		//insert ke table product families
              } else{
                let f = [];
                let z = [];
                for(let x = 0; x < fam_temp.length; x++){
                  f.push(fam_temp[x].familyId); //masukan family id yg ada ke temp array
                }

                for(let y = 0; y < temp.length; y++){
                  if(!f.includes(temp[y])){ //jika family id yg dipilih sama dengan yg ada di table product families, maka hapus dari temp array. Jika tidak, abaikan
                    z.push(temp[y]);
                  }
                }

                if(z.length != 0){ //cek dulu, temp array kosong atau engga. Jangan sampai nyimpen data family id string kosong
                  dataVersi.familyId = z.join(","); //perbarui property familyId dari objek dataVersi
                  this.service.httpClientPost(this._serviceUrl + "/AddIntoPF", JSON.stringify(dataVersi))
					 .subscribe(result => {
						console.log("success");
					}, error => {
						this.service.errorserver();
					});				//insert ke table product families
                }
              }

              //cek apakah product version ada atau tidak di table product version
              this.service.httpClientGet(this._serviceUrl + "/CheckAvailableVersion/" + this.productID + "/" + this.addproduct.value.version,ver_temp)
                .subscribe(res2 => {
                  ver_temp = res2;
                  if(ver_temp.length == 0){
                    this.service.httpClientPost(this._serviceUrl + "/ProductVersions", JSON.stringify(dataVersi))
						.subscribe(result => {
								console.log("success");
							}, error => {
								this.service.errorserver();
							});//jika tidak ada, maka insert ke table product version
						}
					});
            });
        }

        setTimeout(() => {
          //redirect
          // this.router.navigate(['/admin/products/products-list'], { queryParams: { familyId: this.addproduct.value.familyId } });
          this.router.navigate(['/admin/products/products-list']);
          this.loading = false;
        }, 1000)
      } else {
        this.showValid = true;
      }
    }
  }

  resetForm() {
    this.addproduct.reset({
      'familyId': '',
      'year': '',
      'productName': ''
    });
  }
}
