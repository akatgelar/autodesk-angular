import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterCategory' })
export class DataFilterCategoryPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => (row.familyId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.familyName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-products-category-list-epdb',
    templateUrl: './products-category-list-epdb.component.html',
    styleUrls: [
        './products-category-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProductsCategoryListEPDBComponent implements OnInit {

    private data;
    private _serviceUrl = "api/Product";
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "familyName";
    public sortOrder: string = "asc";
    useraccessdata: any;
    messageResult: string = '';
    messageError: string = '';
    public loading = false;

    constructor(public session: SessionService, private router: Router, private service: AppService) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
    }

    getCategory() {
        this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl + "/ProductFamilies", data)
            .subscribe(res => {
                if (res == "Not found") {
                    this.service.notfound();
                    this.loading = false;
                } else {
                    this.data = res;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
    }

    openConfirmsSwal(id) {
        // var index = this.data.findIndex(x => x.familyId == id);
        // setTimeout(() => {
        //     this.service.httpClientDelete(this._serviceUrl + "/ProductFamilies", this.data, id, index);            
        // }, 1000);
        // this.loading = true;
        // var cuid = this.useraccessdata.ContactName;
        // var UserId = this.useraccessdata.UserId;

        // var data = '';
        // this.service.httpClientDelete(this._serviceUrl + '/ProductFamilies' + '/' + id + '/' + cuid + '/' + UserId, data,'','');
        // this.getCategory();
        // setTimeout(function () {
        //     this.router.navigate(['/admin/products/list-category']);
        //     this.loading = false;
        // }, 1000);
        // this.loading = false;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
               this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;

        var data = '';

                this.service.httpClientDelete(this._serviceUrl + '/ProductFamilies' + '/' + id + '/' + cuid + '/' + UserId, data)
                    .subscribe(result => {
                        let tmpData : any = result;
                        this.messageResult = tmpData;
                        var resource = result;
                        if (resource['code'] == '1') {
                            this.getCategory();
                            setTimeout(function () {
                                this.router.navigate(['/admin/products/list-category']);
                                this.loading = false;
                            }, 1000);
                        }
                        else {
                            swal(
                                'Information!',
                                "Delete Data Failed",
                                'error'
                            );
                            this.loading = false;
                        }
                    },
                        error => {
                            this.messageError = <any>error
                            this.service.errorserver();
                            this.loading = false;
                        });
            }
        }).catch(swal.noop);
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {
        this.getCategory();
        this.getCategory();

        this.accesAddBtn = this.session.checkAccessButton("admin/products/add-category");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/products/products-category-update");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/products/delete-category");
    }
}
