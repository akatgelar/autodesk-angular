import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsCategoryListEPDBComponent } from './products-category-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterCategoryPipe } from './products-category-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const ProductsCategoryListEPDBRoutes: Routes = [
    {
        path: '',
        component: ProductsCategoryListEPDBComponent,
        data: {
            breadcrumb: 'epdb.admin.product.category_list',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProductsCategoryListEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [ProductsCategoryListEPDBComponent,DataFilterCategoryPipe],
    providers: [AppService, SessionService]
})
export class ProductsCategoryListEPDBModule { }
