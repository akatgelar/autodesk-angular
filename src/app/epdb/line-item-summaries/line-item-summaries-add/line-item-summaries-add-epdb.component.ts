import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-line-item-summaries-add-epdb',
  templateUrl: './line-item-summaries-add-epdb.component.html',
  styleUrls: [
    './line-item-summaries-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class LineItemSummariesAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/LineItemSummary';
  messageResult: string = '';
  messageError: string = '';
  addlineitemsumm: FormGroup;
  public activitytype:any;

  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate) { 

    //validation
    let summary = new FormControl('', Validators.required);
    let description = new FormControl('', Validators.required);
    let quantity = new FormControl('', [Validators.required, CustomValidators.number]);
    let unit_price = new FormControl('', [Validators.required, CustomValidators.number]);
    let vat_rate = new FormControl('', [Validators.required, CustomValidators.number]);
    let activity_type = new FormControl('', Validators.required);
    this.addlineitemsumm = new FormGroup({
      summary:summary,
      description:description,
      quantity:quantity,
      unit_price:unit_price,
      vat_rate:vat_rate,
      activity_type:activity_type
    });

  }

  ngOnInit() {

    //get data Activity Type
    var data = '';
    this.service.httpClientGet('api/ActivityType',data)
    .subscribe(result => {
      if(result=="Not found"){
          this.service.notfound();
          this.activitytype = null;
      }
      else{
          this.activitytype = result; 
      } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

  }

  //submit form
  submitted: boolean;
  onSubmit() {
    this.submitted = true;

    this.addlineitemsumm.value.cdate = this.formatdate.dateJStoYMD(new Date());
    this.addlineitemsumm.value.mdate = this.formatdate.dateJStoYMD(new Date());
    this.addlineitemsumm.value.cuid = "admin";
    this.addlineitemsumm.value.muid = "admin";

    //convert object to json
    let data = JSON.stringify(this.addlineitemsumm.value); 

    //post action
    this.service.httpClientPost(this._serviceUrl,data)
    .subscribe(result => {
        var resource =result;
        if(resource['code'] == '1') {
            this.service.openSuccessSwal(resource['name']); 
        }
        else{
            swal(
              'Information!',
              "Insert Data Failed",
              'error'
            );
        }  
        this.router.navigate(['/epdb/line-item-summaries/line-item-summaries-list']);
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  } 

}
