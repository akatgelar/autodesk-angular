import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineItemSummariesAddEPDBComponent } from './line-item-summaries-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const LineItemSummariesAddEPDBRoutes: Routes = [
  {
    path: '',
    component: LineItemSummariesAddEPDBComponent,
    data: {
      breadcrumb: 'Add New Line Item Summaries',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LineItemSummariesAddEPDBRoutes),
    SharedModule
  ],
  declarations: [LineItemSummariesAddEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class LineItemSummariesAddEPDBModule { }
