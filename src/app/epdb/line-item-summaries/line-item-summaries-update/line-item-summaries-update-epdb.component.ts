import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-line-item-summaries-update-epdb',
  templateUrl: './line-item-summaries-update-epdb.component.html',
  styleUrls: [
    './line-item-summaries-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class LineItemSummariesUpdateEPDBComponent implements OnInit {

  private _serviceUrl = 'api/LineItemSummary';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  updatelineitemsumm: FormGroup;
  public activitytype:any;
  
  constructor(private service:AppService, private route:ActivatedRoute, private router: Router) { 
    let line_item_summary_id = new FormControl();
    let summary = new FormControl();
    let description = new FormControl();
    let quantity = new FormControl();
    let unit_price = new FormControl();
    let vat_rate = new FormControl();
    let activity_type = new FormControl();
    this.updatelineitemsumm = new FormGroup({
      line_item_summary_id:line_item_summary_id,
      summary:summary,
      description:description,
      quantity:quantity,
      unit_price:unit_price,
      vat_rate:vat_rate,
      activity_type:activity_type
    });

  }

  id:string;
  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    console.log("id ->"+this.id);
    var data = '';
    this.service.httpClientGet(this._serviceUrl+"/"+this.id, data)
    .subscribe(result => {
      if(result=="Not found"){
          this.service.notfound();
          this.data = null;
      }
      else{
          this.data = result; 
          this.buildForm();
      } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

  }

  //update using api
  buildForm(): void {
    let line_item_summary_id = new FormControl(this.data.line_item_summary_id);
    let summary = new FormControl(this.data.summary);
    let description = new FormControl(this.data.description);
    let quantity = new FormControl(this.data.quantity);
    let unit_price = new FormControl(this.data.unit_price);
    let vat_rate = new FormControl(this.data.vat_rate);
    let activity_type = new FormControl(this.data.activity_type);
    this.updatelineitemsumm = new FormGroup({
      line_item_summary_id:line_item_summary_id,
      summary:summary,
      description:description,
      quantity:quantity,
      unit_price:unit_price,
      vat_rate:vat_rate,
      activity_type:activity_type
    });
  }

  //submit form
  submitted:boolean;
  onSubmit(){
    this.submitted = true;

    let data = JSON.stringify(this.updatelineitemsumm.value);

    let id = JSON.parse(data).line_item_summary_id;

    this.service.httpCLientPut(this._serviceUrl+"/"+id,data)
      .subscribe(result => {
        var resource = result;
        if(resource['code'] == '1') {
          this.service.openSuccessSwal(resource['name']); 
        }
        else{
            swal(
              'Information!',
              "Update Data Failed",
              'error'
            );
        }
        this.router.navigate(['/epdb/line-item-summaries/line-item-summaries-list']);
      },
      error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
  }

}
