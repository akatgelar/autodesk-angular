import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineItemSummariesUpdateEPDBComponent } from './line-item-summaries-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const LineItemSummariesUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: LineItemSummariesUpdateEPDBComponent,
    data: {
      breadcrumb: 'Update Line Item Summaries',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LineItemSummariesUpdateEPDBRoutes),
    SharedModule
  ],
  declarations: [LineItemSummariesUpdateEPDBComponent],
  providers:[AppService]
})
export class LineItemSummariesUpdateEPDBModule { }
