import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineItemSummariesListEPDBComponent } from './line-item-summaries-list-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const LineItemSummariesListEPDBRoutes: Routes = [
  {
    path: '',
    component: LineItemSummariesListEPDBComponent,
    data: {
      breadcrumb: 'Line Item Summaries List',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LineItemSummariesListEPDBRoutes),
    SharedModule
  ],
  declarations: [LineItemSummariesListEPDBComponent],
  providers:[AppService]
})
export class LineItemSummariesListEPDBModule { }
