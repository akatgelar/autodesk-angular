import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-line-item-summaries-list-epdb',
  templateUrl: './line-item-summaries-list-epdb.component.html',
  styleUrls: [
    './line-item-summaries-list-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class LineItemSummariesListEPDBComponent implements OnInit {

  private _serviceUrl = 'api/LineItemSummary';
  messageResult: string = '';
  messageError: string = '';   
  public data: any;
  public datadetail:any;
  public activitytype:any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";

  constructor(private service:AppService) { }

  ngOnInit() {

    //get data Activity Type
    var data = '';
    this.service.httpClientGet('api/ActivityType',data)
    .subscribe(result => { 
      if(result=="Not found"){
          this.service.notfound();
          this.activitytype = null;
      }
      else{
          this.activitytype = result; 
      } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

  }

  //cange table based on combobox
  title : string = "";

  changetable(newvalue){
    if(newvalue != ""){
        this.title = newvalue;
        //get data action
        var data = '';
        this.service.httpClientGet(this._serviceUrl+"/where/{'activity_type':'"+newvalue+"'}",data)
        .subscribe(result => {
          if(result=="Not found"){
            this.service.notfound();
            this.data = null;
          }
          else{
              this.data = result;  
          }  
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });
    }
    else{
        this.title = "";
        this.data = null;
    }
  }

  //delete confirm
  openConfirmsSwal(id) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
    .then(result => {  
      if (result == true) {
        var data = '';
        this.service.httpClientDelete(this._serviceUrl+'/'+id, data)
        .subscribe(result => {
          let tmpData : any = result;
            this.messageResult = tmpData;  
            var resource = result; 
            if(resource['code'] == '1') {
              this.service.openSuccessSwal(resource['name']); 
              var index = this.data.findIndex(x => x.line_item_summary_id == id);
              if (index !== -1) {
                  this.data.splice(index, 1);
              }    
            }
            else{
              swal(
                'Information!',
                "Delete Data Failed",
                'error'
              );
            }
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });
      }
    }).catch(swal.noop);
  }

  //view detail
  viewdetail(id) {
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+id, data)
    .subscribe(result => {
      this.datadetail = result;  
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  }
}
