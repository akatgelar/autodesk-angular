import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EPDBRoutes } from './epdb.routing';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EPDBRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class EPDBModule {}
