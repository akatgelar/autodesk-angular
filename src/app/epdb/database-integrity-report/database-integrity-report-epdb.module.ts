import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatabaseIntegrityReportEPDBComponent } from './database-integrity-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const DatabaseIntegrityReportEPDBRoutes: Routes = [
  {
    path: '',
    component: DatabaseIntegrityReportEPDBComponent,
    data: {
      breadcrumb: 'Database Integrity Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DatabaseIntegrityReportEPDBRoutes),
    SharedModule
  ],
  declarations: [DatabaseIntegrityReportEPDBComponent]
})
export class DatabaseIntegrityReportEPDBModule { }
