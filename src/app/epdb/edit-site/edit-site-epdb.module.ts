import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditSiteEPDBComponent } from './edit-site-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const EditSiteEPDBRoutes: Routes = [
  {
    path: '',
    component: EditSiteEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.site.add_edit.edit_site',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditSiteEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [EditSiteEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class EditSiteEPDBModule { }
