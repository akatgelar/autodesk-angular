import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { stream } from 'xlsx/types';

@Component({
  selector: 'app-edit-site-epdb',
  templateUrl: './edit-site-epdb.component.html',
  styleUrls: [
    './edit-site-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class EditSiteEPDBComponent implements OnInit {

  @ViewChild('fileUploader') fileUploader 
  private attachments = []
  private attachments_remove = []
  private _serviceUrl = 'api/MainSite';
  messageError: string = '';
  editsite: FormGroup;
  public datadetail: any;
  public datacountry: any;
  public datastate1: any;
  public datastate2: any;
  public datastate3: any;
  public datastate4: any;
  public partnertype: any;
  dropdownListPartnerType = [];
  selectedItemsPartnerType = [];
  arrpartnertype = [];
  arrpartnertypetmp = [];
  dropdownSettingsPartnerType = {};
  public partnertypestatus: any;
  public useraccesdata: any;
  public loading = false;

  constructor(private session: SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let SiteId = new FormControl('');
    let ATCSiteId = new FormControl('');
    let OrgId = new FormControl('');
    let SiteStatus_retired = new FormControl('');
    let SiteName = new FormControl('');
    let SiebelSiteName = new FormControl('');
    let EnglishSiteName = new FormControl('');
    let CommercialSiteName = new FormControl('');
    let Workstations = new FormControl('', Validators.required);
    // let MagellanId = new FormControl('');
    let SAPNumber_retired = new FormControl('');
    let SAPShipTo_retired = new FormControl('');
    let SiteTelephone = new FormControl('', Validators.required);
    let SiteFax = new FormControl('');
    let SiteEmailAddress = new FormControl('', Validators.required);
    let SiteWebAddress = new FormControl('');
    // let SiteDepartment = new FormControl('');
    let SiteAddress1 = new FormControl('', Validators.required);
    let SiteAddress2 = new FormControl('');
    let SiteAddress3 = new FormControl('');
    let SiteCity = new FormControl('', Validators.required);
    let SiteStateProvince = new FormControl('');
    let SiteCountryCode = new FormControl('', Validators.required);
    let SitePostalCode = new FormControl('');
    // let MailingDepartment = new FormControl('');
    let MailingAddress1 = new FormControl('', Validators.required);
    let MailingAddress2 = new FormControl('');
    let MailingAddress3 = new FormControl('');
    let MailingCity = new FormControl('', Validators.required);
    let MailingStateProvince = new FormControl('');
    let MailingCountryCode = new FormControl('', Validators.required);
    let MailingPostalCode = new FormControl('');
    // let ShippingDepartment = new FormControl('');
    let ShippingAddress1 = new FormControl('', Validators.required);
    let ShippingAddress2 = new FormControl('');
    let ShippingAddress3 = new FormControl('');
    let ShippingCity = new FormControl('', Validators.required);
    let ShippingStateProvince = new FormControl('');
    let ShippingCountryCode = new FormControl('', Validators.required);
    let ShippingPostalCode = new FormControl('');
    let SiteAdminFirstName = new FormControl('', Validators.required);
    let SiteAdminLastName = new FormControl('', Validators.required);
    let SiteAdminEmailAddress = new FormControl('', Validators.required);
    let SiteAdminTelephone = new FormControl('', Validators.required);
    let SiteAdminFax = new FormControl('');
    let SiteManagerFirstName = new FormControl('', Validators.required);
    let SiteManagerLastName = new FormControl('', Validators.required);
    let SiteManagerEmailAddress = new FormControl('', Validators.required);
    let SiteManagerTelephone = new FormControl('', Validators.required);
    let SiteManagerFax = new FormControl('');
    let AdminNotes = new FormControl('');
    let Status = new FormControl();
    let DateAdded = new FormControl();
    let AddedBy = new FormControl();
    let DateLastAdmin = new FormControl();
    let LastAdminBy = new FormControl();
    let CSOLocationUUID = new FormControl();
    let CSOLocationName = new FormControl();
    let CSOLocationNumber = new FormControl();
    let CSOAuthorizationCodes = new FormControl();
    let CSOUpdatedOn = new FormControl();
    let CSOVersion = new FormControl();
    let CSOpartner_type = new FormControl();
    let Files = new FormControl();

    this.editsite = new FormGroup({
      SiteId: SiteId,
      ATCSiteId: ATCSiteId,
      OrgId: OrgId,
      SiteStatus_retired: SiteStatus_retired,
      SiteName: SiteName,
      SiebelSiteName: SiebelSiteName,
      EnglishSiteName: EnglishSiteName,
      CommercialSiteName: CommercialSiteName,
      Workstations: Workstations,
      // MagellanId:MagellanId,
      SAPNumber_retired: SAPNumber_retired,
      SAPShipTo_retired: SAPShipTo_retired,
      SiteTelephone: SiteTelephone,
      SiteFax: SiteFax,
      SiteEmailAddress: SiteEmailAddress,
      SiteWebAddress: SiteWebAddress,
      // SiteDepartment:SiteDepartment,
      SiteAddress1: SiteAddress1,
      SiteAddress2: SiteAddress2,
      SiteAddress3: SiteAddress3,
      SiteCity: SiteCity,
      SiteStateProvince: SiteStateProvince,
      SiteCountryCode: SiteCountryCode,
      SitePostalCode: SitePostalCode,
      // MailingDepartment:MailingDepartment,
      MailingAddress1: MailingAddress1,
      MailingAddress2: MailingAddress2,
      MailingAddress3: MailingAddress3,
      MailingCity: MailingCity,
      MailingStateProvince: MailingStateProvince,
      MailingCountryCode: MailingCountryCode,
      MailingPostalCode: MailingPostalCode,
      // ShippingDepartment:ShippingDepartment,
      ShippingAddress1: ShippingAddress1,
      ShippingAddress2: ShippingAddress2,
      ShippingAddress3: ShippingAddress3,
      ShippingCity: ShippingCity,
      ShippingStateProvince: ShippingStateProvince,
      ShippingCountryCode: ShippingCountryCode,
      ShippingPostalCode: ShippingPostalCode,
      SiteAdminFirstName: SiteAdminFirstName,
      SiteAdminLastName: SiteAdminLastName,
      SiteAdminEmailAddress: SiteAdminEmailAddress,
      SiteAdminTelephone: SiteAdminTelephone,
      SiteAdminFax: SiteAdminFax,
      SiteManagerFirstName: SiteManagerFirstName,
      SiteManagerLastName: SiteManagerLastName,
      SiteManagerEmailAddress: SiteManagerEmailAddress,
      SiteManagerTelephone: SiteManagerTelephone,
      SiteManagerFax: SiteManagerFax,
      AdminNotes: AdminNotes,
      Status: Status,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy,
      CSOLocationUUID: CSOLocationUUID,
      CSOLocationName: CSOLocationName,
      CSOLocationNumber: CSOLocationNumber,
      CSOAuthorizationCodes: CSOAuthorizationCodes,
      CSOUpdatedOn: CSOUpdatedOn,
      CSOVersion: CSOVersion,
      CSOpartner_type: CSOpartner_type,
      Files:Files
    });

  }

  ngOnInit() {

    //get data site
    var id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        this.datadetail = result;
        if (this.datadetail != null) {
          if (this.datadetail.AdminNotes != null && this.datadetail.AdminNotes != '') {
            this.datadetail.AdminNotes = this.datadetail.AdminNotes.replace("&#10;", " ");
            this.datadetail.AdminNotes = this.service.decoder(this.datadetail.AdminNotes);
          }
          if (this.datadetail.OrgName != null && this.datadetail.OrgName != '') {
            this.datadetail.OrgName = this.service.decoder(this.datadetail.OrgName);
          }
          if (this.datadetail.SiteName != null && this.datadetail.SiteName != '') {
            this.datadetail.SiteName = this.service.decoder(this.datadetail.SiteName);
          }
          if (this.datadetail.EnglishSiteName != null && this.datadetail.EnglishSiteName != '') {
            this.datadetail.EnglishSiteName = this.service.decoder(this.datadetail.EnglishSiteName);
          }
          if (this.datadetail.CommercialSiteName != null && this.datadetail.CommercialSiteName != '') {
            this.datadetail.CommercialSiteName = this.service.decoder(this.datadetail.CommercialSiteName);
          }
          if (this.datadetail.SiteAddress1 != null && this.datadetail.SiteAddress1 != '') {
            this.datadetail.SiteAddress1 = this.service.decoder(this.datadetail.SiteAddress1);
          }
          if (this.datadetail.SiteAddress2 != null && this.datadetail.SiteAddress2 != '') {
            this.datadetail.SiteAddress2 = this.service.decoder(this.datadetail.SiteAddress2);
          }
          if (this.datadetail.SiteAddress3 != null && this.datadetail.SiteAddress3 != '') {
            this.datadetail.SiteAddress3 = this.service.decoder(this.datadetail.SiteAddress3);
          }
          if (this.datadetail.SiteCity != null && this.datadetail.SiteCity != '') {
            this.datadetail.SiteCity = this.service.decoder(this.datadetail.SiteCity);
          }
          if (this.datadetail.SitePostalCode != null && this.datadetail.SitePostalCode != '') {
            this.datadetail.SitePostalCode = this.service.decoder(this.datadetail.SitePostalCode);
          }
          if (this.datadetail.MailingAddress1 != null && this.datadetail.MailingAddress1 != '') {
            this.datadetail.MailingAddress1 = this.service.decoder(this.datadetail.MailingAddress1);
          }
          if (this.datadetail.MailingAddress2 != null && this.datadetail.MailingAddress2 != '') {
            this.datadetail.MailingAddress2 = this.service.decoder(this.datadetail.MailingAddress2);
          }
          if (this.datadetail.MailingAddress3 != null && this.datadetail.MailingAddress3 != '') {
            this.datadetail.MailingAddress3 = this.service.decoder(this.datadetail.MailingAddress3);
          }
          if (this.datadetail.MailingCity != null && this.datadetail.MailingCity != '') {
            this.datadetail.MailingCity = this.service.decoder(this.datadetail.MailingCity);
          }
          if (this.datadetail.MailingPostalCode != null && this.datadetail.MailingPostalCode != '') {
            this.datadetail.MailingPostalCode = this.service.decoder(this.datadetail.MailingPostalCode);
          }
          if (this.datadetail.ShippingAddress1 != null && this.datadetail.ShippingAddress1 != '') {
            this.datadetail.ShippingAddress1 = this.service.decoder(this.datadetail.ShippingAddress1);
          }
          if (this.datadetail.ShippingAddress2 != null && this.datadetail.ShippingAddress2 != '') {
            this.datadetail.ShippingAddress2 = this.service.decoder(this.datadetail.ShippingAddress2);
          }
          if (this.datadetail.ShippingAddress3 != null && this.datadetail.ShippingAddress3 != '') {
            this.datadetail.ShippingAddress3 = this.service.decoder(this.datadetail.ShippingAddress3);
          }
          if (this.datadetail.ShippingCity != null && this.datadetail.ShippingCity != '') {
            this.datadetail.ShippingCity = this.service.decoder(this.datadetail.ShippingCity);
          }
          if (this.datadetail.ShippingPostalCode != null && this.datadetail.ShippingPostalCode != '') {
            this.datadetail.ShippingPostalCode = this.service.decoder(this.datadetail.ShippingPostalCode);
          }
          if (this.datadetail.SiteManagerFirstName != null && this.datadetail.SiteManagerFirstName != '') {
            this.datadetail.SiteManagerFirstName = this.service.decoder(this.datadetail.SiteManagerFirstName);
          }
          if (this.datadetail.SiteManagerLastName != null && this.datadetail.SiteManagerLastName != '') {
            this.datadetail.SiteManagerLastName = this.service.decoder(this.datadetail.SiteManagerLastName);
          }
          if (this.datadetail.SiteAdminFirstName != null && this.datadetail.SiteAdminFirstName != '') {
            this.datadetail.SiteAdminFirstName = this.service.decoder(this.datadetail.SiteAdminFirstName);
          }
          if (this.datadetail.SiteAdminLastName != null && this.datadetail.SiteAdminLastName != '') {
            this.datadetail.SiteAdminLastName = this.service.decoder(this.datadetail.SiteAdminLastName);
          }
          if (this.datadetail.SiteWebAddress != null && this.datadetail.SiteWebAddress != '') {
            this.datadetail.SiteWebAddress = this.service.decoder(this.datadetail.SiteWebAddress);
          }
         
        }
        this.attachments = this.datadetail.Files || []
       
        this.buildForm();
        this.getpartnertypeselected(this.datadetail.SiteId)
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
          this.datadetail = null;
        });

    //get data country
    var data = '';
    this.service.httpClientGet('api/Countries', data)
      .subscribe(result => {
        this.datacountry = result;
      },
        error => {
          this.service.errorserver();
        });

    // Partner Type
    var data = '';
    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListPartnerType = null;
        }
        else {
          this.dropdownListPartnerType = Object.keys(result).map(function (Index) {
            return {
              id: result[Index].RoleId,
              itemName: result[Index].RoleName
            }
          })
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
    this.selectedItemsPartnerType = [];

    //setting dropdown
    this.dropdownSettingsPartnerType = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false
    };

    this.getSubPartnerType1();
    this.getSubPartnerType58();
    this.getSubPartnerType60();
    this.getSubPartnerType55();
    this.getSubPartnerType54();

    //get partner type status
    this.service.httpClientGet('api/Roles/Status', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.partnertypestatus = '';
        }
        else {
          this.partnertypestatus = result
        }
      },
        error => {
          this.service.errorserver();
          this.partnertypestatus = '';
        });
  }

  //get partner type selected
  getpartnertypeselected(id) {
    var data = '';
    this.service.httpClientGet('api/Roles/SiteRoles/' + id, data)
      .subscribe(result => {
        // this.partnertype = JSON.parse(result).map((item) => {
        //   return {
        //     id: item.RoleId,
        //     itemName: item.RoleName,
        //     PartnerTypeId: item.SiteRoleId,
        //     Status: item.Status,
        //     AccreditationDate: item.AccreditationDate,
        //     CSN: item.CSN,
        //     ParentCSN: item.ParentCSN,
        //     UParent_CSN: item.UParent_CSN,
        //     StatusName: item.KeyValue,
        //     SubPartnerTypeId: item.RoleParamId,
        //     SubPartnerType: item.ParamValue
        //   }
        // })
        this.partnertype = Object.keys(result).map(function (Index) {
          return {
            id: result[Index].RoleId,
            itemName: result[Index].RoleName,
            PartnerTypeId: result[Index].SiteRoleId,
            Status: result[Index].Status,
            AccreditationDate: result[Index].AccreditationDate,
            CSN: result[Index].CSN,
            ParentCSN: result[Index].ParentCSN,
            UParent_CSN: result[Index].UParent_CSN,
            StatusName: result[Index].KeyValue,
            SubPartnerTypeId: result[Index].RoleParamId,
            SubPartnerType: result[Index].ParamValue
          }
        })
        this.selectedItemsPartnerType = this.partnertype;
        for (var i = 0; i < this.partnertype.length; i++) {
          this.arrpartnertypetmp.push([this.partnertype[i].id, "exist", this.partnertype[i].PartnerTypeId]);
          this.arrpartnertype.push(
            [
              this.partnertype[i].id,
              "exist",
              this.partnertype[i].PartnerTypeId,
              this.partnertype[i].itemName,
              this.partnertype[i].Status,
              this.partnertype[i].AccreditationDate,
              this.partnertype[i].CSN,
              this.partnertype[i].ParentCSN,
              this.partnertype[i].UParent_CSN,
              this.partnertype[i].StatusName,
              this.partnertype[i].SubPartnerTypeId,
              this.partnertype[i].SubPartnerType
            ]
          );
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  public partnertypedata: any
  public subpartnertype_1: any;
  public subpartnertype_58: any;
  public subpartnertype_60: any;
  public subpartnertype_55: any;
  public subpartnertype_54: any;
  public subpartnertype_61: any;
  getSubPartnerType1() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"ATC"}', data)
      .subscribe(result => {
        this.subpartnertype_1 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType58() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"AAP"}', data)
      .subscribe(result => {
        this.subpartnertype_58 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType60() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"MTP"}', data)
      .subscribe(result => {
        this.subpartnertype_60 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType55() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"MED"}', data)
      .subscribe(result => {
        this.subpartnertype_55 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType54() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"MER"}', data)
      .subscribe(result => {
        this.subpartnertype_54 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType61() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"CTC"}', data)
      .subscribe(result => {
        this.subpartnertype_61 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  onItemSelect(item: any) {
    var index = this.arrpartnertype.findIndex(x => x[0] == item.id);
    if (index == -1) {
      this.arrpartnertype.push([item.id, "insert", "", item.itemName]);
    } else {
      this.arrpartnertype[index][1] = "exist";
      $('#SubPartnerType_' + item.id).prop('disabled', false);
      $('#Status_' + item.id).prop('disabled', false);
      $('#InitialAccreditationDate_' + item.id).prop('disabled', false);
      $('#LocationCSN_' + item.id).prop('disabled', false);
      $('#ParentCSN_' + item.id).prop('disabled', false);
      $('#UltimateParentCSN_' + item.id).prop('disabled', false);
    }
  }
  OnItemDeSelect(item: any) {
    var indextmp = this.arrpartnertypetmp.findIndex(x => x[0] == item.id);
    var index = this.arrpartnertype.findIndex(x => x[0] == item.id);
    console.log(indextmp + " -> ")
    if (index != -1 && indextmp != -1) {
      this.arrpartnertype[index][1] = "delete";
      $('#SubPartnerType_' + item.id).prop('disabled', true);
      $('#Status_' + item.id).prop('disabled', true);
      $('#InitialAccreditationDate_' + item.id).prop('disabled', true);
      $('#LocationCSN_' + item.id).prop('disabled', true);
      $('#ParentCSN_' + item.id).prop('disabled', true);
      $('#UltimateParentCSN_' + item.id).prop('disabled', true);
    }
    else if (index != -1 && indextmp == -1) {
      this.arrpartnertype.splice(index, 1);
    }
  }
  onSelectAll(items: any) {
    this.arrpartnertype = [];

    for (var i = 0; i < items.length; i++) {
      this.arrpartnertype.push([items[i].id, "exist", this.partnertype[i].RoleParamId]);
    }

    for (var i = 0; i < items.length; i++) {
      var indextmp = this.arrpartnertypetmp.findIndex(x => x[0] == items[i].id);
      var index = this.arrpartnertype.findIndex(x => x[0] == items[i].id);
      if (index != -1 && indextmp == -1) {
        this.arrpartnertype[index][1] = "insert";
        this.arrpartnertype[index][2] = "";
        this.arrpartnertype[index][3] = items[i].itemName;
      }
    }
  }
  onDeSelectAll(items: any) {
    this.arrpartnertype = [];

    for (var i = 0; i < this.partnertype.length; i++) {
      this.arrpartnertype.push([this.partnertype[i].id, "deleted", this.partnertype[i].RoleParamId]);
    }

    for (var i = 0; i < this.partnertype.length; i++) {
      var indextmp = this.arrpartnertypetmp.findIndex(x => x[0] == this.partnertype[i].id);
      var index = this.arrpartnertype.findIndex(x => x[0] == this.partnertype[i].id);
      if (index != -1 && indextmp == -1) {
        this.arrpartnertype.splice(index, 1);
      }
    }
  }

  getState1(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate1 = Object.keys(result).map(function (Index) {
          return {
            SiteStateProvince: result[Index].StateName
          }
        })

      },
        error => {
          this.service.errorserver();
        });
		console.log(this.datastate1);
  }

  getState2(value) {
    //get data state
     var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
       
        this.datastate2 = Object.keys(result).map(function (Index) {
          return {
            SiteStateProvince: result[Index].StateName
          }
        })
      },
        error => {
          this.service.errorserver();
        });
		console.log(this.datastate2);
  }

  getState3(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate3 = Object.keys(result).map(function (Index) {
          return {
			
            MailingStateProvince: result[Index].StateName
          }
        })
      },
        error => {
          this.service.errorserver();
        });
  }

  getState4(value) {
    //get data state
    var data = '';
	this.datastate4=null;
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        // this.datastate4 = JSON.parse(result).map((item) => {
        //   return {
			
        //       ShippingStateProvince: item.StateName
        //   }
        // })
        this.datastate4 = Object.keys(result).map(function (Index) {
          return {
            ShippingStateProvince: result[Index].StateName,
          }
        })
      },
        error => {
          this.service.errorserver();
        });
  }

  validpartnertype: Boolean = true;
  //submit form
  onSubmit() {

    this.editsite.controls['Workstations'].markAsTouched();
    this.editsite.controls['SiteTelephone'].markAsTouched();
    this.editsite.controls['SiteEmailAddress'].markAsTouched();
    this.editsite.controls['SiteAddress1'].markAsTouched();
    this.editsite.controls['SiteCity'].markAsTouched();
    this.editsite.controls['SiteCountryCode'].markAsTouched();
    this.editsite.controls['MailingAddress1'].markAsTouched();
    this.editsite.controls['MailingCity'].markAsTouched();
    this.editsite.controls['MailingCountryCode'].markAsTouched();
    this.editsite.controls['ShippingAddress1'].markAsTouched();
    this.editsite.controls['ShippingCity'].markAsTouched();
    this.editsite.controls['ShippingCountryCode'].markAsTouched();
    this.editsite.controls['SiteAdminFirstName'].markAsTouched();
    this.editsite.controls['SiteAdminLastName'].markAsTouched();
    this.editsite.controls['SiteAdminEmailAddress'].markAsTouched();
    this.editsite.controls['SiteAdminTelephone'].markAsTouched();
    this.editsite.controls['SiteManagerFirstName'].markAsTouched();
    this.editsite.controls['SiteManagerLastName'].markAsTouched();
    this.editsite.controls['SiteManagerEmailAddress'].markAsTouched();
    this.editsite.controls['SiteManagerTelephone'].markAsTouched();

    if (this.selectedItemsPartnerType.length < 1) {
      this.validpartnertype = false;
    } else {
      this.validpartnertype = true;
    }

    if (this.editsite.valid && this.selectedItemsPartnerType.length > 0) {
      let fileData = []
      this.fileUploader.files.map((file)=>{
        let reader = new FileReader()
        let name = file.name
        reader.onload = ()=>{
          fileData.push({ name:name, data:reader.result })
          if(this.fileUploader.files.length == fileData.length)
            this.editsite.patchValue({
              Files: fileData
            })
        }
        reader.readAsDataURL(file);
      })
      swal({
        title: 'Are you sure?',
        text: "If you wish to Edit Site!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          let arrpartnertyperesult = []
          for (var i = 0; i < this.arrpartnertype.length; i++) {
            if (this.arrpartnertype[i][1] != "delete") {
              var subpartnertype = "";
              if (this.arrpartnertype[i][0] == '1' || this.arrpartnertype[i][0] == '58' || this.arrpartnertype[i][0] == '60' ) {
                subpartnertype = (document.getElementById('SubPartnerType_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
              }
              else if (this.arrpartnertype[i][0] == '54' || this.arrpartnertype[i][0] == '55' || this.arrpartnertype[i][0] == '61') {
                subpartnertype = (document.getElementById('SubPartnerType_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
                  if (subpartnertype == null || subpartnertype == '' || subpartnertype == "")  subpartnertype = '0';
              }
                
              var status = (document.getElementById('Status_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
              var locationCSN = (document.getElementById('LocationCSN_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
              if (locationCSN == null || locationCSN == '' || locationCSN == "") locationCSN = ' ';
              var parentCSN = (document.getElementById('ParentCSN_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
              if (parentCSN == null || parentCSN == '' || parentCSN == "") parentCSN = ' ';
              var ultimateparentCSN = (document.getElementById('UltimateParentCSN_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
              if (ultimateparentCSN == null || ultimateparentCSN == '' || ultimateparentCSN == "") ultimateparentCSN = ' ';
              var initialaccreditationdate = "";
              if (this.arrpartnertype[i][0] == '58') {
                initialaccreditationdate = (document.getElementById('InitialAccreditationDate_' + this.arrpartnertype[i][0]) as HTMLInputElement).value;
              }
            }
            if (this.arrpartnertype[i][1] == "exist") {
              arrpartnertyperesult.push([this.arrpartnertype[i][0], "exist", this.arrpartnertype[i][2], this.arrpartnertype[i].itemName, status, initialaccreditationdate, ultimateparentCSN, locationCSN, parentCSN, ultimateparentCSN, subpartnertype]);
            } else if (this.arrpartnertype[i][1] == "insert") {
              arrpartnertyperesult.push([this.arrpartnertype[i][0], "insert", this.arrpartnertype[i][2], this.arrpartnertype[i].itemName, status, initialaccreditationdate, ultimateparentCSN, locationCSN, parentCSN, ultimateparentCSN, subpartnertype]);
            } else {
              arrpartnertyperesult.push([this.arrpartnertype[i][0], "delete", this.arrpartnertype[i][2], this.arrpartnertype[i].itemName, status, initialaccreditationdate, ultimateparentCSN, locationCSN, parentCSN, ultimateparentCSN, subpartnertype]);
            }
          }

          // this.editsite.value.AdminNotes = this.htmlEntityService.encoder(this.editsite.value.AdminNotes);
          // this.editsite.value.SiteName = this.htmlEntityService.encoder(this.editsite.value.SiteName);
          // this.editsite.value.EnglishSiteName = this.htmlEntityService.encoder(this.editsite.value.EnglishSiteName);
          // this.editsite.value.CommercialSiteName = this.htmlEntityService.encoder(this.editsite.value.CommercialSiteName);
          // this.editsite.value.SiteAddress1 = this.htmlEntityService.encoder(this.editsite.value.SiteAddress1);
          // this.editsite.value.SiteAddress2 = this.htmlEntityService.encoder(this.editsite.value.SiteAddress2);
          // this.editsite.value.SiteAddress3 = this.htmlEntityService.encoder(this.editsite.value.SiteAddress3);
          // this.editsite.value.SiteCity = this.htmlEntityService.encoder(this.editsite.value.SiteCity);
          // this.editsite.value.SitePostalCode = this.htmlEntityService.encoder(this.editsite.value.SitePostalCode);
          // this.editsite.value.MailingAddress1 = this.htmlEntityService.encoder(this.editsite.value.MailingAddress1);
          // this.editsite.value.MailingAddress2 = this.htmlEntityService.encoder(this.editsite.value.MailingAddress2);
          // this.editsite.value.MailingAddress3 = this.htmlEntityService.encoder(this.editsite.value.MailingAddress3);
          // this.editsite.value.MailingCity = this.htmlEntityService.encoder(this.editsite.value.MailingCity);
          // this.editsite.value.MailingPostalCode = this.htmlEntityService.encoder(this.editsite.value.MailingPostalCode);
          // this.editsite.value.ShippingAddress1 = this.htmlEntityService.encoder(this.editsite.value.ShippingAddress1);
          // this.editsite.value.ShippingAddress2 = this.htmlEntityService.encoder(this.editsite.value.ShippingAddress2);
          // this.editsite.value.ShippingAddress3 = this.htmlEntityService.encoder(this.editsite.value.ShippingAddress3);
          // this.editsite.value.ShippingCity = this.htmlEntityService.encoder(this.editsite.value.ShippingCity);
          // this.editsite.value.ShippingPostalCode = this.htmlEntityService.encoder(this.editsite.value.ShippingPostalCode);
          // this.editsite.value.SiteManagerFirstName = this.htmlEntityService.encoder(this.editsite.value.SiteManagerFirstName);
          // this.editsite.value.SiteManagerLastName = this.htmlEntityService.encoder(this.editsite.value.SiteManagerLastName);
          // this.editsite.value.SiteAdminFirstName = this.htmlEntityService.encoder(this.editsite.value.SiteAdminFirstName);
          // this.editsite.value.SiteAdminLastName = this.htmlEntityService.encoder(this.editsite.value.SiteAdminLastName);
         
          //convert object to json
          this.editsite.value.PartnerType = arrpartnertyperesult;
          this.editsite.value.LastAdminBy = this.useraccesdata.ContactName;

          this.editsite.value.cuid = this.useraccesdata.ContactName;
          this.editsite.value.UserId = this.useraccesdata.UserId;

          let data = JSON.stringify(this.editsite.value);

          let finaldata = data.replace(/\'/g, "\\\\'");
          finaldata.replace(/"/g, '\"');

          //put action
          this.service.httpClientPost('api/MainOrganization/DeleteFile',this.attachments_remove).subscribe(res=>{
			  
			  var result = res;
			  if (result['code'] == '1') {
				   this.service.httpCLientPut(this._serviceUrl+'/'+this.editsite.value.SiteIdInt, finaldata)
						.subscribe(res=>{
							this.loading = false;
						});
			  }else {
                this.loading = false;
					swal(
							'Error!',
							'Failed to delete the attachments!',
							'error'
					)
              }
		  });
         

          //redirect
          setTimeout(() => {
            this.router.navigate(['/manage/site/detail-site', this.editsite.value.SiteIdInt]);
            this.loading = false;
          }, 5000);
        }
      }).catch(swal.noop);
    }
    else {
      if(this.fileUploader.valid())
        swal(
          'Field is Required!',
          'Please enter the Add Organization form required!',
          'error'
        )
      else
        swal(
          'Field is Required!',
          'Please enter the Edit Site form required!',
          'error'
        )
    }
  }

  buildForm(): void {

    let SiteIdInt = new FormControl(this.datadetail.SiteIdInt);
    let SiteId = new FormControl(this.datadetail.SiteId);
    let ATCSiteId = new FormControl(this.datadetail.ATCSiteId);
    let OrgId = new FormControl(this.datadetail.OrgId);
    let SiteStatus_retired = new FormControl(this.datadetail.SiteStatus_retired);
    let SiteName = new FormControl(this.datadetail.SiteName);
    let SiebelSiteName = new FormControl(this.datadetail.SiebelSiteName);
    let EnglishSiteName = new FormControl(this.datadetail.EnglishSiteName);
    let CommercialSiteName = new FormControl(this.datadetail.CommercialSiteName);
    let Workstations = new FormControl(this.datadetail.Workstations, [Validators.required, CustomValidators.number]);
    // let MagellanId = new FormControl(this.datadetail.MagellanId);
    let SAPNumber_retired = new FormControl(this.datadetail.SAPNumber_retired);
    let SAPShipTo_retired = new FormControl(this.datadetail.SAPShipTo_retired);
    // let SiteTelephone = new FormControl(this.datadetail.SiteTelephone, [Validators.required, CustomValidators.number]);
    let SiteTelephone = new FormControl(this.datadetail.SiteTelephone, Validators.required);
    let SiteFax = new FormControl(this.datadetail.SiteFax);
    let SiteEmailAddress = new FormControl(this.datadetail.SiteEmailAddress, [Validators.required, Validators.email]);
    let SiteWebAddress = new FormControl(this.datadetail.SiteWebAddress);
    // let SiteDepartment = new FormControl(this.datadetail.SiteDepartment);
    let SiteAddress1 = new FormControl(this.datadetail.SiteAddress1, Validators.required);
    let SiteAddress2 = new FormControl(this.datadetail.SiteAddress2);
    let SiteAddress3 = new FormControl(this.datadetail.SiteAddress3);
    let SiteCity = new FormControl(this.datadetail.SiteCity, Validators.required);
    let SiteStateProvince = new FormControl(this.datadetail.SiteStateProvince);
    let SiteCountryCode = new FormControl(this.datadetail.SiteCountryCode, Validators.required);
    let SitePostalCode = new FormControl(this.datadetail.SitePostalCode);
    // let MailingDepartment = new FormControl(this.datadetail.MailingDepartment);
    let MailingAddress1 = new FormControl(this.datadetail.MailingAddress1, Validators.required);
    let MailingAddress2 = new FormControl(this.datadetail.MailingAddress2);
    let MailingAddress3 = new FormControl(this.datadetail.MailingAddress3);
    let MailingCity = new FormControl(this.datadetail.MailingCity, Validators.required);
    let MailingStateProvince = new FormControl(this.datadetail.MailingStateProvince);
    let MailingCountryCode = new FormControl(this.datadetail.MailingCountryCode, Validators.required);
    let MailingPostalCode = new FormControl(this.datadetail.MailingPostalCode);
    // let ShippingDepartment = new FormControl(this.datadetail.ShippingDepartment);
    let ShippingAddress1 = new FormControl(this.datadetail.ShippingAddress1, Validators.required);
    let ShippingAddress2 = new FormControl(this.datadetail.ShippingAddress2);
    let ShippingAddress3 = new FormControl(this.datadetail.ShippingAddress3);
    let ShippingCity = new FormControl(this.datadetail.ShippingCity, Validators.required);
    let ShippingStateProvince = new FormControl(this.datadetail.ShippingStateProvince);
    let ShippingCountryCode = new FormControl(this.datadetail.ShippingCountryCode, Validators.required);
    let ShippingPostalCode = new FormControl(this.datadetail.ShippingPostalCode);
    let SiteAdminFirstName = new FormControl(this.datadetail.SiteAdminFirstName, Validators.required);
    let SiteAdminLastName = new FormControl(this.datadetail.SiteAdminLastName, Validators.required);
    let SiteAdminEmailAddress = new FormControl(this.datadetail.SiteAdminEmailAddress, [Validators.required, Validators.email]);
    // let SiteAdminTelephone = new FormControl(this.datadetail.SiteAdminTelephone, [Validators.required, CustomValidators.number]);
    let SiteAdminTelephone = new FormControl(this.datadetail.SiteAdminTelephone, Validators.required);
    let SiteAdminFax = new FormControl(this.datadetail.SiteAdminFax);
    let SiteManagerFirstName = new FormControl(this.datadetail.SiteManagerFirstName, Validators.required);
    let SiteManagerLastName = new FormControl(this.datadetail.SiteManagerLastName, Validators.required);
    let SiteManagerEmailAddress = new FormControl(this.datadetail.SiteManagerEmailAddress, [Validators.required, Validators.email]);
    // let SiteManagerTelephone = new FormControl(this.datadetail.SiteManagerTelephone, [Validators.required, CustomValidators.number]);
    let SiteManagerTelephone = new FormControl(this.datadetail.SiteManagerTelephone, Validators.required);
    let SiteManagerFax = new FormControl(this.datadetail.SiteManagerFax);
    let AdminNotes = new FormControl(this.datadetail.AdminNotes);
    let Status = new FormControl(this.datadetail.Status);
    let DateAdded = new FormControl(this.datadetail.DateAdded);
    let AddedBy = new FormControl(this.datadetail.AddedBy);
    let DateLastAdmin = new FormControl(this.datadetail.DateLastAdmin);
    let LastAdminBy = new FormControl(this.datadetail.LastAdminBy);
    let CSOLocationUUID = new FormControl(this.datadetail.CSOLocationUUID);
    let CSOLocationName = new FormControl(this.datadetail.CSOLocationName);
    let CSOLocationNumber = new FormControl(this.datadetail.CSOLocationNumber);
    let CSOAuthorizationCodes = new FormControl(this.datadetail.CSOAuthorizationCodes);
    let CSOUpdatedOn = new FormControl(this.datadetail.CSOUpdatedOn);
    let CSOVersion = new FormControl(this.datadetail.CSOVersion);
    let CSOpartner_type = new FormControl(this.datadetail.CSOpartner_type);
    this.webaddress = this.datadetail.SiteWebAddress;
    let Files = new FormControl();


    //get data state 
    var data = '';
	
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.SiebelCountryCode + '"}', data)
      .subscribe(result => {
        this.datastate1 = Object.keys(result).map(function (Index) {
          return {
            SiteStateProvince: result[Index].StateName,
          }
        })
      },
        error => {
          this.service.errorserver();
        });

    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.SiteCountryCode + '"}', data)
      .subscribe(result => {
        this.datastate2 = Object.keys(result).map(function (Index) {
          return {
            SiteStateProvince: result[Index].StateName,
          }
        })
      },
        error => {
          this.service.errorserver();
        });

    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.MailingCountryCode + '"}', data)
      .subscribe(result => {
        this.datastate3 = Object.keys(result).map(function (Index) {
          return {
            MailingStateProvince: result[Index].StateName,
          }
        })
      },
        error => {
          this.service.errorserver();
        });

    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.ShippingCountryCode + '"}', data)
      .subscribe(result => {
        this.datastate4 = Object.keys(result).map(function (Index) {
          return {
            ShippingStateProvince: result[Index].StateName,
          }
        })
      },
        error => {
          this.service.errorserver();
        });

    this.editsite = new FormGroup({
      SiteIdInt: SiteIdInt,
      SiteId: SiteId,
      ATCSiteId: ATCSiteId,
      OrgId: OrgId,
      SiteStatus_retired: SiteStatus_retired,
      SiteName: SiteName,
      SiebelSiteName: SiebelSiteName,
      EnglishSiteName: EnglishSiteName,
      CommercialSiteName: CommercialSiteName,
      Workstations: Workstations,
      // MagellanId:MagellanId,
      SAPNumber_retired: SAPNumber_retired,
      SAPShipTo_retired: SAPShipTo_retired,
      SiteTelephone: SiteTelephone,
      SiteFax: SiteFax,
      SiteEmailAddress: SiteEmailAddress,
      SiteWebAddress: SiteWebAddress,
      // SiteDepartment:SiteDepartment,
      SiteAddress1: SiteAddress1,
      SiteAddress2: SiteAddress2,
      SiteAddress3: SiteAddress3,
      SiteCity: SiteCity,
      SiteStateProvince: SiteStateProvince,
      SiteCountryCode: SiteCountryCode,
      SitePostalCode: SitePostalCode,
      // MailingDepartment:MailingDepartment,
      MailingAddress1: MailingAddress1,
      MailingAddress2: MailingAddress2,
      MailingAddress3: MailingAddress3,
      MailingCity: MailingCity,
      MailingStateProvince: MailingStateProvince,
      MailingCountryCode: MailingCountryCode,
      MailingPostalCode: MailingPostalCode,
      // ShippingDepartment:ShippingDepartment,  
      ShippingAddress1: ShippingAddress1,
      ShippingAddress2: ShippingAddress2,
      ShippingAddress3: ShippingAddress3,
      ShippingCity: ShippingCity,
      ShippingStateProvince: ShippingStateProvince,
      ShippingCountryCode: ShippingCountryCode,
      ShippingPostalCode: ShippingPostalCode,
      SiteAdminFirstName: SiteAdminFirstName,
      SiteAdminLastName: SiteAdminLastName,
      SiteAdminEmailAddress: SiteAdminEmailAddress,
      SiteAdminTelephone: SiteAdminTelephone,
      SiteAdminFax: SiteAdminFax,
      SiteManagerFirstName: SiteManagerFirstName,
      SiteManagerLastName: SiteManagerLastName,
      SiteManagerEmailAddress: SiteManagerEmailAddress,
      SiteManagerTelephone: SiteManagerTelephone,
      SiteManagerFax: SiteManagerFax,
      AdminNotes: AdminNotes,
      Status: Status,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy,
      CSOLocationUUID: CSOLocationUUID,
      CSOLocationName: CSOLocationName,
      CSOLocationNumber: CSOLocationNumber,
      CSOAuthorizationCodes: CSOAuthorizationCodes,
      CSOUpdatedOn: CSOUpdatedOn,
      CSOVersion: CSOVersion,
      CSOpartner_type: CSOpartner_type,
      Files:Files
    });
  }

  //reset form
  resetForm() {
    this.attachments_remove = []
    this.editsite.reset({
      'SiteName': this.datadetail.SiteName,
      'EnglishSiteName': this.datadetail.EnglishSiteName,
      'CommercialSiteName': this.datadetail.CommercialSiteName,
      'Workstations': this.datadetail.Workstations,
      // 'MagellanId': this.datadetail.MagellanId,
      'SiteTelephone': this.datadetail.SiteTelephone,
      'SiteFax': this.datadetail.SiteFax,
      'SiteEmailAddress': this.datadetail.SiteEmailAddress,
      'SiteWebAddress': this.datadetail.SiteWebAddress,
      // 'SiteDepartment': this.datadetail.SiteDepartment,
      'SiteAddress1': this.datadetail.SiteAddress1,
      'SiteAddress2': this.datadetail.SiteAddress2,
      'SiteAddress3': this.datadetail.SiteAddress3,
      'SiteCity': this.datadetail.SiteCity,
      'SiteCountryCode': this.datadetail.SiteCountryCode,
      'SiteStateProvince': this.datadetail.SiteStateProvince,
      'SitePostalCode': this.datadetail.SitePostalCode,
      // 'MailingDepartment': this.datadetail.MailingDepartment,
      'MailingAddress1': this.datadetail.MailingAddress1,
      'MailingAddress2': this.datadetail.MailingAddress2,
      'MailingAddress3': this.datadetail.MailingAddress3,
      'MailingCity': this.datadetail.MailingCity,
      'MailingCountryCode': this.datadetail.MailingCountryCode,
      'MailingStateProvince': this.datadetail.MailingStateProvince,
      'MailingPostalCode': this.datadetail.MailingPostalCode,
      // 'ShippingDepartment': this.datadetail.ShippingDepartment,
      'ShippingAddress1': this.datadetail.ShippingAddress1,
      'ShippingAddress2': this.datadetail.ShippingAddress2,
      'ShippingAddress3': this.datadetail.ShippingAddress3,
      'ShippingCity': this.datadetail.ShippingCity,
      'ShippingCountryCode': this.datadetail.ShippingCountryCode,
      'ShippingStateProvince': this.datadetail.ShippingStateProvince,
      'ShippingPostalCode': this.datadetail.ShippingPostalCode,
      'SiteManagerFirstName': this.datadetail.SiteManagerFirstName,
      'SiteManagerLastName': this.datadetail.SiteManagerLastName,
      'SiteManagerEmailAddress': this.datadetail.SiteManagerEmailAddress,
      'SiteManagerTelephone': this.datadetail.SiteManagerTelephone,
      'SiteManagerFax': this.datadetail.SiteManagerFax,
      'SiteAdminFirstName': this.datadetail.SiteAdminFirstName,
      'SiteAdminLastName': this.datadetail.SiteAdminLastName,
      'SiteAdminEmailAddress': this.datadetail.SiteAdminEmailAddress,
      'SiteAdminTelephone': this.datadetail.SiteAdminTelephone,
      'SiteAdminFax': this.datadetail.SiteAdminFax,
      'AdminNotes': this.datadetail.AdminNotes
    });
  }

  webaddress: string = "";
  getwebaddress(value) {
    this.webaddress = value;
  }

  testwebaddress() {
    if (this.webaddress != "") {
      window.open("//" + this.webaddress);
    } else {
      alert("insert web address");
    }
  }

  // button copy
  // MailingDepartment = "";
  MailingAddress1 = "";
  MailingAddress2 = "";
  MailingAddress3 = "";
  MailingCity = "";
  MailingCountryCode = "";
  MailingStateProvince = "";
  MailingPostalCode = "";
  getSiteInvoice() {
    // this.MailingDepartment = this.editsite.value.SiteDepartment;
    this.MailingAddress1 = this.editsite.value.SiteAddress1;
    this.MailingAddress2 = this.editsite.value.SiteAddress2;
    this.MailingAddress3 = this.editsite.value.SiteAddress3;
    this.MailingCity = this.editsite.value.SiteCity;
    this.editsite.patchValue({ MailingCountryCode: this.editsite.value.SiteCountryCode });
    this.getState3(this.editsite.value.SiteCountryCode);
    this.editsite.patchValue({ MailingStateProvince: this.editsite.value.SiteStateProvince });
    this.MailingPostalCode = this.editsite.value.SitePostalCode;
  }

  // ShippingDepartment = "";
  ShippingAddress1 = "";
  ShippingAddress2 = "";
  ShippingAddress3 = "";
  ShippingCity = "";
  ShippingCountryCode = "";
  ShippingStateProvince = "";
  ShippingPostalCode = "";
  getSiteContract() {
    // this.ShippingDepartment = this.editsite.value.SiteDepartment;
    this.ShippingAddress1 = this.editsite.value.SiteAddress1;
    this.ShippingAddress2 = this.editsite.value.SiteAddress2;
    this.ShippingAddress3 = this.editsite.value.SiteAddress3;
    this.ShippingCity = this.editsite.value.SiteCity;
    this.editsite.patchValue({ ShippingCountryCode: this.editsite.value.SiteCountryCode });
    this.getState4(this.editsite.value.SiteCountryCode);
    this.editsite.patchValue({ ShippingStateProvince: this.editsite.value.SiteStateProvince });
    this.ShippingPostalCode = this.editsite.value.SitePostalCode;
  }

  SiteAdminFirstName = "";
  SiteAdminLastName = "";
  SiteAdminEmailAddress = "";
  SiteAdminTelephone = "";
  SiteAdminFax = "";
  getManagerAdmin() {
    this.SiteAdminFirstName = this.editsite.value.SiteManagerFirstName;
    this.SiteAdminLastName = this.editsite.value.SiteManagerLastName;
    this.SiteAdminEmailAddress = this.editsite.value.SiteManagerEmailAddress;
    this.SiteAdminTelephone = this.editsite.value.SiteManagerTelephone;
    this.SiteAdminFax = this.editsite.value.SiteManagerFax;
  }
}
