import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

@Component({
  selector: 'app-add-site-epdb',//
  templateUrl: './add-site-epdb.component.html',
  styleUrls: [
    './add-site-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddSiteEPDBComponent implements OnInit {

  @ViewChild('fileUploader') fileUploader

  private _serviceUrl = 'api/MainSite';
  //abah tambah url utk get ContactName dipakai  di line 204,
  private _ContactAllserviceUrl = 'api/ContactAll'; //_ContactAllserviceUrl

  messageError: string = '';
  addsite: FormGroup;
  public datadetail: any;
  public datacountry: any;
  public datastate1: any;
  public datastate2: any;
  public datastate3: any;
  public datastate4: any;
  dropdownListPartnerType = [];
  selectedItemsPartnerType = [];
  dropdownSettings = {};
  arrpartnertype = [];
  public partnertypestatus: any;
  public useraccesdata: any;
  public loading = false;

  constructor(private session: SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute, private http: HttpClient) {//, private htmlEntityService: htmlentityService

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let SiteId = new FormControl('');
    let ATCSiteId = new FormControl('');
    let OrgId = new FormControl('');
    let SiteStatus_retired = new FormControl('');
    let SiteName = new FormControl('', Validators.required);
    let SiebelSiteName = new FormControl('');
    let EnglishSiteName = new FormControl('');
    let CommercialSiteName = new FormControl('');
    let Workstations = new FormControl('', [Validators.required, CustomValidators.number]);
    // let MagellanId = new FormControl('');
    let SAPNumber_retired = new FormControl('');
    let SAPShipTo_retired = new FormControl('');
    let SiteTelephone = new FormControl('', [Validators.required]);
    let SiteFax = new FormControl('');
    let SiteEmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let SiteWebAddress = new FormControl('');
    // let SiteDepartment = new FormControl('');
    let SiteAddress1 = new FormControl('', Validators.required);
    let SiteAddress2 = new FormControl('');
    let SiteAddress3 = new FormControl('');
    let SiteCity = new FormControl('', Validators.required);
    let SiteStateProvince = new FormControl('');
    let SiteCountryCode = new FormControl('', Validators.required);
    let SitePostalCode = new FormControl('');
    // let MailingDepartment = new FormControl('');
    let MailingAddress1 = new FormControl('', Validators.required);
    let MailingAddress2 = new FormControl('');
    let MailingAddress3 = new FormControl('');
    let MailingCity = new FormControl('', Validators.required);
    let MailingStateProvince = new FormControl('');
    let MailingCountryCode = new FormControl('', Validators.required);
    let MailingPostalCode = new FormControl('');
    // let ShippingDepartment = new FormControl('');
    let ShippingAddress1 = new FormControl('', Validators.required);
    let ShippingAddress2 = new FormControl('');
    let ShippingAddress3 = new FormControl('');
    let ShippingCity = new FormControl('', Validators.required);
    let ShippingStateProvince = new FormControl('');
    let ShippingCountryCode = new FormControl('', Validators.required);
    let ShippingPostalCode = new FormControl('');
    let SiteAdminFirstName = new FormControl('', Validators.required);
    let SiteAdminLastName = new FormControl('', Validators.required);
    let SiteAdminEmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let SiteAdminTelephone = new FormControl('', [Validators.required]);
    let SiteAdminFax = new FormControl('');
    let SiteManagerFirstName = new FormControl('', Validators.required);
    let SiteManagerLastName = new FormControl('', Validators.required);
    let SiteManagerEmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let SiteManagerTelephone = new FormControl('', [Validators.required]);
    let SiteManagerFax = new FormControl('');
    let AdminNotes = new FormControl('');
    let Status = new FormControl();
    let DateAdded = new FormControl();
    let AddedBy = new FormControl();
    let DateLastAdmin = new FormControl();
    let LastAdminBy = new FormControl();
    let CSOLocationUUID = new FormControl();
    let CSOLocationName = new FormControl();
    let CSOLocationNumber = new FormControl();
    let CSOAuthorizationCodes = new FormControl();
    let CSOUpdatedOn = new FormControl();
    let CSOVersion = new FormControl();
    let CSOpartner_type = new FormControl();
    let Files = new FormControl();

    this.addsite = new FormGroup({
      SiteId: SiteId,
      ATCSiteId: ATCSiteId,
      OrgId: OrgId,
      SiteStatus_retired: SiteStatus_retired,
      SiteName: SiteName,
      SiebelSiteName: SiebelSiteName,
      EnglishSiteName: EnglishSiteName,
      CommercialSiteName: CommercialSiteName,
      Workstations: Workstations,
      // MagellanId:MagellanId,
      SAPNumber_retired: SAPNumber_retired,
      SAPShipTo_retired: SAPShipTo_retired,
      SiteTelephone: SiteTelephone,
      SiteFax: SiteFax,
      SiteEmailAddress: SiteEmailAddress,
      SiteWebAddress: SiteWebAddress,
      // SiteDepartment:SiteDepartment,
      SiteAddress1: SiteAddress1,
      SiteAddress2: SiteAddress2,
      SiteAddress3: SiteAddress3,
      SiteCity: SiteCity,
      SiteStateProvince: SiteStateProvince,
      SiteCountryCode: SiteCountryCode,
      SitePostalCode: SitePostalCode,
      // MailingDepartment:MailingDepartment,
      MailingAddress1: MailingAddress1,
      MailingAddress2: MailingAddress2,
      MailingAddress3: MailingAddress3,
      MailingCity: MailingCity,
      MailingStateProvince: MailingStateProvince,
      MailingCountryCode: MailingCountryCode,
      MailingPostalCode: MailingPostalCode,
      // ShippingDepartment:ShippingDepartment,
      ShippingAddress1: ShippingAddress1,
      ShippingAddress2: ShippingAddress2,
      ShippingAddress3: ShippingAddress3,
      ShippingCity: ShippingCity,
      ShippingStateProvince: ShippingStateProvince,
      ShippingCountryCode: ShippingCountryCode,
      ShippingPostalCode: ShippingPostalCode,
      SiteAdminFirstName: SiteAdminFirstName,
      SiteAdminLastName: SiteAdminLastName,
      SiteAdminEmailAddress: SiteAdminEmailAddress,
      SiteAdminTelephone: SiteAdminTelephone,
      SiteAdminFax: SiteAdminFax,
      SiteManagerFirstName: SiteManagerFirstName,
      SiteManagerLastName: SiteManagerLastName,
      SiteManagerEmailAddress: SiteManagerEmailAddress,
      SiteManagerTelephone: SiteManagerTelephone,
      SiteManagerFax: SiteManagerFax,
      AdminNotes: AdminNotes,
      Status: Status,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy,
      CSOLocationUUID: CSOLocationUUID,
      CSOLocationName: CSOLocationName,
      CSOLocationNumber: CSOLocationNumber,
      CSOAuthorizationCodes: CSOAuthorizationCodes,
      CSOUpdatedOn: CSOUpdatedOn,
      CSOVersion: CSOVersion,
      CSOpartner_type: CSOpartner_type,
      Files: Files
    });
  }

  //public datasession:any;
  //addedby:string="";
  ngOnInit() {
    this.getSubPartnerType1();
    this.getSubPartnerType58();
    this.getSubPartnerType60();
    this.getSubPartnerType55();
    this.getSubPartnerType54();

    var data = '';

    //get data organization
    var id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet('api/MainOrganization/' + id, data)
      .subscribe(result => {
        this.datadetail = result;
        if (this.datadetail != null) {
            this.datadetail.OrgName = this.service.decoder(this.datadetail.OrgName);
        }
        //this.buildForm();
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });

    //get data country
    var data = '';
    this.service.httpClientGet('api/Countries', data)
      .subscribe(result => {
        this.datacountry = result;
      },
        error => {
          this.service.errorserver();
        });

    // Partner Type
    var data = '';
    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListPartnerType = null;
        }
        else {
          this.dropdownListPartnerType  = Object.keys(result).map(function (Index) {
            return {
              id: result[Index].RoleId,
              itemName: result[Index].RoleName
            }
          })
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
    this.selectedItemsPartnerType = [];

    //setting dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };

    //get partner type status
    this.service.httpClientGet('api/Roles/Status', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.partnertypestatus = '';
        }
        else {
          this.partnertypestatus = result
        }
      },
        error => {
          this.service.errorserver();
          this.partnertypestatus = '';
        });


  }

  formpartnertypedinamic = [];
  onItemSelect(item: any) {
    this.formpartnertypedinamic.push(item);
  }
  OnItemDeSelect(item: any) {
    var index = this.formpartnertypedinamic.findIndex(x => x.id == item.id);
    this.formpartnertypedinamic.splice(index, 1);
  }
  onSelectAll(items: any) {
    this.formpartnertypedinamic = [];

    for (var i = 0; i < items.length; i++) {
      this.formpartnertypedinamic.push(items[i]);
    }
  }
  onDeSelectAll(items: any) {
    this.formpartnertypedinamic = [];
  }


  getState1(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate1 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getState2(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate2 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getState3(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate3 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getState4(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate4 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  validpartnertype: Boolean = true;
  //submit form
  onSubmit() {

    this.addsite.controls['SiteName'].markAsTouched();
    this.addsite.controls['Workstations'].markAsTouched();
    this.addsite.controls['SiteTelephone'].markAsTouched();
    this.addsite.controls['SiteEmailAddress'].markAsTouched();
    this.addsite.controls['SiteAddress1'].markAsTouched();
    this.addsite.controls['SiteCity'].markAsTouched();
    this.addsite.controls['SiteCountryCode'].markAsTouched();
    this.addsite.controls['MailingAddress1'].markAsTouched();
    this.addsite.controls['MailingCity'].markAsTouched();
    this.addsite.controls['MailingCountryCode'].markAsTouched();
    this.addsite.controls['ShippingAddress1'].markAsTouched();
    this.addsite.controls['ShippingCity'].markAsTouched();
    this.addsite.controls['ShippingCountryCode'].markAsTouched();
    this.addsite.controls['SiteAdminFirstName'].markAsTouched();
    this.addsite.controls['SiteAdminLastName'].markAsTouched();
    this.addsite.controls['SiteAdminEmailAddress'].markAsTouched();
    this.addsite.controls['SiteAdminTelephone'].markAsTouched();
    this.addsite.controls['SiteManagerFirstName'].markAsTouched();
    this.addsite.controls['SiteManagerLastName'].markAsTouched();
    this.addsite.controls['SiteManagerEmailAddress'].markAsTouched();
    this.addsite.controls['SiteManagerTelephone'].markAsTouched();

    if (this.selectedItemsPartnerType.length < 1) {
      this.validpartnertype = false;
    } else {
      this.validpartnertype = true;
    }

    if (this.addsite.valid && this.selectedItemsPartnerType.length > 0 && this.validname) {  // && this.fileUploader.valid()
      let fileData = []
      this.fileUploader.files.map((file)=>{
        let reader = new FileReader()
        let name = file.name
        reader.onload = ()=>{
          fileData.push({ name:name, data:reader.result })
          if(this.fileUploader.files.length == fileData.length)
            this.addsite.patchValue({
              Files: fileData
            })
        }
        reader.readAsDataURL(file);
      })
      swal({
        title: 'Are you sure?',
        text: "If you wish to Add Site!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          this.addsite.value.SiteId = this.datadetail.OrgId;
          this.addsite.value.OrgId = this.datadetail.OrgId;
          this.addsite.value.Status = "I";
          this.addsite.value.DateAdded = this.formatdate.dateJStoYMD(new Date());
          this.addsite.value.AddedBy = this.useraccesdata.ContactName;

          for (var i = 0; i < this.selectedItemsPartnerType.length; i++) {
            var subpartnertype = "";
            if (this.selectedItemsPartnerType[i].id == '1' || this.selectedItemsPartnerType[i].id == '58' || this.selectedItemsPartnerType[i].id == '60' || this.selectedItemsPartnerType[i].id == '55' || this.selectedItemsPartnerType[i].id == '54') {
              subpartnertype = (document.getElementById('SubPartnerType_' + this.selectedItemsPartnerType[i].id) as HTMLInputElement).value;
            }

            var status = (document.getElementById('Status_' + this.selectedItemsPartnerType[i].id) as HTMLInputElement).value;
            var locationCSN = (document.getElementById('LocationCSN_' + this.selectedItemsPartnerType[i].id) as HTMLInputElement).value;
            var parentCSN = (document.getElementById('ParentCSN_' + this.selectedItemsPartnerType[i].id) as HTMLInputElement).value;
            var ultimateparentCSN = (document.getElementById('UltimateParentCSN_' + this.selectedItemsPartnerType[i].id) as HTMLInputElement).value;
            var initialaccreditationdate = "";
            if (this.selectedItemsPartnerType[i].id == '58') {
              initialaccreditationdate = (document.getElementById('InitialAccreditationDate_' + this.selectedItemsPartnerType[i].id) as HTMLInputElement).value;
            }
            this.arrpartnertype.push([this.selectedItemsPartnerType[i].id, subpartnertype, status, locationCSN, parentCSN, ultimateparentCSN, initialaccreditationdate]);
          }
          this.addsite.value.PartnerType = this.arrpartnertype;

          // this.addsite.value.AdminNotes = this.htmlEntityService.encoder(this.addsite.value.AdminNotes);
          // this.addsite.value.SiteName = this.htmlEntityService.encoder(this.addsite.value.SiteName);
          // this.addsite.value.EnglishSiteName = this.htmlEntityService.encoder(this.addsite.value.EnglishSiteName);
          // this.addsite.value.CommercialSiteName = this.htmlEntityService.encoder(this.addsite.value.CommercialSiteName);
          // this.addsite.value.SiteAddress1 = this.htmlEntityService.encoder(this.addsite.value.SiteAddress1);
          // this.addsite.value.SiteAddress2 = this.htmlEntityService.encoder(this.addsite.value.SiteAddress2);
          // this.addsite.value.SiteAddress3 = this.htmlEntityService.encoder(this.addsite.value.SiteAddress3);
          // this.addsite.value.SiteCity = this.htmlEntityService.encoder(this.addsite.value.SiteCity);
          // this.addsite.value.SitePostalCode = this.htmlEntityService.encoder(this.addsite.value.SitePostalCode);
          // this.addsite.value.MailingAddress1 = this.htmlEntityService.encoder(this.addsite.value.MailingAddress1);
          // this.addsite.value.MailingAddress2 = this.htmlEntityService.encoder(this.addsite.value.MailingAddress2);
          // this.addsite.value.MailingAddress3 = this.htmlEntityService.encoder(this.addsite.value.MailingAddress3);
          // this.addsite.value.MailingCity = this.htmlEntityService.encoder(this.addsite.value.MailingCity);
          // this.addsite.value.MailingPostalCode = this.htmlEntityService.encoder(this.addsite.value.MailingPostalCode);
          // this.addsite.value.ShippingAddress1 = this.htmlEntityService.encoder(this.addsite.value.ShippingAddress1);
          // this.addsite.value.ShippingAddress2 = this.htmlEntityService.encoder(this.addsite.value.ShippingAddress2);
          // this.addsite.value.ShippingAddress3 = this.htmlEntityService.encoder(this.addsite.value.ShippingAddress3);
          // this.addsite.value.ShippingCity = this.htmlEntityService.encoder(this.addsite.value.ShippingCity);
          // this.addsite.value.ShippingPostalCode = this.htmlEntityService.encoder(this.addsite.value.ShippingPostalCode);
          // this.addsite.value.SiteManagerFirstName = this.htmlEntityService.encoder(this.addsite.value.SiteManagerFirstName);
          // this.addsite.value.SiteManagerLastName = this.htmlEntityService.encoder(this.addsite.value.SiteManagerLastName);
          // this.addsite.value.SiteAdminFirstName = this.htmlEntityService.encoder(this.addsite.value.SiteAdminFirstName);
          // this.addsite.value.SiteAdminLastName = this.htmlEntityService.encoder(this.addsite.value.SiteAdminLastName);

          //Audit Log
          this.addsite.value.cuid = this.useraccesdata.ContactName;
          this.addsite.value.UserId = this.useraccesdata.UserId;
          //End Audit Log

          //convert object to json
          let data = JSON.stringify(this.addsite.value);

          let finaldata = data.replace(/\'/g, "\\\\'");
          finaldata.replace(/"/g, '\"');

          /* autodesk plan 10 oct */

          //post action
          this.service.httpClientPost(this._serviceUrl, finaldata)
            .subscribe(res => {
              var result = res;
			 
              if (result['code'] == '1') {
                this.router.navigate(['/manage/site/detail-site', result['SiteIdInt']]);
                this.loading = false;
              }
              else {
                swal({
                  title: 'Information!',
                  text: result['message'],
                  type: 'error'
                }).catch(swal.noop);
                this.loading = false;
              }
            },
            error => {
              swal(
                'Information!',
                'Can\'t connect to server.',
                'error'
              );
              this.loading = false;
            });

          /* end line autodesk plan 10 oct */

        }
      }).catch(swal.noop);
    }
    else {
      if(!this.addsite.valid || this.selectedItemsPartnerType.length == 0 || !this.validname)
        swal(
          'Field is Required!',
          'Please enter the Add Site form required!',
          'error'
        )
      else if(!this.fileUploader.valid())
        swal(
          'File is Required!',
          'Please add file for Site',
          'error'
        )
    }

  }

  webaddress: string = "";
  getwebaddress(value) {
    this.webaddress = value;
  }

  public partnertype: any
  public partnertypedata: any
  public subpartnertype_1: any;
  public subpartnertype_58: any;
  public subpartnertype_60: any;
  public subpartnertype_55: any;
  public subpartnertype_54: any;
  getSubPartnerType1() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"ATC"}', data)
      .subscribe(result => {
        this.subpartnertype_1 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType58() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"AAP"}', data)
      .subscribe(result => {
        this.subpartnertype_58 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType60() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"MTP"}', data)
      .subscribe(result => {
        this.subpartnertype_60 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType55() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"MED"}', data)
      .subscribe(result => {
        this.subpartnertype_55 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getSubPartnerType54() {
    //get sub partner type
    var data = '';
    this.service.httpClientGet('api/RoleParams/where/{"RoleCode":"MER"}', data)
      .subscribe(result => {
        this.subpartnertype_54 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  //check name exist
  validname: Boolean = true;
  public namedata: any;
  checkNameDuplicate(value) {
    if (value != '' && value != null) {
      let finalvalue = value.replace(/\'/g, "")
      this.service.httpClientGet("api/MainSite/sitename/" + finalvalue, '')
        .subscribe(result => {
          this.namedata = result;
          if (this.namedata.SiteName != null) {
            this.validname = false;
          }
          else if (this.namedata.SiteName == null) {
            this.validname = true;
          }
        },
          error => {
            this.service.errorserver();
          });
    }
    else {
      this.validname = true;
    }
  }


  testwebaddress() {
    if (this.webaddress != "") {
      window.open("//" + this.webaddress);
    } else {
      alert("insert web address");
    }
  }

  // MailingDepartment = "";
  MailingAddress1 = "";
  MailingAddress2 = "";
  MailingAddress3 = "";
  MailingCity = "";
  MailingCountryCode = "";
  MailingStateProvince = "";
  MailingPostalCode = "";
  getSiteInvoice() {
    // this.MailingDepartment = this.addsite.value.SiteDepartment;
    this.MailingAddress1 = this.addsite.value.SiteAddress1;
    this.MailingAddress2 = this.addsite.value.SiteAddress2;
    this.MailingAddress3 = this.addsite.value.SiteAddress3;
    this.MailingCity = this.addsite.value.SiteCity;
    this.addsite.patchValue({ MailingCountryCode: this.addsite.value.SiteCountryCode });
    this.getState3(this.addsite.value.SiteCountryCode);
    this.addsite.patchValue({ MailingStateProvince: this.addsite.value.SiteStateProvince });
    this.MailingPostalCode = this.addsite.value.SitePostalCode;
  }

  // ShippingDepartment = "";
  ShippingAddress1 = "";
  ShippingAddress2 = "";
  ShippingAddress3 = "";
  ShippingCity = "";
  ShippingCountryCode = "";
  ShippingStateProvince = "";
  ShippingPostalCode = "";
  getSiteContract() {
    // this.ShippingDepartment = this.addsite.value.SiteDepartment;
    this.ShippingAddress1 = this.addsite.value.SiteAddress1;
    this.ShippingAddress2 = this.addsite.value.SiteAddress2;
    this.ShippingAddress3 = this.addsite.value.SiteAddress3;
    this.ShippingCity = this.addsite.value.SiteCity;
    this.addsite.patchValue({ ShippingCountryCode: this.addsite.value.SiteCountryCode });
    this.getState4(this.addsite.value.SiteCountryCode);
    this.addsite.patchValue({ ShippingStateProvince: this.addsite.value.SiteStateProvince });
    this.ShippingPostalCode = this.addsite.value.SitePostalCode;
  }

  SiteAdminFirstName = "";
  SiteAdminLastName = "";
  SiteAdminEmailAddress = "";
  SiteAdminTelephone = "";
  SiteAdminFax = "";
  getManagerAdmin() {
    this.SiteAdminFirstName = this.addsite.value.SiteManagerFirstName;
    this.SiteAdminLastName = this.addsite.value.SiteManagerLastName;
    this.SiteAdminEmailAddress = this.addsite.value.SiteManagerEmailAddress;
    this.SiteAdminTelephone = this.addsite.value.SiteManagerTelephone;
    this.SiteAdminFax = this.addsite.value.SiteManagerFax;
  }

}
