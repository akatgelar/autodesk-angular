import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSiteEPDBComponent } from './add-site-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const AddSiteEPDBRoutes: Routes = [
  {
    path: '',
    component: AddSiteEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.site.add_edit.add_new_site',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddSiteEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [AddSiteEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class AddSiteEPDBModule { }
