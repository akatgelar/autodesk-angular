import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSiteAttributeEPDBComponent } from './add-site-attribute-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddSiteAttributeEPDBRoutes: Routes = [
    {
        path: '',
        component: AddSiteAttributeEPDBComponent,
        data: {
            breadcrumb: 'Add Site Attribute',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddSiteAttributeEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [AddSiteAttributeEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class AddSiteAttributeEPDBModule { }
