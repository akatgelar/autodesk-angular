import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyEditEPDBComponent } from './currency-edit-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";

import { DataFilterCurrencyPipe } from './currency-edit-epdb.component';

export const CurrencyEditEPDBRoutes: Routes = [
  {
    path: '',
    component: CurrencyEditEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.currency.edit_currency',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CurrencyEditEPDBRoutes),
    SharedModule
  ],
  declarations: [CurrencyEditEPDBComponent, DataFilterCurrencyPipe],
  providers: [AppService]
})

export class CurrencyEditEPDBModule { }
