import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";

import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterCurrency' })
export class DataFilterCurrencyPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Key.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

declare const $: any;
declare var Morris: any;

@Component({
    selector: 'app-currency-edit-epdb',
    templateUrl: './currency-edit-epdb.component.html',
    styleUrls: [
        './currency-edit-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CurrencyEditEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Currency';
    messageResult: string = '';
    messageError: string = '';
    private data;
    currency;
    dataCurrency;
    fyindicator;
    conversion;
    dataConversion;
    currencyValue;

    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    addCurrency: FormGroup;
    updateCurrency: FormGroup;
    addFinancial: FormGroup;
    addCurrencyConversion: FormGroup;
    updateConversion: FormGroup;

    key:any;
    useraccesdata:any;

    constructor(public session: SessionService, private router: Router, private service: AppService, private route: ActivatedRoute) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let Key = new FormControl('', Validators.required);
        let KeyValue = new FormControl('', Validators.required);
        let Parent = new FormControl('');
        let Status = new FormControl('');

        this.addCurrency = new FormGroup({
            Key: Key,
            KeyValue: KeyValue,
            Parent: Parent,
            Status: Status
        });

        let Year = new FormControl('', Validators.required);
        let Currency = new FormControl('', Validators.required);
        let ValuePerUSD = new FormControl('', Validators.required);

        this.addCurrencyConversion = new FormGroup({
            Year: Year,
            Currency: Currency,
            ValuePerUSD: ValuePerUSD,
            Status: Status
        });
    }

    ngOnInit() {
        // this.dataCurrency = {};
        // this.dataConversion = {};
        // this.getCurrency();
        // this.getFYIndicator();
        // this.getCurrencyConversion();
        // this.currencyCombobox();
        var dataCurrency = '';
        this.key = this.route.snapshot.params['id'];
        this.service.httpClientGet("api/Currency/" + this.key, dataCurrency)
            .subscribe(result => {
                this.dataCurrency = result;
                if (Object.keys(this.dataCurrency).length !== 0 && this.dataCurrency.constructor === Object) {
                    this.addCurrency.patchValue({ Key: this.dataCurrency.Key });
                    this.addCurrency.patchValue({ KeyValue: this.dataCurrency.KeyValue });
                }
            },
            error => {
                this.service.errorserver();
            });
    }

    getCurrency() {
        var currency = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'Currencies','Status':'A'}", currency)
            .subscribe(result => {
                this.currency = result;
            },
            error => {
                this.service.errorserver();
            });
    }

    currencyCombobox() {
        var currencyValue = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'Currencies','Status':'A'}", currencyValue)
            .subscribe(result => {
                this.currencyValue = result;
            },
            error => {
                this.service.errorserver();
            });
    }

    httpClientGetFYIndicator() {
        var fyindicator = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'FYIndicator','Status':'A'}", fyindicator)
            .subscribe(result => {
                this.fyindicator = result;
            },
            error => {
                this.service.errorserver();
            });
    }

    getCurrencyConversion() {
        var conversion = '';
        this.service.httpClientGet("api/Currency/CurrencyConversion", conversion)
            .subscribe(result => {
                this.conversion = result;
            },
            error => {
                this.service.errorserver();
            });
    }

    onSubmit() {
        this.addCurrency.controls['Key'].markAsTouched();
        this.addCurrency.controls['KeyValue'].markAsTouched();
        if (this.addCurrency.valid) {
            var check: any;
            this.service.httpClientGet("api/Currency/where/{'Key':'" + this.key + "'}", check)
                .subscribe(result => {
                    check = result;
                    if (check.length != 0) {

                        /* autodesk plan 10 oct - complete all history log */

                        var update = {
                            'Key': this.addCurrency.value.Key,
                            'KeyValue': this.addCurrency.value.KeyValue,
                            'cuid': this.useraccesdata.ContactName,
                            'UserId': this.useraccesdata.UserId
                        };

                        /* end line autodesk plan 10 oct - complete all history log */
                        
                        this.service.httpCLientPut(this._serviceUrl+'/'+this.key, update)
                        .subscribe(res=>{
                            console.log(res)
                        });
                        //this.service.httpCLientPut(this._serviceUrl,this.key, update);
 
                        
                        this.dataCurrency = {};

                        //redirect
                        this.router.navigate(['/admin/currency']);
                        
                    } else {
                        swal('Information!', 'Duplicate Data In Database', 'error');
                        this.resetCurrency();
                    }
                }, error => {
                    this.service.errorserver();
                });
        }
    }

    resetCurrency() {
        this.addCurrency.reset({
            'Key': '',
            'KeyValue': ''
        });
    }

    // onSubmitConversion() {
    //     this.addCurrencyConversion.controls['Year'].markAsTouched();
    //     this.addCurrencyConversion.controls['Currency'].markAsTouched();
    //     this.addCurrencyConversion.controls['ValuePerUSD'].markAsTouched();

    //     if (this.addCurrencyConversion.valid) {
    //         if (Object.keys(this.dataConversion).length === 0 && this.dataConversion.constructor === Object) {
    //             this.addCurrencyConversion.value.Status = "A";
    //             let data = JSON.stringify(this.addCurrencyConversion.value);
    //             this.service.httpClientPos(this._serviceUrl + "/CurrencyConversion", data);
    //         } else {
    //             var updateConversion = {
    //                 'Year': this.addCurrencyConversion.value.Year,
    //                 'Currency': this.addCurrencyConversion.value.Currency,
    //                 'ValuePerUSD': this.addCurrencyConversion.value.ValuePerUSD,
    //                 'Status': 'A'
    //             };
    //             this.service.httpCLientPu(this._serviceUrl + "/CurrencyConversion", this.dataConversion.CurrencyConversionId, updateConversion);
    //             this.dataConversion = {};
    //         }
    //         this.addCurrencyConversion.reset({
    //             'Year': '',
    //             'Currency': '',
    //             'ValuePerUSD': '',
    //         });
    //         this.getCurrencyConversion();
    //         this.getCurrencyConversion();
    //     }
    // }

    // resetConversion() {
    //     this.addCurrencyConversion.reset({
    //         'Year': '',
    //         'Currency': '',
    //         'ValuePerUSD': '',
    //     });
    // }

    // detailCurrency(key) {
    //     var dataCurrency = '';
    //     this.service.get("api/Currency/" + key, dataCurrency)
    //         .subscribe(result => {
    //             this.dataCurrency = JSON.parse(result);
    //             if (Object.keys(this.dataCurrency).length !== 0 && this.dataCurrency.constructor === Object) {
    //                 this.addCurrency.patchValue({ Key: this.dataCurrency.Key });
    //                 this.addCurrency.patchValue({ KeyValue: this.dataCurrency.KeyValue });
    //             }
    //         },
    //         error => {
    //             this.service.errorserver();
    //         });
    // }

    // getConversion(id) {
    //     var dataConversion = '';
    //     this.service.get("api/Currency/CurrencyConversion/" + id, dataConversion)
    //         .subscribe(result => {
    //             this.dataConversion = JSON.parse(result);
    //             if (Object.keys(this.dataConversion).length !== 0 && this.dataConversion.constructor === Object) {
    //                 this.addCurrencyConversion.patchValue({ Year: this.dataConversion.Year });
    //                 this.addCurrencyConversion.patchValue({ Currency: this.dataConversion.Currency });
    //                 this.addCurrencyConversion.patchValue({ ValuePerUSD: this.dataConversion.ValuePerUSD });
    //             }
    //         },
    //         error => {
    //             this.service.errorserver();
    //         });
    // }

    // openConfirmsSwal(id, index) {
    //     swal({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //     })
    //         .then(result => {
    //             if (result == true) {
    //                 var data = '';
    //                 this.service.delete("api/Currency/CurrencyConversion/" + id, data)
    //                     .subscribe(value => {
    //                         var resource = JSON.parse(value);
    //                         this.service.openSuccessSwal(resource['message']);
    //                         var index = this.conversion.findIndex(x => x.CurrencyConversionId == id);
    //                         if (index !== -1) {
    //                             this.conversion.splice(index, 1);
    //                         }
    //                     },
    //                     error => {
    //                         this.messageError = <any>error
    //                         this.service.errorserver();
    //                     });
    //             }

    //         }).catch(swal.noop);
    // }
}