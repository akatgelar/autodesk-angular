import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateInvoiceEPDBComponent } from './update-invoice-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";

import { DataFilterInvoicePipe, DataFilterSKUPipe } from './update-invoice-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const UpdateInvoiceEPDBRoutes: Routes = [
  {
    path: '',
    component: UpdateInvoiceEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.add_invoice.edit_invoice',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateInvoiceEPDBRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [UpdateInvoiceEPDBComponent, DataFilterInvoicePipe, DataFilterSKUPipe],
  providers: [AppService, AppFormatDate],
})
export class UpdateInvoiceEPDBModule { }
