import { Component, OnInit, ViewEncapsulation } from '@angular/core';
declare const $: any;
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";

import * as _ from "lodash";
import { Pipe } from "@angular/core";

@Pipe({ name: 'dataFilterInvoice' })
export class DataFilterInvoicePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'dataFilterSKU' })
export class DataFilterSKUPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'update-invoice-epdb',
  templateUrl: './update-invoice-epdb.component.html',
  styleUrls: [
    './update-invoice-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class UpdateInvoiceEPDBComponent implements OnInit {

  havePrimarySite = true;
  lastUpdateBy = "";
  approvedBy = "";
  rejectedBy = "";
  lastUpdateDate = "";
  status = '';
  organizationId = '';  
  reasonReject = '';
  processed = false;
  comments = '';
  cannotChangeInvoiceStatus = true;
  loading = false;
  orgName = "";
  orgPartnerType = '';
  currentSites = [];
  types = [];
  territories = [];
  countries = [];
  sites = [];
  fys = [];
  skus = {};
  filter = {
    ter:0,
    coun:''
  }
  isSuperAdmin = false;
  isDistributor = false;
  checkDiscount = false;
  checkProRata = false;
  addInvoice:any = {
    InvoiceId: '',
    OrgId: '',
    InvoiceNumber: '',
    InvoiceDescription: '',
    InvoiceDate: '',
    Status: '',
    FinancialYear: '',
    PricingSKUs: [],
    Discounts: {
      Discount:{
        Id:'',
        Name: '',
        Amount: '',
        AmountType: 'Exactly',
        InvoiceId: ''
      },
      ProRata: {
        Id:'',
        Name: '',
        Amount: '',
        AmountType: '',
        InvoiceId: ''
      },
    }
  }
  constructor(private service:AppService, private router: ActivatedRoute, private navRouter: Router){}
  
  validProrataTime(val){
    if(val.month <8 && val.month>1){
      this.checkProRata = false
    }
    return (val.month >=8 || val.month==1)
  }
  
  ngOnInit(){
    this.router.params.subscribe((params)=>{
      this.service.httpClientGet('api/MainInvoices/GetInvoice?invoiceId='+params['id'],{}).subscribe((res:any)=>{
        this.status = res.status
        this.organizationId = res.organizationId
        this.lastUpdateBy = res.lastAdminBy
        this.lastUpdateDate = res.lastUpdate
        this.approvedBy = res.approvedBy
        this.rejectedBy = res.rejectedBy
        this.addInvoice = {
          InvoiceId: params['id'],
          OrgId: res.orgId,
          InvoiceNumber: res.invoiceNumber,
          InvoiceDescription: res.invoiceDescription,
          InvoiceDate: '',
          Status: res.status,
          FinancialYear: res.financialYear,
          PricingSKUs: res.listPricingSKU,
          Discounts: {
            Discount: {
              Id: res.discounts.discount?res.discounts.discount.id:'',
              Name: res.discounts.discount?res.discounts.discount.name:'',
              Amount: res.discounts.discount?res.discounts.discount.amount:'',
              AmountType: res.discounts.discount?res.discounts.discount.amountType:'',
              InvoiceId: res.discounts.discount?res.discounts.discount.invoiceId:''
            },
            ProRata: {
              Id: res.discounts.proRata?res.discounts.proRata.id:'',
              Name: res.discounts.proRata?res.discounts.proRata.name:'',
              Amount: res.discounts.proRata?res.discounts.proRata.amount:'',
              AmountType: res.discounts.proRata?res.discounts.proRata.amountType:'',
              InvoiceId: res.discounts.proRata?res.discounts.proRata.invoiceId:''
            }
          }
        };
        if(res.invoiceDate)
          this.addInvoice.InvoiceDate = { 
            day: new Date(res.invoiceDate).getDate(), 
            month: new Date(res.invoiceDate).getMonth() + 1, 
            year: new Date(res.invoiceDate).getFullYear()
          }
        let user_data = JSON.parse(localStorage.getItem('autodesk-data'))
        this.processed = res.comments=="Approved"
        this.comments = res.comments
        this.isSuperAdmin = user_data.UserLevelId.indexOf('SUPERADMIN')>-1?true:false
		this.isDistributor = user_data.UserLevelId.indexOf('DISTRIBUTOR')>-1?true:false
        this.checkDiscount = (res.discounts.discount && res.discounts.discount.amount)?true:false
        this.checkProRata = (res.discounts.proRata && res.discounts.proRata.amount)?true:false
        this.cannotChangeInvoiceStatus = ((this.checkDiscount || this.checkProRata) && !this.processed)
        if(this.isSuperAdmin){
          this.processed = false
          this.cannotChangeInvoiceStatus = false
        }
        res.listPricingSKU.map((el)=>{
          if(this.skus[el.siteId])
            this.skus[el.siteId].push(el)
          else
          this.skus[el.siteId] = [el]
        })
        this.loadData()
      })
    })
  }
  
  goBack(){
    window.history.back()
  }

  loadData(){
    let orgId = this.addInvoice.OrgId
    this.service.httpClientGet('api/MainInvoices/PopulateTerritoryAndCountry?OrganizationId='+orgId,{}).subscribe((res:any)=>{
      if(res){
        this.filter.ter = res.territoryId
        this.filter.coun = res.countriesCode
        this.getSites()
        this.service.httpClientGet('api/MainInvoices/GetListInvoiceCountry?territoryId='+this.filter.ter+'&orgId='+orgId,{}).subscribe((res:any)=>{
          this.countries = res
        })
        this.service.httpClientGet('api/MainInvoices/GetListTerritoryById?Tid='+this.filter.ter,{}).subscribe((res:any)=>{
          this.territories = [res]
        })
      }
    })
    this.service.httpClientGet('api/MainInvoices/GetOrganizationName?orgId='+orgId,{}).subscribe((res:any)=>{
      this.orgName = res.orgName
      this.orgPartnerType = res.partnerType
    })
    this.service.httpClientGet('api/MainInvoices/GetListFY',{}).subscribe((res:any)=>{
      this.fys = res
    })
    this.service.httpClientGet('api/MainInvoices/GetPartnerTypes',{}).subscribe((res:any)=>{
      this.types = res
    })
  }

  getSites(){
    let orgId = this.addInvoice.OrgId
    this.service.httpClientGet('api/MainInvoices/GetListSite?orgId='+orgId+'&countrycode='+this.filter.coun,{}).subscribe((res:any)=>{
      this.sites = res?res:[]
      let havePrime = false
      this.sites.map((el)=>{
        if(el.isPrimary)
          havePrime = true
        if(!this.skus[el.siteId])  
          this.skus[el.siteId] = []
      })
      this.havePrimarySite = havePrime
      this.getSkus()
    })
  }

  selectSite(siteId){
    if(this.currentSites.indexOf(siteId)>-1)
      this.currentSites = this.currentSites.filter((el)=>el!=siteId)
    else
      this.currentSites.push(siteId)
  }

  getSkus(){
    let orgId = this.addInvoice.OrgId
    this.service.httpClientGet('api/MainInvoices/GetListPricingSKU?orgId='+orgId+'&countrycode='+this.filter.coun,{}).subscribe((res:any)=>{
      this.sites.map((el)=>{
        let new_data = [...res].map((e)=>{
          let data = {...e}
          this.skus[el.siteId].map((d)=>{
            if(d.id==e.id)
              data = {...d}
          })
          if(el.partnerType.indexOf('ATC')>-1 && el.partnerType.indexOf('AAP')>-1 && data.distributor =='AAP')
            data = null
          return data
        })
        this.skus[el.siteId] = new_data.filter((el)=>el !=null)
      })
    })
  }

  getTotal(value){
    let total = 0
    let discount = this.checkDiscount?Number(this.addInvoice.Discounts.Discount.Amount):0
    let prorata = this.checkProRata?Number(this.addInvoice.Discounts.ProRata.Amount):0
    this.sites.map((e)=>{
      let skus = this.skus[e.siteId].filter((el)=> el.siteId)
      if(value != 'TOTAL_PRICE' && value != 'TOTAL_QTY')
        skus.map((el)=>{
          if(el.distributor==value)
            total = el.qty*Number(el.license) + total
        })
      if(value == 'TOTAL_PRICE' || value == 'TOTAL_DISCOUNT'){
        skus.map((el)=>{
          total = el.qty*el.price + total 
        })
      }
      if(value == 'TOTAL_QTY')
        skus.map((el)=>{
          total = Number(el.qty)+ total 
        })
    })
    if(value == 'TOTAL_PRICE'){
      //if(this.addInvoice.InvoiceDate.month >=8 || this.addInvoice.InvoiceDate.month == 1){
        total = total - prorata*total/100
        if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
          total = total - discount
        if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
          total = total - total*discount/100
      //}else{
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
      //    total = total - discount
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
      //    total = total - total*discount/100
      //}
    }
    if(value == 'TOTAL_DISCOUNT'){
      //if(this.addInvoice.InvoiceDate.month >=8 || this.addInvoice.InvoiceDate.month == 1){
        let dis = prorata*total/100
        if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
          dis = dis + discount
        if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
          dis = dis + total*discount/100
        total = dis
      //}else{
      //  let dis = 0
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
      //    dis = dis + discount
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
      //    dis = dis + total*discount/100
      //  total = dis
      //}
    }
    if(total<0)
      total = 0
    if(value != 'TOTAL_PRICE' && value !='TOTAL_DISCOUNT')
      return total
    return total.toFixed(2)
  }

  resetForm(){
    this.addInvoice = {...this.addInvoice,
      InvoiceNumber: '',
      InvoiceDescription: '',
      InvoiceDate: '',
      Status: 'Pending',
      FinancialYear: '',
      PricingSKUs: [],
      Discounts: {
        Discount:{
          Name: '',
          Amount: '',
          AmountType: '',
          InvoiceId: ''
        },
        ProRata: {
          Name: '',
          Amount: '',
          AmountType: '',
          InvoiceId: ''
        },
      }
    }
    this.checkDiscount = false;
    this.checkProRata = false;
    this.sites.map((el)=>{
      this.skus[el.siteId] = this.skus[el.siteId].map((e)=>{
        return {...e,siteId:'',qty:0}
      })
    })
  }
  
  reject(){
    this.loading = true
    let listSku = []
    this.sites.map((el)=>{
      listSku = listSku.concat(this.skus[el.siteId].filter((e)=> e.siteId))
    })
    if(this.addInvoice.Status == 'Paid' && !this.addInvoice.InvoiceDate){
      this.loading = false
      return swal('ERROR','Please enter payment date','error')
    }
    if(this.checkDiscount && (!this.addInvoice.Discounts.Discount.Amount || !this.addInvoice.Discounts.Discount.AmountType)){
      this.loading = false
      return swal('ERROR','Please enter discount','error')
    }
    if(this.checkProRata && !this.addInvoice.Discounts.ProRata.Amount){
      this.loading = false
      return swal('ERROR','Please enter prorata','error')
    }
    let data = {...this.addInvoice}
    data.PricingSKUs = listSku
    if(this.addInvoice.Status == 'Paid')
      data.InvoiceDate = data.InvoiceDate.year+'/'+data.InvoiceDate.month+'/'+data.InvoiceDate.day
    else
      data.InvoiceDate = ''
    if(!this.checkDiscount)
      data.Discounts.Discount = {
        Id:'',
        Name: '',
        Amount: '',
        AmountType: 'Exactly',
        InvoiceId: ''
      }
    if(!this.checkProRata)
      data.Discounts.ProRata = {
        Id:'',
        Name: '',
        Amount: '',
        AmountType: 'Exactly',
        InvoiceId: ''
      }
    this.service.httpClientPost('api/MainInvoices/UpdateInvoice',data).subscribe((res:any)=>{
      this.loading= false
      let res_json = res
      if(res_json.code == 0)
        swal('ERROR',res_json.message,'error')
      else{
        let user = JSON.parse(localStorage.getItem('autodesk-data'));
        let params = {
          Email: user.Email,
          InvoiceId: this.addInvoice.InvoiceId,
          RejectReason: this.reasonReject
        }
        this.service.httpClientPost('api/MainInvoices/RejectInvoice', params).subscribe((res:any)=>{
          this.loading= false
          let res_json = res
          if(res_json.code == 0)
            swal('ERROR',res_json.message,'error')
          else
            swal('SUCCESS','Reject invoice success','success').then(()=>{
              this.navRouter.navigateByUrl('/manage/organization/detail-organization/'+this.organizationId)
            })
        },(err)=>{
          this.loading = false
          swal('ERROR','Sending Reject Email failed!','error')
        })
      }
    },(err)=>{
      swal('ERROR','Save invoice faild','error')
    })
  }

  updateSku(event,id,siteId){
    this.skus[siteId].map((el)=>{
      if(el.id==id)
        el.qty = event.target.value
      return el
    })
  }
  
  validSku(){
    let valid = true
    this.sites.map((el)=>{
      if(el.isPrimary)
        this.skus[el.siteId].map((sku)=>{
          if(sku.isForPrimarySite && sku.qty && sku.qty!=1)
            valid = false
        })
    })
    return valid
  }

  onSubmit(){
    if(!this.addInvoice.FinancialYear)
      return swal('ERROR','Please Select Financial Year','error')
    this.service.httpClientGet('api/MainInvoices/CheckInvoiceMainSKU?orgId='+this.addInvoice.OrgId+'&financialYear='+this.addInvoice.FinancialYear,{}).subscribe((res1: any)=>{
      this.loading = true
      let listSku = []
      let useMainSku = false
      let mainSkuUsed = false
      this.sites.map((el)=>{
        listSku = listSku.concat(this.skus[el.siteId].filter((e)=> e.siteId))
        if(el.isPrimary)
          this.skus[el.siteId].map((e)=>{
            if(e.isForPrimarySite && e.siteId)
              useMainSku = true
          })
      })
      res1.data.map((el)=>{
         if(el.invoiceId != this.addInvoice.InvoiceId)
          mainSkuUsed = true
      })
      let invalid_qty = false
      listSku.map((el)=>{
        if(el.qty==0 || Number.isInteger(el.qty))
          invalid_qty = true
      })
      if(invalid_qty && !this.isSuperAdmin){
        this.loading = false
        return swal('ERROR','Please enter valid qty for selected sku','error')
      }
      if(useMainSku && mainSkuUsed){
        this.loading = false
        return swal('ERROR','The Main Site SKU has already been selected in this Fiscal Year. It can only be selected once per Fiscal Year. Please select another SKU.','error')
      }
      if(!listSku.length){
        this.loading = false
        return swal('ERROR','Please select at least 1 SKU','error')
      }
      if(this.addInvoice.Status == 'Paid' && !this.addInvoice.InvoiceDate){
        this.loading = false
        return swal('ERROR','Please enter payment date','error')
      }
      if(this.checkDiscount && (!this.addInvoice.Discounts.Discount.Amount || !this.addInvoice.Discounts.Discount.AmountType)){
        this.loading = false
        return swal('ERROR','Please enter discount','error')
      }
      if(this.checkProRata && !this.addInvoice.Discounts.ProRata.Amount){
        this.loading = false
        return swal('ERROR','Please enter prorata','error')
      }
      if(!this.validSku() && !this.isSuperAdmin){
        this.loading = false
        return swal('ERROR','Please enter valid qty for main site','error')
      }
      let data = {...this.addInvoice}
      data.PricingSKUs = listSku
      if(this.addInvoice.Status == 'Paid')
        data.InvoiceDate = data.InvoiceDate.year+'/'+data.InvoiceDate.month+'/'+data.InvoiceDate.day
      else
        data.InvoiceDate = ''
      if(!this.checkDiscount)
        data.Discounts.Discount = {
          Id:'',
          Name: '',
          Amount: '',
          AmountType: 'Exactly',
          InvoiceId: ''
        }
      if(!this.checkProRata)
        data.Discounts.ProRata = {
          Id:'',
          Name: '',
          Amount: '',
          AmountType: 'Exactly',
          InvoiceId: ''
        }
      this.service.httpClientPost('api/MainInvoices/ApplyDiscount',data).subscribe((res:any)=>{
        this.loading= false
        let res_json = res
        if(res_json.code == 0)
          swal('ERROR',res_json.message,'error')
        else
          swal('SUCCESS','Approve invoice success','success').then(()=>{
            this.navRouter.navigateByUrl('/manage/organization/detail-organization/'+this.organizationId)
          })
      },(err)=>{
        swal('ERROR','Approve invoice faild','error')
      })
    })
  }

  isDiscount(){
    return (this.checkDiscount && this.addInvoice.Discounts.Discount.Amount) || (this.checkProRata && this.addInvoice.Discounts.ProRata.Amount)
  }

  onSave(){
    swal({
      title: 'Are you sure you want to edit ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result) {
        if(!this.addInvoice.FinancialYear)
          return swal('ERROR','Please Select Financial Year','error')
        this.service.httpClientGet('api/MainInvoices/CheckInvoiceMainSKU?orgId='+this.addInvoice.OrgId+'&financialYear='+this.addInvoice.FinancialYear,{}).subscribe((res1: any)=>{
          this.loading = true
          let listSku = []
          let useMainSku = false
          let mainSkuUsed = false
          this.sites.map((el)=>{
            listSku = listSku.concat(this.skus[el.siteId].filter((e)=> e.siteId))
            if(el.isPrimary)
              this.skus[el.siteId].map((e)=>{
                if(e.isForPrimarySite && e.siteId)
                  useMainSku = true
              })
          })
          res1.data.map((el)=>{
            if(el.invoiceId != this.addInvoice.InvoiceId)
              mainSkuUsed = true
          })
          if(useMainSku && mainSkuUsed){
            this.loading = false
            return swal('ERROR','Main site sku for current Fy is already in use by another invoice','error')
          }
          if(!listSku.length){
            this.loading = false
            return swal('ERROR','Please select at least 1 SKU','error')
          }
          if(this.addInvoice.Status == 'Paid' && !this.addInvoice.InvoiceDate){
            this.loading = false
            return swal('ERROR','Please enter payment date','error')
          }
          if(this.checkDiscount && (!this.addInvoice.Discounts.Discount.Amount || !this.addInvoice.Discounts.Discount.AmountType)){
            this.loading = false
            return swal('ERROR','Please enter discount','error')
          }
          if(this.checkProRata && !this.addInvoice.Discounts.ProRata.Amount){
            this.loading = false
            return swal('ERROR','Please enter prorata','error')
          }
          if(!this.validSku()){
            this.loading = false
            return swal('ERROR','Please enter valid qty for main site','error')
          }
          let data = {...this.addInvoice}
          data.PricingSKUs = listSku
          if(this.addInvoice.Status == 'Paid')
            data.InvoiceDate = data.InvoiceDate.year+'/'+data.InvoiceDate.month+'/'+data.InvoiceDate.day
          else
            data.InvoiceDate = ''
          if(!this.checkDiscount)
            data.Discounts.Discount = {
              Id:'',
              Name: '',
              Amount: '',
              AmountType: 'Exactly',
              InvoiceId: ''
            }
          if(!this.checkProRata)
            data.Discounts.ProRata = {
              Id:'',
              Name: '',
              Amount: '',
              AmountType: 'Exactly',
              InvoiceId: ''
            }
          this.service.httpClientPost('api/MainInvoices/UpdateInvoice',data).subscribe((res:any)=>{
            this.loading= false
            let res_json = res
            if(res_json.code == 0)
              swal('ERROR',res_json.message,'error')
            else
              swal('SUCCESS','Save invoice success','success').then(()=>{
                this.navRouter.navigateByUrl('/manage/organization/detail-organization/'+this.organizationId)
              })
          },(err)=>{
            swal('ERROR','Save invoice faild','error')
          })
        })
      }
    })
  }

  statusChange(){
    if(this.addInvoice.Status !='Paid')
      this.addInvoice.InvoiceDate = ''
  }
  
  checker(value,option){
    console.log(option,value)
    if(option == 'Discount'){
      this.checkDiscount = value
      this.checkProRata = false
      this.addInvoice.Discounts.Discount.Amount=''
    }
    if(option == 'ProRata'){
      this.checkProRata = value
      this.checkDiscount = false
      if(value)
        this.addInvoice.Discounts.ProRata.Amount='50'
      else
        this.addInvoice.Discounts.ProRata.Amount=''
    }
    console.log('checkDiscount',this.checkDiscount)
    console.log('checkProRata',this.checkProRata)
  }
  
}
