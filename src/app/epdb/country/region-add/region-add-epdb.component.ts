import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { SessionService } from '../../../shared/service/session.service';
import { DatePipe } from '@angular/common';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

@Component({
    selector: 'app-region-add-epdb',
    templateUrl: './region-add-epdb.component.html',
    styleUrls: [
        './region-add-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class RegionAddEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Region';
    addregion: FormGroup;
    public data: any;
    public datageo: any;
    public dataterritory: any;
    dropdownGeo = [];
    selectedGeo = [];
    dropdownSettings = {};
    public useraccesdata: any;
    public loading = false;

    constructor(private router: Router, private service: AppService,
        private formatdate: AppFormatDate, private session: SessionService, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
        //validation
        let geo_code = new FormControl('', Validators.required);
        let region_code = new FormControl('', Validators.required);
        let region_name = new FormControl('', Validators.required);

        this.addregion = new FormGroup({
            geo_code: geo_code,
            region_code: region_code,
            region_name: region_name
        });

    }

    ngOnInit() {
        this.getGeo();
        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            enableCheckAll: false,
            // limitSelection: 1,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    getGeo() {
        //get data Geo
        var data: any;
        this.service.httpClientGet('api/Geo', data)
            .subscribe(result => {
                data = result;
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedGeo = [];
                }
                else {
                    this.dropdownGeo = data.map((item) => {
                        return {
                            id: item.geo_code,
                            itemName: item.geo_name
                        }
                    })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedGeo = [];
    }

    onGeoSelect(item: any) { }

    OnGeoDeSelect(item: any) { }

    //submit form
    onSubmit() {
        this.addregion.controls['geo_code'].markAsTouched();
        this.addregion.controls['region_code'].markAsTouched();
        this.addregion.controls['region_name'].markAsTouched();
        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.addregion.value.region_name))
            return swal('ERROR','Special character not allowed in region name','error')
        if(format.test(this.addregion.value.region_code))
            return swal('ERROR','Special character not allowed in region code','error')
var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.addregion.valid) {
            this.loading = true;
            var dataPost = {
                'geo_code': this.selectedGeo[0].id,
                'region_code': this.addregion.value.region_code,
                'region_name': this.addregion.value.region_name,
                'cuid' : this.useraccesdata.ContactName,
                 'cdate': dateFormat,
				 //this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                'UserId': this.useraccesdata.UserId
            };

            //convert object to json
            let data = JSON.stringify(dataPost);

            //post action
            this.service.httpClientPost(this._serviceUrl, data)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
            setTimeout(() => {
                //redirect
                this.router.navigate(['/admin/country/region-list']);
                this.loading = false;
            }, 1000)
        }
    }

    resetFormAdd() {
        this.addregion.reset({
            'region_code': '',
            'region_name': ''
        });
        this.selectedGeo = [];
    }
}
