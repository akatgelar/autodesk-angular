import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionAddEPDBComponent } from './region-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const RegionAddEPDBRoutes: Routes = [
  {
    path: '',
    component: RegionAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.region.add_new_region',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RegionAddEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [RegionAddEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class RegionAddEPDBModule { }
