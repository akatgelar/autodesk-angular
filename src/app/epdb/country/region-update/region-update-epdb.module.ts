import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionUpdateEPDBComponent } from './region-update-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const RegionUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: RegionUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.region.edit_region',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RegionUpdateEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [RegionUpdateEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class RegionUpdateEPDBModule { }
