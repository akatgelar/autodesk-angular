import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from '../../../shared/service/session.service';
import { DatePipe } from '@angular/common';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

@Component({
    selector: 'app-region-update-epdb',
    templateUrl: './region-update-epdb.component.html',
    styleUrls: [
        './region-update-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class RegionUpdateEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Region';
    editregion: FormGroup;
    public data: any;
    public datageo: any;
    public dataterritory: any;
    public datadetail: any;
    id: string = '';
    dropdownGeo = [];
    selectedGeo = [];
    dropdownSettings = {};
    public useraccesdata: any;
    public loading = false;

    constructor(private router: Router, private service: AppService,
        private formatdate: AppFormatDate, private route: ActivatedRoute, private session: SessionService, private datePipe: DatePipe) {

        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let geo_code = new FormControl('');
        let region_code = new FormControl('');
        let region_name = new FormControl('', Validators.required);

        this.editregion = new FormGroup({
            geo_code: geo_code,
            region_code: region_code,
            region_name: region_name
        });

    }

    getDataRegion(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                this.datadetail = result;
                this.editregion.patchValue({ region_code: this.datadetail.region_code });
                this.editregion.controls["region_code"].disable();
                this.editregion.patchValue({ region_name: this.datadetail.region_name });
                var geos: any;
                this.service.httpClientGet("api/Geo/ByCode/" + this.datadetail.geo_code, geos)
                    .subscribe(res => {
                        geos = res;
                        var geoTemp = {
                            'id': geos.geo_code,
                            'itemName': geos.geo_name
                        }
                        this.selectedGeo.push(geoTemp);
                    }, error => {
                        this.service.errorserver();
                    });
            },
                error => {
                    this.service.errorserver();
                });
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.getGeo();
        this.getDataRegion(this.id);
        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            enableCheckAll: false,
            // limitSelection: 1,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
        // //get data Territory
        // var data = '';
        // this.service.get('api/Territory', data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             this.dataterritory = null;
        //         }
        //         else {
        //             this.dataterritory = JSON.parse(result);
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });
    }

    getGeo() {
        //get data Geo
        var data: any;
        this.service.httpClientGet('api/Geo', data)
            .subscribe(result => {
                data = result;
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedGeo = [];
                }
                else {
                    this.dropdownGeo = data.map((item) => {
                        return {
                            id: item.geo_code,
                            itemName: item.geo_name
                        }
                    })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedGeo = [];
    }

    onGeoSelect(item: any) { }

    OnGeoDeSelect(item: any) { }

    getRegion() {
        //get data Region
        var data = '';
        this.service.httpClientGet(this._serviceUrl, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    this.data = result;
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onSubmitUpdate() {
        // this.editregion.controls['geo_code'].markAsTouched();
        // this.editregion.controls['region_code'].markAsTouched();
        this.editregion.controls['region_name'].markAsTouched();
        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.editregion.value.region_name))
            return swal('ERROR','Special character not allowed in region name','error')
        if(format.test(this.editregion.value.region_code))
            return swal('ERROR','Special character not allowed in region code','error')
        var today = new Date();
		var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.editregion.valid) {
            this.loading = true;
            var dataUpdate = {
                'geo_code': this.selectedGeo[0].id,
                // 'region_code': this.editregion.value.region_code,
                'region_name': this.editregion.value.region_name,
                'muid': this.useraccesdata.ContactName,
                 'mdate': dateFormat,
				 //this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                'cuid' : this.useraccesdata.ContactName,
                'UserId': this.useraccesdata.UserId
            };

            //data form
            let data = JSON.stringify(dataUpdate);

            //put action
            this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
                .subscribe(res=>{
                    console.log(res)
                });

            setTimeout(() => {
                this.getRegion();
                //redirect
                this.router.navigate(['/admin/country/region-list']);
                this.loading = false;
            }, 1000)
            // this.service.httpCLientPut(this._serviceUrl,this.id, data);
            // setTimeout(() => {
            //     this.getRegion();
            //     //redirect
            //     this.router.navigate(['/admin/country/region-list']);
            //     this.loading = false;
            // }, 1000)
        }
    }

    //build form update
    // buildForm(): void {
    //     let region_id_update = new FormControl(this.datadetail.region_id);
    //     let geo_code_update = new FormControl(this.datadetail.geo_code, Validators.required);
    //     let territory_code_update = new FormControl(this.datadetail.territory_code, Validators.required);
    //     let region_code_update = new FormControl(this.datadetail.region_code);
    //     let region_name_update = new FormControl(this.datadetail.region_name, Validators.required);
    //     let cdate = new FormControl(this.datadetail.cdate);
    //     let mdate = new FormControl(this.datadetail.mdate);
    //     let cuid = new FormControl(this.datadetail.cuid);
    //     let muid = new FormControl(this.datadetail.muid);
    //     this.editregion = new FormGroup({
    //         region_id: region_id_update,
    //         geo_code: geo_code_update,
    //         territory_code: territory_code_update,
    //         region_code: region_code_update,
    //         region_name: region_name_update,
    //         cdate: cdate,
    //         mdate: mdate,
    //         cuid: cuid,
    //         muid: muid
    //     });
    // }

    resetFormUpdate() {
        this.selectedGeo = [];
        this.getDataRegion(this.id);
    }
}
