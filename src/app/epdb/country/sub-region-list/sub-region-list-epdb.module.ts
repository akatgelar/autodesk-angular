import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubRegionListEPDBComponent } from './sub-region-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterSubRegionPipe } from './sub-region-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const SubRegionListEPDBRoutes: Routes = [
  {
    path: '',
    component: SubRegionListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.sub_region.sub_region_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SubRegionListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [SubRegionListEPDBComponent, DataFilterSubRegionPipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class SubRegionListEPDBModule { }
