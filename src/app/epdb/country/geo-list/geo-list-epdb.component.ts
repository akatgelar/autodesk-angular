import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterGeo' })
export class DataFilterGeoPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.geo_code.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.geo_name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-geo-list-epdb',
    templateUrl: './geo-list-epdb.component.html',
    styleUrls: [
        './geo-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class GeoListEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Geo';
    public data: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "geo_name";
    public sortOrder: string = "asc";
    public datadetail: any;
    public useraccessdata: any;
    public messageResult: string = '';
    public messageError: string = '';
    public loading = false;


    constructor(public session: SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {

        this.getAll();

        this.accesAddBtn = this.session.checkAccessButton("admin/country/geo-add");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/country/geo-update");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/country/geo-delete");
    }

    getAll() {
        this.loading = true;
        //get data Geo
        var data = '';
        // this.service.httpClientGet(this._serviceUrl,data)
        this.service.httpClientGet(this._serviceUrl, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.loading = false;
                }
                else {
                    this.data = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
    }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                this.datadetail = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    //delete confirm
    openConfirmsSwal(id) {
        // var index = this.data.findIndex(x => x.geo_id == id);
        // this.service.httpClientDelete(this._serviceUrl,this.data,id,index);

        // this.loading = true;
        // var cuid = this.useraccessdata.ContactName;
        // var UserId = this.useraccessdata.UserId;
        // var data = '';
        // this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid  ,data, UserId, 0);
        // this.getAll();
        // this.router.navigate(['/admin/country/geo-list']);
        // this.loading = false;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
                this.loading = true;
                var cuid = this.useraccessdata.ContactName;
                var UserId = this.useraccessdata.UserId;
                var data = '';
                this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data)
                    .subscribe(result => {
                        let tmpData : any = result;
                        this.messageResult = tmpData;
                        var resource = result;
                        if (resource['code'] == '1') {
                            this.getAll();
                            this.router.navigate(['/admin/country/geo-list']);
                            this.loading = false;
                        }
                        else {
                            swal(
                                'Information!',
                                "Delete Data Failed",
                                'error'
                            );
                            this.loading = false;
                        }
                    },
                        error => {
                            this.messageError = <any>error
                            this.service.errorserver();
                            this.loading = false;
                        });
            }
        }).catch(swal.noop);
    }

}
