import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoListEPDBComponent } from './geo-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterGeoPipe } from './geo-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const GeoListEPDBRoutes: Routes = [
  {
    path: '',
    component: GeoListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.geo.geo_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GeoListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [GeoListEPDBComponent, DataFilterGeoPipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class GeoListEPDBModule { }
