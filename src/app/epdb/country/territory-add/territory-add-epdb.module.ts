import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryAddEPDBComponent } from './territory-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const TerritoryAddEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.territory.add_new_territory',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryAddEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [TerritoryAddEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class TerritoryAddEPDBModule { }
