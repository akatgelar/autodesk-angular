import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-territory-add-epdb',
  templateUrl: './territory-add-epdb.component.html',
  styleUrls: [
    './territory-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class TerritoryAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Territory';
  addterritory: FormGroup;
  editterritory: FormGroup;
  private datageo;
  data: any;
  dropdownCountries = [];
  selectedCountry = [];
  dropdownSettings = {};
  public useraccesdata: any;
  public loading = false;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private datePipe: DatePipe) {
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    // let geo_code = new FormControl('', Validators.required);
    // let territory_code = new FormControl('', Validators.required);
    let territory_name = new FormControl('', Validators.required);
    let country_id = new FormControl('', Validators.required);

    this.addterritory = new FormGroup({
      territory_name: territory_name,
      country_id: country_id
    });

  }

  getGeo() {
    //get data Geo
    var data = '';
    this.service.httpClientGet('api/Geo', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datageo = null;
        }
        else {
          this.datageo = result;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  ngOnInit() {
    this.getCountries();
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };
  }

  getCountries() {
    var data: any;
    this.service.httpClientGet("api/Countries/NotOnTerritory", data)
      .subscribe(res => {
        data = res;
        this.dropdownCountries = data.map((item) => {
          return {
            id: item.countries_id,
            itemName: item.countries_name
          }
        })
      }, error => {
        this.service.errorserver();
      });
    this.selectedCountry = [];
  }

  onCountrySelect(item: any) { }

  OnCountryDeSelect(item: any) { }

  //submit form
  onSubmit() {

    // this.addterritory.controls['geo_code'].markAsTouched();
    // this.addterritory.controls['territory_code'].markAsTouched();
    this.addterritory.controls['country_id'].markAsTouched();
    this.addterritory.controls['territory_name'].markAsTouched();
    
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.addterritory.value.territory_name))
      return swal('ERROR','Special character not allowed in territory name','error')

    if (this.addterritory.valid) {
      this.loading = true;
      
      // this.addterritory.value.cdate = this.formatdate.dateJStoYMD(new Date());
      // this.addterritory.value.mdate = this.formatdate.dateJStoYMD(new Date());
      // this.addterritory.value.cuid = "admin";
      // this.addterritory.value.muid = "admin";
     
      var countries_temp = [];
      for (let j = 0; j < this.selectedCountry.length; j++) {
        countries_temp.push(parseInt(this.selectedCountry[j].id));
      }

      var dataPost = {
        'Territory_Name': this.addterritory.value.territory_name,
        'countries_id': countries_temp.toString(),
        // ditutup karena sepertinya ada pc yg bisa pakai script ini ada yg ga bisa
        // 'cdate': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
        'cdate': this.service.ISO_date(new Date()),
        'cuid': this.useraccesdata.ContactName,
        'UserId': this.useraccesdata.UserId
      };
      
      //convert object to json
      let data = JSON.stringify(dataPost);
      // console.log(dataPost);
      
      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

      setTimeout(() => {
        //get data after post
        this.getAllTerritory();  
        //redirect
        this.router.navigate(['/admin/country/territory-list']);
        this.loading = false;
      }, 1000)
    }
  }

  resetFormAdd() {
    this.addterritory.reset();
  }

  getAllTerritory() {
    //get data Region
    var data = '';
    this.service.httpClientGet(this._serviceUrl, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.data = result;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

}
