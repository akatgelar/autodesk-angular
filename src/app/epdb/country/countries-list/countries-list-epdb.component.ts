import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterCountry' })
export class DataFilterCountry {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.countries_code.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.countries_name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.countries_tel.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.geo_code.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.TerritoryId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.region_code.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.subregion_code.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-countries-list-epdb',
    templateUrl: './countries-list-epdb.component.html',
    styleUrls: [
        './countries-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CountriesListEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Countries';
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "countries_name";
    public sortOrder: string = "asc";
    public useraccessdata: any;
    public messageResult: string = '';
    public messageError: string = '';
    public loading = false;

    constructor(public session: SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {
        this.loading = true;
        //get data Countries
        var data = '';
        this.service.httpClientGet(this._serviceUrl, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.loading = false;
                }
                else {
                    this.data = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });

        this.accesAddBtn = this.session.checkAccessButton("admin/country/countries-add");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/country/countries-update");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/country/countries-delete");

    }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                this.datadetail = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    //delete confirm
    openConfirmsSwal(id) {
        // var index = this.data.findIndex(x => x.countries_id == id);
        // setTimeout(() => {
        //     this.service.httpClientDelete(this._serviceUrl, this.data, id, index);            
        // }, 1000);
        // this.loading = true;
        // var cuid = this.useraccessdata.ContactName;
        // var UserId = this.useraccessdata.UserId;
        // var data = '';
        // this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid  ,data,UserId, 0);
        // this.router.navigate(['/admin/country/countries-list']);
        // this.loading = false;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
                this.loading = true;
                var cuid = this.useraccessdata.ContactName;
                var UserId = this.useraccessdata.UserId;
                var data = '';
                this.service.httpClientDelete(this._serviceUrl + '/' + id + '/' + cuid + '/' + UserId, data)
                    .subscribe(result => {
                        console.log(result)
                        let tmpData : any = result;
                        this.messageResult = tmpData;
                        var resource = result;
                        if (resource['code'] == '1') {
                            this.router.navigate(['/admin/country/countries-list']);
                            this.loading = false;
                        }
                        else {
                            swal(
                                'Information!',
                                "Delete Data Failed",
                                'error'
                            );
                            this.loading = false;
                        }
                    },
                        error => {
                            this.messageError = <any>error
                            this.service.errorserver();
                            this.loading = false;
                        });
            }
        }).catch(swal.noop);
    }

}
