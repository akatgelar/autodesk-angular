import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesListEPDBComponent } from './countries-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterCountry } from './countries-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const CountriesListEPDBRoutes: Routes = [
  {
    path: '',
    component: CountriesListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.countries.countries_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CountriesListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CountriesListEPDBComponent, DataFilterCountry],
  providers: [AppService, AppFormatDate, SessionService]
})
export class CountriesListEPDBModule { }
