import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryListEPDBComponent } from './territory-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterTerritoryPipe } from './territory-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const TerritoryListEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.territory.territory_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [TerritoryListEPDBComponent, DataFilterTerritoryPipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class TerritoryListEPDBModule { }
