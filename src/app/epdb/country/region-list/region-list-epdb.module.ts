import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionListEPDBComponent } from './region-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterRegionPipe } from './region-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const RegionListEPDBRoutes: Routes = [
  {
    path: '',
    component: RegionListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.region.region_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RegionListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [RegionListEPDBComponent, DataFilterRegionPipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class RegionListEPDBModule { }
