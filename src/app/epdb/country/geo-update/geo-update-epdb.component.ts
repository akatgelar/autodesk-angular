import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-geo-update-epdb',
  templateUrl: './geo-update-epdb.component.html',
  styleUrls: [
    './geo-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class GeoUpdateEPDBComponent implements OnInit {
  
  private _serviceUrl = 'api/Geo';
  editgeo:FormGroup;
  public rowsOnPage: number = 10;
  public datadetail:any;
  id:string='';
  public loading = false;
  public useraccesdata: any;
 
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute, private session: SessionService) { 

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let geo_code_update = new FormControl({value:'',disabled:true});
    let geo_name_update = new FormControl('');
    this.editgeo = new FormGroup({
      geo_code:geo_code_update,
      geo_name:geo_name_update 
    });

  }

  ngOnInit() { 

    var data = '';
    this.id = this.route.snapshot.params['id'];
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result;
        this.buildForm();  
    },
    error => {
        this.service.errorserver();
    });

  }

  onSubmitUpdate(){

    this.editgeo.controls['geo_name'].markAsTouched();
    
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.editgeo.value.geo_code))
      return swal('ERROR','Special character not allowed in Geo code','error')
    if(format.test(this.editgeo.value.geo_name))
      return swal('ERROR','Special character not allowed in Geo name','error')

    if(this.editgeo.valid){

      this.loading = true;
      this.editgeo.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.editgeo.value.mdate = this.formatdate.dateJStoYMD(new Date());
      this.editgeo.value.muid = this.useraccesdata.ContactName;      
      this.editgeo.value.cuid = this.useraccesdata.ContactName;
      this.editgeo.value.UserId = this.useraccesdata.UserId;

      //conver to json
      let data = JSON.stringify(this.editgeo.value);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });

      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/country/geo-list']);
        this.loading = false;
      }, 1000)
      // this.service.httpCLientPut(this._serviceUrl,this.id, data)

      // setTimeout(() => {
      //   //redirect
      //   this.router.navigate(['/admin/country/geo-list']);
      //   this.loading = false;
      // }, 1000)
    
    }
  }

  //build form update
  buildForm(): void {
    let geo_id = new FormControl(this.datadetail.geo_id);
    let geo_code = new FormControl(this.datadetail.geo_code);
    let geo_name = new FormControl(this.datadetail.geo_name,Validators.required); 
    let cdate = new FormControl(this.datadetail.cdate);
    let mdate = new FormControl(this.datadetail.mdate);
    let cuid = new FormControl(this.datadetail.cuid);
    let muid = new FormControl(this.datadetail.muid);

    this.editgeo = new FormGroup({
      geo_id:geo_id,
      geo_code:geo_code,
      geo_name:geo_name,
      cdate:cdate,
      mdate:mdate,
      cuid:cuid,
      muid:muid
    });
  }

  resetFormUpdate(){
    this.editgeo.reset({
      'geo_code':this.datadetail.geo_code,
      'geo_name':this.datadetail.geo_name
    });
  }

}
