import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoUpdateEPDBComponent } from './geo-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const GeoUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: GeoUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.geo.edit_geo',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GeoUpdateEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [GeoUpdateEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class GeoUpdateEPDBModule { }
