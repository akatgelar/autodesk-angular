import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubRegionAddEPDBComponent } from './sub-region-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const SubRegionAddEPDBRoutes: Routes = [
  {
    path: '',
    component: SubRegionAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.sub_region.add_new_sub_region',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SubRegionAddEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [SubRegionAddEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class SubRegionAddEPDBModule { }
