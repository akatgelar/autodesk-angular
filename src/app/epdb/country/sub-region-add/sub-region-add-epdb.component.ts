import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-sub-region-add-epdb',
    templateUrl: './sub-region-add-epdb.component.html',
    styleUrls: [
        './sub-region-add-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SubRegionAddEPDBComponent implements OnInit {

    private _serviceUrl = 'api/SubRegion';
    addsubregion: FormGroup;
    public datageo: any;
    public dataterritory: any;
    public dataregion: any;
    public loading = false;
    public useraccesdata: any;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

        // get data User
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        //validation
        let subregion_code = new FormControl('', Validators.required);
        let subregion_name = new FormControl('', Validators.required);
        let geo_code = new FormControl('', Validators.required);
        // let territory_code = new FormControl('', Validators.required);
        let region_code = new FormControl('', Validators.required);

        this.addsubregion = new FormGroup({
            geo_code: geo_code,
            // territory_code: territory_code,
            subregion_code: subregion_code,
            subregion_name: subregion_name,
            region_code: region_code
        });

    }

    getRegion(value) {
        var data = '';
        this.service.httpClientGet('api/Region/RegionFilter/' + value, data)
            .subscribe(result => {
                this.dataregion = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    ngOnInit() {

        //get data Geo
        var data = '';
        this.service.httpClientGet('api/Geo', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.datageo = null;
                }
                else {
                    this.datageo = result;
                }
            },
                error => {
                    this.service.errorserver();
                });

        //get data Territory
        /*var data = '';
        this.service.get('api/Territory', data)
            .subscribe(result => {
                data = result;
                console.log(JSON.parse(data));
                if (result == "Not found") {
                    this.service.notfound();
                    this.dataterritory = null;
                }
                else {
                    this.dataterritory = JSON.parse(result);
                }
            },
                error => {
                    this.service.errorserver();
                });*/

        //get data Region
        /*var data = '';
        this.service.get('api/Region', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dataregion = null;
                }
                else {
                    this.dataregion = JSON.parse(result);
                }
            },
                error => {
                    this.service.errorserver();
                });*/
    }

    //submit form
    onSubmit() {

        this.addsubregion.controls['geo_code'].markAsTouched();
        // this.addsubregion.controls['territory_code'].markAsTouched();
        this.addsubregion.controls['region_code'].markAsTouched();
        this.addsubregion.controls['subregion_code'].markAsTouched();
        this.addsubregion.controls['subregion_name'].markAsTouched();

        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.addsubregion.value.subregion_code))
            return swal('ERROR','Special character not allowed in sub region code','error')
        if(format.test(this.addsubregion.value.subregion_name))
            return swal('ERROR','Special character not allowed in sub region name','error')
        if (this.addsubregion.valid) {
            this.loading = true;
            this.addsubregion.value.cdate = this.formatdate.dateJStoYMD(new Date());
            this.addsubregion.value.mdate = this.formatdate.dateJStoYMD(new Date());
            // this.addsubregion.value.cuid = "admin";
            // this.addsubregion.value.muid = "admin";
            this.addsubregion.value.muid = this.useraccesdata.ContactName;
            this.addsubregion.value.cuid = this.useraccesdata.ContactName;
            this.addsubregion.value.UserId = this.useraccesdata.UserId;

            //convert object to json
            let data = JSON.stringify(this.addsubregion.value);

            //post action
            this.service.httpClientPost(this._serviceUrl, data)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
            setTimeout(() => {
                //redirect
                this.router.navigate(['/admin/country/sub-region-list']);
                this.loading = false;
            }, 1000);
        }
    }

    resetFormAdd() {
        this.addsubregion.reset({
            'geo_code': '',
            // 'territory_code': '',
            'region_code': ''
        });
    }
}
