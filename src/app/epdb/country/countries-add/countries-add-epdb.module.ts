import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesAddEPDBComponent } from './countries-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const CountriesAddEPDBRoutes: Routes = [
  {
    path: '',
    component: CountriesAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.countries.add_new_country',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CountriesAddEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [CountriesAddEPDBComponent],
  providers: [AppService, AppFormatDate, AppFilterGeo]
})
export class CountriesAddEPDBModule { }
