import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { SessionService } from '../../../shared/service/session.service';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'app-countries-add-epdb',
    templateUrl: './countries-add-epdb.component.html',
    styleUrls: [
        './countries-add-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CountriesAddEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Countries';
    addcountries: FormGroup;
    public datageo: any;
    public dataterritory: any;
    public dataregion: any;
    public datasubregion: any;
    private territory_code;

    dropdownGeo = [];
    selectedGeo = [];
    dropdownListRegion = [];
    selectedRegion = [];
    dropdownListSubregion = [];
    selectedSubregion = [];
    dropdownSettings = {};
    public useraccesdata: any;
    public loading = false;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate,
        private filterGeo: AppFilterGeo, private session: SessionService, private datePipe: DatePipe) {

        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        //validation
        let geo_code = new FormControl('', Validators.required);
        let territory_code = new FormControl('');
        let region_code = new FormControl('', Validators.required);
        let subregion_code = new FormControl('', Validators.required);
        let countries_code = new FormControl('', Validators.required);
        let countries_name = new FormControl('', Validators.required);
        let countries_tel = new FormControl('', [Validators.required, CustomValidators.number]);

        this.addcountries = new FormGroup({
            geo_code: geo_code,
            territory_code: territory_code,
            region_code: region_code,
            subregion_code: subregion_code,
            countries_code: countries_code,
            countries_name: countries_name,
            countries_tel: countries_tel
        });

    }

    getGeo() {
        //get data Geo
        var data: any;
        this.service.httpClientGet('api/Geo', data)
            .subscribe(result => {
                data = result;
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedGeo = [];
                }
                else {
                    this.dropdownGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedGeo = [];
    }

    geoid = [];
    onGeoSelect(item: any) {
        // this.dropdownListRegion = [];
        // this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        // this.selectedRegion = [];
        // this.selectedSubregion = [];
        // this.dropdownListSubregion = [];
        this.geoid = [];
        this.selectedRegion = [];
        this.selectedSubregion = [];
        this.dropdownListSubregion = [];
        this.geoid.push(item.id);

        // Region
        var data = '';
        // console.log("api/Region/RegionFilter/" + this.geoid.toString(), data);
        this.service.httpClientGet("api/Region/RegionFilter/" + this.geoid.toString(), data)
            .subscribe(result => {
                // console.log(result)
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListRegion = null;
                }
                else {
                    this.dropdownListRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].region_id,
                          itemName: result[Index].region_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        // this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        this.selectedRegion = [];
        this.selectedSubregion = [];
        this.dropdownListRegion = [];
        this.dropdownListSubregion = [];
    }

    regionid = [];
    onRegionSelect(item: any) {
        // this.dropdownListSubregion = [];
        // this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubregion);
        // this.selectedSubregion = [];
        this.regionid = [];
        this.selectedSubregion = [];
        this.regionid.push(item.id);

        // Sub Region
        var data = '';
        // console.log("api/SubRegion/SubRegionFilter/" + this.regionid.toString(), data)
        this.service.httpClientGet("api/SubRegion/SubRegionFilter/" + this.regionid.toString(), data)
            .subscribe(result => {
                // console.log(result)
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubregion = null;
                }
                else {
                    this.dropdownListSubregion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_id,
                          itemName: result[Index].subregion_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                })
    }

    OnRegionDeSelect(item: any) {
        // this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubregion);
        this.selectedSubregion = [];
        this.dropdownListSubregion = [];
    }

    onSubregionSelect(item: any) { }

    OnSubregionDeSelect(item: any) { }


    ngOnInit() {
        this.getGeo();
        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            enableCheckAll: false,
            // limitSelection: 1,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    //submit form
    onSubmit() {

        this.addcountries.controls['geo_code'].markAsTouched();
        this.addcountries.controls['region_code'].markAsTouched();
        this.addcountries.controls['subregion_code'].markAsTouched();
        this.addcountries.controls['countries_code'].markAsTouched();
        this.addcountries.controls['countries_name'].markAsTouched();
        this.addcountries.controls['countries_tel'].markAsTouched();
        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.addcountries.value.countries_code))
            return swal('ERROR','Special character not allowed in Countries code','error')
        if(format.test(this.addcountries.value.countries_name))
            return swal('ERROR','Special character not allowed in Countries name','error')
        if(format.test(this.addcountries.value.countries_tel))
            return swal('ERROR','Special character not allowed in Countries tel','error')
var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.addcountries.valid) {

            this.loading = true;
            var temp: any;
            this.service.httpClientGet("api/Subregion/" + this.selectedSubregion[0].id, temp)
                .subscribe(res => {
                    temp = res;
                    var dataPost = {
                        'countries_code': this.addcountries.value.countries_code,
                        'countries_name': this.addcountries.value.countries_name,
                        'geo_code': temp.geo_code,
                        'region_code': temp.region_code,
                        'subregion_code': temp.subregion_code,
                        'TerritoryId': '',
                        'geo_name': temp.geo_name,
                        'region_name': temp.region_name,
                        'subregion_name': temp.subregion_name,
                        'countries_tel': this.addcountries.value.countries_tel,
                        'MarketTypeId': 0,
                        'Embargoed': 0,
                        'cuid': this.useraccesdata.ContactName,
                         'cdate': dateFormat,
						//this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                        'UserId': this.useraccesdata.UserId
                    };

                    //convert object to json
                    let data = JSON.stringify(dataPost);

                    //post action
                    this.service.httpClientPost(this._serviceUrl, data)
					 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

                    setTimeout(() => {
                        //redirect
                        this.router.navigate(['/admin/country/countries-list']);
                        this.loading = false;
                    }, 1000)
                }, error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        }
    }

    resetFormAdd() {
        this.addcountries.reset({
            'countries_code': '',
            'countries_name': '',
            'countries_tel': ''
        });
        this.selectedGeo = [];
        this.dropdownListRegion = [];
        this.selectedRegion = [];
        this.dropdownListSubregion = [];
        this.selectedSubregion = [];
    }

}
