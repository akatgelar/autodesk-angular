import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-region-detail-epdb',
  templateUrl: './region-detail-epdb.component.html',
  styleUrls: [
    './region-detail-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class RegionDetailEPDBComponent implements OnInit {
  
  private _serviceUrl = 'api/Region';
  editregion:FormGroup;
  public data:any;
  public datageo:any;
  public dataterritory:any;
  public datadetail:any;
  id:string='';
 
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute) { 

    let geo_code_update = new FormControl('', Validators.required);
    let territory_code_update = new FormControl('', Validators.required);
    let region_code_update = new FormControl({value:'',disabled:true}, Validators.required);
    let region_name_update = new FormControl('', Validators.required);
    this.editregion = new FormGroup({
      geo_code:geo_code_update,
      territory_code:territory_code_update,
      region_code:region_code_update,
      region_name:region_name_update
    });

  }

  ngOnInit() {

    //get data Geo
    var data = '';
    this.service.httpClientGet('api/Geo',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.datageo = null;
        }
        else{
            this.datageo = result; 
        } 
    },
    error => {
        this.service.errorserver();
    });

    //get data Territory
    var data = '';
    this.service.httpClientGet('api/Territory',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dataterritory = null;
        }
        else{
            this.dataterritory = result; 
        } 
    },
    error => {
        this.service.errorserver();
    });

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result;
        this.buildForm();  
    },
    error => {
        this.service.errorserver();
    });
  }


  onSubmitUpdate(){
    this.editregion.controls['geo_code'].markAsTouched();
    this.editregion.controls['territory_code'].markAsTouched();
    this.editregion.controls['region_code'].markAsTouched();
    this.editregion.controls['region_name'].markAsTouched();

    if(this.editregion.valid){
        this.editregion.value.cdate = this.formatdate.dateJStoYMD(new Date());
        this.editregion.value.mdate = this.formatdate.dateJStoYMD(new Date());

        //data form
        let data = JSON.stringify(this.editregion.value);

        //put action
        this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
            .subscribe(res=>{
                console.log(res)
            });
        //this.service.httpCLientPut(this._serviceUrl,this.id, data);

        //redirect
        this.router.navigate(['/admin/country/region-list']);
    }
  }

  //build form update
  buildForm(): void {
    let region_id_update = new FormControl(this.datadetail.region_id);
    let geo_code_update = new FormControl(this.datadetail.geo_code, Validators.required);
    let territory_code_update = new FormControl(this.datadetail.territory_code, Validators.required);
    let region_code_update = new FormControl(this.datadetail.region_code);
    let region_name_update = new FormControl(this.datadetail.region_name, Validators.required);
    let cdate = new FormControl(this.datadetail.cdate);
    let mdate = new FormControl(this.datadetail.mdate);
    let cuid = new FormControl(this.datadetail.cuid);
    let muid = new FormControl(this.datadetail.muid);
    this.editregion = new FormGroup({
        region_id:region_id_update,
        geo_code:geo_code_update,
        territory_code:territory_code_update,
        region_code:region_code_update,
        region_name:region_name_update,
        cdate:cdate,
        mdate:mdate,
        cuid:cuid,
        muid:muid 
    });
}

  resetFormUpdate(){
    this.editregion.reset({
        'geo_code':this.datadetail.geo_code,
        'territory_code':this.datadetail.territory_code,
        'region_code':this.datadetail.region_code,
        'region_name':this.datadetail.region_name
    });
  }  
}
