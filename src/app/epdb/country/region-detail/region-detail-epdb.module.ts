import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionDetailEPDBComponent } from './region-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const RegionDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: RegionDetailEPDBComponent,
    data: {
      breadcrumb: 'Detail Region',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RegionDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [RegionDetailEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class RegionDetailEPDBModule { }
