import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryDetailEPDBComponent } from './territory-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const TerritoryDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryDetailEPDBComponent,
    data: {
      breadcrumb: 'Detail Territory',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [TerritoryDetailEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class TerritoryDetailEPDBModule { }
