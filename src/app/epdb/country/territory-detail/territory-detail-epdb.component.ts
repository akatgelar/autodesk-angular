import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-territory-detail-epdb',
  templateUrl: './territory-detail-epdb.component.html',
  styleUrls: [
    './territory-detail-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class TerritoryDetailEPDBComponent implements OnInit {
  
  private _serviceUrl = 'api/Territory';
  editterritory:FormGroup;
  public datadetail:any;
  id:string='';
 
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute) { 

  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result;
      
    },
    error => {
        this.service.errorserver();
    });
  }


  

}
