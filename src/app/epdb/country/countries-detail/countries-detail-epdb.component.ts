import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

@Component({
    selector: 'app-countries-detail-epdb',
    templateUrl: './countries-detail-epdb.component.html',
    styleUrls: [
        './countries-detail-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CountriesDetailEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Countries';
    editcountries: FormGroup;
    public datageo: any;
    public dataterritory: any;
    public dataregion: any;
    public datasubregion: any;
    public datadetail: any;
    id:string = '';

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private route:ActivatedRoute) {

        let geo_code_update = new FormControl('');
        let territory_code_update = new FormControl('');
        let region_code_update = new FormControl('');
        let subregion_code_update = new FormControl('');
        let countries_code_update = new FormControl({ value: '', disabled: true });
        let countries_name_update = new FormControl('');
        let countries_tel_update = new FormControl('');

        this.editcountries = new FormGroup({
            geo_code: geo_code_update,
            territory_code: territory_code_update,
            region_code: region_code_update,
            subregion_code: subregion_code_update,
            countries_code: countries_code_update,
            countries_name: countries_name_update,
            countries_tel: countries_tel_update
        });

    }

    ngOnInit() {

       
        this.id = this.route.snapshot.params['id'];
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
            .subscribe(result => {
                this.datadetail = result;
            },
            error => {
                this.service.errorserver();
            });
    }

   

  



}
