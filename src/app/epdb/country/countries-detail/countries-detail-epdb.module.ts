import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesDetailEPDBComponent } from './countries-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const CountriesDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: CountriesDetailEPDBComponent,
    data: {
      breadcrumb: 'Detail Countries',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CountriesDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [CountriesDetailEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class CountriesDetailEPDBModule { }
