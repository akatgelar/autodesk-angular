import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-geo-detail-epdb',
  templateUrl: './geo-detail-epdb.component.html',
  styleUrls: [
    './geo-detail-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class GeoDetailEPDBComponent implements OnInit {
  
  private _serviceUrl = 'api/Geo';
  editgeo:FormGroup;
  public rowsOnPage: number = 10;
  public datadetail:any;
  id:string='';
 
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute) { 

    let geo_code_update = new FormControl({value:'',disabled:true});
    let geo_name_update = new FormControl('');
    this.editgeo = new FormGroup({
      geo_code:geo_code_update,
      geo_name:geo_name_update 
    });

  }

  ngOnInit() { 

    var data = '';
    this.id = this.route.snapshot.params['id'];
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result;
        this.buildForm();  
    },
    error => {
        this.service.errorserver();
    });

  }

  onSubmitUpdate(){

    this.editgeo.controls['geo_name'].markAsTouched();
    
    if(this.editgeo.valid){

      this.editgeo.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.editgeo.value.mdate = this.formatdate.dateJStoYMD(new Date());

      //conver to json
      let data = JSON.stringify(this.editgeo.value);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });

      //redirect
      this.router.navigate(['/admin/country/geo-list']);
    
    }
  }

  //build form update
  buildForm(): void {
    let geo_id = new FormControl(this.datadetail.geo_id);
    let geo_code = new FormControl(this.datadetail.geo_code);
    let geo_name = new FormControl(this.datadetail.geo_name,Validators.required); 
    let cdate = new FormControl(this.datadetail.cdate);
    let mdate = new FormControl(this.datadetail.mdate);
    let cuid = new FormControl(this.datadetail.cuid);
    let muid = new FormControl(this.datadetail.muid);

    this.editgeo = new FormGroup({
      geo_id:geo_id,
      geo_code:geo_code,
      geo_name:geo_name,
      cdate:cdate,
      mdate:mdate,
      cuid:cuid,
      muid:muid
    });
  }

  resetFormUpdate(){
    this.editgeo.reset({
      'geo_code':this.datadetail.geo_code,
      'geo_name':this.datadetail.geo_name
    });
  }

}
