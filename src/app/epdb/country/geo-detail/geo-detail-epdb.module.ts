import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoDetailEPDBComponent } from './geo-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

export const GeoDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: GeoDetailEPDBComponent,
    data: {
      breadcrumb: 'Detail Geo',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GeoDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [GeoDetailEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class GeoDetailEPDBModule { }
