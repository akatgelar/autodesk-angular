import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-country-epdb',
  templateUrl: './country-epdb.component.html',
  styleUrls: [
    './country-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class CountryEPDBComponent implements OnInit {

  messageError: string = ''; 
  public datageo: any;
  public dataregion:any;
  public datasubregion:any;
  public datacountries:any;

  constructor(private service:AppService) { }

  ngOnInit() {
    //get data Geo
    var data = '';
    this.service.httpClientGet('api/Geo',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.datageo = null;
        }
        else{
            this.datageo =result; 
        } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

    //get data Region
    var data = '';
    this.service.httpClientGet('api/Region',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dataregion = null;
        }
        else{
            this.dataregion =result; 
        } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

    //get data SubRegion
    var data = '';
    this.service.httpClientGet('api/SubRegion',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.datasubregion = null;
        }
        else{
            this.datasubregion = result; 
        } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

    //get data Countries
    var data = '';
    this.service.httpClientGet('api/Countries',data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.datacountries = null;
        }
        else{
            this.datacountries = result; 
        } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  }

}