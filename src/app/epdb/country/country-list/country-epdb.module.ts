import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryEPDBComponent } from './country-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const CountryEPDBRoutes: Routes = [
  {
    path: '',
    component: CountryEPDBComponent,
    data: {
      breadcrumb: 'List of Countries',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CountryEPDBRoutes),
    SharedModule
  ],
  declarations: [CountryEPDBComponent],
  providers:[AppService]
})
export class CountryEPDBModule { }
