import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-geo-add-epdb',
  templateUrl: './geo-add-epdb.component.html',
  styleUrls: [
    './geo-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class GeoAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Geo';
  addgeo: FormGroup;
  public loading = false;
  public useraccesdata: any;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let geo_code = new FormControl('', Validators.required);
    let geo_name = new FormControl('', Validators.required);

    this.addgeo = new FormGroup({
      geo_code: geo_code,
      geo_name: geo_name
    });

  }

  ngOnInit() { }

  //submit form
  onSubmit() {

    this.addgeo.controls['geo_code'].markAsTouched();
    this.addgeo.controls['geo_name'].markAsTouched();
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.addgeo.value.geo_code))
      return swal('ERROR','Special character not allowed in Geo code','error')
    if(format.test(this.addgeo.value.geo_name))
      return swal('ERROR','Special character not allowed in Geo name','error')
    if (this.addgeo.valid) {

      this.loading = true;
      this.addgeo.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.addgeo.value.mdate = this.formatdate.dateJStoYMD(new Date());
      // this.addgeo.value.cuid = "Admin (Default)";
      // this.addgeo.value.muid = "Admin (Default)";
      this.addgeo.value.muid = this.useraccesdata.ContactName;      
      this.addgeo.value.cuid = this.useraccesdata.ContactName;
      this.addgeo.value.UserId = this.useraccesdata.UserId;

      //convert object to json
      let data = JSON.stringify(this.addgeo.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/country/geo-list']);
        this.loading = false;
      }, 1000)
    }
  }

  resetFormAdd() {
    this.addgeo.reset();
  }

}
