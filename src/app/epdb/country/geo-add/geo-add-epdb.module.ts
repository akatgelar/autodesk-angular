import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoAddEPDBComponent } from './geo-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const GeoAddEPDBRoutes: Routes = [
  {
    path: '',
    component: GeoAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.geo.add_new_geo',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GeoAddEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [GeoAddEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class GeoAddEPDBModule { }
