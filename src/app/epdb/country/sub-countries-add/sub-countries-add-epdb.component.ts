import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({ name: 'dataFilterSubCountry' })
export class DataFilterSubCountry {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
                (row.geo_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.territory_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.region_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.subregion_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.countries_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.subcountries_code.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.subcountries_name.toLowerCase().indexOf(query.toLowerCase()) > -1));
        } 
        return array;
    }
}

@Component({
  selector: 'app-sub-countries-add-epdb',
  templateUrl: './sub-countries-add-epdb.component.html',
  styleUrls: [
    './sub-countries-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SubCountriesAddEPDBComponent implements OnInit {
  
  private _serviceUrl = 'api/SubCountries';
  messageResult: string = '';
  messageError: string = '';
  addsubcountries: FormGroup;
  editsubcountries:FormGroup;
  public datageo:any;
  public dataterritory:any;
  public dataregion:any;
  public datasubregion:any;
  public datacountries:any;
  public data:any;
  public datadetail:any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
 
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate) { 

    //validation
    let geo_code = new FormControl('', Validators.required);
    let territory_code = new FormControl('', Validators.required);
    let region_code = new FormControl('', Validators.required);
    let subregion_code = new FormControl('', Validators.required);  
    let countries_code = new FormControl('', Validators.required); 
    let subcountries_code = new FormControl('', Validators.required); 
    let subcountries_name = new FormControl('', Validators.required); 

    this.addsubcountries = new FormGroup({
      geo_code:geo_code,
      territory_code:territory_code,
      region_code:region_code,
      subregion_code:subregion_code, 
      countries_code:countries_code,
      subcountries_code:subcountries_code, 
      subcountries_name:subcountries_name
    });

    let geo_code_update = new FormControl('', Validators.required);
    let territory_code_update = new FormControl('', Validators.required);
    let region_code_update = new FormControl('', Validators.required);
    let subregion_code_update = new FormControl('', Validators.required);  
    let countries_code_update = new FormControl('', Validators.required); 
    let subcountries_code_update = new FormControl({value:'',disabled:true}, Validators.required); 
    let subcountries_name_update = new FormControl('', Validators.required); 

    this.editsubcountries = new FormGroup({
      geo_code:geo_code_update,
      territory_code:territory_code_update,
      region_code:region_code_update,
      subregion_code:subregion_code_update, 
      countries_code:countries_code_update,
      subcountries_code:subcountries_code_update, 
      subcountries_name:subcountries_name_update
    });
   

  }

    ngOnInit() {
        
        //get data Geo
        var data = '';
        this.service.httpClientGet('api/Geo',data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.datageo = null;
            }
            else{
                this.datageo = result; 
            } 
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });

        //get data Territory
        var data = '';
        this.service.httpClientGet('api/Territory',data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.dataterritory = null;
            }
            else{
                this.dataterritory = result; 
            } 
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });

        //get data Region
        var data = '';
        this.service.httpClientGet('api/Region',data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.dataregion = null;
            }
            else{
                this.dataregion = result; 
            } 
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });

        //get data SubRegion
        var data = '';
        this.service.httpClientGet('api/SubRegion',data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.datasubregion = null;
            }
            else{
                this.datasubregion = result; 
            } 
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });

        //get data SubRegion
        var data = '';
        this.service.httpClientGet('api/Countries',data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.datacountries = null;
            }
            else{
                this.datacountries = result; 
            } 
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });
        
        this.getAll();
            
    }

    getAll(){
        //get data SubRegion
        var data = '';
        this.service.httpClientGet(this._serviceUrl,data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
            }
            else{
                this.data = result; 
            } 
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });
    }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl+'/'+id, data)
        .subscribe(result => {
            this.datadetail = result;
            this.buildForm();  
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });
    }

    //submit form
    onSubmit() {
        this.addsubcountries.controls['geo_code'].markAsTouched();
        this.addsubcountries.controls['territory_code'].markAsTouched();
        this.addsubcountries.controls['region_code'].markAsTouched();
        this.addsubcountries.controls['subregion_code'].markAsTouched();
        this.addsubcountries.controls['countries_code'].markAsTouched();
        this.addsubcountries.controls['subcountries_code'].markAsTouched();
        this.addsubcountries.controls['subcountries_name'].markAsTouched();

        if(this.addsubcountries.valid){
            this.addsubcountries.value.cdate = this.formatdate.dateJStoYMD(new Date());
            this.addsubcountries.value.mdate = this.formatdate.dateJStoYMD(new Date());
            this.addsubcountries.value.cuid = "admin";
            this.addsubcountries.value.muid = "admin";
    
            //convert object to json
            let data = JSON.stringify(this.addsubcountries.value);
            
            //post action
            this.service.httpClientPost(this._serviceUrl, data)
                .subscribe(res => {
                    console.log(res);
                });

            setTimeout(() => {
                this.ngOnInit()
            }, 1000)

            //reset form
            this.resetFormAdd();
        }
    }

  onSubmitUpdate(){
    this.editsubcountries.controls['geo_code'].markAsTouched();
    this.editsubcountries.controls['territory_code'].markAsTouched();
    this.editsubcountries.controls['region_code'].markAsTouched();
    this.editsubcountries.controls['subregion_code'].markAsTouched();
    this.editsubcountries.controls['countries_code'].markAsTouched();
    this.editsubcountries.controls['subcountries_code'].markAsTouched();
    this.editsubcountries.controls['subcountries_name'].markAsTouched();

    if(this.editsubcountries.valid){
        this.editsubcountries.value.cdate = this.formatdate.dateJStoYMD(new Date());
        this.editsubcountries.value.mdate = this.formatdate.dateJStoYMD(new Date());
        
        //data form
        let data = JSON.stringify(this.editsubcountries.value);
    
        //put action
        var index = this.data.findIndex(x => x.subcountries_id == this.editsubcountries.value.subcountries_id);
       // //this.service.httpCLientPutModal(this._serviceUrl, this.editsubcountries.value.subcountries_id, data, index, this.data, this.editsubcountries.value);
    }
  }

  //build form update
  buildForm(): void {

    let subcountries_id_update = new FormControl(this.datadetail.subcountries_id);
    let geo_code_update = new FormControl(this.datadetail.geo_code, Validators.required);
    let territory_code_update = new FormControl(this.datadetail.territory_code, Validators.required);
    let region_code_update = new FormControl(this.datadetail.region_code, Validators.required);
    let subregion_code_update = new FormControl(this.datadetail.subregion_code, Validators.required);  
    let countries_code_update = new FormControl(this.datadetail.countries_code, Validators.required); 
    let subcountries_code_update = new FormControl(this.datadetail.subcountries_code); 
    let subcountries_name_update = new FormControl(this.datadetail.subcountries_name, Validators.required); 
    let cdate = new FormControl(this.datadetail.cdate);
    let mdate = new FormControl(this.datadetail.mdate);
    let cuid = new FormControl(this.datadetail.cuid);
    let muid = new FormControl(this.datadetail.muid); 

    this.editsubcountries = new FormGroup({
        subcountries_id:subcountries_id_update,
        geo_code:geo_code_update,
        territory_code:territory_code_update,
        region_code:region_code_update,
        subregion_code:subregion_code_update, 
        countries_code:countries_code_update,
        subcountries_code:subcountries_code_update, 
        subcountries_name:subcountries_name_update,
        cdate:cdate,
        mdate:mdate,
        cuid:cuid,
        muid:muid 
    });    
  }

    //delete confirm
    openConfirmsSwal(id) {
        var index = this.data.findIndex(x => x.subcountries_id == id);
        //this.service.httpClientDelete(this._serviceUrl, this.data, id, index);
        this.service.httpClientDelete(this._serviceUrl+'/id',this.data)
    }

    resetFormAdd(){
        this.addsubcountries.reset({
            'geo_code':'',
            'territory_code':'',
            'region_code':'',
            'subregion_code':'',
            'countries_code': ''
        });
    }

    resetFormUpdate(){
        this.editsubcountries.reset({
            'geo_code':this.datadetail.geo_code,
            'territory_code':this.datadetail.territory_code,
            'region_code':this.datadetail.region_code,
            'subregion_code':this.datadetail.subregion_code,
            'countries_code':this.datadetail.countries_code,
            'subcountries_code':this.datadetail.subcountries_code,
            'subcountries_name':this.datadetail.subcountries_name
        });
    }
 

}
