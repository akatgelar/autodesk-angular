import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubCountriesAddEPDBComponent } from './sub-countries-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

import { DataFilterSubCountry } from './sub-countries-add-epdb.component';

export const SubCountriesAddEPDBRoutes: Routes = [
  {
    path: '',
    component: SubCountriesAddEPDBComponent,
    data: {
      breadcrumb: 'List Sub Countries',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SubCountriesAddEPDBRoutes),
    SharedModule
  ],
  declarations: [SubCountriesAddEPDBComponent, DataFilterSubCountry],
  providers:[AppService, AppFormatDate]
})
export class SubCountriesAddEPDBModule { }
