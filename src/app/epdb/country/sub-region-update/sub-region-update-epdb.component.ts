import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-sub-region-update-epdb',
    templateUrl: './sub-region-update-epdb.component.html',
    styleUrls: [
        './sub-region-update-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SubRegionUpdateEPDBComponent implements OnInit {

    private _serviceUrl = 'api/SubRegion';
    public datageo: any;
    public dataterritory: any;
    public dataregion: any;
    public datadetail: any;
    editsubregion: FormGroup;
    id: string = '';
    public loading = false;

    useraccesdata:any;

    constructor(public session: SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let geo_code_update = new FormControl('', Validators.required);
        let region_code_update = new FormControl('', Validators.required);
        let subregion_code_update = new FormControl({ value: '', disabled: true }, Validators.required);
        let subregion_name_update = new FormControl('', Validators.required);
        this.editsubregion = new FormGroup({
            geo_code: geo_code_update,
            region_code: region_code_update,
            subregion_code: subregion_code_update,
            subregion_name: subregion_name_update
        });

    }

    getRegion(value) {
        var data = '';
        this.service.httpClientGet('api/Region/RegionFilter/' + value, data)
            .subscribe(result => {
                this.dataregion = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    ngOnInit() {

        //get data Geo
        var data = '';
        this.service.httpClientGet('api/Geo', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.datageo = null;
                }
                else {
                    this.datageo = result;
                }
            },
                error => {
                    this.service.errorserver();
                });

        var data = '';
        this.id = this.route.snapshot.params['id'];
        this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
            .subscribe(result => {
                this.datadetail = result;
                this.buildForm();
            },
                error => {
                    this.service.errorserver();
                });

    }

    onSubmitUpdate() {

        this.editsubregion.controls['geo_code'].markAsTouched();
        // this.editsubregion.controls['territory_code'].markAsTouched();
        this.editsubregion.controls['region_code'].markAsTouched();
        this.editsubregion.controls['subregion_code'].markAsTouched();
        this.editsubregion.controls['subregion_name'].markAsTouched();
        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.editsubregion.value.subregion_code))
            return swal('ERROR','Special character not allowed in sub region code','error')
        if(format.test(this.editsubregion.value.subregion_name))
            return swal('ERROR','Special character not allowed in sub region name','error')
        if (this.editsubregion.valid) {
            this.loading = true;
            this.editsubregion.value.cdate = this.formatdate.dateJStoYMD(new Date());
            this.editsubregion.value.mdate = this.formatdate.dateJStoYMD(new Date());

            /* autodesk plan 10 oct - complete all history log */

            this.editsubregion.value.UserId = this.useraccesdata.UserId;

            /* end line autodesk plan 10 oct - complete all history log */

            //data form
            let data = JSON.stringify(this.editsubregion.value);

            //put action
            this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
                .subscribe(res=>{
                    console.log(res)
                });
          // this.service.httpCLientPut(this._serviceUrl,this.id, data);

            setTimeout(() => {
                //redirect
                this.router.navigate(['/admin/country/sub-region-list']);
                this.loading = false;
            }, 1000);
        }
    }

    //build form update
    buildForm(): void {

        let subregion_id_update = new FormControl(this.datadetail.subregion_id);
        let geo_code_update = new FormControl(this.datadetail.geo_code, Validators.required);
        let region_code_update = new FormControl(this.datadetail.region_code, Validators.required);
        let subregion_code_update = new FormControl(this.datadetail.subregion_code);
        let subregion_name_update = new FormControl(this.datadetail.subregion_name, Validators.required);
        let cdate = new FormControl(this.datadetail.cdate);
        let mdate = new FormControl(this.datadetail.mdate);
        let cuid = new FormControl(this.datadetail.cuid);
        let muid = new FormControl(this.datadetail.muid);

        //get data state
        var data = '';
        this.service.httpClientGet('api/Region/RegionFilter/' + this.datadetail.geo_code, data)
            .subscribe(result => {
                this.dataregion = result;
            },
                error => {
                    this.service.errorserver();
                });

        this.editsubregion = new FormGroup({
            subregion_id: subregion_id_update,
            geo_code: geo_code_update,
            region_code: region_code_update,
            subregion_code: subregion_code_update,
            subregion_name: subregion_name_update,
            cdate: cdate,
            mdate: mdate,
            cuid: cuid,
            muid: muid
        });
    }

    resetFormUpdate() {
        this.editsubregion.reset({
            'geo_code': this.datadetail.geo_code,
            'region_code': this.datadetail.region_code,
            'subregion_code': this.datadetail.subregion_code,
            'subregion_name': this.datadetail.subregion_name
        });
    }

}
