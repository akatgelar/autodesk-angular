import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubRegionUpdateEPDBComponent } from './sub-region-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const SubRegionUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: SubRegionUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.sub_region.edit_sub_region',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SubRegionUpdateEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [SubRegionUpdateEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class SubRegionUpdateEPDBModule { }
