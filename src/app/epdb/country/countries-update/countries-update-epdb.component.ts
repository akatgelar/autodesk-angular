import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { SessionService } from '../../../shared/service/session.service';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-countries-update-epdb',
    templateUrl: './countries-update-epdb.component.html',
    styleUrls: [
        './countries-update-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CountriesUpdateEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Countries';
    editcountries: FormGroup;
    public datageo: any;
    public dataterritory: any;
    public dataregion: any;
    public datasubregion: any;
    public datadetail: any;
    id: string = '';

    dropdownGeo = [];
    selectedGeo = [];
    dropdownListRegion = [];
    selectedRegion = [];
    dropdownListSubregion = [];
    selectedSubregion = [];
    dropdownSettings = {};
    public useraccesdata: any;
    public loading = false;

    constructor(private router: Router, private service: AppService,
        private formatdate: AppFormatDate, private route: ActivatedRoute, private filterGeo: AppFilterGeo, private session: SessionService, private datePipe: DatePipe) {

        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        // let geo_code_update = new FormControl('');
        // let territory_code_update = new FormControl('');
        // let region_code_update = new FormControl('');
        // let subregion_code_update = new FormControl('');
        // let countries_code_update = new FormControl({ value: '', disabled: true });
        // let countries_name_update = new FormControl('');
        // let countries_tel_update = new FormControl('');

        // this.editcountries = new FormGroup({
        //     geo_code: geo_code_update,
        //     territory_code: territory_code_update,
        //     region_code: region_code_update,
        //     subregion_code: subregion_code_update,
        //     countries_code: countries_code_update,
        //     countries_name: countries_name_update,
        //     countries_tel: countries_tel_update
        // });

        let geo_code = new FormControl('');
        let territory_code = new FormControl('');
        let region_code = new FormControl('', Validators.required);
        let subregion_code = new FormControl('', Validators.required);
        let countries_code = new FormControl('', Validators.required);
        let countries_name = new FormControl('', Validators.required);
        let countries_tel = new FormControl('', [Validators.required, CustomValidators.number]);

        this.editcountries = new FormGroup({
            geo_code: geo_code,
            territory_code: territory_code,
            region_code: region_code,
            subregion_code: subregion_code,
            countries_code: countries_code,
            countries_name: countries_name,
            countries_tel: countries_tel
        });

    }

    getGeo() {
        //get data Geo
        var data: any;
        this.service.httpClientGet('api/Geo', data)
            .subscribe(result => {
                data = result;
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedGeo = [];
                }
                else {
                    this.dropdownGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedGeo = [];
    }

    geoid = [];
    onGeoSelect(item: any) {
        // this.dropdownListRegion = [];
        // this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        // this.selectedRegion = [];
        // this.selectedSubregion = [];
        // this.dropdownListSubregion = [];
        this.geoid = [];
        this.selectedRegion = [];
        this.selectedSubregion = [];
        this.dropdownListSubregion = [];
        this.geoid.push(item.id);
        // Region
        var data = '';
        // console.log("api/Region/RegionFilter/" + this.geoid.toString(), data);
        this.service.httpClientGet("api/Region/RegionFilter/" + this.geoid.toString(), data)
            .subscribe(result => {
                // console.log(result)
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListRegion = null;
                }
                else {
                    this.dropdownListRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].region_id,
                          itemName: result[Index].region_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        // this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        this.selectedRegion = [];
        this.selectedSubregion = [];
        this.dropdownListRegion = [];
        this.dropdownListSubregion = [];
    }

    regionid = [];
    onRegionSelect(item: any) {
        // this.dropdownListSubregion = [];
        // this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubregion);
        // this.selectedSubregion = [];
        this.regionid = [];
        this.selectedSubregion = [];
        this.regionid.push(item.id);

        // Sub Region
        var data = '';
        // console.log("api/SubRegion/SubRegionFilter/" + this.regionid.toString(), data)
        this.service.httpClientGet("api/SubRegion/SubRegionFilter/" + this.regionid.toString(), data)
            .subscribe(result => {
                // console.log(result)
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubregion = null;
                }
                else {
                    this.dropdownListSubregion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_id,
                          itemName: result[Index].subregion_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                })
    }

    OnRegionDeSelect(item: any) {
        // this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubregion);
        this.selectedSubregion = [];
        this.dropdownListSubregion = [];
    }

    onSubregionSelect(item: any) { }

    OnSubregionDeSelect(item: any) { }


    setForm(data) {
        var geoTemp = {
            'id': data.geo_code,
            'itemName': data.geo_name
        };
        this.selectedGeo.push(geoTemp);
        this.onGeoSelect(this.selectedGeo[0]);

        var regionTemp = {
            'id': data.region_id,
            'itemName': data.region_name
        };
        this.selectedRegion.push(regionTemp);
        this.onRegionSelect(this.selectedRegion[0]);

        var subregionTemp = {
            'id': data.subregion_id,
            'itemName': data.subregion_name
        };
        this.selectedSubregion.push(subregionTemp);

        this.editcountries.patchValue({ countries_code: data.countries_code });
        this.editcountries.patchValue({ countries_name: data.countries_name });
        this.editcountries.patchValue({ countries_tel: data.countries_tel });
    }

    ngOnInit() {
        this.getGeo();

        this.loading = true;
        var data = '';
        this.id = this.route.snapshot.params['id'];
        this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
            .subscribe(result => {
                this.datadetail = result;
                // console.log(this.datadetail);
                this.setForm(this.datadetail);
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });

        this.dropdownSettings = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            enableCheckAll: false,
            // limitSelection: 1,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    onSubmitUpdate() {

        // this.editcountries.controls['geo_code'].markAsTouched();
        this.editcountries.controls['region_code'].markAsTouched();
        this.editcountries.controls['subregion_code'].markAsTouched();
        this.editcountries.controls['countries_code'].markAsTouched();
        this.editcountries.controls['countries_name'].markAsTouched();
        this.editcountries.controls['countries_tel'].markAsTouched();
        let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
        if(format.test(this.editcountries.value.countries_code))
            return swal('ERROR','Special character not allowed in Countries code','error')
        if(format.test(this.editcountries.value.countries_name))
            return swal('ERROR','Special character not allowed in Countries name','error')
        if(format.test(this.editcountries.value.countries_tel))
            return swal('ERROR','Special character not allowed in Countries tel','error')
var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.editcountries.valid) {

            this.loading = true;
            var temp: any;
            this.service.httpClientGet("api/Subregion/" + this.selectedSubregion[0].id, temp)
                .subscribe(res => {
                    temp = res;
                    var dataPut = {
                        'countries_code': this.editcountries.value.countries_code,
                        'countries_name': this.editcountries.value.countries_name,
                        'geo_code': temp.geo_code,
                        'region_code': temp.region_code,
                        'subregion_code': temp.subregion_code,
                        'geo_name': temp.geo_name,
                        'region_name': temp.region_name,
                        'subregion_name': temp.subregion_name,
                        'countries_tel': this.editcountries.value.countries_tel,
                        'muid': this.useraccesdata.ContactName,
                        'mdate': dateFormat,
						//this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                        'cuid': this.useraccesdata.ContactName,
                        'UserId': this.useraccesdata.UserId
                    };

                    //convert object to json
                    let data = JSON.stringify(dataPut);

                    //post action
                    this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
                        .subscribe(res=>{
                            console.log(res)
                        });
                    //this.service.httpCLientPut(this._serviceUrl,this.id, data)
                    
                    setTimeout(() => {
                        //redirect
                        this.router.navigate(['/admin/country/countries-list']);
                        this.loading = false;
                    }, 1000)
                }, error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        }

    }

    //build form update
    // buildForm(): void {

    //     let countries_id_update = new FormControl(this.datadetail.countries_id);
    //     let geo_code_update = new FormControl(this.datadetail.geo_code, Validators.required);
    //     let territory_code_update = new FormControl(this.datadetail.territory_code, Validators.required);
    //     let region_code_update = new FormControl(this.datadetail.region_code, Validators.required);
    //     let subregion_code_update = new FormControl(this.datadetail.subregion_code, Validators.required);
    //     let countries_code_update = new FormControl(this.datadetail.countries_code, Validators.required);
    //     let countries_name_update = new FormControl(this.datadetail.countries_name, Validators.required);
    //     let countries_tel_update = new FormControl(this.datadetail.countries_tel, [Validators.required, CustomValidators.number]);
    //     let cdate = new FormControl(this.datadetail.cdate);
    //     let mdate = new FormControl(this.datadetail.mdate);
    //     let cuid = new FormControl(this.datadetail.cuid);
    //     let muid = new FormControl(this.datadetail.muid);

    //     this.editcountries = new FormGroup({
    //         countries_id: countries_id_update,
    //         geo_code: geo_code_update,
    //         territory_code: territory_code_update,
    //         region_code: region_code_update,
    //         subregion_code: subregion_code_update,
    //         countries_code: countries_code_update,
    //         countries_name: countries_name_update,
    //         countries_tel: countries_tel_update,
    //         cdate: cdate,
    //         mdate: mdate,
    //         cuid: cuid,
    //         muid: muid
    //     });
    // }

    resetFormUpdate() {
        this.ngOnInit();
    }


}
