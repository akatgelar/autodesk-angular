import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesUpdateEPDBComponent } from './countries-update-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const CountriesUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: CountriesUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.countries.edit_country',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CountriesUpdateEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [CountriesUpdateEPDBComponent],
  providers: [AppService, AppFormatDate, AppFilterGeo]
})
export class CountriesUpdateEPDBModule { }
