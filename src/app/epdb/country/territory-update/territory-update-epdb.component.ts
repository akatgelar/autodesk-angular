import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-territory-update-epdb',
  templateUrl: './territory-update-epdb.component.html',
  styleUrls: [
    './territory-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class TerritoryUpdateEPDBComponent implements OnInit {

  editterritory: FormGroup;
  id: string = '';
  private _serviceUrl = 'api/Territory';
  public datadetail: any;
  private datageo;
  dropdownCountries = [];
  selectedCountry = [];
  dropdownSettings = {};
  private useraccessdata;
  private data;
  public loading = false;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate,
    private route: ActivatedRoute, private session: SessionService, private datePipe: DatePipe) {

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);

    // let geo_code_update = new FormControl({ value: '', disabled: true });
    // let territory_code_update = new FormControl({ value: '', disabled: true }, Validators.required);
    let territory_name = new FormControl('', Validators.required);
    let country_id = new FormControl('',Validators.required);

    this.editterritory = new FormGroup({
      territory_name: territory_name,
      country_id: country_id
    });

  }

  getGeo() {
    //get data Geo
    var data = '';
    this.service.httpClientGet('api/Geo', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datageo = null;
        }
        else {
          this.datageo = result;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  getCountries(value) {
    var data: any;
    this.service.httpClientGet("api/Countries/NotOnTerritoryWithId/"+value, data)
      .subscribe(res => {
        data = res;
        this.dropdownCountries = data.map((item) => {
          return {
            id: item.countries_id,
            itemName: item.countries_name
          }
        })
        this.loading = false;
      }, error => {
        this.service.errorserver();
        this.loading = false;
      });
    // this.selectedCountry = [];
  }

  getDataTerritory(id) {
    var data: any;
    this.service.httpClientGet("api/Territory/" + id, data)
      .subscribe(res => {
        data = res;
        if (data.length >= 1) {
          this.editterritory.patchValue({ territory_name: data[0].Territory_Name });
          if (data[0].countries_id != "") {
            for (let k = 0; k < data.length; k++) {
              var country_temp = {
                'id': data[k].countries_id,
                'itemName': data[k].countries_name
              };
              this.selectedCountry.push(country_temp);
            }
          } else {
            this.selectedCountry = [];
          }
        }
      }, error => {
        this.service.errorserver();
        this.loading = false;
      });
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.loading = true;
    setTimeout(() => {
        this.getDataTerritory(this.id);
        this.getCountries(this.id);
      }, 2000)
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };
  }

  onCountrySelect(item: any) { }

  OnCountryDeSelect(item: any) { }

  getAllTerritory() {
    //get data Region
    var data = '';
    this.service.httpClientGet(this._serviceUrl, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.data = result;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  onSubmitUpdate() {
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.editterritory.value.territory_name))
      return swal('ERROR','Special character not allowed in territory name','error')
    this.editterritory.controls['territory_name'].markAsTouched();
    if(this.selectedCountry.length != 0){
      this.editterritory.removeControl('country_id');
    } else{
      this.editterritory.controls['country_id'].markAsTouched();
    }

    if (this.editterritory.valid) {
      this.loading = true;
      var countries_temp = [];
      for (let j = 0; j < this.selectedCountry.length; j++) {
        countries_temp.push(parseInt(this.selectedCountry[j].id));
      }

      var dataUpdate = {
        'Territory_Name': this.editterritory.value.territory_name,
        'countries_id': countries_temp.toString(),
        // 'mdate': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
        'mdate': this.service.ISO_date(new Date()),
        'muid': this.useraccessdata.ContactName,
        'UserId': this.useraccessdata.UserId
      };

      //conver to json
      let data = JSON.stringify(dataUpdate);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
      .subscribe(res=>{
          console.log(res)
      });
   //this.service.httpCLientPut(this._serviceUrl,this.id, data);

      setTimeout(() => {
        this.getAllTerritory();
        //redirect
        this.router.navigate(['/admin/country/territory-list']);
        this.loading = false;
      }, 1000)
    }
  }


  resetFormUpdate() {
    this.getDataTerritory(this.id);
  }

}
