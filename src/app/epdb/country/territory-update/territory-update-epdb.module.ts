import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerritoryUpdateEPDBComponent } from './territory-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const TerritoryUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: TerritoryUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.country.territory.edit_territory',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TerritoryUpdateEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [TerritoryUpdateEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class TerritoryUpdateEPDBModule { }
