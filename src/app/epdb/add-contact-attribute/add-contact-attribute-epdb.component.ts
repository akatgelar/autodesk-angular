import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { DatePipe } from '@angular/common';

const now = new Date();

@Component({
    selector: 'app-contact-site-attribute-epdb',
    templateUrl: './add-contact-attribute-epdb.component.html',
    styleUrls: [
        './add-contact-attribute-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddContactAttributeEPDBComponent implements OnInit {
    private SiteId;
    private ContactId;
    public journal;
    private activityDate;
    private useraccessdata;
    addContactJournal: FormGroup;
    modelPopup1: NgbDateStruct;
    public loading = false;

    modelPopup: NgbDateStruct;

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate,
      private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {//, private htmlEntityService: htmlentityService
        
        this.modelPopup = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        let ActivityId = new FormControl('', Validators.required);
        let Notes = new FormControl('', Validators.required);
        let ActivityDate = new FormControl('', Validators.required);

        this.addContactJournal = new FormGroup({
            ActivityId: ActivityId,
            Notes: Notes,
            ActivityDate: ActivityDate
        });
    }

    getJournalActivities(ContactId) {
        var data: any;
        var activityType = "Contact Attribute";
        this.service.httpClientGet("api/MainSite/JournalActivities/" + ContactId + "/" + activityType, data)
            .subscribe(res => {
                data = res;
                data.length == 0 ? this.journal = [] : this.journal = data;
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.activityDate = this.parserFormatter.format(date);
        } else {
            this.activityDate = "";
        }
    }

    submitid:string=''; /* 01102018 */
    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || '';
            this.ContactId = params['ContactId'] || 0;
        });
        this.getJournalActivities(this.SiteId);

        /* get contactid - 01102018 */

        this.service.httpClientGet("api/ContactAll/ContactIdInt/" + this.ContactId, '')
            .subscribe(res => {
                let data:any = res;
                this.submitid = data.ContactId;
            }, error => {
                this.service.errorserver();
            });

        /* end line get contactid - 01102018 */
    }

    onSubmit() {

        this.addContactJournal.controls["ActivityId"].markAsTouched();
        this.addContactJournal.controls["ActivityDate"].markAsTouched();
        this.addContactJournal.controls["Notes"].markAsTouched();
		var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.addContactJournal.valid) {
            this.loading = true;
            var dataJournal = {
                // 'ParentId': this.ContactId, 
                'ParentId': this.submitid, /* 01102018 */
                 'DateAdded': dateFormat,
				 //this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                'AddedBy': this.useraccessdata.ContactName,
                'ActivityId': this.addContactJournal.value.ActivityId,
                'ActivityDate': this.formatdate.dateCalendarToYMD(this.addContactJournal.value.ActivityDate),
                'Notes': this.addContactJournal.value.Notes,//this.htmlEntityService.encoder(this.addContactJournal.value.Notes),
                'History':'Contact Attribute',
                'cuid':this.useraccessdata.ContactName,
                'UserId':this.useraccessdata.UserId
            };
            this.service.httpClientPost("api/OrganizationJournalEntries", dataJournal)
			.subscribe(res => {
				console.log(res);
			});
            setTimeout(() => {
                this.router.navigate(['manage/contact/detail-contact/', this.ContactId, this.SiteId]);
                this.loading = false;
            }, 1000)
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/contact/detail-contact/', this.ContactId, this.SiteId]);
    }

    resetForm() {
        this.addContactJournal.reset({
            'ActivityId': '',
            'ActivityDate': '',
            'Notes': ''
        });
    }
}
