import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactAttributeEPDBComponent } from './add-contact-attribute-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddContactAttributeEPDBRoutes: Routes = [
    {
        path: '',
        component: AddContactAttributeEPDBComponent,
        data: {
            breadcrumb: 'Add Contact Attribute',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddContactAttributeEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [AddContactAttributeEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class AddContactAttributeEPDBModule { }
