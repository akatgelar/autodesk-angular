import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionalMappingsReportEPDBComponent } from './regional-mappings-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const RegionalMappingsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: RegionalMappingsReportEPDBComponent,
    data: {
      breadcrumb: 'Regional Mappings Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RegionalMappingsReportEPDBRoutes),
    SharedModule
  ],
  declarations: [RegionalMappingsReportEPDBComponent]
})
export class RegionalMappingsReportEPDBModule { }
