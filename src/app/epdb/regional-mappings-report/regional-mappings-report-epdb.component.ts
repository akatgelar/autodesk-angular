import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-regional-mappings-report-epdb',
  templateUrl: './regional-mappings-report-epdb.component.html',
  styleUrls: [
    './regional-mappings-report-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class RegionalMappingsReportEPDBComponent implements OnInit {

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
   
 
  constructor(public http: Http) { }

  ngOnInit() {
    this.http.get(`assets/data/activities.json`)
      .subscribe((data)=> {
        this.data = data.json();
      });
  }

  //cange table based on combobox
  showall : boolean;
  showemail : boolean;
  showcountries : boolean;
  showstateprovpref : boolean;

  changetable(newvalue){
    if(newvalue == "Show Administrator Email"){
        this.showemail = true;
        this.showcountries = false;
        this.showstateprovpref = false;
    }
    else if(newvalue == "Show Countries"){
        this.showcountries = true;
        this.showemail = false;
        this.showstateprovpref = false;
    }
    else if(newvalue == "Show States / Provinces / Prefectures"){
        this.showstateprovpref = true;
        this.showemail = false;
        this.showcountries = false;
    }
    else{
        this.showemail = true;
        this.showcountries = true;
        this.showstateprovpref = true;
    }
  }

}