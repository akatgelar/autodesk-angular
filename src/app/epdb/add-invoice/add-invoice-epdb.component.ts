import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterInvoice' })
export class DataFilterInvoicePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'dataFilterSKU' })
export class DataFilterSKUPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-add-invoice-epdb',
  templateUrl: './add-invoice-epdb.component.html',
  styleUrls: [
    './add-invoice-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddInvoiceEPDBComponent implements OnInit {

  havePrimarySite = true;
  loading = false;
  orgName = ''
  orgPartnerType = ''
  types = [];
  checkedSku = [];
  currentSites = [];
  territories = [];
  countries = [];
  sites = [];
  fys = [];
  skus = {};
  filter = {
    ter:0,
    coun:''
  }
  selectedTerritory:any;

  checkDiscount = false;
  checkProRata = false;
  addInvoice:any = {
    OrgId: '',
    InvoiceNumber: '',
    InvoiceDescription: '',
    InvoiceDate: '',
    Status: 'Pending',
    FinancialYear: '',
    PricingSKUs: [],
    Discounts: {
      Discount:{
        Name: 'Discount',
        Amount: '',
        AmountType: 'Exactly',
        InvoiceId: ''
      },
      ProRata: {
        Name: 'Pro-Rata',
        Amount: '',
        AmountType: '',
        InvoiceId: ''
      },
    }
  }
  
  constructor(private service:AppService, private router: ActivatedRoute,private cd:ChangeDetectorRef){}
  
  ngOnInit(){
    this.router.params.subscribe((params)=>{
      this.addInvoice.OrgId = params['id']
      this.loadData()
    })
  }
  
  goBack(){
    window.history.back()
  }
  
  validProrataTime(val){
    if(val.month <8 && val.month>1){
      this.checkProRata = false
    }
    return (val.month >=8 || val.month==1)
  }

  loadData(){
    let orgId = this.addInvoice.OrgId
    this.service.httpClientGet('api/MainInvoices/PopulateTerritoryAndCountry?OrganizationId='+orgId,{}).subscribe((res:any)=>{
      if(res){
        this.filter.ter = res.territoryId
        this.filter.coun = res.countriesCode
        this.getSites()
        this.service.httpClientGet('api/MainInvoices/GetListInvoiceCountry?territoryId='+this.filter.ter+'&orgId='+orgId,{}).subscribe((res:any)=>{
          this.countries = res
        })
      }
      this.service.httpClientGet('api/MainInvoices/GetListTerritoryById?Tid='+this.filter.ter,{}).subscribe((res:any)=>{
        this.territories = [res]
      })
    })
    
    this.service.httpClientGet('api/MainInvoices/GetOrganizationName?orgId='+orgId,{}).subscribe((res:any)=>{
      this.orgName = this.service.decoder(res.orgName)
      this.orgPartnerType = res.partnerType
    })

    this.service.httpClientGet('api/MainInvoices/GetListFY',{}).subscribe((res:any)=>{
      this.fys = res
    })

    this.service.httpClientGet('api/MainInvoices/GetPartnerTypes',{}).subscribe((res:any)=>{
      this.types = res
    })

  }

  getSites(){
    let orgId = this.addInvoice.OrgId
    this.service.httpClientGet('api/MainInvoices/GetListSite?orgId='+orgId+'&countrycode='+this.filter.coun,{}).subscribe((res:any)=>{
      this.sites = res?res:[]
      let havePrime = false
      this.sites.map((el)=>{
        if(el.isPrimary)
          havePrime = true
        if(!this.skus[el.siteId])  
          this.skus[el.siteId] = []
      })
      this.havePrimarySite = havePrime
      this.getSkus()
    })
  }

  selectSite(siteId){
    if(this.currentSites.indexOf(siteId)>-1)
      this.currentSites = this.currentSites.filter((el)=>el!=siteId)
    else
      this.currentSites.push(siteId)
  }

  getSkus(){
    let orgId = this.addInvoice.OrgId
    this.service.httpClientGet('api/MainInvoices/GetListPricingSKU?orgId='+orgId+'&countrycode='+this.filter.coun,{}).subscribe((res:any)=>{
      this.sites.map((el)=>{
        let new_data = [...res].map((e)=>{
          let data = {...e}
          this.skus[el.siteId].map((d)=>{
            if(d.id==e.id)
              data = {...d}
          })
          if(el.partnerType.indexOf('ATC')>-1 && el.partnerType.indexOf('AAP')>-1 && data.distributor == 'AAP')
            data = null
          return data
        })
        console.log(new_data)
        this.skus[el.siteId] = new_data.filter((el)=>el !=null)
      })
    })
  }

  updateSku(event,id,siteId){
    console.log(siteId,id)
    this.skus[siteId].map((el)=>{
      if(el.id==id)
        el.qty = event.target.value
      return el
    })
    console.log(this.skus)
  }

  getTotal(value){
    let total = 0
    let discount = this.checkDiscount?Number(this.addInvoice.Discounts.Discount.Amount):0
    let prorata = this.checkProRata?Number(this.addInvoice.Discounts.ProRata.Amount):0
    this.sites.map((e)=>{
      let skus = this.skus[e.siteId].filter((el)=> el.siteId)
      if(value != 'TOTAL_PRICE' && value != 'TOTAL_QTY')
        skus.map((el)=>{
          if(el.distributor==value)
            total = el.qty*Number(el.license) + total
        })
      if(value == 'TOTAL_PRICE' || value == 'TOTAL_DISCOUNT'){
        skus.map((el)=>{
          total = el.qty*el.price + total 
        })
      }
      if(value == 'TOTAL_QTY')
        skus.map((el)=>{
          total = Number(el.qty)+ total 
        })
    })
    if(value == 'TOTAL_PRICE'){
      //if(this.addInvoice.InvoiceDate.month >=8 || this.addInvoice.InvoiceDate.month == 1){
        total = total - prorata*total/100
        if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
          total = total - discount
        if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
          total = total - total*discount/100
      //}else{
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
      //    total = total - discount
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
      //    total = total - total*discount/100
      //}
    }
    if(value == 'TOTAL_DISCOUNT'){
      //if(this.addInvoice.InvoiceDate.month >=8 || this.addInvoice.InvoiceDate.month == 1){
        let dis = prorata*total/100
        if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
          dis = dis + discount
        if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
          dis = dis + total*discount/100
        total = dis
      //}else{
      //  let dis = 0
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Exactly')
      //    dis = dis + discount
      //  if(this.addInvoice.Discounts.Discount.AmountType == 'Percentage')
      //    dis = dis + total*discount/100
      //  total = dis
      //}
    }
    if(total<0)
      total = 0
    if(value != 'TOTAL_PRICE' && value !='TOTAL_DISCOUNT')
      return total
    return total.toFixed(2)
  }

  resetForm(){
    this.addInvoice = {...this.addInvoice,
      InvoiceNumber: '',
      InvoiceDescription: '',
      InvoiceDate: '',
      Status: 'Pending',
      FinancialYear: '',
      PricingSKUs: [],
      Discounts: {
        Discount:{
          Name: '',
          Amount: '',
          AmountType: '',
          InvoiceId: ''
        },
        ProRata: {
          Name: '',
          Amount: '',
          AmountType: '',
          InvoiceId: ''
        },
      }
    }
    this.checkDiscount = false
    this.checkProRata = false
    this.sites.map((el)=>{
      this.skus[el.siteId] = this.skus[el.siteId].map((e)=>{
        return {...e,siteId:'', qty:0}
      })
    })
  }

  validSku(){
    let valid = true
    this.sites.map((el)=>{
      if(el.isPrimary)
        this.skus[el.siteId].map((sku)=>{
          if(sku.isForPrimarySite && sku.qty && sku.qty!=1)
            valid = false
        })
    })
    return valid
  }

  onSubmit(){
    if(!this.addInvoice.FinancialYear)
      return swal('ERROR','Please Select Financial Year','error')
    this.loading = true
    this.service.httpClientGet('api/MainInvoices/CheckInvoiceMainSKU?orgId='+this.addInvoice.OrgId+'&financialYear='+this.addInvoice.FinancialYear,{}).subscribe((res1: any)=>{
      let listSku = []
      let useMainSku = false
      let mainSkuUsed = res1.data.length?true:false
      this.sites.map((el)=>{
        listSku = listSku.concat(this.skus[el.siteId].filter((e)=> e.siteId))
        if(el.isPrimary)
          this.skus[el.siteId].map((e)=>{
            if(e.isForPrimarySite && e.siteId)
              useMainSku = true
          })
      })
      let invalid_qty = false
      listSku.map((el)=>{
        if(el.qty==0 || Number.isInteger(el.qty))
          invalid_qty = true
      })
      if(invalid_qty){
        this.loading = false
        return swal('ERROR','Please enter valid qty for selected sku','error')
      }
      if(useMainSku && mainSkuUsed){
        this.loading = false
        return swal('ERROR','The Main Site SKU has already been selected in this Fiscal Year. It can only be selected once per Fiscal Year. Please select another SKU.','error')
      }
      if(!listSku.length){
        this.loading = false
        return swal('ERROR','Please select at least 1 SKU','error')
      }
      if(this.checkDiscount && (!this.addInvoice.Discounts.Discount.Amount || !this.addInvoice.Discounts.Discount.AmountType)){
        this.loading = false
        return swal('ERROR','Please enter discount','error')
      }
      if(this.checkProRata && !this.addInvoice.Discounts.ProRata.Amount){
        this.loading = false
        return swal('ERROR','Please enter prorata','error')
      }
      //if(this.addInvoice.Status == 'Paid' && !this.addInvoice.InvoiceDate){
		  //this.loading = false
      //  return swal('ERROR','Please enter payment date','error')
	    //}
      if(!this.validSku()){
        this.loading = false
        return swal('ERROR','Please enter valid qty for main site','error')
      }
      let data = {...this.addInvoice}
      data.PricingSKUs = listSku
      if(this.addInvoice.Status == 'Paid')
        data.InvoiceDate = data.InvoiceDate.year+'/'+data.InvoiceDate.month+'/'+data.InvoiceDate.day
      else
        data.InvoiceDate = ''
      this.service.httpClientPost('api/MainInvoices/AddInvoice',data).subscribe((res:any)=>{
        this.loading= false
        if (res['code'] == '1') {
          swal({
            type: 'success',
            title: 'Success',
            text: 'Save invoice success',
            allowOutsideClick:false
          }).then(()=>{
            window.history.back();
        })
      }else
      {
        swal('ERROR',res.message,'error')
      }
      
       // let res_json = JSON.parse(res)
       // if(res_json.code == 0)
       //   swal('ERROR',res_json.message,'error')
       // else
        //  swal('SUCCESS','Save invoice success','success').then(()=>{
        //    window.history.back();
        //  })
    //  },(err)=>{
     //   swal('ERROR','Save invoice faild','error')
     })
    })
  }
  
  statusChange(){
    if(this.addInvoice.Status !='Paid')
      this.addInvoice.InvoiceDate = ''
  }

  checker(value,option){
    console.log(option,value)
    if(option == 'Discount'){
      this.checkDiscount = value
      this.checkProRata = false
      this.addInvoice.Discounts.Discount.Amount=''
    }
    if(option == 'ProRata'){
      this.checkProRata = value
      this.checkDiscount = false
      if(value)
        this.addInvoice.Discounts.ProRata.Amount= 50
      else
        this.addInvoice.Discounts.ProRata.Amount=''
    }
    console.log('checkDiscount',this.checkDiscount)
    console.log('checkProRata',this.checkProRata)
  }
}
