import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddInvoiceEPDBComponent } from './add-invoice-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";

import { DataFilterInvoicePipe, DataFilterSKUPipe } from './add-invoice-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { CurrencyMaskModule } from "ng2-currency-mask";

export const AddInvoiceEPDBRoutes: Routes = [
  {
    path: '',
    component: AddInvoiceEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.add_invoice.add_invoice',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddInvoiceEPDBRoutes),
    SharedModule,
    LoadingModule,
    CurrencyMaskModule,
    AngularMultiSelectModule
  ],
  declarations: [AddInvoiceEPDBComponent, DataFilterInvoicePipe, DataFilterSKUPipe],
  providers: [AppService, AppFormatDate],
})
export class AddInvoiceEPDBModule { }
