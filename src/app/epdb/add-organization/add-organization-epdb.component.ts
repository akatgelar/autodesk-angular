import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '../../shared/service/session.service';
import { ViewChild } from "../../../../node_modules/@angular/core";
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

@Component({
  selector: 'app-add-organization-epdb',
  templateUrl: './add-organization-epdb.component.html',
  styleUrls: [
    './add-organization-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddOrganizationEPDBComponent implements OnInit {

  @ViewChild('fileUploader') fileUploader

  private _serviceUrl = 'api/MainOrganization';
  messageError: string = '';
  addorganization: FormGroup;
  public datacountry: any;
  public datastate1: any;
  public datastate2: any;
  public datastate3: any;
  public useraccesdata: any;
  public loading = false;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private http: HttpClient, private session: SessionService) {//, private htmlEntityService: htmlentityService
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let OrgName = new FormControl('', Validators.required);
    let EnglishOrgName = new FormControl('');
    let CommercialOrgName = new FormControl('');
    let ATCCSN = new FormControl('');
    let VARCSN = new FormControl('');
    let AUPCSN = new FormControl('');
    let TaxExempt = new FormControl('');
    let VATNumber = new FormControl('');
    let YearJoined = new FormControl('');
    let OrgWebAddress = new FormControl('');
    let RegisteredDepartment = new FormControl('');
    let RegisteredAddress1 = new FormControl('', Validators.required);
    let RegisteredAddress2 = new FormControl('');
    let RegisteredAddress3 = new FormControl('');
    let RegisteredCity = new FormControl('', Validators.required);
    let RegisteredStateProvince = new FormControl('');
    let RegisteredPostalCode = new FormControl('');
    let RegisteredCountryCode = new FormControl('', Validators.required);
    let InvoicingDepartment = new FormControl('');
    let InvoicingAddress1 = new FormControl('', Validators.required);
    let InvoicingAddress2 = new FormControl('');
    let InvoicingAddress3 = new FormControl('');
    let InvoicingCity = new FormControl('', Validators.required);
    let InvoicingStateProvince = new FormControl('');
    let InvoicingPostalCode = new FormControl('');
    let InvoicingCountryCode = new FormControl('', Validators.required);
    let ContractDepartment = new FormControl('');
    let ContractAddress1 = new FormControl('', Validators.required);
    let ContractAddress2 = new FormControl('');
    let ContractAddress3 = new FormControl('');
    let ContractCity = new FormControl('', Validators.required);
    let ContractStateProvince = new FormControl('');
    let ContractPostalCode = new FormControl('');
    let ContractCountryCode = new FormControl('', Validators.required);
    let ATCDirectorFirstName = new FormControl('', Validators.required);
    let ATCDirectorLastName = new FormControl('', Validators.required);
    let ATCDirectorEmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let ATCDirectorTelephone = new FormControl('', [Validators.required]);
    let ATCDirectorFax = new FormControl('');
    let LegalContactFirstName = new FormControl('', Validators.required);
    let LegalContactLastName = new FormControl('', Validators.required);
    let LegalContactEmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let LegalContactTelephone = new FormControl('', [Validators.required]);
    let LegalContactFax = new FormControl('');
    let BillingContactFirstName = new FormControl('', Validators.required);
    let BillingContactLastName = new FormControl('', Validators.required);
    let BillingContactEmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let BillingContactTelephone = new FormControl('', [Validators.required]);
    let BillingContactFax = new FormControl('');
    let AdminNotes = new FormControl('');
    let OrgId = new FormControl();
    let ATCOrgId = new FormControl();
    let OrgStatus_retired = new FormControl();
    let SAPSoldTo = new FormControl();
    let Status = new FormControl();
    let DateAdded = new FormControl();
    let AddedBy = new FormControl();
    let DateLastAdmin = new FormControl();
    let LastAdminBy = new FormControl();
    let CSOResellerUUID = new FormControl();
    let CSOResellerName = new FormControl();
    let CSOPartnerManager = new FormControl();
    let CSOUpdatedOn = new FormControl();
    let CSOVersion = new FormControl();
    let Files = new FormControl();
    let orgLogo = new FormControl();

    this.addorganization = new FormGroup({
      OrgId: OrgId,
      ATCOrgId: ATCOrgId,
      OrgStatus_retired: OrgStatus_retired,
      OrgName: OrgName,
      EnglishOrgName: EnglishOrgName,
      CommercialOrgName: CommercialOrgName,
      YearJoined: YearJoined,
      TaxExempt: TaxExempt,
      VATNumber: VATNumber,
      SAPSoldTo: SAPSoldTo,
      VARCSN: VARCSN,
      ATCCSN: ATCCSN,
      AUPCSN: AUPCSN,
      OrgWebAddress: OrgWebAddress,
      RegisteredDepartment: RegisteredDepartment,
      RegisteredAddress1: RegisteredAddress1,
      RegisteredAddress2: RegisteredAddress2,
      RegisteredAddress3: RegisteredAddress3,
      RegisteredCity: RegisteredCity,
      RegisteredStateProvince: RegisteredStateProvince,
      RegisteredPostalCode: RegisteredPostalCode,
      RegisteredCountryCode: RegisteredCountryCode,
      InvoicingDepartment: InvoicingDepartment,
      InvoicingAddress1: InvoicingAddress1,
      InvoicingAddress2: InvoicingAddress2,
      InvoicingAddress3: InvoicingAddress3,
      InvoicingCity: InvoicingCity,
      InvoicingStateProvince: InvoicingStateProvince,
      InvoicingPostalCode: InvoicingPostalCode,
      InvoicingCountryCode: InvoicingCountryCode,
      ContractDepartment: ContractDepartment,
      ContractAddress1: ContractAddress1,
      ContractAddress2: ContractAddress2,
      ContractAddress3: ContractAddress3,
      ContractCity: ContractCity,
      ContractStateProvince: ContractStateProvince,
      ContractPostalCode: ContractPostalCode,
      ContractCountryCode: ContractCountryCode,
      ATCDirectorFirstName: ATCDirectorFirstName,
      ATCDirectorLastName: ATCDirectorLastName,
      ATCDirectorEmailAddress: ATCDirectorEmailAddress,
      ATCDirectorTelephone: ATCDirectorTelephone,
      ATCDirectorFax: ATCDirectorFax,
      LegalContactFirstName: LegalContactFirstName,
      LegalContactLastName: LegalContactLastName,
      LegalContactEmailAddress: LegalContactEmailAddress,
      LegalContactTelephone: LegalContactTelephone,
      LegalContactFax: LegalContactFax,
      BillingContactFirstName: BillingContactFirstName,
      BillingContactLastName: BillingContactLastName,
      BillingContactEmailAddress: BillingContactEmailAddress,
      BillingContactTelephone: BillingContactTelephone,
      BillingContactFax: BillingContactFax,
      AdminNotes: AdminNotes,
      Status: Status,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy,
      CSOResellerUUID: CSOResellerUUID,
      CSOResellerName: CSOResellerName,
      CSOPartnerManager: CSOPartnerManager,
      CSOUpdatedOn: CSOUpdatedOn,
      CSOVersion: CSOVersion,
      Files: Files,
      orgLogo: orgLogo
    });

  }

  public datasession: any;
  ngOnInit() {

    //get data country
    var data = '';
    this.service.httpClientGet('api/Countries', data)
      .subscribe(result => {
        this.datacountry = result;
      },
        error => {
          this.service.errorserver();
        });

  }

  getState1(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/States/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate1 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getState2(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/States/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate2 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getState3(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/States/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate3 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  //submit form
  onSubmit() {
    this.addorganization.controls['OrgName'].markAsTouched();
    this.addorganization.controls['RegisteredAddress1'].markAsTouched();
    this.addorganization.controls['RegisteredCity'].markAsTouched();
    this.addorganization.controls['RegisteredCountryCode'].markAsTouched();
    this.addorganization.controls['InvoicingAddress1'].markAsTouched();
    this.addorganization.controls['InvoicingCity'].markAsTouched();
    this.addorganization.controls['InvoicingCountryCode'].markAsTouched();
    this.addorganization.controls['ContractAddress1'].markAsTouched();
    this.addorganization.controls['ContractAddress2'].markAsTouched();
    this.addorganization.controls['ContractCity'].markAsTouched();
    this.addorganization.controls['ContractCountryCode'].markAsTouched();
    this.addorganization.controls['ATCDirectorFirstName'].markAsTouched();
    this.addorganization.controls['ATCDirectorLastName'].markAsTouched();
    this.addorganization.controls['ATCDirectorEmailAddress'].markAsTouched();
    this.addorganization.controls['ATCDirectorTelephone'].markAsTouched();
    this.addorganization.controls['LegalContactFirstName'].markAsTouched();
    this.addorganization.controls['LegalContactLastName'].markAsTouched();
    this.addorganization.controls['LegalContactEmailAddress'].markAsTouched();
    this.addorganization.controls['LegalContactTelephone'].markAsTouched();
    this.addorganization.controls['BillingContactFirstName'].markAsTouched();
    this.addorganization.controls['BillingContactLastName'].markAsTouched();
    this.addorganization.controls['BillingContactEmailAddress'].markAsTouched();
    this.addorganization.controls['BillingContactTelephone'].markAsTouched();

    // let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    // let keys = Object.keys(this.addorganization.controls)
    // for(let i =0;i<keys.length;i++){
    //   if(format.test(this.addorganization.value[keys[i]]) && keys[i]!='Files')
    //     return swal('ERROR','Special character not allowed in '+keys[i],'error')
    // }
    console.log('end')
    if (this.addorganization.valid && this.validname) {
      let fileData = []
      this.fileUploader.files.map((file)=>{
        let reader = new FileReader()
        let name = file.name
        reader.onload = ()=>{
          fileData.push({ name:name, data:reader.result })
          if(this.fileUploader.files.length == fileData.length)
            this.addorganization.patchValue({
              Files: fileData
            })
        }
        reader.readAsDataURL(file);
      })
      swal({
        title: 'Are you sure?',
        text: "If you wish to add new organization",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then(result => {
        if (result == true) {
          this.loading = true;

          //For Audit
          this.addorganization.value.cuid = this.useraccesdata.ContactName;
          this.addorganization.value.UserId = this.useraccesdata.UserId;
          //End For Audit

          this.addorganization.value.AddedBy = this.useraccesdata.ContactName;
          this.addorganization.value.OrgName = this.service.encoder(this.addorganization.value.OrgName);
          this.addorganization.value.EnglishOrgName = this.service.encoder(this.addorganization.value.EnglishOrgName);
          this.addorganization.value.CommercialOrgName = this.service.encoder(this.addorganization.value.CommercialOrgName);
          this.addorganization.value.RegisteredDepartment = this.service.encoder(this.addorganization.value.RegisteredDepartment);
          this.addorganization.value.RegisteredAddress1 = this.service.encoder(this.addorganization.value.RegisteredAddress1);
          this.addorganization.value.RegisteredAddress2 = this.service.encoder(this.addorganization.value.RegisteredAddress2);
          this.addorganization.value.RegisteredAddress3 = this.service.encoder(this.addorganization.value.RegisteredAddress3);
          this.addorganization.value.RegisteredCity = this.service.encoder(this.addorganization.value.RegisteredCity);
          this.addorganization.value.InvoicingDepartment = this.service.encoder(this.addorganization.value.InvoicingDepartment);
          this.addorganization.value.InvoicingAddress1 = this.service.encoder(this.addorganization.value.InvoicingAddress1);
          this.addorganization.value.InvoicingAddress2 = this.service.encoder(this.addorganization.value.InvoicingAddress2);
          this.addorganization.value.InvoicingAddress3 = this.service.encoder(this.addorganization.value.InvoicingAddress3);
          this.addorganization.value.InvoicingCity = this.service.encoder(this.addorganization.value.InvoicingCity);
          this.addorganization.value.ContractDepartment = this.service.encoder(this.addorganization.value.ContractDepartment);
          this.addorganization.value.ContractAddress1 = this.service.encoder(this.addorganization.value.ContractAddress1);
          this.addorganization.value.ContractAddress2 = this.service.encoder(this.addorganization.value.ContractAddress2);
          this.addorganization.value.ContractAddress3 = this.service.encoder(this.addorganization.value.ContractAddress3);
          this.addorganization.value.ContractCity = this.service.encoder(this.addorganization.value.ContractCity);
          this.addorganization.value.ATCDirectorFirstName = this.service.encoder(this.addorganization.value.ATCDirectorFirstName);
          this.addorganization.value.ATCDirectorLastName = this.service.encoder(this.addorganization.value.ATCDirectorLastName);
          this.addorganization.value.LegalContactFirstName = this.service.encoder(this.addorganization.value.LegalContactFirstName);
          this.addorganization.value.LegalContactLastName = this.service.encoder(this.addorganization.value.LegalContactLastName);
          this.addorganization.value.BillingContactFirstName = this.service.encoder(this.addorganization.value.BillingContactFirstName);
          this.addorganization.value.BillingContactLastName = this.service.encoder(this.addorganization.value.BillingContactLastName);
          this.addorganization.value.AdminNotes = this.service.encoder(this.addorganization.value.AdminNotes);

          //convert object to json
          let data = JSON.stringify(this.addorganization.value);

          let finaldata = data.replace(/\'/g, "\\\\'");
          finaldata.replace(/"/g, '\"');

          //kalo ada error action
          var orgidtmp = "";

          /* autodesk plan 10 oct */

          //post action

          this.service.httpClientPost(this._serviceUrl, data).subscribe(res => {
            var result :any =res;
            if (result['code'] == '1') {
              orgidtmp = result['organizationid'];
              this.router.navigate(['/manage/organization/detail-organization', result['organizationid']]);
              this.service.httpClientGet("api/Auth/EmailNotificationNewOrg", '').subscribe(result => { console.log(result); })
              this.loading = false;
            }
            else {
              swal({
                title: 'Information!',
                text: result['message'],
                type: 'error'
              }).catch(swal.noop);
              this.loading = false;
            }
          },
          error => {
            if (orgidtmp != "") {
              this.router.navigate(['/manage/organization/detail-organization', result['organizationid']]);
              swal({
                title: 'Information!',
                text: 'Email failed to send',
                type: 'error',
                showConfirmButton: false,
                timer: 3000
              })
            } else {
              console.log("failed");
            }
            this.loading = false;
          });

          /* end line autodesk plan 10 oct */

        }
      }).catch((err)=>{
        console.log(err)
      });
    }
    else {
      this.loading = false;
      if(this.fileUploader.valid())
        swal(
          'Field is Required!',
          'Please enter the Add Organization form required!',
          'error'
        )
      else
        swal(
          'File is Required!',
          'Please add file for Organization',
          'error'
        )
    }
  }

  webaddress: string = "";
  getwebaddress(value) {
    this.webaddress = value;
  }

  testwebaddress() {
    if (this.webaddress != "") {
      window.open("//" + this.webaddress);
    } else {
      alert("insert web address");
    }
  }

  //check name exist
  validname: Boolean = true;
  public namedata: any;
  checkNameDuplicate(value) {
    let finalvalue = this.service.encoder(value);//value.replace(/\'/g, "")
    if (finalvalue != '' && finalvalue != null) {
      this.service.httpClientGet("api/MainOrganization/orgname/" + finalvalue, '')
        .subscribe(result => {
          this.namedata = result;
          if (this.namedata.OrgName != null) {
            this.validname = false;
          }
          else if (this.namedata.OrgName == null) {
            this.validname = true;
          }
        },
          error => {
            this.service.errorserver();
          });
    }
    else {
      this.validname = true;
    }
  }

  ContractDepartment = "";
  ContractAddress1 = "";
  ContractAddress2 = "";
  ContractAddress3 = "";
  ContractCity = "";
  ContractCountryCode = "";
  ContractStateProvince = "";
  ContractPostalCode = "";
  getRegisteredContract() {
    this.ContractDepartment = this.addorganization.value.RegisteredDepartment;
    this.ContractAddress1 = this.addorganization.value.RegisteredAddress1;
    this.ContractAddress2 = this.addorganization.value.RegisteredAddress2;
    this.ContractAddress3 = this.addorganization.value.RegisteredAddress3;
    this.ContractCity = this.addorganization.value.RegisteredCity;
    this.addorganization.patchValue({ ContractCountryCode: this.addorganization.value.RegisteredCountryCode });
    this.getState2(this.addorganization.value.RegisteredCountryCode);
    this.addorganization.patchValue({ ContractStateProvince: this.addorganization.value.RegisteredStateProvince });
    this.ContractPostalCode = this.addorganization.value.RegisteredPostalCode;
    console.log(this.addorganization.value.RegisteredStateProvince);
  }

  InvoicingDepartment = "";
  InvoicingAddress1 = "";
  InvoicingAddress2 = "";
  InvoicingAddress3 = "";
  InvoicingCity = "";
  InvoicingCountryCode = "";
  InvoicingStateProvince = "";
  InvoicingPostalCode = "";
  getRegisteredInvoice() {
    this.InvoicingDepartment = this.addorganization.value.RegisteredDepartment;
    this.InvoicingAddress1 = this.addorganization.value.RegisteredAddress1;
    this.InvoicingAddress2 = this.addorganization.value.RegisteredAddress2;
    this.InvoicingAddress3 = this.addorganization.value.RegisteredAddress3;
    this.InvoicingCity = this.addorganization.value.RegisteredCity;
    this.addorganization.patchValue({ InvoicingCountryCode: this.addorganization.value.RegisteredCountryCode });
    this.getState3(this.addorganization.value.RegisteredCountryCode);
    this.addorganization.patchValue({ InvoicingStateProvince: this.addorganization.value.RegisteredStateProvince });
    this.InvoicingPostalCode = this.addorganization.value.RegisteredPostalCode;
  }

  LegalContactFirstName = "";
  LegalContactLastName = "";
  LegalContactEmailAddress = "";
  LegalContactTelephone = "";
  LegalContactFax = "";
  getATCLegal() {
    this.LegalContactFirstName = this.addorganization.value.ATCDirectorFirstName;
    this.LegalContactLastName = this.addorganization.value.ATCDirectorLastName;
    this.LegalContactEmailAddress = this.addorganization.value.ATCDirectorEmailAddress;
    this.LegalContactTelephone = this.addorganization.value.ATCDirectorTelephone;
    this.LegalContactFax = this.addorganization.value.ATCDirectorFax;
  }

  BillingContactFirstName = "";
  BillingContactLastName = "";
  BillingContactEmailAddress = "";
  BillingContactTelephone = "";
  BillingContactFax = "";
  getATCBilling() {
    this.BillingContactFirstName = this.addorganization.value.ATCDirectorFirstName;
    this.BillingContactLastName = this.addorganization.value.ATCDirectorLastName;
    this.BillingContactEmailAddress = this.addorganization.value.ATCDirectorEmailAddress;
    this.BillingContactTelephone = this.addorganization.value.ATCDirectorTelephone;
    this.BillingContactFax = this.addorganization.value.ATCDirectorFax;
  }

  /* issue 20092018 - Reset button on add organisation takes to different page */
  resetForm(){
    this.addorganization.reset({
      OrgId: '',
      ATCOrgId: '',
      OrgStatus_retired: '',
      OrgName: '',
      EnglishOrgName: '',
      CommercialOrgName: '',
      YearJoined: '',
      TaxExempt: '',
      VATNumber: '',
      SAPSoldTo: '',
      VARCSN: '',
      ATCCSN: '',
      AUPCSN: '',
      OrgWebAddress: '',
      RegisteredDepartment: '',
      RegisteredAddress1: '',
      RegisteredAddress2: '',
      RegisteredAddress3: '',
      RegisteredCity: '',
      RegisteredStateProvince: '',
      RegisteredPostalCode: '',
      RegisteredCountryCode: '',
      InvoicingDepartment: '',
      InvoicingAddress1: '',
      InvoicingAddress2: '',
      InvoicingAddress3: '',
      InvoicingCity: '',
      InvoicingStateProvince: '',
      InvoicingPostalCode: '',
      InvoicingCountryCode: '',
      ContractDepartment: '',
      ContractAddress1: '',
      ContractAddress2: '',
      ContractAddress3: '',
      ContractCity: '',
      ContractStateProvince: '',
      ContractPostalCode: '',
      ContractCountryCode: '',
      ATCDirectorFirstName: '',
      ATCDirectorLastName: '',
      ATCDirectorEmailAddress: '',
      ATCDirectorTelephone: '',
      ATCDirectorFax: '',
      LegalContactFirstName: '',
      LegalContactLastName: '',
      LegalContactEmailAddress: '',
      LegalContactTelephone: '',
      LegalContactFax: '',
      BillingContactFirstName: '',
      BillingContactLastName: '',
      BillingContactEmailAddress: '',
      BillingContactTelephone: '',
      BillingContactFax: '',
      AdminNotes: '',
      Status: '',
      DateAdded: '',
      AddedBy: '',
      DateLastAdmin: '',
      LastAdminBy: '',
      CSOResellerUUID: '',
      CSOResellerName: '',
      CSOPartnerManager: '',
      CSOUpdatedOn: '',
      CSOVersion: ''
    });
  }
  /* end line issue 20092018 - Reset button on add organisation takes to different page */


  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.addorganization.get('orgLogo').setValue({
          filename: file.name,
          filetype: file.type,
          value: (<string>reader.result).split(',')[1]
        })
      };
    }
  }
}
