import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddOrganizationEPDBComponent } from './add-organization-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddOrganizationEPDBRoutes: Routes = [
  {
    path: '',
    component: AddOrganizationEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.add_edit.add_new_organization',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddOrganizationEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddOrganizationEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class AddOrganizationEPDBModule { }
