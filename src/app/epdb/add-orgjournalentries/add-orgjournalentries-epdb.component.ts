import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

const now = new Date();

@Component({
  selector: 'app-add-orgjournalentries-epdb',
  templateUrl: './add-orgjournalentries-epdb.component.html',
  styleUrls: [
    './add-orgjournalentries-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class AddOrgJournalEntriesEPDBComponent implements OnInit {
 
  private _serviceUrl = 'api/OrganizationJournalEntries';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  public dataorg:any;
  orgjournalentriesform: FormGroup;
  public orgentriestype:any;
  public useraccesdata:any;
  public loading = false;

  modelPopup: NgbDateStruct;
  
  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private formatdate: AppFormatDate, private session: SessionService) { //, private htmlEntityService: htmlentityService
    
    this.modelPopup = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
    
    let ParentId = new FormControl();
    let ActivityId = new FormControl('', Validators.required);
    let ActivityDate = new FormControl('', Validators.required);
    let DateLastAdmin = new FormControl();
    let LastAdminBy = new FormControl();
    let Notes = new FormControl('');
    
    this.orgjournalentriesform = new FormGroup({
        ParentId:ParentId,
        ActivityId: ActivityId,
        ActivityDate: ActivityDate,
        DateLastAdmin:DateLastAdmin,
        LastAdminBy:LastAdminBy,
        Notes:Notes
    });

  }

  id:string="";
  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet('api/MainOrganization/' + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dataorg = '';
        }
        else {
          this.dataorg = result;
          console.log(this.dataorg)
        }
      },
      error => {
        this.service.errorserver();
        this.dataorg = '';
      });


    data = '';
    this.service.httpClientGet("api/JournalActivities/where/{'ActivityType': 'Organization Journal Entry'}", data)
      .subscribe(result => {
          if(result=="Not found"){
              this.orgentriestype = '';
          }else{
              this.orgentriestype = result;
          }
      },
      error => {
          this.service.errorserver();
          this.orgentriestype = '';
      });
  }

  //submit
  submittedOrgJournalEntries: boolean;
  onSubmitOrgJournalEntries(){

    this.orgjournalentriesform.controls['ActivityId'].markAsTouched();
    this.orgjournalentriesform.controls['ActivityDate'].markAsTouched();
    this.orgjournalentriesform.controls['Notes'].markAsTouched();
    if(this.orgjournalentriesform.valid){
      this.loading = true;
      this.submittedOrgJournalEntries = true;
      this.orgjournalentriesform.value.ParentId = this.dataorg.OrgId
      this.orgjournalentriesform.value.DateLastAdmin = this.formatdate.dateJStoYMD(new Date());
      this.orgjournalentriesform.value.LastAdminBy = this.formatdate.dateJStoYMD(new Date());
      this.orgjournalentriesform.value.AddedBy = this.useraccesdata.ContactName;
      this.orgjournalentriesform.value.ActivityDate = this.formatdate.dateCalendarToYMD(this.orgjournalentriesform.value.ActivityDate); 
      //this.orgjournalentriesform.value.ActivityId = "157"; 

       //Audit Log
       this.orgjournalentriesform.value.cuid = this.useraccesdata.ContactName;
       this.orgjournalentriesform.value.UserId = this.useraccesdata.UserId;
       this.orgjournalentriesform.value.History = "Organization Journal Entries";
       this.orgjournalentriesform.value.Notes = this.service.encoder(this.orgjournalentriesform.value.Notes);
       //End Audit Log
    //  this.orgjournalentriesform.value.Notes = this.htmlEntityService.encoder(this.orgjournalentriesform.value.Notes);
      //convert object to json
      let data = JSON.stringify(this.orgjournalentriesform.value);
  
      //post action
      this.service.httpClientPost(this._serviceUrl, data)

	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

  
      //redirect
      this.router.navigate(['/manage/organization/detail-organization',this.dataorg.OrganizationId]);
      this.loading = false;
    }
  }
}
