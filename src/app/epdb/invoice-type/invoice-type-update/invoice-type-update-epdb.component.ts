import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import { Router } from '@angular/router';
import {Http, Headers, Response} from "@angular/http";
import {ActivatedRoute} from '@angular/router';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-invoice-type-update-epdb',
  templateUrl: './invoice-type-update-epdb.component.html',
  styleUrls: [
    './invoice-type-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class InvoiceTypeUpdateEPDBComponent implements OnInit {
 
  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc"; 
  public id:string;
  public activitytype: any;

  private _serviceUrl = 'api/invoicetype';
  myForm: FormGroup;
  messageResult: string = '';
  messageError: string = ''; 
   
 
  constructor(private router: Router, private service:AppService, private formatdate:AppFormatDate,  private route: ActivatedRoute) { 

    //validation
    let invoice_type_id = new FormControl('', Validators.required);
    let invoice_type = new FormControl('', Validators.required);
    let activity_type = new FormControl('', Validators.required);
    let invoiced_amount = new FormControl('', Validators.required);
    let invoiced_currency = new FormControl('', Validators.required);
    let contract_start_date = new FormControl('', Validators.required);
    let contract_end_date = new FormControl('', Validators.required);
    let description = new FormControl('', Validators.required);
    let status = new FormControl('', Validators.required);
    let cdate = new FormControl('', Validators.required);
    let cuid = new FormControl('', Validators.required);

    this.myForm = new FormGroup({
      invoice_type_id:invoice_type_id,
      invoice_type:invoice_type,
      activity_type:activity_type,
      invoiced_amount:invoiced_amount,
      invoiced_currency:invoiced_currency,
      contract_start_date: contract_start_date,
      contract_end_date:contract_end_date,
      description:description,
      status:status,
      cdate:cdate,
      cuid:cuid,
    });

  }

  ngOnInit() { 
    
    //find data by id
    this.id = this.route.snapshot.params['id']; 
    console.log(this.id);
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
      if(result=="Not found"){
          this.service.notfound();
          this.data = null;
      }
      else{
          this.data = result; 
          this.buildForm();
      } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });

    //get data Activity Type
    this.service.httpClientGet('api/ActivityType',data)
    .subscribe(result => {
      if(result=="Not found"){
          this.service.notfound();
          this.activitytype = null;
      }
      else{
          this.activitytype = result; 
      } 
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  }

  buildForm(): void {
    
    var start = this.data.contract_start_date.split('T');
    var end = this.data.contract_end_date.split('T');
    var today = {year: 2017, month: 5, day: 13};
    var json_start = this.formatdate.dateYMDToCalendar(start);
    var json_end = this.formatdate.dateYMDToCalendar(end);

    let invoice_type_id = new FormControl(this.data.invoice_type_id);
    let activity_type = new FormControl(this.data.activity_type);
    let invoice_type = new FormControl(this.data.invoice_type);
    let status = new FormControl(this.data.status);
    let contract_start_date = new FormControl(json_start);
    let contract_end_date = new FormControl(json_end);
    let invoiced_currency = new FormControl(this.data.invoiced_currency);
    let invoiced_amount = new FormControl(this.data.invoiced_amount);
    let description = new FormControl(this.data.description);
    let cdate = new FormControl(this.data.cdate);
    let cuid = new FormControl(this.data.cuid);
  
    this.myForm = new FormGroup({
      invoice_type_id:invoice_type_id,
      activity_type:activity_type,
      invoice_type:invoice_type,
      status:status,
      contract_start_date:contract_start_date,
      contract_end_date:contract_end_date,
      invoiced_currency:invoiced_currency,
      invoiced_amount:invoiced_amount,
      description:description,
      cdate:cdate,
      cuid:cuid,
    });
  }

  //submit
  submitted:boolean;
  onSubmit(){
    this.submitted = true;
 
    let login_val = this.myForm.value; 
    login_val.contract_start_date = this.formatdate.dateCalendarToYMD(login_val.contract_start_date); 
    login_val.contract_end_date = this.formatdate.dateCalendarToYMD(login_val.contract_end_date);  
    login_val.mdate = this.formatdate.dateJStoYMD(new Date());
    login_val.muid = login_val.cuid; 
 
    let data = JSON.stringify(login_val); 

    this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        console.log(result);
        let tmpData : any = result;
        this.messageResult = tmpData;  
        var resource = result; 
        if(resource['code'] == '1') {
          this.service.openSuccessSwal(resource['name']); 
        }
        else{
          swal(
            'Information!',
            "Update Data Failed",
            'error'
          );
        }
        this.router.navigate(['/epdb/invoice-type/invoice-type-list']);
    },
    error => {
        this.messageError = <any>error
        this.service.errorserver();
    });
  }

  //calendar
  modelPopup1: NgbDateStruct;  
  modelPopup2: NgbDateStruct; 

}
