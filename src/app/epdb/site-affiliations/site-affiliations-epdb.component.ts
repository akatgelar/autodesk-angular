import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";

import {AppService} from "../../shared/service/app.service";
import { Router,ActivatedRoute } from '@angular/router';
import {AppFormatDate} from "../../shared/format-date/app.format-date";

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";
import { SessionService } from '../../shared/service/session.service';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";

// Contact
@Pipe({ name: 'dataFilterAffiliation' })
export class DataFilterAffiliationPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
                (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                // (row.Status.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
        } 
        return array;
    }
}

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        }else{
            return "No Partner Type"
        }
    }
}

@Component({
  selector: 'app-site-affiliations-epdb',
  templateUrl: './site-affiliations-epdb.component.html',
  styleUrls: [
    './site-affiliations-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SiteAffiliationsEPDBComponent implements OnInit {
  
  private _serviceUrl = 'api/MainSite/Affiliate/';
  public data =[]; /* just show affiliate site checked */
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "SiteName";
  public sortOrder: string = "asc";
  sitesarray=[];
  sitescontactid=[];
  status=[];
  public affiliate:FormGroup;
  id:String;
  orgid:String;
  siteid:string;
  public datacontact:any;
  primarysiteid_value:String;
  link:string="";
  linkcode:string="";
  public loading = false;
  useraccesdata:any;
  protected dataService: RemoteData;

  constructor(private completerService: CompleterService,private router: Router,private session: SessionService,private service:AppService,private route:ActivatedRoute, private formatdate:AppFormatDate) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
        this.link = params['link'] || "";
        this.linkcode = params['code'] || "";
    });

    let ContactIdInt = new FormControl('');
    let Status = new FormControl('');
    let DateAdded = new FormControl('');
    let AddedBy = new FormControl('');
    let SiteId = new FormControl('');
    let PrimarySiteId = new FormControl('');

    this.affiliate = new FormGroup({
      ContactIdInt:ContactIdInt,  
      Status:Status, 
      DateAdded:DateAdded,  
      AddedBy:AddedBy,
      SiteId:SiteId,
      PrimarySiteId:PrimarySiteId
    });

  }

  urlGetOrgId(): string {
    var orgarr = this.useraccesdata.OrgId.split(',');
    var orgnew = [];
    for (var i = 0; i < orgarr.length; i++) {
        orgnew.push('"' + orgarr[i] + '"');
    }
    return orgnew.toString();
  }

  checkrole(): boolean {
  let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
            return false;
        } else {
            return true;
        }
    }
  }

  /* add choose affiliate site, kalewat anjir */

  PickedSite(data){
    var dataSite = {
      id : data["originalObject"].SiteId,
      itemName : data["originalObject"].SiteName
    };
    
    this.service.httpClientGet("api/MainSite/AffiliateBySite/"+this.id+"/"+dataSite.id,'')
      .subscribe(result=>{
          console.log(result);
      })
  }

  /* add choose affiliate site, kalewat anjir */

  ngOnInit() {

    /* add choose affiliate site, kalewat anjir */

    var org = "null";

    if(this.checkrole()){
      org = this.urlGetOrgId();
    }

    this.dataService = this.completerService.remote(
      null,
      "Sites",
      "Sites");
    this.dataService.urlFormater(term => {
        return `api/MainOrganization/FindSite/` + this.useraccesdata.UserLevelId + `/` + org + `/` + term;
    });
    this.dataService.dataField("results");

    /* add choose affiliate site, kalewat anjir */

    this.loading = true;
    this.id = this.route.snapshot.params['id'];
    this.orgid = this.route.snapshot.params['orgid'];
    this.siteid = this.route.snapshot.params['siteid'];

    this.loading = true;
    var data='';

    /* populate based role (distributor) */
    var url = this._serviceUrl+this.id+'/'+this.orgid;
    if(this.checkrole()){
      url = 'api/MainSite/AffiliateDistributor/' + this.id + '/' + this.orgid + '/' + this.urlGetOrgId();
    }

    this.service.httpClientGet(url,data) /* populate based role (distributor) */
    .subscribe(result => {
        if(result=="Not found" || result==""){
            this.service.notfound();
            this.data = ["eweuh"]; /* just show affiliate site checked */
            this.loading = false;
        }
        else{

            /* just show affiliate site checked */
            let datatmp:any = result;
            for(var i=0;i<datatmp.length;i++){
              if(datatmp[i].CheckedSites){
                this.data.push({
                  CheckedSites:datatmp[i].CheckedSites,
                  OrgId:datatmp[i].OrgId,
                  SiteAddress1:datatmp[i].SiteAddress1,
                  SiteContactLinkId:datatmp[i].SiteContactLinkId,
                  SiteCountryCode:datatmp[i].SiteCountryCode,
                  SiteId:datatmp[i].SiteId,
                  SiteIdInt:datatmp[i].SiteIdInt,
                  SiteName:datatmp[i].SiteName,
                  Status:datatmp[i].Status,
                  geo_name:datatmp[i].geo_name,
                  PartnerStatusDetail:datatmp[i].PartnerStatusDetail
                })
              }
            }
            /* just show affiliate site checked */

            // this.data = result;
            
            if(this.data.length > 0){
              this.sitechecked(this.data);
            }else{
              this.data = ["eweuh"]; /* just show affiliate site checked */
            }
            
            this.loading = false;
        } 
    },
    error => {
        this.service.errorserver();
        this.data = ["eweuh"]; /* just show affiliate site checked */
        this.loading = false;
    });

    // this.loading = true;
    data='';
    this.service.httpClientGet('api/MainContact/DetailContact/'+this.id+'/'+this.siteid,data)
    .subscribe(result => { 
      try{
        if(result=="Not found"){
            this.service.notfound();
            this.datacontact = ''; 
            this.loading = false;
        }
        else{
            this.datacontact = result; 
            // console.log(this.datacontact); 
            this.primarysiteid_value = this.datacontact.PrimarySiteId;
            this.loading = false;
        } 
      }
      catch(err){
        this.service.errorserver();
        this.datacontact = ''; 
        this.loading = false;
      }
    },
    error => {
        this.service.errorserver();
        this.datacontact = ''; 
        this.loading = false;
    });
    
  }

  sitechecked(site){
    for(var i=0; i<site.length;i++){
      if(site[i].CheckedSites == true){
        this.sitesarray.push([site[i].SiteId,true,site[i].SiteContactLinkId]);
      }else{
        this.sitesarray.push([site[i].SiteId,false,site[i].SiteContactLinkId]);
      }
    }

    for(var i=0; i<this.sitesarray.length;i++){
      this.status.push("A");
    }
  }

  selectSite(value,sites){
    if (value.target.checked) {
      for(var i=0; i<this.sitesarray.length;i++){
        if(this.sitesarray[i][0] == sites){
          this.sitesarray[i][1] = true;
        }
      }
    } 
    else {
      for(var i=0; i<this.sitesarray.length;i++){
        if(this.sitesarray[i][0] == sites){
          this.sitesarray[i][1] = false;
        }
      }
    }
  }

  onCheckboxChangeFn(value){
    this.primarysiteid_value = value;
  }

  updateSiteAffiliations(){

    //convert data to json
    for(var i=0; i<this.sitesarray.length;i++){
      if(this.sitesarray[i][1] == true){
        this.status[i] = "A";
      }else{
        this.status[i] = "X";
      }
    }

    this.loading = true;
    this.affiliate.value.PrimarySiteId = this.primarysiteid_value;
    this.affiliate.value.SiteIdArr = this.sitesarray;
    this.affiliate.value.DateAdded = this.formatdate.dateJStoYMD(new Date());
    this.affiliate.value.AddedBy = "Admin";
    this.affiliate.value.Status = this.status;
    this.affiliate.value.ContactIdInt = this.id;

    this.affiliate.value.cuid = this.useraccesdata.ContactName;
    this.affiliate.value.UserId = this.useraccesdata.UserId;

    let data = JSON.stringify(this.affiliate.value);

    //update site on contact action
    this.service.httpClientPost('api/SiteContactLinks',data)
		 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

    //redirect
    setTimeout(() => {
      this.linkaction();
    }, 3000)
  }

  linkaction(){
    if(this.link=="organization"){
      this.router.navigate(['/manage/organization/detail-organization',this.linkcode]);
    }
    else if(this.link=="site"){
      this.router.navigate(['/manage/site/detail-site',this.linkcode]);
    }
    else if(this.link=="contact"){
      this.loading = true;
      this.service.httpClientGet('api/MainSite/Affiliate/' + this.id + '/' + this.orgid, '');
      setTimeout(() => {
        var splitlink = this.linkcode.split(",");
        this.router.navigate(['/manage/contact/detail-contact/',splitlink[0],splitlink[1]]);
        this.loading = false;
      }, 1000)
    }else if(this.link="searchcontact"){
      this.router.navigate(['/manage/contact/search-contact']);
    }
  }

}