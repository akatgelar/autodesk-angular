import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteAffiliationsEPDBComponent } from './site-affiliations-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

import { DataFilterAffiliationPipe,ReplacePipe } from './site-affiliations-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";

export const SiteAffiliationsEPDBRoutes: Routes = [
  {
    path: '',
    component: SiteAffiliationsEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.site_affiliated.site_affiliated',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SiteAffiliationsEPDBRoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [SiteAffiliationsEPDBComponent, DataFilterAffiliationPipe, ReplacePipe],
  providers:[AppService,AppFormatDate]
})
export class SiteAffiliationsEPDBModule { }
