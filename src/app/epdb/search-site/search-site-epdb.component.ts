import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SessionService } from '../../shared/service/session.service';
import { Router } from '@angular/router';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterSite' })
export class DataFilterSitePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        } else {
            return "No Partner Type"
        }
    }
}

@Component({
    selector: 'app-search-site-epdb',
    templateUrl: './search-site-epdb.component.html',
    styleUrls: [
        './search-site-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SearchSiteEPDBComponent implements OnInit {

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];
    dropdownListSubPartnerType = [];
    selectedItemsSubPartnerType = [];
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownListSubCountry = [];
    selectedItemsSubCountry = [];
    dropdownSettings = {};
    dropdownListState = [];
    selectedItemsState = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    private _serviceUrl = 'api/MainSite';
    messageError: string = '';
    public data: any = '';
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "";
    public sortOrder: string = "desc";
    public partnertype: any;
    filterorgform: FormGroup;
    public useraccesdata: any;
    public loading = false;
  

    constructor(private _http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let SiteId = new FormControl('');
        let SiteName = new FormControl('');
        let PartnerType = new FormControl('');
        let SiteStateProvince = new FormControl('');
        let ContactName = new FormControl('');
        let CSN = new FormControl('');

        this.filterorgform = new FormGroup({
            SiteId: SiteId,
            SiteName: SiteName,
            PartnerType: PartnerType,
            SiteStateProvince: SiteStateProvince,
            ContactName: ContactName,
            CSN: CSN
        });

    }

    allregion = [];
    allsubregion = [];
    allcountries = [];

    /* populate data issue role distributor */
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true;
            }
        }
    }

    itsinstructor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsOrganization(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsDistributor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
    /* populate data issue role distributor */

    territorydefault: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if(this.itsDistributor()){
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         }else{
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* new request nambah user role -__ */

        // if (this.checkrole()) {
        //     if (this.itsDistributor()) {
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     }
        //     else {
        //         if (this.itsOrganization()) {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         }
        //     }
        // }
        var  OrgId = null; var Distributor = null;
        if (this.checkrole()) {

            if (this.itsinstructor()) {
                OrgId = this.urlGetOrgId();
               url = "api/Territory/wherenew/'OrgId'";
            } else {
                if (this.itsDistributor()) {
                    Distributor = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'Distributor'";
                } else {
                    OrgId = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'OrgId'";
                }
            }
            
        }
        var keyword : any = {
            OrgId: OrgId,
            Distributor: Distributor
           
        };
        keyword = JSON.stringify(keyword);
        /* new request nambah user role -__ */

        /* populate data issue role distributor */

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.checkrole()) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }



    ngOnInit() {

        this.loading = true;
        //partner type
        var data = '';
        this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.partnertype = '';
                    this.loading = false;
                }
                else {
                    this.partnertype = result.sort((a,b)=>{
                        if(a.RoleName > b.RoleName)
                            return 1
                        else if(a.RoleName < b.RoleName)
                            return -1
                        else return 0
                    });
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.partnertype = '';
                    this.loading = false;
                });

        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.filterorgform.patchValue({ SiteId: item.SiteId });
            this.filterorgform.patchValue({ SiteName: item.SiteName });
            this.filterorgform.patchValue({ ContactName: item.ContactName });
            this.filterorgform.patchValue({ CSN: item.CSN });
            this.filterorgform.patchValue({ PartnerType: item.Partner });
            this.selectedItemsPartnerTypeStatus = item.PartnerStatus;
            this.dropdownListPartnerTypeStatus = item.dropdownStatus;
            this.selectedItemsSubPartnerType = item.subpartnertype;
            this.dropdownListSubPartnerType = item.dropdownsubpartnertype;
            this.selectedItemsGeo = item.Geo;
            this.dropdownListGeo = item.dropdownGeo;
            this.selectedItemsTerritories = item.Territory;
            this.dropdownListTerritories = item.dropdownTerritory;
            this.selectedItemsCountry = item.Country;
            this.dropdownListCountry = item.dropdownCountry;
            this.selectedItemsState = item.state;
            this.dropdownListState = item.dropdownstate;
            this.filterorgform.patchValue({ SiteStateProvince: item.SiteStateProvince });
            this.onSubmit();
        } else {
            this.getTerritory();
            // Partner Type Status
            data = '';
            this.service.httpClientGet('api/Roles/Status', data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListPartnerTypeStatus = null;
                        this.loading = false;
                    }
                    else {
                        this.dropdownListPartnerTypeStatus = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].Key,
                              itemName: result[Index].KeyValue
                            }
                          })
                          this.selectedItemsPartnerTypeStatus = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].Key,
                              itemName: result[Index].KeyValue
                            }
                          })
        
                        this.loading = false;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsPartnerTypeStatus = [];

            // Geo

            /* populate data issue role distributor */
            var url = "api/Geo/SelectAdmin";
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if(this.itsDistributor()){
            //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
            //         }else{
            //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }

            /* new request nambah user role -__ */

            // if (this.checkrole()) {
            //     if (this.itsDistributor()) {
            //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
            //     }
            //     else {
            //         if (this.itsOrganization()) {
            //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
            //         }
            //     }
            // }
            var  DistributorGeo = null; var OrgIdGeo = null; var SiteIdGeo = null;
            if (this.checkrole()) {
                if (this.itsDistributor()) {
                    DistributorGeo = this.urlGetOrgId();
                   url = "api/Territory/wherenew/'DistributorGeo'";
                } else {
                    if (this.itsOrganization()) {
                        OrgIdGeo = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    } else {
                        SiteIdGeo = this.urlGetSiteId();
                   url = "api/Territory/wherenew/'SiteIdGeo'";
                    }
                }
                
            }
            var keyword : any = {
                DistributorGeo: DistributorGeo,
                OrgIdGeo: OrgIdGeo,
                SiteIdGeo:SiteIdGeo
               
            };
            keyword = JSON.stringify(keyword);
            /* new request nambah user role -__ */

            data = '';
            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe((result:any) => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListGeo = null;
                        this.loading = false;
                    }
                    else {
                        this.dropdownListGeo = result.map(function (el) {
                            return {
                              id: el.geo_code,
                              itemName: el.geo_name
                            }
                          })
                        this.selectedItemsGeo = result.map(function (el) {
                            return {
                              id: el.geo_code,
                              itemName: el.geo_name
                            }
                          })
    
                        this.loading = false;
                    }
                },
                    error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsGeo = [];

            // Region
            data = '';
            this.service.httpClientGet("api/Region", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListRegion = null;
                    }
                    else {
                        this.dropdownListRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].region_code,
                              itemName: result[Index].region_name
                            }
                          })
        
                    }
                    this.allregion = this.dropdownListRegion;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsRegion = [];

            // Sub Region
            data = '';
            this.service.httpClientGet("api/SubRegion", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
    
                    }
                    this.allsubregion = this.dropdownListSubRegion;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsSubRegion = [];

            // Countries
            /* populate data issue role distributor */
             var url2 = "api/Countries/SelectAdmin";
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }
            var  CountryOrgId = null; var DistributorCountry = null;
            if (this.checkrole()) {  
                if (this.itsinstructor()) {
                    CountryOrgId = this.urlGetOrgId();
                     url2 = "api/Territory/wherenew/'CountryOrgId'";
                } else {
                    if (this.itsDistributor()) {
                        DistributorCountry = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'DistributorCountry'";
                    } else {
                        CountryOrgId = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
                
            }
            var keyword : any = {
                CountryOrgId: CountryOrgId,
                DistributorCountry: DistributorCountry,
            };
            keyword = JSON.stringify(keyword);
            data = '';
            this.service.httpClientPost(url2, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                    this.allcountries = this.dropdownListCountry;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsCountry = [];

        }

        //setting dropdown
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

    }

    onItemSelect(item: any) { }
    OnItemDeSelect(item: any) { }
    onSelectAll(items: any) { }
    onDeSelectAll(items: any) { }

    arraygeoid = [];
    arrayterritory = [];
    arraycountry = [];


    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
        if (this.itsDistributor()) {
            DistTerr = this.urlGetOrgId();
           url = "api/Countries/DistTerr";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistTerr:DistTerr,
        CtmpTerritory:this.arrayterritory.toString()
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
        // this.arrayterritory = [];
        // if (this.selectedItemsTerritories.length > 0) {
        //     for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
        //         this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
        //     }
        // }

        // var tmpGeo = "''"
        // this.arraygeoid = [];
        // if (this.selectedItemsGeo.length > 0) {
        //     for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //         this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //     }
        //     tmpGeo = this.arraygeoid.toString()
        // }

        // // Countries
        // var data = '';
        // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString(), data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             this.dropdownListCountry = null;
        //         }
        //         else {
        //             this.dropdownListCountry = JSON.parse(result).map((item) => {
        //                 return {
        //                     id: item.countries_code,
        //                     itemName: item.countries_name
        //                 }
        //             })
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });
    }

    onCountrySelect(item: any) {
        //task malam sabtu 27/07/2018
        this.selectedItemsState= [];
       /// this.selectedItemsTerritories = [];

        var tmpCountry = "''"
        this.arraycountry = [];
        if (this.selectedItemsCountry.length > 0) {
           // for (var i = 0; i < this.selectedItemsCountry.length; i++) {
           //     this.arraycountry.push('"' + this.selectedItemsCountry[i].id + '"');
            //}
            this.arraycountry.push('"' + this.selectedItemsCountry[0].id + '"');
            tmpCountry = this.arraycountry.toString()
        }

        var data = '';
        this.service.httpClientGet('api/State/whereState/' + tmpCountry + '', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListState = null;
                    this.loading = false;
                }
                else {
                    this.dropdownListState = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].StateID,
                          itemName: result[Index].StateName
                        }
                      })
                   this.dropdownListState = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].StateID,
                          itemName: result[Index].StateName
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsState = [];
    

        

    }


    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';
            /* populate data issue role distributor */
        //    var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }
            var  DistTerr = null;
        if (this.itsDistributor()) {
            DistTerr = this.urlGetOrgId();
           url = "api/Countries/DistTerr";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistTerr:DistTerr,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        //split countries
        // let countriesArrTmp = [];

        // var tmpTerritory = "''"
        // this.arrayterritory = [];
        // if (this.selectedItemsTerritories.length > 0) {
        //     for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
        //         this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
        //     }
        //     tmpTerritory = this.arrayterritory.toString()
        // }

        // var tmpGeo = "''"
        // this.arraygeoid = [];
        // if (this.selectedItemsGeo.length > 0) {
        //     for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //         this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //     }
        //     tmpGeo = this.arraygeoid.toString()
        // }


        // if (this.arrayterritory.length > 0) {
        //     // Countries
        //     var data = '';
        //     this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)
        //         .subscribe(result => {
        //             if (result == "Not found") {
        //                 this.service.notfound();
        //                 this.dropdownListCountry = null;
        //             }
        //             else {
        //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
        //                     return {
        //                         id: item.countries_code,
        //                         itemName: item.countries_name
        //                     }
        //                 })
        //             }
        //         },
        //             error => {
        //                 this.service.errorserver();
        //             });
        // }
    }


    onPartnerSelect(item) {
        
        var url = "api/Roles/wheresub/{RoleCode:'" + item + "'}" ;      
  // subpartner type
  var data = '';
  this.service.httpClientGet(url, data)
      .subscribe((result:any) => {
          if (result == "Not found") {
              this.service.notfound();
              this.dropdownListSubPartnerType = null;
              this.loading = false;
          }
          else {
              this.dropdownListSubPartnerType = result.sort((a,b)=>{
                if(a.RoleName > b.RoleName)
                    return 1
                else if(a.RoleName < b.RoleName)
                    return -1
                else return 0
              }).map(function (el) {
                  return {
                    id: el.RoleId,
                    itemName: el.RoleName
                  }
                })
             this.selectedItemsSubPartnerType = result.map(function (el) {
                  return {
                    id: el.RoleId,
                    itemName: el.RoleName
                  }
                })
          }
      },
          error => {
              this.service.errorserver();
          });
  this.selectedItemsSubPartnerType = [];




    }
    

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
        if (this.itsDistributor()) {
            DistTerr = this.urlGetOrgId();
           url = "api/Countries/DistTerr";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistTerr:DistTerr,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }



    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
            if (this.itsDistributor()) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })



                }
            },
                error => {
                    this.service.errorserver();
                });
        // Countries and Territory
        // var tmpTerritory = "''"
        // this.arrayterritory = [];
        // if (this.selectedItemsTerritories.length > 0) {
        //     for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
        //         this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
        //     }
        //     tmpTerritory = this.arrayterritory.toString()
        // }

        // var tmpGeo = "''"
        // this.arraygeoid = [];
        // if (this.selectedItemsGeo.length > 0) {
        //     for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //         this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //     }
        //     tmpGeo = this.arraygeoid.toString()
        // }


        // Countries
        // var data = '';
        // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             this.dropdownListCountry = null;
        //         }
        //         else {
        //             this.dropdownListCountry = JSON.parse(result).map((item) => {
        //                 return {
        //                     id: item.countries_code,
        //                     itemName: item.countries_name
        //                 }
        //             })


        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
            if (this.itsDistributor()) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // var data = '';

        // //split countries
        // let countriesArrTmp = [];
        // this.service.httpClientGet("api/Countries/filter/'" + item.id + "'", data)
        //     .subscribe(result => {
        //         if (result == "Not found") {
        //             this.service.notfound();
        //             countriesArrTmp = null;
        //         }
        //         else {
        //             countriesArrTmp = JSON.parse(result);
        //             for (var i = 0; i < countriesArrTmp.length; i++) {
        //                 var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
        //                 if (index !== -1) {
        //                     this.selectedItemsCountry.splice(index, 1);
        //                 }
        //             }
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });

        // var index = this.arraygeoid.findIndex(x => x == '"' + item.id + '"');
        // this.arraygeoid.splice(index, 1);
        // this.selectedItemsCountry.splice(index, 1);

        // if (this.selectedItemsGeo.length > 0) {

        //     // Countries
        //     var tmpTerritory = "''"
        //     this.arrayterritory = [];
        //     if (this.selectedItemsTerritories.length > 0) {
        //         for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
        //             this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
        //         }
        //         tmpTerritory = this.arrayterritory.toString()
        //     }

        //     var tmpGeo = "''"
        //     this.arraygeoid = [];
        //     if (this.selectedItemsGeo.length > 0) {
        //         for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        //             this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        //         }
        //         tmpGeo = this.arraygeoid.toString()
        //     }

        //     // Countries
        //     var data = '';
        //     this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)
        //         .subscribe(result => {
        //             if (result == "Not found") {
        //                 this.service.notfound();
        //                 this.dropdownListCountry = null;
        //             }
        //             else {
        //                 this.dropdownListCountry = JSON.parse(result).map((item) => {
        //                     return {
        //                         id: item.countries_code,
        //                         itemName: item.countries_name
        //                     }
        //                 })
        //             }
        //         },
        //             error => {
        //                 this.service.errorserver();
        //             });
        // }
        // else {
        //     this.dropdownListCountry = this.allcountries;
        //     this.selectedItemsCountry.splice(index, 1);
        // }
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];

        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
            if (this.itsDistributor()) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistGeo:DistGeo,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

       // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
       this.service.httpClientPost(url, keyword)
       .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });





    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {

        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp = result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = this.allsubregion;
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
        this.onCountrySelect(item);
    }
    OnCountriesDeSelect(item: any) {
        this.onCountrySelect(item);
     }
    onCountriesSelectAll(items: any) { 
        this.onCountrySelect(items);
    }
    onCountriesDeSelectAll(items: any) { 
        this.onCountrySelect(items);
    }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    //submit form
    onSubmit() {
        this.loading = true;
        var data = '';
        var geoarr = [];
        var regionarr = [];
        var subregionarr = [];
        var countryarr = [];
        var partnertypestatus = [];
        var subpartnertype =[];
        var territoryarr = [];
        var state =[]
        this.data = null;

        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countryarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnertypestatus.push("'" + this.selectedItemsPartnerTypeStatus[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
            territoryarr.push("'" + this.selectedItemsTerritories[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsSubPartnerType.length; i++) {
            subpartnertype.push("'" + this.selectedItemsSubPartnerType[i].id + "'");
        }

        for (var i = 0; i < this.selectedItemsState.length; i++) {
            state.push("'" + this.selectedItemsState[i].id + "'");
        }

        // let notAdmin = true;
        // let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        // for (var i = 0; i < userlvlarr.length; i++) {
        //     if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
        //         notAdmin = false;
        //     }
        // }

        // Buat object untuk filter yang dipilih
        var params = {
            SiteId: this.filterorgform.value.SiteId,
            SiteName: this.filterorgform.value.SiteName,
            ContactName: this.filterorgform.value.ContactName,
            CSN: this.filterorgform.value.CSN,
            Partner: this.filterorgform.value.PartnerType,
            PartnerStatus: this.selectedItemsPartnerTypeStatus,
            dropdownStatus: this.dropdownListPartnerTypeStatus,
            subpartnertype: this.selectedItemsSubPartnerType,
            dropdownsubpartnertype: this.dropdownListSubPartnerType,
            Geo: this.selectedItemsGeo,
            dropdownGeo: this.dropdownListGeo,
            Territory: this.selectedItemsTerritories,
            dropdownTerritory: this.dropdownListTerritories,
            Country: this.selectedItemsCountry,
            dropdownCountry: this.dropdownListCountry,
            state: this.selectedItemsState,
            dropdownstate: this.dropdownListState,
            SiteStateProvince: this.filterorgform.value.SiteStateProvince
        };

        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
        // console.log(params);


        // if (this.checkrole()) {
        //     var orgarr = this.useraccesdata.OrgId.split(',');
        //     var orgnew = [];
        //     for (var i = 0; i < orgarr.length; i++) {
        //         orgnew.push('"' + orgarr[i] + '"');
        //     }

        //     var dataFilter = 
        //         {
        //             SiteId: this.filterorgform.value.SiteId,
        //             SiteName: this.filterorgform.value.SiteName,
        //             SiteStateProvince: this.filterorgform.value.SiteStateProvince,
        //             SiteCountryCode: countryarr.toString(),
        //             ContactName: this.filterorgform.value.ContactName,
        //             PartnerType: this.filterorgform.value.PartnerType,
        //             CSN: this.filterorgform.value.CSN,
        //             PartnerTypeStatus: partnertypestatus.toString(),
        //             Geo: geoarr.toString(),
        //             Territory: territoryarr.toString()
        //         }

        //     var url = this._serviceUrl + "/where/{'SiteId': '" + this.filterorgform.value.SiteId + "','SiteName': '" + this.filterorgform.value.SiteName +
        //         "','SiteStateProvince': '" + this.filterorgform.value.SiteStateProvince + "','SiteCountryCode': '" + countryarr.toString() +
        //         "','ContactName': '" + this.filterorgform.value.ContactName + "','PartnerType': '" + this.filterorgform.value.PartnerType + "','CSN': '" + this.filterorgform.value.CSN +
        //         "','PartnerTypeStatus': '" + partnertypestatus.toString() +
        //         "','Geo': '" + geoarr.toString() + "','Territory': '" + territoryarr.toString() + "',"

        //     if (this.itsdistributor()) {
        //         url += "'UserRoleTerritory':'" + this.territorydefault + "'}"
        //     } else {
        //         url += "'JustThisOrgId': '" + orgnew.toString() + "'}"
        //     }

        //     this.service.httpClientGet(url, data)
        //         .subscribe(result => {
        //             var finalresult = result.replace(/\t/g, " ")
        //             if (finalresult == "Not found") {
        //                 this.service.notfound();
        //                 this.data = '';
        //                 this.loading = false;
        //             }
        //             else {
        //                 this.data = JSON.parse(finalresult);
        //                 this.loading = false;
        //             }

        //         },
        //             error => {
        //                 this.service.errorserver();
        //                 this.data = '';
        //                 this.loading = false;
        //             });
        // } else {

        var dataFilter =
        {
            SiteId: this.filterorgform.value.SiteId,
            SiteName: this.filterorgform.value.SiteName,
            SiteNameSpecial: this.service.encoder(this.filterorgform.value.SiteName),
           // SiteStateProvince: this.filterorgform.value.SiteStateProvince,
            SiteCountryCode: countryarr.toString(),
            ContactName: this.filterorgform.value.ContactName,
            PartnerType: this.filterorgform.value.PartnerType,
            CSN: this.filterorgform.value.CSN,
            PartnerTypeStatus: partnertypestatus.toString(),
            SubPartnerType: subpartnertype.toString(),
            Geo: geoarr.toString(),
            Territory: territoryarr.toString(),
            SiteStateProvince: state.toString()

        }

        // if (this.checkrole()) {
        //     var orgarr = this.useraccesdata.OrgId.split(',');
        //     var orgnew = [];
        //     for (var i = 0; i < orgarr.length; i++) {
        //         orgnew.push("'" + orgarr[i] + "'");
        //     }

        //     var roleFilter = {};

        //     if(this.checkrole()){
        //         if(this.itsDistributor()){
        //             roleFilter = {
        //                 "Distributor" : this.urlGetOrgId(),
        //                 // "UserRoleTerritory": this.territorydefault
        //             }
        //         }else{
        //             roleFilter = {
        //                 "SiteAdmin" : this.urlGetOrgId(),
        //                 "JustThisOrgId": orgnew.toString()
        //             }
        //         }
        //     }

        //     dataFilter = Object.assign(dataFilter, roleFilter);
        // }


        /* new request nambah user role -__ */

        if (this.checkrole()) {

            var roleFilter = {};

            if (this.itsDistributor()) {
                roleFilter = {
                    "Distributor": this.urlGetOrgId()
                }
            } else {
                if (this.itsOrganization()) {
                    roleFilter = {
                        "OrgAdmin": this.urlGetOrgId()
                    }
                } else {
                    roleFilter = {
                        "SiteAdmin": this.urlGetSiteId()
                    }
                }
            }
            dataFilter = Object.assign(dataFilter, roleFilter);
        }

        /* new request nambah user role -__ */


        // this.service.httpClientGet(this._serviceUrl + "/where/{'SiteId': '" + this.filterorgform.value.SiteId + "','SiteName': '" + this.filterorgform.value.SiteName +
        //     "','SiteStateProvince': '" + this.filterorgform.value.SiteStateProvince + "','SiteCountryCode': '" + countryarr.toString() +
        //     "','ContactName': '" + this.filterorgform.value.ContactName + "','PartnerType': '" + this.filterorgform.value.PartnerType + "','CSN': '" + this.filterorgform.value.CSN +
        //     "','PartnerTypeStatus': '" + partnertypestatus.toString() +
        //     "','Geo': '" + geoarr.toString() + "','Territory': '" + territoryarr.toString() + "'}", data)

        // console.log(dataFilter)

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost(
            this._serviceUrl + "/FilterSite",
            dataFilter
        )
            .subscribe(result => {
                //var finalresult = result.replace(/\t/g, " ")
                var finalresult : any = result;
                if (finalresult == "Not found") {
                    this.service.notfound();
                    this.data = '';
                    this.loading = false;
                }
                else {
                    // this.data = finalresult.map((el)=>{
                    //     return {...el,PartnerType: el.PartnerType.split(',')}
                   // });
                    //  this.loading = false;



                      this.data = finalresult.map((el)=>{
                        let pt = []
                        if(el.PartnerType.indexOf('ATC')>-1){
                            if(el.PartnerType.indexOf('ATC(Active)')>-1)
                                pt.push('ATC(Active)')
                            else pt.push('ATC(Inactive)')
                        }
                        if(el.PartnerType.indexOf('AAP')>-1){
                            if(el.PartnerType.indexOf('AAP(Active)')>-1)
                                pt.push('AAP(Active)')
                            else pt.push('AAP(Inactive)')
                        }
                        if(el.PartnerType.indexOf('CTC')>-1){
                            if(el.PartnerType.indexOf('CTC(Active)')>-1)
                                pt.push('CTC(Active)')
                            else pt.push('CTC(Inactive)')
                        }
                        if(el.PartnerType.indexOf('MTP')>-1){
                            if(el.PartnerType.indexOf('MTP(Active)')>-1)
                                pt.push('MTP(Active)')
                            else pt.push('MTP(Inactive)')
                        }
                        if(el.PartnerType.indexOf('MED')>-1){
                            if(el.PartnerType.indexOf('MED(Active)')>-1)
                                pt.push('MED(Active)')
                            else pt.push('MED(Inactive)')
                        }
                        if(el.PartnerType.indexOf('VCP')>-1){
                            if(el.PartnerType.indexOf('VCP(Active)')>-1)
                                pt.push('VCP(Active)')
                            else pt.push('VCP(Inactive)')
                        }
                        if(el.PartnerType.indexOf('RED')>-1){
                            if(el.PartnerType.indexOf('RED(Active)')>-1)
                                pt.push('RED(Active)')
                            else pt.push('RED(Inactive)')
                        }
                        //return {...el,PartnerType: el.PartnerType.split(',')}
                        return {...el,PartnerType: pt}
                    });
                    this.loading = false;
                   
                }

            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
        /* new post for autodesk plan 17 oct */
        // }
    }
}
