import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchSiteEPDBComponent } from './search-site-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";

import { DataFilterSitePipe, ReplacePipe } from './search-site-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const SearchSiteEPDBRoutes: Routes = [
  {
    path: '',
    component: SearchSiteEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.site.search.search_site',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchSiteEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [SearchSiteEPDBComponent, DataFilterSitePipe, ReplacePipe],
  providers: [AppService, AppFilterGeo]
})
export class SearchSiteEPDBModule { }
