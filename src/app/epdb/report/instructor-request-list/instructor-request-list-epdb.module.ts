import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorRequestListComponent } from './instructor-request-list.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';
import { SearchPipe } from "../../../instructor-report/instructor-list/instructor-search-pipe"

export const InstructorRequestListEPDBRoutes: Routes = [
  {
    path: '',
    component: InstructorRequestListComponent,
    data: {
      breadcrumb: 'menu_report_epdb.instructor_request_list_report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(InstructorRequestListEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [InstructorRequestListComponent, SearchPipe],
  providers: [AppService, AppFilterGeo]
})
export class InstructorRequestListEPDBModule { }