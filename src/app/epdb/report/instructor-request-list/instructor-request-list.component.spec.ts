import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorRequestListComponent } from './instructor-request-list.component';

describe('InstructorRequestListComponent', () => {
  let component: InstructorRequestListComponent;
  let fixture: ComponentFixture<InstructorRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
