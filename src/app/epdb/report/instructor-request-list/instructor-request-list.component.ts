import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { SessionService } from '../../../shared/service/session.service';
import * as Rollbar from 'rollbar';
import {NgbModal, ModalDismissReasons,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-instructor-request-list',
  templateUrl: './instructor-request-list.component.html',
  styleUrls: ['./instructor-request-list.component.css']
})
export class InstructorRequestListComponent implements OnInit {

  private instructorRequestedModel =[];
  private instructorList = [];
  private currentUserId: any;
  private baseApiUrl: string ="api/InstructorDeletion/";
  public loading = false;  
  public  messageError: string = '';
  private rowsOnPage: number = 10;
  private searchParam;
  constructor(private service: AppService,private session: SessionService,private rollbar: Rollbar, private modalService: NgbModal, private router :Router) { }

  ngOnInit() {
    var userCurrentData = this.session.getData();

    if(userCurrentData != null){
      userCurrentData= JSON.parse(userCurrentData);
      this.currentUserId=userCurrentData['UserId'];
    
      var userLevel=userCurrentData['UserLevelId'].split(',');
      if(userLevel.indexOf("DISTRIBUTOR") != -1){
        this.getRequestedInstructorList();
     
      }else{
        this.router.navigate(['/forbidden']);
      }

     
    }else{
      this.router.navigate(['/no-role-page']);
    }
  }


  getRequestedInstructorList(){
    if (this.currentUserId != null) {
      this.service.httpClientGet(this.baseApiUrl + "GetRequestedList/" + this.currentUserId,'')
        .subscribe(res => {
          var data = res;
          if (data['code'] == true) {
            this.instructorRequestedModel = data['model'];
          } else if (data['code'] == false) {
            this.rollbar.log("error", data['message']);
          }
          if (data == null) {
            this.rollbar.log("error", "Something went wrong when parsing SKU data!");
            this.loading = false;
          }

        }, error => {
          this.messageError = <any>error;
          this.service.errorserver();
          this.loading = false;
        });
    }
  }

  getInstructorList(contactId){
    if(this.instructorRequestedModel.indexOf(contactId)>-1 && this.instructorList.length == 0){
      var idx = this.instructorRequestedModel.indexOf(contactId);
      this.instructorList = this.instructorRequestedModel[idx].requestedInstructorLists;
    }
     else
        this.instructorList = [];
  }

  approveAction(requestID){
  
    if(requestID != null && this.currentUserId != null){
      this.service.httpClientGet(this.baseApiUrl + "ActionForRequest/" + requestID +"/" + this.currentUserId,'')
      .subscribe(res => {
        var data = res;
        if (data['code'] == true) {
          this.getRequestedInstructorList();
          this.instructorList = [];
          swal(
            'Information!',
            data['message'],
            'success'
        );
        } else if (data['code'] == false) {
          this.rollbar.log("error", data['message']);
          swal(
            'warning!',
            data['message'],
            'success'
        );
        }
        if (data == null) {
          this.rollbar.log("error", "Something went wrong when parsing SKU data!");
          this.loading = false;
        }

      }, error => {
        this.messageError = <any>error;
        this.service.errorserver();
        this.loading = false;
      });
    }
  }
}
