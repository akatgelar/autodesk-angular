import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QualificationByContactReportEPDBComponent } from './qualifications-by-contact-report-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
// import { DataFilterReport } from './organization-report-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const QualificationByContactReportEPDBRoutes: Routes = [
    {
        path: '',
        component: QualificationByContactReportEPDBComponent,
        data: {
            breadcrumb: 'menu_report_epdb.qualification_contact_report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(QualificationByContactReportEPDBRoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [QualificationByContactReportEPDBComponent],
    providers: [AppService, AppFilterGeo]
})
export class QualificationByContactReportEPDBModule { }