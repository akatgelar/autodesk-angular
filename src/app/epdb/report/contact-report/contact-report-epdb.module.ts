import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactReportEPDBComponent } from './contact-report-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const ContactReportEPDBRoutes: Routes = [
  {
    path: '',
    component: ContactReportEPDBComponent,
    data: {
      breadcrumb: 'menu_report_epdb.contact_report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ContactReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [ContactReportEPDBComponent],
  providers: [AppService, AppFilterGeo]
})
export class ContactReportEPDBModule { }