import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-site-accreditations-report',
    templateUrl: './site-accreditations-report.component.html',
    styleUrls: [
        './site-accreditations-report.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SARComponent implements OnInit {

    public data: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";

    dropdownListActivity = [];
    selectedItemsActivity = [];

    dropdownListPartner = [];
    selectedItemsPartner = [];

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListFieldOrganization = [];
    selectedItemsFieldOrganization = [];

    dropdownListFieldSite = [];
    selectedItemsFieldSite = [];

    dropdownListFieldShow2 = [];
    selectedItemsFieldShow2 = [];

    dropdownSettings = {};

    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;

    public dataFound = false;
    listkey = [];
    presentSetting;
    public siteAccreditation;
    public loading = false;

    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private parserFormatter: NgbDateParserFormatter) { }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsMarketType = [];
    }

    getPartnerTypeStatus() {
        var data: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'SiteStatus','Status':'A'}", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsPartnerTypeStatus = [];
        this.getMarketType();
    }

    getPartnerType() {
        var data = '';
        this.service.httpClientGet("api/Roles", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListPartner = null;
                }
                else {
                    this.dropdownListPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsPartner = [];
        this.getPartnerTypeStatus();
    }

    getDataActivity() {
        var data: any;
        this.service.httpClientGet("api/JournalActivities/where/{'ActivityType':'Site Accreditation','StatusNot':'X'}", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListActivity = data.map((item) => {
                        return {
                            id: item.ActivityId,
                            itemName: item.ActivityName
                        }
                    });
                } else {
                    this.dropdownListActivity = [];
                }
            }, error => {
                this.service.errorserver();
                this.dropdownListActivity = [];
            });
        this.selectedItemsActivity = [];
    }

    getGeo() {
        var data = '';
        this.service.httpClientGet("api/Geo", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    getTerritory() {
        var data: any;
        this.service.httpClientGet("api/Territory", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListTerritories = data.map((item) => {
                        return {
                            id: item.TerritoryId,
                            itemName: item.Territory_Name
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsTerritories = [];
    }

    getOrgFields() {
        var data: any;
        this.service.httpClientGet("api/MainOrganization/getColumns", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListFieldOrganization = data.map((item) => {
                        return {
                            id: "o." + item.Field,
                            itemName: item.Field
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsFieldOrganization = [];
    }

    getSiteFields() {
        var data: any;
        this.service.httpClientGet("api/MainSite/getColumns", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].Field != "SiteIdInt") {
                            this.dropdownListFieldSite.push({ id: "s." + data[i].Field, itemName: data[i].Field });
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsFieldSite = [];
    }

    getAnotherFields() {
        var field = [
            { "id": "m.MarketType", "itemName": "Market Type" },
            { "id": "r.RoleName", "itemName": "Partner Type" },
            { "id": "sr.Status", "itemName": "Partner Type Status" },
            { "id": "sr.CSN", "itemName": "Partner Type CSN" },
            { "id": "sr.ITS_ID", "itemName": "ITS Site ID" },
            { "id": "sr.UParent_CSN", "itemName": "Ultimate Parent CSN" },
            { "id": "tc.geo_name", "itemName": "Geo" },
            { "id": "tc.region_name", "itemName": "Region" },
            { "id": "tc.subregion_name", "itemName": "SubRegion" },
            { "id": "t.Territory_Name", "itemName": "Territory" },
        ];
        for (let n = 0; n < field.length; n++) {
            this.dropdownListFieldShow2.push(field[n]);
        }
        this.selectedItemsFieldShow2 = [];
    }

    allregion = [];
    allsubregion = [];
    allcountries = [];
    ngOnInit() {
        this.getDataActivity();
        this.getPartnerType();
        this.getGeo();
        this.getTerritory();
        this.getOrgFields();
        this.getSiteFields();
        this.getAnotherFields();
        this.presentSetting = "Present";

        // Region
        var data = '';
        this.service.httpClientGet("api/Region",data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.dropdownListRegion = null;
            }
            else{
                this.dropdownListRegion = Object.keys(result).map(function (Index) {
                    return {
                      id: result[Index].region_code,
                      itemName: result[Index].region_name
                    }
                  })

            } 
            this.allregion = this.dropdownListRegion;
        },
        error => {
            this.service.errorserver();
        });
        this.selectedItemsRegion = [];

        // Sub Region
        data = '';
        this.service.httpClientGet("api/SubRegion",data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.dropdownListSubRegion = null;
            }
            else{
                this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                    return {
                      id: result[Index].subregion_code,
                      itemName: result[Index].subregion_name
                    }
                  })
            } 
            this.allsubregion = this.dropdownListSubRegion;
        },
        error => {
            this.service.errorserver();
        });
        this.selectedItemsSubRegion = [];

        // Countries
        data = '';
        this.service.httpClientGet("api/Countries",data)
        .subscribe(result => { 
            if(result=="Not found"){
                this.service.notfound();
                this.dropdownListCountry = null;
            }
            else{
                this.dropdownListCountry = Object.keys(result).map(function (Index) {
                    return {
                      id: result[Index].countries_code,
                      itemName: result[Index].countries_name
                    }
                  })

            } 
            this.allcountries = this.dropdownListCountry;
        },
        error => {
            this.service.errorserver();
        });
        this.selectedItemsCountry = [];

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    arraygeoid=[];
  onGeoSelect(item:any){ 
    this.arraygeoid.push('"'+item.id+'"');
    
    // Region
    var data = '';
    this.service.httpClientGet("api/Region/filter/"+this.arraygeoid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListRegion = null;
        }
        else{
            this.dropdownListRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].region_code,
                  itemName: result[Index].region_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });

    // SubRegion
    var data = '';
    this.service.httpClientGet("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListSubRegion = null;
        }
        else{
            this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].subregion_code,
                  itemName: result[Index].subregion_name
                }
              })
        } 
    },
    error => {
        this.service.errorserver();
    });

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filter/"+this.arraygeoid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListCountry = null;
        }
        else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });


  }

  OnGeoDeSelect(item:any){
    var data = '';

    //split region
    let regionArrTmp :any;
    this.service.httpClientGet("api/Region/filter/'"+item.id+"'",data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            regionArrTmp = null;
        }
        else{
            regionArrTmp = result;
            for(var i=0;i<regionArrTmp.length;i++){
                var index = this.selectedItemsRegion.findIndex(x => x.id == regionArrTmp[i].region_code);
                if(index !== -1){
                    this.selectedItemsRegion.splice(index,1);
                }
            }
        } 
    },
    error => {
        this.service.errorserver();
    });

    //split sub region
    let subregionArrTmp :any;
    this.service.httpClientGet("api/SubRegion/filter/'"+item.id+"'",data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            subregionArrTmp = null;
        }
        else{
            subregionArrTmp = result;
            for(var i=0;i<subregionArrTmp.length;i++){
                var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                if(index !== -1){
                    this.selectedItemsSubRegion.splice(index,1);
                }
            }
        } 
    },
    error => {
        this.service.errorserver();
    });

    //split countries
    let countriesArrTmp : any;
    this.service.httpClientGet("api/Countries/filter/'"+item.id+"'",data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            countriesArrTmp = null;
        }
        else{
            countriesArrTmp = result;
            for(var i=0;i<countriesArrTmp.length;i++){
                var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                if(index !== -1){
                    this.selectedItemsCountry.splice(index,1);
                }
            }
        } 
    },
    error => {
        this.service.errorserver();
    });

    var index = this.arraygeoid.findIndex(x => x == '"'+item.id+'"');
    this.arraygeoid.splice(index,1);
    this.selectedItemsRegion.splice(index,1);
    this.selectedItemsSubRegion.splice(index,1);
    this.selectedItemsCountry.splice(index,1);

    if(this.arraygeoid.length > 0){
      // Region
      var data = '';
      this.service.httpClientGet("api/Region/filter/"+this.arraygeoid.toString(),data)
      .subscribe(result => { 
          if(result=="Not found"){
              this.service.notfound();
              this.dropdownListRegion = null;
          }
          else{
            this.dropdownListRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].region_code,
                  itemName: result[Index].region_name
                }
              })

          } 
      },
      error => {
          this.service.errorserver();
      });

      // SubRegion
      var data = '';
      this.service.httpClientGet("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
      .subscribe(result => { 
          if(result=="Not found"){
              this.service.notfound();
              this.dropdownListSubRegion = null;
          }
          else{
            this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].subregion_code,
                  itemName: result[Index].subregion_name
                }
              })
          } 
      },
      error => {
          this.service.errorserver();
      });

      // Countries
      var data = '';
      this.service.httpClientGet("api/Countries/filter/"+this.arraygeoid.toString(),data)
      .subscribe(result => { 
          if(result=="Not found"){
              this.service.notfound();
              this.dropdownListCountry = null;
          }
          else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

          } 
      },
      error => {
          this.service.errorserver();
      });
    }
    else{
        this.dropdownListRegion = this.allregion;
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsRegion.splice(index,1);
        this.selectedItemsSubRegion.splice(index,1);
        this.selectedItemsCountry.splice(index,1);
    }
  }

  onGeoSelectAll(items: any){
    this.arraygeoid = [];
    
    for(var i=0;i<items.length;i++){
      this.arraygeoid.push('"'+items[i].id+'"');
    }
    
    // Region
    var data = '';
    this.service.httpClientGet("api/Region/filter/"+this.arraygeoid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListRegion = null;
        }
        else{
            this.dropdownListRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].region_code,
                  itemName: result[Index].region_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });

    // SubRegion
    var data = '';
    this.service.httpClientGet("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListSubRegion = null;
        }
        else{
            this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].subregion_code,
                  itemName: result[Index].subregion_name
                }
              })
        } 
    },
    error => {
        this.service.errorserver();
    });

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filter/"+this.arraygeoid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListCountry = null;
        }
        else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });
  }

  onGeoDeSelectAll(items: any){
    this.dropdownListRegion = this.allregion;
    this.dropdownListSubRegion = this.allsubregion;
    this.dropdownListCountry = this.allcountries;
    
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
  }

  arrayregionid=[];

  onRegionSelect(item:any){
    this.arrayregionid.push('"'+item.id+'"');
    
    // SubRegion
    var data = '';
    this.service.httpClientGet("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListSubRegion = null;
        }
        else{
            this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].subregion_code,
                  itemName: result[Index].subregion_name
                }
              })
        } 
    },
    error => {
        this.service.errorserver();
    });

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListCountry = null;
        }
        else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });
  }

  OnRegionDeSelect(item:any){

    //split sub region
    let subregionArrTmp :any;
    this.service.httpClientGet("api/SubRegion/filterregion/'"+item.id+"'",data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            subregionArrTmp = null;
        }
        else{
            subregionArrTmp = result;
            for(var i=0;i<subregionArrTmp.length;i++){
                var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                if(index !== -1){
                    this.selectedItemsSubRegion.splice(index,1);
                }
            }
        } 
    },
    error => {
        this.service.errorserver();
    });

    //split countries
    let countriesArrTmp :any;
    this.service.httpClientGet("api/Countries/filterregion/'"+item.id+"'",data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            countriesArrTmp = null;
        }
        else{
            countriesArrTmp = result;
            for(var i=0;i<countriesArrTmp.length;i++){
                var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                if(index !== -1){
                    this.selectedItemsCountry.splice(index,1);
                }
            }
        } 
    },
    error => {
        this.service.errorserver();
    });
    
    var index = this.arrayregionid.findIndex(x => x == '"'+item.id+'"');
    this.arrayregionid.splice(index,1);
    this.selectedItemsSubRegion.splice(index,1);
    this.selectedItemsCountry.splice(index,1);
    
    if(this.arrayregionid.length > 0){
      // SubRegion
      var data = '';
      this.service.httpClientGet("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
      .subscribe(result => { 
          if(result=="Not found"){
              this.service.notfound();
              this.dropdownListSubRegion = null;
          }
          else{
            this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].subregion_code,
                  itemName: result[Index].subregion_name
                }
              })
          } 
      },
      error => {
          this.service.errorserver();
      });

      // Countries
      var data = '';
      this.service.httpClientGet("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
      .subscribe(result => { 
          if(result=="Not found"){
              this.service.notfound();
              this.dropdownListCountry = null;
          }
          else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

          } 
      },
      error => {
          this.service.errorserver();
      });
    }
    else{
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsSubRegion.splice(index,1);
        this.selectedItemsCountry.splice(index,1);
    }
  }
  onRegionSelectAll(items: any){
    this.arrayregionid = [];
    
    for(var i=0;i<items.length;i++){
      this.arrayregionid.push('"'+items[i].id+'"');
    }

    // SubRegion
    var data = '';
    this.service.httpClientGet("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListSubRegion = null;
        }
        else{
            this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].subregion_code,
                  itemName: result[Index].subregion_name
                }
              })
        } 
    },
    error => {
        this.service.errorserver();
    });

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListCountry = null;
        }
        else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });
  }

  onRegionDeSelectAll(items: any){
    this.dropdownListSubRegion = this.allsubregion;
    this.dropdownListCountry = this.allcountries;

    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
  }

  arraysubregionid=[];

  onSubRegionSelect(item:any){
    this.arraysubregionid.push('"'+item.id+'"');

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListCountry = null;
        }
        else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });
  }

  OnSubRegionDeSelect(item:any){

    //split countries
    let countriesArrTmp : any;
    this.service.httpClientGet("api/Countries/filtersubregion/'"+item.id+"'",data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            countriesArrTmp = null;
        }
        else{
            countriesArrTmp = result;
            for(var i=0;i<countriesArrTmp.length;i++){
                var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                if(index !== -1){
                    this.selectedItemsCountry.splice(index,1);
                }
            }
        } 
    },
    error => {
        this.service.errorserver();
    });

    var index = this.arraysubregionid.findIndex(x => x == '"'+item.id+'"');
    this.arraysubregionid.splice(index,1);
    this.selectedItemsCountry.splice(index,1);
    
    if(this.arraysubregionid.length > 0){
      // Countries
      var data = '';
      this.service.httpClientGet("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
      .subscribe(result => { 
          if(result=="Not found"){
              this.service.notfound();
              this.dropdownListCountry = null;
          }
          else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

          } 
      },
      error => {
          this.service.errorserver();
      });
    }
    else{
        this.dropdownListCountry = this.allcountries;
      
        this.selectedItemsCountry.splice(index,1);
    }
  }
  onSubRegionSelectAll(items: any){
    this.arraysubregionid = [];
    
    for(var i=0;i<items.length;i++){
      this.arraysubregionid.push('"'+items[i].id+'"');
    }

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
    .subscribe(result => { 
        if(result=="Not found"){
            this.service.notfound();
            this.dropdownListCountry = null;
        }
        else{
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
                return {
                  id: result[Index].countries_code,
                  itemName: result[Index].countries_name
                }
              })

        } 
    },
    error => {
        this.service.errorserver();
    });
  }
  onSubRegionDeSelectAll(items: any){
    this.dropdownListCountry = this.allcountries;

    this.selectedItemsCountry = [];
  }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    resetForm() {
        this.dataFound = false;
        this.selectedItemsActivity = [];
        this.selectedItemsPartner = [];
        this.selectedItemsPartnerTypeStatus = [];
        this.selectedItemsMarketType = [];
        this.selectedItemsGeo = [];
        this.dropdownListRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsTerritories = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.selectedItemsFieldOrganization = [];
        this.selectedItemsFieldSite = [];
        this.selectedItemsFieldShow2 = [];
        this.modelPopup1 = null;
        this.modelPopup2 = null;
        this.siteAccreditation = "";
    }

    searchSiteAccreditation() {
        this.loading = true;
        var dateStart: string;
        var dateEnd: string;
        var activity = [];
        var partner = [];
        var partnerStatus = [];
        var marketType = [];
        var countries = [];
        var territory = [];
        var siteField = [];
        var orgField = [];
        var anotherField = [];

        for (let i = 0; i < this.selectedItemsActivity.length; i++) {
            activity.push(this.selectedItemsActivity[i].id);
        }

        if (this.presentSetting == "Present") {
            if (this.modelPopup1 != null && this.modelPopup2 != null) {
                dateStart = this.parserFormatter.format(this.modelPopup1);
                dateEnd = this.parserFormatter.format(this.modelPopup2);
                var dateRange: any = { dateStart, dateEnd };
            } else {
                dateRange = null;
            }
        } else {
            dateRange = null;
        }

        for (let i = 0; i < this.selectedItemsPartner.length; i++) {
            partner.push(this.selectedItemsPartner[i].id);
        }
        for (let i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnerStatus.push('"' + this.selectedItemsPartnerTypeStatus[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsMarketType.length; i++) {
            marketType.push(this.selectedItemsMarketType[i].id);
        }
        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            countries.push(this.selectedItemsCountry[i].id);
        }
        for (let i = 0; i < this.selectedItemsTerritories.length; i++) {
            territory.push(this.selectedItemsTerritories[i].id);
        }
        for (let i = 0; i < this.selectedItemsFieldOrganization.length; i++) {
            orgField.push(this.selectedItemsFieldOrganization[i].id);
        }
        for (let i = 0; i < this.selectedItemsFieldSite.length; i++) {
            siteField.push(this.selectedItemsFieldSite[i].id);
        }
        for (let i = 0; i < this.selectedItemsFieldShow2.length; i++) {
            anotherField.push(this.selectedItemsFieldShow2[i].id);
        }

        var site: any;
        this.service.httpClientGet("api/MainSite/SiteAccreditationReport/{'FieldOrg':'" + orgField + "','FieldSite':'" + siteField + "','AnotherField':'" + anotherField + "','PartnerType':'" + partner +
            "','IsPresent':'" + this.presentSetting + "','ActivityId':'" + activity + "','ActivityDate':'" + [dateStart, dateEnd] + "','SiteCountryCode':'" + countries + "','PartnerStatus':'" + partnerStatus +
            "','MarketType':'" + marketType + "','Territory':'" + territory + "'}", site)
            .subscribe(res => {
                site = res;
                // console.log(site);
                this.listkey = [];
                if (site.length > 0) {
                    for (let key in site[0]) {
                        this.listkey.push(key);
                    }
                    this.siteAccreditation = site;
                    this.dataFound = true;
                    this.loading = false;
                } else {
                    this.siteAccreditation = "";
                    this.dataFound = true;
                    this.loading = false;
                }
            }, error => {
                // this.service.errorserver();
                this.siteAccreditation = "";
                this.dataFound = true;
            });
    }
}