import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SARComponent } from './site-accreditations-report.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const SAREPDBRoutes: Routes = [
    {
        path: '',
        component: SARComponent,
        data: {
            breadcrumb: 'Site Accreditations Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SAREPDBRoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [SARComponent],
    providers: [AppService, AppFilterGeo]
})
export class SAREPDBModule { }
