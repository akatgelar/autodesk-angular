import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryReportEPDBComponent } from './history-report-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { DataFilterReport } from './history-report-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";

export const HistoryReportEPDBRoutes: Routes = [
  {
    path: '',
    component: HistoryReportEPDBComponent,
    data: {
      breadcrumb: 'menu_report_epdb.history_report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HistoryReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [HistoryReportEPDBComponent, DataFilterReport],
  providers: [AppService, AppFilterGeo]
})
export class HistoryReportEPDBModule { }