import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { CustomValidators } from "ng2-validation";
import * as XLSX from 'xlsx';
import { RequestOptions } from '@angular/http';
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilter' })
export class DataFilterReport {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.OrganizationId.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-history-report-epdb',
    templateUrl: './history-report-epdb.component.html',
    styleUrls: [
        './history-report-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class HistoryReportEPDBComponent implements OnInit {

    public data: any;
    public partnertype: any;
    public partnertypestatus: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "OrganizationId";
    public sortOrder: string = "asc";
    public dataFound = false;
    searchHistory: FormGroup;
    public reportHistory: any = null;
    listkey = [];
    public loading = false;
    OrgId :any = "";
    SiteId :any = "";
    ContactId :any = "";
    fileName = "HistoryRpt.xls";
    protected dataService1: RemoteData;
    protected dataService2: RemoteData;
    protected dataService3: RemoteData;

    constructor(private completerService: CompleterService, public http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {
        let OrgId = new FormControl('');
        let SiteId = new FormControl('');
        let ContactId = new FormControl('');
        let TextToMatch = new FormControl('');
        let Admin = new FormControl('');
        let LastXDays = new FormControl(0, CustomValidators.number);
        let options = new RequestOptions({ headers: new Headers() });
        var token = this.session.getToken();
            options.headers.set("Authorization", 'Bearer '+ token);
        this.searchHistory = new FormGroup({
            OrgId: OrgId,
            SiteId: SiteId,
            ContactId: ContactId,
            TextToMatch: TextToMatch,
            Admin: Admin,
            LastXDays: LastXDays
        });

        this.dataService1 = completerService.remote(
            null,
            "Org",
            "Org");
             this.dataService1.requestOptions(options);
        this.dataService1.urlFormater(term => {
            return `api/MainOrganization/FindOrg/` + term;
        });
        this.dataService1.dataField("results");

        this.dataService2 = completerService.remote(
            null,
            "Site",
            "Site");
            this.dataService2.requestOptions(options);
        this.dataService2.urlFormater(term => {
            return `api/MainSite/FindSite/` + term;
        });
        this.dataService2.dataField("results");

        this.dataService3 = completerService.remote(
            null,
            "Contact",
            "Contact");
            this.dataService3.requestOptions(options);
        this.dataService3.urlFormater(term => {
            return `api/MainContact/FindContact/` + term;
        });
        this.dataService3.dataField("results");
    }
    ExportExceltoXls()
    {
        this.fileName = "HistoryRpt.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "HistoryRpt.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "HistoryRpt.csv";
        this.ExportExcel();
     }


    ExportExcel() {
        // var date = this.service.formatDate();
        // var fileName = "HistoryRpt.xls";
        var ws = XLSX.utils.json_to_sheet(this.reportHistory);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, this.fileName);
        XLSX.writeFile(wb, this.fileName);
    }


    ngOnInit() {
       
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.searchHistory.patchValue({ OrgId: item.idOrg });
            this.searchHistory.patchValue({ SiteId: item.idSite });
            this.searchHistory.patchValue({ ContactId: item.idContact });
            this.searchHistory.patchValue({ TextToMatch: item.textMatch });
            this.searchHistory.patchValue({ Admin: item.admin });
            this.searchHistory.patchValue({ LastXDays: item.lastDays });
            // this.onSubmit();
        }
    }

    OrgSelect(value) {
        this.searchHistory.value.OrgId = value["originalObject"].OrgId;
    }

    SiteSelect(value) {
        this.searchHistory.value.SiteId = value["originalObject"].SiteId;
    }

    ContactSelect(value) {
        this.searchHistory.value.ContactId = value["originalObject"].ContactId;
    }

    onSubmit() {
        this.searchHistory.controls['LastXDays'].markAsTouched();
        this.OrgId = this.OrgId.split('|')[0].trim();
        this.SiteId = this.SiteId.split('|')[0].trim();
        this.ContactId = this.ContactId.split('|')[0].trim();
        //this.OrgId = this.OrgId;
        //Buat object untuk filter yang dipilih
        var params =
        {
            idOrg: this.OrgId,
            idSite: this.SiteId,
            idContact: this.ContactId,
            TextToMatch: this.searchHistory.value.TextToMatch,
            admin: this.searchHistory.value.Admin,
            lastDays: this.searchHistory.value.LastXDays

        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        if (this.searchHistory.valid) {
            this.loading = true;
        var keyword =
        {
            OrgId: this.OrgId,
            SiteId: this.SiteId,
            ContactId: this.ContactId,
            TextToMatch: this.searchHistory.value.TextToMatch,
            Admin: this.searchHistory.value.Admin,
            LastXDays: this.searchHistory.value.LastXDays

        };
        
            // "api/ReportEPDB/ReportHistory/{"+
            // "'OrgId':'"+this.searchHistory.value.OrgId+"',"+
            // "'SiteId':'"+this.searchHistory.value.SiteId+"',"+
            // "'ContactId':'"+this.searchHistory.value.ContactId+"',"+
            // "'TextToMacth':'"+this.searchHistory.value.TextToMacth+"',"+
            // "'Admin':'"+this.searchHistory.value.Admin+"',"+
            // "'LastXDays':'"+this.searchHistory.value.LastXDays+"'"+
            // "}"

            /* new post for autodesk plan 18 oct */
            
            this.service.httpClientPost("api/ReportEPDB/ReportHistory", keyword)
                .subscribe(res => {
                    // var finalres = res.replace(/\r\n/g, "<br/>");
                    // var finalres = res.replace(/\n/g, "<br/>");
                    // this.reportHistory = JSON.parse(finalres);
                    this.reportHistory = res;
                    this.loading = false;
                }, error => {
                    this.reportHistory = "";
                    this.loading = false;
                });

            /* new post for autodesk plan 18 oct */
        }
    }

    resetForm() {
        this.searchHistory.reset();
    }

}