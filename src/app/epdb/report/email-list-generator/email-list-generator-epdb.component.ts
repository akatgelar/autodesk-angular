import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'app-email-list-generator-epdb',
    templateUrl: './email-list-generator-epdb.component.html',
    styleUrls: [
        './email-list-generator-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class EmailListGeneratorEPDBComponent implements OnInit {

    dropdownListPartner = [];
    selectedItemsPartner = [];

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];

    dropdownListContactRole = [];
    selectedItemsContactRole = [];

    dropdownListIndustry = [];
    selectedItemsIndustry = [];

    dropdownListLanguage = [];
    selectedItemsLanguage = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownSettings = {};
    dropdownCountrySettings = {};

    public data: any;
    public partnertype: any;
    public partnertypestatus: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "ContactName";
    public sortOrder: string = "asc";
    public dataFound = false;
    byLastLogin = [];
    emailGenerator: FormGroup;
    public loading = false;
    public emailRes;

    constructor(public http: Http, private service: AppService, private filterGeo: AppFilterGeo) {
        this.byLastLogin = [
            { val: 1, name: "1 day" },
            { val: 2, name: "2 days" },
            { val: 3, name: "3 days" },
            { val: 7, name: "1 week" },
            { val: 14, name: "2 weeks" },
            { val: 30, name: "30 days" },
            { val: 60, name: "60 days" },
            { val: 90, name: "90 days" },
            { val: 120, name: "120 days" },
            { val: 182, name: "182 days" },
            { val: 364, name: "1 year" },
            { val: 728, name: "2 years" },
            // { val: 0, name: "Ever" }
        ];

        let searchBySiteName = new FormControl('');
        let searchByContactName = new FormControl('');
        let byLastLogin = new FormControl('');
        let logAfterMarch = new FormControl('');

        this.emailGenerator = new FormGroup({
            searchBySiteName: searchBySiteName,
            searchByContactName: searchByContactName,
            byLastLogin: byLastLogin,
            logAfterMarch: logAfterMarch
        });
    }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    getPartnerType() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Roles", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListPartner = null;
                }
                else {
                    this.dropdownListPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsPartner = [];
        this.getPartnerTypeStatus();
    }

    getPartnerTypeStatus() {
        var data: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'SiteStatus','Status':'A'}", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsPartnerTypeStatus = [];
    }

    getContactRole() {
        var data: any;
        this.service.httpClientGet("api/Roles", data)
            .subscribe(res => {
                data = res;
                this.dropdownListContactRole = data.map((item) => {
                    return {
                        id: item.RoleId,
                        itemName: item.RoleName
                    }
                });
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsContactRole = [];
    }

    getIndustry() {
        var data: any;
        this.service.httpClientGet("api/Industries", data)
            .subscribe(res => {
                data = res;
                this.dropdownListIndustry = data.map((item) => {
                    return {
                        id: item.IndustryCode,
                        itemName: item.Industry
                    }
                });
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsIndustry = [];
    }

    getLanguage() {
        var data: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", data)
            .subscribe(res => {
                data = res;
                this.dropdownListLanguage = data.sort(this.compare).map((item) => {
                    return {
                        id: item.Key,
                        itemName: item.KeyValue
                    }
                });
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsLanguage = [];
    }

    getGeo() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Geo", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_id,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    ngOnInit() {
        this.getPartnerType();
        this.getGeo();
        this.getContactRole();
        this.getIndustry();
        this.getLanguage();

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };

        this.dropdownCountrySettings = {
            singleSelection: true,
            text: "Please Select",
            enableSearchFilter: true,
            enableCheckAll: false,
            // limitSelection: 1,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    onGeoSelect(item: any) {
        this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    OnGeoDeSelect(item: any) {
        this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onGeoSelectAll(items: any) {
        this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onGeoDeSelectAll(items: any) {
        this.selectedItemsRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListRegion = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onRegionSelect(item: any) {
        this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    OnRegionDeSelect(item: any) {
        this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onRegionSelectAll(items: any) {
        this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onRegionDeSelectAll(items: any) {
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
        this.dropdownListSubRegion = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onSubRegionSelect(item: any) {
        this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    OnSubRegionDeSelect(item: any) {
        this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onSubRegionSelectAll(items: any) {
        this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
        this.selectedItemsCountry = [];
        this.dataFound = false;
    }

    onSubRegionDeSelectAll(items: any) {
        this.selectedItemsCountry = [];
        this.dropdownListCountry = [];
        this.dataFound = false;
    }

    onCountriesSelect(item: any) {
        this.dataFound = false;
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
    }

    onCountriesSelectAll(items: any) {
        this.dataFound = false;
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
    }

    resetForm() {
        this.emailGenerator.reset();
        this.dataFound = false;
        this.selectedItemsPartner = [];
        this.selectedItemsPartnerTypeStatus = [];
        this.selectedItemsGeo = [];
        this.dropdownListRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
    }

    onSubmit() {
        this.loading = true;
        var partner = [];
        var partnerStatus = [];
        var contactRole = [];
        var industry = [];
        var language;
        var countries = [];
        var lastLogin;
        var logAfterMarch;

        var filterSiteName: string = "";
        var filterContactName: string = "";

        if (this.emailGenerator.value.searchBySiteName != "" && this.emailGenerator.value.searchBySiteName != null) {
            filterSiteName = this.emailGenerator.value.searchBySiteName;
        } else {
            filterSiteName = "";
        }

        if (this.emailGenerator.value.searchByContactName != "" && this.emailGenerator.value.searchByContactName != null) {
            filterContactName = this.emailGenerator.value.searchByContactName;
        } else {
            filterContactName = "";
        }

        if (this.emailGenerator.value.logAfterMarch != "" && this.emailGenerator.value.logAfterMarch != null) {
            logAfterMarch = this.emailGenerator.value.logAfterMarch;
        } else {
            logAfterMarch = "";
        }

        if (this.emailGenerator.value.byLastLogin != "" && this.emailGenerator.value.byLastLogin != null) {
            lastLogin = this.emailGenerator.value.byLastLogin;
        } else {
            lastLogin = "";
        }

        this.selectedItemsLanguage.length == 1 ? language = this.selectedItemsLanguage[0].id : language = "";

        for (let i = 0; i < this.selectedItemsPartner.length; i++) {
            partner.push(this.selectedItemsPartner[i].id);
        }
        for (let i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnerStatus.push('"' + this.selectedItemsPartnerTypeStatus[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsContactRole.length; i++) {
            contactRole.push(this.selectedItemsContactRole[i].id);
        }
        for (let i = 0; i < this.selectedItemsIndustry.length; i++) {
            industry.push(this.selectedItemsIndustry[i].id);
        }
        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            countries.push(this.selectedItemsCountry[i].id);
        }

        let email: any;
        this.service.httpClientGet("api/ContactAll/EmailGenerator/{'SiteName':'" + filterSiteName + "','ContactName':'" + filterContactName + "','LastLogin':'" + lastLogin +
            "','LogAfterMarch':'" + logAfterMarch + "','PartnerType':'" + partner + "','PartnerStatus':'" + partnerStatus + "','ContactRole':'" + contactRole + "','Industry':'" + industry +
            "','Language':'" + language + "','CountryCode':'" + countries + "'}", email)
            .subscribe(res => {
                email = res;
                // console.log(email);
                if (email.length > 0) {
                    this.emailRes = email;
                    this.dataFound = true;
                    this.loading = false;
                } else {
                    this.emailRes = "";
                    this.dataFound = true;
                    this.loading = false;
                }
            }, error => {
                this.emailRes = "";
                this.dataFound = true;
                this.loading = false;
            });
    }
}