import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailListGeneratorEPDBComponent } from './email-list-generator-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

export const EmailListGeneratorEPDBRoutes: Routes = [
  {
    path: '',
    component: EmailListGeneratorEPDBComponent,
    data: {
      breadcrumb: 'Email List Generator ',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EmailListGeneratorEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [EmailListGeneratorEPDBComponent],
  providers: [AppService, AppFilterGeo]
})
export class EmailListGeneratorEPDBModule { }