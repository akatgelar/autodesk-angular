import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceReportNewComponent } from './invoice-report-new.component';

describe('InvoiceReportNewComponent', () => {
  let component: InvoiceReportNewComponent;
  let fixture: ComponentFixture<InvoiceReportNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceReportNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceReportNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
