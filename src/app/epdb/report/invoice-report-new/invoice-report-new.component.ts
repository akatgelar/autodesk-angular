import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from '@angular/forms';
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        }
    }
}

@Component({
  selector: 'app-invoice-report-new',
  templateUrl: './invoice-report-new.component.html',
  styleUrls: ['./invoice-report-new.component.css']
})

export class InvoiceReportNewComponent implements OnInit {
    totalamount = null
    filterByInvoiceStatus = '';
    ORGReport = 'SKU';
    dropdownListCheckInvoice = [];
    selectedItemsCheckInvoice = [];
    orgTotals = [];
    dropdownListPartner = [];
    selectedItemsPartner = [];
    
    dropdownListFiscalYear = [];
    selectedItemsFiscalYear = [];
    
    dropdownListStatus = [];
    selectedItemsStatus = [];

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListFieldOrganization = [];
    selectedItemsFieldOrganization = [];

    dropdownListFieldInvoice = [];
    selectedItemsFieldInvoice = [];

    dropdownListFieldShow2 = [];
    selectedItemsFieldShow2 = [];

    dropdownSettings = {};

    public data: any;
    public partnertype: any;
    public partnertypestatus: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "OrganizationId";
    public sortOrder: string = "asc";
    public dataFound = false;
    searchInvoice: FormGroup;
    public loading = false;

    public datayearcurrency: any;

    //Validation Select 2
    public showValidMarketType = false;
    public showValidPartnerType = false;
    public showValidPartnerStatus = false;
    //  public showValidTerritory = false;
    //  public showValidGeo = false
    public showValidCountry = false;
    fileName = "feemanagement.xls";
    useraccesdata: any;

    constructor(private _http: Http, private session: SessionService, private service: AppService, private filterGeo: AppFilterGeo, private formatdate: AppFormatDate) {

        let invoiceNumber = new FormControl('');
        let FinancialYear = new FormControl('');
        let filterByInvoiceStatus = new FormControl('');
        let ORGReport = new FormControl('SKU');
        this.searchInvoice = new FormGroup({
            invoiceNumber: invoiceNumber,
            FinancialYear: FinancialYear,
            filterByInvoiceStatus: filterByInvoiceStatus,
            ORGReport: ORGReport
        });

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        this.dropdownListStatus = [
            {
                id: 'X', itemName: 'Paid'
            },
            {
                id: 'A', itemName: 'Pending'
            },
            {
                id: 'C', itemName: 'Cancelled'
            }
        ];
        var dataParse : any = this.dropdownListStatus;
        for (let i = 0; i < dataParse.length; i++) {
            this.selectedItemsStatus.push({ 'id': dataParse[i].id, 'itemName': dataParse[i].itemName });
        }
    }

    resetForm() {
        this.searchInvoice.reset();
        this.dataFound = false;
        this.dropdownListCheckInvoice = [];
        this.selectedItemsCheckInvoice = [];
        this.dropdownListPartner = [];
        this.selectedItemsPartner = [];
        this.dropdownListFiscalYear = [];
        this.selectedItemsFiscalYear=[];
        this.selectedItemsStatus = [];
        this.dropdownListStatus = [];
        this.dropdownListPartnerTypeStatus = [];
        this.selectedItemsPartnerTypeStatus = [];
        this.dropdownListMarketType = [];
        this.selectedItemsMarketType = [];
        this.dropdownListTerritories = [];
        this.selectedItemsTerritories = [];
        this.dropdownListGeo = [];
        this.selectedItemsGeo = [];
        this.dropdownListRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry = [];
        this.selectedItemsCountry = [];
        this.dropdownListFieldOrganization = [];
        this.selectedItemsFieldOrganization = [];
        this.dropdownListFieldInvoice = [];
        this.selectedItemsFieldInvoice = [];
        this.dropdownListFieldShow2 = [];
        this.selectedItemsFieldShow2 = [];
    }

    stopquery() {
        this.reportInvoice = "";
        this.reportInvoiceOrg = "";
        this.listkey = null;
    }

    modelPopup1: any = null;
    modelPopup2: any = null;
    poreciveddatedate: boolean = false;
    present() {
        this.poreciveddatedate = true;
    }

    notpresent() {
        this.poreciveddatedate = false;
        this.modelPopup1 = null;
        this.modelPopup2 = null;
    }

    adminya: Boolean = true;
    checkrole() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya: Boolean = false;
    itsinstructor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya: Boolean = false;
    itsOrganization() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya: Boolean = false;
    itsSite() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya: Boolean = false;
    itsDistributor() {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    reportInvoice: any = "";
    reportInvoiceOrg: any = "";
    reportInvoiceSite:any = "";
    listkey: any;
    onSubmit() {
        let invoices: any;

        let partnertypearr = [];
        for (var i = 0; i < this.selectedItemsPartner.length; i++) {
            partnertypearr.push("'" + this.selectedItemsPartner[i].id + "'");
        }

        let fiscalyeararr = [];
        for (var i = 0; i < this.selectedItemsFiscalYear.length; i++) {
            fiscalyeararr.push("'" + this.selectedItemsFiscalYear[i].id + "'");
        }
        
        let statusarr = [];
        for (var i = 0; i < this.selectedItemsStatus.length; i++) {
            statusarr.push("'" + this.selectedItemsStatus[i].id + "'");
        }

        let partnertypestatusarr = [];
        for (var i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnertypestatusarr.push("'" + this.selectedItemsPartnerTypeStatus[i].id + "'");
        }

        let markettypearr = [];
        for (var i = 0; i < this.selectedItemsMarketType.length; i++) {
            markettypearr.push("'" + this.selectedItemsMarketType[i].id + "'");
        }

        let geoarr = [];
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push("'" + this.selectedItemsGeo[i].id + "'");
        }

        let regionarr = [];
        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push("'" + this.selectedItemsRegion[i].id + "'");
        }

        let subregionarr = [];
        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push("'" + this.selectedItemsSubRegion[i].id + "'");
        }

        let countriesarr = [];
        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countriesarr.push("'" + this.selectedItemsCountry[i].id + "'");
        }

        let territoryarr = [];
        for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
            territoryarr.push("'" + this.selectedItemsTerritories[i].id + "'");
        }

        let anotherfieldarr = [];
        for (var i = 0; i < this.selectedItemsFieldShow2.length; i++) {
            anotherfieldarr.push(this.selectedItemsFieldShow2[i].id);
        }

        let orgfieldarr = [];
        for (var i = 0; i < this.selectedItemsFieldOrganization.length; i++) {
            orgfieldarr.push(this.selectedItemsFieldOrganization[i].id);
        }

        let invoicefieldarr = [];
        for (var i = 0; i < this.selectedItemsFieldInvoice.length; i++) {
            invoicefieldarr.push(this.selectedItemsFieldInvoice[i].id);
        }

        let startdate = "";
        let enddate = "";
        if(this.modelPopup1){
            startdate = this.formatdate.dateCalendarToYMD(this.modelPopup1);
        }
        if(this.modelPopup2){
            enddate = this.formatdate.dateCalendarToYMD(this.modelPopup2);
        }
        var params =
        {
            invoiceNumber: this.searchInvoice.value.invoiceNumber,
            Year: this.dropdownListFiscalYear,//this.searchInvoice.value.FinancialYear,
            ORGReport: this.searchInvoice.value.ORGReport,
            selectStatus: this.dropdownListStatus,
            selectPartner: this.selectedItemsPartner,
            dropdownPartner: this.dropdownListPartner,
            selectPartnerStatus: this.selectedItemsPartnerTypeStatus,
            dropdownPartnerStatus: this.dropdownListPartnerTypeStatus,
            selectMarketType: this.selectedItemsMarketType,
            dropdownMarketType: this.dropdownListMarketType,
            selectTerritory: this.selectedItemsTerritories,
            dropdownTerritory: this.dropdownListTerritories,
            selectGeo: this.selectedItemsGeo,
            dropdownGeo: this.dropdownListGeo,
            selectCountry: this.selectedItemsCountry,
            dropdownCountry: this.dropdownListCountry,
            selectFieldOrg: this.selectedItemsFieldOrganization,
            dropdownFieldOrg: this.dropdownListFieldOrganization,
            selectFieldShow: this.selectedItemsFieldShow2,
            dropdownFieldShow: this.dropdownListFieldShow2,
            selectFieldInvoice: this.selectedItemsFieldInvoice,
            dropdownFieldInvoice: this.dropdownListFieldInvoice
        }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        if (
            this.selectedItemsPartner.length == 0 ||
            this.selectedItemsMarketType.length == 0 ||
            // this.selectedItemsTerritories.length == 0 || 
            // this.selectedItemsGeo.length == 0 ||
            this.selectedItemsCountry.length == 0 ||
            this.selectedItemsPartnerTypeStatus.length == 0

        ) {
            swal(
                'Field is Required!',
                'Please enter the Invoice Report form required!',
                'error'
            )

            if (this.selectedItemsPartner.length == 0) {
                this.showValidPartnerType = true;
            } else {
                this.showValidPartnerType = false;
            }
            if (this.selectedItemsMarketType.length == 0) {
                this.showValidMarketType = true;
            } else {
                this.showValidMarketType = false;
            }
            if (this.selectedItemsCountry.length == 0) {
                this.showValidCountry = true;
            } else {
                this.showValidCountry = false;
            }
            if (this.selectedItemsPartnerTypeStatus.length == 0) {
                this.showValidPartnerStatus = true
            } else {
                this.showValidPartnerStatus = false
            }

        }
        else {
            this.loading = true;
            this.showValidPartnerType = false;
            this.showValidMarketType = false;
            this.showValidCountry = false;
            this.showValidPartnerStatus = false

            var orgid = "";
            var role = "";
            if (this.adminya) {
                orgid = this.urlGetOrgId();
                if (this.distributorya) {
                    role = "DISTRIBUTOR";
                } else {
                    role = "ORGANIZATION";
                }
            }

            var keyword = {
                fieldOrg: orgfieldarr.toString(),
                anotherField: anotherfieldarr.toString(),
                invoicesField: invoicefieldarr.toString(),
                filterByInvoicesNumber: this.searchInvoice.value.invoiceNumber,
                filterByPOReceivedDate: startdate + "," + enddate,
                filterFromDate: startdate?new Date(startdate):'',
                filterToDate: enddate?new Date(enddate):'',
                filterByInvoiceStatus: statusarr.toString(),//this.searchInvoice.value.filterByInvoiceStatus,
                filterByPartnerType: partnertypearr.toString(),
                filterByPartnerTypeStatus: partnertypestatusarr.toString(),
                filterByMarketType: markettypearr.toString(),
                filterByGeo: geoarr.toString(),
                filterByRegion: regionarr.toString(),
                filterBySubRegion: subregionarr.toString(),
                filterByTerritory: territoryarr.toString(),
                filterByCountries: countriesarr.toString(),
                FinancialYear: fiscalyeararr.toString(),//this.searchInvoice.value.FinancialYear,
                ORGReport: this.searchInvoice.value.ORGReport,
                OrgId: orgid, //fix issue excel no 263.Show data based role
                Role: role //fix issue excel no 263.Show data based role
            };

            this.reportInvoice = null;
            this.reportInvoiceOrg = null;
            this.reportInvoiceSite = null;
            this.listkey = null;
            /* new post for autodesk plan 18 oct */
            this.totalamount = null;
            this.service.httpClientPost("api/MainInvoices/InvoiceReporting", keyword)
                .subscribe(res => {
                    var resOb = JSON.stringify(res);
                    invoices = JSON.parse(resOb).WebInfo;
                    
                    this.listkey = [];
                    if (invoices && invoices.length > 0) {
                        for (let key in invoices[0]) {
                            this.listkey.push(key);
                        }
                        this.reportInvoice = invoices;
                        this.reportInvoiceOrg = JSON.parse(resOb).ExcelOrgInfo;
                        this.reportInvoiceSite = JSON.parse(resOb).ExcelSiteInfo;
                        this.dataFound = true;
                        this.loading = false;
                    } else {
                        this.reportInvoice = "";
                        this.reportInvoiceOrg = "";
                        this.dataFound = true;
                        this.loading = false;
                    }
                }, error => {
                    this.data = true;
                    this.loading = false;
                });
            /* new post for autodesk plan 18 oct */
        }

    }

    preselected(value) {
        if (value == "1") {
            this.selectedItemsFieldOrganization =
                [
                    {
                        id: "o.OrgName",
                        itemName: "OrgName"
                    }
                ];
            this.selectedItemsFieldInvoice =
                [
                    {
                        id: "i.InvoiceId",
                        itemName: "Invoice Id"
                    },
                    {
                        id: "i.TotalPaymentAmount",
                        itemName: "Total Payment Amount"
                    },
                ];
        }
        else {
            this.selectedItemsFieldOrganization =
                [
                    {
                        id: "o.OrgName",
                        itemName: "OrgName"
                    },
                    {
                        id: "o.TaxExempt",
                        itemName: "TaxExempt"
                    },
                    {
                        id: "o.VATNumber",
                        itemName: "VATNumber"
                    },
                    {
                        id: "o.InvoicingDepartment",
                        itemName: "InvoicingDepartment"
                    },
                    {
                        id: "o.InvoicingAddress1",
                        itemName: "InvoicingAddress1"
                    },
                    {
                        id: "o.InvoicingAddress3",
                        itemName: "InvoicingAddress3"
                    },
                    {
                        id: "o.InvoicingCity",
                        itemName: "InvoicingCity"
                    },
                    {
                        id: "o.InvoicingStateProvince",
                        itemName: "InvoicingStateProvince"
                    },
                    {
                        id: "o.InvoicingPostalCode",
                        itemName: "InvoicingPostalCode"
                    },
                    {
                        id: "o.InvoicingCountryCode",
                        itemName: "InvoicingCountryCode"
                    },
                    {
                        id: "o.BillingContactFirstName",
                        itemName: "BillingContactFirstName"
                    },
                    {
                        id: "o.BillingContactLastName",
                        itemName: "BillingContactLastName"
                    },
                    {
                        id: "o.BillingContactEmailAddress",
                        itemName: "BillingContactEmailAddress"
                    },
                    {
                        id: "o.BillingContactTelephone",
                        itemName: "BillingContactTelephone"
                    }
                ];
            this.selectedItemsFieldInvoice =
                [
                    {
                        id: "i.InvoiceId",
                        itemName: "Invoice Id"
                    },
                    {
                        id: "i.InvoiceNumber",
                        itemName: "Invoice Number"
                    },
                    {
                        id: "i.InvoiceDescription",
                        itemName: "Invoice Description"
                    },
                    {
                        id: "i.TotalInvoicedAmount",
                        itemName: "Total Invoiced Amount"
                    },
                    {
                        id: "i.InvoicedCurrency",
                        itemName: "Invoiced Currency"
                    },
                    {
                        id: "i.TotalPaymentAmount",
                        itemName: "Total Payment Amount"
                    }
                ];
        }
    }

    getSiteStatusLabel(id){
        let result = this.dropdownListPartnerTypeStatus.find((el)=>el.id==id)
        return result?result.itemName:''
    }

    getOrgDiscountAmount(item){
        let discount = item.Discount
        if(item.AmountType=='Exactly')
            discount = discount+' $'
        else
            discount = ''
        return discount
    }

    getOrgDiscountPercent(item){
        let discount = item.Discount
        if(item.AmountType=='Percentage')
            discount = discount+' %'
        else
            discount = ''
        return discount
    }

    getTotalDiscountOrg(item){
        let res = item.Discount
        if(item.AmountType=='Percentage')
            res = item.TotalInvoiceAmmount * (item.Discount / 100)
        else
            res = item.Discount

        return res
    }

    ExportExceltoXls()
    {
        this.fileName = "feemanagement.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "feemanagement.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "feemanagement.csv";
        this.ExportExcel();
     }

    ExportExcel() {  
       // var fileName = "feemanagement.xls";
        let excel_data: any;
        let excel_data_site: any;
        
        excel_data = this.reportInvoiceOrg.map((item)=>{
            return {
                OrgId: item.OrgId,
                OrgName: item.OrgName,
                //OrgStatus: item.OrgStatus,
                OrgCountry: item.OrgCountry,
                Territory: item.Territory,
                ReferenceNo:item.ReferenceNo,
                OrgTotal: item.OrgTotal,//getTotal(item),//item.TotalAmount,//this.getOrgTotal(item.OrgId),
                DiscountPercentage: item.DiscountPercentage,
                Discount$: item.Discount,
                OrgTotalDiscount: item.OrgTotalDiscount,
                InvoiceStatus: item.InvoiceStatus,  
                FY: item.FY,
                TotalLicenseCount: item.TotalLicenseCount,//Number(item.InvoiceLicense) * item.QTY,
                CreatedID: item.CreatedID,
                CreatedDate: item.CreatedDate,
                PaymentReceiveDate: item.PaymentReceiveDate,
                ApproverID: item.ApproverID,
                ApproverDate: item.ApproverDate,
                RejectedID: item.RejectedID,
                RejectedDate: item.RejectedDate,
                RejectReason: item.RejectReason
            }
        });
        excel_data_site = this.reportInvoiceSite.map((item)=>{
            return {
                OrgId: item.SiteOrgId,
                SiteName: item.SiteName,
                SiteId: item.SiteId,
                PartnerType: item.PartnerType,
                SiteCountry: item.SiteCountry,
                SiteStatus: item.SiteStatus,
                SiteTerritory: item.SiteTerritory,
                SKUDescription: item.SKUDescription,
                SKUName: item.SKUName,
                SKUQTYASPerSetup: item.SKUQTYASPerSetup,
                SKUFY: item.SKUFY,
                SKUValue: item.SKUValue,
                SKUQTY: item.SKUQTY,
                SKULicenseCount: item.SKULicenseCount,
                proratedYN: item.ProratedYN,             
                //siteTotal: this.getSiteTotalAmount(item.OrgId,item.SiteId),
            }
        });

        var result = [];
        excel_data_site.reduce(function(res, value) {
        if (!res[value.OrgId]) {
            res[value.OrgId] = { OrgId: value.OrgId, SiteName: 'Total', SKUValue: 0, SKUQTY: 0, SKULicenseCount: 0 };
            //if (excel_data_site.filter(x=>x.OrgId == value.OrgId).length > 1)
                excel_data_site.push(res[value.OrgId])
        }
        res[value.OrgId].SKUValue += Number(value.SKUValue);
        res[value.OrgId].SKUQTY += Number(value.SKUQTY);
        res[value.OrgId].SKULicenseCount += Number(value.SKULicenseCount);
        return res;
        }, {});

        excel_data_site.sort(function(a,b){
            // var aCat = a.OrgId + a.SKUValue;
            // var bCat = b.OrgId + b.SKUValue;
            // return (aCat > bCat ? 1 : aCat < bCat ? -1 : 0);
            var o1 = a.OrgId.toLowerCase();
            var o2 = b.OrgId.toLowerCase();

            var p1 = a.SKUValue;
            var p2 = b.SKUValue;

            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
         });
    
        let partnertypearr = [];
        for (var i = 0; i < this.selectedItemsPartner.length; i++) {
            partnertypearr.push(this.selectedItemsPartner[i].itemName);
        }

        let fiscalyeararr = [];
        for (var i = 0; i < this.selectedItemsFiscalYear.length; i++) {
            fiscalyeararr.push(this.selectedItemsFiscalYear[i].itemName);
        }
        
        let statusarr = [];
        for (var i = 0; i < this.selectedItemsStatus.length; i++) {
            statusarr.push(this.selectedItemsStatus[i].itemName);
        }

        let partnertypestatusarr = [];
        for (var i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnertypestatusarr.push(this.selectedItemsPartnerTypeStatus[i].itemName);
        }

        let markettypearr = [];
        for (var i = 0; i < this.selectedItemsMarketType.length; i++) {
            markettypearr.push(this.selectedItemsMarketType[i].itemName);
        }

        let geoarr = [];
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push(this.selectedItemsGeo[i].itemName);
        }

        let countriesarr = [];
        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countriesarr.push(this.selectedItemsCountry[i].itemName);
        }

        let territoryarr = [];
        for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
            territoryarr.push(this.selectedItemsTerritories[i].itemName);
        }

        let startdate = "";
        let enddate = "";
        if(this.modelPopup1){
            startdate = this.formatdate.dateCalendarToYMD(this.modelPopup1);
        }
        if(this.modelPopup2){
            enddate = this.formatdate.dateCalendarToYMD(this.modelPopup2);
        }
        
        let getInvoiceStatus = (status)=>{
            let result = 'All';
            if(status=='X')
                result = 'Paid';
            else if(status == 'A')
                result = 'Pending';
            else if(status == 'C')
                result = 'Cancelled';
            return result
        } 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //As January is 0.
        var yyyy = today.getFullYear();

        var currentDate = yyyy + '-' + mm + '-' + dd;
                   
        var ws_org = XLSX.utils.aoa_to_sheet([
            ["Report generated on:", currentDate],
            ["By:", this.useraccesdata.ContactName, "", "", "", "", "", "", "", "", "", "Invoice status", statusarr.toString()],
            ["Filter", "Reference no.", this.searchInvoice.value.invoiceNumber, "", "", "Fiscal Year", fiscalyeararr.toString()],
            ["", "From", startdate?new Date(startdate):'', "", "", "To", enddate?new Date(enddate):'', "", "", "", "", "Market Type", markettypearr.toString()],
            ["", "Partner Type:", partnertypearr.toString(), "", "", "Partner Status:", partnertypestatusarr.toString(), "", "", "", "", "Country", countriesarr.toString()],
            ["", "Territory", territoryarr.toString(), "", "", "Geo", geoarr.toString()]
          ]);
        var header = ["ORG ID", "ORG Name", "ORG Country", "Territory", "Reference No.", "Total SRP $amount on the ORG for that reference no.", "Discount (%) applied", 
                    "Discount ($$$) applied", "Total discounted $$amount", "Invoice Status", "FY", "Total license count on the ORG for that reference no.", "Created ID", 
                    "Created Date", "Approver ID", "Approver Date", "Rejected ID", "Rejected Date"];
        XLSX.utils.sheet_add_json(ws_org, excel_data, { origin:"A10"});

        //var ws_org = XLSX.utils.json_to_sheet(excel_data);
        var ws_site = XLSX.utils.json_to_sheet(excel_data_site);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws_org, 'Org Info');
        XLSX.utils.book_append_sheet(wb, ws_site, 'Site Info');
        XLSX.writeFile(wb, this.fileName);
    }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    getPartnerType() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Roles/where/{'RoleType': 'Company'}", data)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListPartner = null;
                }
                else {
                    this.dropdownListPartner = result.sort((a,b)=>{
                        if(a.RoleName > b.RoleName)
                            return 1
                        else if(a.RoleName < b.RoleName)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.RoleId,
                          itemName: el.RoleName
                        }
                      })
                    var dataParse : any = result;
                    for (let i = 0; i < dataParse.length; i++) {
                        this.selectedItemsPartner.push({ 'id': dataParse[i].RoleId, 'itemName': dataParse[i].RoleName });
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsPartner = [];
        this.getPartnerTypeStatus();
    }

    getPartnerTypeStatus() {
        var data: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'SiteStatus','Status':'A'}", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.selectedItemsPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsPartnerTypeStatus = [];
        this.getMarketType();
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                            this.selectedItemsMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsMarketType = [];
    }

    territorydefault: string = "";
    getTerritory() {
        this.loading = true;
        var url = "api/Territory/SelectAdmin";

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'Distributor'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteId'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteId";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            Distributor: DistributorGeo,
            SiteId: siteRes,
            OrgId: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);        
        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.adminya) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    getGeo() { 
        var url = "api/Geo/SelectAdmin";

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                   
                    this.dropdownListGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                    this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    getOrgFields() {
        var data: any;
        this.service.httpClientGet("api/MainOrganization/getColumns", data)
            .subscribe(res => {
                data = res;
                var exclude = ['OrganizationId','ATCOrgId','AUPCSN','RegisteredDepartment','InvoicingDepartment','ContractDepartment','CSOResellerUUID','CSOResellerName','CSOPartnerManager','CSOUpdatedOn','CSOVersion','OrgStatus_retired','SAPSoldTo','Status']
                if (data.length > 0) {
                    
                    // this.dropdownListFieldOrganization = data.map((item) => {
                    //     return {
                    //         id: "o." + item.Field,
                    //         itemName: item.Field
                    //     }
                    // });
                    for (let i = 0; i < data.length; i++) {
                        if (exclude.indexOf(data[i].Field) == -1) {}
                        var DataListField = {
                            id: "o." + data[i].Field,
                            itemName: data[i].Field
                          }
                          this.dropdownListFieldOrganization.push(DataListField)
                          if (data[i].Field == "OrgId" || data[i].Field == "OrgName" || data[i].Field == "RegisteredCountryCode" || data[i].Field == "ATCDirectorEmailAddress") {
                            this.selectedItemsFieldOrganization.push({ 'id': "o." + data[i].Field, 'itemName': data[i].Field });
                        }
                        // if (data[i].Field != "OrganizationId" && data[i].Field != "ATCOrgId" && data[i].Field != "AUPCSN"  && data[i].Field != "RegisteredDepartment" && data[i].Field != "InvoicingDepartment" && data[i].Field != "ContractDepartment" && data[i].Field != "CSOResellerUUID" && data[i].Field != "CSOResellerName" && data[i].Field != "CSOPartnerManager" && data[i].Field != "CSOUpdatedOn" && data[i].Field != "CSOVersion" &&  data[i].Field != "Status") {
                        //     var DataListField = {
                        //     id: "o." + data[i].Field,
                        //     itemName: data[i].Field
                        //   }
                        //   this.dropdownListFieldOrganization.push(DataListField)
                        // }
                        
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsFieldOrganization = [];
    }

    getInvoicesFields() {
        // var field = [
        //     { "id": "ORG ID", "itemName": "ORG ID" },
        //     { "id": "ORG Name", "itemName": "ORG Name" },
        //     { "id": "ORG Country", "itemName": "ORG Country" },
        //     { "id": "Territory", "itemName": "Territory" },
        //     { "id": "Reference No", "itemName": "Reference No." },
        //     { "id": "Total SRP amount", "itemName": "Total SRP $amount on the ORG for that reference no." },
        //     { "id": "Discount percent applied", "itemName": "Discount (%) applied" },
        //     { "id": "Discount amount applied", "itemName": "Discount ($$$) applied" },
        //     { "id": "Total discounted amount", "itemName": "Total discounted $$amount" },
        //     { "id": "Invoice status", "itemName": "Invoice status" },
        //     { "id": "FY", "itemName": "FY" },
        //     { "id": "Total license count", "itemName": "Total license count on the ORG for that reference no." },
        //     { "id": "Created ID", "itemName": "Created ID" },
        //     { "id": "Created Date", "itemName": "Created Date" },
        //     { "id": "Approver ID", "itemName": "Approver ID" },
        //     { "id": "Approver Date", "itemName": "Approver Date" },
        //     { "id": "Rejected ID", "itemName": "Rejected ID" },
        //     { "id": "Rejected Date", "itemName": "Rejected Date" },
        //     { "id": "Reject Reason", "itemName": "Reject Reason" }
        // ];
        var field = [
            { "id": "Territory name", "itemName": "Territory name" },
            { "id": "ORG ID", "itemName": "ORG ID" },
            { "id": "ORG Name", "itemName": "ORG Name" },
            { "id": "ORG Status", "itemName": "ORG Status" },
            { "id": "Site ID", "itemName": "Site ID" },
            { "id": "Site Name", "itemName": "Site Name" },
            { "id": "Partner Type", "itemName": "Partner Type" },
            { "id": "SKU Name", "itemName": "SKU Name" },
            { "id": "Quantity", "itemName": "Quantity" },
            { "id": "Site Status", "itemName": "Site Status" },
            { "id": "SKU Price Band", "itemName": "SKU Price Band" },
            { "id": "Country", "itemName": "Country" },
            { "id": "SKU $ SRP", "itemName": "SKU $ SRP" },
            { "id": "Invoice Status", "itemName": "Invoice Status" },
            { "id": "Discount", "itemName": "Discount" },
            { "id": "site total $$", "itemName": "site total $$" },
            { "id": "ORG total $$", "itemName": "ORG total $$" },
            { "id": "ORG total $$ (Incl. Discount)", "itemName": "ORG total $$ (Incl. Discount)" },
            { "id": "Valid Agreement in place", "itemName": "Valid Agreement in place" },
            { "id": "Date Payment Received", "itemName": "Date Payment Received" },
            { "id": "Description", "itemName": "Description" },
            { "id": "Last Admin", "itemName": "Last Admin" },
            { "id": "Autodesk Approver", "itemName": "Autodesk Approver" }
        ];

        for (let n = 0; n < field.length; n++) {
            this.dropdownListFieldInvoice.push(field[n]);
        }

        this.selectedItemsFieldInvoice = field;
    }

    displayField(id){
        return this.selectedItemsFieldInvoice.find((el)=>el.id==id)
    }

    getAnotherFields() {
        var field = [
            { "id": "m.MarketType", "itemName": "Market Type" },
            { "id": "r.RoleName AS PartnerType", "itemName": "Partner Type" },
            { "id": "sr.Status", "itemName": "Partner Type Status" },
            { "id": "sr.CSN", "itemName": "Partner Type CSN" },
            { "id": "sr.ITS_ID", "itemName": "ITS Site ID" },
            { "id": "sr.UParent_CSN", "itemName": "Ultimate Parent CSN" },
            { "id": "rg.geo_name", "itemName": "Geo" },
            { "id": "rr.region_name", "itemName": "Region" },
            { "id": "rsr.subregion_name", "itemName": "SubRegion" },
            { "id": "t.Territory_Name", "itemName": "Territory" },
            { "id": "ja.ActivityName", "itemName": "Organization Journal Entries" },
            { "id": "j.ActivityDate", "itemName": "Organization Journal Entries Date" },
            { "id": "sku.Currency", "itemName": "Currency" }
        ];
        for (let n = 0; n < field.length; n++) {
            this.dropdownListFieldShow2.push(field[n]);
        }
        this.selectedItemsFieldShow2 = [];
    }

    allregion = [];
    allsubregion = [];
    allcountries = [];
    ngOnInit() {

        let that = this 
        $(document).keypress(function (e) {
            if (e.which == 13) {
                that.onSubmit()
            }
        });
        /* call function get user level id (issue31082018)*/

        this.checkrole()
        this.itsinstructor()
        this.itsOrganization()
        this.itsSite()
        this.itsDistributor()

        /* call function get user level id (issue31082018)*/

        this.loading = true;
        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.searchInvoice.patchValue({ invoiceNumber: item.invoiceNumber });
            //this.searchInvoice.patchValue({ FinancialYear: item.Year });
            this.dropdownListFiscalYear = item.Year;
            this.ORGReport = item.ORGReport;
            this.dropdownListStatus = item.selectStatus;
            this.selectedItemsPartner = item.selectPartner;
            this.dropdownListPartner = item.dropdownPartner;
            this.selectedItemsPartnerTypeStatus = item.selectPartnerStatus;
            this.dropdownListPartnerTypeStatus = item.dropdownPartnerStatus;
            this.selectedItemsMarketType = item.selectMarketType;
            this.dropdownListMarketType = item.dropdownMarketType;
            this.selectedItemsTerritories = item.selectTerritory;
            this.dropdownListTerritories = item.dropdownTerritory;
            this.selectedItemsGeo = item.selectGeo;
            this.dropdownListGeo = item.dropdownGeo;
            this.selectedItemsCountry = item.selectCountry;
            this.dropdownListCountry = item.dropdownCountry;
            this.selectedItemsFieldOrganization = item.selectFieldOrg;
            this.dropdownListFieldOrganization = item.dropdownFieldOrg;
            this.selectedItemsFieldShow2 = item.selectFieldShow;
            this.dropdownListFieldShow2 = item.dropdownFieldShow;
            this.selectedItemsFieldInvoice = item.selectFieldInvoice;
            this.dropdownListFieldInvoice = item.dropdownFieldInvoice;
            this.onSubmit();
        }
        else {

            this.getPartnerType();
            this.getTerritory();
            this.getGeo();
            this.getOrgFields();
            this.getAnotherFields();
            this.getInvoicesFields();

            var url2 = "api/Countries/SelectAdmin";

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.orgya) {
            //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             if (this.siteya) {
            //                 url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //             } else {
            //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //             }
            //         }
            //     }
            // }
            var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistributorCountry = this.urlGetOrgId();
                     url2 = "api/Territory/wherenew/'DistributorCountry'";
                } else {
                    if (this.orgya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                    } else {
                        if (this.siteya) {
                            OnlyCountryOrgId = this.urlGetOrgId();
                            url2 = "api/Territory/wherenew/'OnlyCountryOrgId";
                        } else {
                            CountryOrgId= this.urlGetOrgId();
                            url2 = "api/Territory/wherenew/'CountryOrgId'";
                        }
                    }
                }
                
            }
            var keyword : any = {
                DistributorCountry: DistributorCountry,
                OnlyCountryOrgId: OnlyCountryOrgId,
                CountryOrgId: CountryOrgId,
               
            };
            keyword = JSON.stringify(keyword);
            
            var data = '';
            this.service.httpClientPost(url2, keyword)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                    this.allcountries = this.dropdownListCountry;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsCountry = [];

        }

        data = '';
        this.service.httpClientGet('api/Currency/where/{"Parent":"FYIndicator","Status":"A"}', data)
            .subscribe((result:any) => {
                this.dropdownListFiscalYear = result.sort((a,b)=>{
                    if(a.KeyValue > b.KeyValue)
                        return 1
                    else if(a.KeyValue < b.KeyValue)
                        return -1
                    else return 0
                }).map(function (el) {
                    return {
                      id: el.KeyValue,
                      itemName: el.KeyValue
                    }
                  });
                //this.searchInvoice.patchValue({ FinancialYear: this.datayearcurrency[this.datayearcurrency.length - 1].Key });
                
                var dataParse : any = result;
                for (let i = 0; i < dataParse.length; i++) {
                    this.selectedItemsFiscalYear.push({ 'id': dataParse[i].KeyValue, 'itemName': dataParse[i].KeyValue });
                }

                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
    }
    onSelectAll(items: any) {
        this.dataFound = false;
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
    }

    arraygeoid = [];
    arrayterritory = [];

    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        var data = '';

        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            var data = '';

            var url = "api/Countries/filterByGeoByTerritory";

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //     } else {
            //         if (this.orgya) {
            //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //         } else {
            //             if (this.siteya) {
            //             url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //         } else {
            //             url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //         }
            //     }
            // }
            var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistTerr = this.urlGetOrgId();
                   url = "api/Countries/DistTerr";
                } else {
                    if (this.orgya) {
                        OrgSiteTerr = this.urlGetOrgId();
                        url = "api/Countries/OrgSiteTerr";
                    } else {
                        if (this.siteya) {
                            OnlyCountryOrgId = this.urlGetOrgId();
                            url = "api/Territory/wherenew/'OnlyCountryOrgId";
                        } else {
                            CountryOrgId= this.urlGetOrgId();
                            url = "api/Territory/wherenew/'CountryOrgId'";
                        }
                    }
                }
                
            }
            var keyword : any = {
                CtmpGeo:tmpGeo,
                CDistTerr:DistTerr,
                COrgSiteTerr:OrgSiteTerr,
                OnlyCountryOrgId: OnlyCountryOrgId,
                CountryOrgId: CountryOrgId,
                CtmpTerritory:tmpTerritory
               
            };
            keyword = JSON.stringify(keyword);
            
            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        var data = '';

        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        
        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        var data = '';
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        
        this.service.httpClientPost(url, keyword)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory"; 
        
        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';
        /* populate data issue role distributor */
       // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        
        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        
        //this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)
        this.service.httpClientPost(url, keyword) 
        .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                    console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {

        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp = result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = this.allsubregion;
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })
                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        this.showValidCountry = false;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }
    OnCountriesDeSelect(item: any) { }
    onCountriesSelectAll(items: any) {
        this.showValidCountry = false;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }
    onCountriesDeSelectAll(items: any) { }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    getOrgTotal(orgId){
        let data = this.orgTotals.find((el)=>el.OrgId == orgId)
        if(data)
            return data.Total
        return null
    }

    getOrgTotalDiscount(orgId){
        let data = this.orgTotals.find((el)=>el.OrgId == orgId)
        if(data)
            return data.TotalWithDiscount
        return null
    }

    getSiteTotalAmount(orgId,siteId){
        let data = this.orgTotals.find((el)=>el.OrgId == orgId)
        if(data){
            let site_data = data.SiteTotal.find((el)=>el.SiteId == siteId)
            if(site_data)
                return site_data.Total
        }
        return null
    }

    getOrgNumberOfLicense(orgId){
        let data = this.orgTotals.find((el)=>el.OrgId == orgId)
        if(data)
            return data.TotalLicense
        return null
    }
}
