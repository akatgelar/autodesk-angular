import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceReportNewComponent, ReplacePipe } from './invoice-report-new.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const InvoiceReportEPDBRoutes: Routes = [
    {
        path: '',
        component: InvoiceReportNewComponent,
        data: {
            breadcrumb: 'Fee Management Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(InvoiceReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule,
      LoadingModule
    ],
    declarations: [InvoiceReportNewComponent,ReplacePipe],
    providers: [AppService, AppFilterGeo,AppFormatDate]
  })
  export class InvoiceReportNewEPDBModule { }