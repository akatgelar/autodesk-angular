import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SitesReportEPDBComponent } from './sites-report-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { LoadingModule } from 'ngx-loading';

export const SitesReportEPDBRoutes: Routes = [
  {
    path: '',
    component: SitesReportEPDBComponent,
    data: {
      breadcrumb: 'menu_report_epdb.site_report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SitesReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [SitesReportEPDBComponent],
  providers: [AppService, AppFilterGeo]
})
export class SitesReportEPDBModule { }