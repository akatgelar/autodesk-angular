import { Component, OnInit, ViewEncapsulation, AnimationKeyframesSequenceMetadata } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'app-sites-report-epdb',
    templateUrl: './sites-report-epdb.component.html',
    styleUrls: [
        './sites-report-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SitesReportEPDBComponent implements OnInit {

    apiCall:any
    dropdownListPartner = [];
    selectedItemsPartner = [];

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];

    dropdownListMarketType = [];
    selectedItemsMarketType = [];

    dropdownListSubPartnerType = [];
    selectedItemsSubPartnerType = [];

    dropdownListGeo = [];
    selectedItemsGeo = [];

    dropdownListRegion = [];
    selectedItemsRegion = [];

    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    dropdownListCountry = [];
    selectedItemsCountry = [];

    dropdownListFieldOrganization = [];
    selectedItemsFieldOrganization = [];

    dropdownListFieldSite = [];
    selectedItemsFieldSite = [];

    dropdownListFieldShow2 = [];
    selectedItemsFieldShow2 = [];

    dropdownListcity=[];
    selectedItemscity=[];

    dropdownListstate=[];
    selectedItemsstate=[];

    dropdownSettings = {};

    public data: any;
    public partnertype: any;
    public partnertypestatus: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "";
    public sortOrder: string = "asc";
    public dataFound = false;
    searchSite: FormGroup;
    listkey = [];
    public reportSite;
    public loading = false;


    //Validation Select 2
    public showValidMarketType = false;
    public showValidPartnerType = false;
    public showValidPartnerStatus = false;
    // public showValidTerritory = false;
    // public showValidGeo = false;
    public showValidCountry = false;
    public showValidFieldSite = false;
    fileName = "SitesReport.xls";
    useraccesdata: any;

    constructor(private _http: Http, private service: AppService, private filterGeo: AppFilterGeo, private session: SessionService) {
        let filterByName = new FormControl('');

        this.searchSite = new FormGroup({
            filterByName: filterByName
        });

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const valueA = a.KeyValue.toUpperCase();
        const valueB = b.KeyValue.toUpperCase();

        let comparison = 0;
        if (valueA > valueB) {
            comparison = 1;
        } else if (valueA < valueB) {
            comparison = -1;
        }
        return comparison;
    }

    ExportExceltoXls()
    {
        this.fileName = "SitesReport.xls";
        this.ExportExcel();
     }
     ExportExceltoXlSX()
     {
        this.fileName = "SitesReport.xlsx";
        this.ExportExcel();
     }
     ExportExceltoCSV()
     {
        this.fileName = "SitesReport.csv";
        this.ExportExcel();
     }


    ExportExcel() {
       // var fileName = "SitesReport.xls";
        var ws = XLSX.utils.json_to_sheet(this.reportSite);
        // var ws = XLSX.utils.json_to_sheet(this.reportSite.map((el)=>{
        //     let obj = {}
        //     Object.keys(el).map((key)=>{
        //         if(this.selectedItemsFieldSite.find((e)=>e.itemName==key) || this.selectedItemsFieldShow2.find((e)=>e.itemName==key) || this.selectedItemsFieldOrganization.find((e)=>e.itemName==key))
        //             obj[key] = el[key]
        //     })
        //     return obj
        // }));
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, this.fileName);
        XLSX.writeFile(wb, this.fileName);
    }

    getSubPartnerType() {
        var data: any;
        this.service.httpClientGet("api/RoleParams/getParamValue", data)
            .subscribe(res => {
                data = res;
                this.dropdownListSubPartnerType = data.map((item) => {
                    return {
                        //id: item.RoleParamId,
						id: item.ParamValue,
                        itemName: item.ParamValue
                    }
                });
                this.selectedItemsSubPartnerType = data.map((item) => {
                    return {
                        //id: item.RoleParamId,
						id: item.ParamValue,
                        itemName: item.ParamValue
                    }
                });
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsSubPartnerType = [];
    }

    getPartnerType() {
        // get Geo
        var data = '';
        this.service.httpClientGet("api/Roles/where/{'RoleType': 'Company'}", data)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListPartner = null;
                }
                else {
                    this.dropdownListPartner = result.sort((a,b)=>{
                        if(a.RoleName > b.RoleName)
                            return 1
                        else if(a.RoleName < b.RoleName)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.RoleId,
                          itemName: el.RoleName
                        }
                      })
 this.selectedItemsPartner = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RoleId,
                          itemName: result[Index].RoleName
                        }
                      })



                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsPartner = [];
        this.getPartnerTypeStatus();
        this.getSubPartnerType();
    }

    getPartnerTypeStatus() {
        var data: any;
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'SiteStatus','Status':'A'}", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    this.dropdownListPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                    this.selectedItemsPartnerTypeStatus = data.sort(this.compare).map((item) => {
                        return {
                            id: item.Key,
                            itemName: item.KeyValue
                        }
                    });
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsPartnerTypeStatus = [];
        this.getMarketType();
    }

    getMarketType() {
        var data: any;
        this.service.httpClientGet("api/MarketType", data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].MarketTypeId != 1) {
                            var market = {
                                'id': data[i].MarketTypeId,
                                'itemName': data[i].MarketType
                            };
                            this.dropdownListMarketType.push(market);
                            this.selectedItemsMarketType.push(market);
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsMarketType = [];
    }

    getGeo() { /* Reset dropdown country if user select geo that doesn't belong to -> geo show based role */
        // get Geo
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Geo";
        // } else {
        //     url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        // }

        var url = "api/Geo/SelectAdmin";
        
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if(this.distributorya){
        //         url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
        //     }else{
        //         if(this.orgya){
        //             url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //         }else{
        //             if(this.siteya){
        //                 url = "api/Territory/where/{'SiteIdGeo':'" + this.urlGetSiteId() + "'}";
        //             }else{
        //                 url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'DistributorGeo'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteIdGeo'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteIdGeo";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
            }
            
        }
        var keyword : any = {
            DistributorGeo: DistributorGeo,
            SiteIdGeo: siteRes,
            OrgIdGeo: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data = '';
        this.service.httpClientPost(url, keyword)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListGeo = null;
                }
                else {
                    this.dropdownListGeo = result.sort((a,b)=>{
                        if(a.geo_name > b.geo_name)
                            return 1
                        else if(a.geo_name < b.geo_name)
                            return -1
                        else return 0
                    }).map(function (el) {
                        return {
                          id: el.geo_code,
                          itemName: el.geo_name
                        }
                      })
                this.selectedItemsGeo = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].geo_code,
                          itemName: result[Index].geo_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
        this.selectedItemsGeo = [];
    }

    territorydefault: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        this.loading = true;
        var url = "api/Territory/SelectAdmin";
        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }

        /* populate data issue role distributor */

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'SiteId':'" + this.urlGetSiteId() + "'}";
        //             } else {
        //                 url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistributorGeo = null; var siteRes = null;var OrgIdGeo = null;
        if (this.adminya) {

            if (this.distributorya) {
                 DistributorGeo = this.urlGetOrgId();
               url = "api/Territory/wherenew/'Distributor'";
            } else {
                if (this.orgya) {
                    siteRes = this.urlGetSiteId();
                    url = "api/Territory/wherenew/'SiteId'";
                } else {
                    if (this.siteya) {
                        siteRes = this.urlGetSiteId();
                        url = "api/Territory/wherenew/'SiteId";
                    } else {
                        OrgIdGeo= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            Distributor: DistributorGeo,
            SiteId: siteRes,
            OrgId: OrgIdGeo,
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }

                    if (this.adminya) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                    
                }
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });
        this.selectedItemsTerritories = [];
    }

    getOrgFields() {
        var data: any;
        this.service.httpClientGet("api/MainOrganization/getColumns", data)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Field > b.Field)
                        return 1
                    else if(a.Field < b.Field)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    // this.dropdownListFieldOrganization = data.map((item) => {
                    //     return {
                    //         id: "o." + item.Field,
                    //         itemName: item.Field
                    //     }
                    // });
                    for (let l = 0; l < data.length; l++) {
                      
                        if (data[l].Field == "OrganizationId" || data[l].Field == "ATCOrgId" || data[l].Field == "AUPCSN"  || data[l].Field == "RegisteredDepartment" || data[l].Field == "InvoicingDepartment" || data[l].Field == "ContractDepartment" || data[l].Field == "CSOResellerUUID" || data[l].Field == "CSOResellerName" || data[l].Field == "CSOPartnerManager" || data[l].Field == "CSOUpdatedOn" || data[l].Field == "CSOVersion" || data[l].Field =="OrgStatus_retired" || data[l].Field =="Status" ) {}
                        else{
                            var DataListField = {
                                id: "o." + data[l].Field,
                                itemName: data[l].Field
                              }
                              this.dropdownListFieldOrganization.push(DataListField)
                        }
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsFieldOrganization = [];
    }

    getSiteFields() {
        var data: any;
        this.service.httpClientGet("api/MainSite/getColumns", data)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Field > b.Field)
                        return 1
                    else if(a.Field <b.Field)
                        return -1
                    else return 0
                });
                var exclude = ['SiteIdInt','Status','DateAdded','AddedBy','CSOLocationUUID','CSOLocationName','CSOLocationNumber','CSOAuthorizationCodes','CSOUpdatedOn','CSOVersion','CSOpartner_type','ATCSiteId','SiebelSiteName','SiebelDepartment','SiebelAddress1','SiebelAddress2','SiebelAddress3','SiebelCity','SiebelStateProvince','SiebelCountryCode','SiebelPostalCode','MagellanId','SAPNumber_retired','SAPShipTo_retired']
                if (data.length > 0) {
                  for (let i = 0; i < data.length; i++) {
                    if (exclude.indexOf(data[i].Field) == -1) {
                        if (data[i].Field == "ParamValue") {
                            this.dropdownListFieldSite.push({ 'id': "rop." + data[i].Field, 'itemName': 'SubPartnerType' });
                        }
                        else if (data[i].Field == "SubPartnerTypeStatus") {
                            this.dropdownListFieldSite.push({ 'id': data[i].Field, 'itemName': data[i].Field });
                        }
                        else if (data[i].Field == "SiteCountryCode") {
                            this.dropdownListFieldSite.push({ 'id': 'ss.SiteCountryName', 'itemName': 'SiteCountryName' });
                        }
                        else{
                            this.dropdownListFieldSite.push({ 'id': "ss." +  data[i].Field, itemName: data[i].Field });
                        }
                        if (data[i].Field == "SiteId" || data[i].Field == "SiteName" || data[i].Field == "SiteCountryName" || data[i].Field == "LastAdminBy") {
                            this.selectedItemsFieldSite.push({ 'id': "ss." + data[i].Field, 'itemName': data[i].Field });
                        }

                    }   


                    // if (data[i].Field == "SiteIdInt" || data[i].Field == "ATCSiteId" || data[i].Field == "SiebelSiteName" || data[i].Field == "MagellanId" || data[i].Field == "SAPNumber_retired" || data[i].Field == "SAPShipTo_retired" || data[i].Field == "SiebelAddress1" || data[i].Field == "SiebelAddress2" || data[i].Field == "SiebelAddress3" || data[i].Field == "SiebelCity" || data[i].Field == "SiebelStateProvince" || data[i].Field == "SiebelCountryCode" || data[i].Field == "SiebelPostalCode" || data[i].Field == "CSOLocationUUID" || data[i].Field == "CSOLocationName"  || data[i].Field == "CSOLocationNumber" || data[i].Field == "CSOAuthorizationCodes" || data[i].Field == "CSOUpdatedOn" || data[i].Field == "CSOVersion" || data[i].Field == "CSOpartner_type" || data[i].Field == "SiebelDepartment" || data[i].Field == "Status") {}
                    // else{
                    //   if (data[i].Field == "ParamValue") {
                    //     this.dropdownListFieldSite.push({ 'id': "rop." + data[i].Field, 'itemName': 'SubPartnerType' });
                    //   }
                    //   else if (data[i].Field == "SubPartnerTypeStatus") {
                    //     this.dropdownListFieldSite.push({ 'id': data[i].Field, 'itemName': data[i].Field });
                    //   }
                    //   else {

                    //       this.dropdownListFieldSite.push({ 'id': "ss." + data[i].Field, 'itemName': data[i].Field });
                    //   }
                    // }
                    // if (data[i].Field == "SiteCountryCode")
                    // {
                    //     data[i].Field ="SiteCountryName";
                    //     var FieldSiteFilterSiteCountryCode = {
                    //         id: "ss." + data[i].Field,
                    //         itemName: data[i].Field
                    //       }
                    //     this.dropdownListFieldSite.push(FieldSiteFilterSiteCountryCode)
                    // }
                    // if (data[i].Field == "SiteId" || data[i].Field == "SiteName" || data[i].Field == "SiteCountryName" || data[i].Field == "LastAdminBy") {
                    //   this.selectedItemsFieldSite.push({ 'id': "ss." + data[i].Field, 'itemName': data[i].Field });
                    // }
                    
                  }


                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsFieldSite = [];
    }

    getAnotherFields() {
        var field = [
            { "id": "m.MarketType", "itemName": "Market Type" },
            { "id": "r.RoleName", "itemName": "Partner Type" },
            { "id": "sr.Status", "itemName": "Partner Type Status" },
            { "id": "sr.CSN", "itemName": "Partner Type CSN" },
           // { "id": "sr.ITS_ID", "itemName": "ITS Site ID" },
            { "id": "sr.UParent_CSN", "itemName": "Ultimate Parent CSN" },
            { "id": "tc.geo_name", "itemName": "Geo" },
            { "id": "tc.region_name", "itemName": "Region" },
            { "id": "tc.subregion_name", "itemName": "SubRegion" },
            { "id": "t.Territory_Name", "itemName": "Territory" },
        ].sort((a,b)=>{
            if(a.itemName > b.itemName)
                return 1
            else if(a.itemName < b.itemName)
                return -1
            else return 0
        });
        for (let n = 0; n < field.length; n++) {
            this.dropdownListFieldShow2.push(field[n]);
        }
        this.selectedItemsFieldShow2 = [];
    }

    allregion = [];
    allsubregion = [];
    allcountries = [];
    ngOnInit() {

        let that = this 
        $(document).keypress(function (e) {
            if (e.which == 13) {
                that.onSubmit()
            }
        });
         /* call function get user level id (issue31082018)*/

         this.checkrole()
         this.itsinstructor()
         this.itsOrganization()
         this.itsSite()
         this.itsDistributor()
 
         /* call function get user level id (issue31082018)*/

        this.loading = true;
        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.searchSite.patchValue({ filterByName: item.filterName });
            this.selectedItemsPartner = item.itemPartner;
            this.dropdownListPartner = item.dropdownPartner;
            this.selectedItemsPartnerTypeStatus = item.PartnerStatus;
            this.dropdownListPartnerTypeStatus = item.dropdownStatus;
            this.selectedItemsMarketType = item.marketType;
            this.dropdownListMarketType = item.dropdownMarketType;
            this.selectedItemsSubPartnerType = item.itemSubPartner;
            this.dropdownListSubPartnerType = item.dropdownSubPartner;
            this.selectedItemsGeo = item.geo;
            this.dropdownListGeo = item.dropdownGeo;
            this.selectedItemsTerritories = item.dropdownTerritory;
            this.dropdownListTerritories = item.dropdownTerritory;
            this.selectedItemsCountry = item.country;
            this.dropdownListCountry = item.dropdownCountry;
            this.selectedItemsFieldOrganization = item.fieldOrg;
            this.dropdownListFieldOrganization = item.dropdownFieldOrg;
            this.selectedItemsFieldSite = item.itemFieldSite;
            this.dropdownListFieldSite = item.dropdownFieldSite;
            this.selectedItemsFieldShow2 = item.fieldShow;
            this.dropdownListFieldShow2 = item.dropdownFieldShow;
            this.onSubmit();
        }
        else {

            this.getSubPartnerType();
            this.getPartnerType();
            this.getGeo();
            this.getTerritory();
            this.getOrgFields();
            this.getSiteFields();
            this.getAnotherFields();

            // Region
            /*var data = '';
            this.service.httpClientGet("api/Region", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListRegion = null;
                    }
                    else {
                        this.dropdownListRegion = JSON.parse(result).map((item) => {
                            return {
                                id: item.region_code,
                                itemName: item.region_name
                            }
                        })
                    }
                    this.allregion = this.dropdownListRegion;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsRegion = [];*/

            // Sub Region
            /*data = '';
            this.service.httpClientGet("api/SubRegion", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = JSON.parse(result).map((item) => {
                            return {
                                id: item.subregion_code,
                                itemName: item.subregion_name
                            }
                        })
                    }
                    this.allsubregion = this.dropdownListSubRegion;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsSubRegion = [];*/

            // Countries
            /* populate data issue role distributor */
            var url2 = "api/Countries/SelectAdmin";

            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.orgya) {
            //             url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             if (this.siteya) {
            //                 url2 = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //             } else {
            //                 url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //             }
            //         }
            //     }
            // }
            var  DistributorCountry = null; var OnlyCountryOrgId = null;var CountryOrgId = null;
            if (this.adminya) {
    
                if (this.distributorya) {
                    DistributorCountry = this.urlGetOrgId();
                     url2 = "api/Territory/wherenew/'DistributorCountry'";
                } else {
                    if (this.orgya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'OnlyCountryOrgId'";
                    } else {
                        if (this.siteya) {
                            OnlyCountryOrgId = this.urlGetOrgId();
                            url2 = "api/Territory/wherenew/'OnlyCountryOrgId";
                        } else {
                            CountryOrgId= this.urlGetOrgId();
                            url2 = "api/Territory/wherenew/'CountryOrgId'";
                        }
                    }
                }
                
            }
            var keyword : any = {
                DistributorCountry: DistributorCountry,
                OnlyCountryOrgId: OnlyCountryOrgId,
                CountryOrgId: CountryOrgId,
               
            };
            keyword = JSON.stringify(keyword);
            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            var data = '';
            this.service.httpClientPost(url2, keyword)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                    this.allcountries = this.dropdownListCountry;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
            this.selectedItemsCountry = [];

        }

        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelectPartner(item) {
        this.dataFound = false;
        // alert(item);
        this.onPartnerSelect(item);
    }
    OnItemDeSelectPartner(item) {
        this.dataFound = false;
        this.onPartnerSelect(item);
    }
    onSelectAllPartner(items) {
        this.dataFound = false;
        this.onPartnerSelect(items);
    }
    onDeSelectAllPartner(items) {
        this.dataFound = false;
        this.onPartnerSelect(items);
    }

    onItemSelect(item: any) {
        this.dataFound = false;
    
    }
    OnItemDeSelect(item: any) {
        this.dataFound = false;
       
    }
    onSelectAll(items: any) {
        this.dataFound = false;
   
    }
    onDeSelectAll(items: any) {
        this.dataFound = false;
       
    }

    // onGeoSelect(item: any) {
    //     this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // OnGeoDeSelect(item: any) {
    //     this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoSelectAll(items: any) {
    //     this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    //     this.selectedItemsRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.selectedItemsRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onGeoDeSelectAll(items: any) {
    //     this.selectedItemsRegion = [];
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListRegion = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelect(item: any) {
    //     this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnRegionDeSelect(item: any) {
    //     this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionSelectAll(items: any) {
    //     this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    //     this.selectedItemsSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onRegionDeSelectAll(items: any) {
    //     this.selectedItemsSubRegion = [];
    //     this.selectedItemsCountry = [];
    //     this.dropdownListSubRegion = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // OnSubRegionDeSelect(item: any) {
    //     this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionSelectAll(items: any) {
    //     this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    //     this.selectedItemsCountry = [];
    //     this.dataFound = false;
    // }

    // onSubRegionDeSelectAll(items: any) {
    //     this.selectedItemsCountry = [];
    //     this.dropdownListCountry = [];
    //     this.dataFound = false;
    // }

    arraygeoid = [];
    arrayterritory = [];
    arraypartnertype = [];
    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onPartnerSelect(item) {
        

        this.arraypartnertype = [];
        if (this.selectedItemsPartner.length > 0) {
            for (var i = 0; i < this.selectedItemsPartner.length; i++) {
                this.arraypartnertype.push('"' + this.selectedItemsPartner[i].id + '"');
            }
        }


        var url = "api/Roles/whererolesub/{RoleCode:'" + this.arraypartnertype + "'}" ;      
  // subpartner type
  var data = '';
  this.service.httpClientGet(url, data)
      .subscribe((result:any) => {
          if (result == "Not found") {
              this.service.notfound();
              this.dropdownListSubPartnerType = null;
              this.loading = false;
          }
          else {
              this.dropdownListSubPartnerType = result.sort((a,b)=>{
                if(a.RoleName > b.RoleName)
                    return 1
                else if(a.RoleName < b.RoleName)
                    return -1
                else return 0
              }).map(function (el) {
                  return {
                    id: el.RoleName,
                    itemName: el.RoleName
                  }
                })
             this.selectedItemsSubPartnerType = result.map(function (el) {
                  return {
                    id: el.RoleName,
                    itemName: el.RoleName
                  }
                })
          }
      },
          error => {
              this.service.errorserver();
          });
  this.selectedItemsSubPartnerType = [];




    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';

            /* populate data issue role distributor */
            //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
            var url = "api/Countries/filterByGeoByTerritory";
            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            // if (this.adminya) {
            //     if (this.distributorya) {
            //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //     } else {
            //         if (this.orgya) {
            //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            //         } else {
            //             if (this.siteya) {
            //             url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
            //         } else {
            //             url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //         }
            //     }
            // }
            var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        // Countries
        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSiteTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistTerr = null; var OrgSiteTerr = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            } else {
                if (this.orgya) {
                    OrgSiteTerr = this.urlGetOrgId();
                    url = "api/Countries/OrgSiteTerr";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            COrgSiteTerr:OrgSiteTerr,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })


                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';
        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }

        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

        // if (this.adminya) {
        //     if (this.distributorya) {
        //         url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        //     } else {
        //         if (this.orgya) {
        //             url = "api/Countries/OrgSite/" + tmpGeo + "/" + this.urlGetOrgId();
        //         } else {
        //             if (this.siteya) {
        //                 url = "api/Territory/where/{'OnlyCountryOrgId':'" + this.urlGetOrgId() + "'}"; 
        //             } else {
        //                 url = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
        //             }
        //         }
        //     }
        // }
        var  DistGeo = null; var OrgSite = null;var OnlyCountryOrgId = null;var CountryOrgId = null;
        if (this.adminya) {

            if (this.distributorya) {
                DistGeo = this.urlGetOrgId();
               url = "api/Countries/DistGeo";
            } else {
                if (this.orgya) {
                    OrgSite = this.urlGetOrgId();
                    url = "api/Countries/OrgSite";
                } else {
                    if (this.siteya) {
                        OnlyCountryOrgId = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'OnlyCountryOrgId";
                    } else {
                        CountryOrgId= this.urlGetOrgId();
                        url = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
            }
            
        }
        var keyword : any = {
            CtmpGeo: tmpGeo,
            CDistGeo:DistGeo,
            COrgSite:OrgSite,
            OnlyCountryOrgId: OnlyCountryOrgId,
            CountryOrgId: CountryOrgId,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
        /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

       // this.service.httpClientGet("api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory, data)/* populate data issue role distributor */
       this.service.httpClientPost(url, keyword)
       .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                    // console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {

        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp = result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = this.allsubregion;
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsCountry = [];
    }
    getCity(value){
        this.selectedItemscity = [];
       
        var url = "api/Countries/whererpt";
       
        var keyword : any = {
            Countrycode:value.toString(), 
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword) 
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.selectedItemscity = null;
                }
                else {
                    this.dropdownListcity = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].RegisteredCity,
                          itemName: result[Index].RegisteredCity
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
                this.selectedItemscity= [];
    }
    getState(value){
        this.selectedItemsstate = [];
       
        var url = "api/States/whererpt";
       
        var keyword : any = {
            Countrycode:value.toString(), 
        };
        keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword) 
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.selectedItemsstate = null;
                }
                else {
                    this.dropdownListstate = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].StateCode,
                          itemName: result[Index].StateCode
                        }
                      })
                }
            },
                error => {
                    this.service.errorserver();
                });
                this.selectedItemsstate=[];
    }
    arrayCountry =[];
    onCountriesSelect(item: any) {
        // this.dataFound = false;
        this.showValidCountry = false;
        let country_temp: any;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }

        this.arrayCountry = [];
        if (this.selectedItemsCountry.length > 0) {
            for (var i = 0; i < this.selectedItemsCountry.length; i++) {
                this.arrayCountry.push('"' + this.selectedItemsCountry[i].id + '"');
            }
        }

        this.getCity(this.arrayCountry);
        this.getState(this.arrayCountry);
    }

    OnCountriesDeSelect(item: any) {
        this.dataFound = false;
        this.dropdownListcity =[];
        this.dropdownListstate =[];
    }

    onCountriesSelectAll(items: any) {
        this.showValidCountry = false;
        // this.dataFound = false;
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
        this.arrayCountry = [];
        if (this.selectedItemsCountry.length > 0) {
            for (var i = 0; i < this.selectedItemsCountry.length; i++) {
                this.arrayCountry.push('"' + this.selectedItemsCountry[i].id + '"');
            }
        }

        this.getCity(this.arrayCountry);
        this.getState(this.arrayCountry);
    }

    onCountriesDeSelectAll(items: any) {
        this.dataFound = false;
        this.dropdownListcity =[];
        this.dropdownListstate =[];
    }

    resetForm() {
        this.searchSite.reset();
        this.dataFound = false;
        this.selectedItemsPartner = [];
        this.selectedItemsSubPartnerType = [];
        this.selectedItemsPartnerTypeStatus = [];
        this.selectedItemsMarketType = [];
        this.selectedItemsGeo = [];
        this.dropdownListRegion = [];
        this.selectedItemsRegion = [];
        this.dropdownListSubRegion = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsTerritories = [];
        this.dropdownListCountry = [];
        this.dropdownListcity = [];
        this.dropdownListstate = [];
        this.selectedItemsCountry = [];
        this.selectedItemsFieldOrganization = [];
        this.selectedItemsFieldSite = [];
        this.selectedItemsFieldShow2 = [];
        this.itsRollup ="";
    }

    /*fix issue excel no 263.Show data based role*/

    /* populate data issue role distributor */
    // checkrole(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // }

    // itsinstructor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "TRAINER") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // itsDistributor(): boolean {
    //     let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    //     for (var i = 0; i < userlvlarr.length; i++) {
    //         if (userlvlarr[i] == "DISTRIBUTOR") {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    // }

    // urlGetOrgId(): string {
    //     var orgarr = this.useraccesdata.OrgId.split(',');
    //     var orgnew = [];
    //     for (var i = 0; i < orgarr.length; i++) {
    //         orgnew.push('"' + orgarr[i] + '"');
    //     }
    //     return orgnew.toString();
    // }

    // urlGetSiteId(): string {
    //     var sitearr = this.useraccesdata.SiteId.split(',');
    //     var sitenew = [];
    //     for (var i = 0; i < sitearr.length; i++) {
    //         sitenew.push('"' + sitearr[i] + '"');
    //     }
    //     return sitenew.toString();
    // }
    /* populate data issue role distributor */

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    adminya:Boolean=true;
    checkrole(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                this.adminya = false;
            }
        }
    }

    trainerya:Boolean=false;
    itsinstructor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                this.trainerya = true;
            }
        }
    }

    orgya:Boolean=false;
    itsOrganization(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ORGANIZATION") {
                this.orgya = true;
            }
        }
    }

    siteya:Boolean=false;
    itsSite(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "SITE") {
                this.siteya = true;
            }
        }
    }

    distributorya:Boolean=false;
    itsDistributor(){
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                this.distributorya = true;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }

    /* ganti logic (detect user level), nu kamari salah, hampura pisan (issue31082018)*/

    /*end fix issue excel no 263.Show data based role*/

    onSubmit() {
        var partner = [];
        var partnerStatus = [];
        var marketType = [];
        var subPartnerType = [];
        var geo = [];
        var region = [];
        var subregion = [];
        var countries = [];
        var territory = [];
        var orgField = [];
        var siteField = [];
        var anotherField = [];
        var cities =[];
        var states = [];

        var filterSiteName: string = "";
        if (this.searchSite.value.filterByName != "" && this.searchSite.value.filterByName != null) {
            filterSiteName = this.searchSite.value.filterByName;
        } else {
            filterSiteName = "";
        }

        for (let i = 0; i < this.selectedItemsPartner.length; i++) {
            partner.push('"' + this.selectedItemsPartner[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnerStatus.push('"' + this.selectedItemsPartnerTypeStatus[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsMarketType.length; i++) {
            marketType.push('"' + this.selectedItemsMarketType[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsSubPartnerType.length; i++) {
            subPartnerType.push(this.selectedItemsSubPartnerType[i].id);
        }
        for (let i = 0; i < this.selectedItemsGeo.length; i++) {
            geo.push('"' + this.selectedItemsGeo[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsRegion.length; i++) {
            region.push('"' + this.selectedItemsRegion[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregion.push('"' + this.selectedItemsSubRegion[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsCountry.length; i++) {
            countries.push('"' + this.selectedItemsCountry[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemscity.length; i++) {
            cities.push('"' + this.selectedItemscity[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsstate.length; i++) {
            states.push('"' + this.selectedItemsstate[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsTerritories.length; i++) {
            territory.push('"' + this.selectedItemsTerritories[i].id + '"');
        }
        for (let i = 0; i < this.selectedItemsFieldOrganization.length; i++) {
            orgField.push(this.selectedItemsFieldOrganization[i].id);
        }
        for (let i = 0; i < this.selectedItemsFieldSite.length; i++) {
            siteField.push(this.selectedItemsFieldSite[i].id);
        }
        for (let i = 0; i < this.selectedItemsFieldShow2.length; i++) {
            anotherField.push(this.selectedItemsFieldShow2[i].id);
        }

        var params = {
            filterName: this.searchSite.value.filterByName,
            itemPartner: this.selectedItemsPartner,
            dropdownPartner: this.dropdownListPartner,
            PartnerStatus: this.selectedItemsPartnerTypeStatus,
            dropdownStatus: this.dropdownListPartnerTypeStatus,
            marketType: this.selectedItemsMarketType,
            dropdownMarketType: this.dropdownListMarketType,
            itemSubPartner: this.selectedItemsSubPartnerType,
            dropdownSubPartner: this.dropdownListSubPartnerType,
            geo: this.selectedItemsGeo,
            dropdownGeo: this.dropdownListGeo,
            territory: this.selectedItemsTerritories,
            dropdownTerritory: this.dropdownListTerritories,
            country: this.selectedItemsCountry,
            dropdownCountry: this.dropdownListCountry,
            city: this.selectedItemscity,
            dropdownListcity: this.dropdownListcity,
            state: this.selectedItemsstate,
            dropdownListstate: this.dropdownListstate,
            fieldOrg: this.selectedItemsFieldOrganization,
            dropdownFieldOrg: this.dropdownListFieldOrganization,
            itemFieldSite: this.selectedItemsFieldSite,
            dropdownFieldSite: this.dropdownListFieldSite,
            fieldShow: this.selectedItemsFieldShow2,
            dropdownFieldShow: this.dropdownListFieldShow2
        }

        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        if (
            this.selectedItemsPartner.length == 0 ||
            this.selectedItemsMarketType.length == 0 ||
            // this.selectedItemsTerritories.length == 0 ||
            // this.selectedItemsGeo.length == 0 ||
            this.selectedItemsCountry.length == 0 ||
            this.selectedItemsPartnerTypeStatus.length == 0 ||
            this.selectedItemsFieldSite.length == 0
        ) {
            /*
            swal(
                'Field is Required!',
                'Please enter the Site Report form required!',
                'error'
            )
            */
            if (this.selectedItemsPartner.length == 0) {
                this.showValidPartnerType = true;
            } else {
                this.showValidPartnerType = false;
            }
            if (this.selectedItemsMarketType.length == 0) {
                this.showValidMarketType = true;
            } else {
                this.showValidMarketType = false;
            }
            // if (this.selectedItemsTerritories.length == 0) {
            //     this.showValidTerritory = true;
            // } else {
            //     this.showValidTerritory = false;
            // }
            // if (this.selectedItemsGeo.length == 0) {
            //     this.showValidGeo = true;
            // } else {
            //     this.showValidGeo = false;
            // }
            if (this.selectedItemsCountry.length == 0) {
                this.showValidCountry = true;
            } else {
                this.showValidCountry = false;
            }
            if (this.selectedItemsPartnerTypeStatus.length == 0) {
                this.showValidPartnerStatus = true
            } else {
                this.showValidPartnerStatus = false
            }
            if (this.selectedItemsFieldSite.length == 0) {
                this.showValidFieldSite = true;
            }
            else {
                this.showValidFieldSite = false;
            }
        }
        else {
            this.loading = true;
            this.showValidPartnerType = false;
            this.showValidMarketType = false;
            // this.showValidTerritory = false;
            // this.showValidGeo = false;
            this.showValidCountry = false;
            this.showValidPartnerStatus = false;
            this.showValidFieldSite = false;

            /*fix issue excel no 263.Show data based role*/

            var orgid = "";
            var role = "";

            // if (this.checkrole()) {
            //     orgid = this.urlGetOrgId();
            //     if (this.itsDistributor()) {
            //         role = "ORGANIZATION";
            //     } else {
            //         role = "DISTRIBUTOR";
            //     }
            // }

            /*end fix issue excel no 263.Show data based role*/

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            if (this.adminya) {
                orgid = this.urlGetOrgId();
                if (this.distributorya) {
                    role = "DISTRIBUTOR";
                } else {
                    role = "ORGANIZATION";
                }
            }

            /* salah logic, ganti get highest userlevelid, ganti jadi ditampung variable aja sebenernya fungsi nya (issue31082018)*/

            var keyword = {
                filterByName: filterSiteName,
                partnerType: partner.toString(),
                subPartnerType: subPartnerType.toString(),
                marketType: marketType.toString(),
                partnerStatus: partnerStatus.toString(),
                geo: geo.toString(),
                region: region.toString(),
                subregion: subregion.toString(),
                country: countries.toString(),
                city: cities.toString(),
                state: states.toString(),
                territory: territory.toString(),
                fieldOrg: orgField.toString(),
                anotherField: anotherField.toString(),
                fieldSite: siteField.toString(),
                filterByRollup: this.itsRollup,
                OrgId: orgid, //fix issue excel no 263.Show data based role
                Role: role //fix issue excel no 263.Show data based role
            };

            //Hanya untuk dikenang
            // "api/MainSite/reportSite/{'filterByName':'" + filterSiteName + "','partnerType':'" + partner +
            //     "','subPartnerType':'" + subPartnerType + "','marketType':'" + marketType + "','partnerStatus':'" + partnerStatus + "','geo':'" + geo +
            //     "','region':'" + region + "','subregion':'" + subregion + "','country':'" + countries +
            //     "','territory':'" + territory + "','fieldOrg':'" + orgField + "','anotherField':'" + anotherField + "','fieldSite':'" + siteField + "'}"

            let sites: any;
            /* new post for autodesk plan 18 oct */
            this.apiCall = this.service.httpClientPost("api/MainSite/reportSite", keyword)
                .subscribe(res => {
                    sites = res;
                    let parsing : any= sites;
                    // console.log(parsing);
                    this.listkey = [];
                    if (parsing.length > 0) {
                        for (let key in parsing[0]) {
                            this.listkey.push(key);
                        }
                        for (let n = 0; n < parsing.length; n++) {
                            for (let j = 0; j < this.listkey.length; j++) {
                                var kolom = this.listkey[j];
                                //karena dari .net pakai serialize jadi kalau value property nya null itu bakal jadi object baru dan kalau dicetak ke frontend jadinya
                                //bakal ky gini [object Object], jadi pakai cara dibawah ini. Bagusnya diatur di query pakai IFNULL, tapi field yg ditampilin random bos jadi pararanjang
                                //cukup itu saja yg PANJANG, yg lain jangan hehe..
                                if (typeof parsing[n][kolom] === 'object' && parsing[n][kolom].constructor === Object) {
                                    //hapus lah jek, tuman
                                    delete parsing[n][kolom];
                                    parsing[n][kolom] = "";
                                }
                            }
                        }
                        this.reportSite = parsing;//insert data for export excel
                        this.dataFound = true;
                        this.loading = false;
                    } else {
                        this.reportSite = "";
                        this.dataFound = true;
                        this.loading = false;
                    }
                }, error => {
                    this.reportSite = "";
                    this.dataFound = true;
                    this.loading = false;
                });
            /* new post for autodesk plan 18 oct */
        }

    }
    itsRollup: string = "";
    RollupPartnerDataType(value) {
        this.itsRollup = value;
        
    }
}
