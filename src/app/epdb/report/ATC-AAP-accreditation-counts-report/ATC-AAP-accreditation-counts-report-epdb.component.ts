import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { AppService } from "../../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import * as XLSX from 'xlsx';
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilteratcaap' })
export class DataFilterATCAAPPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteCountryCode.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.geo_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.region_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.subregion_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Territory_Name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.market_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteCount.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-ATC-AAP-accreditation-counts-report-epdb',
  templateUrl: './ATC-AAP-accreditation-counts-report-epdb.component.html',
  styleUrls: [
    './ATC-AAP-accreditation-counts-report-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ATCAAPAccreditationCountsReportEPDBComponent implements OnInit {

  private _serviceUrl = "api/MainSite";
  // data = [];
  data :any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  partner_1: number;
  partner_2: number;
  status_1: string;
  status_2: string;
  public loading = false;
  useraccesdata:any;
  fileName ="AccCountRpt.xls";

  constructor(public http: Http, private service: AppService, private session: SessionService) {

      //get user level
      let useracces = this.session.getData();
      this.useraccesdata = JSON.parse(useracces);

  }

  
  ExportExceltoXls()
  {
     this.fileName = "AccCountRpt.xls";
     this.ExportExcel();
  }
  ExportExceltoXlSX()
  {
     this.fileName = "AccCountRpt.xlsx";
     this.ExportExcel();
  }
  ExportExceltoCSV()
  {
     this.fileName = "AccCountRpt.csv";
     this.ExportExcel();
  }

  ExportExcel() {  
   
    let excel_data: any; 
    excel_data = this.data.map((item)=>{
        return {
            Country: item.Country,
            GEO: item.GEO,
            //OrgStatus: item.OrgStatus,
            Region: item.Region,
            "Sub-region": item['Sub-region'],
            "Territory Name":item['Territory Name'],
            "Market Name": item['Market Name'],
            "Active Training Partner ORGs": Number(item['Active Training Partner ORGs']),
             "Active ATC only Sites": Number(item['Active ATC only Sites']) ,
            "Active Academic Partner only": Number(item['Active Academic Partner only']),
            "Active ATC and AAP":Number(item['Active ATC and AAP']),
            "Total Training Partner sites":Number(item['Total Training Partner sites']),
            "ATC Instructors": Number(item['ATC Instructors']),
          
        }
    });
   
    var ws_site = XLSX.utils.json_to_sheet(excel_data);
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws_site, this.fileName);
    XLSX.writeFile(wb, this.fileName);
}


  

  ngOnInit() {
    this.loading = true;
    this.getATC_SiteNew();
  }

  listkey=[];
  reportATCAAP: any = "";
  getATC_SiteNew(){

    var orgid = "null";
    var role = "null";
    
    if(this.checkrole()){
        if(this.itsDistributor()){
          orgid = this.urlGetOrgId();
          role = "Distributor";
        }else{
          orgid = this.urlGetOrgId();
          role = "SiteAdmin";
        }
    }
    this.reportATCAAP = null;
    this.service.httpClientGet(this._serviceUrl + "/ATC_AAP_REPORT/" + role + "/" + orgid, '')
      .subscribe(res => {
        this.data = res;
        var resOb = JSON.stringify(res);
        if(this.data != null){
          
          for (let key in this.data[0]) {
             this.listkey.push(key);

          }
          this.reportATCAAP =JSON.parse(resOb).ExcelATCAAPInfo;
        }else{
          this.reportATCAAP = "";
          this.data = '';
        }
        this.loading = false;
      }, error => {
        this.data = null;
        this.loading = false;
      });
     
  }

  /* populate data issue role distributor */
  checkrole(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
            return false;
        } else {
            return true;
        }
    }
  }

  itsinstructor(): boolean {
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
      for (var i = 0; i < userlvlarr.length; i++) {
          if (userlvlarr[i] == "TRAINER") {
              return true;
          } else {
              return false;
          }
      }
  }

  itsDistributor(): boolean {
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
      for (var i = 0; i < userlvlarr.length; i++) {
          if (userlvlarr[i] == "DISTRIBUTOR") {
              return true;
          } else {
              return false;
          }
      }
  }

  urlGetOrgId(): string {
      var orgarr = this.useraccesdata.OrgId.split(',');
      var orgnew = [];
      for (var i = 0; i < orgarr.length; i++) {
          orgnew.push('"' + orgarr[i] + '"');
      }
      return orgnew.toString();
  }

  urlGetSiteId(): string {
      var sitearr = this.useraccesdata.SiteId.split(',');
      var sitenew = [];
      for (var i = 0; i < sitearr.length; i++) {
          sitenew.push('"' + sitearr[i] + '"');
      }
      return sitenew.toString();
  }
  /* populate data issue role distributor */

}