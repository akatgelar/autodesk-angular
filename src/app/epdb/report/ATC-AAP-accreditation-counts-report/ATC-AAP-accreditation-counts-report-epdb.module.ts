import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ATCAAPAccreditationCountsReportEPDBComponent } from './ATC-AAP-accreditation-counts-report-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterATCAAPPipe } from './ATC-AAP-accreditation-counts-report-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const ATCAAPAccreditationCountsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: ATCAAPAccreditationCountsReportEPDBComponent,
    data: {
      breadcrumb: 'menu_report_epdb.atc_aap_acc_count_report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ATCAAPAccreditationCountsReportEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ATCAAPAccreditationCountsReportEPDBComponent, DataFilterATCAAPPipe],
  providers: [AppService]
})
export class ATCAAPAccreditationCountsReportEPDBModule { }
