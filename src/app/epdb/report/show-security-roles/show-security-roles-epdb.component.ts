import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { AppService } from "../../../shared/service/app.service";

@Component({
    selector: 'app-show-security-roles-epdb',
    templateUrl: './show-security-roles-epdb.component.html',
    styleUrls: [
        './show-security-roles-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ShowSecurityRolesEPDBComponent implements OnInit {

    public data: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";


    constructor(public http: Http, private service: AppService) { }

    ngOnInit() {
        var list: any;
        this.service.httpClientGet("api/ContactAll/SecurityRole", list)
            .subscribe(res => {
                list = res;
                list.length > 0 ? this.data = list : this.data = "";
            }, error => {
                this.data = "";
            });
    }

}