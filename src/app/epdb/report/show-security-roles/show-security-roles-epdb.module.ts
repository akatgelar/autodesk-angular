import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowSecurityRolesEPDBComponent } from './show-security-roles-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";

export const ShowSecurityRolesEPDBRoutes: Routes = [
  {
    path: '',
    component: ShowSecurityRolesEPDBComponent,
    data: {
      breadcrumb: 'Show Security Roles Assignments ',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ShowSecurityRolesEPDBRoutes),
    SharedModule
  ],
  declarations: [ShowSecurityRolesEPDBComponent],
  providers: [AppService]
})
export class ShowSecurityRolesEPDBModule { }