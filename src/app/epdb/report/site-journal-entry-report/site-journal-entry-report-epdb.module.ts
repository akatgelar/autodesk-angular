import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteJournalEntryReportEPDBComponent } from './site-journal-entry-report-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
// import { DataFilterReport } from './Site-report-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const SiteJournalEntryReportEPDBRoutes: Routes = [
    {
        path: '',
        component: SiteJournalEntryReportEPDBComponent,
        data: {
            breadcrumb: 'menu_report_epdb.site_journal_entry',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SiteJournalEntryReportEPDBRoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule
    ],
    declarations: [SiteJournalEntryReportEPDBComponent],
    providers: [AppService, AppFilterGeo, AppFormatDate]
})
export class SiteJournalEntryReportEPDBModule { }