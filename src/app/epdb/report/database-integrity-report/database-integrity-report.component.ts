import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { AppService } from "../../../shared/service/app.service";
import { FormGroup, FormControl } from '@angular/forms';
import * as XLSX from 'xlsx';

@Component({
    selector: 'app-database-integrity-report',
    templateUrl: './database-integrity-report.component.html',
    styleUrls: [
        './database-integrity-report.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DatabaseIntegrityComponent implements OnInit {

    private _serviceUrl = "api/ReportEPDB";
    public data: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    listkolom = [];
    report = [];
    public dataFound = false;
    public loading = false;

    constructor(public http: Http, private service: AppService) {

    }

    ExportExcel(){
        // var date = this.service.formatDate();
        var fileName = "DatabaseIntegrityRpt.xls";
        var ws = XLSX.utils.json_to_sheet(this.report);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb,ws,fileName);
        XLSX.writeFile(wb,fileName);
    }

    findReport(value) {
        // this.loading = true;
        this.listkolom = [];
        this.report = [];
        var serviceTemp: string = "";

        switch (value) {
            case 'siteless_organization':
                serviceTemp = "/SiteOrg";
                break

            case 'organization_no_address':
                serviceTemp = "/OrgNoAddress";
                break

            case 'orphaned_site':
                serviceTemp = "/OrphanedSites";
                break

            case 'site_no_address':
                serviceTemp = "/SitesNoAddress";
                break

            case 'site_no_contact':
                serviceTemp = "/SitesNoContacts";
                break

            // query blm fix di api "Sites with similar Address1"
            case 'site_address1':
                serviceTemp = "/SitesAddress1";
                break

            // di exsisting tidak nampil "SiebelAddress <> SiteAddress"
            case 'siebel_site_address':
                serviceTemp = "/SiebelSiteAddress";
                break

            // di exsisting tidak nampil "Bad CSN Matches"
            case 'bad_csn_matches':
                serviceTemp = "/BadCSNMatches";
                break

            // query blm fix di api "PDB Status <> Siebel Status"
            case 'pdb_siebel_status':
                serviceTemp = "/PDBSiebelStatus";
                break

            // data banyak teuing. Query sudah jalan "Orphaned Contacts"
            case 'orphaned_contact':
                serviceTemp = "/OrphanedContacts";
                break

            // query belum fix "Potential Duplicate Users by Name"
            case 'duplicate_name':
                serviceTemp = "/DuplicateName";
                break

            // di exsisting tidak nampil "Potential Duplicate Users by SiebelId"
            case 'duplicate_siebel_id':
                serviceTemp = "/DuplicateSiebelId";
                break

            // query belum fix "Siebel Matches to verify"
            case 'siebel_matches':
                serviceTemp = "/SiebelMatches";
                break

            case 'active_users':
                serviceTemp = "/ActiveUsers";
                break

            case 'duplicate_email':
                serviceTemp = "/DuplicateEmail";
                break

            case 'illegal_email':
                serviceTemp = "/IllegalEmail";
                break

            // data banyak teuing. Query sudah jalan
            case 'bad_primary_site_id':
                serviceTemp = "/BadPrimarySiteId";
                break

            case 'missing_primary_site_id':
                serviceTemp = "/MissingPrimarySiteId";
                break

            // query belum fix "Missing First or Last Name"
            case 'missing_name':
                serviceTemp = "/MissingName";
                break

            // query belum fix "Duplicate (hidden) Users with LMS History"
            case 'duplicate_history':
                serviceTemp = "/DuplicateHistory";
                break

            // query belum fix "Duplicate (hidden) Users with LMS Certifications"
            case 'duplicate_certifications':
                serviceTemp = "/DuplicateCertifications";
                break

            case 'duplicate_attributes':
                serviceTemp = "/DuplicateAttributes";
                break

            case 'illegal_state':
                serviceTemp = "/IllegalState";
                break

            // di exsisting tidak nampil "Instructors whose name changed during the ATC Migration"
            case 'InstructorsWhose':
                serviceTemp = "/InstructorsWhose";
                break

            // di exsisting tidak nampil "ATC Instructors missing "Active for on-line eval" = "On""
            case 'atc_instructors':
                serviceTemp = "/ATCInstructors";
                break
        }

        var data: any;
        this.service.httpClientGet(this._serviceUrl + serviceTemp, data)
            .subscribe(res => {
                data = res;
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        this.report.push(data[i]); //untuk row data table
                    }
                    this.dataFound = true;
                    // this.loading = false;
                    for (let key in data[0]) {
                        this.listkolom.push(key); //untuk kolom di data table
                    }
                    this.dataFound = true;
                    // this.loading = false;

                } else {
                    this.listkolom = [];
                    this.report = [];
                    this.dataFound = true;
                    // this.loading = false;
                }
            }, error => {
                // this.service.errorserver();
                this.listkolom = [];
                this.report = [];
                this.dataFound = true;
                // this.loading = false;
            });
    }

    ngOnInit() {}
}