import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatabaseIntegrityComponent } from './database-integrity-report.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const DatabaseIntegrityRoutes: Routes = [
    {
      path: '',
      component: DatabaseIntegrityComponent,
      data: {
        breadcrumb: 'menu_report_epdb.db_integrity_report',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(DatabaseIntegrityRoutes),
      SharedModule,
      LoadingModule
    ],
    declarations: [DatabaseIntegrityComponent],
    providers:[AppService]
  })
  export class DatabaseIntegrityModule { }