import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BetaReportBuilderEPDBComponent } from './beta-report-builder-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const BetaReportBuilderEPDBRoutes: Routes = [
    {
      path: '',
      component: BetaReportBuilderEPDBComponent,
      data: {
        breadcrumb: 'Beta Report Builder ',
        icon: 'icofont-home bg-c-blue',
        status: false
      }
    }
  ];

  @NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(BetaReportBuilderEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [BetaReportBuilderEPDBComponent]
  })
  export class BetaReportBuilderEPDBModule { }