import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';

@Component({
    selector: 'app-beta-report-builder-epdb',
    templateUrl: './beta-report-builder-epdb.component.html',
    styleUrls: [
        './beta-report-builder-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css', 
        ], 
    encapsulation: ViewEncapsulation.None
})

export class BetaReportBuilderEPDBComponent implements OnInit {
    
    // Choose Criteri
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownSettingsGeo = {};
  
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownSettingsRegion = {};
  
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownSettingsSubRegion = {};
    
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownSettingsCountry = {};
  
    dropdownListSubCountry = [];
    selectedItemsSubCountry = [];
    dropdownSettingsSubCountry = {};

    dropdownListPartner = [];
    selectedItemsPartner = [];
    dropdownSettingsPartner = {};
  
    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];
    dropdownSettingsPartnerTypeStatus = {};

    dropdownListJobRole = [];
    selectedItemsJobRole = [];
    dropdownSettingsJobRole = {};

    dropdownListIndustry = [];
    selectedItemsIndustry = [];
    dropdownSettingsIndustry = {};

    // Choose Field to Show
    dropdownListFieldOrganization = [];
    selectedItemsFieldOrganization = [];
    dropdownSettingsFieldOrganization = {};

    dropdownListLocationOrganization = [];
    selectedItemsLocationOrganization = [];
    dropdownSettingsLocationOrganization = {};

    dropdownListFieldSite = [];
    selectedItemsFieldSite = [];
    dropdownSettingsFieldSite = {};

    dropdownListLocationSite = [];
    selectedItemsLocationSite = [];
    dropdownSettingsLocationdSite = {};

    dropdownListPartnerType = [];
    selectedItemsPartnerType = [];
    dropdownSettingsPartnerType = {};

    dropdownListPartnerTypeAttribute = [];
    selectedItemsPartnerTypeAttribute = [];
    dropdownSettingsPartnerTypeAttribute = {};

    dropdownListFieldContact = [];
    selectedItemsFieldContact = [];
    dropdownSettingsFieldContact = {};

    public data: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    
    currentCenter = 'start';
    
    constructor(public http: Http) { }

    ngOnInit() {
        this.http.get(`assets/data/activities.json`)
        .subscribe((data)=> {
        this.data = data.json();
        });

        /** Choose Criteria */
        // Geo
        this.dropdownListGeo = [
            {"id":1,"itemName":"AMER"},
            {"id":2,"itemName":"APAC (excl. GCR)"},
            {"id":3,"itemName":"EMEA"},
            {"id":4,"itemName":"GCR"}
        ];
        this.selectedItemsGeo = [];
        this.dropdownSettingsGeo = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Region
        this.dropdownListRegion = [
            {"id":1,"itemName":"ANZ"},
            {"id":2,"itemName":"ASEAN"},
            {"id":3,"itemName":"Canada"},
            {"id":4,"itemName":"Central Europe"}
        ];
        this.selectedItemsRegion = [];
        this.dropdownSettingsRegion = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // SubRegion
        this.dropdownListSubRegion = [
            {"id":1,"itemName":"Africa"},
            {"id":2,"itemName":"ASEAN"},
            {"id":3,"itemName":"Australia / New Zealand"},
            {"id":4,"itemName":"Central Europe"}
        ];
        this.selectedItemsSubRegion = [];
        this.dropdownSettingsSubRegion = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Country
        this.dropdownListCountry = [
            {"id":1,"itemName":"Afganistan"},
            {"id":2,"itemName":"Albania"},
            {"id":3,"itemName":"Algeria"},
            {"id":4,"itemName":"Andorra"}
        ];
        this.selectedItemsCountry = [];
        this.dropdownSettingsCountry = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // SubCountry
        this.dropdownListSubCountry = [
            {"id":1,"itemName":"Ajman"},
            {"id":2,"itemName":"Abia State"},
            {"id":3,"itemName":"Abkhazia"},
            {"id":4,"itemName":"Absheron Rayon"}
        ];
        this.selectedItemsSubCountry = [];
        this.dropdownSettingsSubCountry = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Partner
        this.dropdownListPartner = [
        {"id":58,"itemName":"Authorized Academic Partner (AAP)"},
        {"id":1,"itemName":"Authorized Training Center (ATC)"},
        {"id":7,"itemName":"Authors and Publishers"},
        {"id":30,"itemName":"Autodesk Contractor"},
        {"id":9,"itemName":"Autodesk Developer Network (ADN)"}
        ];
        this.selectedItemsPartner = [];
        this.dropdownSettingsPartner = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Partner Type Status
        this.dropdownListPartnerTypeStatus = [
            {"id":"A","itemName":"Active"},
            {"id":"V","itemName":"Approved"},
            {"id":"O","itemName":"Opportunity"},
            {"id":"P","itemName":"Pending"},
            {"id":"R","itemName":"Rejected"}
        ];
        this.selectedItemsPartnerTypeStatus = [];
        this.dropdownSettingsPartnerTypeStatus = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Job Role
        this.dropdownListJobRole = [
            {"id":19,"itemName":"Admin/Order Entry"},
            {"id":23,"itemName":"Instructor"},
            {"id":18,"itemName":"Marketing"},
            {"id":11,"itemName":"Owner/Principal"},
            {"id":12,"itemName":"Sales Manager"}
        ];
        this.selectedItemsJobRole = [];
        this.dropdownSettingsJobRole = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Industry
        this.dropdownListIndustry = [
            {"id":"Building","itemName":"AEC: Building"},
            {"id":"Infrastructure","itemName":"AEC: Infrastructure"},
            {"id":"Education","itemName":"Education"},
            {"id":"Horizontal","itemName":"Horizontal Design"},
            {"id":"Manufacturing","itemName":"Manufacturing"}
        ];
        this.selectedItemsIndustry = [];
        this.dropdownSettingsIndustry = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        /** Choose Field to Show */
        // Field Organization
        this.dropdownListFieldOrganization = [
            {"id":"s.OrgId","itemName":"OrgId"},
            {"id":"s.ATCOrgId","itemName":"ATCOrgId"},
            {"id":"s.OrgStatus_retired","itemName":"OrgStatus_retired"},
            {"id":"s.OrgName","itemName":"OrgName"},
            {"id":"s.EnglishOrgName","itemName":"EnglishOrgName"}
        ];
        this.selectedItemsFieldOrganization = [];
        this.dropdownSettingsFieldOrganization = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Location Organization
        this.dropdownListLocationOrganization = [
            {"id":"oco.GeoName","itemName":"Geo"},
            {"id":"oco.RegionName","itemName":"Region"},
            {"id":"oco.SubRegionName","itemName":"SubRegion"},
            {"id":"oco.Country","itemName":"Country"},
            {"id":"ost.SubCountry","itemName":"SubCountry"}
        ];
        this.selectedItemsLocationOrganization = [];
        this.dropdownSettingsLocationOrganization = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Field Site
        this.dropdownListFieldSite = [
            {"id":"s.SiteId","itemName":"SiteId"},
            {"id":"s.ATCSiteId","itemName":"ATCSiteId"},
            {"id":"s.OrgId","itemName":"OrgId"},
            {"id":"s.SiteStatus_retired","itemName":"SiteStatus_retired"},
            {"id":"s.SiteName","itemName":"SiteName"}
        ];
        this.selectedItemsFieldSite = [];
        this.dropdownSettingsFieldSite = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Location Site
        this.dropdownListFieldSite = [
            {"id":"sco.GeoName","itemName":"Geo"},
            {"id":"sco.RegionName","itemName":"Region"},
            {"id":"sco.SubRegionName","itemName":"SubRegion"},
            {"id":"sco.Country","itemName":"Country"},
            {"id":"sst.SubCountry","itemName":"SubCountry"}
        ];
        this.selectedItemsFieldSite = [];
        this.dropdownSettingsFieldSite = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Partner Type
        this.dropdownListPartnerType = [
            {"id":"r.RoleName","itemName":"Partner Type"},
            {"id":"sr.Status","itemName":"Partner Type Status"},
            {"id":"sr.CSN","itemName":"CSN"},
            {"id":"sr.ExternalId","itemName":"External Id"}
        ];
        this.selectedItemsPartnerType = [];
        this.dropdownSettingsPartnerType = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Partner Type Attribute
        this.dropdownListPartnerTypeAttribute = [
            {"id":"srp.ParamName","itemName":"Attribute Name"},
            {"id":"rp.ParamValue","itemName":"Attribute Value"}
        ];
        this.selectedItemsPartnerTypeAttribute = [];
        this.dropdownSettingsPartnerTypeAttribute = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Field Contact
        this.dropdownListFieldContact = [
            {"id":"s.ContactId","itemName":"ContactId"},
            {"id":"s.SiebelId","itemName":"SiebelId"},
            {"id":"s.SiebelStatus","itemName":"SiebelStatus"},
            {"id":"s.PrimarySiteId","itemName":"PrimarySiteId"},
            {"id":"s.ContactName","itemName":"ContactName"}
        ];
        this.selectedItemsFieldContact = [];
        this.dropdownSettingsFieldContact = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };
    }

    onItemSelect(item:any){
        console.log(item);
        console.log(this.selectedItemsPartner);
        console.log(this.selectedItemsPartnerTypeStatus);
        console.log(this.selectedItemsGeo);
        console.log(this.selectedItemsRegion);
        console.log(this.selectedItemsSubRegion);
        console.log(this.selectedItemsJobRole);
        console.log(this.selectedItemsCountry);
        console.log(this.selectedItemsSubCountry);
        console.log(this.selectedItemsIndustry);
        console.log(this.selectedItemsFieldSite);
        console.log(this.selectedItemsFieldOrganization);
        console.log(this.selectedItemsFieldContact);
    }
    OnItemDeSelect(item:any){
        console.log(item);
        console.log(this.selectedItemsPartner);
        console.log(this.selectedItemsPartnerTypeStatus);
        console.log(this.selectedItemsGeo);
        console.log(this.selectedItemsRegion);
        console.log(this.selectedItemsSubRegion);
        console.log(this.selectedItemsJobRole);
        console.log(this.selectedItemsCountry);
        console.log(this.selectedItemsSubCountry);
        console.log(this.selectedItemsIndustry);
        console.log(this.selectedItemsFieldSite);
        console.log(this.selectedItemsFieldOrganization);
        console.log(this.selectedItemsFieldContact);
    }
    onSelectAll(items: any){
        console.log(items);
    }
    onDeSelectAll(items: any){
        console.log(items);
    }
}