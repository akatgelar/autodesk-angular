import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

@Component({
  selector: 'app-edit-contact-qualifications-epdb',
  templateUrl: './edit-contact-qualifications-epdb.component.html',
  styleUrls: [
    './edit-contact-qualifications-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class EditContactQualificationsEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Qualifications';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  public datasite: any;
  contactqualificationform: FormGroup;
  public orgentriestype: any;
  public useraccesdata: any;
  public datayearcurrency: any;
  public programtype: any;
  public productcategory: any;
  public product: any;
  public typequalifications: any;
  public datadetail: any;
  modelPopup1: NgbDateStruct;
  public loading = false;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private formatdate: AppFormatDate,
    private session: SessionService, private datePipe: DatePipe) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let AutodeskProduct = new FormControl('');
    let QualificationType = new FormControl('');
    let QualificationDate = new FormControl('');
    let Comments = new FormControl('');
    let Version = new FormControl('');
    this.contactqualificationform = new FormGroup({
      AutodeskProduct: AutodeskProduct,
      QualificationType: QualificationType,
      QualificationDate: QualificationDate,
      Comments: Comments,
      Version:Version
    });

  }

  id: string = "";
  ContactId: string = "";
  SiteId: string = "";
  ngOnInit() {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.ContactId = params['ContactId'] || '';
      this.SiteId = params['SiteId'] || '';
    });

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datadetail = '';
        }
        else {
          this.datadetail = result;
          if (this.datadetail != null) {
            if (this.datadetail.Comments != null && this.datadetail.Comments != "") {
              this.datadetail.Comments = this.service.decoder(this.datadetail.Comments);
            }
          }
          var dateConversion = this.service.DateConversion(this.datadetail.QualificationDate);
          var temp = dateConversion.split("-");
          this.modelPopup1 = {
            "year": parseInt(temp[2]),
            "month": parseInt(temp[1]),
            "day": parseInt(temp[0])
          };
          this.findVersion(this.datadetail.AutodeskProduct)
          this.buildForm();
        }
      },
        error => {
          this.service.errorserver();
          this.datadetail = '';
        });

    var data = '';
    this.service.httpClientGet("api/Product/qualification/ProductFamilies", data)
      .subscribe(res => {
        if (res == "Not found") {
          this.service.notfound();
          this.productcategory = '';
        } else {
          this.productcategory = res;
          this.getProductList(this.productcategory);
        }
      }, error => {
        this.service.errorserver();
        this.productcategory = '';
      }
      );

    this.service.httpClientGet('api/Currency/where/{"Parent":"QualificationTypes","Status":"A"}', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.typequalifications = '';
        } else {
          this.typequalifications = result;
        }
      },
        error => {
          this.service.errorserver();
          this.typequalifications = '';
        });

  }

  versions:any;
  findVersion(value) {
    if (value != null) {
        var version: any;
        this.service.httpClientGet("api/Product/Version/" + value, version)
            .subscribe(result => {
                this.versions = result;
            }, error => { this.service.errorserver(); });
    } else {
        this.versions = "";
    }
  }

  dropdownListProduct = [];
  dropdownListProductCategory = [];
  getProductList(data) {
    for (var i = 0; i < data.length; i++) {
      this.dropdownListProductCategory.push(data[i].familyId);
    }
    for (var i = 0; i < this.dropdownListProductCategory.length; i++) {
      this.service.httpClientGet("api/Product/qualification/where/{'familyId':'" + this.dropdownListProductCategory[i] + "','Status':'A','VersionStatus':'A'}", '')
        .subscribe(result => {
          // data = result.replace(/\t|\n|\r|\f|\\|\/|'/g, "");
          this.product = result;
          this.dropdownListProduct.push([this.product]);
        },
          error => {
            this.service.errorserver();
          });
    }
  }

  //submit
  onSubmit() {
    this.contactqualificationform.controls['AutodeskProduct'].markAsTouched();
    this.contactqualificationform.controls['QualificationType'].markAsTouched();
    this.contactqualificationform.controls['QualificationDate'].markAsTouched();
    //this.contactqualificationform.controls['FYIndicator'].markAsTouched();

    if (this.contactqualificationform.valid) {
      this.loading = true;
      this.contactqualificationform.value.QualificationDate = this.formatdate.dateCalendarToYMD(this.contactqualificationform.value.QualificationDate);
      this.contactqualificationform.value.LastAdminBy = this.useraccesdata.ContactName;

      this.contactqualificationform.value.ContactId = this.ContactId;
      this.contactqualificationform.value.cuid = this.useraccesdata.ContactName;
      this.contactqualificationform.value.UserId = this.useraccesdata.UserId;
     // this.contactqualificationform.value.Comments = this.htmlEntityService.encoder(this.contactqualificationform.value.Comments);
      //convert object to json
      let data = JSON.stringify(this.contactqualificationform.value);

      //post action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });

      //redirect
      this.router.navigate(['/manage/contact/detail-contact', this.ContactId, this.SiteId]);
      this.loading = false;
    }
  }

  buildForm(): void {

    // var QualificationDateConvert = this.formatdate.dateYMDToCalendar(this.datadetail.QualificationDate);

    let AutodeskProduct = new FormControl(this.datadetail.AutodeskProduct, Validators.required);
    let QualificationType = new FormControl(this.datadetail.QualificationType, Validators.required);
    let QualificationDate = new FormControl('');
    let Comments = new FormControl(this.datadetail.Comments);
    let Version = new FormControl(this.datadetail.AutodeskProductVersion);
    this.contactqualificationform = new FormGroup({
      AutodeskProduct: AutodeskProduct,
      QualificationType: QualificationType,
      QualificationDate: QualificationDate,
      Comments: Comments,
      Version:Version
    });
  }

  //reset form
  resetForm(){
    this.contactqualificationform.reset({
      'AutodeskProduct': this.datadetail.AutodeskProduct,
      'QualificationType': this.datadetail.QualificationType,
      'QualificationDate': this.datadetail.QualificationDate,
      'Comments': this.datadetail.Comments,
      'Version': this.datadetail.AutodeskProductVersion
    });
  }
}
