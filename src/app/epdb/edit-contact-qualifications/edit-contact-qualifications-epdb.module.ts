import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditContactQualificationsEPDBComponent } from './edit-contact-qualifications-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const EditContactQualificationsEPDBRoutes: Routes = [
  {
    path: '',
    component: EditContactQualificationsEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.partner_type_spec.edit_qualification',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditContactQualificationsEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [EditContactQualificationsEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class EditContactQualificationsEPDBModule { }
