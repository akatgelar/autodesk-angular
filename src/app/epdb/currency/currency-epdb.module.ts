import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyEPDBComponent } from './currency-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { DataFilterCurrencyPipe } from './currency-epdb.component';
import { SessionService } from '../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const CurrencyEPDBRoutes: Routes = [
  {
    path: '',
    component: CurrencyEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.currency.currency_conversion',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CurrencyEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [CurrencyEPDBComponent, DataFilterCurrencyPipe],
  providers:[AppService, SessionService]
})

export class CurrencyEPDBModule { }
