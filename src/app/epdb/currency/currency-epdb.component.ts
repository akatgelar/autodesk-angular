import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';

@Pipe({ name: 'dataFilterCurrency' })
export class DataFilterCurrencyPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.KeyValue.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.Key.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

declare const $: any;
declare var Morris: any;

@Component({
    selector: 'currency-epdb',
    templateUrl: './currency-epdb.component.html',
    styleUrls: [
        './currency-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CurrencyEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Currency';
    messageResult: string = '';
    messageError: string = '';
    private data;
    currency;
    dataCurrency;
    fyindicator;
    conversion;
    dataConversion;
    currencyValue;
    public useraccesdata: any;

    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortByCurrency: string = "KeyValue";
    public sortBy: string = "";
    public sortByKeyValue: string = "KeyValue";
    public sortOrder: string = "asc";
    addCurrency: FormGroup;
    updateCurrency: FormGroup;
    addFinancial: FormGroup;
    addCurrencyConversion: FormGroup;
    updateConversion: FormGroup;
    public loading = false;

    constructor(public session: SessionService, private router: Router, private service: AppService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let Key = new FormControl('', Validators.required);
        let KeyValue = new FormControl('', Validators.required);
        let Parent = new FormControl('');
        let Status = new FormControl('');

        this.addCurrency = new FormGroup({
            Key: Key,
            KeyValue: KeyValue,
            Parent: Parent,
            Status: Status
        });

        let Year = new FormControl('', Validators.required);
        let Currency = new FormControl('', Validators.required);
        let ValuePerUSD = new FormControl('', Validators.required);

        this.addCurrencyConversion = new FormGroup({
            Year: Year,
            Currency: Currency,
            ValuePerUSD: ValuePerUSD,
            Status: Status
        });
    }

    changecurrency(year) {
        if (year != "") {
            // console.log(year);
            this.service.httpClientGet(this._serviceUrl + "/SelectByYear/" + year,  '')
            .subscribe((result:any) => {
                // console.log(result);
                this.currencyValue = result.sort((a,b)=>{
                    if(a.KeyValue > b.KeyValue)
                        return 1
                    if(a.KeyValue < b.KeyValue)
                        return -1
                    else return 0
                });
            },
                error => {
                    this.service.errorserver();
                });
        }
    }

    accesAddBtnCurrency: Boolean = true;
    accesUpdateBtnCurrency: Boolean = true;

    accesAddBtnConversion: Boolean = true;
    accesUpdateBtnConversion: Boolean = true;
    accesDeleteBtnConversion: Boolean = true;
    ngOnInit() {
        this.dataCurrency = {};
        this.dataConversion = {};
        this.getCurrency();
        this.getFYIndicator();
        this.getCurrencyConversion();
        this.currencyCombobox();

        this.accesAddBtnCurrency = this.session.checkAccessButton("admin/currency-add");
        this.accesUpdateBtnCurrency = this.session.checkAccessButton("admin/currency-edit");

        this.accesAddBtnConversion = this.session.checkAccessButton("admin/currency-conversion-add");
        this.accesUpdateBtnConversion = this.session.checkAccessButton("admin/currency-conversion-edit");
        this.accesDeleteBtnConversion = this.session.checkAccessButton("admin/currency-conversion-delete");
    }

    getCurrency() {
        this.loading = true;
        var currency = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'Currencies','Status':'A'}", currency)
            .subscribe(result => {
                this.currency = result;
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
    }

    currencyCombobox() {
        var currencyValue = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'Currencies','Status':'A'}", currencyValue)
            .subscribe((result:any) => {
                this.currencyValue = result.sort((a,b)=>{
                    if(a.KeyValue > b.KeyValue)
                        return 1
                    if(a.KeyValue < b.KeyValue)
                        return -1
                    else return 0
                });;
            },
                error => {
                    this.service.errorserver();
                });
    }

    getFYIndicator() {
        var fyindicator = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'FYIndicator','Status':'A'}", fyindicator)
            .subscribe(result => {
                this.fyindicator = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    getCurrencyConversion() {
        this.loading = true;
        var conversion = '';
        this.service.httpClientGet("api/Currency/CurrencyConversion", conversion)
            .subscribe(result => {
                this.conversion = result;
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
    }

    onSubmit() {
        this.addCurrency.controls['Key'].markAsTouched();
        this.addCurrency.controls['KeyValue'].markAsTouched();
        if (this.addCurrency.valid) {
            var check: any;
            this.service.httpClientGet("api/Currency/where/{'Key':'" + this.addCurrency.value.Key + "'}", check)
                .subscribe(result => {
                    check = result;
                    if (check.length != 0) {
                        swal('Information!', 'Duplicate Data In Database', 'error');
                        this.resetCurrency();
                    } else {
                        if (Object.keys(this.dataCurrency).length !== 0 && this.dataCurrency.constructor === Object) {
                            var update = {
                                'Key': this.addCurrency.value.Key,
                                'KeyValue': this.addCurrency.value.KeyValue
                            };
                            this.service.httpCLientPut(this._serviceUrl+'/'+this.dataCurrency.Key, update)
                                .subscribe(res=>{
                                    console.log(res)
                                });
                                //this.service.httpCLientPut(this._serviceUrl,this.dataCurrency.Key, update)
 
                            this.dataCurrency = {};
                        } else {
                            this.addCurrency.value.Parent = "Currencies";
                            this.addCurrency.value.Status = "A";
                            this.addCurrency.value.muid = this.useraccesdata.ContactName; 
                            this.addCurrency.value.cuid = this.useraccesdata.ContactName;
                            this.addCurrency.value.UserId = this.useraccesdata.UserId;
                            let data = JSON.stringify(this.addCurrency.value);
                            this.service.httpClientPost(this._serviceUrl, data)
							 .subscribe(result => {
									console.log("success");
								}, error => {
									this.service.errorserver();
								});

                            setTimeout(() => {
                                this.loading = false;
                            }, 1000);
                        }
                        this.getCurrency();
                        this.getCurrency();
                        this.addCurrency.reset({
                            'Key': '',
                            'KeyValue': ''
                        });
                    }
                }, error => {
                    this.service.errorserver();
                });
        }
    }

    resetCurrency() {
        this.addCurrency.reset({
            'Key': '',
            'KeyValue': ''
        });
    }

    onSubmitConversion() {
        this.addCurrencyConversion.controls['Year'].markAsTouched();
        this.addCurrencyConversion.controls['Currency'].markAsTouched();
        this.addCurrencyConversion.controls['ValuePerUSD'].markAsTouched();

        if (this.addCurrencyConversion.valid) {
            if (Object.keys(this.dataConversion).length === 0 && this.dataConversion.constructor === Object) {
                this.addCurrencyConversion.value.Status = "A";
                this.addCurrencyConversion.value.muid = this.useraccesdata.ContactName; 
                this.addCurrencyConversion.value.cuid = this.useraccesdata.ContactName;
                this.addCurrencyConversion.value.UserId = this.useraccesdata.UserId;
                let data = JSON.stringify(this.addCurrencyConversion.value);
                this.service.httpClientPost(this._serviceUrl + "/CurrencyConversion", data)
				 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
                setTimeout(() => {
                    this.ngOnInit();
                }, 1000);
            } else {
                var updateConversion = {
                    'Year': this.addCurrencyConversion.value.Year,
                    'Currency': this.addCurrencyConversion.value.Currency,
                    'ValuePerUSD': this.addCurrencyConversion.value.ValuePerUSD,
                    'Status': 'A'
                };
                
                this.service.httpCLientPut(this._serviceUrl + "/CurrencyConversion/"+this.dataConversion.CurrencyConversionId, updateConversion)
                    .subscribe(res=>{
                        console.log(res)
                    });
                    //this.service.httpCLientPut(this._serviceUrl + "/CurrencyConversion",this.dataConversion.CurrencyConversionId, updateConversion)
  
                this.dataConversion = {};

                setTimeout(() => {
                    this.getCurrencyConversion();    
                }, 3000);
            }
            this.addCurrencyConversion.reset({
                'Year': '',
                'Currency': '',
                'ValuePerUSD': '',
            });
        }
    }

    resetConversion() {
        this.addCurrencyConversion.reset({
            'Year': '',
            'Currency': '',
            'ValuePerUSD': '',
        });
    }

    // detailCurrency(key) {
    //     var dataCurrency = '';
    //     this.service.get("api/Currency/" + key, dataCurrency)
    //         .subscribe(result => {
    //             this.dataCurrency = JSON.parse(result);
    //             if (Object.keys(this.dataCurrency).length !== 0 && this.dataCurrency.constructor === Object) {
    //                 this.addCurrency.patchValue({ Key: this.dataCurrency.Key });
    //                 this.addCurrency.patchValue({ KeyValue: this.dataCurrency.KeyValue });
    //             }
    //         },
    //         error => {
    //             this.service.errorserver();
    //         });
    // }

    getConversion(id) {
        this.router.navigate(['/admin/currency/currency-conversion-edit/', id]);
        // var dataConversion = '';
        // this.service.get("api/Currency/CurrencyConversion/" + id, dataConversion)
        //     .subscribe(result => {
        //         this.dataConversion = JSON.parse(result);
        //         if (Object.keys(this.dataConversion).length !== 0 && this.dataConversion.constructor === Object) {
        //             this.addCurrencyConversion.patchValue({ Year: this.dataConversion.Year });
        //             this.addCurrencyConversion.patchValue({ Currency: this.dataConversion.Currency });
        //             this.addCurrencyConversion.patchValue({ ValuePerUSD: this.dataConversion.ValuePerUSD });
        //         }
        //     },
        //         error => {
        //             this.service.errorserver();
        //         });
    }

    openConfirmsSwal(id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then(result => {
                if (result == true) {
                    this.loading = true;
                    var cuid = this.useraccesdata.ContactName;
                    var UserId = this.useraccesdata.UserId;
                    var data = '';
                    this.service.httpClientDelete("api/Currency/CurrencyConversion/" + id + '/' + cuid + '/' + UserId, data)
                        .subscribe(value => {
                            var resource = value;
                            var index = this.conversion.findIndex(x => x.CurrencyConversionId == id);
                            setTimeout(() => {
                                // this.service.openSuccessSwal(resource['message']);
                                this.loading = false;                                
                            }, 1000);
                            if (index !== -1) {
                                this.conversion.splice(index, 1);
                                this.loading = false;
                            }
                        },
                            error => {
                                this.messageError = <any>error
                                this.service.errorserver();
                                this.loading = false;
                            });
                }

            }).catch(swal.noop);
        // this.loading = true;
        // var cuid = this.useraccesdata.ContactName;
        // var UserId = this.useraccesdata.UserId;
        // var data = '';
        // this.service.httpClientDelete("api/Currency/CurrencyConversion/" + id + '/' + cuid ,data, UserId,0 );
        // var index = this.conversion.findIndex(x => x.CurrencyConversionId == id);
        //         setTimeout(() => {
        //             // this.service.openSuccessSwal(resource['message']);
        //             this.loading = false;                                
        //         }, 1000);
        //         if (index !== -1) {
        //             this.conversion.splice(index, 1);
        //             this.loading = false;
        //         }
    }
}