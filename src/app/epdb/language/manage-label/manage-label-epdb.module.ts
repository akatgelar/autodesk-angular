import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageLabelEPDBComponent } from './manage-label-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';
import { AppService } from "../../../shared/service/app.service";

export const ManageLabelEPDBRoutes: Routes = [
  {
    path: '',
    component: ManageLabelEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.language.manage_label',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ManageLabelEPDBRoutes),
    SharedModule,LoadingModule
  ],
  declarations: [ManageLabelEPDBComponent],
  providers: [AppService]
})
export class ManageLabelEPDBModule { }
