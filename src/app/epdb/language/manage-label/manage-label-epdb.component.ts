import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
import '../../../../assets/echart/echarts-all.js';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";

import { SessionService } from '../../../shared/service/session.service';
import swal from 'sweetalert2';
import { debug } from 'util';

@Component({
  selector: 'app-manage-label-epdb',
  templateUrl: './manage-label-epdb.component.html',
  styleUrls: [
    './manage-label-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ManageLabelEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Language';
  languagePacks = [];
  data = {};
  dataInArray = [];
  elementName = "";
  elementValues = [];
  selectedLanguage = 'English';
  displayAddElement = false;
  parents = [];
  childs = [];
  grandChilds = [];
  grandGrandChilds = [];
  newParentName = '';
  newChildName = '';
  newGrandChildName = '';
  newGrandGrandChildName = '';

  constructor(public session: SessionService, private router: Router, private _http: Http, private service: AppService, private route: ActivatedRoute) { 
  }

  getDataLanguage() {
    this.service.httpClientGet(this._serviceUrl + '/GetAllLanguagePacks', '')
    .subscribe((result: any) => {
      this.languagePacks = result;
      this.data = this.languagePacks.filter(s => {return s.languageCountry == 'English'})[0]['languageJson'];
      console.log(this.data);
      this.mapLanguageToArray(this.data);
      console.log(this.dataInArray);
      this.parents = this.dataInArray.filter(f => { return f.hasChild });
    },
      error => {
        this.service.errorserver();
      });
  }

  mapLanguageToArray(source){
    for (var parentName in source) {
      if (source.hasOwnProperty(parentName)) {
          var parentObject = source[parentName];
          // create parent object
          var dataInArrayObj = {name: parentName, hasChild: typeof(parentObject) === 'object', childs: []};

          if (typeof(parentObject) === 'object') {
            for (var childName in parentObject) {
              if (parentObject.hasOwnProperty(childName)) {
                var childObject = parentObject[childName];
                // create child object
                var childObj = {name: childName, hasChild: typeof(childObject) === 'object', childs: []}

                if (typeof(childObject) === 'object') {
                  for (var grandChildName in childObject) {
                    if (childObject.hasOwnProperty(grandChildName)) {
                      var grandChildObject = childObject[grandChildName];
                      // create grand child object
                      var grandChildObj = {name: grandChildName, hasChild: typeof(grandChildObject) === 'object', childs: []}

                      if (typeof(grandChildObject) === 'object') {
                        for (var grandGrandChildName in grandChildObject) {
                          if (grandChildObject.hasOwnProperty(grandGrandChildName)) {
                            var grandGrandChildObject = grandChildObject[grandGrandChildName];
                            // create grand grand child object
                            var grandGrandChildObj = {name: grandGrandChildName, hasChild: typeof(grandGrandChildObject) === 'object', childs: []}

                            if (typeof(grandGrandChildObject) === 'object') {
                              for (var grandGrandGrandChildName in grandGrandChildObject) {
                                if (grandGrandChildObject.hasOwnProperty(grandGrandGrandChildName)) {
                                  // create grand grand grand child object
                                  grandGrandChildObj.childs.push(grandGrandGrandChildName);
                                }
                              }
                            }

                            // add grand grand child object
                            grandChildObj.childs.push(grandGrandChildObj);
                          }
                        }
                      }
                      
                      // add grand child object
                      childObj.childs.push(grandChildObj);
                    }
                  }
                }

                // add child object
                dataInArrayObj.childs.push(childObj);
              }
            }
          }

          this.dataInArray.push(dataInArrayObj);
      }
    }
  }

  ngOnInit() {
    this.getDataLanguage();
    this.setToggle();
  }

  setToggle(){
    var toggler = document.getElementsByClassName("caret");
    var i;
    
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  toggleChild(e) {
    e.classList.toggle("caret-down");
    e.parentElement.querySelector(".nested").classList.toggle("active");;
  }

  addElement() {
    debugger;
    var parentName = $("#parentName").val();
    var childName = $("#childName").val();
    var grandChildName = $("#grandChildName").val();
    var grandGrandChildName = $("#grandGrandChildName").val();

    if (this.elementName == '') {
      swal('Empty', 'elementName can not be empty!', 'error');
      return;
    }

    if (this.elementValues.length == 0) {
      swal('Empty', 'You need to insert at least an element name!', 'error');
      return;
    }

    try {
      if (parentName == 'parentRoot') {
        if (this.dataInArray.filter((data) => {return data.name == parentName}).length > 0) {
          swal('Duplicate', 'elementName already exists!', 'error')
          return;
        }
  
        var parentObj = {name: this.elementName, hasChild: false, childs: []};
        this.dataInArray.push(parentObj);
        this.languagePacks.forEach(pack => {
          pack.languageJson[this.elementName] = this.elementValues[pack.languagePackId];
        });
        return;
      }

      var parent = this.dataInArray.filter((data) => {return data.name == parentName})[0];
      var child;
      var grandChild;
      var grandGrandChild;
      if (!!parent) {
        child = parent.childs.filter((data) => {return data.name == childName})[0];
      }
      if (!!child) {
        grandChild = child.childs.filter((data) => {return data.name == grandChildName})[0];
      }
      if (!!grandChild) {
        grandGrandChild = grandChild.childs.filter((data) => {return data.name == grandGrandChildName})[0];
      }

      // add grandGrandChildName
      if (grandGrandChildName != 'grandGrandChildRoot') {
        if (grandGrandChild.childs.filter(f => {return f === this.elementName}).length > 0) {
          swal('Duplicate', 'elementName already exists!', 'error')
          return;
        }
  
        grandGrandChild.childs.push(this.elementName);
        this.languagePacks.forEach(pack => {
          pack.languageJson[parentName][childName][grandChildName][grandGrandChildName][this.elementName] = this.elementValues[pack.languagePackId];
        });
        return;
      }

      // add grandChildName
      if (grandChildName != 'grandChildRoot') {
        if (grandChild.childs.filter(f => {return f.name === this.elementName}).length > 0) {
          swal('Duplicate', 'elementName already exists!', 'error')
          return;
        }
  
        var grandGrandChildObj = {name: this.elementName, hasChild: false, childs: []};
        grandChild.childs.push(grandGrandChildObj);
        this.languagePacks.forEach(pack => {
          pack.languageJson[parentName][childName][grandChildName][this.elementName] = this.elementValues[pack.languagePackId];
        });
        return;
        
      }
    
      // add childName
      if (childName != 'childRoot') {
        if (child.childs.filter(f => {return f.name === this.elementName}).length > 0) {
          swal('Duplicate', 'elementName already exists!', 'error')
          return;
        }
  
        var grandChildObj = {name: this.elementName, hasChild: false, childs: []};
        child.childs.push(grandChildObj);
        this.languagePacks.forEach(pack => {
          pack.languageJson[parentName][childName][this.elementName] = this.elementValues[pack.languagePackId];
        });
        return;
      }

      // add parentName
      if (parentName != 'parentRoot') {
        if (parent.childs.filter(f => {return f.name === this.elementName}).length > 0) {
          swal('Duplicate', 'elementName already exists!', 'error')
          return;
        }
  
        var childObj = {name: this.elementName, hasChild: false, childs: []};
        parent.childs.push(childObj);
        this.languagePacks.forEach(pack => {
          pack.languageJson[parentName][this.elementName] = this.elementValues[pack.languagePackId];
        });
        return;
      }
    }
    finally {
      this.elementName = '';
      this.elementValues = [];
      console.log(this.data);
      console.log(this.dataInArray);
      console.log(this.languagePacks);
      swal('Successful', 'Add new element successful!', 'success');
    }
  }

  onChangeParent(value) {
    var parentArr = this.dataInArray.filter(f => { return f.name == value});
    if (parentArr.length > 0)
      this.childs = parentArr[0].childs.filter(f => { return f.hasChild });
    else {
      this.childs = [];
      this.grandChilds = [];
      this.grandGrandChilds = [];
    }
  }

  onChangeChild(value) {
    var parentName = $("#parentName").val();
    var childArr = this.dataInArray.filter(f => { return f.name == parentName})[0]
                    .childs.filter(f => { return f.hasChild });
    var grandChildArr = childArr.filter(f => { return f.name == value });
    if (grandChildArr.length > 0)
      this.grandChilds = grandChildArr[0].childs.filter(f => { return f.hasChild });
    else {
      this.grandChilds = [];
      this.grandGrandChilds = [];
    }
  }

  onChangeGrandChild(value) {
    var parentName = $("#parentName").val();
    var childName = $("#childName").val();
    var grandChildArr = this.dataInArray.filter(f => { return f.name == parentName})[0]
                        .childs.filter(f => { return f.name == childName})[0]
                        .childs.filter(f => { return f.hasChild });
    var grandGrandChildArr = grandChildArr.filter(f => { return f.name == value });
    if (grandGrandChildArr.length > 0)
      this.grandGrandChilds = grandGrandChildArr[0].childs.filter(f => { return f.hasChild });
    else
      this.grandGrandChilds = [];
  }

  addParent() {
    if(this.newParentName == '') {
      swal('Empty', 'parentName can not be empty!', 'error');
      return;
    }
    if(this.dataInArray.filter(f => {return f.name == this.newParentName}).length > 0) {
      swal('Duplicate', 'parentName already exists (this name already exists at same level)!', 'error');
      return;
    }

    var newElement = {name: this.newParentName, hasChild: true, childs: []};
    this.dataInArray.push(newElement);
    this.parents.push(newElement);
    this.languagePacks.forEach(pack => {
      pack.languageJson[this.newParentName] = {};
    });
    this.newParentName = ''
  }

  removeParent() {
    var parentName = $("#parentName").val();
    if(parentName == 'parentRoot') {
      swal('Denied', 'You can not remove this item!', 'error');
      return;
    }

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this node!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        this.dataInArray = this.dataInArray.filter(f => { return f.name != parentName });
        this.parents = this.parents.filter(f => { return f.name != parentName });
        delete this.data[parentName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName];
        });
        this.childs = [];
        this.grandChilds = [];
        this.grandGrandChilds = [];
      }
    });
  }

  addChild() {
    var parentName = $("#parentName").val();
    if(parentName == 'parentRoot') {
      swal('parentName is Empty', 'Please select parentName at select box first!', 'error');
      return;
    }

    if(this.newChildName == '') {
      swal('Empty', 'childName can not be empty!', 'error');
      return;
    }

    if(this.dataInArray.filter(f => {return f.name == parentName})[0].childs.filter(f => {return f.name == this.newChildName}).length > 0) {
      swal('Duplicate', 'childName already exists (this name already exists at same level)!', 'error');
      return;
    }

    var element = this.dataInArray.filter(f => {return f.name == parentName})[0];
    var elementChild = {name: this.newChildName, hasChild: true, childs: []};
    element.childs.push(elementChild);
    this.childs.push(elementChild);
    this.languagePacks.forEach(pack => {
      pack.languageJson[parentName][this.newChildName] = {};
    });
    this.newChildName = ''
  }

  removeChild() {
    var childName = $("#childName").val();
    if(childName == 'childRoot') {
      swal('Denied', 'You can not remove this item!', 'error');
      return;
    }

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this node!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var parentName = $("#parentName").val();
        var childName = $("#childName").val();
        var parent = this.dataInArray.filter(f => { return f.name == parentName })[0];
        parent.childs = parent.childs.filter(f => { return f.name != childName });
        this.childs = this.childs.filter(f => { return f.name != childName });
        delete this.data[parentName][childName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName];
        });
        this.grandChilds = [];
        this.grandGrandChilds = [];
      }
    });
  }

  addGrandChild() {
    var parentName = $("#parentName").val();
    var childName = $("#childName").val();
    if(childName == 'childRoot') {
      swal('childName is Empty', 'Please select childName at select box first!', 'error');
      return;
    }

    if(this.newGrandChildName == '') {
      swal('Empty', 'grandChildName can not be empty!', 'error');
      return;
    }

    if(this.dataInArray.filter(f => {return f.name == parentName})[0]
        .childs.filter(f => {return f.name == childName})[0]
        .childs.filter(f => {return f.name == this.newGrandChildName}).length > 0) {
      swal('Duplicate', 'grandChildName already exists (this name already exists at same level)!', 'error');
      return;
    }

    var childElement = this.dataInArray.filter(f => {return f.name == parentName})[0].childs.filter(f => {return f.name == childName})[0];
    var elementgrandChild = {name: this.newGrandChildName, hasChild: true, childs: []};
    childElement.childs.push(elementgrandChild);
    this.grandChilds.push(elementgrandChild);
    this.languagePacks.forEach(pack => {
      pack.languageJson[parentName][childName][this.newGrandChildName] = {};
    });
    this.newGrandChildName = ''
  }

  removeGrandChild() {
    var grandChildName = $("#grandChildName").val();
    if(grandChildName == 'grandChildRoot') {
      swal('Denied', 'You can not remove this item!', 'error');
      return;
    }

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this node!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var parentName = $("#parentName").val();
        var childName = $("#childName").val();

        var child = this.dataInArray.filter(f => { return f.name == parentName })[0].childs.filter(f => { return f.name == childName })[0];
        child.childs = child.childs.filter(f => { return f.name != grandChildName });
        
        this.grandChilds = this.grandChilds.filter(f => { return f.name != grandChildName });
        delete this.data[parentName][childName][grandChildName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName][grandChildName];
        });
        this.grandGrandChilds = [];
      }
    });
  }

  addGrandGrandChild() {
    var parentName = $("#parentName").val();
    var childName = $("#childName").val();
    var grandChildName = $("#grandChildName").val();
    if(grandChildName == 'grandChildRoot') {
      swal('grandChildName is Empty', 'Please select grandChildName at select box first!', 'error');
      return;
    }

    if(this.newGrandGrandChildName == '') {
      swal('Empty', 'newGrandGrandChildName can not be empty!', 'error');
      return;
    }


    if(this.dataInArray.filter(f => {return f.name == parentName})[0]
        .childs.filter(f => {return f.name == childName})[0]
        .childs.filter(f => {return f.name == grandChildName})[0]
        .childs.filter(f => {return f.name == this.newGrandGrandChildName}).length > 0) {
      swal('Duplicate', 'grandGrandChildName already exists (this name already exists at same level)!', 'error');
      return;
    }

    var grandChildElement = this.dataInArray.filter(f => {return f.name == parentName})[0]
      .childs.filter(f => {return f.name == childName})[0]
      .childs.filter(f => {return f.name == grandChildName})[0];

    var grandGrandChildElement = {name: this.newGrandGrandChildName, hasChild: true, childs: []};
    grandChildElement.childs.push(grandGrandChildElement);
    this.grandGrandChilds.push(grandGrandChildElement);
    this.languagePacks.forEach(pack => {
      pack.languageJson[parentName][childName][grandChildName][this.newGrandGrandChildName] = {};
    });
    this.newGrandGrandChildName = ''
  }

  removeGrandGrandChild() {
    var grandGrandChildName = $("#grandGrandChildName").val();
    if(grandGrandChildName == 'grandGrandChildRoot') {
      swal('Denied', 'You can not remove this item!', 'error');
      return;
    }

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this node!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var parentName = $("#parentName").val();
        var childName = $("#childName").val();
        var grandChildName = $("#grandChildName").val();

        var granChild = this.dataInArray.filter(f => { return f.name == parentName })[0]
                    .childs.filter(f => { return f.name == childName })[0]
                    .childs.filter(f => { return f.name == grandChildName })[0];
        granChild.childs = granChild.childs.filter(f => { return f.name != grandGrandChildName });
        
        this.grandGrandChilds = this.grandGrandChilds.filter(f => { return f.name != grandGrandChildName });
        delete this.data[parentName][childName][grandChildName][grandGrandChildName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName][grandChildName][grandGrandChildName];
        });
      }
    });
  }

  removeGrandGrandGrandChildElement(parentName, childName, grandChildName, grandGrandChildName, grandGrandGrandChildName) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this element (at all language packs)!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var element = this.dataInArray.filter((data) => {return data.name == parentName})[0]
                      .childs.filter((data) => {return data.name == childName})[0]
                      .childs.filter((data) => {return data.name == grandChildName})[0]
                      .childs.filter((data) => {return data.name == grandGrandChildName})[0];
    
        element.childs = element.childs.filter(f => {return f != grandGrandGrandChildName});
        delete this.data[parentName][childName][grandChildName][grandGrandChildName][grandGrandGrandChildName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName][grandChildName][grandGrandChildName][grandGrandGrandChildName];
        });
      }
    });
  }

  removeGrandGrandChildElement(parentName, childName, grandChildName, grandGrandChildName) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this element (at all language packs)!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var element = this.dataInArray.filter((data) => {return data.name == parentName})[0]
                      .childs.filter((data) => {return data.name == childName})[0]
                      .childs.filter((data) => {return data.name == grandChildName})[0];
    
        element.childs = element.childs.filter(f => {return f.name != grandGrandChildName});
        delete this.data[parentName][childName][grandChildName][grandGrandChildName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName][grandChildName][grandGrandChildName];
        });
      }
    });
  }

  removeGrandChildElement(parentName, childName, grandChildName) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this element (at all language packs)!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var element = this.dataInArray.filter((data) => {return data.name == parentName})[0]
                      .childs.filter((data) => {return data.name == childName})[0];
    
        element.childs = element.childs.filter(f => {return f.name != grandChildName});
        delete this.data[parentName][childName][grandChildName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName][grandChildName];
        });
      }
    });
  }

  removeChildElement(parentName, childName) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this element (at all language packs)!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var element = this.dataInArray.filter((data) => {return data.name == parentName})[0];
    
        element.childs = element.childs.filter(f => {return f.name != childName});
        delete this.data[parentName][childName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName][childName];
        });
      }
    });
  }

  removeParentElement(parentName) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this element (at all language packs)!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        this.dataInArray = this.dataInArray.filter((data) => {return data.name != parentName});
    
        delete this.data[parentName];
        this.languagePacks.forEach(pack => {
          delete pack.languageJson[parentName];
        });
      }
    });
  }

  saveLanguagePacks() {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this element (at all language packs)!",
      type: 'question',
      showCancelButton: true
    })
    .then((willDelete) => {
      if (willDelete) {
        var data = this.languagePacks.map(p => {return {
          languagePackId: p.languagePackId,
          languageJson: JSON.stringify(p.languageJson)
        }});
        
        this.service.httpClientPost(this._serviceUrl + "/SaveAllLanguagePacks", data)
         .subscribe(result => {
            swal('Successful', 'Save language packs successful!', 'success');
            }, error => {
              this.service.errorserver();
            });
      }
    });
  }
}
