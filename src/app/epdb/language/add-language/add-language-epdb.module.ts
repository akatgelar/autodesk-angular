import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLanguageEPDBComponent } from './add-language-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import { DataFilterActivitiesPipe } from './add-language-epdb.component';

export const AddLanguageEPDBRoutes: Routes = [
  {
    path: '',
    component: AddLanguageEPDBComponent,
    data: {
      breadcrumb: 'Add Language',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddLanguageEPDBRoutes),
    SharedModule
  ],
  declarations: [AddLanguageEPDBComponent,DataFilterActivitiesPipe],
  providers:[AppService]
})
export class AddLanguageEPDBModule { }
