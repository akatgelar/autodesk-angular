import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row => (row.unique.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-add-language-epdb',
  templateUrl: './add-language-epdb.component.html',
  styleUrls: [
    './add-language-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddLanguageEPDBComponent implements OnInit {

  private _serviceUrl = "api/Language";
  addlanguage:FormGroup;
  
  constructor(private router: Router, private _http: Http, private service: AppService) {
    //declaration form
    let Language = new FormControl('', Validators.required);
    let FlagLanguage = new FormControl('', Validators.required)

    this.addlanguage = new FormGroup({
      Language: Language,
      FlagLanguage: FlagLanguage
    });
  }

  ngOnInit() { }

  //submit form
  onSubmit() {
    //validation
    this.addlanguage.controls['Language'].markAsTouched();
    this.addlanguage.controls['FlagLanguage'].markAsTouched();

    //on valid
    if (this.addlanguage.valid) {
      //form data
      let data = JSON.stringify(this.addlanguage.value);

      console.log(data);
      
      //action post
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      
      //redirect
      this.router.navigate(['/admin/language/list-language']);
    }
  }

  resetForm() {
    this.addlanguage.reset();
  }


}
