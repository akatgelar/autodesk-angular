import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';

import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-setup-language-epdb',
  templateUrl: './setup-language-epdb.component.html',
  styleUrls: [
    './setup-language-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class SetupLanguageEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Language';
  private _serviceUrlCountry = "api/Countries";
  public datadetail: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  public data: any;
  public datamodule: any;
  languageselect: string = '';
  moduleselect: boolean = false;
  submoduleselect: boolean = false;
  submoduleselectcountry: boolean = false;
  submoduleselectorganization: boolean = false;
  submoduleselectsite: boolean = false;
  submoduleselectcontact: boolean = false;
  submenuselect: boolean = false;
  menuselect: boolean = false;
  submenuselectcontent: boolean = false;
  modulearr = [];
  submodulearr = [];
  menuarr = [];
  submenuarr = [];
  contentarr = [];
  public changevalue: string = '';
  public itemvalue: string = '';

  public loading = false;

  useraccesdata:any;

  constructor(public session: SessionService, private router: Router, private _http: Http, private service: AppService, private route: ActivatedRoute) { 

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

  }

  getDataLanguage() {
    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + this.languageselect, data)
      .subscribe(result => {
        this.data = result;
        // console.log(this.data);
      },
        error => {
          this.service.errorserver();
          this.data = '';
        });
  }

  ngOnInit() {
    //get data json
    this.languageselect = this.route.snapshot.params['language'];

    //Untuk set filter terakhir hasil pencarian
    if (!(localStorage.getItem("filter") === null)) {
      this.loading = true;
      var item = JSON.parse(localStorage.getItem("filter"));

      this.getDataLanguage();

      // console.log(item)

      setTimeout(() => {
        this.modulearr = item.modulearr.sort((a,b)=>{
          if(a.name > b.name)
            return 1
          if(a.name < b.name)
            return -1
          else return 0
        });
        this.moduleval = item.moduleval;
        this.submoduleval = item.submoduleval;
        this.menueval = item.menueval;
        this.submenueval = item.submenueval;
        this.pickmodule(item.moduleval);
        this.picksubmodule(item.submoduleval);
        this.pickmenu(item.menueval);
        this.picksubmenucontent(item.submenueval);
      }, 1000)

      setTimeout(() => {
        localStorage.removeItem("filter");
        this.loading = false;
      }, 2000)
    } else {
      this.getDataLanguage();

      // get data country
      // var datadetail = '';
      // this.service.httpClientGet(this._serviceUrlCountry, datadetail).subscribe(result => {
      //   if (result == null) {
      //     this.datadetail = '';
      //   }
      //   else {
      //     this.datadetail = result;
      //   }
      // },
      // error => {
      //   this.service.errorserver();
      //   this.datadetail = '';
      // });

      //inisialitation module
      this.modulearr =
        [
          {
            "value": "general",
            "name": "General"
          },
          {
            "value": "auth",
            "name": "Authentication"
          },
          {
            "value": "list_menu",
            "name": "List Menu"
          },
          {
            "value": "profile",
            "name": "Profile"
          },
          {
            "value": "admin",
            "name": "Admin"
          },
          {
            "value": "manage-partner",
            "name": "Manage Partner"
          },
          // {
          //   "value": "epdb",
          //   "name": "EPDB"
          // },
          {
            "value": "eva",
            "name": "EVA"
          },
          {
            "value": "pdb-report",
            "name": "PDB Report"
          },
          {
            "value": "evaluation-report",
            "name": "Evaluation Report"
          },
          {
            "value": "student",
            "name": "Student"
          },
          {
            "value": "role",
            "name": "Role"
          }
        ].sort((a,b)=>{
          if(a.name > b.name)
            return 1
          if(a.name < b.name)
            return -1
          else return 0
        })
    }
  }

  // Sub module 1
  pickmodule(value) {
    this.moduleselect = true;
    // if (value == "eim") {
    //   this.submenuselectcontent = false;
    //   this.menuselect = false;
    //   this.submodulearr =
    //     [
    //       // {
    //       //   "value": "eim-profile",
    //       //   "name": "Profile"
    //       // },
    //       // {
    //       //   "value": "eim-change-password",
    //       //   "name": "Change Password"
    //       // },
    //       // {
    //       //   "value": "eim-change-email",
    //       //   "name": "Change Email"
    //       // }
    //     ]
    // }
    // else if (value == "epdb") {
    //   this.submenuselectcontent = false;
    //   this.menuselect = false;
    //   this.submodulearr =
    //     [
    //       {
    //         "value": "epdb-dashboard",
    //         "name": "Dashboard"
    //       },
    //       {
    //         "value": "epdb-admin",
    //         "name": "Admin"
    //       },
    //       {
    //         "value": "epdb-user",
    //         "name": "User"
    //       },
    //       {
    //         "value": "epdb-manager",
    //         "name": "Manage"
    //       },
    //       {
    //         "value": "epdb-role",
    //         "name": "Role"
    //       },
    //       {
    //         "value": "epdb-report",
    //         "name": "Report ePDB"
    //       }
    //     ]
    // }
    if (value == "eva") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          // {
          //   "value": "eva-dashboard",
          //   "name": "Dashboard"
          // },
          {
            "value": "eva-manage-evaluation",
            "name": "Manage Evaluation"
          },
          {
            "value": "eva-manage-course",
            "name": "Manage Course"
          },
          {
            "value": "eva-manage-student-info",
            "name": "Manage Student Info"
          },
          {
            "value": "eva-manage-certificate",
            "name": "Manage Certificate"
          },
          // {
          //   "value": "eva-report-eva",
          //   "name": "Report Evaluation"
          // }
        ]
    }
    // else if (value == "eim") {
    //   this.moduleselect = false;
    //   this.submoduleselect = false;
    //   this.submenuselectcontent = false;
    //   this.menuselect = false;
    //   this.submodulearr =
    //     [
    //       {
    //         "value": "eim-dashboard",
    //         "name": "Dashboard"
    //       },
    //       {
    //         "value": "eim-profile",
    //         "name": "My Profile"
    //       }
    //     ]
    // }
    else if (value == "student") {
      this.moduleselect = false;
      this.submoduleselect = false;
      this.submenuselect = false;
      this.submenuselectcontent = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"my_course":',
            "value": this.data.menu_course.my_course,
            "name": "My Course"
          },
          {
            "attribut": '"history_course":',
            "value": this.data.student.my_course.history_course,
            "name": "History Course"
          },
          {
            "attribut": '"next_course":',
            "value": this.data.menu_course.next_course,
            "name": "Next Course"
          },
          {
            "attribut": '"course_date":',
            "value": this.data.student.my_course.course_date,
            "name": "Course Date"
          },
          {
            "attribut": '"completion_course_date":',
            "value": this.data.student.my_course.completion_course_date,
            "name": "Completion Course"
          },
          {
            "attribut": '"completion_date_on":',
            "value": this.data.student.my_course.completion_date_on,
            "name": "Completion Date On"
          },
          {
            "attribut": '"survey_link":',
            "value": this.data.student.my_course.survey_link,
            "name": "Survey Link"
          },
          {
            "attribut": '"download_certificate":',
            "value": this.data.student.my_course.download_certificate,
            "name": "Download Certificate"
          },
          {
            "attribut": '"tool_tip_survey":',
            "value": this.data.student.my_course.tool_tip_survey,
            "name": "Survey Evaluation"
          },
          {
            "attribut": '"survey_closed":',
            "value": this.data.student.my_course.survey_closed,
            "name": "Survey Closed"
          },
          {
            "attribut": '"click_here":',
            "value": this.data.student.my_course.click_here,
            "name": "Download Click here"
          },
          {
            "attribut": '"search_course":',
            "value": this.data.menu_course.search_course,
            "name": "Search Course"
          },
          {
            "attribut": '"course_id":',
            "value": this.data.eva.manage_course.list_course.course_id,
            "name": "Course ID"
          },
          {
            "attribut": '"course_name":',
            "value": this.data.eva.manage_course.list_course.course_name,
            "name": "Course Name"
          },
          {
            "attribut": '"instructor_id":',
            "value": this.data.eva.manage_course.list_course.instructor_id,
            "name": "Instructor ID"
          },
          {
            "attribut": '"instructor_name":',
            "value": this.data.eva.manage_course.list_course.instructor_name,
            "name": "Instructor Name"
          },
          {
            "attribut": '"site_name":',
            "value": this.data.epdb.manage.site.detail.site_name,
            "name": "Site Name"
          },
          {
            "attribut": '"survey_status":',
            "value": this.data.student.my_course.survey_status,
            "name": "Survey Status"
          },
          {
            "attribut": '"dashboard_student":',
            "value": this.data.menu_dashboard.dashboard_student,
            "name": "Dashboard Student"
          },
          {
            "attribut": '"main_course":',
            "value": this.data.dashboard.student.main_course,
            "name": "Total Courses"
          },
          {
            "attribut": '"finished_course":',
            "value": this.data.dashboard.student.finished_course,
            "name": "Finished Course"
          },
          {
            "attribut": '"your_date_birth":',
            "value": this.data.login_register.your_date_birth,
            "name": "Your Date Birth"
          },
          {
            "attribut": '"expected_graduation_date":',
            "value": this.data.login_register.expected_graduation_date,
            "name": "Expected Graduation Date"
          },
          {
            "attribut": '"due_days":',
            "value": this.data.student.my_course.due_days,
            "name": "Due Days"
          }
        ]
    
      /* issue 21112018 - error when pick module student & label student portal translate */

      if(this.data.student.hasOwnProperty('search_course')){
        this.contentarr.push({
          "attribut": '"popupMesage":',
          "value": this.data.student.search_course.popupMesage,
          "name": "Pop Up Mesage"
        })
      }

      /* end line issue 21112018 - error when pick module student & label student portal translate */

    }
    else if (value == "auth") {
      this.moduleselect = false;
      this.submoduleselect = false;
      this.submenuselect = false;
      this.submenuselectcontent = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"register":',
            "value": this.data.login_register.register,
            "name": "Create your account"
          },
          {
            "attribut": '"btn_register":',
            "value": this.data.login_register.btn_register,
            "name": "Label the Register Button"
          },
          {
            "attribut": '"sign_in":',
            "value": this.data.login_register.sign_in,
            "name": "Sign in with your account"
          },
          {
            "attribut": '"keep_me":',
            "value": this.data.login_register.keep_me,
            "name": "Keep me signed in"
          },
          {
            "attribut": '"btn_login":',
            "value": this.data.login_register.btn_login,
            "name": "Label The Login Button"
          },
          {
            "attribut": '"forgot_account":',
            "value": this.data.login_register.forgot_account,
            "name": "Forgot Account"
          },
          {
            "attribut": '"info_admin1":',
            "value": this.data.login_register.info_admin1,
            "name": "Autodesk Partner Database"
          },
          {
            "attribut": '"info_admin2":',
            "value": this.data.login_register.info_admin2,
            "name": "Training Evaluation System"
          },
          {
            "attribut": '"info_admin3":',
            "value": this.data.login_register.info_admin3,
            "name": "Partner Access to Autodesk Education"
          },
          {
            "attribut": '"info_admin4":',
            "value": this.data.login_register.info_admin4,
            "name": "Training Programs Services"
          },
          {
            "attribut": '"info_admin5":',
            "value": this.data.login_register.info_admin5,
            "name": "Access a range of Autodesk Education services"
          },
          {
            "attribut": '"info_admin6":',
            "value": this.data.login_register.info_admin6,
            "name": "including facilities for users of these services"
          },
          {
            "attribut": '"info_admin7":',
            "value": this.data.login_register.info_admin7,
            "name": "to maintain core profile data"
          },
          {
            "attribut": '"student_login_link":',
            "value": this.data.login_register.student_login_link,
            "name": "Sign in to Student Page"
          },
          // {
          //   "attribut": '"info_admin8":',
          //   "value": this.data.login_register.info_admin8,
          //   "name": "Access a range of training and education services (in Student Portal)"
          // },
          // {
          //   "attribut": '"info_admin9":',
          //   "value": this.data.login_register.info_admin9,
          //   "name": "including facilities for users to maintain core (in Student Portal)"
          // },
          // {
          //   "attribut": '"info_admin10":',
          //   "value": this.data.login_register.info_admin10,
          //   "name": "profile data & course certificates. (in Student Portal)"
          // },
          {
            "attribut": '"info_student1":',
            "value": this.data.login_register.info_student1,
            "name": "Autodesk Training Evaluation System (Student Portal)"
          },
          {
            "attribut": '"info_student2":',
            "value": this.data.login_register.info_student2,
            "name": "Partner Access to Autodesk Education ... (Student Portal)"
          },
          {
            "attribut": '"info_student3":',
            "value": this.data.login_register.info_student3,
            "name": "Access a range of Autodesk Education services ... (Student Portal)"
          },
          {
            "attribut": '"your_email":',
            "value": this.data.login_register.your_email,
            "name": "Your Email Address"
          },
          {
            "attribut": '"password":',
            "value": this.data.login_register.password,
            "name": "Password"
          },
          {
            "attribut": '"your_first_name":',
            "value": this.data.login_register.your_first_name,
            "name": "Your First Name"
          },
          {
            "attribut": '"your_last_name":',
            "value": this.data.login_register.your_last_name,
            "name": "Your Last Name"
          },
          {
            "attribut": '"select_your_country":',
            "value": this.data.login_register.select_your_country,
            "name": "Select Your Country"
          },
          {
            "attribut": '"select_language":',
            "value": this.data.login_register.select_language,
            "name": "Select Language"
          },
          {
            "attribut": '"your_date_birth":',
            "value": this.data.login_register.your_date_birth,
            "name": "Select Date Birth"
          },
          {
            "attribut": '"you_forget":',
            "value": this.data.login_register.you_forget,
            "name": "Which did you forget"
          },
          {
            "attribut": '"username":',
            "value": this.data.login_register.username,
            "name": "Username"
          },
          {
            "attribut": '"your_username":',
            "value": this.data.login_register.your_username,
            "name": "Your Username"
          },
          {
            "attribut": '"back_login":',
            "value": this.data.login_register.back_login,
            "name": "Back to Login page"
          },
          {
            "attribut": '"dont_have_account":',
            "value": this.data.login_register.dont_have_account,
            "name": "You don't have account on this system"
          },
          {
            "attribut": '"btn_resetsend_email":',
            "value": this.data.login_register.btn_resetsend_email,
            "name": "Button Reset and send me an Email"
          },
          {
            "attribut": '"thank_you":',
            "value": this.data.login_register.thank_you,
            "name": "Thank you and enjoy our website"
          },
          {
            "attribut": '"terminate_account":',
            "value": this.data.login_register.terminate_account,
            "name": "Terminate My Account"
          },
          {
            "attribut": '"acknowledge":',
            "value": this.data.login_register.acknowledge,
            "name": "I acknowledge that upon successful termination, all my certificates, courses' details and other"
          },
          {
            "attribut": '"information_related":',
            "value": this.data.login_register.information_related,
            "name": "information related to my account will be fully removed from the system"
          },
          {
            "attribut": '"please_allow":',
            "value": this.data.login_register.please_allow,
            "name": "Please allow 2-3 working days for the terminal process"
          },
          {
            "attribut": '"if_you_have":',
            "value": this.data.login_register.if_you_have,
            "name": "If you have any question or comments, please call your local training center for more information"
          },
          {
            "attribut": '"understand":',
            "value": this.data.login_register.understand,
            "name": "Yes, I Understand"
          },
          {
            "attribut": '"btn_terminate":',
            "value": this.data.login_register.btn_terminate,
            "name": "Terminate"
          },
          {
            "attribut": '"process_terminate":',
            "value": this.data.login_register.process_terminate,
            "name": "Your account is under process terminated"
          }
        ]
    }
    else if (value == "general") {
      this.moduleselect = false;
      this.submoduleselect = false;
      this.submenuselect = false;
      this.submenuselectcontent = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"btn_submit":',
            "value": this.data.general.btn_submit,
            "name": "Label Submit Button"
          },
          {
            "attribut": '"btn_save":',
            "value": this.data.general.btn_save,
            "name": "Label Save Button"
          },
          {
            "attribut": '"btn_save_change":',
            "value": this.data.general.btn_save_change,
            "name": "LabelSave Change Button "
          },
          {
            "attribut": '"btn_reset":',
            "value": this.data.general.btn_reset,
            "name": "Label Reset Button"
          },
          {
            "attribut": '"btn_cancel":',
            "value": this.data.general.btn_cancel,
            "name": "Label Cancel Button"
          },
          {
            "attribut": '"btn_run_query":',
            "value": this.data.general.btn_run_query,
            "name": "LabelRun Query Button "
          },
          {
            "attribut": '"btn_stop_query":',
            "value": this.data.general.btn_stop_query,
            "name": "Label Stop Query Button"
          },
          {
            "attribut": '"btn_search":',
            "value": this.data.general.btn_search,
            "name": "Label Search Button"
          },
          {
            "attribut": '"btn_preview":',
            "value": this.data.general.btn_preview,
            "name": "Label Preview Button"
          },
          {
            "attribut": '"btn_close":',
            "value": this.data.general.btn_close,
            "name": "Label Close Button"
          },
          {
            "attribut": '"btn_back":',
            "value": this.data.general.btn_back,
            "name": "Label Back Button"
          },
          {
            "attribut": '"btn_help":',
            "value": this.data.general.btn_help,
            "name": "Label Help Button"
          },
          {
            "attribut": '"btn_add":',
            "value": this.data.general.btn_add,
            "name": "Label Add Button"
          },
          {
            "attribut": '"btn_update":',
            "value": this.data.general.btn_update,
            "name": "Update"
          },
          {
            "attribut": '"show":',
            "value": this.data.general.show,
            "name": "Label Show"
          },
          {
            "attribut": '"entries":',
            "value": this.data.general.entries,
            "name": "Label Entries"
          },
          {
            "attribut": '"search":',
            "value": this.data.general.search,
            "name": "Label Search"
          },
          {
            "attribut": '"action":',
            "value": this.data.general.action,
            "name": "Label Action"
          },
          {
            "attribut": '"status":',
            "value": this.data.general.status,
            "name": "Label Status"
          },
          {
            "attribut": '"description":',
            "value": this.data.general.description,
            "name": "Label Description"
          },
          {
            "attribut": '"date":',
            "value": this.data.general.date,
            "name": "Label Date"
          },
          {
            "attribut": '"start_date":',
            "value": this.data.general.start_date,
            "name": "Label Start Date"
          },
          {
            "attribut": '"end_date":',
            "value": this.data.general.end_date,
            "name": "Label End Date"
          },
          {
            "attribut": '"select":',
            "value": this.data.general.select,
            "name": "Label Select"
          },
          {
            "attribut": '"select_all":',
            "value": this.data.general.select_all,
            "name": "Label Select All"
          },
          {
            "attribut": '"tool_tip_view":',
            "value": this.data.general.tool_tip_view,
            "name": "Tooltip Viwe"
          },
          {
            "attribut": '"tool_tip_edit":',
            "value": this.data.general.tool_tip_edit,
            "name": "Tooltip Edit"
          },
          {
            "attribut": '"tool_tip_delete":',
            "value": this.data.general.tool_tip_delete,
            "name": "Tooltip Delete"
          },
          {
            "attribut": '"tool_tip_disable":',
            "value": this.data.general.tool_tip_disable,
            "name": "Tooltip Disable"
          },
          {
            "attribut": '"date_added":',
            "value": this.data.general.date_added,
            "name": "Date Added"
          },
          {
            "attribut": '"last_admin":',
            "value": this.data.general.last_admin,
            "name": "Last Admin"
          },
          {
            "attribut": '"admin_added":',
            "value": this.data.general.admin_added,
            "name": "Admin Added"
          },
          {
            "attribut": '"admin_note":',
            "value": this.data.general.admin_note,
            "name": "Admin Notes"
          },
          {
            "attribut": '"created_by":',
            "value": this.data.general.created_by,
            "name": "Created By"
          },
          {
            "attribut": '"created_date":',
            "value": this.data.general.created_date,
            "name": "Created Date"
          },
          {
            "attribut": '"added_by":',
            "value": this.data.general.added_by,
            "name": "Added By"
          },
          {
            "attribut": '"type":',
            "value": this.data.general.type,
            "name": "Type"
          },
          {
            "attribut": '"name":',
            "value": this.data.general.name,
            "name": "Name"
          },
          {
            "attribut": '"created":',
            "value": this.data.general.created,
            "name": "Created"
          },
          {
            "attribut": '"modified":',
            "value": this.data.general.modified,
            "name": "Modified"
          },
          {
            "attribut": '"cant_find_data":',
            "value": this.data.general.cant_find_data,
            "name": "Can't find Data"
          },
          {
            "attribut": '"loading_data":',
            "value": this.data.general.loading_data,
            "name": "Loading Data ..."
          },
          {
            "attribut": '"data_empty":',
            "value": this.data.general.data_empty,
            "name": "Data Empty"
          },
          {
            "attribut": '"note_value":',
            "value": this.data.general.note_value,
            "name": "Note / Value"
          },
          {
            "attribut": '"note":',
            "value": this.data.general.note,
            "name": "Notes"
          },
          // {
          //   "attribut": '"choose_language":',
          //   "value": this.data.general.choose_language,
          //   "name": "Choose Language"
          // },
          {
            "attribut": '"last_modified_by":',
            "value": this.data.general.last_modified_by,
            "name": "Last Modified By"
          },
          {
            "attribut": '"date_modified":',
            "value": this.data.general.date_modified,
            "name": "Date Modified"
          },
          {
            "attribut": '"and":',
            "value": this.data.general.and,
            "name": "And"
          },
          {
            "attribut": '"general_information":',
            "value": this.data.general.general_information,
            "name": "General Information"
          },
          {
            "attribut": '"upload":',
            "value": this.data.general.upload,
            "name": "Upload"
          },
          {
            "attribut": '"year":',
            "value": this.data.general.year,
            "name": "Year"
          }
        ]

        /* issue 21112018 - error when pick module student & label student portal translate */

        if(this.data.general.hasOwnProperty('yes_sure')){
          this.contentarr.push({
            "attribut": '"yes_sure":',
            "value": this.data.general.yes_sure,
            "name": "Yes, Sure"
          })
        }

        if(this.data.general.hasOwnProperty('confirmation')){
          this.contentarr.push({
            "attribut": '"confirmation":',
            "value": this.data.general.confirmation,
            "name": "Confirmation"
          })
        }

        /* end line issue 21112018 - error when pick module student & label student portal translate */

    }
    else if (value == "student") {
      this.moduleselect = false;
      this.submoduleselect = false;
      this.submenuselect = false;
      this.menuselect = false;
      this.submenuselectcontent = false;
    }
    else if (value == "list_menu") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          {
            "value": "menu-dashboard",
            "name": "Menu Dashboard"
          },
          {
            "value": "menu-admin",
            "name": "Menu Admin"
          },
          // {
          //   "value": "epdb-user",
          //   "name": "User"
          // },
          {
            "value": "menu-manage",
            "name": "Menu Manage Partner"
          },
          {
            "value": "menu-manage-evaluation",
            "name": "Menu Manage Evaluation"
          },
          {
            "value": "menu-manage-course",
            "name": "Menu Manage Course"
          },
          {
            "value": "menu-manage-student-information",
            "name": "Menu Manage Student Information"
          },
          {
            "value": "menu-manage-certificate",
            "name": "Menu Manage Certificate"
          },
          {
            "value": "menu-Course",
            "name": "Menu Course"
          },
          {
            "value": "menu-report-epdb",
            "name": "Menu PDB Report"
          },
          {
            "value": "menu-report-eva",
            "name": "Menu Evaluation Report"
          },
          {
            "value": "menu-Role",
            "name": "Menu Role"
          }
        ]
    }

    // New Profile
    else if (value == "profile") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          {
            "value": "profile-my-profile",
            "name": "My Profile"
          },
          {
            "value": "profile-change-password",
            "name": "Change Password"
          },
          {
            "value": "profile-change-email",
            "name": "Change Email"
          }
        ]
    }

    else if (value == "admin") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          {
            "value": "admin-activities",
            "name": "Activities"
          },
          {
            "value": "admin-product",
            "name": "Product"
          },
          {
            "value": "admin-sku",
            "name": "SKU"
          },
          {
            "value": "admin-glossary",
            "name": "Glossary"
          },
          {
            "value": "admin-country",
            "name": "Country"
          },
          {
            "value": "admin-market-type",
            "name": "Market Type"
          },
          {
            "value": "admin-variables",
            "name": "Variables"
          },
          {
            "value": "admin-partner-type",
            "name": "Partner Type"
          },
          {
            "value": "admin-distributor",
            "name": "Distributor"
          },
          {
            "value": "admin-currency",
            "name": "Currency"
          },
          {
            "value": "admin-language",
            "name": "Language"
          },
          {
            "value": "admin-email-body",
            "name": "Email Body"
          }
        ]
    }

    // New Manage Partner
    else if (value == "manage-partner") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          {
            "value": "manage-organization",
            "name": "Organization"
          },
          {
            "value": "manage-site",
            "name": "Site"
          },
          {
            "value": "manage-contact",
            "name": "Contact"
          }
        ]
    }

    // New Role
    else if (value == "role") {
      this.moduleselect = false;
      this.submoduleselect = false;
      this.submenuselect = false;
      this.submenuselectcontent = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"list_access":',
            "value": this.data.epdb.role.list_access,
            "name": "List Access"
          },
          {
            "attribut": '"list_access_role":',
            "value": this.data.epdb.role.list_access_role,
            "name": "List Access Role"
          },
          {
            "attribut": '"add_new_role":',
            "value": this.data.epdb.role.add_new_role,
            "name": "Add Role "
          },
          {
            "attribut": '"btn_add_role":',
            "value": this.data.epdb.role.btn_add_role,
            "name": "Label Add Role Button"
          },
          {
            "attribut": '"sub_menu":',
            "value": this.data.epdb.role.sub_menu,
            "name": "Sub Menu"
          },
          {
            "attribut": '"new_role":',
            "value": this.data.epdb.role.new_role,
            "name": "New Role"
          },
          {
            "attribut": '"role_id":',
            "value": this.data.epdb.role.role_id,
            "name": "Role ID"
          },
          {
            "attribut": '"role_name":',
            "value": this.data.epdb.role.role_name,
            "name": "Role Name"
          },
          {
            "attribut": '"chck_access":',
            "value": this.data.epdb.role.access,
            "name": "View"
          },
          {
            "attribut": '"chck_write":',
            "value": this.data.general.chck_write,
            "name": "Write"
          },
          {
            "attribut": '"chck_delete":',
            "value": this.data.general.chck_delete,
            "name": "Delete"
          }
        ]
    }

    // New PDB Report
    else if (value == "pdb-report") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          {
            "value": "pdb-report-organization",
            "name": "Organization Report"
          },
          {
            "value": "pdb-report-site",
            "name": "Site Report"
          },
          {
            "value": "pdb-report-contact",
            "name": "Contact Report"
          },
          {
            "value": "pdb-report-org-journal-entry",
            "name": "Organization Journal Entry Report"
          },
          {
            "value": "pdb-report-site-journal-entry",
            "name": "Site Journal Entry Report"
          },
          {
            "value": "pdb-report-invoice-report",
            "name": "Invoice Report"
          },
          {
            "value": "pdb-report-qualification-by-contact",
            "name": "Qualification by Contact Report"
          },
          {
            "value": "pdb-report-site-attribute",
            "name": "Site Attribute Report"
          },
          {
            "value": "pdb-report-contact-attribute",
            "name": "Contact Attribute Report"
          },
          {
            "value": "pdb-atcaap-acc-count-report",
            "name": "ATC AAP Accreditation Counts Report"
          },
          {
            "value": "pdb-database-integrity-report",
            "name": "Database Integrity Report"
          },
          {
            "value": "pdb-history-report",
            "name": "History Report"
          }
        ]
    }
    else if (value == "evaluation-report") {
      this.submenuselectcontent = false;
      this.menuselect = false;
      this.submodulearr =
        [
          {
            "value": "evaluation-report-organization",
            "name": "Organization"
          },
          {
            "value": "evaluation-report-performance-overview",
            "name": "Performance Overview"
          },
          {
            "value": "evaluation-report-instructor-performance",
            "name": "Instructor Performance"
          },
          {
            "value": "evaluation-report-evaluation-response",
            "name": "Evaluation Responses"
          },
          {
            "value": "evaluation-report-student-search",
            "name": "Student Search"
          },
          {
            "value": "evaluation-report-download-site-evaluation",
            "name": "Download Site Evaluation"
          },
          {
            "value": "evaluation-report-channel-performnace",
            "name": "Channel Performance"
          },
          {
            "value": "evaluation-report-submission-language",
            "name": "Submissions by Language"
          },
          {
            "value": "evaluation-report-audit",
            "name": "Audit"
          }
        ]
    }

    else {
      this.submoduleselect = false;
      this.submenuselect = false;
      this.menuselect = false;
      this.submenuselectcontent = false;
    }
  }

  // sub module 2
  picksubmodule(value) {
    this.submoduleselect = true;
    if (value == "epdb-dashboard") {
      this.menuarr =
        [
          {
            "value": "epdb-dashboard-chart",
            "name": "Chart"
          },
          {
            "value": "epdb-dashboard-head_table",
            "name": "Header On Table"
          }
        ]
    }
    else if (value == "epdb-admin") {
      this.menuarr =
        [
          {
            "value": "epdb-admin-activities",
            "name": "Activities"
          },
          {
            "value": "epdb-admin-product",
            "name": "Product"
          },
          {
            "value": "epdb-admin-sku",
            "name": "SKU"
          },
          {
            "value": "epdb-admin-glosarry",
            "name": "Glosarry"
          },
          {
            "value": "epdb-admin-country",
            "name": "Country"
          },
          {
            "value": "epdb-admin-market-type",
            "name": "Market Type"
          },
          // {
          //   "value": "epdb-admin-site-service",
          //   "name": "Site Services"
          // },
          {
            "value": "epdb-admin-variable",
            "name": "Variables"
          },
          {
            "value": "epdb-admin-sub-partner-type",
            "name": "Sub Partner Type"
          },
          {
            "value": "epdb-admin-partner-type",
            "name": "Partner Type"
          },
          {
            "value": "epdb-admin-distributor",
            "name": "Distributor"
          },
          {
            "value": "epdb-admin-currency-conversion",
            "name": "Currency Conversion"
          },
          {
            "value": "epdb-admin-setup-language",
            "name": "Setup Language"
          },
          {
            "value": "epdb-admin-email-body",
            "name": "Email Body"
          }
        ];
    }
    // else if (value == "epdb-user") {
    //   this.menuarr =
    //     [
    //       {
    //         "value": "epdb-user-view",
    //         "name": "View User Information"
    //       },
    //       {
    //         "value": "epdb-user-change",
    //         "name": "Change User Information"
    //       }
    //     ]
    // }
    else if (value == "epdb-manager") {
      this.menuarr =
        [
          {
            "value": "epdb-manager-organization",
            "name": "Organization"
          },
          {
            "value": "epdb-manager-site",
            "name": "Site"
          },
          {
            "value": "epdb-manager-contact",
            "name": "Contact"
          }
          // {
          //   "value": "epdb-manager-aci",
          //   "name": "ACI"
          // }
        ]
    }
    // else if (value == "eva-dashboard") {
    //   this.submoduleselect = false;
    //   this.menuselect = true;
    //   this.submenuselect = false;
    //   this.contentarr =
    //   [

    //   ]
    // }



    // New Manage Org
    else if (value == "manage-organization") {
      this.menuarr =
        [
          {
            "value": "manage-organization-search",
            "name": "Search Organization"
          },
          {
            "value": "manage-organization-detail",
            "name": "Detail Organization"
          },
          {
            "value": "manage-organization-add-edit",
            "name": "Add & Edit Organization"
          },
          {
            "value": "manage-organization-add-partner-type",
            "name": "Partner Type"
          },
          {
            "value": "manage-organization-journal-entries",
            "name": "Add Organization Journal Entries"
          },
          {
            "value": "manage-invoice",
            "name": "Add Invoice"
          }
        ]
    }

    // New Manage Site
    else if (value == "manage-site") {
      this.menuarr =
        [
          {
            "value": "manage-site-search",
            "name": "Search Site"
          },
          {
            "value": "manage-site-detail",
            "name": "Detail Site"
          },
          {
            "value": "manage-site-add-edit",
            "name": "Add & Edit Site"
          },
          {
            "value": "manage-site-add-partner-type",
            "name": "Partner Type"
          },
          {
            "value": "manage-site-contact-qualification",
            "name": "Contact Qualification"
          },
          {
            "value": "manage-site-journal-entries",
            "name": "Site Journal Entries"
          }
        ]
    }

    // New Manage Contact
    else if (value == "manage-contact") {
      this.menuarr =
        [
          {
            "value": "manage-contact-search",
            "name": "Search Contact"
          },
          {
            "value": "manage-contact-detail",
            "name": "Detail Contact"
          },
          {
            "value": "manage-contact-add-edit",
            "name": "Add & Edit Contact"
          },
          {
            "value": "manage-contact-site-affiliation",
            "name": "Site Affiliation"
          },
          // {
          //   "value": "manage-contact-journal-entries",
          //   "name": "Contact Journal Entries"
          // }
        ]
    }

    else if (value == "eva-manage-evaluation") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"list_survey_template":',
            "value": this.data.eva.manage_evaluation.list_survey_template,
            "name": "List Survey Template"
          },
          {
            "attribut": '"btn_add_template":',
            "value": this.data.eva.manage_evaluation.btn_add_template,
            "name": "Add Template"
          },
          {
            "attribut": '"evaluation_template_code":',
            "value": this.data.eva.manage_evaluation.evaluation_template_code,
            "name": "Evaluation Template Code"
          },
          {
            "attribut": '"evaluation_template_title":',
            "value": this.data.eva.manage_evaluation.evaluation_template_title,
            "name": "Evaluation Template Title"
          },
          {
            "attribut": '"year":',
            "value": this.data.eva.manage_evaluation.year,
            "name": "Year"
          },
          {
            "attribut": '"template_builder":',
            "value": this.data.eva.manage_evaluation.template_builder,
            "name": "Template Builder"
          },
          {
            "attribut": '"add_evaluation_template_data":',
            "value": this.data.eva.manage_evaluation.add_evaluation_template_data,
            "name": "Add Evaluation Template Data"
          },
          {
            "attribut": '"edit_evaluation_template_data":',
            "value": this.data.eva.manage_evaluation.edit_evaluation_template_data,
            "name": "Edit Evaluation Template Data"
          },
          {
            "attribut": '"evaluation_id":',
            "value": this.data.eva.manage_evaluation.evaluation_id,
            "name": "Evaluation ID"
          }
        ]
    }
    else if (value == "eva-manage-course") {
      this.menuarr =
        [
          {
            "value": "eva-manage-course-list",
            "name": "Search Course"
          },
          {
            "value": "eva-manage-course-detail",
            "name": "Detail Course"
          },
          {
            "value": "eva-manage-course-add",
            "name": "Add Course"
          },
          {
            "value": "eva-manage-course-question",
            "name": "Question"
          }
          // {
          //   "value": "eva-manage-course-question",
          //   "name": "Question"
          // }
        ]
    }
    else if (value == "eva-manage-student-info") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"list_student":',
            "value": this.data.eva.manage_student_info.list_student,
            "name": "Title in List Student"
          },
          {
            "attribut": '"student_id":',
            "value": this.data.eva.manage_student_info.student_id,
            "name": "Student ID"
          },
          {
            "attribut": '"student_name":',
            "value": this.data.eva.manage_student_info.student_name,
            "name": "Student Name"
          },
          {
            "attribut": '"company":',
            "value": this.data.eva.manage_student_info.company,
            "name": "Company"
          },
          {
            "attribut": '"student_detail_information":',
            "value": this.data.eva.manage_student_info.student_detail_information,
            "name": "Title in Student Detail Information"
          },
          {
            "attribut": '"address":',
            "value": this.data.eva.manage_student_info.address,
            "name": "Address"
          },
          {
            "attribut": '"edit_student_information":',
            "value": this.data.eva.manage_student_info.edit_student_information,
            "name": "Title in Edit Student Information"
          },
          {
            "attribut": '"confirm_email":',
            "value": this.data.eva.manage_student_info.confirm_email,
            "name": "Confirm Email"
          },
          {
            "attribut": '"confirm_student":',
            "value": this.data.eva.manage_course.confirm_student.confirm_student,
            "name": "Confirm Final Student"
          },
          {
            "attribut": '"terminate_account":',
            "value": this.data.eva.manage_student_info.terminate_account,
            "name": "Terminate Account Student"
          },
          {
            "attribut": '"list_terminate_account":',
            "value": this.data.eva.manage_student_info.list_terminate_account,
            "name": "List Request Terminate Account Student"
          }
        ]
    }
    else if (value == "eva-manage-certificate") {
      this.moduleselect = true;
      this.submenuselect = false;
      this.submenuselectcontent = false;
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"certificate":',
            "value": this.data.eva.manage_certificate.certificate,
            "name": "Certificate"
          },
          {
            "attribut": '"certificate_no":',
            "value": this.data.eva.manage_certificate.certificate_no,
            "name": "Certificate No"
          },
          {
            "attribut": '"width":',
            "value": this.data.eva.manage_certificate.width,
            "name": "Width"
          },
          {
            "attribut": '"height":',
            "value": this.data.eva.manage_certificate.height,
            "name": "Height"
          },
          {
            "attribut": '"font_color":',
            "value": this.data.eva.manage_certificate.font_color,
            "name": "Font Color"
          },
          {
            "attribut": '"font":',
            "value": this.data.eva.manage_certificate.font,
            "name": "Font"
          },
          {
            "attribut": '"title":',
            "value": this.data.eva.manage_certificate.title,
            "name": "Title"
          },
          {
            "attribut": '"duration":',
            "value": this.data.eva.manage_certificate.duration,
            "name": "Duration"
          },
          {
            "attribut": '"partner":',
            "value": this.data.eva.manage_certificate.partner,
            "name": "Partner"
          },
          {
            "attribut": '"location":',
            "value": this.data.eva.manage_certificate.location,
            "name": "Location"
          },
          {
            "attribut": '"form_edit_certificate":',
            "value": this.data.eva.manage_certificate.form_edit_certificate,
            "name": "Form Edit Certificate"
          },
          {
            "attribut": '"backgound":',
            "value": this.data.eva.manage_certificate.backgound,
            "name": "Background"
          },
          {
            "attribut": '"list_background":',
            "value": this.data.eva.manage_certificate.list_background,
            "name": "Title in List Background"
          },
          {
            "attribut": '"btn_add_background":',
            "value": this.data.eva.manage_certificate.btn_add_background,
            "name": "Label Add Background Button "
          },
          {
            "attribut": '"background_id":',
            "value": this.data.eva.manage_certificate.background_id,
            "name": "Background ID"
          },
          {
            "attribut": '"background_name":',
            "value": this.data.eva.manage_certificate.background_name,
            "name": "Background Name"
          },
          {
            "attribut": '"certificate_background":',
            "value": this.data.eva.manage_certificate.certificate_background,
            "name": "Certificate Background"
          },
          {
            "attribut": '"list_certificate_design":',
            "value": this.data.eva.manage_certificate.list_certificate_design,
            "name": "List Certificate Design"
          },
          {
            "attribut": '"btn_add_design_template":',
            "value": this.data.eva.manage_certificate.btn_add_design_template,
            "name": "Add Design Template"
          },
          {
            "attribut": '"template":',
            "value": this.data.eva.manage_certificate.template,
            "name": "Templates"
          },
          {
            "attribut": '"template_id":',
            "value": this.data.eva.manage_certificate.template_id,
            "name": "Template ID"
          },
          {
            "attribut": '"template_code":',
            "value": this.data.eva.manage_certificate.template_code,
            "name": "Template Code"
          },
          {
            "attribut": '"template_name":',
            "value": this.data.eva.manage_certificate.template_name,
            "name": "Tempalate Name"
          },
          {
            "attribut": '"add_background":',
            "value": this.data.eva.manage_certificate.add_background.add_background,
            "name": "Add Background"
          },
          {
            "attribut": '"drag_drop":',
            "value": this.data.eva.manage_certificate.add_background.drag_drop,
            "name": "Drag & Drop"
          },
          {
            "attribut": '"drag_drop_file":',
            "value": this.data.eva.manage_certificate.add_background.drag_drop_file,
            "name": "Drag & Drop files here"
          },
          {
            "attribut": '"single_file":',
            "value": this.data.eva.manage_certificate.add_background.single_file,
            "name": "Single File"
          },
          {
            "attribut": '"upload_queue":',
            "value": this.data.eva.manage_certificate.add_background.upload_queue,
            "name": "Upload Queue"
          },
          {
            "attribut": '"queue_length":',
            "value": this.data.eva.manage_certificate.add_background.queue_length,
            "name": "Queue Length"
          },
          {
            "attribut": '"size":',
            "value": this.data.eva.manage_certificate.add_background.size,
            "name": "Size"
          },
          {
            "attribut": '"upload_file":',
            "value": this.data.eva.manage_certificate.add_background.upload_file,
            "name": "Your Upload Files"
          },
          {
            "attribut": '"queue_progress":',
            "value": this.data.eva.manage_certificate.add_background.queue_progress,
            "name": "Queue progress"
          },
          {
            "attribut": '"common_action":',
            "value": this.data.eva.manage_certificate.add_background.common_action,
            "name": "Common Action"
          },
          {
            "attribut": '"upload_all":',
            "value": this.data.eva.manage_certificate.add_background.upload_all,
            "name": "Label Upload All Button"
          },
          {
            "attribut": '"cancel_all":',
            "value": this.data.eva.manage_certificate.add_background.cancel_all,
            "name": "Label Cancel All Button"
          },
          {
            "attribut": '"remove_all":',
            "value": this.data.eva.manage_certificate.add_background.remove_all,
            "name": "Label Remove All Button"
          }
        ]
    }
    /*else if (value == "epdb-role") {
      // this.moduleselect = false;
      // this.submenuselect = false;
      // this.submenuselectcontent = false;
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"list_access":',
            "value": this.data.epdb.role.list_access,
            "name": "List Access"
          },
          {
            "attribut": '"list_access_role":',
            "value": this.data.epdb.role.list_access_role,
            "name": "List Role Access"
          },
          {
            "attribut": '"add_new_role":',
            "value": this.data.epdb.role.add_new_role,
            "name": "Add New Role "
          },
          {
            "attribut": '"btn_add_role":',
            "value": this.data.epdb.role.btn_add_role,
            "name": "Label Add Role Button"
          },
          {
            "attribut": '"sub_menu":',
            "value": this.data.epdb.role.sub_menu,
            "name": "Sub Menu"
          },
          {
            "attribut": '"access":',
            "value": this.data.epdb.role.access,
            "name": "Access"
          },
          {
            "attribut": '"new_role":',
            "value": this.data.epdb.role.new_role,
            "name": "New Role"
          },
          {
            "attribut": '"role_id":',
            "value": this.data.epdb.role.role_id,
            "name": "Role ID"
          },
          {
            "attribut": '"role_name":',
            "value": this.data.epdb.role.role_name,
            "name": "Role Name"
          },
          {
            "attribut": '"chck_access":',
            "value": this.data.general.chck_access,
            "name": "Check Box Access"
          },
          {
            "attribut": '"chck_write":',
            "value": this.data.general.chck_write,
            "name": "Check Box Write"
          },
          {
            "attribut": '"chck_delete":',
            "value": this.data.general.chck_delete,
            "name": "Check Box Delete"
          }
        ]
    }*/

    /*else if (value == "epdb-report") {
      this.menuarr =
        [

        ]
    }*/

    /*else if (value == "eva-report-eva") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"organization_report":',
            "value": this.data.epdb.report_eva.organization_report.organization_report,
            "name": "Title in Organization Report"
          },
          {
            "attribut": '"performance_overview":',
            "value": this.data.epdb.report_eva.performance_overview.performance_overview,
            "name": "Title in Performance Overview Report"
          },
          {
            "attribut": '"evaluation_responses":',
            "value": this.data.epdb.report_eva.evaluation_responses.evaluation_responses,
            "name": "Title in Evaluation Responses Report"
          },
          {
            "attribut": '"student_search":',
            "value": this.data.epdb.report_eva.student_search.student_search,
            "name": "Title in Student Search Report"
          },
          {
            "attribut": '"download_site_evaluation":',
            "value": this.data.epdb.report_eva.download_site_evaluation.download_site_evaluation,
            "name": "Title in Download Site Evaluations"
          },
          {
            "attribut": '"channel_performance":',
            "value": this.data.epdb.report_eva.channel_performance.channel_performance,
            "name": "Title in Channel Performance Report"
          },
          {
            "attribut": '"submission_language":',
            "value": this.data.epdb.report_eva.submission_language.submission_language,
            "name": "Title in Submissions by Language"
          },
          {
            "attribut": '"audit":',
            "value": this.data.epdb.report_eva.audit.audit,
            "name": "Title in Audit"
          },
          {
            "attribut": '"post_evaluation_responses":',
            "value": this.data.epdb.report_eva.post_evaluation_responses.post_evaluation_responses,
            "name": "Title in Post Evaluation Responses Report"
          },
          {
            "attribut": '"comparison_report":',
            "value": this.data.epdb.report_eva.comparison_report.comparison_report,
            "name": "Title in Comparison Report"
          },
          {
            "attribut": '"instructor_course_list":',
            "value": this.data.epdb.report_eva.instructor_course.instructor_course_list,
            "name": "Title in Instructor Course List"
          },
          {
            "attribut": '"instructor_student_list":',
            "value": this.data.epdb.report_eva.instructor_student_list.instructor_student_list,
            "name": "Title in Instrucotr Student List"
          },
          {
            "attribut": '"student_course_evaluation":',
            "value": this.data.epdb.report_eva.student_course_evaluation.student_course_evaluation,
            "name": "Title in Students Course Evaluation"
          },
          {
            "attribut": '"instructor_performance":',
            "value": this.data.epdb.report_eva.instructor_performance.instructor_performance,
            "name": "Title in Instructor Performance Report"
          },
          {
            "attribut": '"other_answer":',
            "value": this.data.epdb.report_eva.other_answer.other_answer,
            "name": "Title in Other Answer Report"
          },
          {
            "attribut": '"select_organization":',
            "value": this.data.epdb.report_eva.organization_report.select_organization,
            "name": "Select an Organization"
          },
          {
            "attribut": '"filter_data":',
            "value": this.data.epdb.report_eva.organization_report.filter_data,
            "name": "Filter Data Based on"
          },
          {
            "attribut": '"survey_year":',
            "value": this.data.epdb.report_eva.organization_report.survey_year,
            "name": "Survey Year"
          },
          {
            "attribut": '"month":',
            "value": this.data.epdb.report_eva.organization_report.month,
            "name": "Month"
          },
          {
            "attribut": '"exclude":',
            "value": this.data.epdb.report_eva.organization_report.exclude,
            "name": "Exclude Onsite course facilities scores from FR scores"
          },
          {
            "attribut": '"total_evaliation":',
            "value": this.data.epdb.report_eva.organization_report.total_evaliation,
            "name": "Total Evaluations"
          },
          {
            "attribut": '"performance_overview":',
            "value": this.data.epdb.report_eva.organization_report.performance_overview,
            "name": "Performance Overview Summary View"
          },
          {
            "attribut": '"of_eval":',
            "value": this.data.epdb.report_eva.organization_report.of_eval,
            "name": "of Evaluation"
          }
        ]
    }*/

    // EIM
    /*else if (value == "eim-profile") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"my_profile":',
            "value": this.data.eim.profile.my_profile.my_profile,
            "name": "Title in My Profile"
          },
          {
            "attribut": '"btn_change_password":',
            "value": this.data.eim.profile.my_profile.btn_change_password,
            "name": "Change Password"
          },
          {
            "attribut": '"partner_id":',
            "value": this.data.eim.profile.my_profile.partner_id,
            "name": "Partner ID"
          },
          {
            "attribut": '"first_name":',
            "value": this.data.eim.profile.my_profile.first_name,
            "name": "First Name"
          },
          {
            "attribut": '"last_name":',
            "value": this.data.eim.profile.my_profile.last_name,
            "name": "Last Name"
          },
          {
            "attribut": '"honorific":',
            "value": this.data.eim.profile.my_profile.honorific,
            "name": "Honorific"
          },
          {
            "attribut": '"gender":',
            "value": this.data.eim.profile.my_profile.gender,
            "name": "Gender"
          },
          {
            "attribut": '"primary_industry":',
            "value": this.data.eim.profile.my_profile.primary_industry,
            "name": "Primary Industry"
          },
          {
            "attribut": '"primary_language":',
            "value": this.data.eim.profile.my_profile.primary_language,
            "name": "Primary Language"
          },
          {
            "attribut": '"secondary_language":',
            "value": this.data.eim.profile.my_profile.secondary_language,
            "name": "Secondary Language"
          },
          {
            "attribut": '"address":',
            "value": this.data.eim.profile.my_profile.address,
            "name": "Address"
          },
          {
            "attribut": '"city":',
            "value": this.data.eim.profile.my_profile.city,
            "name": "City"
          },
          {
            "attribut": '"state":',
            "value": this.data.eim.profile.my_profile.state,
            "name": "State"
          },
          {
            "attribut": '"postal_code":',
            "value": this.data.eim.profile.my_profile.postal_code,
            "name": "ZIP / Postal Code"
          },
          // {
          //   "attribut": '"website":',
          //   "value": this.data.eim.profile.my_profile.website,
          //   "name": "Website"
          // },
          // {
          //   "attribut": '"bio":',
          //   "value": this.data.eim.profile.my_profile.bio,
          //   "name": "Bio"
          // },
          // {
          //   "attribut": '"allow_other_to_see":',
          //   "value": this.data.eim.profile.my_profile.allow_other_to_see,
          //   "name": "Allow others to see my profile in search results"
          // },
          // {
          //   "attribut": '"email_address":',
          //   "value": this.data.eim.profile.my_profile.email_address,
          //   "name": "Email Address"
          // },
          {
            "attribut": '"email_address_1":',
            "value": this.data.eim.profile.my_profile.email_address_1,
            "name": "Primary Email"
          },
          {
            "attribut": '"email_address_2":',
            "value": this.data.eim.profile.my_profile.email_address_2,
            "name": "Secondary Email"
          },
          {
            "attribut": '"phone_number":',
            "value": this.data.eim.profile.my_profile.phone_number,
            "name": "Phone Number"
          },
          {
            "attribut": '"mobile_number":',
            "value": this.data.eim.profile.my_profile.mobile_number,
            "name": "Mobile Number"
          },
          {
            "attribut": '"telephone":',
            "value": this.data.eim.profile.my_profile.telephone,
            "name": "Telephone"
          },
          {
            "attribut": '"do_not_send_email":',
            "value": this.data.eim.profile.my_profile.do_not_send_email,
            "name": "Do not send email"
          },
          // {
          //   "attribut": '"share_my_email":',
          //   "value": this.data.eim.profile.my_profile.share_my_email,
          //   "name": "Share my email address"
          // },
          // {
          //   "attribut": '"share_my_phone":',
          //   "value": this.data.eim.profile.my_profile.share_my_phone,
          //   "name": "Share my phone number"
          // },
          // {
          //   "attribut": '"share_my_mobile_number":',
          //   "value": this.data.eim.profile.my_profile.share_my_mobile_number,
          //   "name": "Share my Mobile Number"
          // }
        ]
    }*/
    /*else if (value == "eim-change-password") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"change_password":',
            "value": this.data.eim.profile.change_password.change_password,
            "name": "Title in Change Password"
          },
          {
            "attribut": '"current_password":',
            "value": this.data.eim.profile.change_password.current_password,
            "name": "Current Password"
          },
          {
            "attribut": '"new_password":',
            "value": this.data.eim.profile.change_password.new_password,
            "name": "New Password"
          },
          {
            "attribut": '"repeat_password":',
            "value": this.data.eim.profile.change_password.repeat_password,
            "name": "Repeat Password"
          }
        ]
    }*/
    /*else if (value == "eim-change-email") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"change_email":',
            "value": this.data.eim.profile.change_email.change_email,
            "name": "Title in Change Email"
          },
          {
            "attribut": '"password":',
            "value": this.data.eim.profile.change_email.password,
            "name": "Password"
          },
          {
            "attribut": '"new_email":',
            "value": this.data.eim.profile.change_email.new_email,
            "name": "New Email Address"
          }
        ]
    }*/

    // Menu Dashboard
    else if (value == "menu-dashboard") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"dashboard":',
            "value": this.data.menu_dashboard.dashboard,
            "name": "Menu Dashboard"
          },
          {
            "attribut": '"dashboard_admin":',
            "value": this.data.menu_dashboard.dashboard_admin,
            "name": "Dashboard Admin"
          },
          {
            "attribut": '"dashboard_distributor":',
            "value": this.data.menu_dashboard.dashboard_distributor,
            "name": "Dashboard Distributor"
          },
          {
            "attribut": '"dashboard_organization":',
            "value": this.data.menu_dashboard.dashboard_organization,
            "name": "Dashboard Site Admin"
          },
          {
            "attribut": '"dashboard_trainer":',
            "value": this.data.menu_dashboard.dashboard_trainer,
            "name": "Dashboard Trainer"
          }
          // {
          //   "attribut": '"access":',
          //   "value": this.data.menu_dashboard.dashboard_student,
          //   "name": "Menu Dashboard"
          // }
        ]
    }
    // menu Admin
    else if (value == "menu-admin") {
      this.menuarr =
        [
          {
            "value": "menu-admin-activities",
            "name": "Menu Activities"
          },
          {
            "value": "menu-admin-products",
            "name": "Menu Products"
          },
          {
            "value": "menu-admin-sku",
            "name": "Menu SKU"
          },
          {
            "value": "menu-admin-glossary",
            "name": "Menu Golssary"
          },
          {
            "value": "menu-admin-country",
            "name": "Menu Country"
          },
          {
            "value": "menu-admin-market-type",
            "name": "Menu Market Type"
          },
          {
            "value": "menu-admin-variables",
            "name": "Menu Variables"
          },
          {
            "value": "menu-admin-partner-type",
            "name": "Menu Partner Type"
          },
          {
            "value": "menu-admin-distributor",
            "name": "Menu Distributor"
          },
          {
            "value": "menu-admin-currency",
            "name": "Menu Currency"
          },
          {
            "value": "menu-admin-language",
            "name": "Menu Language"
          },
          {
            "value": "menu-admin-email-body",
            "name": "Menu Email Body"
          }
        ];
    }

    // menu Manage
    else if (value == "menu-manage") {
      this.menuarr =
        [
          {
            "value": "menu-manage-organization",
            "name": "Menu Organization"
          },
          {
            "value": "menu-manage-site",
            "name": "Menu Site"
          },
          {
            "value": "menu-manage-contact",
            "name": "Menu Contact"
          }
        ];
    }

    // Menu Manage Evaluation
    else if (value == "menu-manage-evaluation") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"manage_evaluation":',
            "value": this.data.menu_manage_evaluation.manage_evaluation,
            "name": "Menu Manage Evaluation"
          },
          {
            "attribut": '"list_template":',
            "value": this.data.menu_manage_evaluation.list_template,
            "name": "Menu List Template"
          },
          {
            "attribut": '"add_template":',
            "value": this.data.menu_manage_evaluation.add_template,
            "name": "Menu Add Template"
          },
          {
            "attribut": '"copy_template":',
            "value": this.data.menu_manage_evaluation.copy_template,
            "name": "Menu Copy Template"
          }
        ]
    }

    // Menu Manage Course
    else if (value == "menu-manage-course") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"manage_course":',
            "value": this.data.menu_manage_course.manage_course,
            "name": "Menu Manage Course"
          },
          {
            "attribut": '"list_course":',
            "value": this.data.menu_manage_course.list_course,
            "name": "Menu Search Course"
          },
          {
            "attribut": '"add_course":',
            "value": this.data.menu_manage_course.add_course,
            "name": "Menu Add Course"
          }
        ]
    }

    // Menu Manage Student
    else if (value == "menu-manage-student-information") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"manage_student_info":',
            "value": this.data.menu_manage_student.manage_student_info,
            "name": "Menu Manage Student Information"
          },
          {
            "attribut": '"list_student":',
            "value": this.data.menu_manage_student.list_student,
            "name": "Menu List Student"
          },
          {
            "attribut": '"list_request_terminate":',
            "value": this.data.menu_manage_student.list_request_terminate,
            "name": "Menu List Request Terminate"
          }
        ]
    }

    // Menu Manage Certificate
    else if (value == "menu-manage-certificate") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"manage_certificate":',
            "value": this.data.menu_manage_certificate.manage_certificate,
            "name": "Menu Manage Certificate"
          },
          {
            "attribut": '"list_background":',
            "value": this.data.menu_manage_certificate.list_background,
            "name": "Menu List Background"
          },
          {
            "attribut": '"add_background":',
            "value": this.data.menu_manage_certificate.add_background,
            "name": "Menu Add Background"
          },
          {
            "attribut": '"list_certificate":',
            "value": this.data.menu_manage_certificate.list_certificate,
            "name": "Menu List Certificate"
          },
          {
            "attribut": '"add_certificate":',
            "value": this.data.menu_manage_certificate.add_certificate,
            "name": "Menu Add Certificate"
          }
        ]
    }

    // Menu Course
    else if (value == "menu-Course") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"course":',
            "value": this.data.menu_course.course,
            "name": "Menu Course"
          },
          {
            "attribut": '"search_course":',
            "value": this.data.menu_course.search_course,
            "name": "Menu Search Course"
          },
          {
            "attribut": '"my_course":',
            "value": this.data.menu_course.my_course,
            "name": "Menu My Course"
          },
          {
            "attribut": '"next_course":',
            "value": this.data.menu_course.next_course,
            "name": "Menu Next Course"
          },
          {
            "attribut": '"history_course":',
            "value": this.data.dashboard.student.finished_course,
            "name": "Menu Finished Course"
          },
          {
            "attribut": '"all_course":',
            "value": this.data.menu_course.all_course,
            "name": "Menu All Course"
          }
        ]
    }

    // Menu Report ePDB
    else if (value == "menu-report-epdb") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"report_epdb":',
            "value": this.data.menu_report_epdb.report_epdb,
            "name": "Menu PDB Report"
          },
          {
            "attribut": '"organization_report":',
            "value": this.data.menu_report_epdb.organization_report,
            "name": "Menu Organization Report"
          },
          {
            "attribut": '"site_report":',
            "value": this.data.menu_report_epdb.site_report,
            "name": "Menu Sites Report"
          },
          {
            "attribut": '"contact_report":',
            "value": this.data.menu_report_epdb.contact_report,
            "name": "Menu Contact Report"
          },
          {
            "attribut": '"org_journal_entry":',
            "value": this.data.menu_report_epdb.org_journal_entry,
            "name": "Menu Organization Journal Entry Report"
          },
          {
            "attribut": '"site_journal_entry":',
            "value": this.data.menu_report_epdb.site_journal_entry,
            "name": "Menu Site Journal Entry Report"
          },
          {
            "attribut": '"invoice_report":',
            "value": this.data.menu_report_epdb.invoice_report,
            "name": "Menu Invoice Report"
          },
          {
            "attribut": '"qualification_contact_report":',
            "value": this.data.menu_report_epdb.qualification_contact_report,
            "name": "Menu Qualifications by Contact Report"
          },
          {
            "attribut": '"site_attribute_report":',
            "value": this.data.menu_report_epdb.site_attribute_report,
            "name": "Menu Site Attributes Report"
          },
          {
            "attribut": '"contact_attribute_report":',
            "value": this.data.menu_report_epdb.contact_attribute_report,
            "name": "Menu Contact Attributes Report"
          },
          {
            "attribut": '"atc_aap_acc_count_report":',
            "value": this.data.menu_report_epdb.atc_aap_acc_count_report,
            "name": "Menu ATC AAP Accreditation Counts Report"
          },
          {
            "attribut": '"db_integrity_report":',
            "value": this.data.menu_report_epdb.db_integrity_report,
            "name": "Menu Database Integrity Report"
          },
          {
            "attribut": '"history_report":',
            "value": this.data.menu_report_epdb.history_report,
            "name": "Menu History Report"
          }
        ]
    }

    // Menu Report Eva
    else if (value == "menu-report-eva") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"report_eva":',
            "value": this.data.menu_report_eva.report_eva,
            "name": "Menu Evaluation Report"
          },
          {
            "attribut": '"organization_report":',
            "value": this.data.epdb.report_eva.organization_report.organization_report,
            "name": "Menu Organization"
          },
          {
            "attribut": '"performance_overview":',
            "value": this.data.epdb.report_eva.performance_overview.performance_overview,
            "name": "Menu Performance Overview"
          },
          {
            "attribut": '"instructor_performance":',
            "value": this.data.epdb.report_eva.instructor_performance.instructor_performance,
            "name": "Menu Instructor Performance"
          },
          {
            "attribut": '"post_evaluation_responses":',
            "value": this.data.epdb.report_eva.post_evaluation_responses.post_evaluation_responses,
            "name": "Menu Evaluation Responses"
          },
          {
            "attribut": '"student_search":',
            "value": this.data.epdb.report_eva.student_search.student_search,
            "name": "Menu Student Search"
          },
          {
            "attribut": '"download_site_evaluation":',
            "value": this.data.epdb.report_eva.download_site_evaluation.download_site_evaluation,
            "name": "Menu Download Site Evaluations"
          },
          {
            "attribut": '"channel_performance":',
            "value": this.data.epdb.report_eva.channel_performance.channel_performance,
            "name": "Menu Channel Performance"
          },
          {
            "attribut": '"submission_language":',
            "value": this.data.epdb.report_eva.submission_language.submission_language,
            "name": "Menu Submissions by Language"
          },
          {
            "attribut": '"audit":',
            "value": this.data.epdb.report_eva.audit.audit,
            "name": "Menu Audit"
          }
        ]
    }

    // Menu Role
    else if (value == "menu-Role") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"role":',
            "value": this.data.menu_role.role,
            "name": "Menu Role"
          },
          {
            "attribut": '"add_role":',
            "value": this.data.menu_role.add_role,
            "name": "Menu Add Role"
          },
          {
            "attribut": '"list_access":',
            "value": this.data.menu_role.list_access,
            "name": "Menu List Access"
          }
        ]
    }

    // New My Profile
    else if (value == "profile-my-profile") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"my_profile":',
            "value": this.data.eim.profile.my_profile.my_profile,
            "name": "Title in My Profile"
          },
          {
            "attribut": '"btn_change_password":',
            "value": this.data.eim.profile.my_profile.btn_change_password,
            "name": "Button Change Password"
          },
          {
            "attribut": '"reset_password":',
            "value": this.data.eim.profile.my_profile.reset_password,
            "name": "Button Reset Password"
          },
          {
            "attribut": '"partner_id":',
            "value": this.data.eim.profile.my_profile.partner_id,
            "name": "Partner ID"
          },
          {
            "attribut": '"first_name":',
            "value": this.data.eim.profile.my_profile.first_name,
            "name": "First Name"
          },
          {
            "attribut": '"last_name":',
            "value": this.data.eim.profile.my_profile.last_name,
            "name": "Last Name"
          },
          {
            "attribut": '"date_birth":',
            "value": this.data.eim.profile.my_profile.date_birth,
            "name": "Date of Birth"
          },
          {
            "attribut": '"honorific":',
            "value": this.data.eim.profile.my_profile.honorific,
            "name": "Honorific"
          },
          {
            "attribut": '"primary_language":',
            "value": this.data.eim.profile.my_profile.primary_language,
            "name": "Primary Language"
          },
          {
            "attribut": '"secondary_language":',
            "value": this.data.eim.profile.my_profile.secondary_language,
            "name": "Secondary Language"
          },
          {
            "attribut": '"gender":',
            "value": this.data.eim.profile.my_profile.gender,
            "name": "Gender"
          },
          {
            "attribut": '"address":',
            "value": this.data.eim.profile.my_profile.address,
            "name": "Address"
          },
          {
            "attribut": '"city":',
            "value": this.data.eim.profile.my_profile.city,
            "name": "City"
          },
          {
            "attribut": '"state":',
            "value": this.data.eim.profile.my_profile.state,
            "name": "State"
          },
          {
            "attribut": '"postal_code":',
            "value": this.data.eim.profile.my_profile.postal_code,
            "name": "ZIP / Postal Code"
          },
          {
            "attribut": '"website":',
            "value": this.data.eim.profile.my_profile.website,
            "name": "Website"
          },
          {
            "attribut": '"primary_industry":',
            "value": this.data.eim.profile.my_profile.primary_industry,
            "name": "Primary Industry"
          },
          {
            "attribut": '"bio":',
            "value": this.data.eim.profile.my_profile.bio,
            "name": "Bio"
          },
          {
            "attribut": '"allow_other_to_see":',
            "value": this.data.eim.profile.my_profile.allow_other_to_see,
            "name": "Allow others to see my profile in search results"
          },
          {
            "attribut": '"email_address":',
            "value": this.data.general.email_address,
            "name": "Email Address"
          },
          {
            "attribut": '"email":',
            "value": this.data.general.email,
            "name": "Email"
          },
          {
            "attribut": '"do_not_send_email":',
            "value": this.data.eim.profile.my_profile.do_not_send_email,
            "name": "Do not send email"
          },
          {
            "attribut": '"share_my_email":',
            "value": this.data.eim.profile.my_profile.share_my_email,
            "name": "Share my email address"
          },
          {
            "attribut": '"email_address_1":',
            "value": this.data.eim.profile.my_profile.email_address_1,
            "name": "Primary Email"
          },
          {
            "attribut": '"email_address_2":',
            "value": this.data.eim.profile.my_profile.email_address_2,
            "name": "Secondary Email Address"
          },
          {
            "attribut": '"phone_number":',
            "value": this.data.eim.profile.my_profile.phone_number,
            "name": "Phone Number"
          },
          {
            "attribut": '"share_my_phone":',
            "value": this.data.eim.profile.my_profile.share_my_phone,
            "name": "Share my phone number"
          },
          {
            "attribut": '"mobile_number":',
            "value": this.data.eim.profile.my_profile.mobile_number,
            "name": "Mobile Number"
          },
          {
            "attribut": '"share_my_mobile_number":',
            "value": this.data.eim.profile.my_profile.share_my_mobile_number,
            "name": "Share my Mobile Number"
          },
          {
            "attribut": '"permission_role":',
            "value": this.data.eim.profile.my_profile.permission_role,
            "name": "Permission and Role"
          },
          {
            "attribut": '"service_role":',
            "value": this.data.eim.profile.my_profile.service_role,
            "name": "Service Roles"
          },
          {
            "attribut": '"profile_update":',
            "value": this.data.eim.profile.my_profile.profile_update,
            "name": "Profile is updated successfully"
          }
        ]
    }
    // New Change Password
    else if (value == "profile-change-password") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"change_password":',
            "value": this.data.eim.profile.change_password.change_password,
            "name": "Title in Change Password"
          },
          {
            "attribut": '"current_password":',
            "value": this.data.eim.profile.change_password.current_password,
            "name": "Current Password"
          },
          {
            "attribut": '"new_password":',
            "value": this.data.eim.profile.change_password.new_password,
            "name": "New Password"
          },
          {
            "attribut": '"repeat_password":',
            "value": this.data.eim.profile.change_password.repeat_password,
            "name": "Repeat Password"
          }
        ]
    }
    // New Change Email
    else if (value == "profile-change-email") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"change_email":',
            "value": this.data.eim.profile.change_email.change_email,
            "name": "Title in Change Email"
          },
          {
            "attribut": '"password":',
            "value": this.data.eim.profile.change_email.password,
            "name": "Password"
          },
          {
            "attribut": '"new_email":',
            "value": this.data.eim.profile.change_email.new_email,
            "name": "New Email Address"
          }
        ]
    }

    // New Admin Activites
    else if (value == "admin-activities") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"activities":',
            "value": this.data.epdb.admin.activities.activities,
            "name": "Activities"
          },
          {
            "attribut": '"activities_list":',
            "value": this.data.epdb.admin.activities.activities_list,
            "name": "Title in List Activities"
          },
          {
            "attribut": '"add_new_activities":',
            "value": this.data.epdb.admin.activities.add_new_activities,
            "name": " Title in Add New Activities"
          },
          {
            "attribut": '"edit_activities":',
            "value": this.data.epdb.admin.activities.edit_activities,
            "name": "Title in Edit Activities"
          },
          {
            "attribut": '"btn_add_activities":',
            "value": this.data.epdb.admin.activities.btn_add_activities,
            "name": "Label Add Activity Button"
          },
          {
            "attribut": '"activity_id":',
            "value": this.data.epdb.admin.activities.activity_id,
            "name": "Activity ID"
          },
          {
            "attribut": '"activity_name":',
            "value": this.data.epdb.admin.activities.activity_name,
            "name": "Activity Name"
          },
          {
            "attribut": '"activity_type":',
            "value": this.data.epdb.admin.activities.activity_type,
            "name": "Activity Type"
          },
          {
            "attribut": '"unique":',
            "value": this.data.epdb.admin.activities.unique,
            "name": "Unique"
          },
          {
            "attribut": '"must_be_unique":',
            "value": this.data.epdb.admin.activities.must_be_unique,
            "name": "Must be Unique"
          },
          {
            "attribut": '"there_can_only":',
            "value": this.data.epdb.admin.activities.there_can_only,
            "name": "There can only be one attribute of this type per Organization, Site, or Instructor"
          }
        ]
    }
    // New Admin Product
    else if (value == "admin-product") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [

          {
            "attribut": '"category_list":',
            "value": this.data.epdb.admin.product.category_list,
            "name": "Title in List Category"
          },
          {
            "attribut": '"add_new_category":',
            "value": this.data.epdb.admin.product.add_new_category,
            "name": "Title in Add New Category"
          },
          {
            "attribut": '"edit_category":',
            "value": this.data.epdb.admin.product.edit_category,
            "name": "Title in Edit Category"
          },
          {
            "attribut": '"btn_add_category":',
            "value": this.data.epdb.admin.product.btn_add_category,
            "name": "Label Add Category Button"
          },
          {
            "attribut": '"product_category":',
            "value": this.data.epdb.admin.product.product_category,
            "name": "Products Category"
          },
          {
            "attribut": '"category_id":',
            "value": this.data.epdb.admin.product.category_id,
            "name": "Category ID"
          },
          {
            "attribut": '"category_name":',
            "value": this.data.epdb.admin.product.category_name,
            "name": "Category Name"
          },
          {
            "attribut": '"product":',
            "value": this.data.epdb.admin.product.product,
            "name": "Product"
          },
          {
            "attribut": '"product_list":',
            "value": this.data.epdb.admin.product.product_list,
            "name": "Title in List Products"
          },
          {
            "attribut": '"add_new_product":',
            "value": this.data.epdb.admin.product.add_new_product,
            "name": "Title in Add Products"
          },
          {
            "attribut": '"edit_product":',
            "value": this.data.epdb.admin.product.edit_product,
            "name": "Title in Edit Product"
          },
          {
            "attribut": '"btn_add_product":',
            "value": this.data.epdb.admin.product.btn_add_product,
            "name": "Label Add Product Button"
          },
          {
            "attribut": '"product_id":',
            "value": this.data.epdb.admin.product.product_id,
            "name": "Product ID"
          },
          {
            "attribut": '"product_name":',
            "value": this.data.epdb.admin.product.product_name,
            "name": "Product Name"
          },
          {
            "attribut": '"version":',
            "value": this.data.epdb.admin.product.version,
            "name": "Version"
          }
        ]
    }
    // New Admin SKU
    else if (value == "admin-sku") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"sku":',
            "value": this.data.epdb.admin.sku.sku,
            "name": "SKU"
          },
          {
            "attribut": '"list_sku":',
            "value": this.data.epdb.admin.sku.list_sku,
            "name": "Title in List SKU"
          },
          {
            "attribut": '"add_edit_sku":',
            "value": this.data.epdb.admin.sku.add_edit_sku,
            "name": "Title in Add / Edit SKU"
          },
          {
            "attribut": '"btn_add_sku":',
            "value": this.data.epdb.admin.sku.btn_add_sku,
            "name": "Label Add SKU Button"
          },
          {
            "attribut": '"price":',
            "value": this.data.epdb.admin.sku.price,
            "name": "Price"
          },
          {
            "attribut": '"license":',
            "value": this.data.epdb.admin.sku.license,
            "name": "License"
          },
        ]
    }
    // New Admin Glossary
    else if (value == "admin-glossary") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"glosarry_list":',
            "value": this.data.epdb.admin.glosarry.glosarry_list,
            "name": "Title in List Glosarry"
          },
          {
            "attribut": '"add_new_term":',
            "value": this.data.epdb.admin.glosarry.add_new_term,
            "name": "Title in Add New Term"
          },
          {
            "attribut": '"edit_term":',
            "value": this.data.epdb.admin.glosarry.edit_term,
            "name": "Title in Edit Term"
          },
          {
            "attribut": '"btn_add_term":',
            "value": this.data.epdb.admin.glosarry.btn_add_term,
            "name": "Label Add Term Button"
          },
          {
            "attribut": '"term":',
            "value": this.data.epdb.admin.glosarry.term,
            "name": "Term"
          }
        ]
    }
    // New Admin Country
    else if (value == "admin-country") {
      this.menuarr =
        [
          {
            "value": "admin-country-geo",
            "name": "Geo"
          },
          {
            "value": "admin-country-territory",
            "name": "Territory"
          },
          {
            "value": "admin-country-region",
            "name": "Region"
          },
          {
            "value": "admin-country-subregion",
            "name": "Sub Region"
          },
          {
            "value": "admin-country-countries",
            "name": "Countries"
          }
        ]
    }
    // New Admin Market Type
    else if (value == "admin-market-type") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"market_type":',
            "value": this.data.epdb.admin.market_type.market_type,
            "name": "Market Type"
          },
          {
            "attribut": '"market_type_list":',
            "value": this.data.epdb.admin.market_type.market_type_list,
            "name": "Title in List Market Type"
          },
          {
            "attribut": '"add_update_market_type":',
            "value": this.data.epdb.admin.market_type.add_update_market_type,
            "name": "Title in Add / Update Market Type"
          },
          {
            "attribut": '"btn_add_market_type":',
            "value": this.data.epdb.admin.market_type.btn_add_market_type,
            "name": "Label Add Market Type Button"
          },
          {
            "attribut": '"market_name":',
            "value": this.data.epdb.admin.market_type.market_name,
            "name": "Market Name"
          }
        ]
    }
    // New Admin Variables
    else if (value == "admin-variables") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"variable_list":',
            "value": this.data.epdb.admin.variables.variable_list,
            "name": "Title in List Variables"
          },
          {
            "attribut": '"add_new_variable":',
            "value": this.data.epdb.admin.variables.add_new_variable,
            "name": "Title in Add New Variable"
          },
          {
            "attribut": '"edit_variable":',
            "value": this.data.epdb.admin.variables.edit_variable,
            "name": "Title in Edit Variable"
          },
          {
            "attribut": '"btn_add_variable":',
            "value": this.data.epdb.admin.variables.btn_add_variable,
            "name": "Label Add Variable Button"
          },
          {
            "attribut": '"variabel_type":',
            "value": this.data.epdb.admin.variables.variabel_type,
            "name": "Variable Type"
          },
          {
            "attribut": '"key":',
            "value": this.data.epdb.admin.variables.key,
            "name": "Key"
          },
          {
            "attribut": '"value":',
            "value": this.data.epdb.admin.variables.value,
            "name": "Value"
          }
        ]
    }
    // New Admin Partner Type
    else if (value == "admin-partner-type") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type,
            "name": "Sub Partner Type"
          },
          {
            "attribut": '"list_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.list_sub_partner_type,
            "name": "Title in List Sub Partner Type"
          },
          {
            "attribut": '"add_new_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.add_new_sub_partner_type,
            "name": "Title in Add New Sub Partner Type"
          },
          {
            "attribut": '"edit_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.edit_sub_partner_type,
            "name": "Title in Edit Sub Partner Type"
          },
          {
            "attribut": '"btn_add_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.btn_add_sub_partner_type,
            "name": "Label Add Sub Partner Type Button"
          },
          {
            "attribut": '"sub_partner_type_name":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type_name,
            "name": "Sub Partner Type Name"
          },
          {
            "attribut": '"partner_type":',
            "value": this.data.epdb.admin.partner_type.partner_type,
            "name": "Partner Type"
          },
          {
            "attribut": '"list_partner_type":',
            "value": this.data.epdb.admin.partner_type.list_partner_type,
            "name": "Title in List Partner Type"
          },
          {
            "attribut": '"add_new_partner_type":',
            "value": this.data.epdb.admin.partner_type.add_new_partner_type,
            "name": "Title in Add Partner Type"
          },
          {
            "attribut": '"edit_partner_type":',
            "value": this.data.epdb.admin.partner_type.edit_partner_type,
            "name": "Title in Edit Partner Type"
          },
          {
            "attribut": '"btn_add_partner_type":',
            "value": this.data.epdb.admin.partner_type.btn_add_partner_type,
            "name": "Label Add Partner Type Button"
          },
          {
            "attribut": '"code":',
            "value": this.data.epdb.admin.partner_type.code,
            "name": "Code"
          },
          {
            "attribut": '"siebel_name":',
            "value": this.data.epdb.admin.partner_type.siebel_name,
            "name": "Siebel Name"
          },
          {
            "attribut": '"framework_name":',
            "value": this.data.epdb.admin.partner_type.framework_name,
            "name": "Framework Name"
          },
          {
            "attribut": '"crb_approved_name":',
            "value": this.data.epdb.admin.partner_type.crb_approved_name,
            "name": "CRB Approved Name"
          },
          {
            "attribut": '"role_type":',
            "value": this.data.epdb.admin.partner_type.role_type,
            "name": "Role Type"
          },
          {
            "attribut": '"role_sub_type":',
            "value": this.data.epdb.admin.partner_type.role_sub_type,
            "name": "Role Sub Type"
          },
          {
            "attribut": '"certificate_type":',
            "value": this.data.epdb.admin.partner_type.certificate_type,
            "name": "Certificate Type"
          },
          {
            "attribut": '"partner_type_status":',
            "value": this.data.epdb.admin.partner_type.partner_type_status,
            "name": "Partner Type Status"
          },
          {
            "attribut": '"sub_type":',
            "value": this.data.epdb.admin.partner_type.sub_type,
            "name": "Sub Type"
          }
        ]
    }
    // New Admin Distributor
    else if (value == "admin-distributor") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"distributor":',
            "value": this.data.epdb.admin.distributor.distributor,
            "name": "Distributor"
          },
          {
            "attribut": '"add_distributor":',
            "value": this.data.epdb.admin.distributor.add_distributor,
            "name": "Title in Tag Distributor"
          },
          {
            "attribut": '"embargoed":',
            "value": this.data.epdb.admin.distributor.embargoed,
            "name": "Diembargoed"
          },
          {
            "attribut": '"master_distributor":',
            "value": this.data.epdb.admin.distributor.master_distributor,
            "name": "Master Distributor"
          },
          {
            "attribut": '"regional_distributor":',
            "value": this.data.epdb.admin.distributor.regional_distributor,
            "name": "Regional Distributor"
          },
          {
            "attribut": '"add_distributor_site":',
            "value": this.data.epdb.admin.distributor.add_distributor_site,
            "name": "Add Distributor Site"
          },
          {
            "attribut": '"contact_distributor":',
            "value": this.data.epdb.admin.distributor.contact_distributor,
            "name": "Contact Distributor"
          },
          {
            "attribut": '"add_contact_distributor":',
            "value": this.data.epdb.admin.distributor.add_contact_distributor,
            "name": "Add Contact Distributor"
          },
          {
            "attribut": '"job_role":',
            "value": this.data.epdb.admin.distributor.job_role,
            "name": "Job Role"
          },
          {
            "attribut": '"no_contact":',
            "value": this.data.epdb.admin.distributor.no_contact,
            "name": "There are no contacts meeting the search criteria specified above"
          }
        ]
    }
    // New Admin Currency
    else if (value == "admin-currency") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"currency":',
            "value": this.data.epdb.admin.currency.currency,
            "name": "Currency"
          },
          {
            "attribut": '"currency_conversion":',
            "value": this.data.epdb.admin.currency.currency_conversion,
            "name": "Title in Currency Conversion"
          },
          {
            "attribut": '"edit_currency":',
            "value": this.data.epdb.admin.currency.edit_currency,
            "name": "Title in Edit Currency"
          },
          {
            "attribut": '"edit_currency_conversion":',
            "value": this.data.epdb.admin.currency.edit_currency_conversion,
            "name": "Title in Edit Currency Conversion"
          },
          {
            "attribut": '"list_currency":',
            "value": this.data.epdb.admin.currency.list_currency,
            "name": "List Currency"
          },
          {
            "attribut": '"value_1_usd":',
            "value": this.data.epdb.admin.currency.value_1_usd,
            "name": "Value 1 USD"
          }
        ]
    }
    // New Admin Language
    else if (value == "admin-language") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"list_language":',
            "value": this.data.epdb.admin.language.list_language,
            "name": "List Language"
          },
          {
            "attribut": '"language":',
            "value": this.data.epdb.admin.language.language,
            "name": "Language"
          },
          {
            "attribut": '"setup_language":',
            "value": this.data.epdb.admin.language.setup_language,
            "name": "Setup Language"
          },
          {
            "attribut": '"tool_tip_save_language":',
            "value": this.data.epdb.admin.language.tool_tip_save_language,
            "name": "Tool Tip Save Language"
          }
        ]
    }
    // New Admin Email Body
    else if (value == "admin-email-body") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"list_email_body":',
            "value": this.data.epdb.admin.email_body.list_email_body,
            "name": "Title in List Email Body"
          },
          {
            "attribut": '"add_edit_email_body":',
            "value": this.data.epdb.admin.email_body.add_edit_email_body,
            "name": "Title in Add / Edit Email Body"
          },
          {
            "attribut": '"btn_add_email_body":',
            "value": this.data.epdb.admin.email_body.btn_add_email_body,
            "name": "Label Add Email Body Button"
          },
          {
            "attribut": '"email_body_type":',
            "value": this.data.epdb.admin.email_body.email_body_type,
            "name": "Email Body Type"
          },
          {
            "attribut": '"email_body":',
            "value": this.data.epdb.admin.email_body.email_body,
            "name": "Email Body"
          }
        ]
    }

    // New PDB Report
    else if (value == "pdb-report-organization") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"organization_report":',
            "value": this.data.menu_report_epdb.organization_report,
            "name": "Organization Report"
          },
          {
            "attribut": '"filter_by_name":',
            "value": this.data.epdb.report.organization_report.filter_by_name,
            "name": "Filter by Name"
          },
          {
            "attribut": '"filter_by_partner_type":',
            "value": this.data.epdb.report.organization_report.filter_by_partner_type,
            "name": "Filter by Partner Type"
          },
          {
            "attribut": '"filter_by_partner_status":',
            "value": this.data.epdb.report.organization_report.filter_by_partner_status,
            "name": "Filter by Partner Status"
          },
          {
            "attribut": '"filter_by_market_type":',
            "value": this.data.epdb.report.organization_report.filter_by_market_type,
            "name": "Filter by Market Type"
          },
          {
            "attribut": '"filter_by_location":',
            "value": this.data.epdb.report.organization_report.filter_by_location,
            "name": "Filter by Location"
          },
          {
            "attribut": '"field_show_result":',
            "value": this.data.epdb.report.organization_report.field_show_result,
            "name": "Fields to Show Result"
          },
          {
            "attribut": '"rollup_partner_type_data":',
            "value": this.data.epdb.report.organization_report.rollup_partner_type_data,
            "name": "Rollup Partner Type Data"
          }
        ]
    }
    else if (value == "pdb-report-site") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"site_report":',
            "value": this.data.menu_report_epdb.site_report,
            "name": "Site Report"
          },
          {
            "attribut": '"filter_by_sub_partner_type":',
            "value": this.data.epdb.report.site_report.filter_by_sub_partner_type,
            "name": "Filter by Sub Partner Type"
          },
          {
            "attribut": '"organization_field":',
            "value": this.data.epdb.report.site_report.organization_field,
            "name": "Organization Fields"
          },
          {
            "attribut": '"site_field":',
            "value": this.data.epdb.report.site_report.site_field,
            "name": "Site Fields"
          }
        ]
    }
    else if (value == "pdb-report-contact") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"contact_report":',
            "value": this.data.menu_report_epdb.contact_report,
            "name": "Contact Report"
          },
          {
            "attribut": '"filter_by_email":',
            "value": this.data.report.contact_report.filter_by_email,
            "name": "Filter by Email"
          },
          {
            "attribut": '"filter_by_site_name":',
            "value": this.data.report.contact_report.filter_by_site_name,
            "name": "Filter by Site Name"
          },
          {
            "attribut": '"filter_by_contact_role":',
            "value": this.data.report.contact_report.filter_by_contact_role,
            "name": "Filter by Contact Role"
          },
          {
            "attribut": '"atc_role":',
            "value": this.data.report.contact_report.atc_role,
            "name": "Where ATC Role"
          },
          {
            "attribut": '"filter_by_industry":',
            "value": this.data.report.contact_report.filter_by_industry,
            "name": "Filter by Industry"
          },
          {
            "attribut": '"use_primary":',
            "value": this.data.report.contact_report.use_primary,
            "name": "Use Primary Site Only"
          },
          {
            "attribut": '"only_show_user":',
            "value": this.data.report.contact_report.only_show_user,
            "name": "Only show users who have logged in after Mar 1, 2009 Who logged in the last"
          },
          {
            "attribut": '"contact_field":',
            "value": this.data.report.contact_report.contact_field,
            "name": "Contact Field"
          }
        ]
    }
    else if (value == "pdb-report-org-journal-entry") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"org_journal_entry":',
            "value": this.data.menu_report_epdb.org_journal_entry,
            "name": "Organization Journal Entry Report"
          },
          {
            "attribut": '"activity_subject":',
            "value": this.data.epdb.report.org_journal_entry.activity_subject,
            "name": "Check for this Activity / Subject"
          },
          {
            "attribut": '"where_the_setting":',
            "value": this.data.epdb.report.org_journal_entry.where_the_setting,
            "name": "Where the setting"
          },
          {
            "attribut": '"is_not_present":',
            "value": this.data.epdb.report.org_journal_entry.is_not_present,
            "name": "Is NOT Present"
          },
          {
            "attribut": '"is_present":',
            "value": this.data.epdb.report.org_journal_entry.is_present,
            "name": "Is Present"
          }
        ]
    }
    else if (value == "pdb-report-site-journal-entry") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"site_journal_entry":',
            "value": this.data.menu_report_epdb.site_journal_entry,
            "name": "Site Journal Entry Report"
          }
        ]
    }
    else if (value == "pdb-report-invoice-report") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"invoice_report":',
            "value": this.data.menu_report_epdb.invoice_report,
            "name": "Invoice Report"
          },
          {
            "attribut": '"invoice_type":',
            "value": this.data.epdb.report.invoice_report.invoice_type,
            "name": "Check for this Invoice Type"
          },
          {
            "attribut": '"entry_was_created":',
            "value": this.data.epdb.report.invoice_report.entry_was_created,
            "name": "And where the entry was created between"
          },
          {
            "attribut": '"preselected":',
            "value": this.data.epdb.report.invoice_report.preselected,
            "name": "Preselected 'Output Fields'"
          }
        ]
    }
    else if (value == "pdb-report-qualification-by-contact") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"qualification_contact_report":',
            "value": this.data.menu_report_epdb.qualification_contact_report,
            "name": "Qualifications by Contact Report"
          },
          {
            "attribut": '"choose_one_more":',
            "value": this.data.epdb.report.qualification_by_contact.choose_one_more,
            "name": "Choose One or More"
          },
          {
            "attribut": '"is_qualified":',
            "value": this.data.epdb.report.qualification_by_contact.is_qualified,
            "name": "Is Qualified"
          },
          {
            "attribut": '"is_not_qualified":',
            "value": this.data.epdb.report.qualification_by_contact.is_not_qualified,
            "name": "Is Not Qualified"
          }
        ]
    }
    else if (value == "pdb-report-site-attribute") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"site_attribute_report":',
            "value": this.data.menu_report_epdb.site_attribute_report,
            "name": "Site Attributes Report"
          }
        ]
    }
    else if (value == "pdb-report-contact-attribute") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"contact_attribute_report":',
            "value": this.data.menu_report_epdb.contact_attribute_report,
            "name": "Contact Attributes Report"
          }
        ]
    }
    else if (value == "pdb-atcaap-acc-count-report") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"atc_aap_acc_count_report":',
            "value": this.data.menu_report_epdb.atc_aap_acc_count_report,
            "name": "ATC AAP Accreditation Counts Report"
          }
        ]
    }
    else if (value == "pdb-database-integrity-report") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"db_integrity_report":',
            "value": this.data.menu_report_epdb.db_integrity_report,
            "name": "Database Integrity Report"
          },
          {
            "attribut": '"site_organization":',
            "value": this.data.epdb.report.database_integrity.organization.site_organization,
            "name": "Organization : Siteless Organizations"
          },
          {
            "attribut": '"org_no_address":',
            "value": this.data.epdb.report.database_integrity.organization.org_no_address,
            "name": "Organization : Organizations with No Address"
          },
          {
            "attribut": '"orphaned_site":',
            "value": this.data.epdb.report.database_integrity.site.orphaned_site,
            "name": "Site : Orphaned Sites"
          },
          {
            "attribut": '"site_no_address":',
            "value": this.data.epdb.report.database_integrity.site.site_no_address,
            "name": "Site : Sites with No Address"
          },
          {
            "attribut": '"site_no_contact":',
            "value": this.data.epdb.report.database_integrity.site.site_no_contact,
            "name": "Site : Sites with No Contacts"
          },
          {
            "attribut": '"site_address":',
            "value": this.data.epdb.report.database_integrity.site.site_address,
            "name": "Site : Sites with similar Address1"
          },
          {
            "attribut": '"siebel_site_address":',
            "value": this.data.epdb.report.database_integrity.site.siebel_site_address,
            "name": "Site : SiebelAddress <> SiteAddress"
          },
          {
            "attribut": '"bad_csn_match":',
            "value": this.data.epdb.report.database_integrity.site.bad_csn_match,
            "name": "Site : Bad CSN Matches"
          },
          {
            "attribut": '"pdb_siebel_status":',
            "value": this.data.epdb.report.database_integrity.site.pdb_siebel_status,
            "name": "Site : PDB Status <> Siebel Status"
          },
          {
            "attribut": '"orphaned_contact":',
            "value": this.data.epdb.report.database_integrity.contact.orphaned_contact,
            "name": "Contact : Orphaned Contacts"
          },
          {
            "attribut": '"duplicate_name":',
            "value": this.data.epdb.report.database_integrity.contact.duplicate_name,
            "name": "Contact : Potential Duplicate Users by Name"
          },
          {
            "attribut": '"duplicate_siebelid":',
            "value": this.data.epdb.report.database_integrity.contact.duplicate_siebelid,
            "name": "Contact : Potential Duplicate Users by SiebelId"
          },
          {
            "attribut": '"siebel_match":',
            "value": this.data.epdb.report.database_integrity.contact.siebel_match,
            "name": "Contact : Siebel Matches to verify"
          },
          {
            "attribut": '"active_user":',
            "value": this.data.epdb.report.database_integrity.contact.active_user,
            "name": "Contact : Active Users in more than one Organization"
          },
          {
            "attribut": '"duplicate_email":',
            "value": this.data.epdb.report.database_integrity.contact.duplicate_email,
            "name": "Contact : icate Email Addresses"
          },
          {
            "attribut": '"illegal_email":',
            "value": this.data.epdb.report.database_integrity.contact.illegal_email,
            "name": "Contact : Illegal? Email Addresses"
          },
          {
            "attribut": '"bad_primary":',
            "value": this.data.epdb.report.database_integrity.contact.bad_primary,
            "name": "Contact : Bad Primary Site Id"
          },
          {
            "attribut": '"missing_primary":',
            "value": this.data.epdb.report.database_integrity.contact.missing_primary,
            "name": "Contact : Missing Primary Site Id"
          },
          {
            "attribut": '"missing_name":',
            "value": this.data.epdb.report.database_integrity.contact.missing_name,
            "name": "Contact : Missing First or Last Name"
          },
          {
            "attribut": '"duplicate_user_history":',
            "value": this.data.epdb.report.database_integrity.contact.duplicate_user_history,
            "name": "Contact : Duplicate (hidden) Users with LMS History"
          },
          {
            "attribut": '"duplicate_user_certification":',
            "value": this.data.epdb.report.database_integrity.contact.duplicate_user_certification,
            "name": "Contact : Duplicate (hidden) Users with LMS Certifications"
          },
          {
            "attribut": '"other":',
            "value": this.data.epdb.report.database_integrity.other.other,
            "name": "Other : Other"
          },
          {
            "attribut": '"duplicate_attribute":',
            "value": this.data.epdb.report.database_integrity.other.duplicate_attribute,
            "name": "Other : Duplicate Attributes"
          },
          {
            "attribut": '"illegal_state":',
            "value": this.data.epdb.report.database_integrity.other.illegal_state,
            "name": "Other : Illegal State/Province/Province values"
          },
          {
            "attribut": '"instructor_whose":',
            "value": this.data.epdb.report.database_integrity.other.instructor_whose,
            "name": "Other : Instructors whose name changed during the ATC Migration"
          },
          {
            "attribut": '"atc_instructor":',
            "value": this.data.epdb.report.database_integrity.other.atc_instructor,
            "name": "Other : ATC Instructors missing Active for on-line eval = On"
          }
        ]
    }
    else if (value == "pdb-history-report") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"history_report":',
            "value": this.data.menu_report_epdb.history_report,
            "name": "History Report"
          },
          {
            "attribut": '"text_match":',
            "value": this.data.epdb.report.history_report.text_match,
            "name": "Text to Match"
          },
          {
            "attribut": '"last_day":',
            "value": this.data.epdb.report.history_report.last_day,
            "name": "Last X days"
          },
          {
            "attribut": '"user_id":',
            "value": this.data.epdb.report.history_report.user_id,
            "name": "User ID"
          }
        ]
    }

    // New Evaluation Report
    else if (value == "evaluation-report-organization") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"organization_report":',
            "value": this.data.epdb.report_eva.organization_report.organization_report,
            "name": "Organization"
          },
          {
            "attribut": '"select_organization":',
            "value": this.data.epdb.report_eva.organization_report.select_organization,
            "name": "Select an Organization"
          },
          {
            "attribut": '"filter_data":',
            "value": this.data.epdb.report_eva.organization_report.filter_data,
            "name": "Filter Data Based on"
          },
          {
            "attribut": '"survey_year":',
            "value": this.data.epdb.report_eva.organization_report.survey_year,
            "name": "Survey Year"
          },
          {
            "attribut": '"month":',
            "value": this.data.epdb.report_eva.organization_report.month,
            "name": "Month"
          },
          {
            "attribut": '"total_evaliation":',
            "value": this.data.epdb.report_eva.organization_report.total_evaliation,
            "name": "Total Evaluations"
          }
        ]
    }
    else if (value == "evaluation-report-performance-overview") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"performance_overview":',
            "value": this.data.epdb.report_eva.performance_overview.performance_overview,
            "name": "Performance Overview"
          },
          {
            "attribut": '"site_active":',
            "value": this.data.epdb.report_eva.performance_overview.site_active,
            "name": "Site Active"
          },
          {
            "attribut": '"exclude":',
            "value": this.data.epdb.report_eva.organization_report.exclude,
            "name": "Exclude Onsite course facilities scores from FR scores"
          }
        ]
    }
    else if (value == "evaluation-report-instructor-performance") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"instructor_performance":',
            "value": this.data.epdb.report_eva.instructor_performance.instructor_performance,
            "name": "Instructor Performance"
          }
        ]
    }
    else if (value == "evaluation-report-evaluation-response") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"post_evaluation_responses":',
            "value": this.data.epdb.report_eva.post_evaluation_responses.post_evaluation_responses,
            "name": "Evaluation Responses"
          }
        ]
    }
    else if (value == "evaluation-report-student-search") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"student_search":',
            "value": this.data.epdb.report_eva.student_search.student_search,
            "name": "Student Search"
          }
        ]
    }
    else if (value == "evaluation-report-download-site-evaluation") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"download_site_evaluation":',
            "value": this.data.epdb.report_eva.download_site_evaluation.download_site_evaluation,
            "name": "Download Site Evaluation"
          }
        ]
    }
    else if (value == "evaluation-report-channel-performnace") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"channel_performance":',
            "value": this.data.epdb.report_eva.channel_performance.channel_performance,
            "name": "Channel Performance"
          }
        ]
    }
    else if (value == "evaluation-report-submission-language") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"submission_language":',
            "value": this.data.epdb.report_eva.submission_language.submission_language,
            "name": "Submissions by Language"
          }
        ]
    }
    else if (value == "evaluation-report-audit") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.contentarr =
        [
          {
            "attribut": '"audit":',
            "value": this.data.epdb.report_eva.audit.audit,
            "name": "Audit"
          }
        ]
    }
  }

  // sub menu
  picksubmenu(value) {
    /*if (value == "epdb-admin-country") {
      this.submenuselect = true;
      this.menuselect = false;
      this.submenuselectcontent = false;
      this.submenuarr =
        [
          {
            "value": "epdb-admin-country-geo",
            "name": "Geo"
          },
          {
            "value": "epdb-admin-country-territory",
            "name": "Territory"
          },
          {
            "value": "epdb-admin-country-region",
            "name": "Region"
          },
          {
            "value": "epdb-admin-country-sub-region",
            "name": "Sub Region"
          },
          {
            "value": "epdb-admin-country-countries",
            "name": "Country"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-organization") {
      this.submenuselect = true;
      this.submenuarr =
        [
          {
            "value": "epdb-manager-organization-search",
            "name": "Search Organization"
          },
          {
            "value": "epdb-manager-organization-detail",
            "name": "Detail Organization"
          },
          {
            "value": "epdb-manager-organization-add-edit",
            "name": "Add & Edit Organization"
          },
          {
            "value": "epdb-manager-organization-journal-entries",
            "name": "Add Organization Journal Entries"
          },
          {
            "value": "epdb-manager-invoice",
            "name": "Add Invoice"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site") {
      this.submenuselect = true;
      this.submenuarr =
        [
          {
            "value": "epdb-manager-site-search",
            "name": "Search Site"
          },
          {
            "value": "epdb-manager-site-detail",
            "name": "Detail Site"
          },
          {
            "value": "epdb-manager-site-add-edit",
            "name": "Add & Edit Site"
          },
          {
            "value": "epdb-manager-site-add-partner-type",
            "name": "Partner Type"
          },
          {
            "value": "epdb-manager-site-accreditation",
            "name": "Site Accreditation"
          },
          {
            "value": "epdb-manager-site-contact-qualification",
            "name": "Contact Qualification"
          },
          {
            "value": "epdb-manager-site-journal-entries",
            "name": "Site Journal Entries"
          },
          {
            "value": "epdb-manager-site-academic-program",
            "name": "Academic Program"
          },
          {
            "value": "epdb-manager-site-academic-target",
            "name": "Academic Target"
          },
          {
            "value": "epdb-manager-site-academic-project",
            "name": "Academic Project"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-contact") {
      this.submenuselect = true;
      this.submenuarr =
        [
          {
            "value": "epdb-manager-contact-search",
            "name": "Search Contact"
          },
          {
            "value": "epdb-manager-contact-detail",
            "name": "Detail Contact"
          },
          {
            "value": "epdb-manager-contact-add-edit",
            "name": "Add & Edit Contact"
          },
          {
            "value": "epdb-manager-contact-site-affiliation",
            "name": "Site Affiliation"
          },
          {
            "value": "epdb-manager-contact-partner-type-specific",
            "name": "Partner Type Specific Properties"
          }
          // {
          //   "value": "epdb-manager-contact-search-general",
          //   "name": "Search General"
          // }
        ]
    }*/
    if (value == "eva-manage-course-question") {
      this.submenuselect = true;
      this.menuselect = false;
      this.submenuselectcontent = false;
      this.submenuarr =
        [
          {
            "value": "eva-manage-course-question-atc",
            "name": "ATC"
          },
          {
            "value": "eva-manage-course-question-course",
            "name": "Course"
          },
          {
            "value": "eva-manage-course-question-project",
            "name": "Project"
          },
          {
            "value": "eva-manage-course-question-event",
            "name": "Event"
          }
        ]
    }
  }


  pickmenu(value) {
    /*if (value == "epdb-dashboard-chart") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"week":',
            "value": this.data.epdb.dashboard.chart.week,
            "name": "Week"
          },
          {
            "attribut": '"month":',
            "value": this.data.epdb.dashboard.chart.month,
            "name": "Month"
          },
          {
            "attribut": '"year":',
            "value": this.data.epdb.dashboard.chart.year,
            "name": "Year"
          }
        ]
    }*/
    /*else if (value == "epdb-dashboard-head_table") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"type":',
            "value": this.data.epdb.dashboard.head_table.type,
            "name": "Type"
          },
          {
            "attribut": '"name":',
            "value": this.data.epdb.dashboard.head_table.name,
            "name": "Name"
          },
          {
            "attribut": '"views":',
            "value": this.data.epdb.dashboard.head_table.views,
            "name": "Views"
          },
          {
            "attribut": '"favourites":',
            "value": this.data.epdb.dashboard.head_table.favourites,
            "name": "Favourites"
          },
          {
            "attribut": '"last_visit":',
            "value": this.data.epdb.dashboard.head_table.last_visit,
            "name": "Last Visit"
          },
          {
            "attribut": '"last_action":',
            "value": this.data.epdb.dashboard.head_table.last_action,
            "name": "Last Action"
          },
          {
            "attribut": '"last_date":',
            "value": this.data.epdb.dashboard.head_table.last_date,
            "name": "Last Date"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-activities") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"activities":',
            "value": this.data.epdb.admin.activities.activities,
            "name": "Activities"
          },
          {
            "attribut": '"activities_list":',
            "value": this.data.epdb.admin.activities.activities_list,
            "name": "Title in List Activities"
          },
          {
            "attribut": '"add_new_activities":',
            "value": this.data.epdb.admin.activities.add_new_activities,
            "name": " Title in Add New Activities"
          },
          {
            "attribut": '"edit_activities":',
            "value": this.data.epdb.admin.activities.edit_activities,
            "name": "Title in Edit Activities"
          },
          {
            "attribut": '"btn_add_activities":',
            "value": this.data.epdb.admin.activities.btn_add_activities,
            "name": "Label Add Activity Button"
          },
          {
            "attribut": '"activity_id":',
            "value": this.data.epdb.admin.activities.activity_id,
            "name": "Activity ID"
          },
          {
            "attribut": '"activity_name":',
            "value": this.data.epdb.admin.activities.activity_name,
            "name": "Activity Name"
          },
          {
            "attribut": '"activity_type":',
            "value": this.data.epdb.admin.activities.activity_type,
            "name": "Activity Type"
          },
          {
            "attribut": '"unique":',
            "value": this.data.epdb.admin.activities.unique,
            "name": "Unique"
          },
          {
            "attribut": '"must_be_unique":',
            "value": this.data.epdb.admin.activities.must_be_unique,
            "name": "Must be Unique"
          },
          {
            "attribut": '"there_can_only":',
            "value": this.data.epdb.admin.activities.there_can_only,
            "name": "There can only be one attribute of this type per Organization, Site, or Instructor"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-product") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"product":',
            "value": this.data.epdb.admin.product.product,
            "name": "Product"
          },
          {
            "attribut": '"product_list":',
            "value": this.data.epdb.admin.product.product_list,
            "name": "Title in List Products"
          },
          {
            "attribut": '"add_new_product":',
            "value": this.data.epdb.admin.product.add_new_product,
            "name": "Title in Add Products"
          },
          {
            "attribut": '"edit_product":',
            "value": this.data.epdb.admin.product.edit_product,
            "name": "Title in Edit Product"
          },
          {
            "attribut": '"btn_add_product":',
            "value": this.data.epdb.admin.product.btn_add_product,
            "name": "Label Add Product Button"
          },
          {
            "attribut": '"product_id":',
            "value": this.data.epdb.admin.product.product_id,
            "name": "Product ID"
          },
          {
            "attribut": '"product_name":',
            "value": this.data.epdb.admin.product.product_name,
            "name": "Product Name"
          },
          {
            "attribut": '"year":',
            "value": this.data.epdb.admin.product.year,
            "name": "Year"
          },
          {
            "attribut": '"category_list":',
            "value": this.data.epdb.admin.product.category_list,
            "name": "Title in List Category"
          },
          {
            "attribut": '"add_new_category":',
            "value": this.data.epdb.admin.product.add_new_category,
            "name": "Title in Add New Category"
          },
          {
            "attribut": '"edit_category":',
            "value": this.data.epdb.admin.product.edit_category,
            "name": "Title in Edit Category"
          },
          {
            "attribut": '"btn_add_category":',
            "value": this.data.epdb.admin.product.btn_add_category,
            "name": "Label Add Category Button"
          },
          {
            "attribut": '"product_category":',
            "value": this.data.epdb.admin.product.product_category,
            "name": "Products Category"
          },
          {
            "attribut": '"category_id":',
            "value": this.data.epdb.admin.product.category_id,
            "name": "Category ID"
          },
          {
            "attribut": '"category_name":',
            "value": this.data.epdb.admin.product.category_name,
            "name": "Category Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-sku") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"sku":',
            "value": this.data.epdb.admin.sku.sku,
            "name": "SKU"
          },
          {
            "attribut": '"list_sku":',
            "value": this.data.epdb.admin.sku.list_sku,
            "name": "Title in List SKU"
          },
          {
            "attribut": '"add_edit_sku":',
            "value": this.data.epdb.admin.sku.add_edit_sku,
            "name": "Title in Add / Update SKU"
          },
          {
            "attribut": '"btn_add_sku":',
            "value": this.data.epdb.admin.sku.btn_add_sku,
            "name": "Label Add SKU Button"
          },
          {
            "attribut": '"sku_id":',
            "value": this.data.epdb.admin.sku.sku_id,
            "name": "SKU ID"
          },
          {
            "attribut": '"price_license":',
            "value": this.data.epdb.admin.sku.price_license,
            "name": "Price / License"
          },
          {
            "attribut": '"sku_description":',
            "value": this.data.epdb.admin.sku.sku_description,
            "name": "SKU Description"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-glosarry") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"glosarry_list":',
            "value": this.data.epdb.admin.glosarry.glosarry_list,
            "name": "Title in List Glosarry"
          },
          {
            "attribut": '"add_new_term":',
            "value": this.data.epdb.admin.glosarry.add_new_term,
            "name": "Title in Add New Term"
          },
          {
            "attribut": '"edit_term":',
            "value": this.data.epdb.admin.glosarry.edit_term,
            "name": "Title in Edit Term"
          },
          {
            "attribut": '"btn_add_term":',
            "value": this.data.epdb.admin.glosarry.btn_add_term,
            "name": "Label Add Term Button"
          },
          {
            "attribut": '"term":',
            "value": this.data.epdb.admin.glosarry.term,
            "name": "Term"
          },
          {
            "attribut": '"term_id":',
            "value": this.data.epdb.admin.glosarry.term_id,
            "name": "Term ID"
          },
          {
            "attribut": '"term_name":',
            "value": this.data.epdb.admin.glosarry.term_name,
            "name": "Term Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-market-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"market_type_list":',
            "value": this.data.epdb.admin.market_type.market_type_list,
            "name": "Title in List Market Type"
          },
          {
            "attribut": '"add_update_market_type":',
            "value": this.data.epdb.admin.market_type.add_update_market_type,
            "name": "Title in Add / Update Market Type"
          },
          {
            "attribut": '"btn_add_market_type":',
            "value": this.data.epdb.admin.market_type.btn_add_market_type,
            "name": "Label Add Market Type Button"
          },
          {
            "attribut": '"market_name":',
            "value": this.data.epdb.admin.market_type.market_name,
            "name": "Market Name"
          },
          {
            "attribut": '"market_type":',
            "value": this.data.epdb.admin.market_type.market_type,
            "name": "Market Type"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-site-service") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"site_services_list":',
            "value": this.data.epdb.admin.site_services.site_services_list,
            "name": "Title in List Site Services"
          },
          {
            "attribut": '"add_new_site_services":',
            "value": this.data.epdb.admin.site_services.add_new_site_services,
            "name": "Title in Add New Site Services"
          },
          {
            "attribut": '"edit_site_services":',
            "value": this.data.epdb.admin.site_services.edit_site_services,
            "name": "Title in Edit Site Service"
          },
          {
            "attribut": '"btn_add_site_services":',
            "value": this.data.epdb.admin.site_services.btn_add_site_services,
            "name": "Label Add Site Service Button"
          },
          {
            "attribut": '"code":',
            "value": this.data.epdb.admin.site_services.code,
            "name": "Code"
          },
          {
            "attribut": '"site_service_id":',
            "value": this.data.epdb.admin.site_services.site_service_id,
            "name": "Site Service ID"
          },
          {
            "attribut": '"site_service_code":',
            "value": this.data.epdb.admin.site_services.site_service_code,
            "name": "Site Service Code"
          },
          {
            "attribut": '"site_service_name":',
            "value": this.data.epdb.admin.site_services.site_service_name,
            "name": "Site Service Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-variable") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"variable_list":',
            "value": this.data.epdb.admin.variables.variable_list,
            "name": "Title in List Variables"
          },
          {
            "attribut": '"add_new_variable":',
            "value": this.data.epdb.admin.variables.add_new_variable,
            "name": "Title in Add New Variable"
          },
          {
            "attribut": '"edit_variable":',
            "value": this.data.epdb.admin.variables.edit_variable,
            "name": "Title in Edit Variable"
          },
          {
            "attribut": '"btn_add_variable":',
            "value": this.data.epdb.admin.variables.btn_add_variable,
            "name": "Label Add Variable Button"
          },
          {
            "attribut": '"variabel_type":',
            "value": this.data.epdb.admin.variables.variabel_type,
            "name": "Variables Type"
          },
          {
            "attribut": '"id":',
            "value": this.data.epdb.admin.variables.id,
            "name": "ID"
          },
          {
            "attribut": '"key":',
            "value": this.data.epdb.admin.variables.key,
            "name": "Key"
          },
          {
            "attribut": '"value":',
            "value": this.data.epdb.admin.variables.value,
            "name": "Value"
          },
          {
            "attribut": '"variable_id":',
            "value": this.data.epdb.admin.variables.variable_id,
            "name": "Variable ID"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-sub-partner-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type,
            "name": "Sub Partner Type"
          },
          {
            "attribut": '"list_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.list_sub_partner_type,
            "name": "Title in List Sub Partner Type"
          },
          {
            "attribut": '"add_new_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.add_new_sub_partner_type,
            "name": "Title in Add New Sub Partner Type"
          },
          {
            "attribut": '"edit_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.edit_sub_partner_type,
            "name": "Title in Edit Sub Partner Type"
          },
          {
            "attribut": '"btn_add_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.btn_add_sub_partner_type,
            "name": "Label Add Sub Partner Type Button"
          },
          {
            "attribut": '"sub_partner_type_id":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type_id,
            "name": "Sub Partner Type ID"
          },
          {
            "attribut": '"sub_partner_type_name":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type_name,
            "name": "Sub Partner Type Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-partner-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"partner_type":',
            "value": this.data.epdb.admin.partner_type.partner_type,
            "name": "Partner Type"
          },
          {
            "attribut": '"list_partner_type":',
            "value": this.data.epdb.admin.partner_type.list_partner_type,
            "name": "Title in List Partner Type"
          },
          {
            "attribut": '"add_new_partner_type":',
            "value": this.data.epdb.admin.partner_type.add_new_partner_type,
            "name": "Title in Add Partner Type"
          },
          {
            "attribut": '"edit_partner_type":',
            "value": this.data.epdb.admin.partner_type.edit_partner_type,
            "name": "Title in Edit Partner Type"
          },
          {
            "attribut": '"btn_add_partner_type":',
            "value": this.data.epdb.admin.partner_type.btn_add_partner_type,
            "name": "Label Add Partner Type Button"
          },
          {
            "attribut": '"code":',
            "value": this.data.epdb.admin.partner_type.code,
            "name": "Code"
          },
          {
            "attribut": '"siebel_name":',
            "value": this.data.epdb.admin.partner_type.siebel_name,
            "name": "Siebel Name"
          },
          {
            "attribut": '"framework_name":',
            "value": this.data.epdb.admin.partner_type.framework_name,
            "name": "Framework Name"
          },
          {
            "attribut": '"crb_approved_name":',
            "value": this.data.epdb.admin.partner_type.crb_approved_name,
            "name": "CRB Approved Name"
          },
          {
            "attribut": '"role_type":',
            "value": this.data.epdb.admin.partner_type.role_type,
            "name": "Role Type"
          },
          {
            "attribut": '"role_sub_type":',
            "value": this.data.epdb.admin.partner_type.role_sub_type,
            "name": "Role Sub Type"
          },
          {
            "attribut": '"certificate_type":',
            "value": this.data.epdb.admin.partner_type.certificate_type,
            "name": "Certificate Type"
          },
          {
            "attribut": '"partner_type_status":',
            "value": this.data.epdb.admin.partner_type.partner_type_status,
            "name": "Partner Type Status"
          },
          {
            "attribut": '"sub_type":',
            "value": this.data.epdb.admin.partner_type.sub_type,
            "name": "Sub Type"
          }
        ]
    }*/
    // Distributor
    /*else if (value == "epdb-admin-distributor") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"distributor":',
            "value": this.data.epdb.admin.distributor.distributor,
            "name": "Distributor"
          },
          {
            "attribut": '"list_distributor":',
            "value": this.data.epdb.admin.distributor.list_distributor,
            "name": "Title in List Distributor"
          },
          {
            "attribut": '"add_distributor":',
            "value": this.data.epdb.admin.distributor.add_distributor,
            "name": "Title in Add Distributor"
          },
          {
            "attribut": '"embargoed":',
            "value": this.data.epdb.admin.distributor.embargoed,
            "name": "Embargoed"
          },
          {
            "attribut": '"master_distributor":',
            "value": this.data.epdb.admin.distributor.master_distributor,
            "name": "Master Distributor"
          },
          {
            "attribut": '"regional_distributor":',
            "value": this.data.epdb.admin.distributor.regional_distributor,
            "name": "Regional Distributor"
          },
          {
            "attribut": '"contact_distributor":',
            "value": this.data.epdb.admin.distributor.contact_distributor,
            "name": "Contact Distributor"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-currency-conversion") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"currency":',
            "value": this.data.epdb.admin.currency.currency,
            "name": "Currency"
          },
          {
            "attribut": '"currency_conversion":',
            "value": this.data.epdb.admin.currency.currency_conversion,
            "name": "Title in Currency Conversion"
          },
          {
            "attribut": '"list_currency":',
            "value": this.data.epdb.admin.currency.list_currency,
            "name": "List Currency"
          },
          {
            "attribut": '"year":',
            "value": this.data.epdb.admin.currency.year,
            "name": "Year"
          },
          {
            "attribut": '"value_1_usd":',
            "value": this.data.epdb.admin.currency.value_1_usd,
            "name": "Value 1 USD"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-setup-language") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"list_language":',
            "value": this.data.epdb.admin.language.list_language,
            "name": "List Language"
          },
          {
            "attribut": '"language_code":',
            "value": this.data.epdb.admin.language.language_code,
            "name": "Language Code"
          },
          {
            "attribut": '"language":',
            "value": this.data.epdb.admin.language.language,
            "name": "Language"
          },
          {
            "attribut": '"tool_tip_setup_language":',
            "value": this.data.epdb.admin.language.tool_tip_setup_language,
            "name": "Tool Tip Setup Language"
          },
          {
            "attribut": '"setup_language":',
            "value": this.data.epdb.admin.language.setup_language,
            "name": "Setup Language"
          },
          {
            "attribut": '"tool_tip_save_language":',
            "value": this.data.epdb.admin.language.tool_tip_save_language,
            "name": "Tool Tip Save Language"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-email-body") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"list_email_body":',
            "value": this.data.epdb.admin.email_body.list_email_body,
            "name": "Title in List Email Body"
          },
          {
            "attribut": '"add_edit_email_body":',
            "value": this.data.epdb.admin.email_body.add_edit_email_body,
            "name": "Title in Add / Edit Email Body"
          },
          {
            "attribut": '"btn_add_email_body":',
            "value": this.data.epdb.admin.email_body.add_edit_email_body,
            "name": "Label Add Email Body Button"
          },
          {
            "attribut": '"email_body_type":',
            "value": this.data.epdb.admin.email_body.email_body_type,
            "name": "Email Body Type"
          },
          {
            "attribut": '"email_body":',
            "value": this.data.epdb.admin.email_body.email_body,
            "name": "Email Body"
          }
        ]
    }*/

    // Start New Manage Org
    if (value == "manage-organization-search") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"search_organization":',
            "value": this.data.epdb.manage.organization.search.search_organization,
            "name": "Search Organization"
          },
          {
            "attribut": '"tab_organization":',
            "value": this.data.epdb.manage.organization.search.tab_organization,
            "name": "Tab Organization"
          },
          {
            "attribut": '"tab_site":',
            "value": this.data.epdb.manage.organization.search.tab_site,
            "name": "Tab Site"
          },
          {
            "attribut": '"tab_contact":',
            "value": this.data.epdb.manage.organization.search.tab_contact,
            "name": "Tab Contact"
          },
          {
            "attribut": '"search_result":',
            "value": this.data.epdb.manage.organization.search.search_result,
            "name": "Search Result"
          }
        ]
    }
    else if (value == "manage-organization-detail") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"detail_organization":',
            "value": this.data.epdb.manage.organization.detail.detail_organization,
            "name": "Detail Organization"
          },
          {
            "attribut": '"organization":',
            "value": this.data.epdb.manage.organization.detail.organization,
            "name": "Organization"
          },
          {
            "attribut": '"organization_id":',
            "value": this.data.epdb.manage.organization.detail.organization_id,
            "name": "Organization ID"
          },
          {
            "attribut": '"organization_name":',
            "value": this.data.epdb.manage.organization.detail.organization_name,
            "name": "Organization Name"
          },
          {
            "attribut": '"commercial_name":',
            "value": this.data.epdb.manage.organization.detail.commercial_name,
            "name": "Commercial Name"
          },
          {
            "attribut": '"geo_region":',
            "value": this.data.epdb.manage.organization.detail.geo_region,
            "name": "Geo / Region"
          },
          {
            "attribut": '"email_1":',
            "value": this.data.epdb.manage.organization.detail.email_1,
            "name": "Primary Email"
          },
          {
            "attribut": '"email_2":',
            "value": this.data.epdb.manage.organization.detail.email_2,
            "name": "Secondary Email"
          },
          {
            "attribut": '"fax":',
            "value": this.data.epdb.manage.organization.detail.fax,
            "name": "Fax"
          },
          {
            "attribut": '"organization_web":',
            "value": this.data.epdb.manage.organization.detail.organization_web,
            "name": "Organization Web"
          },
          {
            "attribut": '"address_siebel":',
            "value": this.data.epdb.manage.organization.detail.address_siebel,
            "name": "Address is Siebel"
          },
          {
            "attribut": '"organization_address":',
            "value": this.data.epdb.manage.organization.detail.organization_address,
            "name": "Organization Address"
          },
          {
            "attribut": '"mailing_address":',
            "value": this.data.epdb.manage.organization.detail.mailing_address,
            "name": "Mailing Address"
          },
          {
            "attribut": '"shipping_address":',
            "value": this.data.epdb.manage.organization.detail.shipping_address,
            "name": "Shipping Address"
          }
        ]
    }
    else if (value == "manage-organization-add-edit") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"add_new_organization":',
            "value": this.data.epdb.manage.organization.add_edit.add_new_organization,
            "name": "Add New Organization"
          },
          {
            "attribut": '"edit_organization":',
            "value": this.data.epdb.manage.organization.add_edit.edit_organization,
            "name": "Edit Organization"
          },
          {
            "attribut": '"english_organization_name":',
            "value": this.data.epdb.manage.organization.add_edit.english_organization_name,
            "name": "English Organization Name"
          },
          {
            "attribut": '"commercial_organization_name":',
            "value": this.data.epdb.manage.organization.add_edit.commercial_organization_name,
            "name": "Commercial Organization Name"
          },
          // {
          //   "attribut": '"atc_global_csn":',
          //   "value": this.data.epdb.manage.organization.add_edit.atc_global_csn,
          //   "name": "CSN"
          // },
          // {
          //   "attribut": '"var_global_csn":',
          //   "value": this.data.epdb.manage.organization.add_edit.var_global_csn,
          //   "name": "Parent CSN"
          // },
          // {
          //   "attribut": '"ap_global_csn":',
          //   "value": this.data.epdb.manage.organization.add_edit.ap_global_csn,
          //   "name": "Ultimate CSN"
          // },
          {
            "attribut": '"tax_exempt":',
            "value": this.data.epdb.manage.organization.add_edit.tax_exempt,
            "name": "Tax Exempt?"
          },
          {
            "attribut": '"vat_number":',
            "value": this.data.epdb.manage.organization.add_edit.vat_number,
            "name": "VAT Number"
          },
          {
            "attribut": '"year_joined":',
            "value": this.data.epdb.manage.organization.add_edit.year_joined,
            "name": "Year Joined"
          },
          {
            "attribut": '"organization_web_address":',
            "value": this.data.epdb.manage.organization.add_edit.organization_web_address,
            "name": "Organization Web Address"
          },
          {
            "attribut": '"registered_address":',
            "value": this.data.epdb.manage.organization.add_edit.registered_address,
            "name": "Registered Address"
          },
          {
            "attribut": '"contact_address":',
            "value": this.data.epdb.manage.organization.add_edit.contact_address,
            "name": "Contact Address"
          },
          {
            "attribut": '"invoicing_address":',
            "value": this.data.epdb.manage.organization.add_edit.invoicing_address,
            "name": "Invoicing Address"
          },
          // {
          //   "attribut": '"department":',
          //   "value": this.data.epdb.manage.organization.add_edit.department,
          //   "name": "Department"
          // },
          {
            "attribut": '"atc_director":',
            "value": this.data.epdb.manage.organization.add_edit.atc_director,
            "name": "ATC Director"
          },
          {
            "attribut": '"legal_contact":',
            "value": this.data.epdb.manage.organization.add_edit.legal_contact,
            "name": "Legal Contact"
          },
          {
            "attribut": '"billing_contact":',
            "value": this.data.epdb.manage.organization.add_edit.billing_contact,
            "name": "Billing Contact"
          },
          // {
          //   "attribut": '"first_name":',
          //   "value": this.data.epdb.manage.organization.add_edit.first_name,
          //   "name": "First Name"
          // },
          {
            "attribut": '"last_name":',
            "value": this.data.epdb.manage.organization.add_edit.last_name,
            "name": "Last Name"
          },
          {
            "attribut": '"administrative_notes":',
            "value": this.data.epdb.manage.organization.add_edit.administrative_notes,
            "name": "Administrative Notes"
          }
        ]
    }
    else if (value == "manage-organization-journal-entries") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"organization_journal_entry":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.organization_journal_entry,
            "name": "Organization Journal Entries"
          },
          {
            "attribut": '"add_org_journal_entries":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.add_org_journal_entries,
            "name": "Journal Entry Type"
          },
          {
            "attribut": '"journal_entry_type":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.journal_entry_type,
            "name": "Journal Entry Type"
          },
          {
            "attribut": '"detail_organization_journal_entries":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.detail_organization_journal_entries,
            "name": "Detail Organization Journal Entries"
          },
          {
            "attribut": '"journal_id":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.journal_id,
            "name": "Journal ID"
          },
          {
            "attribut": '"parent_id":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.parent_id,
            "name": "Parent ID"
          },
          {
            "attribut": '"activity_date":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.activity_date,
            "name": "Activity Date"
          },
          {
            "attribut": '"date_last_admin":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.date_last_admin,
            "name": "Date Last Admin"
          },
          {
            "attribut": '"last_admin_by":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.last_admin_by,
            "name": "Last Admin By"
          }
        ]
    }
    else if (value == "manage-invoice") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice,
            "name": "Invoice"
          },
          {
            "attribut": '"pay_receive_date":',
            "value": this.data.epdb.manage.organization.add_invoice.pay_receive_date,
            "name": "Payment Received Date"
          },
          {
            "attribut": '"amount_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.amount_invoice,
            "name": "Amount Invoiced"
          },
          {
            "attribut": '"amount_invoice_usd":',
            "value": this.data.epdb.manage.organization.add_invoice.amount_invoice_usd,
            "name": "Amount Invoiced in USD"
          },
          {
            "attribut": '"add_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.add_invoice,
            "name": "Title in Add Invoice"
          },
          {
            "attribut": '"detail_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.detail_invoice,
            "name": "Detail Invoice"
          },
          {
            "attribut": '"edit_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.edit_invoice,
            "name": "Edit Invoice"
          },
          {
            "attribut": '"invoice_id":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_id,
            "name": "Invoice ID"
          },
          {
            "attribut": '"invoice_currency":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_currency,
            "name": "Invoice Currency"
          },
          {
            "attribut": '"invoice_number":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_number,
            "name": "Invoice Number"
          },
          {
            "attribut": '"po_receive_date":',
            "value": this.data.epdb.manage.organization.add_invoice.po_receive_date,
            "name": "P.O. Receive Date"
          },
          {
            "attribut": '"financial_year":',
            "value": this.data.epdb.manage.organization.add_invoice.financial_year,
            "name": "Financial Year"
          },
          {
            "attribut": '"invoice_description":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_description,
            "name": "Invoice Decription"
          },
          {
            "attribut": '"license":',
            "value": this.data.epdb.manage.organization.add_invoice.license,
            "name": "License"
          },
          {
            "attribut": '"total_amount":',
            "value": this.data.epdb.manage.organization.add_invoice.total_amount,
            "name": "Total Amount"
          },
          {
            "attribut": '"total_amount_usd":',
            "value": this.data.epdb.manage.organization.add_invoice.total_amount_usd,
            "name": "Total Amount in USD"
          }
        ]
    }
    // End New Manage Org


    // Start New Manage Site
    else if (value == "manage-site-search") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"search_site":',
            "value": this.data.epdb.manage.site.search.search_site,
            "name": "Search Site"
          },
          {
            "attribut": '"sap_csn":',
            "value": this.data.epdb.manage.site.search.sap_csn,
            "name": "SAP / CSN"
          },
          {
            "attribut": '"sap_id":',
            "value": this.data.epdb.manage.site.search.sap_id,
            "name": "SAP ID"
          }
        ]
    }
    else if (value == "manage-site-detail") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"detail_site":',
            "value": this.data.epdb.manage.site.detail.detail_site,
            "name": "Detail Site"
          },
          {
            "attribut": '"site":',
            "value": this.data.epdb.manage.site.detail.site,
            "name": "Site"
          },
          {
            "attribut": '"site_id":',
            "value": this.data.epdb.manage.site.detail.site_id,
            "name": "Site ID"
          },
          {
            "attribut": '"site_name":',
            "value": this.data.epdb.manage.site.detail.site_name,
            "name": "Site Name"
          },
          {
            "attribut": '"site_status":',
            "value": this.data.epdb.manage.site.detail.site_status,
            "name": "Site Status"
          },
          {
            "attribut": '"primary_site":',
            "value": this.data.epdb.manage.site.detail.primary_site,
            "name": "Primary Site"
          }
        ]
    }
    else if (value == "manage-site-add-edit") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"add_new_site":',
            "value": this.data.epdb.manage.site.add_edit.add_new_site,
            "name": "Add New Site"
          },
          {
            "attribut": '"edit_site":',
            "value": this.data.epdb.manage.site.add_edit.edit_site,
            "name": "Edit Site"
          },
          {
            "attribut": '"english_site_name":',
            "value": this.data.epdb.manage.site.add_edit.english_site_name,
            "name": "English Site Name"
          },
          {
            "attribut": '"commercial_site_name":',
            "value": this.data.epdb.manage.site.add_edit.commercial_site_name,
            "name": "Commercial Site Name"
          },
          {
            "attribut": '"workstation":',
            "value": this.data.epdb.manage.site.add_edit.workstation,
            "name": "Workstation"
          },
          {
            "attribut": '"location_csn":',
            "value": this.data.epdb.manage.site.add_edit.location_csn,
            "name": "Location CSN"
          },
          {
            "attribut": '"parent_csn":',
            "value": this.data.epdb.manage.site.add_edit.parent_csn,
            "name": "Parent CSN"
          },
          {
            "attribut": '"ultimate_parent_csn":',
            "value": this.data.epdb.manage.site.add_edit.ultimate_parent_csn,
            "name": "Ultimate CSN"
          },
          {
            "attribut": '"site_web_address":',
            "value": this.data.epdb.manage.site.add_edit.site_web_address,
            "name": "Site Web Address"
          },
          {
            "attribut": '"site_address":',
            "value": this.data.epdb.manage.site.add_edit.site_address,
            "name": "Site Address"
          },
          {
            "attribut": '"site_manager":',
            "value": this.data.epdb.manage.site.add_edit.site_manager,
            "name": "Site Manager"
          },
          {
            "attribut": '"site_administrator":',
            "value": this.data.epdb.manage.site.add_edit.site_administrator,
            "name": "Site Administrator"
          },
          {
            "attribut": '"initial_accreditation_date":',
            "value": this.data.epdb.manage.site.add_edit.initial_accreditation_date,
            "name": "Initial Accreditation Date"
          }
        ]
    }
    else if (value == "manage-site-add-partner-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"csn":',
            "value": this.data.epdb.manage.site.add_partner_type.csn,
            "name": "CSN"
          },
          {
            "attribut": '"external_id":',
            "value": this.data.epdb.manage.site.add_partner_type.external_id,
            "name": "External ID"
          }
        ]
    }
    else if (value == "manage-organization-add-partner-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"csn":',
            "value": this.data.epdb.manage.organization.add_partner_type.csn,
            "name": "CSN"
          },
          {
            "attribut": '"external_id":',
            "value": this.data.epdb.manage.organization.add_partner_type.external_id,
            "name": "External ID"
          }
        ]
    }
    else if (value == "manage-site-contact-qualification") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"contact_qualification":',
            "value": this.data.epdb.manage.site.contact_qualification.contact_qualification,
            "name": "Contact Qualifications"
          },
          {
            "attribut": '"qualified_instructor":',
            "value": this.data.epdb.manage.site.contact_qualification.qualified_instructor,
            "name": "Qualified Instructors"
          }
        ]
    }
    else if (value == "manage-site-journal-entries") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"site_journal_entries":',
            "value": this.data.epdb.manage.site.site_journal_entries.site_journal_entries,
            "name": "Site Journal Entries"
          },
          {
            "attribut": '"journal_entries_type":',
            "value": this.data.epdb.manage.site.site_journal_entries.journal_entries_type,
            "name": "Journal Entries Type"
          },
          {
            "attribut": '"add_new_site_journal":',
            "value": this.data.epdb.manage.site.site_journal_entries.add_new_site_journal,
            "name": "Add New Site Journal Entries"
          },
          {
            "attribut": '"edit_site_journal":',
            "value": this.data.epdb.manage.site.site_journal_entries.edit_site_journal,
            "name": "Edit Site Journal Entries"
          }
        ]
    }
    // End New Manage Site

    // Start New Manage Contact
    else if (value == "manage-contact-search") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"search_contact":',
            "value": this.data.epdb.manage.contact.search.search_contact,
            "name": "Search Contact"
          }
        ]
    }
    else if (value == "manage-contact-detail") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"detail_contact":',
            "value": this.data.epdb.manage.contact.detail.detail_contact,
            "name": "Detail Contact"
          },
          {
            "attribut": '"contact":',
            "value": this.data.epdb.manage.contact.detail.contact,
            "name": "Contact"
          },
          {
            "attribut": '"contact_id":',
            "value": this.data.epdb.manage.contact.detail.contact_id,
            "name": "Contact ID"
          },
          {
            "attribut": '"contact_name":',
            "value": this.data.epdb.manage.contact.detail.contact_name,
            "name": "Contact Name"
          },
          {
            "attribut": '"eidm_guid":',
            "value": this.data.epdb.manage.contact.detail.eidm_guid,
            "name": "EIDM GUID"
          },
          {
            "attribut": '"title_position":',
            "value": this.data.epdb.manage.contact.detail.title_position,
            "name": "Title / Posistion"
          },
          {
            "attribut": '"mobile":',
            "value": this.data.epdb.manage.contact.detail.mobile,
            "name": "Mobile"
          },
          {
            "attribut": '"time_zone":',
            "value": this.data.epdb.manage.contact.detail.time_zone,
            "name": "Time Zone"
          },
          {
            "attribut": '"comment":',
            "value": this.data.epdb.manage.contact.detail.comment,
            "name": "Comments"
          },
          {
            "attribut": '"eidm_user_id":',
            "value": this.data.epdb.manage.contact.detail.eidm_user_id,
            "name": "EIDM User ID"
          },
          {
            "attribut": '"last_login":',
            "value": this.data.epdb.manage.contact.detail.last_login,
            "name": "Last Login"
          },
          {
            "attribut": '"last_valid":',
            "value": this.data.epdb.manage.contact.detail.last_valid,
            "name": "Last Validated"
          },
          {
            "attribut": '"primary_industry":',
            "value": this.data.epdb.manage.contact.detail.primary_industry,
            "name": "Primary Industry"
          },
          {
            "attribut": '"atc_instructor":',
            "value": this.data.epdb.manage.contact.detail.atc_instructor,
            "name": "ATC Instructor"
          },
          {
            "attribut": '"date_questionnaire_complete":',
            "value": this.data.epdb.manage.contact.detail.date_questionnaire_complete,
            "name": "Date Questionnaire Completed"
          },
          {
            "attribut": '"primary_job_role":',
            "value": this.data.epdb.manage.contact.detail.primary_job_role,
            "name": "Primary Job Role"
          },
          {
            "attribut": '"dedicated":',
            "value": this.data.epdb.manage.contact.detail.dedicated,
            "name": "Dedicated"
          },
          {
            "attribut": '"btn_affiliation":',
            "value": this.data.epdb.manage.contact.detail.btn_affiliation,
            "name": "Label Affiliations Button"
          }
        ]
    }
    else if (value == "manage-contact-add-edit") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"add_new_contact":',
            "value": this.data.epdb.manage.contact.add_edit.add_new_contact,
            "name": "Add New Contact"
          },
          {
            "attribut": '"message_add_contact":',
            "value": this.data.epdb.manage.contact.add_edit.message_add_contact,
            "name": "Message add Contact"
          },
          {
            "attribut": '"edit_contact":',
            "value": this.data.epdb.manage.contact.add_edit.edit_contact,
            "name": "Edit Comtact"
          },
          {
            "attribut": '"email_address_username":',
            "value": this.data.epdb.manage.contact.add_edit.email_address_username,
            "name": "Email Address or Username"
          },
          {
            "attribut": '"or_please_click":',
            "value": this.data.epdb.manage.contact.add_edit.or_please_click,
            "name": "Label please click here to choose"
          },
          {
            "attribut": '"btn_choose_affiliated":',
            "value": this.data.epdb.manage.contact.add_edit.btn_choose_affiliated,
            "name": "Label Choose from Affiliated Contacts Button"
          },
          {
            "attribut": '"honorific":',
            "value": this.data.epdb.manage.contact.add_edit.honorific,
            "name": "Honorific"
          },
          {
            "attribut": '"dont_send_email":',
            "value": this.data.epdb.manage.contact.add_edit.dont_send_email,
            "name": "Don't send Email"
          },
          {
            "attribut": '"autodesk_internal":',
            "value": this.data.epdb.manage.contact.add_edit.autodesk_internal,
            "name": "Autodesk Internal"
          },
          {
            "attribut": '"sentence_1":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_1,
            "name": "Sentence 1"
          },
          {
            "attribut": '"sentence_2":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_2,
            "name": "Sentence 2"
          },
          {
            "attribut": '"sentence_3":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_3,
            "name": "Sentence 3"
          },
          {
            "attribut": '"sentence_4":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_4,
            "name": "Sentence 4"
          }
        ]
    }
    else if (value == "manage-contact-site-affiliation") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"site_affiliated":',
            "value": this.data.epdb.manage.contact.site_affiliated.site_affiliated,
            "name": "Sites Affiliated"
          },
          {
            "attribut": '"site_location":',
            "value": this.data.epdb.manage.contact.site_affiliated.site_location,
            "name": "Site Location"
          },
          {
            "attribut": '"p1":',
            "value": this.data.epdb.manage.contact.site_affiliated.p1,
            "name": "Sentence 1"
          },
          {
            "attribut": '"p2":',
            "value": this.data.epdb.manage.contact.site_affiliated.p2,
            "name": "Sentence 2"
          },
          {
            "attribut": '"p3":',
            "value": this.data.epdb.manage.contact.site_affiliated.p3,
            "name": "Sentence 3"
          },
          {
            "attribut": '"p4":',
            "value": this.data.epdb.manage.contact.site_affiliated.p4,
            "name": "Sentence 4"
          }
        ]
    }
    else if (value == "manage-contact-journal-entries") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [

        ]
    }
    // End New Manage Contact


    else if (value == "eva-manage-course-list") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"list_course":',
            "value": this.data.eva.manage_course.list_course.list_course,
            "name": "Search Course"
          },
          {
            "attribut": '"btn_add_course":',
            "value": this.data.eva.manage_course.list_course.btn_add_course,
            "name": "Label Add Course Button"
          },
          {
            "attribut": '"course_id":',
            "value": this.data.eva.manage_course.list_course.course_id,
            "name": "Course ID"
          },
          {
            "attribut": '"course_name":',
            "value": this.data.eva.manage_course.list_course.course_name,
            "name": "Course Name"
          },
          {
            "attribut": '"course_title":',
            "value": this.data.eva.manage_course.list_course.course_title,
            "name": "Course Title"
          },
          {
            "attribut": '"survey_partner_type":',
            "value": this.data.eva.manage_course.list_course.survey_partner_type,
            "name": "Survey Partner Type"
          },
          {
            "attribut": '"instructor":',
            "value": this.data.eva.manage_course.list_course.instructor,
            "name": "Instructor"
          },
          {
            "attribut": '"instructor_id":',
            "value": this.data.eva.manage_course.list_course.instructor_id,
            "name": "Instructor ID"
          },
          {
            "attribut": '"instructor_name":',
            "value": this.data.eva.manage_course.list_course.instructor_name,
            "name": "Instructor Name"
          },
          {
            "attribut": '"teaching_hours":',
            "value": this.data.eva.manage_course.list_course.teaching_hours,
            "name": "Teaching Hours"
          },
          {
            "attribut": '"course_level":',
            "value": this.data.eva.manage_course.list_course.course_level,
            "name": "Course Level"
          },
          {
            "attribut": '"enroll_course":',
            "value": this.data.eva.manage_course.list_course.enroll_course,
            "name": "Enroll Course"
          },
          {
            "attribut": '"invalid_search_course":',
            "value": this.data.eva.manage_course.list_course.invalid_search_course,
            "name": "Course ID you entered is not found, please contact your instructor"
          }
        ]
    }
    else if (value == "eva-manage-course-detail") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"detail_course":',
            "value": this.data.eva.manage_course.detail.detail_course,
            "name": "Detail Course"
          },
          {
            "attribut": '"hour_training":',
            "value": this.data.eva.manage_course.detail.hour_training,
            "name": "Hours Training"
          },
          {
            "attribut": '"survey_indicator":',
            "value": this.data.eva.manage_course.detail.survey_indicator,
            "name": "Survey Indicator"
          },
          {
            "attribut": '"institution":',
            "value": this.data.eva.manage_course.detail.institution,
            "name": "Institution"
          },
          {
            "attribut": '"teaching_level":',
            "value": this.data.eva.manage_course.detail.teaching_level,
            "name": "Teaching Level"
          },
          {
            "attribut": '"version":',
            "value": this.data.eva.manage_course.detail.version,
            "name": "Version"
          }
        ]
    }
    else if (value == "eva-manage-course-add") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"add_course":',
            "value": this.data.eva.manage_course.add.add_course,
            "name": "Add Course"
          },
          {
            "attribut": '"edit_course":',
            "value": this.data.eva.manage_course.add.edit_course,
            "name": "Edit Course"
          },
          {
            "attribut": '"course_facility":',
            "value": this.data.eva.manage_course.add.course_facility,
            "name": "Course Facility"
          },
          {
            "attribut": '"computer_equipment":',
            "value": this.data.eva.manage_course.add.computer_equipment,
            "name": "Computer Equipment"
          },
          {
            "attribut": '"survey":',
            "value": this.data.eva.manage_course.add.survey,
            "name": "Survey"
          },
          {
            "attribut": '"upload_institution_logo":',
            "value": this.data.eva.manage_course.add.upload_institution_logo,
            "name": "Upload Institution Logo"
          },
          {
            "attribut": '"course_date_from":',
            "value": this.data.eva.manage_course.add.course_date_from,
            "name": "Course Date From"
          },
          {
            "attribut": '"project_date_from":',
            "value": this.data.eva.manage_course.add.project_date_from,
            "name": "Project Date From"
          },
          {
            "attribut": '"event_date_from":',
            "value": this.data.eva.manage_course.add.event_date_from,
            "name": "Event Date From"
          },
          {
            "attribut": '"to":',
            "value": this.data.eva.manage_course.add.to,
            "name": "To"
          },
          {
            "attribut": '"tag_student":',
            "value": this.data.eva.manage_course.add.tag_student,
            "name": "Tag Student"
          },
          {
            "attribut": '"find_student":',
            "value": this.data.eva.manage_course.add.find_student,
            "name": "Find Student"
          },
          {
            "attribut": '"type_student":',
            "value": this.data.eva.manage_course.add.type_student,
            "name": "Type to find Students here"
          },
          {
            "attribut": '"current_student":',
            "value": this.data.eva.manage_course.add.current_student,
            "name": "Current Student"
          }
        ]
    }

    // Report
    // if (value == "epdb-report-organization") {
    //   this.menuselect = true;
    //   this.submenuselect = false;
    //   this.contentarr =
    //     [

    //     ]
    // }
    // else if (value == "epdb-report-site") {
    //   this.menuselect = true;
    //   this.submenuselect = false;
    //   this.contentarr =
    //     [

    //     ]
    // }
    /*else if (value == "epdb-report-contact") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"contact_report":',
            "value": this.data.epdb.report.contact_report.contact_report,
            "name": "Contact Report"
          },
          {
            "attribut": '"filter_by_email":',
            "value": this.data.epdb.report.contact_report.filter_by_email,
            "name": "Filter by Email"
          },
          {
            "attribut": '"filter_by_site_name":',
            "value": this.data.epdb.report.contact_report.filter_by_site_name,
            "name": "Filter by Site Name"
          },
          {
            "attribut": '"filter_by_contact_role":',
            "value": this.data.epdb.report.contact_report.filter_by_contact_role,
            "name": "Filter by Contact Role"
          },
          {
            "attribut": '"atc_role":',
            "value": this.data.epdb.report.contact_report.atc_role,
            "name": "Where ATCRole"
          },
          {
            "attribut": '"filter_by_industry":',
            "value": this.data.epdb.report.contact_report.filter_by_industry,
            "name": "Filter by Industry"
          },
          {
            "attribut": '"use_primary":',
            "value": this.data.epdb.report.contact_report.use_primary,
            "name": "Use Primary Site Only"
          },
          {
            "attribut": '"only_show_user":',
            "value": this.data.epdb.report.contact_report.only_show_user,
            "name": "Only show users who have logged in after Mar 1, 2009 Who logged in the last"
          },
          {
            "attribut": '"contact_field":',
            "value": this.data.epdb.report.contact_report.contact_field,
            "name": "Contact Field"
          }
        ]
    }*/
    /*else if (value == "epdb-report-email") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"email_list_generator":',
            "value": this.data.epdb.report.email_list_generator.email_list_generator,
            "name": "Email List Generator"
          },
          {
            "attribut": '"only_show_user":',
            "value": this.data.epdb.report.email_list_generator.only_show_user,
            "name": "Only show users who have logged in after Mar 1, 2009"
          },
          {
            "attribut": '"filter_by_contact_name":',
            "value": this.data.epdb.report.email_list_generator.filter_by_contact_name,
            "name": "Filter by Contact Name"
          },
          {
            "attribut": '"who_looged":',
            "value": this.data.epdb.report.email_list_generator.who_looged,
            "name": "Who logged in the last"
          },
          {
            "attribut": '"filter_by_language":',
            "value": this.data.epdb.report.email_list_generator.filter_by_language,
            "name": "Filter by Language"
          },
          {
            "attribut": '"change_standard":',
            "value": this.data.epdb.report.email_list_generator.change_standard,
            "name": "Change Standard Behavior"
          },
          {
            "attribut": '"use_primary":',
            "value": this.data.epdb.report.email_list_generator.use_primary,
            "name": "Use Primary Site Only"
          },
          {
            "attribut": '"show_even_dont_email":',
            "value": this.data.epdb.report.email_list_generator.show_even_dont_email,
            "name": "Show even if Do not email is checked"
          },
          {
            "attribut": '"show_even_email":',
            "value": this.data.epdb.report.email_list_generator.show_even_email,
            "name": "Show even if email is like test"
          },
          {
            "attribut": '"number_address":',
            "value": this.data.epdb.report.email_list_generator.number_address,
            "name": "Number of addresses per list"
          }
        ]
    }*/
    else if (value == "epdb-report-show-security") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"show_security":',
            "value": this.data.epdb.report.show_security.show_security,
            "name": "Title in Show Security Roles Assignments"
          },
          {
            "attribut": '"group_name":',
            "value": this.data.epdb.report.show_security.group_name,
            "name": "Group Name"
          }
        ]
    }
    else if (value == "epdb-report-org-journal-entry") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          // {
          //   "attribut": '"org_journal_entry":',
          //   "value": this.data.epdb.report.org_journal_entry.org_journal_entry,
          //   "name": "Title in Organization Journal Entry Report"
          // },
          // {
          //   "attribut": '"activity_subject":',
          //   "value": this.data.epdb.report.org_journal_entry.activity_subject,
          //   "name": "Check for this Activity / Subject"
          // },
          // {
          //   "attribut": '"where_the_setting":',
          //   "value": this.data.epdb.report.org_journal_entry.where_the_setting,
          //   "name": "Where the setting"
          // },
          // {
          //   "attribut": '"is_not_present":',
          //   "value": this.data.epdb.report.org_journal_entry.is_not_present,
          //   "name": "Is NOT Present"
          // },
          // {
          //   "attribut": '"is_present":',
          //   "value": this.data.epdb.report.org_journal_entry.is_present,
          //   "name": "Is Present"
          // },
          // {
          //   "attribut": '"activity_date":',
          //   "value": this.data.epdb.report.org_journal_entry.activity_date,
          //   "name": "And where the activity date is between"
          // },
          // {
          //   "attribut": '"date_value":',
          //   "value": this.data.epdb.report.org_journal_entry.date_value,
          //   "name": "Note: date values are used for 'Is Present' only"
          // }
        ]
    }
    else if (value == "epdb-report-invoice-report") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          // {
          //   "attribut": '"invoice_report":',
          //   "value": this.data.epdb.report.invoice_report.invoice_report,
          //   "name": "Title in Invoice Report"
          // },
          // {
          //   "attribut": '"invoice_type":',
          //   "value": this.data.epdb.report.invoice_report.invoice_type,
          //   "name": "Check for this Invoice Type"
          // },
          // {
          //   "attribut": '"entry_was_created":',
          //   "value": this.data.epdb.report.invoice_report.entry_was_created,
          //   "name": "And where the entry was created between"
          // },
          // {
          //   "attribut": '"preselected":',
          //   "value": this.data.epdb.report.invoice_report.preselected,
          //   "name": "Preselected 'Output Fields'"
          // }
        ]
    }
    else if (value == "epdb-report-qualification-by-contact") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          // {
          //   "attribut": '"qualification_by_contact":',
          //   "value": this.data.epdb.report.qualification_by_contact.qualification_by_contact,
          //   "name": "Title in Qualifications By Contact Report"
          // },
          // {
          //   "attribut": '"choose_one_more":',
          //   "value": this.data.epdb.report.qualification_by_contact.choose_one_more,
          //   "name": "Choose One or More"
          // },
          // {
          //   "attribut": '"is_qualified":',
          //   "value": this.data.epdb.report.qualification_by_contact.is_qualified,
          //   "name": "Is Qualified"
          // },
          // {
          //   "attribut": '"is_not_qualified":',
          //   "value": this.data.epdb.report.qualification_by_contact.is_not_qualified,
          //   "name": "Is Not Qualified"
          // }
        ]
    }
    else if (value == "epdb-report-contact-attribute") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"contact_attribute":',
            "value": this.data.epdb.report.contact_attribute.contact_attribute,
            "name": "Title in Contact Attributes Report"
          }
        ]
    }
    else if (value == "epdb-database-integrity-report") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          // {
          //   "attribut": '"site_organization":',
          //   "value": this.data.epdb.report.database_integrity.organization.site_organization,
          //   "name": "Organization : Siteless Organizations"
          // },
          // {
          //   "attribut": '"org_no_address":',
          //   "value": this.data.epdb.report.database_integrity.organization.org_no_address,
          //   "name": "Organization : Organizations with No Address"
          // },
          // {
          //   "attribut": '"orphaned_site":',
          //   "value": this.data.epdb.report.database_integrity.site.orphaned_site,
          //   "name": "Site : Orphaned Sites"
          // },
          // {
          //   "attribut": '"site_no_address":',
          //   "value": this.data.epdb.report.database_integrity.site.site_no_address,
          //   "name": "Site : Sites with No Address"
          // },
          // {
          //   "attribut": '"site_no_contact":',
          //   "value": this.data.epdb.report.database_integrity.site.site_no_contact,
          //   "name": "Site : Sites with No Contacts"
          // },
          // {
          //   "attribut": '"site_address":',
          //   "value": this.data.epdb.report.database_integrity.site.site_address,
          //   "name": "Site : Sites with similar Address1"
          // },
          // {
          //   "attribut": '"siebel_site_address":',
          //   "value": this.data.epdb.report.database_integrity.site.siebel_site_address,
          //   "name": "Site : SiebelAddress <> SiteAddress"
          // },
          // {
          //   "attribut": '"bad_csn_match":',
          //   "value": this.data.epdb.report.database_integrity.site.bad_csn_match,
          //   "name": "Site : Bad CSN Matches"
          // },
          // {
          //   "attribut": '"pdb_siebel_status":',
          //   "value": this.data.epdb.report.database_integrity.site.pdb_siebel_status,
          //   "name": "Site : PDB Status <> Siebel Status"
          // },
          // {
          //   "attribut": '"orphaned_contact":',
          //   "value": this.data.epdb.report.database_integrity.contact.orphaned_contact,
          //   "name": "Contact : Orphaned Contacts"
          // },
          // {
          //   "attribut": '"duplicate_name":',
          //   "value": this.data.epdb.report.database_integrity.contact.duplicate_name,
          //   "name": "Contact : Potential Duplicate Users by Name"
          // },
          // {
          //   "attribut": '"duplicate_siebelid":',
          //   "value": this.data.epdb.report.database_integrity.contact.duplicate_siebelid,
          //   "name": "Contact : Potential Duplicate Users by SiebelId"
          // },
          // {
          //   "attribut": '"siebel_match":',
          //   "value": this.data.epdb.report.database_integrity.contact.siebel_match,
          //   "name": "Contact : Siebel Matches to verify"
          // },
          // {
          //   "attribut": '"active_user":',
          //   "value": this.data.epdb.report.database_integrity.contact.active_user,
          //   "name": "Contact : Active Users in more than one Organization"
          // },
          // {
          //   "attribut": '"duplicate_email":',
          //   "value": this.data.epdb.report.database_integrity.contact.duplicate_email,
          //   "name": "Contact : icate Email Addresses"
          // },
          // {
          //   "attribut": '"illegal_email":',
          //   "value": this.data.epdb.report.database_integrity.contact.illegal_email,
          //   "name": "Contact : Illegal? Email Addresses"
          // },
          // {
          //   "attribut": '"bad_primary":',
          //   "value": this.data.epdb.report.database_integrity.contact.bad_primary,
          //   "name": "Contact : Bad Primary Site Id"
          // },
          // {
          //   "attribut": '"missing_primary":',
          //   "value": this.data.epdb.report.database_integrity.contact.missing_primary,
          //   "name": "Contact : Missing Primary Site Id"
          // },
          // {
          //   "attribut": '"missing_name":',
          //   "value": this.data.epdb.report.database_integrity.contact.missing_name,
          //   "name": "Contact : Missing First or Last Name"
          // },
          // {
          //   "attribut": '"duplicate_user_history":',
          //   "value": this.data.epdb.report.database_integrity.contact.duplicate_user_history,
          //   "name": "Contact : Duplicate (hidden) Users with LMS History"
          // },
          // {
          //   "attribut": '"duplicate_user_certification":',
          //   "value": this.data.epdb.report.database_integrity.contact.duplicate_user_certification,
          //   "name": "Contact : Duplicate (hidden) Users with LMS Certifications"
          // },
          // {
          //   "attribut": '"other":',
          //   "value": this.data.epdb.report.database_integrity.other.other,
          //   "name": "Other : Other"
          // },
          // {
          //   "attribut": '"duplicate_attribute":',
          //   "value": this.data.epdb.report.database_integrity.other.duplicate_attribute,
          //   "name": "Other : Duplicate Attributes"
          // },
          // {
          //   "attribut": '"illegal_state":',
          //   "value": this.data.epdb.report.database_integrity.other.illegal_state,
          //   "name": "Other : Illegal State/Province/Province values"
          // },
          // {
          //   "attribut": '"instructor_whose":',
          //   "value": this.data.epdb.report.database_integrity.other.instructor_whose,
          //   "name": "Other : Instructors whose name changed during the ATC Migration"
          // },
          // {
          //   "attribut": '"atc_instructor":',
          //   "value": this.data.epdb.report.database_integrity.other.atc_instructor,
          //   "name": "Other : ATC Instructors missing Active for on-line eval = On"
          // }
        ]
    }
    else if (value == "epdb-history-report") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          // {
          //   "attribut": '"history_report":',
          //   "value": this.data.epdb.report.history_report.history_report,
          //   "name": "History Report"
          // },
          // {
          //   "attribut": '"text_match":',
          //   "value": this.data.epdb.report.history_report.text_match,
          //   "name": "Text to Match"
          // },
          // {
          //   "attribut": '"last_day":',
          //   "value": this.data.epdb.report.history_report.last_day,
          //   "name": "Last X days"
          // },
          // {
          //   "attribut": '"user_id":',
          //   "value": this.data.epdb.report.history_report.user_id,
          //   "name": "User ID"
          // }
        ]
    }

    // Menu Activities
    else if (value == "menu-admin-activities") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"admin":',
            "value": this.data.menu_admin.admin,
            "name": "Menu Admin"
          },
          {
            "attribut": '"activities":',
            "value": this.data.menu_admin.activities.activities,
            "name": "Menu Activities"
          },
          {
            "attribut": '"list_activities":',
            "value": this.data.menu_admin.activities.list_activities,
            "name": "Menu List Activities"
          },
          {
            "attribut": '"activities":',
            "value": this.data.menu_admin.activities.add_activities,
            "name": "Menu Add Activities"
          }
        ]
    }

    // Menu Product
    else if (value == "menu-admin-products") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"products":',
            "value": this.data.menu_admin.products.products,
            "name": "Menu Products"
          },
          {
            "attribut": '"list_category":',
            "value": this.data.menu_admin.products.list_category,
            "name": "Menu List Category"
          },
          {
            "attribut": '"add_category":',
            "value": this.data.menu_admin.products.add_category,
            "name": "Menu Add Category"
          },
          {
            "attribut": '"list_product":',
            "value": this.data.menu_admin.products.list_product,
            "name": "Menu List Product"
          },
          {
            "attribut": '"add_product":',
            "value": this.data.menu_admin.products.add_product,
            "name": "Menu Add Product"
          }
        ]
    }

    // Menu SKU
    else if (value == "menu-admin-sku") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"sku":',
            "value": this.data.menu_admin.sku.sku,
            "name": "Menu SKU"
          },
          {
            "attribut": '"list_sku":',
            "value": this.data.menu_admin.sku.list_sku,
            "name": "Menu List SKU"
          },
          {
            "attribut": '"add_sku":',
            "value": this.data.menu_admin.sku.add_sku,
            "name": "Menu Add SKU"
          }
        ]
    }

    // Menu Glossary
    else if (value == "menu-admin-glossary") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"glossary":',
            "value": this.data.menu_admin.glossary.glossary,
            "name": "Menu Glossary"
          },
          {
            "attribut": '"list_glossary":',
            "value": this.data.menu_admin.glossary.list_glossary,
            "name": "Menu List Glossary"
          },
          {
            "attribut": '"add_glossary":',
            "value": this.data.menu_admin.glossary.add_glossary,
            "name": "Menu Add Glossary"
          }
        ]
    }

    // Menu Country
    else if (value == "menu-admin-country") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"country":',
            "value": this.data.menu_admin.country.country,
            "name": "Menu Country"
          },
          {
            "attribut": '"list_geo":',
            "value": this.data.menu_admin.country.list_geo,
            "name": "Menu List Geo"
          },
          {
            "attribut": '"add_geo":',
            "value": this.data.menu_admin.country.add_geo,
            "name": "Menu Add Geo"
          },
          {
            "attribut": '"list_territory":',
            "value": this.data.menu_admin.country.list_territory,
            "name": "Menu List Territory"
          },
          {
            "attribut": '"add_territory":',
            "value": this.data.menu_admin.country.add_territory,
            "name": "Menu Add Territory"
          },
          {
            "attribut": '"list_region":',
            "value": this.data.menu_admin.country.list_region,
            "name": "Menu List Region"
          },
          {
            "attribut": '"add_region":',
            "value": this.data.menu_admin.country.add_region,
            "name": "Menu Add Region"
          },
          {
            "attribut": '"list_sub_region":',
            "value": this.data.menu_admin.country.list_sub_region,
            "name": "Menu List Sub Region"
          },
          {
            "attribut": '"add_sub_region":',
            "value": this.data.menu_admin.country.add_sub_region,
            "name": "Menu Add Sub Region"
          },
          {
            "attribut": '"list_countries":',
            "value": this.data.menu_admin.country.list_countries,
            "name": "Menu List Countries"
          },
          {
            "attribut": '"add_countries":',
            "value": this.data.menu_admin.country.add_countries,
            "name": "Menu Add Countries"
          }
        ]
    }

    // Menu Market Type
    else if (value == "menu-admin-market-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"market_type":',
            "value": this.data.menu_admin.market_type.market_type,
            "name": "Menu Market Type"
          },
          {
            "attribut": '"list_market_type":',
            "value": this.data.menu_admin.market_type.list_market_type,
            "name": "Menu List Market Type"
          },
          {
            "attribut": '"add_sku":',
            "value": this.data.menu_admin.market_type.add_market_type,
            "name": "Menu Add Market Type"
          }
        ]
    }

    // Menu Variable
    else if (value == "menu-admin-variables") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"variables":',
            "value": this.data.menu_admin.variables.variables,
            "name": "Menu Variables"
          },
          {
            "attribut": '"list_market_type":',
            "value": this.data.menu_admin.variables.list_variables,
            "name": "Menu List Variables"
          },
          {
            "attribut": '"add_sku":',
            "value": this.data.menu_admin.variables.add_variables,
            "name": "Menu Add Variables"
          }
        ]
    }

    // Menu Partner Type
    else if (value == "menu-admin-partner-type") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"partner_type":',
            "value": this.data.menu_admin.partner_type.menu_partner_type,
            "name": "Menu Partner Type"
          },
          {
            "attribut": '"list_market_type":',
            "value": this.data.menu_admin.partner_type.menu_partner_type,
            "name": "Menu Sub Partner Type"
          },
          {
            "attribut": '"add_sku":',
            "value": this.data.menu_admin.partner_type.menu_partner_type,
            "name": "Menu Partner Type"
          }
        ]
    }

    // Menu Distributor
    else if (value == "menu-admin-distributor") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"distributor":',
            "value": this.data.menu_admin.distributor.distributor,
            "name": "Menu Distributor"
          },
          {
            "attribut": '"list_market_type":',
            "value": this.data.menu_admin.distributor.search_distributor,
            "name": "Menu Search Distributor"
          },
          {
            "attribut": '"add_sku":',
            "value": this.data.menu_admin.distributor.add_distributor,
            "name": "Menu Add Distributor"
          }
        ]
    }

    // Menu Currency
    else if (value == "menu-admin-currency") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"currency":',
            "value": this.data.menu_admin.currency.currency,
            "name": "Menu Currency"
          }
        ]
    }

    // Menu Distributor
    else if (value == "menu-admin-language") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"language":',
            "value": this.data.menu_admin.language.language,
            "name": "Menu Language"
          },
          {
            "attribut": '"list_language":',
            "value": this.data.menu_admin.language.list_language,
            "name": "Menu List Language"
          }
        ]
    }

    // Menu Distributor
    else if (value == "menu-admin-email-body") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"email_body":',
            "value": this.data.menu_admin.email_body.email_body,
            "name": "Menu Email Body"
          },
          {
            "attribut": '"list_market_type":',
            "value": this.data.menu_admin.email_body.list_email_body,
            "name": "Menu List Email Body"
          },
          {
            "attribut": '"add_sku":',
            "value": this.data.menu_admin.email_body.add_edit_email_body,
            "name": "Menu Add / Edit Email Body"
          }
        ]
    }

    // Menu Manage Organization
    else if (value == "menu-manage-organization") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"manage":',
            "value": this.data.menu_manage.manage,
            "name": "Menu Manage Partner"
          },
          {
            "attribut": '"organization":',
            "value": this.data.menu_manage.organization.organization,
            "name": "Menu Organization"
          },
          {
            "attribut": '"add_organization":',
            "value": this.data.menu_manage.organization.add_organization,
            "name": "Menu Add New Organization"
          },
          {
            "attribut": '"search_organization":',
            "value": this.data.menu_manage.organization.search_organization,
            "name": "Menu Add Search Organization"
          }
        ]
    }

    // Menu Manage Site
    else if (value == "menu-manage-site") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"site":',
            "value": this.data.menu_manage.site.site,
            "name": "Menu Site"
          },
          {
            "attribut": '"search_site":',
            "value": this.data.menu_manage.site.search_site,
            "name": "Menu Search Site"
          }
        ]
    }

    // Menu Manage Contact
    else if (value == "menu-manage-contact") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"contact":',
            "value": this.data.menu_manage.contact.contact,
            "name": "Menu Contact"
          },
          {
            "attribut": '"search_contact":',
            "value": this.data.menu_manage.contact.search_contact,
            "name": "Menu Search Contact"
          },
          {
            "attribut": '"add_new_contact":',
            "value": this.data.menu_manage.contact.add_new_contact,
            "name": "Menu Add New Contact"
          }
        ]
    }

    // New Admin Country Geo
    else if (value == "admin-country-geo") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"geo":',
            "value": this.data.epdb.admin.country.geo.geo,
            "name": "Geo"
          },
          {
            "attribut": '"geo_list":',
            "value": this.data.epdb.admin.country.geo.geo_list,
            "name": "Title in List Geo"
          },
          {
            "attribut": '"add_new_geo":',
            "value": this.data.epdb.admin.country.geo.add_new_geo,
            "name": "Title in Add New Geo"
          },
          {
            "attribut": '"edit_geo":',
            "value": this.data.epdb.admin.country.geo.edit_geo,
            "name": "Title in Edit Geo"
          },
          {
            "attribut": '"btn_add_geo":',
            "value": this.data.epdb.admin.country.geo.btn_add_geo,
            "name": "Label Add Geo Button"
          },
          {
            "attribut": '"geo_code":',
            "value": this.data.epdb.admin.country.geo.geo_code,
            "name": "Geo Code"
          },
          {
            "attribut": '"geo_name":',
            "value": this.data.epdb.admin.country.geo.geo_name,
            "name": "Geo Name"
          }
        ]
    }
    // New Admin Country Territory
    else if (value == "admin-country-territory") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"territory":',
            "value": this.data.epdb.admin.country.territory.territory,
            "name": "Territory"
          },
          {
            "attribut": '"territory_list":',
            "value": this.data.epdb.admin.country.territory.territory_list,
            "name": "Title in List Territory"
          },
          {
            "attribut": '"add_new_territory":',
            "value": this.data.epdb.admin.country.territory.add_new_territory,
            "name": "Title in Add New Territory"
          },
          {
            "attribut": '"edit_territory":',
            "value": this.data.epdb.admin.country.territory.edit_territory,
            "name": "Title in Edit Territory"
          },
          {
            "attribut": '"btn_add_territory":',
            "value": this.data.epdb.admin.country.territory.btn_add_territory,
            "name": "Label Add Territory Button"
          },
          {
            "attribut": '"territory_id":',
            "value": this.data.epdb.admin.country.territory.territory_id,
            "name": "Territory ID"
          },
          {
            "attribut": '"territory_name":',
            "value": this.data.epdb.admin.country.territory.territory_name,
            "name": "Territory Name"
          }
        ]
    }
    // New Admin Country Region
    else if (value == "admin-country-region") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"region":',
            "value": this.data.epdb.admin.country.region.region,
            "name": "Region"
          },
          {
            "attribut": '"region_list":',
            "value": this.data.epdb.admin.country.region.region_list,
            "name": "Title in List Region"
          },
          {
            "attribut": '"add_new_region":',
            "value": this.data.epdb.admin.country.region.add_new_region,
            "name": "Title in Add New Region"
          },
          {
            "attribut": '"edit_region":',
            "value": this.data.epdb.admin.country.region.edit_region,
            "name": "Title in Edit Region"
          },
          {
            "attribut": '"btn_add_region":',
            "value": this.data.epdb.admin.country.region.btn_add_region,
            "name": "Label Add Region Button"
          },
          {
            "attribut": '"region_code":',
            "value": this.data.epdb.admin.country.region.region_code,
            "name": "Region Code"
          },
          {
            "attribut": '"region_name":',
            "value": this.data.epdb.admin.country.region.region_name,
            "name": "Region Name"
          }
        ]
    }
    // New Admin Country Sub Region
    else if (value == "admin-country-subregion") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"sub_region":',
            "value": this.data.epdb.admin.country.sub_region.sub_region,
            "name": "Sub Region"
          },
          {
            "attribut": '"sub_region_list":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_list,
            "name": "Title in Sub Region List"
          },
          {
            "attribut": '"add_new_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.add_new_sub_region,
            "name": "Title in Add New Sub Region"
          },
          {
            "attribut": '"edit_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.edit_sub_region,
            "name": "Title in Edit Sub Region"
          },
          {
            "attribut": '"btn_add_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.btn_add_sub_region,
            "name": "Label Add Sub Region Button"
          },
          {
            "attribut": '"sub_region_id":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_id,
            "name": "Sub Region ID"
          },
          {
            "attribut": '"sub_region_code":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_code,
            "name": "Sub Region Code"
          },
          {
            "attribut": '"sub_region_name":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_name,
            "name": "Sub Region Name"
          }
        ]
    }
    // New Admin Country Countries
    else if (value == "admin-country-countries") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
        [
          {
            "attribut": '"country":',
            "value": this.data.epdb.admin.country.countries.country,
            "name": "Country"
          },
          {
            "attribut": '"countries_list":',
            "value": this.data.epdb.admin.country.countries.countries_list,
            "name": "Title in List Countries"
          },
          {
            "attribut": '"add_new_country":',
            "value": this.data.epdb.admin.country.countries.add_new_country,
            "name": "Title in Add New Country"
          },
          {
            "attribut": '"edit_country":',
            "value": this.data.epdb.admin.country.countries.edit_country,
            "name": "Title in Edit Country"
          },
          {
            "attribut": '"btn_add_country":',
            "value": this.data.epdb.admin.country.countries.btn_add_country,
            "name": "Label Add Countries Button"
          },
          {
            "attribut": '"country_code":',
            "value": this.data.epdb.admin.country.countries.country_code,
            "name": "Country Code"
          },
          {
            "attribut": '"country_name":',
            "value": this.data.epdb.admin.country.countries.country_name,
            "name": "Country Name"
          },
          {
            "attribut": '"country_phone_code":',
            "value": this.data.epdb.admin.country.countries.country_phone_code,
            "name": "Country Phone Code"
          }
        ]
    }
  }


  picksubmenucontent(value) {
    this.submenuselectcontent = true;
    // sub country
    /*if (value == "epdb-admin-country-geo") {
      this.contentarr =
        [
          {
            "attribut": '"geo":',
            "value": this.data.epdb.admin.country.geo.geo,
            "name": "Geo"
          },
          {
            "attribut": '"geo_list":',
            "value": this.data.epdb.admin.country.geo.geo_list,
            "name": "Title in List Geo"
          },
          {
            "attribut": '"add_new_geo":',
            "value": this.data.epdb.admin.country.geo.add_new_geo,
            "name": "Title in Add New Geo"
          },
          {
            "attribut": '"edit_geo":',
            "value": this.data.epdb.admin.country.geo.edit_geo,
            "name": "Title in Edit Geo"
          },
          {
            "attribut": '"btn_add_geo":',
            "value": this.data.epdb.admin.country.geo.btn_add_geo,
            "name": "Label Add Geo Button"
          },
          {
            "attribut": '"geo_id":',
            "value": this.data.epdb.admin.country.geo.geo_id,
            "name": "Geo ID"
          },
          {
            "attribut": '"geo_code":',
            "value": this.data.epdb.admin.country.geo.geo_code,
            "name": "Geo Code"
          },
          {
            "attribut": '"geo_name":',
            "value": this.data.epdb.admin.country.geo.geo_name,
            "name": "Geo Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-country-territory") {
      this.contentarr =
        [
          {
            "attribut": '"territory":',
            "value": this.data.epdb.admin.country.territory.territory,
            "name": "Territory"
          },
          {
            "attribut": '"territory_list":',
            "value": this.data.epdb.admin.country.territory.territory_list,
            "name": "Title in List Territory"
          },
          {
            "attribut": '"add_new_territory":',
            "value": this.data.epdb.admin.country.territory.add_new_territory,
            "name": "Title in Add New Territory"
          },
          {
            "attribut": '"edit_territory":',
            "value": this.data.epdb.admin.country.territory.edit_territory,
            "name": "Title in Edit Territory"
          },
          {
            "attribut": '"btn_add_territory":',
            "value": this.data.epdb.admin.country.territory.btn_add_territory,
            "name": "Label Add Territory Button"
          },
          {
            "attribut": '"territory_id":',
            "value": this.data.epdb.admin.country.territory.territory_id,
            "name": "Territory ID"
          },
          {
            "attribut": '"territory_code":',
            "value": this.data.epdb.admin.country.territory.territory_code,
            "name": "Territory Code"
          },
          {
            "attribut": '"territory_name":',
            "value": this.data.epdb.admin.country.territory.territory_name,
            "name": "Territory Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-country-region") {
      this.contentarr =
        [
          {
            "attribut": '"region":',
            "value": this.data.epdb.admin.country.region.region,
            "name": "Region"
          },
          {
            "attribut": '"region_list":',
            "value": this.data.epdb.admin.country.region.region_list,
            "name": "Title in List Region"
          },
          {
            "attribut": '"add_new_region":',
            "value": this.data.epdb.admin.country.region.add_new_region,
            "name": "Title in Add New Region"
          },
          {
            "attribut": '"edit_region":',
            "value": this.data.epdb.admin.country.region.edit_region,
            "name": "Title in Edit Region"
          },
          {
            "attribut": '"btn_add_region":',
            "value": this.data.epdb.admin.country.region.btn_add_region,
            "name": "Label Add Region Button"
          },
          {
            "attribut": '"region_id":',
            "value": this.data.epdb.admin.country.region.region_id,
            "name": "Region ID"
          },
          {
            "attribut": '"region_code":',
            "value": this.data.epdb.admin.country.region.region_code,
            "name": "Region Code"
          },
          {
            "attribut": '"region_name":',
            "value": this.data.epdb.admin.country.region.region_name,
            "name": "Region Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-country-sub-region") {
      this.contentarr =
        [
          {
            "attribut": '"sub_region":',
            "value": this.data.epdb.admin.country.sub_region.sub_region,
            "name": "Sub Region"
          },
          {
            "attribut": '"sub_region_list":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_list,
            "name": "Title in Sub Region List"
          },
          {
            "attribut": '"add_new_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.add_new_sub_region,
            "name": "Title in Add New Sub Region"
          },
          {
            "attribut": '"edit_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.edit_sub_region,
            "name": "Title in Edit Sub Region"
          },
          {
            "attribut": '"btn_add_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.btn_add_sub_region,
            "name": "Label Add Sub Region Button"
          },
          {
            "attribut": '"sub_region_id":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_id,
            "name": "Sub Region ID"
          },
          {
            "attribut": '"sub_region_code":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_code,
            "name": "Sub Region Code"
          },
          {
            "attribut": '"sub_region_name":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_name,
            "name": "Sub Region Name"
          }
        ]
    }*/
    /*else if (value == "epdb-admin-country-countries") {
      this.contentarr =
        [
          {
            "attribut": '"country":',
            "value": this.data.epdb.admin.country.countries.country,
            "name": "Country"
          },
          {
            "attribut": '"countries_list":',
            "value": this.data.epdb.admin.country.countries.countries_list,
            "name": "Title in List Countries"
          },
          {
            "attribut": '"add_new_country":',
            "value": this.data.epdb.admin.country.countries.add_new_country,
            "name": "Title in Add New Country"
          },
          {
            "attribut": '"edit_country":',
            "value": this.data.epdb.admin.country.countries.edit_country,
            "name": "Title in Edit Country"
          },
          {
            "attribut": '"btn_add_country":',
            "value": this.data.epdb.admin.country.countries.btn_add_country,
            "name": "Label Add Countries Button"
          },
          {
            "attribut": '"country_id":',
            "value": this.data.epdb.admin.country.countries.country_id,
            "name": "Country ID"
          },
          {
            "attribut": '"country_code":',
            "value": this.data.epdb.admin.country.countries.country_code,
            "name": "Country Code"
          },
          {
            "attribut": '"country_name":',
            "value": this.data.epdb.admin.country.countries.country_name,
            "name": "Country Name"
          },
          {
            "attribut": '"country_phone_code":',
            "value": this.data.epdb.admin.country.countries.country_phone_code,
            "name": "Country Phone Code"
          }
        ]
    }8=*/

    // sub manage org
    /*else if (value == "epdb-manager-organization-search") {
      this.contentarr =
        [
          {
            "attribut": '"search_organization":',
            "value": this.data.epdb.manage.organization.search.search_organization,
            "name": "Search Organization"
          },
          {
            "attribut": '"tab_organization":',
            "value": this.data.epdb.manage.organization.search.tab_organization,
            "name": "Tab Organization"
          },
          {
            "attribut": '"tab_site":',
            "value": this.data.epdb.manage.organization.search.tab_site,
            "name": "Tab Site"
          },
          {
            "attribut": '"tab_contact":',
            "value": this.data.epdb.manage.organization.search.tab_contact,
            "name": "Tab Contact"
          },
          {
            "attribut": '"search_result":',
            "value": this.data.epdb.manage.organization.search.search_result,
            "name": "Search Result"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-organization-detail") {
      this.contentarr =
        [
          {
            "attribut": '"detail_organization":',
            "value": this.data.epdb.manage.organization.detail.detail_organization,
            "name": "Detail Organization"
          },
          {
            "attribut": '"organization":',
            "value": this.data.epdb.manage.organization.detail.organization,
            "name": "Organization"
          },
          {
            "attribut": '"organization_id":',
            "value": this.data.epdb.manage.organization.detail.organization_id,
            "name": "Organization ID"
          },
          {
            "attribut": '"organization_name":',
            "value": this.data.epdb.manage.organization.detail.organization_name,
            "name": "Organization Name"
          },
          {
            "attribut": '"commercial_name":',
            "value": this.data.epdb.manage.organization.detail.commercial_name,
            "name": "Commercial Name"
          },
          {
            "attribut": '"geo_region":',
            "value": this.data.epdb.manage.organization.detail.geo_region,
            "name": "Geo / Region"
          },
          {
            "attribut": '"email_1":',
            "value": this.data.epdb.manage.organization.detail.email_1,
            "name": "Primary Email"
          },
          {
            "attribut": '"email_2":',
            "value": this.data.epdb.manage.organization.detail.email_2,
            "name": "Secondary Email"
          },
          {
            "attribut": '"fax":',
            "value": this.data.epdb.manage.organization.detail.fax,
            "name": "Fax"
          },
          {
            "attribut": '"organization_web":',
            "value": this.data.epdb.manage.organization.detail.organization_web,
            "name": "Organization Web"
          },
          {
            "attribut": '"address_siebel":',
            "value": this.data.epdb.manage.organization.detail.address_siebel,
            "name": "Address is Siebel"
          },
          {
            "attribut": '"organization_address":',
            "value": this.data.epdb.manage.organization.detail.organization_address,
            "name": "Organization Address"
          },
          {
            "attribut": '"mailing_address":',
            "value": this.data.epdb.manage.organization.detail.mailing_address,
            "name": "Mailing Address"
          },
          {
            "attribut": '"shipping_address":',
            "value": this.data.epdb.manage.organization.detail.shipping_address,
            "name": "Shipping Address"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-organization-add-edit") {
      this.contentarr =
        [
          {
            "attribut": '"add_new_organization":',
            "value": this.data.epdb.manage.organization.add_edit.add_new_organization,
            "name": "Add New Organization"
          },
          {
            "attribut": '"edit_organization":',
            "value": this.data.epdb.manage.organization.add_edit.edit_organization,
            "name": "Add New Organization"
          },
          {
            "attribut": '"english_organization_name":',
            "value": this.data.epdb.manage.organization.add_edit.english_organization_name,
            "name": "English Organization Name"
          },
          {
            "attribut": '"commercial_organization_name":',
            "value": this.data.epdb.manage.organization.add_edit.commercial_organization_name,
            "name": "Commercial Organization Name"
          },
          // {
          //   "attribut": '"atc_global_csn":',
          //   "value": this.data.epdb.manage.organization.add_edit.atc_global_csn,
          //   "name": "CSN"
          // },
          // {
          //   "attribut": '"var_global_csn":',
          //   "value": this.data.epdb.manage.organization.add_edit.var_global_csn,
          //   "name": "Parent CSN"
          // },
          // {
          //   "attribut": '"ap_global_csn":',
          //   "value": this.data.epdb.manage.organization.add_edit.ap_global_csn,
          //   "name": "Ultimate CSN"
          // },
          {
            "attribut": '"location_csn":',
            "value": this.data.epdb.manage.site.add_edit.location_csn,
            "name": "CSN"
          },
          {
            "attribut": '"parent_csn":',
            "value": this.data.epdb.manage.site.add_edit.parent_csn,
            "name": "Parent CSN"
          },
          {
            "attribut": '"ultimate_parent_csn":',
            "value": this.data.epdb.manage.site.add_edit.ultimate_parent_csn,
            "name": "Ultimate CSN"
          },
          {
            "attribut": '"tax_exempt":',
            "value": this.data.epdb.manage.organization.add_edit.tax_exempt,
            "name": "Tax Exempt?"
          },
          {
            "attribut": '"vat_number":',
            "value": this.data.epdb.manage.organization.add_edit.vat_number,
            "name": "VAT Number"
          },
          {
            "attribut": '"year_joined":',
            "value": this.data.epdb.manage.organization.add_edit.year_joined,
            "name": "Year Joined"
          },
          {
            "attribut": '"organization_web_address":',
            "value": this.data.epdb.manage.organization.add_edit.organization_web_address,
            "name": "Organization Web Address"
          },
          {
            "attribut": '"registered_address":',
            "value": this.data.epdb.manage.organization.add_edit.registered_address,
            "name": "Registered Address"
          },
          {
            "attribut": '"contact_address":',
            "value": this.data.epdb.manage.organization.add_edit.contact_address,
            "name": "Contact Address"
          },
          {
            "attribut": '"invoicing_address":',
            "value": this.data.epdb.manage.organization.add_edit.invoicing_address,
            "name": "Invoicing Address"
          },
          // {
          //   "attribut": '"department":',
          //   "value": this.data.epdb.manage.organization.add_edit.department,
          //   "name": "Department"
          // },
          {
            "attribut": '"atc_director":',
            "value": this.data.epdb.manage.organization.add_edit.atc_director,
            "name": "ATC Director"
          },
          {
            "attribut": '"legal_contact":',
            "value": this.data.epdb.manage.organization.add_edit.legal_contact,
            "name": "Legal Contact"
          },
          {
            "attribut": '"billing_contact":',
            "value": this.data.epdb.manage.organization.add_edit.billing_contact,
            "name": "Billing Contact"
          },
          // {
          //   "attribut": '"first_name":',
          //   "value": this.data.epdb.manage.organization.add_edit.first_name,
          //   "name": "First Name"
          // },
          {
            "attribut": '"last_name":',
            "value": this.data.epdb.manage.organization.add_edit.last_name,
            "name": "Last Name"
          },
          {
            "attribut": '"administrative_notes":',
            "value": this.data.epdb.manage.organization.add_edit.administrative_notes,
            "name": "Administrative Notes"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-organization-journal-entries") {
      this.contentarr =
        [
          {
            "attribut": '"organization_journal_entry":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.organization_journal_entry,
            "name": "Organization Journal Entries"
          },
          {
            "attribut": '"add_org_journal_entries":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.add_org_journal_entries,
            "name": "Journal Entry Type"
          },
          {
            "attribut": '"journal_entry_type":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.journal_entry_type,
            "name": "Journal Entry Type"
          },
          {
            "attribut": '"detail_organization_journal_entries":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.detail_organization_journal_entries,
            "name": "Detail Organization Journal Entries"
          },
          {
            "attribut": '"journal_id":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.journal_id,
            "name": "Journal ID"
          },
          {
            "attribut": '"parent_id":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.parent_id,
            "name": "Parent ID"
          },
          {
            "attribut": '"activity_date":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.activity_date,
            "name": "Activity Date"
          },
          {
            "attribut": '"date_last_admin":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.date_last_admin,
            "name": "Date Last Admin"
          },
          {
            "attribut": '"last_admin_by":',
            "value": this.data.epdb.manage.organization.add_org_journal_entries.last_admin_by,
            "name": "Last Admin By"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-invoice") {
      this.contentarr =
        [
          {
            "attribut": '"invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice,
            "name": "Invoice"
          },
          {
            "attribut": '"pay_receive_date":',
            "value": this.data.epdb.manage.organization.add_invoice.pay_receive_date,
            "name": "Payment Received Date"
          },
          {
            "attribut": '"amount_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.amount_invoice,
            "name": "Amount Invoiced"
          },
          {
            "attribut": '"amount_invoice_usd":',
            "value": this.data.epdb.manage.organization.add_invoice.amount_invoice_usd,
            "name": "Amount Invoiced in USD"
          },
          {
            "attribut": '"add_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.add_invoice,
            "name": "Title in Add Invoice"
          },
          {
            "attribut": '"detail_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.detail_invoice,
            "name": "Detail Invoice"
          },
          {
            "attribut": '"edit_invoice":',
            "value": this.data.epdb.manage.organization.add_invoice.edit_invoice,
            "name": "Edit Invoice"
          },
          {
            "attribut": '"invoice_id":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_id,
            "name": "Invoice ID"
          },
          {
            "attribut": '"invoice_currency":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_currency,
            "name": "Invoice Currency"
          },
          {
            "attribut": '"invoice_number":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_number,
            "name": "Invoice Number"
          },
          {
            "attribut": '"po_receive_date":',
            "value": this.data.epdb.manage.organization.add_invoice.po_receive_date,
            "name": "P.O. Receive Date"
          },
          {
            "attribut": '"financial_year":',
            "value": this.data.epdb.manage.organization.add_invoice.financial_year,
            "name": "Financial Year"
          },
          {
            "attribut": '"invoice_description":',
            "value": this.data.epdb.manage.organization.add_invoice.invoice_description,
            "name": "Invoice Decription"
          },
          {
            "attribut": '"license":',
            "value": this.data.epdb.manage.organization.add_invoice.license,
            "name": "License"
          },
          {
            "attribut": '"total_amount":',
            "value": this.data.epdb.manage.organization.add_invoice.total_amount,
            "name": "Total Amount"
          },
          {
            "attribut": '"total_amount_usd":',
            "value": this.data.epdb.manage.organization.add_invoice.total_amount_usd,
            "name": "Total Amount in USD"
          }
        ]
    }*/

    // sub manage site
    /*else if (value == "epdb-manager-site-search") {
      this.contentarr =
        [
          {
            "attribut": '"search_site":',
            "value": this.data.epdb.manage.site.search.search_site,
            "name": "Search Site"
          },
          {
            "attribut": '"sap_csn":',
            "value": this.data.epdb.manage.site.search.sap_csn,
            "name": "SAP / CSN"
          },
          {
            "attribut": '"sap_id":',
            "value": this.data.epdb.manage.site.search.sap_id,
            "name": "SAP ID"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-detail") {
      this.contentarr =
        [
          {
            "attribut": '"detail_site":',
            "value": this.data.epdb.manage.site.detail.detail_site,
            "name": "Detail Site"
          },
          {
            "attribut": '"site":',
            "value": this.data.epdb.manage.site.detail.site,
            "name": "Site"
          },
          {
            "attribut": '"site_id":',
            "value": this.data.epdb.manage.site.detail.site_id,
            "name": "Site ID"
          },
          {
            "attribut": '"site_name":',
            "value": this.data.epdb.manage.site.detail.site_name,
            "name": "Site Name"
          },
          {
            "attribut": '"site_status":',
            "value": this.data.epdb.manage.site.detail.site_status,
            "name": "Site Status"
          },
          {
            "attribut": '"primary_site":',
            "value": this.data.epdb.manage.site.detail.primary_site,
            "name": "Primary Site"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-add-edit") {
      this.contentarr =
        [
          {
            "attribut": '"add_new_site":',
            "value": this.data.epdb.manage.site.add_edit.add_new_site,
            "name": "Add New Site"
          },
          {
            "attribut": '"edit_site":',
            "value": this.data.epdb.manage.site.add_edit.edit_site,
            "name": "Edit Site"
          },
          {
            "attribut": '"english_site_name":',
            "value": this.data.epdb.manage.site.add_edit.english_site_name,
            "name": "English Site Name"
          },
          {
            "attribut": '"commercial_site_name":',
            "value": this.data.epdb.manage.site.add_edit.commercial_site_name,
            "name": "Commercial Site Name"
          },
          {
            "attribut": '"workstation":',
            "value": this.data.epdb.manage.site.add_edit.workstation,
            "name": "Workstation"
          },
          // {
          //   "attribut": '"magellan_id":',
          //   "value": this.data.epdb.manage.site.add_edit.magellan_id,
          //   "name": "Magellan ID"
          // },
          {
            "attribut": '"site_web_address":',
            "value": this.data.epdb.manage.site.add_edit.site_web_address,
            "name": "Site Web Address"
          },
          {
            "attribut": '"site_address":',
            "value": this.data.epdb.manage.site.add_edit.site_address,
            "name": "Site Address"
          },
          {
            "attribut": '"site_manager":',
            "value": this.data.epdb.manage.site.add_edit.site_manager,
            "name": "Site Manager"
          },
          {
            "attribut": '"site_administrator":',
            "value": this.data.epdb.manage.site.add_edit.site_administrator,
            "name": "Site Administrator"
          },
          {
            "attribut": '"initial_accreditation_date":',
            "value": this.data.epdb.manage.site.add_edit.initial_accreditation_date,
            "name": "Initial Accreditation Date"
          },
          // {
          //   "attribut": '"location_csn":',
          //   "value": this.data.epdb.manage.site.add_edit.location_csn,
          //   "name": "CSN"
          // },
          // {
          //   "attribut": '"parent_csn":',
          //   "value": this.data.epdb.manage.site.add_edit.parent_csn,
          //   "name": "Parent CSN"
          // },
          // {
          //   "attribut": '"ultimate_parent_csn":',
          //   "value": this.data.epdb.manage.site.add_edit.ultimate_parent_csn,
          //   "name": "Ultimate CSN"
          // }
        ]
    }*/
    /*else if (value == "epdb-manager-site-add-partner-type") {
      this.contentarr =
        [
          {
            "attribut": '"csn":',
            "value": this.data.epdb.manage.site.add_partner_type.csn,
            "name": "CSN"
          },
          {
            "attribut": '"external_id":',
            "value": this.data.epdb.manage.site.add_partner_type.external_id,
            "name": "External ID"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-accreditation") {
      this.contentarr =
        [
          {
            "attribut": '"accreditation":',
            "value": this.data.epdb.manage.site.site_accreditation.accreditation,
            "name": "Accreditation"
          },
          {
            "attribut": '"site_accreditation":',
            "value": this.data.epdb.manage.site.site_accreditation.site_accreditation,
            "name": "Site Accreditation"
          },
          {
            "attribut": '"add_new_site_accreditation":',
            "value": this.data.epdb.manage.site.site_accreditation.add_new_site_accreditation,
            "name": "Add New Site Accreditation"
          },

          {
            "attribut": '"edit_site_accreditation":',
            "value": this.data.epdb.manage.site.site_accreditation.edit_site_accreditation,
            "name": "Edit Site Accreditation"
          },
          {
            "attribut": '"activity":',
            "value": this.data.epdb.manage.site.site_accreditation.activity,
            "name": "Activity"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-contact-qualification") {
      this.contentarr =
        [
          {
            "attribut": '"contact_qualification":',
            "value": this.data.epdb.manage.site.contact_qualification.contact_qualification,
            "name": "Contact Qualifications"
          },
          {
            "attribut": '"qualified_instructor":',
            "value": this.data.epdb.manage.site.contact_qualification.qualified_instructor,
            "name": "Qualified Instructors"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-journal-entries") {
      this.contentarr =
        [
          {
            "attribut": '"site_journal_entries":',
            "value": this.data.epdb.manage.site.site_journal_entries.site_journal_entries,
            "name": "Site Journal Entries"
          },
          {
            "attribut": '"journal_entries_type":',
            "value": this.data.epdb.manage.site.site_journal_entries.journal_entries_type,
            "name": "Journal Entries Type"
          },
          {
            "attribut": '"add_new_site_journal":',
            "value": this.data.epdb.manage.site.site_journal_entries.add_new_site_journal,
            "name": "Add New Site Journal Entries"
          },
          {
            "attribut": '"edit_site_journal":',
            "value": this.data.epdb.manage.site.site_journal_entries.edit_site_journal,
            "name": "Edit Site Journal Entries"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-academic-program") {
      this.contentarr =
        [
          {
            "attribut": '"academic_program":',
            "value": this.data.epdb.manage.site.academic_program.academic_program,
            "name": "Acadmic Program"
          },
          {
            "attribut": '"program_type":',
            "value": this.data.epdb.manage.site.academic_program.program_type,
            "name": "Program Type"
          },
          {
            "attribut": '"target_educator_fy":',
            "value": this.data.epdb.manage.site.academic_program.target_educator_fy,
            "name": "Target Educators in FY"
          },
          {
            "attribut": '"target_student_fy":',
            "value": this.data.epdb.manage.site.academic_program.target_student_fy,
            "name": "Target Students in FY"
          },
          {
            "attribut": '"target_number_institution":',
            "value": this.data.epdb.manage.site.academic_program.target_number_institution,
            "name": "Target Number of Institutions"
          },
          {
            "attribut": '"add_academic_program":',
            "value": this.data.epdb.manage.site.academic_program.add_academic_program,
            "name": "Title in Add Academic Program"
          },
          {
            "attribut": '"edit_academic_program":',
            "value": this.data.epdb.manage.site.academic_program.edit_academic_program,
            "name": "Title in Edit Academic Program"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-academic-target") {
      this.contentarr =
        [
          {
            "attribut": '"academic_target":',
            "value": this.data.epdb.manage.site.academic_target.academic_target,
            "name": "Acadmic Target"
          },
          {
            "attribut": '"target_type":',
            "value": this.data.epdb.manage.site.academic_target.target_type,
            "name": "Target Type"
          },
          {
            "attribut": '"fy_indicator":',
            "value": this.data.epdb.manage.site.academic_target.fy_indicator,
            "name": "FY Indicator"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-site-academic-project") {
      this.contentarr =
        [
          {
            "attribut": '"academic_project":',
            "value": this.data.epdb.manage.site.academic_project.academic_project,
            "name": "Acadmic Project"
          },
          {
            "attribut": '"project_type":',
            "value": this.data.epdb.manage.site.academic_project.project_type,
            "name": "Project Type"
          },
          {
            "attribut": '"completion_date":',
            "value": this.data.epdb.manage.site.academic_project.completion_date,
            "name": "Completion Date"
          },
          {
            "attribut": '"number_educator":',
            "value": this.data.epdb.manage.site.academic_project.number_educator,
            "name": "Number of Participants (Educators)"
          },
          {
            "attribut": '"number_student":',
            "value": this.data.epdb.manage.site.academic_project.number_student,
            "name": "Number of Participants (Students)"
          },
          {
            "attribut": '"add_academic_project":',
            "value": this.data.epdb.manage.site.academic_project.add_academic_project,
            "name": "Title in Add Academic Project"
          },
          {
            "attribut": '"edit_academic_project":',
            "value": this.data.epdb.manage.site.academic_project.edit_academic_project,
            "name": "Title in Edit Academic Project"
          }
        ]
    }*/

    // sub manage contact
    /*else if (value == "epdb-manager-contact-search") {
      this.contentarr =
        [
          {
            "attribut": '"search_contact":',
            "value": this.data.epdb.manage.contact.search.search_contact,
            "name": "Search Contact"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-contact-detail") {
      this.contentarr =
        [
          {
            "attribut": '"detail_contact":',
            "value": this.data.epdb.manage.contact.detail.detail_contact,
            "name": "Detail Contact"
          },
          {
            "attribut": '"contact":',
            "value": this.data.epdb.manage.contact.detail.contact,
            "name": "Contact"
          },
          {
            "attribut": '"contact_id":',
            "value": this.data.epdb.manage.contact.detail.contact_id,
            "name": "Contact ID"
          },
          {
            "attribut": '"contact_name":',
            "value": this.data.epdb.manage.contact.detail.contact_name,
            "name": "Contact Name"
          },
          {
            "attribut": '"eidm_guid":',
            "value": this.data.epdb.manage.contact.detail.eidm_guid,
            "name": "EIDM GUID"
          },
          {
            "attribut": '"title_position":',
            "value": this.data.epdb.manage.contact.detail.title_position,
            "name": "Title / Posistion"
          },
          {
            "attribut": '"mobile":',
            "value": this.data.epdb.manage.contact.detail.mobile,
            "name": "Mobile"
          },
          {
            "attribut": '"time_zone":',
            "value": this.data.epdb.manage.contact.detail.time_zone,
            "name": "Time Zone"
          },
          {
            "attribut": '"comment":',
            "value": this.data.epdb.manage.contact.detail.comment,
            "name": "Comments"
          },
          {
            "attribut": '"eidm_user_id":',
            "value": this.data.epdb.manage.contact.detail.eidm_user_id,
            "name": "EIDM User ID"
          },
          {
            "attribut": '"last_login":',
            "value": this.data.epdb.manage.contact.detail.last_login,
            "name": "Last Login"
          },
          {
            "attribut": '"last_valid":',
            "value": this.data.epdb.manage.contact.detail.last_valid,
            "name": "Last Validated"
          },
          {
            "attribut": '"primary_industry":',
            "value": this.data.epdb.manage.contact.detail.primary_industry,
            "name": "Primary Industry"
          },
          {
            "attribut": '"atc_instructor":',
            "value": this.data.epdb.manage.contact.detail.atc_instructor,
            "name": "ATC Instructor"
          },
          {
            "attribut": '"date_questionnaire_complete":',
            "value": this.data.epdb.manage.contact.detail.date_questionnaire_complete,
            "name": "Date Questionnaire Completed"
          },
          {
            "attribut": '"primary_job_role":',
            "value": this.data.epdb.manage.contact.detail.primary_job_role,
            "name": "Primary Job Role"
          },
          {
            "attribut": '"dedicated":',
            "value": this.data.epdb.manage.contact.detail.dedicated,
            "name": "Dedicated"
          },
          {
            "attribut": '"btn_affiliation":',
            "value": this.data.epdb.manage.contact.detail.btn_affiliation,
            "name": "Label Affiliations Button"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-contact-add-edit") {
      this.contentarr =
        [
          {
            "attribut": '"add_new_contact":',
            "value": this.data.epdb.manage.contact.add_edit.add_new_contact,
            "name": "Add New Contact"
          },
          {
            "attribut": '"message_add_contact":',
            "value": this.data.epdb.manage.contact.add_edit.message_add_contact,
            "name": "Message add Contact"
          },
          {
            "attribut": '"edit_contact":',
            "value": this.data.epdb.manage.contact.add_edit.edit_contact,
            "name": "Edit Comtact"
          },
          {
            "attribut": '"email_address_username":',
            "value": this.data.epdb.manage.contact.add_edit.email_address_username,
            "name": "Email Address or Username"
          },
          {
            "attribut": '"or_please_click":',
            "value": this.data.epdb.manage.contact.add_edit.or_please_click,
            "name": "Label please click here to choose"
          },
          {
            "attribut": '"btn_choose_affiliated":',
            "value": this.data.epdb.manage.contact.add_edit.btn_choose_affiliated,
            "name": "Label Choose from Affiliated Contacts Button"
          },
          {
            "attribut": '"honorific":',
            "value": this.data.epdb.manage.contact.add_edit.honorific,
            "name": "Honorific"
          },
          {
            "attribut": '"dont_send_email":',
            "value": this.data.epdb.manage.contact.add_edit.dont_send_email,
            "name": "Don't send Email"
          },
          {
            "attribut": '"autodesk_internal":',
            "value": this.data.epdb.manage.contact.add_edit.autodesk_internal,
            "name": "Autodesk Internal"
          },
          {
            "attribut": '"sentence_1":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_1,
            "name": "Sentence 1"
          },
          {
            "attribut": '"sentence_2":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_2,
            "name": "Sentence 2"
          },
          {
            "attribut": '"sentence_3":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_3,
            "name": "Sentence 3"
          },
          {
            "attribut": '"sentence_4":',
            "value": this.data.epdb.manage.contact.add_edit.sentence_4,
            "name": "Sentence 4"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-contact-site-affiliation") {
      this.contentarr =
        [
          {
            "attribut": '"site_affiliated":',
            "value": this.data.epdb.manage.contact.site_affiliated.site_affiliated,
            "name": "Sites Affiliated"
          },
          {
            "attribut": '"site_location":',
            "value": this.data.epdb.manage.contact.site_affiliated.site_location,
            "name": "Site Location"
          },
          {
            "attribut": '"p1":',
            "value": this.data.epdb.manage.contact.site_affiliated.p1,
            "name": "Sentence 1"
          },
          {
            "attribut": '"p2":',
            "value": this.data.epdb.manage.contact.site_affiliated.p2,
            "name": "Sentence 2"
          },
          {
            "attribut": '"p3":',
            "value": this.data.epdb.manage.contact.site_affiliated.p3,
            "name": "Sentence 3"
          },
          {
            "attribut": '"p4":',
            "value": this.data.epdb.manage.contact.site_affiliated.p4,
            "name": "Sentence 4"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-contact-partner-type-specific") {
      this.contentarr =
        [
          {
            "attribut": '"partner_type_spec":',
            "value": this.data.epdb.manage.contact.partner_type_spec.partner_type_spec,
            "name": "Partner Type specific Properties"
          },
          {
            "attribut": '"qualification":',
            "value": this.data.epdb.manage.contact.partner_type_spec.qualification,
            "name": "Qualifications"
          },
          {
            "attribut": '"add_qualification":',
            "value": this.data.epdb.manage.contact.partner_type_spec.add_qualification,
            "name": "Add Qualification"
          },
          {
            "attribut": '"edit_qualification":',
            "value": this.data.epdb.manage.contact.partner_type_spec.edit_qualification,
            "name": "Edit Qualification"
          },
          {
            "attribut": '"autodesk_product":',
            "value": this.data.epdb.manage.contact.partner_type_spec.autodesk_product,
            "name": "Autodesk Product"
          },
          {
            "attribut": '"contact_journal_entrie":',
            "value": this.data.epdb.manage.contact.partner_type_spec.contact_journal_entrie,
            "name": "Contact Journal Entries"
          },
          {
            "attribut": '"add_contact_journal_entries":',
            "value": this.data.epdb.manage.contact.partner_type_spec.add_contact_journal_entries,
            "name": "Add Contact Journal Entries"
          },
          {
            "attribut": '"edit_contact_journal_entries":',
            "value": this.data.epdb.manage.contact.partner_type_spec.edit_contact_journal_entries,
            "name": "Edit Contact Journal Entries"
          },
          {
            "attribut": '"attribute":',
            "value": this.data.epdb.manage.contact.partner_type_spec.attribute,
            "name": "Attribute"
          }
        ]
    }*/
    /*else if (value == "epdb-manager-contact-search-general") {
      this.contentarr =
        [
          {
            "attribut": '"general_search":',
            "value": this.data.epdb.manage.contact.search_general.general_search,
            "name": "Title in General Search"
          },
          {
            "attribut": '"search_for":',
            "value": this.data.epdb.manage.contact.search_general.search_for,
            "name": "Search For"
          },
          {
            "attribut": '"search_in":',
            "value": this.data.epdb.manage.contact.search_general.search_in,
            "name": "Search In"
          },
          {
            "attribut": '"all":',
            "value": this.data.epdb.manage.contact.search_general.all,
            "name": "All"
          },
          {
            "attribut": '"hide":',
            "value": this.data.epdb.manage.contact.search_general.hide,
            "name": "Checkbox Hide Delete / Duplicates"
          },
          {
            "attribut": '"show":',
            "value": this.data.epdb.manage.contact.search_general.show,
            "name": "Checkbox Show Delete / Duplicates"
          },
          {
            "attribut": '"show_only":',
            "value": this.data.epdb.manage.contact.search_general.show_only,
            "name": "Checkbox Show Delete / Duplicates Only"
          }
        ]
    }*/

    // Manage Course Question
    if (value == "eva-manage-course-question-atc") {
      this.contentarr =
        [
          {
            "attribut": '"question_1_atc1":',
            "value": this.data.eva.manage_course.question.atc.question_1_atc1,
            "name": "1. What level are you going to teach"
          },
          {
            "attribut": '"checkbox1_update":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_update,
            "name": "Checkbox: Update"
          },
          {
            "attribut": '"checkbox1_essential":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_essential,
            "name": "Checkbox: Essentials"
          },
          {
            "attribut": '"checkbox1_intermediate":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_intermediate,
            "name": "Checkbox: Intermediate"
          },
          {
            "attribut": '"checkbox1_advanced":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_advanced,
            "name": "Checkbox: Advanced"
          },
          {
            "attribut": '"checkbox1_custmomized":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_custmomized,
            "name": "Checkbox: Customized"
          },
          {
            "attribut": '"checkbox1_other":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_other,
            "name": "Checkbox: Other"
          },
          {
            "attribut": '"question_2a_atc2":',
            "value": this.data.eva.manage_course.question.atc.question_2a_atc2,
            "name": "2a. What is going to be the primary software used"
          },
          {
            "attribut": '"select2a_primary_product":',
            "value": this.data.eva.manage_course.question.atc.select2a_primary_product,
            "name": "Label Select: Primary Product"
          },
          {
            "attribut": '"select2a_version":',
            "value": this.data.eva.manage_course.question.atc.select2a_version,
            "name": "Label Select: Version"
          },
          {
            "attribut": '"question_2b_atc2":',
            "value": this.data.eva.manage_course.question.atc.question_2b_atc2,
            "name": "2b. What is going to be the secondary software used"
          },
          {
            "attribut": '"select2b_secondary_product":',
            "value": this.data.eva.manage_course.question.atc.select2b_secondary_product,
            "name": "Label Select: Secondary Product"
          },
          {
            "attribut": '"question_3_atc3":',
            "value": this.data.eva.manage_course.question.atc.question_3_atc3,
            "name": "3. How many hours of training will be delivered"
          },
          {
            "attribut": '"question_4_atc4":',
            "value": this.data.eva.manage_course.question.atc.question_4_atc4,
            "name": "4. What will be the training format (Check all that apply)"
          },
          {
            "attribut": '"checkbox4_classroom":',
            "value": this.data.eva.manage_course.question.atc.checkbox4_classroom,
            "name": "Checkbox: Instructor-led in the classroom"
          },
          {
            "attribut": '"checkbox4_online":',
            "value": this.data.eva.manage_course.question.atc.checkbox4_online,
            "name": "Checkbox: Online or e-learning"
          },
          {
            "attribut": '"question_5_atc5":',
            "value": this.data.eva.manage_course.question.atc.question_5_atc5,
            "name": "5. at training materials will you use in this class (Check all that apply)"
          },
          {
            "attribut": '"checkbox1_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox1_5,
            "name": "Checkbox: Autodesk Official Courseware (any Autodesk branded material)"
          },
          {
            "attribut": '"checkbox2_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox2_5,
            "name": "Autodesk Authors and Publishers (AAP) Program Courseware"
          },
          {
            "attribut": '"checkbox3_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox3_5,
            "name": "Checkbox: Course material developed by the ATC"
          },
          {
            "attribut": '"checkbox4_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox4_5,
            "name": "Checkbox: Course material developed by an independent vendor or instructor"
          },
          {
            "attribut": '"checkbox5_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox5_5,
            "name": "Checkbox: Online e-learning course material developed by an independent vendor or instructor"
          },
          {
            "attribut": '"checkbox6_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox6_5,
            "name": "Checkbox: Online e-learning course material developed by the ATC"
          },
          {
            "attribut": '"checkbox7_5":',
            "value": this.data.eva.manage_course.question.atc.checkbox7_5,
            "name": "Checkbox: Other"
          }
        ]
    }
    else if (value == "eva-manage-course-question-course") {
      this.contentarr =
        [
          {
            "attribut": '"question_4_course":',
            "value": this.data.eva.manage_course.question.course.question_4_course,
            "name": "4. What is the training type"
          }
        ]
    }
    else if (value == "eva-manage-course-question-project") {
      this.contentarr =
        [
          {
            "attribut": '"question_3_project":',
            "value": this.data.eva.manage_course.question.project.question_3_project,
            "name": "3. How many project hours will be delivered"
          },
          {
            "attribut": '"question_4_project":',
            "value": this.data.eva.manage_course.question.project.question_4_project,
            "name": "4. What is the project type"
          }
        ]
    }
    else if (value == "eva-manage-course-question-event") {
      this.contentarr =
        [
          {
            "attribut": '"question_1a_event":',
            "value": this.data.eva.manage_course.question.event.question_1a_event,
            "name": "1a. What is going to be the primary software used"
          },
          {
            "attribut": '"question_1b_event":',
            "value": this.data.eva.manage_course.question.event.question_1b_event,
            "name": "1b. What is going to be the secondary software used"
          },
          {
            "attribut": '"question_2_event":',
            "value": this.data.eva.manage_course.question.event.question_2_event,
            "name": "2. Location Name"
          },
          {
            "attribut": '"question_3_event":',
            "value": this.data.eva.manage_course.question.event.question_3_event,
            "name": "3. Learner Type"
          },
          {
            "attribut": '"question_4_event":',
            "value": this.data.eva.manage_course.question.event.question_4_event,
            "name": "4. Event Type"
          }
        ]
    }
  }

  datatmp: any;
  moduleval: string = "";
  submoduleval: string = "";
  menueval: string = "";
  submenueval: string = "";
  saveLanguage(value) {

    // let data = value.attribut + ' "' + value.value + '"';
    // let dataedit = value.attribut + ' "' + this.changevalue + '"';
    // this.service.httpClientGet(this._serviceUrl + '/update/' + this.languageselect + '/' + data + '/' + dataedit, '')
    //   .subscribe(result => {
    //     this.data = result;
    //   },
    //     error => {
    //       this.service.errorserver();
    //       this.data = '';
    //     });

    // this.changevalue = '';

    /* fix issue cant edit language in aws, mungkin url api nya kepanjangan, ganti get ke put */

    // this.changevalue = this.changevalue.replace(/\'/g, "\\\'");
    // this.changevalue = this.changevalue.replace(/\"/g, "\\\"");

    /* autodesk plan 10 oct - complete all history log */

    var dataUpdate = {
      value: value.attribut + '"' + value.value + '"',
      editvalue: value.attribut + '"' + this.changevalue + '"',
      cuid:this.useraccesdata.ContactName,
      UserId:this.useraccesdata.UserId
    }

    /* end line autodesk plan 10 oct - complete all history log */

    // console.log(this.data);
    // this.data = JSON.stringify(this.data);
    // this.data = this.data.replace(new RegExp(dataUpdate.value,"g"),dataUpdate.editvalue);
    // console.log(JSON.parse(this.data));

    // console.log(dataUpdate);
    this.service.httpCLientPut(this._serviceUrl+'/'+this.languageselect, dataUpdate)
      .subscribe(res=>{
        console.log(res)
      });

    this.changevalue = '';

    //Local storage

    var params =
      {
        modulearr: this.modulearr,
        moduleval: this.moduleval,
        submoduleval: this.submoduleval,
        menueval: this.menueval,
        submenueval: this.submenueval
      };
    localStorage.setItem("filter", JSON.stringify(params));

    setTimeout(() => {
      location.reload();
    }, 2000);

    /* end fix issue cant edit language in aws, mungkin url api nya kepanjangan, ganti get ke put */

  }

  inputchange(value, item) {
    this.itemvalue = item;
    this.changevalue = value;
  }

}
