import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupLanguageEPDBComponent } from './setup-language-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';
import { AppService } from "../../../shared/service/app.service";

export const SetupLanguageEPDBRoutes: Routes = [
  {
    path: '',
    component: SetupLanguageEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.language.setup_language',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SetupLanguageEPDBRoutes),
    SharedModule,LoadingModule
  ],
  declarations: [SetupLanguageEPDBComponent],
  providers: [AppService]
})
export class SetupLanguageEPDBModule { }
