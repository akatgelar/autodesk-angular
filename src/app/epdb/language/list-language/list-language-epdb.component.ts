import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterLanguage' })
export class DataFilterLanguagePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row => 
        // (row.countries_code.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-list-language-epdb',
  templateUrl: './list-language-epdb.component.html',
  styleUrls: [
    './list-language-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ListLanguageEPDBComponent implements OnInit {

  // private _serviceUrl = "api/Language";
  // private _serviceUrl = "api/Countries";
  private _serviceUrl = "api/Dictionaries/where/{'Parent':'Languages','Status':'A'}";
  public data:any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "KeyValue";
  public sortOrder: string = "asc";
  private loading = false;
  
  constructor(public session: SessionService, private router: Router, private _http: Http, private service: AppService) {}

  accesSetupLanguageBtn:Boolean=true;
  ngOnInit() { 
    this.loading = true;
    // translate (get lang list from api)
    var data = '';
    this.service.httpClientGet(this._serviceUrl,data).subscribe(result => { 
      if(result==null){
        swal(
          'Information!',
          'List Language Not Found.',
          'error'
        );
        this.data = '';
        this.loading = false;
      }
      else{
        this.data = result;
        this.loading = false;
      } 
    },
    error => {
      this.service.errorserver();
      this.data = '';
      this.loading = false;
    });

    this.accesSetupLanguageBtn = this.session.checkAccessButton("admin/language/setup-language");
  }
}
