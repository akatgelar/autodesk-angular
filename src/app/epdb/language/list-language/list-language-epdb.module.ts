import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListLanguageEPDBComponent } from './list-language-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterLanguagePipe } from './list-language-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const ListLanguageEPDBRoutes: Routes = [
  {
    path: '',
    component: ListLanguageEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.language.list_language',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListLanguageEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ListLanguageEPDBComponent, DataFilterLanguagePipe],
  providers: [AppService, SessionService]
})
export class ListLanguageEPDBModule { }
