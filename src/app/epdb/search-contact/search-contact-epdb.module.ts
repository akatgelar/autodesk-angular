import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchContactEPDBComponent } from './search-contact-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";

import { DataFilterContactPipe } from './search-contact-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const SearchContactEPDBRoutes: Routes = [
  {
    path: '',
    component: SearchContactEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.search.search_contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchContactEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [SearchContactEPDBComponent, DataFilterContactPipe],
  providers: [AppService, AppFilterGeo]
})
export class SearchContactEPDBModule { }
