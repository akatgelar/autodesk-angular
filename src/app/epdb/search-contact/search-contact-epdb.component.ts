import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterContact' })
export class DataFilterContactPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.ContactId.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.EmailAddress.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.countries_name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-search-contact-epdb',
    templateUrl: './search-contact-epdb.component.html',
    styleUrls: [
        './search-contact-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class SearchContactEPDBComponent implements OnInit {

    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];
    dropdownListSubPartnerType =[];
    selectedItemsSubPartnerType = [];
    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownListSubCountry = [];
    selectedItemsSubCountry = [];
    dropdownSettings = {};

    dropdownListTerritories = [];
    selectedItemsTerritories = [];

    private _serviceUrl = 'api/MainContact';
    messageError: string = '';
    public data: any = '';
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "";
    public sortOrder: string = "desc";
    public partnertype: any;
    public subpartnertype: any;
    public ContactSearch: any;
    filtercontactform: FormGroup;
    public useraccesdata: any;
    public loading = false;

    constructor(private _http: Http, private service: AppService, private filterGeo: AppFilterGeo, private router: Router, private session: SessionService) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let ContactId = new FormControl('');
        let FirstName = new FormControl('');
        let LastName = new FormControl('');
        let EmailAddress = new FormControl('');
        let PartnerType = new FormControl('');
        let SubPartnerType = new FormControl('');
        let StateProvince = new FormControl('');
        let ContactSearch = new FormControl('');

        this.filtercontactform = new FormGroup({
            ContactId: ContactId,
            FirstName: FirstName,
            LastName: LastName,
            EmailAddress: EmailAddress,
            PartnerType: PartnerType,
            SubPartnerType: SubPartnerType,
            StateProvince: StateProvince,
            ContactSearch : ContactSearch
        });
    }

    /* populate data issue role distributor */
    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true;
            }
        }
    }

    itsinstructor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "TRAINER") {
                return true;
            } else {
                return false;
            }
        }
    }

    itsDistributor(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "DISTRIBUTOR") {
                return true;
            } else {
                return false;
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    urlGetSiteId(): string {
        var sitearr = this.useraccesdata.SiteId.split(',');
        var sitenew = [];
        for (var i = 0; i < sitearr.length; i++) {
            sitenew.push('"' + sitearr[i] + '"');
        }
        return sitenew.toString();
    }
    /* populate data issue role distributor */

    territorydefault: string = "";
    getTerritory() {

        /* populate data issue role distributor */
        var url = "api/Territory/SelectAdmin";

        // if (this.checkrole()) {
        //     if (this.itsinstructor()) {
        //         url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //     } else {
        //         if (this.itsDistributor()) {
        //             url = "api/Territory/where/{'Distributor':'" + this.urlGetOrgId() + "'}";
        //         } else {
        //             url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        //         }
        //     }
        // }
        var  OrgId = null; var Distributor = null;
        if (this.checkrole()) {

            if (this.itsinstructor()) {
                OrgId = this.urlGetOrgId();
               url = "api/Territory/wherenew/'OrgId'";
            } else {
                if (this.itsDistributor()) {
                    Distributor = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'Distributor'";
                } else {
                    OrgId = this.urlGetOrgId();
                    url = "api/Territory/wherenew/'OrgId'";
                }
            }
            
        }
        var keyword : any = {
            OrgId: OrgId,
            Distributor: Distributor
           
        };
        keyword = JSON.stringify(keyword);
        /* populate data issue role distributor */

        // // this.loading = true;
        // var url = "";
        // if (!this.checkrole()) {
        //     url = "api/Territory";
        // } else {
        //     url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
        // }

        var data: any;
        this.service.httpClientPost(url, keyword)
            .subscribe((res:any) => {
                data = res.sort((a,b)=>{
                    if(a.Territory_Name > b.Territory_Name)
                        return 1
                    else if(a.Territory_Name < b.Territory_Name)
                        return -1
                    else return 0
                });
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].TerritoryId != "") {
                            this.selectedItemsTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                            this.dropdownListTerritories.push({
                                id: data[i].TerritoryId,
                                itemName: data[i].Territory_Name
                            })
                        }
                    }
                    if (this.checkrole()) {
                        var territoryarr = [];
                        for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                            territoryarr.push('"' + this.dropdownListTerritories[i].id + '"')
                        }
                        this.territorydefault = territoryarr.toString();
                    }
                }
            }, error => {
                this.service.errorserver();
            });
        this.selectedItemsTerritories = [];
    }

    allregion = [];
    allsubregion = [];
    allcountries = [];
    ngOnInit() {
        //partner type
        var data = '';
        this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
            .subscribe((result:any) => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.partnertype = null;
                }
                else {
                    this.partnertype = result.sort((a,b)=>{
                        if(a.RoleName > b.RoleName)
                            return 1
                        if(a.RoleName < b.RoleName)
                            return -1
                        else return 0
                    });
                }
            },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });


//subpartner type
  var data = '';
  this.service.httpClientGet('api/Roles/wheresub/{"RoleCode": "ATC"}', data)
      .subscribe(result => {
          if (result == "Not found") {
              this.service.notfound();
              this.subpartnertype = '';
          }
          else {
              this.subpartnertype = result
          }
      },
          error => {
              this.service.errorserver();
              this.subpartnertype = '';
          });


        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            // console.log(item);
            this.filtercontactform.patchValue({ ContactId: item.ContactId });
            this.filtercontactform.patchValue({ FirstName: item.FirstName });
            this.filtercontactform.patchValue({ LastName: item.LastName });
            this.filtercontactform.patchValue({ EmailAddress: item.EmailAddress });
            this.filtercontactform.patchValue({ PartnerType: item.Partner });
            this.selectedItemsPartnerTypeStatus = item.PartnerStatus;
            this.dropdownListPartnerTypeStatus = item.dropdownStatus;
            this.selectedItemsGeo = item.Geo;
            this.dropdownListGeo = item.dropdownGeo;
            this.selectedItemsTerritories = item.Territory;
            this.dropdownListTerritories = item.dropdownTerritory;
            this.selectedItemsCountry = item.Country;
            this.dropdownListCountry = item.dropdownCountry;
            this.selectedItemsSubPartnerType = item.subpartnertype;
            this.dropdownListSubPartnerType = item.dropdownsubpartnertype;
            this.ContactSearch = "ContactSearch";
            this.onSubmit();
        }
        else {
            this.getTerritory();

            // Partner Type Status
            var data = '';
            this.service.httpClientGet('api/Roles/Status', data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListPartnerTypeStatus = null;
                    }
                    else {
                        this.dropdownListPartnerTypeStatus = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].Key,
                          itemName: result[Index].KeyValue
                        }
                      })
 this.selectedItemsPartnerTypeStatus = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].Key,
                          itemName: result[Index].KeyValue
                        }
                      })

                    }
                },
                    error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                    });
            this.selectedItemsPartnerTypeStatus = [];

            // Geo
            // var url = "";
            // if (!this.checkrole()) {
            //     url = "api/Geo";
            // } else {
            //     url = "api/Territory/where/{'OrgId':'" + this.urlGetOrgId() + "'}";
            // }
            /* populate data issue role distributor */
            var url = "api/Geo/SelectAdmin";
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url = "api/Territory/where/{'DistributorGeo':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url = "api/Territory/where/{'OrgIdGeo':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }
            var  DistributorGeo = null; var OrgIdGeo = null;
            if (this.checkrole()) {
                if (this.itsinstructor()) {
                    OrgIdGeo = this.urlGetOrgId();
                   url = "api/Territory/wherenew/'OrgIdGeo'";
                } else {
                    if (this.itsDistributor()) {
                        DistributorGeo = this.urlGetOrgId();
                        url = "api/Territory/wherenew/'DistributorGeo'";
                    } else {
                        OrgIdGeo = this.urlGetOrgId();
                   url = "api/Territory/wherenew/'OrgIdGeo'";
                    }
                }
                
            }
            var keyword : any = {
                DistributorGeo: DistributorGeo,
                OrgIdGeo: OrgIdGeo,
               
            };
            keyword = JSON.stringify(keyword);
            var data = '';
            this.service.httpClientPost(url, keyword)
                .subscribe((result:any) => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListGeo = null;
                    }
                    else {
                        this.dropdownListGeo = result.sort((a,b)=>{
                            if(a.geo_name > b.geo_name)
                                return 1
                            else if(a.geo_name < b.geo_name)
                                return -1
                            else return 0
                        }).map(function (el) {
                            return {
                              id: el.geo_code,
                              itemName: el.geo_name
                            }
                          })
                        this.selectedItemsGeo = result.map(function (el) {
                            return {
                              id: el.geo_code,
                              itemName: el.geo_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.messageError = <any>error
                        this.service.errorserver();
                    });
            this.selectedItemsGeo = [];

            // Region
            /*data = '';
            this.service.httpClientGet("api/Region", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListRegion = null;
                    }
                    else {
                        this.dropdownListRegion = JSON.parse(result).map((item) => {
                            return {
                                id: item.region_code,
                                itemName: item.region_name
                            }
                        })
                    }
                    this.allregion = this.dropdownListRegion;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsRegion = [];*/

            // Sub Region
            /*data = '';
            this.service.httpClientGet("api/SubRegion", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = JSON.parse(result).map((item) => {
                            return {
                                id: item.subregion_code,
                                itemName: item.subregion_name
                            }
                        })
                    }
                    this.allsubregion = this.dropdownListSubRegion;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsSubRegion = [];*/

            // Countries
            /* populate data issue role distributor */
            var url2 = "api/Countries/SelectAdmin";
            // if (this.checkrole()) {
            //     if (this.itsinstructor()) {
            //         url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //     } else {
            //         if (this.itsDistributor()) {
            //             url2 = "api/Territory/where/{'DistributorCountry':'" + this.urlGetOrgId() + "'}";
            //         } else {
            //             url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            //         }
            //     }
            // }
            var  CountryOrgId = null; var DistributorCountry = null;
            if (this.checkrole()) {  
                if (this.itsinstructor()) {
                    CountryOrgId = this.urlGetOrgId();
                     url2 = "api/Territory/wherenew/'CountryOrgId'";
                } else {
                    if (this.itsDistributor()) {
                        DistributorCountry = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'DistributorCountry'";
                    } else {
                        CountryOrgId = this.urlGetOrgId();
                        url2 = "api/Territory/wherenew/'CountryOrgId'";
                    }
                }
                
            }
            var keyword : any = {
                CountryOrgId: CountryOrgId,
                DistributorCountry: DistributorCountry,
            };
            keyword = JSON.stringify(keyword);
            data = '';
            // var url2 = "";
            // if (!this.checkrole()) {
            //     url2 = "api/Countries";
            // } else {
            //     url2 = "api/Territory/where/{'CountryOrgId':'" + this.urlGetOrgId() + "'}";
            // }
            this.service.httpClientPost(url2, keyword)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                    this.allcountries = this.dropdownListCountry;
                },
                    error => {
                        this.service.errorserver();
                    });
            this.selectedItemsCountry = [];
        }

        //setting dropdown
        this.dropdownSettings = {
            singleSelection: false,
            text: "Please Select",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            badgeShowLimit: 5
        };
    }

    onItemSelect(item: any) { }
    OnItemDeSelect(item: any) { }
    onSelectAll(items: any) { }
    onDeSelectAll(items: any) { }

    arraygeoid = [];
    arrayterritory = [];


    onTerritorySelect(item: any) {
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        //Task 27/07/2018
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
        // Countries
        var data = '';

        /* populate data issue role distributor */
        //var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + this.arrayterritory.toString();
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + this.arrayterritory.toString() + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
            if (this.itsDistributor()) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            CtmpTerritory:this.arrayterritory.toString()
           
        };
        keyword = JSON.stringify(keyword);
        this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelect(item: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length != 0) {
            if (this.selectedItemsTerritories.length > 0) {
                for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        } else {
            if (this.dropdownListTerritories.length > 0) {
                for (var i = 0; i < this.dropdownListTerritories.length; i++) {
                    this.arrayterritory.push('"' + this.dropdownListTerritories[i].id + '"');
                }
                tmpTerritory = this.arrayterritory.toString()
            }
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        if (this.arrayterritory.length > 0) {
            // Countries
            var data = '';

            /* populate data issue role distributor */
           // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
           var url = "api/Countries/filterByGeoByTerritory";
            // if (this.itsDistributor()) {
            //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
            // }
            var  DistTerr = null;
            if (this.itsDistributor()) {
                DistTerr = this.urlGetOrgId();
               url = "api/Countries/DistTerr";
            }
 
        var keyword : any = {
            CtmpGeo:tmpGeo,
            CDistTerr:DistTerr,
            CtmpTerritory:tmpTerritory
           
        };
        keyword = JSON.stringify(keyword);
            this.service.httpClientPost(url, keyword) /* populate data issue role distributor */
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }

    }

    onTerritorySelectAll(items: any) {
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];

        this.arrayterritory = [];
        for (var i = 0; i < items.length; i++) {
            this.arrayterritory.push('"' + items[i].id + '"');
        }
        var tmpTerritory = this.arrayterritory.toString()


        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }
        // Countries
        var data = '';

        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistTerr/" + tmpTerritory + "/" + this.urlGetOrgId();
        // }
        var  DistTerr = null;
        if (this.itsDistributor()) {
            DistTerr = this.urlGetOrgId();
           url = "api/Countries/DistTerr";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistTerr:DistTerr,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);
        this.service.httpClientPost(url, keyword)  /* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onTerritoryDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsGeo = [];
        this.selectedItemsCountry = [];
    }

onPartnerSelect(item) {
        
     
        var url = "api/Roles/wheresub/{RoleCode:'" + item + "'}" ;
     
 // subpartner type
 var data = '';
 this.service.httpClientGet(url, data)
     .subscribe(result => {
         if (result == "Not found") {
             this.service.notfound();
             this.dropdownListSubPartnerType = null;
         }
         else {
             this.dropdownListSubPartnerType = Object.keys(result).map(function (Index) {
                 return {
                   id: result[Index].RoleId,
                   itemName: result[Index].RoleName
                 }
               })
            this.selectedItemsSubPartnerType = Object.keys(result).map(function (Index) {
                 return {
                   id: result[Index].RoleId,
                   itemName: result[Index].RoleName
                 }
               })
         }
     },
         error => {
             this.service.errorserver();
         });
 this.selectedItemsSubPartnerType = [];



    }

    onGeoSelect(item: any) {
        this.selectedItemsCountry = [];
        this.selectedItemsTerritories = [];

        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length > 0) {
            for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
            }
            tmpGeo = this.arraygeoid.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
         // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
         var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);
        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnGeoDeSelect(item: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        var tmpGeo = "''"
        this.arraygeoid = [];
        if (this.selectedItemsGeo.length != 0) {
            if (this.selectedItemsGeo.length > 0) {
                for (var i = 0; i < this.selectedItemsGeo.length; i++) {
                    this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        } else {
            if (this.dropdownListGeo.length > 0) {
                for (var i = 0; i < this.dropdownListGeo.length; i++) {
                    this.arraygeoid.push('"' + this.dropdownListGeo[i].id + '"');
                }
                tmpGeo = this.arraygeoid.toString()
            }
        }
        // console.log(tmpGeo);
        // Countries
        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        // Countries
        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);

        this.service.httpClientPost(url, keyword)/* populate data issue role distributor */
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoSelectAll(items: any) {
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
        this.arraygeoid = [];
        for (var i = 0; i < items.length; i++) {
            this.arraygeoid.push('"' + items[i].id + '"');
        }
        var tmpGeo = this.arraygeoid.toString()


        var tmpTerritory = "''"
        this.arrayterritory = [];
        if (this.selectedItemsTerritories.length > 0) {
            for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
                this.arrayterritory.push('"' + this.selectedItemsTerritories[i].id + '"');
            }
            tmpTerritory = this.arrayterritory.toString()
        }

        var data = '';
        /* populate data issue role distributor */
        // var url = "api/Countries/filterByGeoByTerritory/" + tmpGeo + "/" + tmpTerritory;
        var url = "api/Countries/filterByGeoByTerritory";
        // if (this.itsDistributor()) {
        //     url = "api/Countries/DistGeo/" + tmpGeo + "/" + this.urlGetOrgId();
        // }
        var  DistGeo = null;
        if (this.itsDistributor()) {
            DistGeo = this.urlGetOrgId();
           url = "api/Countries/DistGeo";
        }

    var keyword : any = {
        CtmpGeo:tmpGeo,
        CDistGeo:DistGeo,
        CtmpTerritory:tmpTerritory
       
    };
    keyword = JSON.stringify(keyword);
       ///* populate data issue role distributor */
       this.service.httpClientPost(url, keyword)    
       .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                    console.log(this.dropdownListCountry)
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onGeoDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;
        this.selectedItemsTerritories = [];
        this.selectedItemsCountry = [];
    }

    arrayregionid = [];

    onRegionSelect(item: any) {
        this.arrayregionid.push('"' + item.id + '"');

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnRegionDeSelect(item: any) {

        //split sub region
        let subregionArrTmp :any;
        this.service.httpClientGet("api/SubRegion/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    subregionArrTmp = null;
                }
                else {
                    subregionArrTmp = result;
                    for (var i = 0; i < subregionArrTmp.length; i++) {
                        var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
                        if (index !== -1) {
                            this.selectedItemsSubRegion.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filterregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arrayregionid.findIndex(x => x == '"' + item.id + '"');
        this.arrayregionid.splice(index, 1);
        this.selectedItemsSubRegion.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arrayregionid.length > 0) {
            // SubRegion
            var data = '';
            this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListSubRegion = null;
                    }
                    else {
                        this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].subregion_code,
                              itemName: result[Index].subregion_name
                            }
                          })
    
                    }
                },
                    error => {
                        this.service.errorserver();
                    });

            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListSubRegion = this.allsubregion;
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsSubRegion.splice(index, 1);
            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onRegionSelectAll(items: any) {
        this.arrayregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arrayregionid.push('"' + items[i].id + '"');
        }

        // SubRegion
        var data = '';
        this.service.httpClientGet("api/SubRegion/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListSubRegion = null;
                }
                else {
                    this.dropdownListSubRegion = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].subregion_code,
                          itemName: result[Index].subregion_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filterregion/" + this.arrayregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    onRegionDeSelectAll(items: any) {
        this.dropdownListSubRegion = this.allsubregion;
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry = [];
    }

    arraysubregionid = [];

    onSubRegionSelect(item: any) {
        this.arraysubregionid.push('"' + item.id + '"');

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    OnSubRegionDeSelect(item: any) {

        //split countries
        let countriesArrTmp :any;
        this.service.httpClientGet("api/Countries/filtersubregion/'" + item.id + "'", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    countriesArrTmp = null;
                }
                else {
                    countriesArrTmp = result;
                    for (var i = 0; i < countriesArrTmp.length; i++) {
                        var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
                        if (index !== -1) {
                            this.selectedItemsCountry.splice(index, 1);
                        }
                    }
                }
            },
                error => {
                    this.service.errorserver();
                });

        var index = this.arraysubregionid.findIndex(x => x == '"' + item.id + '"');
        this.arraysubregionid.splice(index, 1);
        this.selectedItemsCountry.splice(index, 1);

        if (this.arraysubregionid.length > 0) {
            // Countries
            var data = '';
            this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.dropdownListCountry = null;
                    }
                    else {
                        this.dropdownListCountry = Object.keys(result).map(function (Index) {
                            return {
                              id: result[Index].countries_code,
                              itemName: result[Index].countries_name
                            }
                          })

                    }
                },
                    error => {
                        this.service.errorserver();
                    });
        }
        else {
            this.dropdownListCountry = this.allcountries;

            this.selectedItemsCountry.splice(index, 1);
        }
    }
    onSubRegionSelectAll(items: any) {
        this.arraysubregionid = [];

        for (var i = 0; i < items.length; i++) {
            this.arraysubregionid.push('"' + items[i].id + '"');
        }

        // Countries
        var data = '';
        this.service.httpClientGet("api/Countries/filtersubregion/" + this.arraysubregionid.toString(), data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dropdownListCountry = null;
                }
                else {
                    this.dropdownListCountry = Object.keys(result).map(function (Index) {
                        return {
                          id: result[Index].countries_code,
                          itemName: result[Index].countries_name
                        }
                      })

                }
            },
                error => {
                    this.service.errorserver();
                });
    }
    onSubRegionDeSelectAll(items: any) {
        this.dropdownListCountry = this.allcountries;

        this.selectedItemsCountry = [];
    }

    onCountriesSelect(item: any) {
        if (this.selectedItemsGeo.length != 0 && this.selectedItemsTerritories.length != 0) {
            this.selectedItemsGeo = [];
            this.selectedItemsTerritories = [];
        }
    }
    OnCountriesDeSelect(item: any) { }
    onCountriesSelectAll(items: any) { }
    onCountriesDeSelectAll(items: any) { }

    onSubCountriesSelect(item: any) { }
    OnSubCountriesDeSelect(item: any) { }
    onSubCountriesSelectAll(item: any) { }
    onSubCountriesDeSelectAll(item: any) { }

    //submit form
    submitted: boolean;
    onSubmit() {
        this.loading = true;
        this.data = null;
        this.submitted = true;
        var data = '';
        var geoarr = [];
        var regionarr = [];
        var subregionarr = [];
        var countryarr = [];
        var partnertypestatus = [];
        var subpartnertype =[];
        var territoryarr = []

        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
            geoarr.push('"' + this.selectedItemsGeo[i].id + '"');
        }

        for (var i = 0; i < this.selectedItemsRegion.length; i++) {
            regionarr.push('"' + this.selectedItemsRegion[i].id + '"');
        }

        for (var i = 0; i < this.selectedItemsSubRegion.length; i++) {
            subregionarr.push('"' + this.selectedItemsSubRegion[i].id + '"');
        }

        for (var i = 0; i < this.selectedItemsCountry.length; i++) {
            countryarr.push('"' + this.selectedItemsCountry[i].id + '"');
        }

        for (var i = 0; i < this.selectedItemsPartnerTypeStatus.length; i++) {
            partnertypestatus.push('"' + this.selectedItemsPartnerTypeStatus[i].id + '"');
        }

        for (var i = 0; i < this.selectedItemsTerritories.length; i++) {
            territoryarr.push('"' + this.selectedItemsTerritories[i].id + '"');
        }
        for (var i = 0; i < this.selectedItemsSubPartnerType.length; i++) {
            subpartnertype.push("'" + this.selectedItemsSubPartnerType[i].id + "'");
        }

        var params = {
            ContactId: this.filtercontactform.value.ContactId,
            FirstName: this.filtercontactform.value.FirstName,
            LastName: this.filtercontactform.value.LastName,
            EmailAddress: this.filtercontactform.value.EmailAddress,
            Partner: this.filtercontactform.value.PartnerType,
            PartnerStatus: this.selectedItemsPartnerTypeStatus,
            dropdownStatus: this.dropdownListPartnerTypeStatus,
            subpartnertype: this.selectedItemsSubPartnerType,
            dropdownsubpartnertype: this.dropdownListSubPartnerType,
            Geo: this.selectedItemsGeo,
            dropdownGeo: this.dropdownListGeo,
            Territory: this.selectedItemsTerritories,
            dropdownTerritory: this.dropdownListTerritories,
            Country: this.selectedItemsCountry,
            dropdownCountry: this.dropdownListCountry
        }

        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));

        var dataFilter =
        {
            // UserId:this.useraccesdata.UserId,
            ContactId: this.filtercontactform.value.ContactId,
            FirstName: this.filtercontactform.value.FirstName,
            LastName: this.filtercontactform.value.LastName,
            FirstNameSpecial: this.service.encoder(this.filtercontactform.value.FirstName?this.filtercontactform.value.FirstName:''),
            LastNameSpecial: this.service.encoder(this.filtercontactform.value.LastName?this.filtercontactform.value.LastName:''),
            EmailAddress: this.filtercontactform.value.EmailAddress,
            StateProvince: this.filtercontactform.value.StateProvince,
            PartnerTypeStatus: partnertypestatus.toString(),
            PartnerType: this.filtercontactform.value.PartnerType,
            SubPartnerType: subpartnertype.toString(),
            CountryCode: countryarr.toString(),
            Geo: geoarr.toString(),
            Territory: territoryarr.toString(),
            ContactSearch : "ContactSearch"
        }

        if (this.checkrole()) {
            var orgarr = this.useraccesdata.OrgId.split(',');
            var orgnew = [];
            for (var i = 0; i < orgarr.length; i++) {
                orgnew.push("'" + orgarr[i] + "'");
            }

            var roleFilter = {};

            if (this.checkrole()) {
                if (this.itsDistributor()) {
                    roleFilter = {
                        "Distributor": this.urlGetOrgId(),
                        // "UserRoleTerritory": this.territorydefault
                    }
                } else {
                    roleFilter = {
                        "SiteAdmin": this.urlGetOrgId(),
                        "JustThisOrgId": orgnew.toString()
                    }
                }
            }

            dataFilter = Object.assign(dataFilter, roleFilter);
        }

        var data_temp : any;
        var key_temp = [];

        /* new post for autodesk plan 17 oct */
        this.service.httpClientPost(
            // this._serviceUrl + "/FilterContact",
            this._serviceUrl + "/FilterContactNew",
            // dataFilter,
            JSON.stringify(dataFilter).replace(/'/g,"\\\\'") /* issue 27092018 - handling crud with char (') inside */
        )
            .subscribe(result => {
                var finalresult = result;
                this.loading = false;
                // .replace(/\t/g, " ");
                // finalresult.replace(/\u/g, "u");
                if (finalresult == "Not found") {
                    this.service.notfound();
                    this.data = '';
                }
                else {
                    data_temp = finalresult;
                    for (let key in data_temp[0]) {
                        key_temp.push(key);
                    }
                    for (let n = 0; n < data_temp.length; n++) {
                        for (let j = 0; j < key_temp.length; j++) {
                            var kolom = key_temp[j];
                            //karena dari .net pakai serialize jadi kalau value property nya null itu bakal jadi object baru dan kalau dicetak ke frontend jadinya
                            //bakal ky gini [object Object], jadi pakai cara dibawah ini. Bagusnya diatur di query pakai IFNULL, tapi field yg ditampilin random bos jadi pararanjang
                            //cukup itu saja yg PANJANG, yg lain jangan hehe..
                            if (typeof data_temp[n][kolom] === 'object' && data_temp[n][kolom].constructor === Object) {
                                //hapus lah jek, tuman
                                delete data_temp[n][kolom];
                                data_temp[n][kolom] = "";
                            }
                        }
                    }
                    this.data = data_temp;
                }
            },
                error => {
                    this.loading = false;
                    this.service.errorserver();
                    this.data = '';
                });
        /* new post for autodesk plan 17 oct */
        // }


    }

    stop(newvalue) {
        this.data = '';
    }

    routeaffiliate(ContactIdInt, SiteId, OrganizationId) {
        //redirect
        this.router.navigate(['/manage/contact/site-affiliations', ContactIdInt, SiteId, OrganizationId], { queryParams: { link: "searchcontact" } });
    }

}
