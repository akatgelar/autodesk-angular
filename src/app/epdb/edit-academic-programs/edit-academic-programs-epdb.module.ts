import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditAcademicProgramsEPDBComponent } from './edit-academic-programs-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const EditAcademicProgramsEPDBRoutes: Routes = [
  {
    path: '',
    component: EditAcademicProgramsEPDBComponent,
    data: {
      breadcrumb: 'Edit Academic Program / Target',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditAcademicProgramsEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [EditAcademicProgramsEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class EditAcademicProgramsEPDBModule { }
