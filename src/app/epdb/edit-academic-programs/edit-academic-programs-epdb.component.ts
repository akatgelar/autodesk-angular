import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router,ActivatedRoute } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

@Component({
  selector: 'app-edit-academic-programs-epdb',
  templateUrl: './edit-academic-programs-epdb.component.html',
  styleUrls: [
    './edit-academic-programs-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class EditAcademicProgramsEPDBComponent implements OnInit {
 
  private _serviceUrl = 'api/AcademicTargetProgram';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  public datadetail:any;
  academicprogramform: FormGroup;
  public orgentriestype:any;
  public useraccesdata:any;
  public datayearcurrency:any;
  public programtype:any;
  public loading = false;
  
  constructor(private service:AppService, private route:ActivatedRoute, private router: Router, private formatdate:AppFormatDate, private session: SessionService) { 
    
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
    
    let ProgramType = new FormControl('');
    let TargetEducator = new FormControl('');
    let TargetStudent = new FormControl('');
    let TargetInstitution = new FormControl('');
    let CommentNotes = new FormControl();
    let FYIndicator = new FormControl('');
    
    this.academicprogramform = new FormGroup({
        ProgramType:ProgramType,
        TargetEducator: TargetEducator,
        TargetStudent: TargetStudent,
        TargetInstitution:TargetInstitution,
        Comment:CommentNotes,
        FYIndicator:FYIndicator
    });

  }

  id:string="";
  AcademicType:string="";
  SiteIdInt:string="";
  
  ngOnInit() {

    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
        this.AcademicType = params['AcademicType'] || '';
        this.SiteIdInt = params['SiteIdInt'] || '';
    });

    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/' + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datadetail = '';
        }
        else {
          this.datadetail = result;
          this.buildForm();
        }
      },
      error => {
        this.service.errorserver();
        this.datadetail = '';
      });

    this.service.httpClientGet('api/Currency/where/{"Parent":"FYIndicator","Status":"A"}', data)
    .subscribe(result => {
        this.datayearcurrency = result;
    },
    error => {
        this.service.errorserver();
    });

    if(this.AcademicType == "AcademicPrograms"){
      this.service.httpClientGet('api/Currency/where/{"Parent":"AcademicPrograms","Status":"A"}', data)
      .subscribe(result => {
          this.programtype = result;
      },
      error => {
          this.service.errorserver();
      });
    }else{
      this.service.httpClientGet('api/Currency/where/{"Parent":"AcademicTargets","Status":"A"}', data)
      .subscribe(result => {
          this.programtype = result;
      },
      error => {
          this.service.errorserver();
      });
    }
  }

  //submit
  onSubmit(){
    this.academicprogramform.controls['ProgramType'].markAsTouched();
    this.academicprogramform.controls['TargetEducator'].markAsTouched();
    this.academicprogramform.controls['TargetStudent'].markAsTouched();
    this.academicprogramform.controls['TargetInstitution'].markAsTouched();
    this.academicprogramform.controls['FYIndicator'].markAsTouched();
    
    if(this.academicprogramform.valid){
      this.loading = true;
      this.academicprogramform.value.LastAdminBy = this.useraccesdata.ContactName;
      if(this.AcademicType == "AcademicPrograms"){
        this.academicprogramform.value.AcademicType = "AcademicPrograms";
      }else{
        this.academicprogramform.value.AcademicType = "AcademicTargets";
      }
      
      //convert object to json
      let data = JSON.stringify(this.academicprogramform.value);

      //put action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
  
      //redirect
      this.router.navigate(['/manage/site/detail-site',this.SiteIdInt]);
      this.loading = false;
    }
  }

  buildForm(): void {
    
    let ProgramType = new FormControl(this.datadetail.Usage, Validators.required);
    let TargetEducator = new FormControl(this.datadetail.TargetEducator, Validators.required);
    let TargetStudent = new FormControl(this.datadetail.TargetStudent, Validators.required);
    let TargetInstitution = new FormControl(this.datadetail.TargetInstitution, Validators.required);
    let CommentNotes = new FormControl(this.datadetail.Comment);
    let FYIndicator = new FormControl(this.datadetail.FYIndicator);

    this.academicprogramform = new FormGroup({
      ProgramType:ProgramType,
      TargetEducator: TargetEducator,
      TargetStudent: TargetStudent,
      TargetInstitution:TargetInstitution,
      Comment:CommentNotes,
      FYIndicator:FYIndicator  
    });
  }
}
