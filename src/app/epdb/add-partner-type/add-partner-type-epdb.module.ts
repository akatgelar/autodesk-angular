import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPartnerTypeEPDBComponent } from './add-partner-type-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddPartnerTypeEPDBRoutes: Routes = [
    {
        path: '',
        component: AddPartnerTypeEPDBComponent,
        data: {
            breadcrumb: 'epdb.admin.partner_type.add_new_partner_type',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddPartnerTypeEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [AddPartnerTypeEPDBComponent],
    providers: [AppService, AppFormatDate]
})

export class AddPartnerTypeEPDBModule { }