import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';  

@Component({
    selector: 'app-add-partner-type-epdb',
    templateUrl: './add-partner-type-epdb.component.html',
    styleUrls: [
        './add-partner-type-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddPartnerTypeEPDBComponent implements OnInit {

    addpartnertypeform: FormGroup;
    public datadetail: any;
    public partnertype: any;
    id: string = '';
    public partnertypestatus: any;
    public loading = false;

    useraccesdata:any;

    constructor(public session:SessionService, private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let PartnerType = new FormControl('', Validators.required);
        let Status = new FormControl('', Validators.required);
        let CSN = new FormControl('');
        let ExternalId = new FormControl('');

        this.addpartnertypeform = new FormGroup({
            PartnerType: PartnerType,
            Status: Status,
            CSN: CSN,
            ExternalId: ExternalId
        });
    }

    ngOnInit() {
        //get data organization
        this.id = this.route.snapshot.params['id'];
        var data = '';
        this.service.httpClientGet('api/MainSite/' + this.id, data)
            .subscribe(result => {
                this.datadetail = result;
                if(this.datadetail != null){
                    this.datadetail.SiteName = this.service.decoder(this.datadetail.SiteName);
                }
                this.populatePartnerType(this.datadetail.SiteId);
            },
                error => {
                    this.service.errorserver();
                });

        //get partner type status
        this.service.httpClientGet('api/Roles/Status', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.partnertypestatus = '';
                }
                else {
                    this.partnertypestatus = result
                }
            },
                error => {
                    this.service.errorserver();
                    this.partnertypestatus = '';
                });

    }

    //get partner type
    populatePartnerType(id) {
        var data = '';
        this.service.httpClientGet('api/Roles/ListWithNot/' + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.partnertype = '';
                }
                else {
                    this.partnertype = result
                }
            },
                error => {
                    this.service.errorserver();
                    this.partnertype = '';
                });
    }

    // submit form
    public datasite:any;
    onSubmit() {
        
        this.loading = true;
        
        this.addpartnertypeform.value.SiteId = this.datadetail.SiteId;

        this.addpartnertypeform.value.cuid = this.useraccesdata.ContactName;
        this.addpartnertypeform.value.UserId = this.useraccesdata.UserId;


        let data = JSON.stringify(this.addpartnertypeform.value);

        //action input
        this.service.httpClientPost('api/SitePartnerType', data)
		 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

        setTimeout(() => {

            //get partner type
            this.service.httpClientGet("api/MainSite/where/{'SiteId': '"+this.datadetail.SiteId+"'}",data)
              .subscribe(result => { 
                if(result=="Not found"){
                    this.service.notfound();
                    this.datasite = '';
                }
                else{
                    this.datasite = result;
                    this.service.httpClientGet('api/MainSite/partnertype/' + this.datasite[0].SiteIdInt, data);
                } 
              },
              error => {
                  this.service.errorserver();
                  this.datasite = '';
              });
    
            //redirect
            this.router.navigate(['/manage/site/detail-site/', this.id]);
            this.loading = false;

        }, 1000)
    }

    //reset form
    resetForm() {
        this.addpartnertypeform.reset({
            'PartnerType': '',
            'Status': ''
        });
    }
}