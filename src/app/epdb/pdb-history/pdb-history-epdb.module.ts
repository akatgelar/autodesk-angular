import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PDBHistoryEPDBComponent } from './pdb-history-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const PDBHistoryEPDBRoutes: Routes = [
  {
    path: '',
    component: PDBHistoryEPDBComponent,
    data: {
      breadcrumb: 'PDB History',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PDBHistoryEPDBRoutes),
    SharedModule
  ],
  declarations: [PDBHistoryEPDBComponent]
})
export class PDBHistoryEPDBModule { }
