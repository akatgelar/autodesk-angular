import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from "../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';

@Pipe({ name: 'dataFilterOrganization' })
export class DataFilterOrganizationPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'dataFilterSite' })
export class DataFilterSitePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'dataFilterContact' })
export class DataFilterContactPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.EmailAddress.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.CountriesCode.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      let newValue = value.replace(/,/g, "<br/>")
      return `${newValue}`;
    }
  }
}

@Component({
  selector: 'app-search-general-epdb',
  templateUrl: './search-general-epdb.component.html',
  styleUrls: [
    './search-general-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class SearchGeneralEPDBComponent implements OnInit {

  private _serviceUrl = 'api/MainOrganization';
  public data: any = '';
  public partnertype: any;

  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";

  public rowsOnPage_site: number = 10;
  public filterQuery_site: string = "";
  public sortBy_site: string = "type";
  public sortOrder_site: string = "asc";

  public rowsOnPage_contact: number = 10;
  public filterQuery_contact: string = "";
  public sortBy_contact: string = "type";
  public sortOrder_contact: string = "asc";

  filtergeneralform: FormGroup;
  public loading = false;

  //cebok dulu jek
  public id_all_row_1 = false;
  public id_org = false;
  public id_site = false;
  public id_contact = false;

  public id_all_row_2 = false;
  public name_org = false;
  public name_site = false;
  public name_contact = false;

  public id_all_row_3 = false;
  public address_org = false;
  public address_site = false;

  public id_all_row_4 = false;
  public contacts_org = false;
  public contacts_site = false;

  public id_all_row_5 = false;
  public email_org = false;
  public email_site = false;
  public email_contact = false;

  public id_all_row_6 = false;
  public CSN_org = false;
  public CSN_site = false;

  public id_all_row_7 = false;
  public serialnumber_site = false;

  public id_all_row_8 = false;
  public invoices_org = false;

  public id_all_row_9 = false;
  public notes_org = false;
  public notes_site = false;
  public notes_contact = false;

  public id_all_col_1 = false;
  public id_all_col_2 = false;
  public id_all_col_3 = false;
  public id_all_col_4 = false;

  useraccesdata: any;

  constructor(private _http: Http, private router: Router, private service: AppService, private session: SessionService) {

    let SearchFor = new FormControl('', Validators.required);

    this.filtergeneralform = new FormGroup({
      SearchFor: SearchFor
    });

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

  }

  ngOnInit() { }

  generalArr = [];

  lemesinAjahSay(req, arr) {
    if (req == 1) {
      for (let i = 0; i < arr.length; i++) {
        this.generalArr.push(arr[i]);
      }
    } else if (req == 0) {
      for (let j = 0; j < arr.length; j++) {
        var index = this.generalArr.indexOf(arr[j])
        if (index > -1) {
          this.generalArr.splice(index, 1);
        }
      }
    }
    // console.log(this.generalArr);
  }

  //baris pertama ke bawah jek
  change_row_1(value) {
    var row_1 = ["id_org", "id_contact", "id_site"];
    if (value == true) {
      this.id_org = true;
      this.id_contact = true;
      this.id_site = true;
      this.lemesinAjahSay(1, row_1);
    } else {
      this.id_org = false;
      this.id_contact = false;
      this.id_site = false;
      this.lemesinAjahSay(0, row_1);
    }
  }

  change_row_2(value) {
    var row_2 = ["name_org", "name_contact", "name_site"];
    if (value == true) {
      this.name_org = true;
      this.name_contact = true;
      this.name_site = true;
      this.lemesinAjahSay(1, row_2);
    } else {
      this.name_org = false;
      this.name_contact = false;
      this.name_site = false;
      this.lemesinAjahSay(0, row_2);
    }
  }

  change_row_3(value) {
    var row_3 = ["address_org", "address_site"];
    if (value == true) {
      this.address_org = true;
      this.address_site = true;
      this.lemesinAjahSay(1, row_3);
    } else {
      this.address_org = false;
      this.address_site = false;
      this.lemesinAjahSay(0, row_3);
    }
  }

  change_row_4(value) {
    var row_4 = ["contacts_org", "contacts_site"];
    if (value == true) {
      this.contacts_org = true;
      this.contacts_site = true;
      this.lemesinAjahSay(1, row_4);
    } else {
      this.contacts_org = false;
      this.contacts_site = false;
      this.lemesinAjahSay(0, row_4);
    }
  }

  change_row_5(value) {
    var row_5 = ["email_org", "email_site", "email_contact"];
    if (value == true) {
      this.email_org = true;
      this.email_site = true;
      this.email_contact = true;
      this.lemesinAjahSay(1, row_5);
    } else {
      this.email_org = false;
      this.email_site = false;
      this.email_contact = false;
      this.lemesinAjahSay(0, row_5);
    }
  }

  change_row_6(value) {
    var row_6 = ["CSN_org", "CSN_site"];
    if (value == true) {
      this.CSN_org = true;
      this.CSN_site = true;
      this.lemesinAjahSay(1, row_6);
    } else {
      this.CSN_org = false;
      this.CSN_site = false;
      this.lemesinAjahSay(0, row_6);
    }
  }

  change_row_7(value) {
    var row_7 = ["serialnumber_site"];
    if (value == true) {
      this.serialnumber_site = true;
      this.lemesinAjahSay(1, row_7);
    } else {
      this.serialnumber_site = false;
      this.lemesinAjahSay(0, row_7);
    }
  }

  change_row_8(value) {
    var row_8 = ["invoices_org"];
    if (value == true) {
      this.invoices_org = true;
      this.lemesinAjahSay(1, row_8);
    } else {
      this.invoices_org = false;
      this.lemesinAjahSay(0, row_8);
    }
  }

  change_row_9(value) {
    var row_9 = ["notes_org", "notes_site", "notes_contact"];
    if (value == true) {
      this.notes_org = true;
      this.notes_site = true;
      this.notes_contact = true;
      this.lemesinAjahSay(1, row_9);
    } else {
      this.notes_org = false;
      this.notes_site = false;
      this.notes_contact = false;
      this.lemesinAjahSay(0, row_9);
    }
  }

  change_col_1(value) {
    var col_1 = ["id_org", "name_org", "address_org", "contacts_org", "email_org", "CSN_org", "invoices_org", "notes_org"];
    if (value == true) {
      this.id_org = true;
      this.name_org = true;
      this.address_org = true;
      this.contacts_org = true;
      this.email_org = true;
      this.CSN_org = true;
      this.invoices_org = true;
      this.notes_org = true;
      this.lemesinAjahSay(1, col_1);
    } else {
      this.id_org = false;
      this.name_org = false;
      this.address_org = false;
      this.contacts_org = false;
      this.email_org = false;
      this.CSN_org = false;
      this.invoices_org = false;
      this.notes_org = false;
      this.lemesinAjahSay(0, col_1);
    }
  }

  change_col_2(value) {
    var col_2 = ["id_site", "name_site", "address_site", "contacts_site", "email_site", "CSN_site", "serialnumber_site", "notes_site"];
    if (value == true) {
      this.id_site = true;
      this.name_site = true;
      this.address_site = true;
      this.contacts_site = true;
      this.email_site = true;
      this.CSN_site = true;
      this.serialnumber_site = true;
      this.notes_site = true;
      this.lemesinAjahSay(1, col_2);
    } else {
      this.id_site = false;
      this.name_site = false;
      this.address_site = false;
      this.contacts_site = false;
      this.email_site = false;
      this.CSN_site = false;
      this.serialnumber_site = false;
      this.notes_site = false;
      this.lemesinAjahSay(0, col_2);
    }
  }

  change_col_3(value) {
    var col_3 = ["id_contact", "name_contact", "email_contact", "notes_contact"];
    if (value == true) {
      this.id_contact = true;
      this.name_contact = true;
      this.email_contact = true;
      this.notes_contact = true;
      this.lemesinAjahSay(1, col_3);
    } else {
      this.id_contact = false;
      this.name_contact = false;
      this.email_contact = false;
      this.notes_contact = false;
      this.lemesinAjahSay(0, col_3);
    }
  }

  change_col_4(value) {
    var col_4 = ["id_org", "name_org", "address_org", "contacts_org", "email_org", "CSN_org", "invoices_org", "notes_org", "id_site", "name_site", "address_site", "contacts_site", "email_site", "CSN_site", "serialnumber_site", "notes_site", "id_contact", "name_contact", "email_contact", "notes_contact"];
    if (value == true) {
      this.id_org = true;
      this.name_org = true;
      this.address_org = true;
      this.contacts_org = true;
      this.email_org = true;
      this.CSN_org = true;
      this.invoices_org = true;
      this.notes_org = true;
      this.id_site = true;
      this.name_site = true;
      this.address_site = true;
      this.contacts_site = true;
      this.email_site = true;
      this.CSN_site = true;
      this.serialnumber_site = true;
      this.notes_site = true;
      this.id_contact = true;
      this.name_contact = true;
      this.email_contact = true;
      this.notes_contact = true;
      this.id_all_row_1 = true;
      this.id_all_row_2 = true;
      this.id_all_row_3 = true;
      this.id_all_row_4 = true;
      this.id_all_row_5 = true;
      this.id_all_row_6 = true;
      this.id_all_row_7 = true;
      this.id_all_row_8 = true;
      this.id_all_row_9 = true;
      this.id_all_col_1 = true;
      this.id_all_col_2 = true;
      this.id_all_col_3 = true;
      this.lemesinAjahSay(1, col_4);
    } else {
      this.id_org = false;
      this.name_org = false;
      this.address_org = false;
      this.contacts_org = false;
      this.email_org = false;
      this.CSN_org = false;
      this.invoices_org = false;
      this.notes_org = false;
      this.id_site = false;
      this.name_site = false;
      this.address_site = false;
      this.contacts_site = false;
      this.email_site = false;
      this.CSN_site = false;
      this.serialnumber_site = false;
      this.notes_site = false;
      this.id_contact = false;
      this.name_contact = false;
      this.email_contact = false;
      this.notes_contact = false;
      this.id_all_row_1 = false;
      this.id_all_row_2 = false;
      this.id_all_row_3 = false;
      this.id_all_row_4 = false;
      this.id_all_row_5 = false;
      this.id_all_row_6 = false;
      this.id_all_row_7 = false;
      this.id_all_row_8 = false;
      this.id_all_row_9 = false;
      this.id_all_col_1 = false;
      this.id_all_col_2 = false;
      this.id_all_col_3 = false;

      this.generalArr = [];
    }
  }

  selectionChange(input, value) {
    var index = this.generalArr.findIndex(x => x == value);
    input === true
      ? this.generalArr.push(value)
      : this.generalArr.splice(index, 1);
    console.log(this.generalArr);
  }

  showDataBy: String = "hidedeleted";
  radioBtn(value) {
    this.showDataBy = value;
  }

  showTable: String = "";
  public data_org: any = '';
  public data_site: any = '';
  public data_contact: any = '';
  onSubmit() {
    this.data_org = '';
    this.data_site = '';
    this.data_contact = '';
    this.filtergeneralform.controls['SearchFor'].markAsTouched();
    if (this.filtergeneralform.valid) {
      this.loading = true;
      //hapus dulu duplikat value-nya jek
      let unique = this.generalArr;
      for (let n = 0; n < this.generalArr.length; n++) {
        if (unique.indexOf(this.generalArr[n]) == -1) {
          unique.push(this.generalArr[n])
        }
      }

      this.generalArr = [];
      this.generalArr = unique;

      var searchForValue = this.filtergeneralform.value.SearchFor;
      var apiurlorg = "api/MainOrganization/whereGeneral/{";
      var indexorgurl = 0;
      var apiurlsite = "api/MainSite/whereGeneral/{";
      var indexsiteurl = 0;
      var apiurlcontact = "api/MainContact/whereGeneral/{";
      var indexcontacturl = 0;
      for (var i = 0; i < this.generalArr.length; i++) {
          if (indexorgurl < 1) {
            apiurlorg += "'" + this.generalArr[i] + "':'" + searchForValue + "'";
          } else {
            apiurlorg += ",'" + this.generalArr[i] + "':'" + searchForValue + "'";
          }
          indexorgurl++;
      }
      apiurlorg += "}";
      apiurlsite += "}";
      apiurlcontact += "}";
      var spliturlorg = apiurlorg.split("/");
      var dataorg = JSON.parse(spliturlorg[3].replace(/\'/g, "\""));

      var spliturlsite = apiurlsite.split("/");
      var datasite = JSON.parse(spliturlsite[3].replace(/\'/g, "\""));

      var spliturlcontact = apiurlcontact.split("/");
      var datacontact = JSON.parse(spliturlcontact[3].replace(/\'/g, "\""));

      var roleFilter = {};

      if (this.checkrole()) {
        if (this.itsDistributor()) {
          roleFilter = {
            "Distributor": this.urlGetOrgId()
          }
        } else {
          roleFilter = {
            "SiteAdmin": this.urlGetOrgId()
          }
        }
      }

      dataorg = Object.assign(dataorg, roleFilter);
      datasite = Object.assign(datasite, roleFilter);
      datacontact = Object.assign(datacontact, roleFilter);
      dataorg.showDataBy = this.showDataBy
      dataorg.searchText = this.filtergeneralform.value.SearchFor
      datasite.showDataBy = this.showDataBy
      datacontact.showDataBy = this.showDataBy
      this.service.httpClientPost("api/MainOrganization/FilterGeneral",dataorg).subscribe((result:any) => {
        this.loading = false;
        var finalresult = result;
        this.data_org = finalresult.Orgs?finalresult.Orgs:[];
        this.data_site = finalresult.Sites?finalresult.Sites:[];
        this.data_contact = finalresult.Contacts?finalresult.Contacts:[];
      },
        error => {
          this.loading = false;
          this.data_org = '';
          this.data_site = '';
          this.data_contact = '';
      });
    }
  }

  /* populate data issue role distributor */
  checkrole(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
        return false;
      } else {
        return true;
      }
    }
  }

  itsDistributor(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "DISTRIBUTOR") {
        return true;
      } else {
        return false;
      }
    }
  }

  urlGetOrgId(): string {
    var orgarr = this.useraccesdata.OrgId.split(',');
    var orgnew = [];
    for (var i = 0; i < orgarr.length; i++) {
      orgnew.push('"' + orgarr[i] + '"');
    }
    return orgnew.toString();
  }
  /* populate data issue role distributor */

}
