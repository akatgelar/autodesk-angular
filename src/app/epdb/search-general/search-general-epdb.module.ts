import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchGeneralEPDBComponent, DataFilterOrganizationPipe, ReplacePipe, DataFilterSitePipe, DataFilterContactPipe } from './search-general-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { LoadingModule } from 'ngx-loading';

export const SearchGeneralEPDBRoutes: Routes = [
  {
    path: '',
    component: SearchGeneralEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.search_general.general_search',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchGeneralEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [SearchGeneralEPDBComponent, DataFilterOrganizationPipe, ReplacePipe, DataFilterSitePipe, DataFilterContactPipe]
})
export class SearchGeneralEPDBModule { }
