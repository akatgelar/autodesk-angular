import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { AngularMultiSelect } from 'angular2-multiselect-dropdown'

@Pipe({ name: 'dataFilterAffiliation' })
export class DataFilterAffiliationPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Status.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteCountryCode.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        }else{
            return "No Partner Type"
        }
    }
}

@Component({
  selector: 'app-add-new-contact-epdb',
  templateUrl: './add-new-contact-epdb.component.html',
  styleUrls: [
    './add-new-contact-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})


export class AddNewContactEPDBComponent implements OnInit {
  @ViewChild('rolesDropdown') rolesDropdown:AngularMultiSelect
  private _serviceUrl = 'api/ContactAll';
  public isSelectAll=false;
  public selCounter=0;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  changepasswordform: FormGroup;
  affiliate: FormGroup;
  myprofile: FormGroup;
  email: string = "";
  siteid: string = "";
  siteidint: string = "";
  public userlevel: any;
  dropdownListOrganization = [];
  selectedItemsOrganization = [];
  dropdownSettings = {};
  public useraccesdata: any;
  public datasite: any;
  dropdownListRoleAcces = [];
  selectedItemsRoleAcces:any = [];
  dropdownSettingsRoleAcces = {};
  dropdownSettingsRoleAccesone = {};
  dropdownSettingsRoleAccestwo = {};
  public loading = false;

  // get data
  honorific;
  gender;
  primarylanguage;
  secondarylanguage;
  primaryindustry;
  // stateprovince;
  country;
  country_tel;
  firstname:string="";
  lastname:string="";
  protected dataService: RemoteData;

  constructor(private completerService: CompleterService, private _http: HttpClient, private formatdate: AppFormatDate, private http: Http, private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.email = params['email'];
      this.siteid = params['siteid'];
      this.siteidint = params['siteidint'];
      this.firstname = params['firstname'];
      this.lastname = params['lastname'];
    });

    //validation
    let FirstName = new FormControl(this.firstname, Validators.required);
    let LastName = new FormControl(this.lastname, Validators.required);
    let Salutation = new FormControl('');
    let Gender = new FormControl('');
    let PrimaryRoleId = new FormControl('', Validators.required);
    let PrimaryLanguage = new FormControl('', Validators.required);
    let SecondaryLanguage = new FormControl('');
    let City = new FormControl('');
    let CountryCode = new FormControl('');
    let EmailAddress = new FormControl(this.email, [Validators.required, Validators.email]);
    let EmailAddress2 = new FormControl('');
    let Telephone1 = new FormControl('');
    let Mobile1 = new FormControl('');
    let DoNotEmail = new FormControl('');
    let MobileCode = new FormControl('');
    let TelephoneCode = new FormControl('');
    let ATCRole = new FormControl('');
    let UserLevelId = new FormControl('');
    let Designation =new FormControl('');
    //let DateBirth = new FormControl('');

    this.myprofile = new FormGroup({
      FirstName: FirstName,
      LastName: LastName,
      Salutation: Salutation,
      Gender: Gender,
      PrimaryRoleId: PrimaryRoleId,
      PrimaryLanguage: PrimaryLanguage,
      SecondaryLanguage: SecondaryLanguage,
      City: City,
      CountryCode: CountryCode,
      EmailAddress: EmailAddress,
      EmailAddress2: EmailAddress2,
      Telephone1: Telephone1,
      Mobile1: Mobile1,
      DoNotEmail: DoNotEmail,
      MobileCode: MobileCode,
      TelephoneCode: TelephoneCode,
      ATCRole: ATCRole,
      UserLevelId: UserLevelId,
      //DateBirth: DateBirth,
      Designation: Designation
    });

    this.affiliate = new FormGroup({})
  }

  jobrole: any;
  getJobRole() {
    this.service.httpClientGet('api/Roles/GetJobRole', '')
      .subscribe((result:any) => {
        if (result == "Not found") {
          this.service.notfound();
          this.jobrole = '';
        }
        else {
          this.jobrole = result.sort((a,b)=>{
            if(a.RoleName > b.RoleName)
              return 1
            else if(a.RoleName < b.RoleName)
              return -1
            else return 0
          });
        }
      },
      error => {
        this.service.errorserver();
        this.jobrole = '';
      });
  }

  /* populate data issue role distributor */
  checkrole(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
            return false;
        } else {
            return true;
        }
    }
  }

  itsinstructor(): boolean {
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
      for (var i = 0; i < userlvlarr.length; i++) {
          if (userlvlarr[i] == "TRAINER") {
              return true;
          } else {
              return false;
          }
      }
  }

  itsDistributor(): boolean {
      let userlvlarr = this.useraccesdata.UserLevelId.split(',');
      for (var i = 0; i < userlvlarr.length; i++) {
          if (userlvlarr[i] == "DISTRIBUTOR") {
              return true;
          } else {
              return false;
          }
      }
  }

  itsOrganization(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
        if (userlvlarr[i] == "ORGANIZATION") {
            return true;
        } else {
            return false;
        }
    }
}

  urlGetOrgId(): string {
      var orgarr = this.useraccesdata.OrgId.split(',');
      var orgnew = [];
      for (var i = 0; i < orgarr.length; i++) {
          orgnew.push('"' + orgarr[i] + '"');
      }
      return orgnew.toString();
  }

  urlGetSiteId(): string {
      var sitearr = this.useraccesdata.SiteId.split(',');
      var sitenew = [];
      for (var i = 0; i < sitearr.length; i++) {
          sitenew.push('"' + sitearr[i] + '"');
      }
      return sitenew.toString();
  }
  /* populate data issue role distributor */

  id: string;
  ngOnInit() {
    let options = new RequestOptions({ headers: new Headers() });
    var token = this.session.getToken();
		options.headers.set("Authorization", 'Bearer '+ token);
    this.getHonorfic();
    this.getGender();
    this.getPrimaryIndustry();
    this.getPrimaryLanguage();
    this.getSecondaryLanguage();
    this.getCountry();
    this.getCountryTel();
    this.getJobRole();
    this.checkEmailDuplicate(this.email);

    /* new request nambah user role -__ */

    // if(this.checkrole()){
    //   if(this.itsDistributor()){
    //       this.getUserLvl('api/UserLevel/WithoutStudent/Distributor');
    //   }else{
    //       this.getUserLvl('api/UserLevel/WithoutStudent/Org');
    //   }
    // }else {
    //   this.getUserLvl('api/UserLevel/WithoutStudent');
    // }

    if(this.checkrole()){
      if(this.itsDistributor()){
          this.getUserLvl('api/UserLevel/WithoutStudent/Distributor');
      }else{
        if(this.itsOrganization()){
          this.getUserLvl('api/UserLevel/WithoutStudent/Org');
        }else{
          this.getUserLvl('api/UserLevel/WithoutStudent/Site');
        }
      }
    }else {
      this.getUserLvl('api/UserLevel/WithoutStudent/null');
    }

    /* new request nambah user role -__ */

    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    var org = "";
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "ORGANIZATION" || userlvlarr[i] == "DISTRIBUTOR" || userlvlarr[i] == "SITE") {
        var orgArr = this.useraccesdata.OrgId.toString().split(',');
        var orgTmp = [];
        for (var i = 0; i < orgArr.length; i++) {
          orgTmp.push("'" + orgArr[i] + "'");
        }

        org = orgTmp.join(",");
      }else{
        org='';
      }
    }
    this.dataService = this.completerService.remote(
      null,
      "Organization",
      "Organization");
      this.dataService.requestOptions(options);
    this.dataService.urlFormater(term => {
       // return `api/MainOrganization/FindOrganization/` + this.useraccesdata.UserLevelId + `/` + org + `/` + term;
      return "api/MainOrganization/FindOrganization/{'UserLevelId':'" + this.useraccesdata.UserLevelId + "','org':'" + org + "'}"+ "/" + term;
      });
     
    this.dataService.dataField("results");
    //setting dropdown
    this.dropdownSettings = {
      singleSelection: true,
      text: "Please Select",
      enableSearchFilter: true,
      classes: "myclass custom-class",
      // disabled: false,1
      badgeShowLimit: 5,
      maxHeight: 120,
      showCheckbox: false,
      closeOnSelect: true
    };

    //setting dropdown
    this.dropdownSettingsRoleAcces = {
      singleSelection: false,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true,
      limitSelection: 1
    };
    //setting dropdown for select one
    this.dropdownSettingsRoleAccesone = {
      singleSelection: false,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true,
      limitSelection: 1
    };
    //setting dropdown for select two
    this.dropdownSettingsRoleAccestwo = {
      singleSelection: false,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true,
      limitSelection: 2
    };

  }

  OrganizationPicked:boolean=false;
  PickedOrganization(data){
    this.OrganizationPicked=true;
    var dataOrg = {
      id : data["originalObject"].OrganizationId,
      itemName : data["originalObject"].OrgName
    };
    this.onItemSelect(dataOrg);
  }

  instructorrole = [];
  getUserLvl(url) {
    let dropdownListRoleAccesTmp = [];
    this.service.httpClientGet(url, '')
      .subscribe((result:any) => {
        if (result == "Not found") {
          this.service.notfound();
          dropdownListRoleAccesTmp = null;
        }
        else {
          dropdownListRoleAccesTmp = result.sort((a,b)=>{
            if(a.Name > b.Name)
              return 1
            else if(a.Name < b.Name)
              return -1
            else return 0
          }).map(function (el) {
            return {
              id: el.UserLevelId,
              itemName: el.Name
            }
          })
        }
        for (var i = 0; i < dropdownListRoleAccesTmp.length; i++) {
          var index = this.dropdownListRoleAcces.findIndex(x => x.id == dropdownListRoleAccesTmp[i].id);
          if (index === -1) {
            this.dropdownListRoleAcces.push({
              id: dropdownListRoleAccesTmp[i].id,
              itemName: dropdownListRoleAccesTmp[i].itemName
            })
          }
        }

      },
      error => {
        this.service.errorserver();
      });
      console.log('triger')
    this.selectedItemsRoleAcces = [];
  }

  // getDataOrg(url) {
  //   this.service.get(url, '')
  //     .subscribe(result => {
  //       var finalresult = result.replace(/\t/g, " ")
  //       if (finalresult == "Not found") {
  //         this.service.notfound();
  //         this.dropdownListOrganization = null;
  //       }
  //       else {
  //         this.dropdownListOrganization = JSON.parse(finalresult).map((item) => {
  //           return {
  //             id: item.OrganizationId,
  //             itemName: item.OrgName
  //           }
  //         })
  //       }
  //     },
  //     error => {
  //       this.service.errorserver();
  //     });
  //   this.selectedItemsOrganization = [];
  // }

  onItemSelectRoleAcces(item: any) {	
	if (item.id == "TRAINER" || item.id == "Instructor Read Only") {
      if (this.isSelectAll) {
        this.selCounter = this.selCounter - 1;
      }
      this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
      this.myprofile.patchValue({ ATCRole: true });
    }
    else {
      this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
    }
	
  }
  
  OnItemDeSelectRoleAcces(item: any) { 
	if(this.isSelectAll){
      if (item.id == "TRAINER") {
        this.selCounter = this.selCounter + 1;
        this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
        if (this.selCounter == 2) {
              this.myprofile.patchValue({ ATCRole: false });
        }
		}
		if(item.id == "Instructor Read Only"){
			this.selCounter=this.selCounter+1;
			if(this.selCounter == 2){
				this.myprofile.patchValue({ ATCRole: false });
			}
		}		
    }
    else {
      if (this.selectedItemsRoleAcces[0].id == "TRAINER") {
        this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
      }
      else {
        this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
      }
      
		this.myprofile.patchValue({ ATCRole: false });
	  }
  }
  onSelectAllRoleAcces(items: any) { 
  this.isSelectAll=true;
    for(let i = 0; i < items.length; i++){
      if(items[i].id == "TRAINER" || items[i].id == "Instructor Read Only"){
        this.myprofile.patchValue({ ATCRole: true });
      }
    }
  }

  onDeSelectAllRoleAcces(items: any) { 
  this.isSelectAll=false;
    this.myprofile.patchValue({ ATCRole: false });
  }

  sitesarray = [];
  selectSite(value, sites) {
    if (value.target.checked) {
      for (var i = 0; i < this.sitesarray.length; i++) {
        if (this.sitesarray[i][0] == sites) {
          this.sitesarray[i][1] = true;
        }
      }
    }
    else {
      for (var i = 0; i < this.sitesarray.length; i++) {
        if (this.sitesarray[i][0] == sites) {
          this.sitesarray[i][1] = false;
        }
      }
    }
  }

  onItemSelect(item: any) {
    this.primarysiteidselect = false;
    var data = '';
    this.service.httpClientGet('api/MainSite/Affiliate/addnew' + '/' + item.id, data)
      .subscribe(result => {
        // var finalresult = result.replace(/\t/g, " ");
        var finalresult : any = result;
        if (finalresult == "Not found") {
          this.service.notfound();
          this.datasite = '';
        }
        else {
          this.datasite = finalresult;
          this.sitesarray = [];
          for (var i = 0; i < this.datasite.length; i++) {
            this.sitesarray.push([this.datasite[i].SiteId, true, "InsertNew"]);
            this.status.push("A");
          }
        }
      },
      error => {
        this.service.errorserver();
        this.datasite = '';
      });
  }

  getHonorfic() {
    var honorific = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Salutation','Status':'A'}", honorific)
      .subscribe(result => {
        this.honorific = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getGender() {
    var gender = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Gender','Status':'A'}", gender)
      .subscribe(result => {
        this.gender = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getPrimaryIndustry() {
    var primaryindustry = '';
    this.service.httpClientGet("api/Industries/", primaryindustry)
      .subscribe(result => {
        this.primaryindustry = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getPrimaryLanguage() {
    var primarylanguage = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", primarylanguage)
      .subscribe(result => {
        this.primarylanguage = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getSecondaryLanguage() {
    var secondarylanguage = '';
    this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", secondarylanguage)
      .subscribe(result => {
        this.secondarylanguage = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getCountry() {
    var country = '';
    this.service.httpClientGet("api/Countries/", country)
      .subscribe(result => {
        this.country = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  getCountryTel() {
    var country_tel = '';
    this.service.httpClientGet("api/Countries/order_countries_tel", country_tel)
      .subscribe(result => {
        this.country_tel = result;
      },
      error => {
        this.service.errorserver();
      });
  }

  primarysiteidselect: boolean = true;
  primarysiteid_value: string = "";
  onCheckboxChangeFn(value) {
    this.primarysiteid_value = value;
    this.primarysiteidselect = true;
  }

  status = [];
  roleaccess: boolean = true;
  affiliatesite: boolean = true;
  onSubmit() {

    this.myprofile.controls['FirstName'].markAsTouched();
    this.myprofile.controls['LastName'].markAsTouched();
    this.myprofile.controls['EmailAddress'].markAsTouched();
    this.myprofile.controls['PrimaryRoleId'].markAsTouched();
    this.myprofile.controls['PrimaryLanguage'].markAsTouched();

    // let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    // let format = /[!$%^&*+\-=\[\]{};:\\|.<>\/?]/
    // if(format.test(this.myprofile.value.FirstName))
    //   return swal('ERROR','Special character not allowed in first name','error')
    // if(format.test(this.myprofile.value.LastName))
    //   return swal('ERROR','Special character not allowed in last name','error')
    if (this.selectedItemsRoleAcces.length < 1) {
      this.roleaccess = false;
    } else {
      this.roleaccess = true;
    }

    if (this.sitesarray.length < 1) {
      this.affiliatesite = false;
    } else {
      this.affiliatesite = true;
    }

    if (this.myprofile.valid && this.validemail && this.roleaccess && this.affiliatesite && this.primarysiteidselect) {
      swal({
        title: 'Are you sure?',
        text: "If you wish to Add Contact!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;

          for (var i = 0; i < this.sitesarray.length; i++) {
            if (this.sitesarray[i][1] == true) {
              this.status[i] = "A";
            } else {
              this.status[i] = "X";
            }
          }

          if (this.myprofile.value.ATCRole) {
            this.myprofile.value.ATCRole = 'Y';
          }
          else {
            this.myprofile.value.ATCRole = 'N';
          }

          if (this.myprofile.value.DoNotEmail) {
            this.myprofile.value.DoNotEmail = 'Y';
          }
          else {
            this.myprofile.value.DoNotEmail = 'N';
          }

          if (this.myprofile.value.ShowInSearch) {
            this.myprofile.value.ShowInSearch = 1;
          }
          else {
            this.myprofile.value.ShowInSearch = 0;
          }

          if (this.primarysiteid_value != null && this.primarysiteid_value != "") {
            this.myprofile.value.PrimarySiteId = this.primarysiteid_value;
          } else {
            this.myprofile.value.PrimarySiteId = "";
          }

          if (this.sitesarray.length < 0) {
            this.myprofile.value.SiteId = "";
          }
          else {
            this.myprofile.value.SiteId = this.siteid;
          }
          this.myprofile.value.FirstName=this.service.encoder(this.myprofile.value.FirstName);
          this.myprofile.value.LastName=this.service.encoder(this.myprofile.value.LastName);
          this.myprofile.value.AddedBy = this.useraccesdata.ContactName;
          
         // this.myprofile.value.ContactName = this.myprofile.value.FirstName + " " + this.myprofile.value.LastName;
         this.myprofile.value.ContactName = this.service.encoder(this.myprofile.value.FirstName) + " " + this.service.encoder(this.myprofile.value.LastName);

          let roleacces = [];
          for (var i = 0; i < this.selectedItemsRoleAcces.length; i++) {
            roleacces.push(this.selectedItemsRoleAcces[i].id)
          }
          this.myprofile.value.UserLevelId = roleacces.join(',');

          this.myprofile.value.cuid = this.useraccesdata.ContactName;
          this.myprofile.value.UserId = this.useraccesdata.UserId;
          this.myprofile.value.Designation = this.service.encoder(this.myprofile.value.Designation);
          let data = JSON.stringify(this.myprofile.value);

          let finaldata = data.replace(/\'/g, "\\\\'");
          finaldata.replace(/"/g, '\"');
          /* autodesk plan 10 oct */

          //post action
          this.service.httpClientPost('api/Auth/ProfileEIM', finaldata).subscribe(res => {
            var result = res;
            if (result['code'] == '1') {
              if (this.sitesarray.length > 0) {
                this.affiliatethis(result['ContactId']);
              }

              if (result['emailfailed'] == 1) {
                swal({
                  title: 'Information!',
                  text: result['message'],
                  type: 'error',
                  showConfirmButton: false,
                  timer: 3000
                })
              }

              //redirect
              if (this.sitesarray.length > 0) {
                setTimeout(() => {
                  this.router.navigate(['/manage/contact/detail-contact', result['ContactId'], this.sitesarray[0][0]]);
                  this.loading = false;
                }, 1000)
              }
              else {
                setTimeout(() => {
                  this.router.navigate(['/manage/contact/detail-contact', result['ContactId'], this.siteid]);
                  this.loading = false;
                }, 1000)
              }
            }
            else {
              if (this.sitesarray.length > 0) {
                this.affiliatethis("");
              }
              this.resetForm();
              swal({
                title: 'Information!',
                text: 'Email failed to send',
                type: 'error',
                showConfirmButton: false,
                timer: 3000
              })
              if (this.sitesarray.length > 0) {
                setTimeout(() => {
                  this.router.navigate(['/manage/contact/detail-contact', this.FailEmailContactIdInt, this.sitesarray[0][0]]);
                  this.loading = false;
                }, 1000)
              }
              else {
                setTimeout(() => {
                  this.router.navigate(['/manage/contact/detail-contact', this.FailEmailContactIdInt, this.siteid]);
                  this.loading = false;
                }, 1000)
              }
              this.loading = false;
            }
          },
          error => {
            if (this.sitesarray.length > 0) {
              this.affiliatethis("");
            }
            this.resetForm();
            swal({
              title: 'Information!',
              text: 'Email failed to send',
              type: 'error',
              showConfirmButton: false,
              timer: 3000
            })
            if (this.sitesarray.length > 0) {
              setTimeout(() => {
                this.router.navigate(['/manage/contact/detail-contact', this.FailEmailContactIdInt, this.sitesarray[0][0]]);
                this.loading = false;
              }, 1000)
            }
            else {
              setTimeout(() => {
                this.router.navigate(['/manage/contact/detail-contact', this.FailEmailContactIdInt, this.siteid]);
                this.loading = false;
              }, 1000)
            }
            this.loading = false;
          });

          /* end line autodesk plan 10 oct */

        }
      }).catch(swal.noop);
    }
    else {
      /*
      swal(
        'Field is Required!',
        'Please enter the Add Contact form required!',
        'error'
      )
      */
    }
  }

  FailEmailContactIdInt: any = "";
  affiliatethis(contactid) {
    //For Audit
    this.affiliate.value.cuid = this.useraccesdata.ContactName;
    this.affiliate.value.UserId = this.useraccesdata.UserId;
    //End For Audit

    this.affiliate.value.Status = this.status;
    this.affiliate.value.AddedBy = this.useraccesdata.ContactName;
    this.affiliate.value.SiteIdArr = this.sitesarray;
    this.affiliate.value.ContactIdInt = contactid;
    
    let dataaffiliate = JSON.stringify(this.affiliate.value);

    /* autodesk plan 10 oct */

    this.service.httpClientPost('api/SiteContactLinks', dataaffiliate)
      .subscribe(res => {
        var result = res;
        this.FailEmailContactIdInt = result["ContactIdInt"];
      });

    /* end line autodesk plan 10 oct */
    
  }

  //check email registered
  validemail: Boolean = true;
  public emaildata: any;
  checkEmailDuplicate(value) {
    if (value != '' && value != null) {
      this.service.httpClientGet("api/ContactAll/" + value, '')
        .subscribe(result => {
          this.emaildata = result;
          if (this.emaildata.EmailAddress != null) {
            this.validemail = false;
          }
          else if (this.emaildata.EmailAddress == null) {
            this.validemail = true;
          }
        },
        error => {
          this.service.errorserver();
        });
    }
    else {
      this.validemail = true;
    }
  }

  resetForm() {
    this.myprofile.reset();
    this.selectedItemsOrganization = [];
    this.selectedItemsRoleAcces = [];
    this.myprofile.patchValue({ PrimaryRoleId: '' });
  }

  changeItsInstructor(value){
    if(value){
      this.dropdownListRoleAcces.map((el)=>{
        if (el.id == "TRAINER") {
          this.rolesDropdown.addSelected({
            id: el.id,
            itemName: el.itemName
          })
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
        }
      })
    } else {
      for(let j = 0; j < this.selectedItemsRoleAcces.length; j++){
        if (this.selectedItemsRoleAcces[j].id == "TRAINER") {
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccestwo;
          this.selectedItemsRoleAcces.splice(j, 1);
        }
        else {
          this.dropdownSettingsRoleAcces = this.dropdownSettingsRoleAccesone;
        }
      }
    }

  }

 checkAll(value) { 
   var chkSiteID = document.getElementsByTagName('input');

   if (value.target.checked) {
     for (var i = 0; i < this.sitesarray.length; i++) {
         this.sitesarray[i][1] = true;
     }
     for (var i = 2; i < chkSiteID.length; i++) {
       if (chkSiteID[i].type == 'checkbox') {
         chkSiteID[i].checked = true;
       }
     }
   }
   else {
     for (var i = 0; i < this.sitesarray.length; i++) {
         this.sitesarray[i][1] = false;
     }
     for (var i = 2; i < chkSiteID.length; i++) {
       if (chkSiteID[i].type == 'checkbox') {
         chkSiteID[i].checked = false;
       }
     }
   }
    
  }
}
