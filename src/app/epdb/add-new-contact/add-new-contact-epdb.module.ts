import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddNewContactEPDBComponent } from './add-new-contact-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { DataFilterAffiliationPipe,ReplacePipe } from './add-new-contact-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";

export const AddNewContactEPDBRoutes: Routes = [
  {
    path: '',
    component: AddNewContactEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.contact.add_edit.add_new_contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddNewContactEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [AddNewContactEPDBComponent, DataFilterAffiliationPipe,ReplacePipe],
  providers: [AppService, AppFormatDate, SessionService]
})
export class AddNewContactEPDBModule { }
