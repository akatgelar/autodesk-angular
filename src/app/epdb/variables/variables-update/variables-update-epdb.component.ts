import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-variables-update-epdb',
  templateUrl: './variables-update-epdb.component.html',
  styleUrls: [
    './variables-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class VariablesUpdateEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Variable';
  messageResult: string = '';
  messageError: string = '';
  public data: any;
  updatevariable: FormGroup;
  public variablecategory: any;
  private category;
  private categoryId;
  public getvariablecategory: any;
  public loading = false;
  public useraccesdata: any;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let variable_id = new FormControl();
    let variable_key = new FormControl();
    let variable_value = new FormControl();
    let variable_category = new FormControl();

    this.updatevariable = new FormGroup({
      variable_id: variable_id,
      variable_key: variable_key,
      variable_value: variable_value,
      variable_category: variable_category
    });

  }

  id: string;
  parent: string;
  ngOnInit() {

    //find data
    this.id = this.route.snapshot.params['id'];
    this.parent = this.route.snapshot.params['category'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + "/" + this.id + "/" + this.parent, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.data = result;
          this.buildForm();
        }
      },
        error => {
          this.service.errorserver();
        });

    //get data variable category
    var data = '';
    this.service.httpClientGet("api/VariableCategory", data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.variablecategory = result;
        }
      },
        error => {
          this.service.errorserver();
        });

  }

  // getdatacategory(datacategory) {
  //   var data = '';
  //   this.service.httpClientGet('api/VariableCategory/where/{"variable_category":"' + datacategory.variable_category + '"}', data)
  //     .subscribe(result => {
  //       if (result == "Not found") {
  //         this.service.notfound();
  //         this.getvariablecategory = '';
  //       }
  //       else {
  //         this.getvariablecategory = result;
  //         this.buildForm();
  //       }
  //     },
  //       error => {
  //         this.service.errorserver();
  //         this.getvariablecategory = '';
  //       });
  // }

  //build form update
  buildForm(): void {
    console.log(this.data);
    // let variable_id = new FormControl(this.data.variable_id);
    let variable_key = new FormControl(this.data.Key, Validators.required);
    let variable_value = new FormControl(this.data.KeyValue, Validators.required);
    let variable_category = new FormControl(this.data.Parent, Validators.required);
    this.updatevariable.controls['variable_category'].disable();

    this.updatevariable = new FormGroup({
      // variable_id: variable_id,
      variable_key: variable_key,
      variable_value: variable_value,
      variable_category: variable_category
    });
  }

  //submit form
  submitted: boolean;
  onSubmit() {

    this.updatevariable.controls['variable_key'].markAsTouched();
    this.updatevariable.controls['variable_value'].markAsTouched();
    this.updatevariable.controls['variable_category'].markAsTouched();
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.updatevariable.value.variable_key))
      return swal('ERROR','Special character not allowed in Variable key','error')
    if(format.test(this.updatevariable.value.variable_value))
      return swal('ERROR','Special character not allowed in Variable value','error')
    if (this.updatevariable.valid) {

      this.loading = true;
      this.updatevariable.value.muid = this.useraccesdata.ContactName; 
      this.updatevariable.value.cuid = this.useraccesdata.ContactName;
      this.updatevariable.value.UserId = this.useraccesdata.UserId;
      let data = JSON.stringify(this.updatevariable.value);

      //update action
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
      //this.service.httpCLientPut(this._serviceUrl,this.id, data);

      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/variables/variables-list'], { queryParams: { category: this.updatevariable.value.variable_category } });
        this.loading = false;
      }, 1000)
    }
  }

  resetForm() {
    this.updatevariable.reset({
      'variable_key': this.data.variable_key,
      'variable_value': this.data.variable_value,
      'variable_category': this.data.variable_category
    });
  }

  findCategory(value) {
    if (value != undefined && value != "") {
      var temp: any;
      this.service.httpClientGet("api/VariableCategory/" + value, temp)
        .subscribe(res => {
          temp = res;
          temp != null ? this.category = temp.variable_category : this.category = "";
        }, error => {
          this.service.errorserver();
        });
      this.categoryId = value;
    } else {
      this.categoryId = 0;
    }
  }
}