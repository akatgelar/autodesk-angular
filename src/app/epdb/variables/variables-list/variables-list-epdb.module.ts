import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariablesListEPDBComponent } from './variables-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterVariablePipe } from './variables-list-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { SessionService } from '../../../shared/service/session.service';

export const VariablesListEPDBRoutes: Routes = [
  {
    path: '',
    component: VariablesListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.variables.variable_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VariablesListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [VariablesListEPDBComponent, DataFilterVariablePipe],
  providers: [AppService, SessionService]
})
export class VariablesListEPDBModule { }
