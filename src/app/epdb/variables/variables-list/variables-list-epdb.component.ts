import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterVariable' })
export class DataFilterVariablePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.Key.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.KeyValue.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-variables-list-epdb',
    templateUrl: './variables-list-epdb.component.html',
    styleUrls: [
        './variables-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class VariablesListEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Variable';
    messageResult: string = '';
    messageError: string = '';
    public data: any;
    public datadetail: any;
    public variablecategory: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "KeyValue";
    public sortOrder: string = "asc";
    public categoryId = "";
    private category;
    public loading = false;
    useraccessdata: any;

    constructor(public session: SessionService, private service: AppService, private route: ActivatedRoute) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {
        // var sub: any;
        // sub = this.route.queryParams.subscribe(params => {
        //     this.categoryId = params['category'] || "";
        // });

        // if (this.categoryId != 0 && this.categoryId != undefined) {
        //     this.changetable(this.categoryId);
        // }

        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.categoryId = item.idKategori;
            this.changetable(this.categoryId);
        }


        //get data variable category
        this.loading = true;
        var data = '';
        this.service.httpClientGet("api/VariableCategory", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.loading = false;
                }
                else {
                    this.variablecategory = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });

        this.accesAddBtn = this.session.checkAccessButton("admin/variables/variables-add");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/variables/variables-update");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/variables/variables-delete");
    }

    //cange table based on combobox
    title: string = "";
    changetable(newvalue) {
        this.loading = true;
        if (newvalue != "") {
            this.title = newvalue;
            //get data action
            var data = '';
            this.service.httpClientGet(this._serviceUrl + "/where/{'Parent':'" + newvalue + "','Status':'A'}", data)
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                        this.loading = false;
                    }
                    else {
                        this.data = result;
                        this.loading = false;
                    }
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
        }
        else {
            this.title = "";
            this.data = null;
            this.loading = false;
        }

        //Buat object untuk filter yang dipilih
        var params =
            {
                idKategori: this.categoryId
            }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    }

    // //view detail
    // viewdetail(id) {
    //     var data = '';
    //     this.service.httpClientGet(this._serviceUrl + '/' + id, data)
    //         .subscribe(result => {
    //             this.datadetail = result;
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    //delete confirm
    openConfirmsSwal(id) {
        // var index = this.data.findIndex(x => x.Key == id);
        // this.service.httpClientDelete(this._serviceUrl + "/" + this.categoryId, this.data, id, index);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
                this.loading = true;
                var cuid = this.useraccessdata.ContactName;
                var UserId = this.useraccessdata.UserId;
                var urladd = this.categoryId + "/" + id
                this.service.httpClientGet(this._serviceUrl + "/SoftDelete/" + urladd + '/' + cuid + '/' + UserId, '')
                    .subscribe(result => {
                        if (result["code"] == "1") {
                            setTimeout(() => {
                                this.changetable(this.title);
                                this.loading = false;
                            }, 1000);
                        }
                        else {
                            swal(
                                'Information!',
                                "Delete Data Failed",
                                'error'
                            );
                            this.loading = false;
                        }
                    },
                        error => {
                            this.messageError = <any>error
                            this.service.errorserver();
                            console.log("cant connect server");
                            this.loading = false;
                        });
            }
        }).catch(swal.noop);
    }

}