import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../../shared/service/app.service";

@Component({
  selector: 'app-variables-detail-epdb',
  templateUrl: './variables-detail-epdb.component.html',
  styleUrls: [
    './variables-detail-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class VariablesDetailEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Variable';
  messageResult: string = '';
  messageError: string = ''; 
  public data: any;
  updatevariable: FormGroup;
  public variablecategory:any;
  private category;
  private categoryId;
  public getvariablecategory:any;
  public datadetail:any;
  
  constructor(private service:AppService, private route:ActivatedRoute, private router: Router) { 

    let variable_id = new FormControl();
    let variable_key = new FormControl();
    let variable_value = new FormControl();
    let variable_category = new FormControl();
    
    this.updatevariable = new FormGroup({
      variable_id:variable_id,
      variable_key:variable_key,
      variable_value:variable_value,
      variable_category:variable_category
    });

  }

  id:string;
  ngOnInit() {

    //find data
    this.id = this.route.snapshot.params['id'];
    var data = '';
    var data = '';
    this.service.httpClientGet(this._serviceUrl+'/'+this.id, data)
    .subscribe(result => {
        this.datadetail = result; 
    },
    error => {
        this.service.errorserver();
    });
  }

  
  

}