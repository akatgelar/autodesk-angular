import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-variables-add-epdb',
  templateUrl: './variables-add-epdb.component.html',
  styleUrls: [
    './variables-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class VariablesAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Variable';
  messageResult: string = '';
  messageError: string = '';
  addvariable: FormGroup;
  public variablecategory: any;
  private category;
  private categoryId;
  public loading = false;
  public useraccesdata: any;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let variable_key = new FormControl('', Validators.required);
    let variable_value = new FormControl('', Validators.required);
    let variable_category = new FormControl('', Validators.required);

    this.addvariable = new FormGroup({
      variable_key: variable_key,
      variable_value: variable_value,
      variable_category: variable_category
    });

  }

  ngOnInit() {

    //get data variable category
    var data = '';
    this.service.httpClientGet("api/VariableCategory", data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          this.variablecategory = result;
        }
      },
        error => {
          this.service.errorserver();
        });

  }

  //submit form
  submitted: boolean;
  onSubmit() {

    this.addvariable.controls['variable_key'].markAsTouched();
    this.addvariable.controls['variable_value'].markAsTouched();
    this.addvariable.controls['variable_category'].markAsTouched();
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.addvariable.value.variable_key))
        return swal('ERROR','Special character not allowed in Variable key','error')
    if(format.test(this.addvariable.value.variable_value))
        return swal('ERROR','Special character not allowed in Variable value','error')
    if (this.addvariable.valid) {

      this.loading = true;
      this.addvariable.value.muid = this.useraccesdata.ContactName; 
      this.addvariable.value.cuid = this.useraccesdata.ContactName;
      this.addvariable.value.UserId = this.useraccesdata.UserId;
      let data = JSON.stringify(this.addvariable.value);

      //post action
      this.service.httpClientPost(this._serviceUrl, data)
	   .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/variables/variables-list'], { queryParams: { category: this.addvariable.value.variable_category } });
        this.loading = true;
      }, 1000)
    }
  }

  resetForm() {
    this.addvariable.reset({
      'variable_category': ''
    });
  }

  // findCategory(value) {
  //   if (value != undefined && value != "") {
  //     var temp: any;
  //     this.service.httpClientGet("api/VariableCategory/" + value, temp)
  //       .subscribe(res => {
  //         temp = res;
  //         temp != null ? this.category = temp.variable_category : this.category = "";
  //       }, error => {
  //         this.service.errorserver();
  //       });
  //     this.categoryId = value;
  //   } else {
  //     this.categoryId = 0;
  //   }
  // }
}