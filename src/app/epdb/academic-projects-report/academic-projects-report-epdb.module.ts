import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcademicProjectsReportEPDBComponent } from './academic-projects-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const AcademicProjectsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: AcademicProjectsReportEPDBComponent,
    data: {
      breadcrumb: 'Academic Projects Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AcademicProjectsReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [AcademicProjectsReportEPDBComponent]
})
export class AcademicProjectsReportEPDBModule { }
