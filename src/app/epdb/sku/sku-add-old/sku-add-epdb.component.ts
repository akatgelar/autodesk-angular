import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import * as Rollbar from 'rollbar';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterSKU' })
export class DataFilterSKUPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.SKUDescription.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.PricePerLicense.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
    }
}

@Component({
    selector: 'sku-add-epdb',
    templateUrl: './sku-add-epdb.component.html',
    styleUrls: [
        './sku-add-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddSKUEPDBComponent implements OnInit {

    loading = false
    territories = []
    regions = []
    distributors = []
    addSKU = new FormGroup({
        Territory:new FormControl('',Validators.required),
        Region:new FormControl('',Validators.required),
        Country:new FormControl('',Validators.required),
        Distributor:new FormControl('',Validators.required),
        Price:new FormControl('',Validators.required),
        Status:new FormControl('',Validators.required),
        Description:new FormControl(''),
        SiteId:new FormControl('',Validators.required),
        SiteCountryDistributorSKUId: new FormControl('',Validators.required),
    })
  
    constructor(private router:ActivatedRoute, private service:AppService) { }

    ngOnInit() {
        this.getdata()
        this.getDistributor()
        this.router.params.subscribe((params)=>{
            let id = params['id']
            this.addSKU.controls['SiteCountryDistributorSKUId'].setValue(id)
        })
    }

    getdata(){
        this.service.httpClientGet('api/MainInvoices/GetListTerritory',{}).subscribe((res: any)=>{
            this.territories= res
        })
        this.service.httpClientGet('api/MainInvoices/GetListDistributor',{}).subscribe((res: any)=>{
            this.distributors= res
        })
    }

    getRegion(){
        this.service.httpClientGet('api/MainInvoices/GetListCountry?territoryId='+this.addSKU.get('Territory').value,{}).subscribe((res: any)=>{
            this.regions = res
        })
    }

    getDistributor(){
        this.service.httpClientGet('api/MainInvoices/GetListDistributor',{}).subscribe((res: any)=>{
            this.distributors = res
        })
    }

    onSubmit(){
        this.loading = true
        this.service.httpClientPost('api/MainInvoices/AddPricingSKU',this.addSKU.value).subscribe((res)=>{
            this.loading = false
        },(err)=>{
            this.loading = false
        })
    }

}