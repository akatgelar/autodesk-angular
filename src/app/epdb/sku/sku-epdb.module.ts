import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SKUEPDBComponent } from './sku-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
// import {AppFormatDate} from "../../shared/format-date/app.format-date";

import { DataFilterSKUPipe } from './sku-epdb.component';

export const SKUEPDBRoutes: Routes = [
  {
    path: '',
    component: SKUEPDBComponent,
    data: {
      breadcrumb: 'SKU',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SKUEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [SKUEPDBComponent, DataFilterSKUPipe],
  providers:[AppService,AppFilterGeo]
})

export class SKUEPDBModule { }
