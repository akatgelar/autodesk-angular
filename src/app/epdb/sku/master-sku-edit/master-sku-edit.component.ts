import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import swal from 'sweetalert2';
import {  ActivatedRoute, Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";

@Component({
  selector: 'app-master-sku-edit',
  templateUrl: './master-sku-edit.component.html',
  styleUrls: ['./master-sku-edit.component.css']
})
export class MasterSkuEditComponent implements OnInit {

  data:any
  loading = false
  id:any
  partnerTypes = []
  fys = []
  addMasterSKU = new FormGroup({
    siteCountryDistributorSKUId: new FormControl('',Validators.required),
    FYIndicatorKey:new FormControl('',Validators.required),
    PartnerType:new FormControl('',Validators.required),
    SKUName:new FormControl('',Validators.required),
    License:new FormControl('',Validators.required),
    Status:new FormControl('',Validators.required),
    IsForPrimarySite: new FormControl(false),
  })
  
  constructor(private service:AppService, private router: ActivatedRoute,) { }

  ngOnInit() {
    this.loading = true
    this.service.httpClientGet('api/MainInvoices/GetListPartnerType',{}).subscribe((res: any)=>{
      this.partnerTypes = res.sort((a,b)=>{
        if(a > b)
          return 1
        else if(a < b)
          return -1
        else return 0
      })
    })
    this.service.httpClientGet('api/MainInvoices/GetListFY',{}).subscribe((res: any)=>{
      this.fys = res
    })
    this.router.params.subscribe((params)=>{
      let id = params['id']
      this.id = id
      this.service.httpClientGet('api/MainInvoices/GetMasterSKUDetails?id='+id,{}).subscribe((res: any)=>{
        console.log(res)
        this.addMasterSKU.controls['siteCountryDistributorSKUId'].setValue(res.siteCountryDistributorSKUId)
        this.addMasterSKU.controls['FYIndicatorKey'].setValue(res.fyIndicatorKey)
        this.addMasterSKU.controls['PartnerType'].setValue(res.partnerType)
        this.addMasterSKU.controls['SKUName'].setValue(res.skuName)
        this.addMasterSKU.controls['License'].setValue(res.license)
        this.addMasterSKU.controls['Status'].setValue(res.status)
        this.addMasterSKU.controls['IsForPrimarySite'].setValue(res.isForPrimarySite)
        this.loading = false
      },(err)=>{
        this.loading = false
      })
    })
  }

  onSubmit(){
    this.loading = true
    this.service.httpClientPost('api/MainInvoices/EditMasterSKU/',this.addMasterSKU.value).subscribe((res)=>{
      this.loading = false
      //console.log(res)
      location.href = '/admin/sku/list-sku'
    },(err)=>{
      this.loading = false
      //console.log(err)
    })
  }

}
