import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterSkuEditComponent } from './master-sku-edit.component';

describe('MasterSkuEditComponent', () => {
  let component: MasterSkuEditComponent;
  let fixture: ComponentFixture<MasterSkuEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterSkuEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterSkuEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
