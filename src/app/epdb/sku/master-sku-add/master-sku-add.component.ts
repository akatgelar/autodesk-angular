import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";

@Component({
  selector: 'app-master-sku-add',
  templateUrl: './master-sku-add.component.html',
  styleUrls: ['./master-sku-add.component.css']
})
export class MasterSkuAddComponent implements OnInit {

  loading = false
  partnerTypes = []
  fys = []
  addMasterSKU = new FormGroup({
    FYIndicatorKey:new FormControl('',Validators.required),
    PartnerType:new FormControl('',Validators.required),
    SKUName:new FormControl('',Validators.required),
    License:new FormControl('',Validators.required),
    Status:new FormControl('',Validators.required),
    IsForPrimarySite: new FormControl(false),
  })
  
  constructor(private service:AppService) { }

  ngOnInit() {
    this.service.httpClientGet('api/MainInvoices/GetListPartnerType',{}).subscribe((res: any)=>{
      this.partnerTypes = res.sort((a,b)=>{
        if(a > b)
          return 1
        else if(a < b)
          return -1
        else return 0
      })
    })
    this.service.httpClientGet('api/MainInvoices/GetListFY',{}).subscribe((res: any)=>{
      this.fys = res
    })
  }

  onSubmit(){
    this.loading = true
    this.service.httpClientPost('api/MainInvoices/AddMasterSKU',this.addMasterSKU.value).subscribe((res)=>{
      //this.loading = false
      //console.log(res)
      location.href = '/admin/sku/list-sku'
    },(err)=>{
      this.loading = false
      //console.log(err)
    })
  }

}
