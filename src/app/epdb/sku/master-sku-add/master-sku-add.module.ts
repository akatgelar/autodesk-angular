import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterSkuAddComponent } from './master-sku-add.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';
import { CurrencyMaskModule } from "ng2-currency-mask";

export const MasterSkuAddRoutes: Routes = [
    {
        path: '',
        component: MasterSkuAddComponent,
        data: {
            breadcrumb: "Add / Edit Master SKU",
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MasterSkuAddRoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule,
        CurrencyMaskModule
    ],
    declarations: [MasterSkuAddComponent],
    providers: [AppService, AppFilterGeo]
})

export class MasterSkuAddModule { }
