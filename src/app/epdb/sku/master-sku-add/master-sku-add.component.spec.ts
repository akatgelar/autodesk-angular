import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterSkuAddComponent } from './master-sku-add.component';

describe('MasterSkuAddComponent', () => {
  let component: MasterSkuAddComponent;
  let fixture: ComponentFixture<MasterSkuAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterSkuAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterSkuAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
