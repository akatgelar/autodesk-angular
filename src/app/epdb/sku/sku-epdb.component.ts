import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import { AppFilterGeo } from "../../shared/filter-geo/app.filter-geo";
// import {AppFormatDate} from "../../shared/format-date/app.format-date";

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";
@Pipe({ name: 'dataFilterSKU' })
export class DataFilterSKUPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
                (row.SKUDescription.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.PricePerLicense.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
    }
}

@Component({
    selector: 'sku-epdb',
    templateUrl: './sku-epdb.component.html',
    styleUrls: [
        './sku-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css', 
    ], 
    encapsulation: ViewEncapsulation.None
})
 
export class SKUEPDBComponent implements OnInit {

    dropdownListGeo        = [];
    selectedItemsGeo       = [];
    dropdownListRegion     = [];
    selectedItemsRegion    = [];
    dropdownListSubRegion  = [];
    selectedItemsSubRegion = [];
    dropdownListCountry    = [];
    selectedItemsCountry   = [];
    dropdownSettings       = {};

    private _serviceUrl = 'api/SKU';
    messageResult: string = '';
    messageError: string = '';
    private data;
    private dataSite;
    currency;
    SKU;
    dataSKU;
    addSKU : FormGroup;
    updateSKU : FormGroup;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";

    constructor(private router: Router, private service:AppService, private filterGeo:AppFilterGeo) { 
        let SiteID              = new FormControl('');
        let CountryCode         = new FormControl('');
        let EdistributorType    = new FormControl('');
        let SKUDescription      = new FormControl('');
        let PricePerLicense     = new FormControl('');
        let Currency            = new FormControl('');
        let Status              = new FormControl('');

        this.addSKU = new FormGroup({
            SiteID:SiteID,
            CountryCode:CountryCode,
            EdistributorType:EdistributorType,
            SKUDescription:SKUDescription,
            PricePerLicense:PricePerLicense,
            Currency:Currency,
            Status:Status
        });

        let SKUDescriptionUpdate      = new FormControl();
        let PricePerLicenseUpdate     = new FormControl();
        let CurrencyUpdate            = new FormControl();

        this.updateSKU = new FormGroup({
            SKUDescription:SKUDescriptionUpdate,
            PricePerLicense:PricePerLicenseUpdate,
            Currency:CurrencyUpdate
        });
    }

    ngOnInit(){
        this.getGeo();
        this.getCurrency();
        this.getDataSKU();

        //setting dropdown
        this.dropdownSettings = { 
            singleSelection   : false, 
            text              : "Please Select",
            selectAllText     : 'Select All',
            unSelectAllText   : 'UnSelect All',
            enableSearchFilter: true,
            classes           : "myclass custom-class",
            disabled          : false
        };
    }

    getGeo(){
        var data = '';
        this.service.httpClientGet("api/Geo",data)
        .subscribe(result => { 
            this.data = result;
            this.dropdownListGeo = this.data.map((item) => {
                return {
                    id: item.geo_code,
                    itemName: item.geo_name
                }
            }) 
        },
        error => {
            this.service.errorserver();
        });
        this.selectedItemsGeo = [];
    }

    getCurrency(){
        var currency = '';
        this.service.httpClientGet(this._serviceUrl+"/Currency",currency)
        .subscribe(result => { 
            this.currency = result;
        },
        error => {
            this.service.errorserver();
        });
    }

    getDataSKU(){
        var SKU = '';
        this.service.httpClientGet(this._serviceUrl,SKU)
        .subscribe(result => { 
            this.SKU = result;
        },
        error => {
            this.service.errorserver();
        });
    }

    onGeoSelect(item:any){
        this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
        this.selectedItemsRegion    = [];
        this.dropdownListSubRegion  = [];
        this.dropdownListCountry    = [];
    }
    
    OnGeoDeSelect(item:any){
        this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
        this.selectedItemsRegion    = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry   = [];
        this.dropdownListSubRegion  = [];
        this.dropdownListCountry    = [];
    }
    
    onGeoSelectAll(items: any){
        this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo,this.dropdownListRegion);
        this.selectedItemsRegion    = [];
        this.dropdownListSubRegion  = [];
        this.dropdownListCountry    = [];
    }
    
    onGeoDeSelectAll(items: any){
        this.selectedItemsRegion    = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry   = [];
        this.dropdownListRegion     = [];
        this.dropdownListSubRegion  = [];
        this.dropdownListCountry    = [];
    }
    
    onRegionSelect(item:any){
        this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry    = [];
    }
    
    OnRegionDeSelect(item:any){
        this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry   = [];
        this.dropdownListCountry    = [];
    }
    
    onRegionSelectAll(items: any){
        this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
        this.selectedItemsSubRegion = [];
        this.dropdownListCountry    = [];
    }
    
    onRegionDeSelectAll(items: any){
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry   = [];
        this.dropdownListSubRegion  = [];
        this.dropdownListCountry    = [];
    }
    
    onSubRegionSelect(item:any){
        this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry   = [];
    }
    
    OnSubRegionDeSelect(item:any){
        this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
        this.selectedItemsCountry   = [];
    }
    
    onSubRegionSelectAll(items: any){
        this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
        this.selectedItemsCountry   = [];
    }
    
    onSubRegionDeSelectAll(items: any){
        this.selectedItemsCountry   = [];
        this.dropdownListCountry    = [];
    }
    
    onCountriesSelect(item:any){
        var dataSite = '';
        if(this.selectedItemsCountry.length != 0){
            for(let i = 0; i < this.selectedItemsCountry.length; i++){
                this.service.httpClientGet("api/MainSite/getSiteCountry/{'CountryCode':'"+this.selectedItemsCountry[i]["id"]+"'}", dataSite)
                .subscribe(result=>{
                    this.dataSite = result;
                },
                error => {
                    this.service.errorserver();
                });
            }
        }
    }
    OnCountriesDeSelect(item:any){}
    onCountriesSelectAll(item:any){}
    onCountriesDeSelectAll(item:any){}

    onSubmit(){
        this.addSKU.value.Status           = "A";
        this.addSKU.value.CountryCode      = this.selectedItemsCountry[0]["id"];
        this.addSKU.value.EdistributorType = "MED";
        let dataSKU = JSON.stringify(this.addSKU.value);
        
        this.service.httpClientPost(this._serviceUrl,dataSKU)
        .subscribe(value=>{
            var hasil = value;
            if(hasil["code"]=="1"){
                this.service.openSuccessSwal(hasil["message"]);
                this.getDataSKU();
            } else{
                swal(
                    'Information!',
                    hasil["message"],
                    'error'
                );
            }
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });
    }

    openConfirmsSwal(id, index) {
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        })
        .then(result => {  
            if (result == true) {
                var data = '';
                this.service.httpClientDelete(this._serviceUrl+"/"+id, data)
                .subscribe(value => {
                    var resource = value;
                    this.service.openSuccessSwal(resource['message']);
                    var index = this.SKU.findIndex(x => x.SiteCountryDistributorSKUId == id);
                    if (index !== -1) {
                        this.SKU.splice(index, 1);
                    }
                },
                error => {
                    this.messageError = <any>error
                    this.service.errorserver();
                });
            }
    
        }).catch(swal.noop);
    }

    getDetail(id){
        var dataSKU = '';
        this.service.httpClientGet(this._serviceUrl+'/'+id, dataSKU)
        .subscribe(result => {
            this.dataSKU = result; 
            this.buildForm();
        },
        error => {
            this.service.errorserver();
        });
    }
      
    buildForm(): void {
        let SKUDescriptionUpdate      = new FormControl(this.dataSKU.SKUDescription);
        let PricePerLicenseUpdate     = new FormControl(this.dataSKU.PricePerLicense);
        let CurrencyUpdate            = new FormControl(this.dataSKU.Currency);

        this.updateSKU = new FormGroup({
            SKUDescription:SKUDescriptionUpdate,
            PricePerLicense:PricePerLicenseUpdate,
            Currency:CurrencyUpdate
        });
    }

    onSubmitUpdate(){
        //form data
        let data = JSON.stringify(this.updateSKU.value);
        
        //put action
        this.service.httpCLientPut(this._serviceUrl+'/'+this.dataSKU.SiteCountryDistributorSKUId,data)
            .subscribe(res=>{
                console.log(res)
            });
            
        this.getDataSKU();
    }

    resetForm(){
        this.selectedItemsGeo       = [];
        this.selectedItemsRegion    = [];
        this.selectedItemsSubRegion = [];
        this.selectedItemsCountry   = [];
        this.addSKU.reset({
            'SiteID':'',
            'SKUDescription':'',
            'PricePerLicense':'',
            'Currency':''
        });
    }
}