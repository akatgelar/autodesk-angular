import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSKUEPDBComponent } from './sku-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';
import * as Rollbar from 'rollbar';
import { CurrencyMaskModule } from "ng2-currency-mask";

export const AddSKUEPDBRoutes: Routes = [
    {
        path: '',
        component: AddSKUEPDBComponent,
        data: {
            breadcrumb: 'epdb.admin.sku.add_edit_sku',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddSKUEPDBRoutes),
        SharedModule,
        AngularMultiSelectModule,
        LoadingModule,
        CurrencyMaskModule
    ],
    declarations: [AddSKUEPDBComponent],
    providers: [AppService, AppFilterGeo]
})

export class AddSKUEPDBModule { }
