import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListSKUEPDBComponent } from './sku-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterSKUPipe } from './sku-list-epdb.component';
import { DataFilterMasterSKUPipe } from './sku-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';
// import { ExcelService } from '../../../excel.service';

export const ListSKUEPDBRoutes: Routes = [
    {
        path: '',
        component: ListSKUEPDBComponent,
        data: {
            breadcrumb: 'epdb.admin.sku.list_sku',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ListSKUEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [ListSKUEPDBComponent, DataFilterSKUPipe,DataFilterMasterSKUPipe],
    // providers: [AppService, ExcelService]
    providers: [AppService, SessionService]
})

export class ListSKUEPDBModule { }
