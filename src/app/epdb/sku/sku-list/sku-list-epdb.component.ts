import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';
// import { ExcelService } from '../../../excel.service';
import * as Rollbar from 'rollbar';

@Pipe({ name: 'dataFilterSKU' })
export class DataFilterSKUPipe {
    transform(array: any[], fy: string, ter: string,reg: string): any {
        console.log(fy)
        return _.filter(array, row =>
            (fy?row.fyIndicatorKey.toLowerCase() == fy.toLowerCase():true ) &&
            (ter?row.territory.toLowerCase() == ter.toLowerCase():true ) &&
            (reg?row.region.toLowerCase() == reg.toLowerCase():true))
    }
}

@Pipe({ name: 'dataFilterMasterSKU' })
export class DataFilterMasterSKUPipe {
    transform(array: any[], query): any {
        if (query) {
            return _.filter(array, row =>
                // (row.SiteCountryDistributorSKUId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SKUDescription.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Price.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.License.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Currency.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'sku-list-epdb',
    templateUrl: './sku-list-epdb.component.html',
    styleUrls: [
        './sku-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ListSKUEPDBComponent implements OnInit {

    viewList = "master"
    private _serviceUrl = 'api/SKU';
    messageResult: string = '';
    messageError: string = '';
    territories = []
    fys = []
    regions = []
    filter = {
        fy:'',
        ter:'',
        reg:''
    }
    private data;
    private dataMaster;
    private SKU;
    public rowsOnPage: number = 10;
    public sortBy: string = "SKUDescription";
    public sortOrder: string = "asc";
    useraccessdata: any;
    public loading = false;    
    accesAddBtn:Boolean=true;
    accesUpdateBtn:Boolean=true;
    accesDeleteBtn:Boolean=true;

    constructor(public session: SessionService, private router: Router, private service: AppService, private rollbar: Rollbar) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
    }

    getSKU() {
        this.loading = true;
        var data = '';
        this.service.httpClientGet("api/MainInvoices/GetListSKU?SKUType=1", data)
            .subscribe(res => {
                this.data = res;
                if(this.data == null){
                    this.rollbar.log("error","Something went wrong when parsing SKU data!");
                    this.loading = false;
                }
                this.loading = false;
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
                this.loading = false;
            });
        this.service.httpClientGet("api/MainInvoices/GetListSKU?SKUType=0", data).subscribe((res)=>{
            this.dataMaster = res
        })
    }

    getdata(){
        this.service.httpClientGet('api/MainInvoices/GetListTerritory',{}).subscribe((res: any)=>{
            this.territories= res
        })
        this.service.httpClientGet('api/MainInvoices/GetListFY',{}).subscribe((res: any)=>{
            this.fys = res
        })
    }

    getRegion(){
        this.service.httpClientGet('api/MainInvoices/GetListCountry?territoryId='+this.filter.ter,{}).subscribe((res: any)=>{
            this.filter.reg = ''
            this.regions = res
        })
    }

    ngOnInit() {
        this.getSKU();
        this.getdata();
        this.accesAddBtn = this.session.checkAccessButton("admin/sku/add-sku");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/sku/update-sku");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/sku/delete/sku");
    }

    openConfirmsSwal(id, index) {
        this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;
        var data = '';
        // this.service.httpClientDelete(this._serviceUrl + "/" + id + '/' + cuid ,data, UserId,0 );
        // setTimeout(() => {
        //     this.getSKU();
        //     this.loading = false;                                
        // }, 1000);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then(result => {
                if (result == true) {
                    this.loading = true;
                    var cuid = this.useraccessdata.ContactName;
                    var UserId = this.useraccessdata.UserId;
                    var data = '';
                    this.service.httpClientDelete(this._serviceUrl + "/" + id + '/' + cuid + '/' + UserId, data)
                        .subscribe(value => {
                            var resource = value;
                            setTimeout(() => {
                                this.getSKU();
                                this.loading = false;                                
                            }, 1000);
                        },
                            error => {
                                this.messageError = <any>error
                                this.service.errorserver();
                                this.loading = false;
                            });
                }

            }).catch(swal.noop);
    }

    openConfirmsSwal2(id, index) {
        this.loading = true;
        var cuid = this.useraccessdata.ContactName;
        var UserId = this.useraccessdata.UserId;
        var data = '';
        // this.service.httpClientDelete(this._serviceUrl + "/" + id + '/' + cuid ,data, UserId,0 );
        // setTimeout(() => {
        //     this.getSKU();
        //     this.loading = false;                                
        // }, 1000);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then(result => {
                if (result == true) {
                    this.loading = true;
                    var cuid = this.useraccessdata.ContactName;
                    var UserId = this.useraccessdata.UserId;
                    var data = '';
                    this.service.httpClientDelete(this._serviceUrl + "/" + id + '/' + cuid + '/' + UserId, data)
                        .subscribe(value => {
                            var resource = value;
                            setTimeout(() => {
                                this.getSKU();
                                this.loading = false;                                
                            }, 1000);
                        },
                            error => {
                                this.messageError = <any>error
                                this.service.errorserver();
                                this.loading = false;
                            });
                }

            }).catch(swal.noop);
    }
    getTerName(id){
        let ter = this.territories.find((el)=>el.territoryId == id)
        if(ter)
            return ter.territoryName
        else
            return ''
    }

}