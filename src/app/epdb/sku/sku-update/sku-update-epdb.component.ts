import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../../shared/service/session.service';

@Component({
    selector: 'sku-update-epdb',
    templateUrl: './sku-update-epdb.component.html',
    styleUrls: [
        './sku-update-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class UpdateSKUEPDBComponent implements OnInit {

    loading = false
    territories = []
    regions = []
    distributors = []
    addSKU = new FormGroup({
        Id:new FormControl('',Validators.required),
        Territory:new FormControl('',Validators.required),
        Region:new FormControl('',Validators.required),
        Country:new FormControl('',Validators.required),
        Distributor:new FormControl(''),
        Price:new FormControl('',Validators.required),
        Status:new FormControl('',Validators.required),
        Description:new FormControl('',Validators.required),
        SiteId:new FormControl('',Validators.required),
        SiteCountryDistributorSKUId: new FormControl('',Validators.required),
    })
    masterSku:any = {}
    constructor(private router:ActivatedRoute, private service:AppService) { }

    ngOnInit() {
        this.getdata()
        this.getDistributor()
        this.loading = true
        this.router.params.subscribe((params)=>{
            let id = params['id']
            this.service.httpClientGet('api/MainInvoices/GetPricingSKUDetails?id='+id,{}).subscribe((res: any)=>{
                this.addSKU.controls['Id'].setValue(id)
                this.addSKU.controls['Territory'].setValue(res.territory)
                this.addSKU.controls['Region'].setValue(res.region)
                this.addSKU.controls['Country'].setValue(res.country)
                this.addSKU.controls['Distributor'].setValue(res.distributor)
                this.addSKU.controls['Price'].setValue(res.price)
                this.addSKU.controls['Status'].setValue(res.status)
                this.addSKU.controls['Description'].setValue(res.description)
                this.addSKU.controls['SiteId'].setValue(res.siteId)
                this.addSKU.controls['SiteCountryDistributorSKUId'].setValue(res.siteCountryDistributorSKUId)
                this.getRegion()
                this.service.httpClientGet('api/MainInvoices/GetMasterSKUDetails?id='+res.siteCountryDistributorSKUId,{}).subscribe((res: any)=>{
                    if (res == '' || res == null) {
                        this.masterSku = '';
                      } else {
                        this.masterSku = res
                      }  
                })
                this.loading = false
            })
        })
    }

    getdata(){
        this.service.httpClientGet('api/MainInvoices/GetListTerritory',{}).subscribe((res: any)=>{
            this.territories= res
        })
        this.service.httpClientGet('api/MainInvoices/GetListDistributor',{}).subscribe((res: any)=>{
            this.distributors= res
        })
    }

    getRegion(){
        this.service.httpClientGet('api/MainInvoices/GetListCountry?territoryId='+this.addSKU.get('Territory').value,{}).subscribe((res: any)=>{
            this.regions = res
        })
    }

    getDistributor(){
        this.service.httpClientGet('api/MainInvoices/GetListDistributor',{}).subscribe((res: any)=>{
            this.distributors = res
        })
    }

    onSubmit(){
        this.loading = true
        this.service.httpClientPost('api/MainInvoices/EditPricingSKU',this.addSKU.value).subscribe((res)=>{
            this.loading = false
            let res_json: any = res;
            if(res_json.code == 0)
                swal('ERROR',res_json.message,'error')
            else
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Update Sku success',
                    allowOutsideClick:false
                }).then(()=>{
                window.history.back();
                })
        },(err)=>{
            this.loading = false
            swal('ERROR','Update SKU faild','error')
        })
    }
}