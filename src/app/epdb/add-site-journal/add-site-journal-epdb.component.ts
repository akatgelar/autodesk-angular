import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';
import { DatePipe } from '@angular/common';

const now = new Date();

@Component({
    selector: 'app-add-site-journal-epdb',
    templateUrl: './add-site-journal-epdb.component.html',
    styleUrls: [
        './add-site-journal-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddSiteJournalEPDBComponent implements OnInit {
    private SiteId;
    private SiteIdInt;
    public journal;
    private activityDate;
    private useraccessdata;
    addSiteJournal: FormGroup;
    modelPopup1: NgbDateStruct;
    public loading = false;
    public useraccesdata: any;
    modelPopup: NgbDateStruct;
    userId;
    cuid;

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate,
      private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {

        this.modelPopup = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        this.useraccessdata = JSON.parse(this.session.getData());

        let ActivityId = new FormControl('', Validators.required);
        let Notes = new FormControl('');
        let ActivityDate = new FormControl('', Validators.required);

        this.addSiteJournal = new FormGroup({
            ActivityId: ActivityId,
            Notes: Notes,
            ActivityDate: ActivityDate
        });
    }

    getJournalActivities(SiteId) {
        var data: any;
        var activityType = "Site Journal Entry";
        this.service.httpClientGet("api/MainSite/JournalActivities/" + SiteId + "/" + activityType, data)
            .subscribe(res => {
                data = res;
                data.length == 0 ? this.journal = [] : this.journal = data;
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.activityDate = this.parserFormatter.format(date);
        } else {
            this.activityDate = "";
        }
    }

    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || '';
            this.SiteIdInt = params['SiteIdInt'] || 0;
        });
        this.getJournalActivities(this.SiteId);
    }

    onSubmit() {
        this.addSiteJournal.controls["ActivityId"].markAsTouched();
        this.addSiteJournal.controls["ActivityDate"].markAsTouched();
        if (this.addSiteJournal.valid) {
            this.loading = true;
            var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
            var cname = this.useraccessdata.ContactName;
            var dataJournal = {
                'ParentId': this.SiteId,
                'DateAdded': this.service.formatDate(),
                'AddedBy': cname,
                'ActivityId': this.addSiteJournal.value.ActivityId,
                'ActivityDate': this.activityDate,
                'Notes':this.addSiteJournal.value.Notes,// this.htmlEntityService.encoder(this.addSiteJournal.value.Notes),
                'DateLastAdmin': dateFormat,//this.service.formatDate(),
                //'LastAdminBy': this.useraccessdata.UserLevelId,
                'LastAdminBy': this.useraccessdata.cuid,
                'UserId': this.useraccessdata.UserId,
                'cuid': cname,
                'History':'Site Journal Entry'
            };
            this.service.httpClientPost("api/OrganizationJournalEntries", dataJournal)
			.subscribe(res => {
				console.log(res);
            });
            this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
            this.loading = false;
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
    }

    resetForm() {
        this.addSiteJournal.reset({
            'ActivityId': '',
            'ActivityDate': '',
            'Notes': ''
        });
    }
}
