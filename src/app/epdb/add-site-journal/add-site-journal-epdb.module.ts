import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSiteJournalEPDBComponent } from './add-site-journal-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const AddSiteJournalEPDBRoutes: Routes = [
    {
        path: '',
        component: AddSiteJournalEPDBComponent,
        data: {
            breadcrumb: 'epdb.manage.site.site_journal_entries.add_new_site_journal',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddSiteJournalEPDBRoutes),
        SharedModule,
        // AngularMultiSelectModule
        LoadingModule
    ],
    declarations: [AddSiteJournalEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class AddSiteJournalEPDBModule { }
