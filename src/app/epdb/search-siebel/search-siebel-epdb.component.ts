import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js'; 
import {Http, Headers, Response} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";

import {AppService} from "../../shared/service/app.service";

@Component({
  selector: 'app-search-siebel-epdb',
  templateUrl: './search-siebel-epdb.component.html',
  styleUrls: [
    './search-siebel-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SearchSiebelEPDBComponent implements OnInit {
 
    private _serviceUrl = 'api/MainContact';
    messageError: string = ''; 
    public datadetail: any;
    searchsiebel:FormGroup;

    constructor(private service:AppService, private route:ActivatedRoute) {

        let searchContactName = new FormControl('', Validators.required);
        let searchEmailAddress = new FormControl('', Validators.required);
        let searchContactId = new FormControl('', Validators.required);
        let searchAccountCSN = new FormControl('', Validators.required);
    
        this.searchsiebel = new FormGroup({
            searchContactName:searchContactName,
            searchEmailAddress:searchEmailAddress,  
            searchContactId:searchContactId,  
            searchAccountCSN:searchAccountCSN
        });

    }

    ngOnInit() {

        //get data contact
        var id = this.route.snapshot.params['id'];
        var data = '';
        this.service.httpClientGet(this._serviceUrl+'/'+id, data)
        .subscribe(result => {
            this.datadetail = result;  
            this.buildForm();
        },
        error => {
            this.messageError = <any>error
            this.service.errorserver();
        });

    }

    buildForm(): void {
    
        let searchContactName = new FormControl(this.datadetail.contactName);
        let searchEmailAddress = new FormControl(this.datadetail.emailAddress);
        let searchContactId = new FormControl('');
        let searchAccountCSN = new FormControl('');
    
        this.searchsiebel = new FormGroup({
            searchContactName:searchContactName,
            searchEmailAddress:searchEmailAddress,  
            searchContactId:searchContactId,  
            searchAccountCSN:searchAccountCSN
        });
    
    }
  

}
