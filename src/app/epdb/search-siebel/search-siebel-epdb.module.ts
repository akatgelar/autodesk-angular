import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchSiebelEPDBComponent } from './search-siebel-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";

export const SearchSiebelEPDBRoutes: Routes = [
  {
    path: '',
    component: SearchSiebelEPDBComponent,
    data: {
      breadcrumb: 'Siebel Search',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchSiebelEPDBRoutes),
    SharedModule
  ],
  declarations: [SearchSiebelEPDBComponent],
  providers:[AppService]
})
export class SearchSiebelEPDBModule { }
