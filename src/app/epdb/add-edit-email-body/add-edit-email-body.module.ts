import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditEmailComponent } from './add-edit-email-body.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { TinymceModule } from 'angular2-tinymce';

export const AddEditEmailRoutes: Routes = [
    {
        path: '',
        component: AddEditEmailComponent,
        data: {
            breadcrumb: 'epdb.admin.email_body.add_edit_email_body',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddEditEmailRoutes),
        SharedModule,
        LoadingModule,
        TinymceModule.withConfig({
            menubar: false,
            // skin_url:"assets/tinymce/skins/lightgray"
            skin_url: "../../assets/tinymce/skins/lightgray"
        })
    ],
    declarations: [AddEditEmailComponent],
    providers: [AppService, AppFormatDate]
})
export class AddEditEmailModule { }
