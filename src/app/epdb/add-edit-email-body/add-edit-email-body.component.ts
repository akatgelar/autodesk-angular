import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { AppService } from "../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';



// @Pipe({ name: 'dataFilterLanguage' })
// export class DataFilterLanguagePipe {
//     transform(array: any[], query: string): any {
//         if (query) {
//             return _.filter(array, row =>
//                 // (row.countries_code.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
//                 (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
//         }
//         return array;
//     }
// }

@Component({
    selector: 'app-add-edit-email-body',
    templateUrl: './add-edit-email-body.component.html',
    styleUrls: [
        './add-edit-email-body.component.css',
        '../../../../node_modules/c3/c3.min.css'
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddEditEmailComponent implements OnInit {

    private _serviceUrl = "api/EmailBody";
    public data: any;
    addEmailBody: FormGroup;
    public content;
    public useraccesdata: any;
    private body_id;
    language;
    public loading = false;

    constructor(private router: Router, private _http: Http, private service: AppService,
        private session: SessionService, private formatdate: AppFormatDate, private datePipe: DatePipe, private route: ActivatedRoute) {
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let body_type = new FormControl('', Validators.required);
        let body_email = new FormControl('', Validators.required);
        let subject = new FormControl('', [Validators.required, Validators.maxLength(100)]);
        let Key = new FormControl('', Validators.required);

        this.addEmailBody = new FormGroup({
            body_type: body_type,
            body_email: body_email,
            subject: subject,
            Key: Key
        });
    }

    getLanguage() {
        var language = '';
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", language)
            .subscribe((result:any) => {
                this.language = result.sort((a,b)=>{
                    if(a.KeyValue>b.KeyValue)
                        return 1
                    else if(a.KeyValue<b.KeyValue)
                        return -1
                    else return 0
                });
            },
                error => {
                    this.service.errorserver();
                });
    }

    body_typearr:any;
    ngOnInit() {

        this.loading = true;
        this.service.httpClientGet(this._serviceUrl + "/Type", '')
            .subscribe((res:any) => {
                this.body_typearr = res.sort((a,b)=>{
                    if(a.EmailBodyTypeName>b.EmailBodyTypeName)
                        return 1
                    else if(a.EmailBodyTypeName<b.EmailBodyTypeName)
                        return -1
                    else return 0
                });
                this.loading = false;
            }, error => {
                this.service.errorserver();
                this.loading = false;
            });

        this.getLanguage();

        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.body_id = params['body_id'] || '';
        });

        if (this.body_id != '') {
            var data: any;
            this.service.httpClientGet(this._serviceUrl + "/" + this.body_id, data)
                .subscribe(res => {
                    data = res;
                    // data = data.replace(/\r/g, "\\r");
                    // data = data.replace(/\n/g, "\\n");
                    // data = JSON.parse(data);
                    if (data != null) {
                        this.data = data;
                        this.buildForm();
                    }
                    this.loading = false;
                }, error => {
                    this.service.errorserver();
                    this.loading = false;
                });
        }
    }

    buildForm(): void {
        let body_type = new FormControl(this.data.body_type, Validators.required);
        // this.onChangeType(this.data.body_type, this.data.Key);
        this.onChangeTypeNew(this.data.body_type); /* issue 20092018 - Reset button does not work in add edit email page */
        let body_email = new FormControl(this.data.body_email, Validators.required);
        let subject = new FormControl(this.data.subject, Validators.required);
        let Key = new FormControl(this.data.Key, Validators.required);

        this.addEmailBody = new FormGroup({
            body_type: body_type,
            body_email: body_email,
            subject: subject,
            Key: Key
        });
    }

    onSubmit() {
        this.addEmailBody.controls['body_type'].markAsTouched();
        this.addEmailBody.controls['body_email'].markAsTouched();
        this.addEmailBody.controls['subject'].markAsTouched();
        this.addEmailBody.controls['Key'].markAsTouched();
var today = new Date();
			var dateFormat=today.getFullYear() +"-"+today.getMonth()+"-"+today.getDate()+" " +today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        if (this.addEmailBody.valid) {
            this.loading = true;
            let subjectTemp = this.addEmailBody.value.subject;
            let body = this.addEmailBody.value.body_email;

            //perlu 4x backslash karena kalau 3x ditolak sama mysql pas nyimpen, berhubung di serialize dulu
            subjectTemp = subjectTemp.replace(/\"/g, '\\\\"');
            body = body.replace(/\"/g, '\\\\"');
            if (this.body_id != '') {
                var dataUpdate = {
                    'body_type': this.addEmailBody.value.body_type,
                    'body_email': body,
                    'subject': subjectTemp,
                    'Key': this.addEmailBody.value.Key,
                    'LastAdminBy': this.useraccesdata.ContactName,
                    'DateLastAdmin': dateFormat,
					//this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                    'muid': this.useraccesdata.ContactName,
                    'cuid': this.useraccesdata.ContactName,
                    'UserId': this.useraccesdata.UserId
                };
                this.service.httpCLientPut(this._serviceUrl+'/'+this.body_id, dataUpdate)
                    .subscribe(res=>{
                        console.log(res)
                    });
            } else {
                var data = {
                    'body_type': this.addEmailBody.value.body_type,
                    'body_email': body,
                    'subject': subjectTemp,
                    'Key': this.addEmailBody.value.Key,
                    'AddedBy': this.useraccesdata.ContactName,
                    // 'DateAdded': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"), /* issue 14112018 - Couldn’t add any records */
                    'muid': this.useraccesdata.ContactName,
                    'cuid': this.useraccesdata.ContactName,
                    'UserId': this.useraccesdata.UserId
                };
                this.service.httpClientPost(this._serviceUrl, JSON.stringify(data))
				 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

            }
            setTimeout(() => {
                this.router.navigate(['/admin/email/list-email-body']);
                this.loading = false;
            }, 1000);
        }
    }

    resetFormAdd() {
        // this.addEmailBody.reset();

        /* issue 20092018 - Reset button does not work in add edit email page */
        if(this.data == undefined){
            this.addEmailBody.reset({
                body_type: '',
                body_email: '',
                subject: '',
                Key: ''
            });
        }else{
            this.addEmailBody.reset({
                body_type: this.data.body_type,
                body_email: this.data.body_email,
                subject: this.data.subject,
                Key: this.data.Key
            });
            this.onChangeTypeNew(this.data.body_type);
        }
        /* end line issue 20092018 - Reset button does not work in add edit email page */
    }

    // onChangeType(value, key){
    //     if(key != ""){
    //         this.service.get(this._serviceUrl+"/CheckLanguage/"+value+"/"+key, '')
    //             .subscribe(res => {
    //                 this.language = JSON.parse(res);
    //             })
    //     }else{
    //         this.service.get(this._serviceUrl+"/CheckLanguage/"+value+"/null", '')
    //             .subscribe(res => {
    //                 this.language = JSON.parse(res);
    //             })
    //     }
    // }

    /* issue 20092018 - Reset button does not work in add edit email page */
    onChangeTypeNew(value){
        if(value != ""){
            if(value == this.data.body_type){
                this.service.httpClientGet(this._serviceUrl+"/CheckLanguage/"+value+"/"+this.data.Key, '')
                    .subscribe((res:any) => {
                        this.language = res.sort((a,b)=>{
                            if(a.KeyValue>b.KeyValue)
                                return 1
                            else if(a.KeyValue<b.KeyValue)
                                return -1
                            else return 0
                        });;
                    })
            }else{
                this.service.httpClientGet(this._serviceUrl+"/CheckLanguage/"+value+"/null", '')
                    .subscribe((res:any) => {
                        this.language = res.sort((a,b)=>{
                            if(a.KeyValue>b.KeyValue)
                                return 1
                            else if(a.KeyValue<b.KeyValue)
                                return -1
                            else return 0
                        });;
                    })
            }
        }
    }
    /* end line issue 20092018 - Reset button does not work in add edit email page */

}
