import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-add-site-accreditation-epdb',
    templateUrl: './add-site-accreditation-epdb.component.html',
    styleUrls: [
        './add-site-accreditation-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddSiteAccrEPDBComponent implements OnInit {
    private SiteId;
    private SiteIdInt;
    public journal;
    private activityDate;
    private useraccessdata;
    addJournal: FormGroup;
    modelPopup1: NgbDateStruct;
    public loading = false;

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate,
        private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        let ActivityId = new FormControl('', Validators.required);
        let Notes = new FormControl('', Validators.required);
        let ActivityDate = new FormControl('', Validators.required);

        this.addJournal = new FormGroup({
            ActivityId: ActivityId,
            Notes: Notes,
            ActivityDate: ActivityDate
        });
    }

    getJournalActivities(SiteId) {
        var data: any;
        var activityType = "Site Accreditation";
        this.service.httpClientGet("api/MainSite/JournalActivities/" + SiteId + "/" + activityType, data)
            .subscribe(res => {
                data = res;
                data.length == 0 ? this.journal = [] : this.journal = data;
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.activityDate = this.parserFormatter.format(date);
        } else {
            this.activityDate = "";
        }
    }

    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.SiteId = params['SiteId'] || '';
            this.SiteIdInt = params['SiteIdInt'] || 0;
        });
        this.getJournalActivities(this.SiteId);
    }

    onSubmit() {

        this.addJournal.controls["ActivityId"].markAsTouched();
        this.addJournal.controls["ActivityDate"].markAsTouched();
        this.addJournal.controls["Notes"].markAsTouched();

        if (this.addJournal.valid) {
            this.loading = true;
            var dataJournal = {
                'ParentId': this.SiteId,
                'DateAdded': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                'AddedBy': this.useraccessdata.ContactName,
                'ActivityId': this.addJournal.value.ActivityId,
                'ActivityDate': this.activityDate,
                'Notes': this.addJournal.value.Notes,
                'DateLastAdmin': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                'LastAdminBy': this.useraccessdata.UserLevelId
            };
            this.service.httpClientPost("api/OrganizationJournalEntries", dataJournal)
			 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
            this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
            this.loading = false;
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
    }

    resetForm() {
        this.addJournal.reset({
            'ActivityId': '',
            'ActivityDate': '',
            'Notes': ''
        });
    }
}