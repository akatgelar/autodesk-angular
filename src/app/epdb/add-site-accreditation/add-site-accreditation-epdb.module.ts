import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSiteAccrEPDBComponent } from './add-site-accreditation-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const AddSiteAccrEPDBRoutes: Routes = [
    {
        path: '',
        component: AddSiteAccrEPDBComponent,
        data: {
            breadcrumb: 'epdb.manage.site.site_accreditation.add_new_site_accreditation',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddSiteAccrEPDBRoutes),
        SharedModule,
        // AngularMultiSelectModule
        LoadingModule
    ],
    declarations: [AddSiteAccrEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class AddSiteAccrEPDBModule { }
