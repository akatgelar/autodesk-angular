import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-login-history-graphs-report-epdb',
  templateUrl: './login-history-graphs-report-epdb.component.html',
  styleUrls: [
    './login-history-graphs-report-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class LoginHistoryGraphsReportEPDBComponent implements OnInit {

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
   
 
  constructor(public http: Http) { }

  ngOnInit() {
    this.http.get(`assets/data/activities.json`)
      .subscribe((data)=> {
        this.data = data.json();
      });
  }

}