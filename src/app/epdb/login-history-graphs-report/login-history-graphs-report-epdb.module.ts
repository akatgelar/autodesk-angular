import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginHistoryGraphsReportEPDBComponent } from './login-history-graphs-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const LoginHistoryGraphsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: LoginHistoryGraphsReportEPDBComponent,
    data: {
      breadcrumb: 'Login History Graphs Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginHistoryGraphsReportEPDBRoutes),
    SharedModule
  ],
  declarations: [LoginHistoryGraphsReportEPDBComponent]
})
export class LoginHistoryGraphsReportEPDBModule { }
