import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LearningHistoryEPDBComponent } from './learning-history-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const LearningHistoryEPDBRoutes: Routes = [
  {
    path: '',
    component: LearningHistoryEPDBComponent,
    data: {
      breadcrumb: 'Learning History',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LearningHistoryEPDBRoutes),
    SharedModule
  ],
  declarations: [LearningHistoryEPDBComponent]
})
export class LearningHistoryEPDBModule { }
