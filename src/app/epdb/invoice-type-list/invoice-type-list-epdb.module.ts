import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceTypeListEPDBComponent } from './invoice-type-list-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";

export const InvoiceTypeListEPDBRoutes: Routes = [
  {
    path: '',
    component: InvoiceTypeListEPDBComponent,
    data: {
      breadcrumb: 'Invoice Type List',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(InvoiceTypeListEPDBRoutes),
    SharedModule
  ],
  declarations: [InvoiceTypeListEPDBComponent],
  providers:[AppService]
})
export class InvoiceTypeListEPDBModule { }
