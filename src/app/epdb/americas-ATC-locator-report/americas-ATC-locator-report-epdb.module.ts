import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmericasATCLocatorReportEPDBComponent } from './americas-ATC-locator-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const AmericasATCLocatorReportEPDBRoutes: Routes = [
  {
    path: '',
    component: AmericasATCLocatorReportEPDBComponent,
    data: {
      breadcrumb: 'Americas ATC Locator Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AmericasATCLocatorReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [AmericasATCLocatorReportEPDBComponent]
})
export class AmericasATCLocatorReportEPDBModule { }
