import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-update-site-accreditation-epdb',
    templateUrl: './update-site-accreditation-epdb.component.html',
    styleUrls: [
        './update-site-accreditation-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class UpdateSiteAccrEPDBComponent implements OnInit {
    private SiteId;
    private SiteIdInt;
    public journal;
    private activityDate;
    private useraccessdata;
    private JournalId;
    editJournal: FormGroup;
    modelPopup1: NgbDateStruct;
    public loading = false;

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate,
        private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        let ActivityId = new FormControl('');
        let Notes = new FormControl('', Validators.required);
        let ActivityDate = new FormControl('', Validators.required);

        this.editJournal = new FormGroup({
            ActivityId: ActivityId,
            Notes: Notes,
            ActivityDate: ActivityDate
        });
    }

    getJournalActivities(SiteId) {
        var data: any;
        var activityType = "Site Accreditation";
        this.service.httpClientGet("api/MainSite/JournalActivities/" + SiteId + "/" + activityType, data)
            .subscribe(res => {
                data = res;
                data.length == 0 ? this.journal = [] : this.journal = data;
            }, error => {
                this.service.errorserver();
            });
    }

    getDataJournal(journalId) {
        var data: any;
        this.service.httpClientGet("api/OrganizationJournalEntries/ByJournalId/" + journalId, data)
            .subscribe(res => {
                data = res;
                if (data != null) {
                    var activityDateTemp = (data.ActivityDate).substr(0, 10).replace(/\b0/g, '');
                    var activityDate = activityDateTemp.split('/');
                    this.editJournal.patchValue({ ActivityId: data.ActivityId });
                    this.editJournal.controls["ActivityId"].disable();
                    this.modelPopup1 = {
                        "year": parseInt(activityDate[2]),
                        "month": parseInt(activityDate[1]),
                        "day": parseInt(activityDate[0])
                    };
                    this.editJournal.patchValue({ Notes: data.Notes });
                } else {
                    swal("Information!", "Data Not Found", "error");
                    this.go_Back_Bro();
                }
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.activityDate = this.parserFormatter.format(date);
        } else {
            this.activityDate = "";
        }
    }

    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.JournalId = params['JournalId'] || '';
            this.SiteIdInt = params['SiteIdInt'] || 0;
        });
        this.getJournalActivities(this.SiteId);
        this.getDataJournal(this.JournalId);
    }

    onSubmit() {

        this.editJournal.controls["ActivityDate"].markAsTouched();
        this.editJournal.controls["Notes"].markAsTouched();

        if (this.editJournal.valid) {
            this.loading = true;
            var dataJournal = {
                'DateAdded': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                'AddedBy': this.useraccessdata.ContactName,
                'ActivityDate': this.activityDate,
                'Notes': this.editJournal.value.Notes,
                'DateLastAdmin': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                'LastAdminBy': this.useraccessdata.UserLevelId
            };
            this.service.httpCLientPut("api/OrganizationJournalEntries/"+this.JournalId, dataJournal)
                .subscribe(res=>{
                    console.log(res)
                });
            this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
            this.loading = false;
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
    }

    resetForm() {
        this.getDataJournal(this.JournalId);
    }
}