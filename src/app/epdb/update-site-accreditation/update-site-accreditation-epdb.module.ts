import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateSiteAccrEPDBComponent } from './update-site-accreditation-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const UpdateSiteAccrEPDBRoutes: Routes = [
    {
        path: '',
        component: UpdateSiteAccrEPDBComponent,
        data: {
            breadcrumb: 'epdb.manage.site.site_accreditation.edit_site_accreditation',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UpdateSiteAccrEPDBRoutes),
        SharedModule,
        // AngularMultiSelectModule
        LoadingModule
    ],
    declarations: [UpdateSiteAccrEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class UpdateSiteAccrEPDBModule { }
