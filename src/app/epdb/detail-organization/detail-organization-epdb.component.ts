import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { FileUploader } from "ng2-file-upload";
const URL = 'https://evening-anchorage-3159.herokuapp.comapi/';
import { SessionService } from '../../shared/service/session.service';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
//import { htmlentityService } from '../../shared/htmlentities-service/htmlentity-service';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        if(date)
            return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MMMM-yyyy');
        return ''
    }
  
}

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
    transform(value: string): string {
        if (value) {
            let newValue = value.replace(/,/g, "<br/>")
            return `${newValue}`;
        }
    }
}

@Pipe({ name: 'replaceCurrency' })
export class ReplacePipeCurrency implements PipeTransform {
    transform(value: string): string {
        if (value) {
            var currarr = value.split(',');
            for(var i=0; i<currarr.length; i++){
                currarr[i] = Number(currarr[i]).toLocaleString()
            }
            return currarr.join("<br/>");
        }
    }
}

// Site
@Pipe({ name: 'dataFilterSite' })
export class DataFilterSitePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                // (row.RoleName.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
                (row.SiteAddress1.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Status.toLowerCase().indexOf(query.toLowerCase()) > -1)
                // (row.count.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

// Contact
@Pipe({ name: 'dataFilterContact' })
export class DataFilterContactPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.InstructorId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.EmailAddress.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.CountSites.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

// Organization
@Pipe({ name: 'dataFilterOrganization' })
export class DataFilterOrganizationPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.AddedBy.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ActivityName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ActivityDate.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Notes.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Sites.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}


// Partner Type
@Pipe({ name: 'dataFilterPartnerType' })
export class DataFilterPartnerTypePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.CSN.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.RoleName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.ParamValue.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

// Invoice
@Pipe({ name: 'dataFilterInvoice' })
export class DataFilterInvoicePipe {
    transform(array: any[], query: string): any {
        console.log(query)
        if (query) {
            console.log()
            return _.filter(array, row =>
                (row.InvoiceNumber.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.ContactName.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Component({
    selector: 'app-detail-organization-epdb',
    templateUrl: './detail-organization-epdb.component.html',
    styleUrls: [
        './detail-organization-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DetailOrganizationEPDBComponent implements OnInit {

    @ViewChild('fileUploader') fileUploader
    private attachments = []
    private _serviceUrl = 'api/MainOrganization';
    private deleteOpen = false
    //site datatable
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "SiteName";
    public sortOrder: string = "desc";
    //siebel datatable
    //   public rowsOnPage1: number = 10;
    //   public filterQuery1: string = "";
    //   public sortBy1: string = "name";
    //   public sortOrder1: string = "desc";
    //contacts datatable
    public rowsOnPage2: number = 10;
    public filterQuery2: string = "";
    public sortBy2: string = "ContactName";
    public sortOrder2: string = "desc";
    //organization journal entries datatable
    public rowsOnPage3: number = 10;
    public filterQuery3: string = "";
    public sortBy3: string = "ActivityName";
    public sortOrder3: string = "desc";
    //invoice entries datatable
    public rowsOnPage4: number = 10;
    public filterQuery4: string = "";
    public sortBy4: string = "name";
    public sortOrder4: string = "desc";

    messageError: string = '';
    public datadetail: any;
    public data: any;
    public datacontact: any;
    public datainvoices: any;
    public datadetailinvoices: any;
    public orgentriestype: any;
    public dataorgjournalentries: any;
    public datadetailorgjournalentries: any;
    public datainvoicessites: any;
    public countContactSite = [];
    public countAffiliatedSites = [];
    orgjournalentriesform: FormGroup;
    orgattributesform: FormGroup;
    orgattachmentsform: FormGroup;
    varglobalcsnform: FormGroup;
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    modelPopup3: NgbDateStruct;
    public partnertype: any;
    public partnertypeorg: any;
    link: string = "";
    public loading = false;
    public selectedSite: string;
    useraccessdata:any
    messageResult: string = '';
    uploader: FileUploader = new FileUploader({
        url: URL,
        isHTML5: true
    });
    hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;
  imgLogoName: string = "";
  imgSrc: string = "";
  public isAvailableLogo: boolean = false;

    constructor(private session: SessionService, private router: Router, private service: AppService, private route: ActivatedRoute, private formatdate: AppFormatDate) {//, private htmlEntityService: htmlentityService

        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        //journal entries
        let ParentId = new FormControl();
        let AddedBy = new FormControl();
        let ActivityId = new FormControl('', Validators.required);
        let ActivityDate = new FormControl('', Validators.required);
        let DateLastAdmin = new FormControl();
        let LastAdminBy = new FormControl();
        let Notes = new FormControl('', Validators.required);

        this.orgjournalentriesform = new FormGroup({
            ParentId: ParentId,
            AddedBy: AddedBy,
            ActivityId: ActivityId,
            ActivityDate: ActivityDate,
            DateLastAdmin: DateLastAdmin,
            LastAdminBy: LastAdminBy,
            Notes: Notes
        });

    }

    accesEditOrgBtn: Boolean = true;
    accesAddSiteBtn: Boolean = true;
    accesAffiliateSiteBtn: Boolean = true;
    accessAddorgjournalentriesBtn: Boolean = true;
    accessEditorgjournalentriesBtn: Boolean = true;
    accessAddinvoiceBtn: Boolean = true;
    accessEditinvoiceBtn: Boolean = true;
    ngOnInit() {
        this.accesEditOrgBtn = this.session.checkAccessButton("manage/organization/edit-organization");
        this.accesAddSiteBtn = this.session.checkAccessButton("manage/site/add-site");
        this.accesAffiliateSiteBtn = this.session.checkAccessButton("manage/contact/site-affiliations");
        this.accessAddorgjournalentriesBtn = this.session.checkAccessButton("manage/organization/add-orgjournalentries");
        this.accessEditorgjournalentriesBtn = this.session.checkAccessButton("manage/organization/edit-orgjournalentries")
        this.accessAddinvoiceBtn = this.session.checkAccessButton("manage/organization/add-invoice");
        this.accessEditinvoiceBtn = this.session.checkAccessButton("manage/organization/update-invoice");

        this.loading = true;
        var id = "";
        //get data organization
        id = this.route.snapshot.params['id'];
        var data = '';
        this.LoadData()

        this.service.httpClientGet("api/JournalActivities/where/{'ActivityType': 'Organization Journal Entry'}", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.orgentriestype = '';
                } else {
                    this.orgentriestype = result;
                }
            },
                error => {
                    this.service.errorserver();
                    this.orgentriestype = '';
                });
            
        data = '';
        this.service.httpClientGet("api/MainSite/partnertypeonorg/" + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.partnertypeorg = '';
                } else {
                    this.partnertypeorg = result;
                }
            },
                error => {
                    this.service.errorserver();
                    this.partnertypeorg = '';
                });
    }

    LoadData(){
        var id = "";
        //get data organization
        id = this.route.snapshot.params['id'];
        var data = '';

     
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                this.loading = false;
				var finalresult : any=result;
               // var finalresult = result.replace(/\n/g, "<br>")
               // finalresult = finalresult.replace(/\r/g, " ")
                if (finalresult == "Not found") {
                    this.service.notfound();
                    this.datadetail = '';
                } else {
                    this.datadetail = finalresult;
                    let imageLogo = {};
                    let img;
                    if (this.datadetail.orgLogo != "") {
                      imageLogo = {
                        filename: this.datadetail.LogoName,
                        filetype: this.datadetail.LogoType,
                        value: this.datadetail.orgLogo
                      }
                      this.imgSrc = "data:image/png;base64," + this.datadetail.orgLogo;
                      this.imgLogoName = this.datadetail.LogoName;
                      this.isAvailableLogo = true;

                    } else {
                      imageLogo = {
                        filename: "",
                        filetype: "",
                        value: ""
                      }
                    }
                    this.datadetail.OrgName = this.service.decoder(this.datadetail.OrgName);
                    this.datadetail.EnglishOrgName = this.service.decoder(this.datadetail.EnglishOrgName);
                    this.datadetail.CommercialOrgName = this.service.decoder(this.datadetail.CommercialOrgName);
                    this.datadetail.RegisteredDepartment = this.service.decoder(this.datadetail.RegisteredDepartment);
                    this.datadetail.RegisteredAddress1 = this.service.decoder(this.datadetail.RegisteredAddress1);
                    this.datadetail.RegisteredAddress2 = this.service.decoder(this.datadetail.RegisteredAddress2);
                    this.datadetail.RegisteredAddress3 = this.service.decoder(this.datadetail.RegisteredAddress3);
                    this.datadetail.RegisteredCity = this.service.decoder(this.datadetail.RegisteredCity);
                    this.datadetail.RegisteredStateProvince = this.service.decoder(this.datadetail.RegisteredStateProvince);
                    this.datadetail.InvoicingDepartment = this.service.decoder(this.datadetail.InvoicingDepartment);
                    this.datadetail.InvoicingAddress1 = this.service.decoder(this.datadetail.InvoicingAddress1);
                    this.datadetail.InvoicingAddress2 = this.service.decoder(this.datadetail.InvoicingAddress2);
                    this.datadetail.InvoicingAddress3 = this.service.decoder(this.datadetail.InvoicingAddress3);
                    this.datadetail.InvoicingCity = this.service.decoder(this.datadetail.InvoicingCity);
                    this.datadetail.InvoicingStateProvince = this.service.decoder(this.datadetail.InvoicingStateProvince);
                    this.datadetail.ContractDepartment = this.service.decoder(this.datadetail.ContractDepartment);
                    this.datadetail.ContractAddress1 = this.service.decoder(this.datadetail.ContractAddress1);
                    this.datadetail.ContractAddress2 = this.service.decoder(this.datadetail.ContractAddress2);
                    this.datadetail.ContractAddress3 = this.service.decoder(this.datadetail.ContractAddress3);
                    this.datadetail.ContractCity = this.service.decoder(this.datadetail.ContractCity);
                    this.datadetail.ContractStateProvince = this.service.decoder(this.datadetail.ContractStateProvince);
                    this.datadetail.ATCDirectorFirstName = this.service.decoder(this.datadetail.ATCDirectorFirstName);
                    this.datadetail.ATCDirectorLastName = this.service.decoder(this.datadetail.ATCDirectorLastName);
                    this.datadetail.LegalContactFirstName = this.service.decoder(this.datadetail.LegalContactFirstName);
                    this.datadetail.LegalContactLastName = this.service.decoder(this.datadetail.LegalContactLastName);
                    this.datadetail.BillingContactFirstName = this.service.decoder(this.datadetail.BillingContactFirstName);
                    this.datadetail.BillingContactLastName = this.service.decoder(this.datadetail.BillingContactLastName);
                    this.datadetail.AdminNotes = this.datadetail.AdminNotes.replace("&#10;", " ");
                    this.datadetail.AdminNotes = this.service.decoder(this.datadetail.AdminNotes);
                    this.datadetail.OrgWebAddress = this.service.decoder(this.datadetail.OrgWebAddress);
                    this.datadetail.DateAdded = this.service.decoder(this.datadetail.DateAdded);
                    this.attachments = this.datadetail.Files || []
                    this.link = this.datadetail.OrganizationId;
                  this.selectedSite = this.datadetail.PrimarySiteId;
                 
                 
                    setTimeout(() => {
                        this.showorgjournalentries(this.datadetail.OrgId);
                    }, 1000)

                    setTimeout(() => {
                        this.showinvoices(this.datadetail.OrgId);
                    }, 2000)

                    setTimeout(() => {
                        this.showsite(this.datadetail.OrgId);
                    }, 3000)

                    setTimeout(() => {
                        this.showcontact(this.datadetail.OrgId);
                    }, 4000)
                }
            },
                error => {
                    this.service.errorserver();
                    this.datadetail = '';
                    this.loading = false;
                });
    }
    routeaffiliate(ContactIdInt, SiteId, OrganizationId) {
        //redirect
        this.router.navigate(['/manage/contact/site-affiliations', ContactIdInt, SiteId, OrganizationId], { queryParams: { link: "organization", code: this.link } });
    }

    back(){
        window.history.back()
    }
    
    showsite(id) {
        var data = '';
        this.service.httpClientGet("api/MainSite/getsiteswithcountcontact/" + id, data)
            .subscribe(result => {
                // console.log(result)
                if (result == "Not found") {
                    this.data = '';
                } else {
                    this.data = result;
                    for (var i = 0; i < this.data.length; i++) {
                        if (this.data[i].SiteName != null && this.data[i].SiteName != '') {
                            this.data[i].SiteName = this.service.decoder(this.data[i].SiteName);
                        }
                      }
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                });

        this.service.httpClientGet('api/MainSite/partnertypeonsiteorg/' + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.partnertype = '';
                } else {
                    this.partnertype = result;
                }
            },
                error => {
                    this.service.errorserver();
                    this.partnertype = '';
                });
    }

    showcontact(id) {
        this.loading = true;
        var data = '';
        this.service.httpClientGet("api/MainContact/getcontactwithcountsites/" + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.loading = false;
                    this.datacontact = '';
                    for (var i = 0; i < this.datacontact.length; i++) {
                        if (this.datacontact[i].ContactName != null && this.datacontact[i].ContactName != '') {
                            this.datacontact[i].ContactName = this.service.decoder(this.datacontact[i].ContactName);
                        }
                      }
                } else {
                    this.datacontact = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.datacontact = '';
                    this.loading = false;
                });
    }

    showinvoices(id) {
        var data = '';
        this.service.httpClientGet("api/MainInvoices/where/{'OrgId':'" + id + "'}", data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.datainvoices = '';
                } else {
                    this.datainvoices = result;
                }
            },
                error => {
                    this.service.errorserver();
                    this.datainvoices = '';
                });
    }

    showorgjournalentries(id) {
        var data = '';
        this.service.httpClientGet("api/OrganizationJournalEntries/where/{'ParentId': '" + id + "'}", data)
            .subscribe(result => {
                // console.log(result);
                // var finalresult = result.replace(/\n/g, " ")
                // finalresult = finalresult.replace(/\r/g, " ")
                var finalresult : any = result;
                if (result == "Not Found" || result == null) {
                    this.dataorgjournalentries = '';
                } else {
                    this.dataorgjournalentries = finalresult;
                    for (var i = 0; i < this.dataorgjournalentries.length; i++) {
                        if (this.dataorgjournalentries[i].Notes != null && this.dataorgjournalentries[i].Notes != '') {
                            this.dataorgjournalentries[i].Notes = this.dataorgjournalentries[i].Notes.replace("&#10;", " ");
                            this.dataorgjournalentries[i].Notes = this.service.decoder(this.dataorgjournalentries[i].Notes);
                        }
                      }
                }
            },
                error => {
                    this.service.errorserver();
                    this.dataorgjournalentries = '';
                });
    }

    //   Delete data Org Journal Entries
    //delete confirm
    openConfirmsSwal(id) {
        // var index = this.data.findIndex(x => x.JournalId == id);
        // this.service.httpClientDelete("api/OrganizationJournalEntries/", this.data, id, index)
        // this.loading = false;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          })
            .then(result => {
              if (result == true) {
                var cuid = this.useraccessdata.ContactName
                var UserId = this.useraccessdata.UserId
                var data = '';
               
                this.service.httpClientDelete('api/OrganizationJournalEntries/' + id + '/' + cuid + '/' + UserId, data)
                  .subscribe(result => {
                    var resource :any = result;
                    if (resource['code'] == '1') {
                        this.ngOnInit();
                    }
                    else {
                      swal(
                        'Information!',
                        "Delete Data Failed",
                        'error'
                      );
                    }
                  },
                    error => {
                      this.service.errorserver();
                    });
              }
            }).catch(swal.noop);
    }

    //   //view detail invoices
    //   viewdetailinvoices(id) {
    //     var data = '';
    //     this.service.httpClientGet('api/MainInvoices/'+id, data)
    //     .subscribe(result => {
    //         if(result=="Not found"){
    //             this.datadetailinvoices = '';
    //         }else{
    //             this.datadetailinvoices = result;
    //         }  
    //     },
    //     error => {
    //         this.service.errorserver();
    //         this.datadetailinvoices = '';
    //     });

    //     data='';
    //     this.service.httpClientGet('api/InvoicesSites/where/{"InvoiceId":"'+id+'"}', data)
    //     .subscribe(result => {
    //         if(result=="Not found"){
    //             this.datainvoicessites = '';
    //         }else{
    //             this.datainvoicessites = result;
    //         }  
    //     },
    //     error => {
    //         this.service.errorserver();
    //         this.datainvoicessites = '';
    //     });
    //   }

    //   //view detail invoices
    //   viewdetailorgjournalentries(id) {
    //     var data = '';
    //     this.service.httpClientGet('api/OrganizationJournalEntries/'+id, data)
    //     .subscribe(result => {
    //         if(result=="Not found"){
    //             this.datadetailorgjournalentries = '';
    //         }else{
    //             this.datadetailorgjournalentries = result; 
    //         }  
    //     },
    //     error => {
    //         this.service.errorserver();
    //         this.datadetailorgjournalentries = '';
    //     });
    //   }

    //   //modal form Organization Journal Entries 
    //   submittedOrgJournalEntries: boolean;
    //   onSubmitOrgJournalEntries(){
    //     this.submittedOrgJournalEntries = true;
    //     this.orgjournalentriesform.value.ParentId = this.datadetail.OrgId
    //     this.orgjournalentriesform.value.DateLastAdmin = this.formatdate.dateJStoYMD(new Date());
    //     this.orgjournalentriesform.value.LastAdminBy = this.formatdate.dateJStoYMD(new Date());
    //     this.orgjournalentriesform.value.AddedBy = "admin";
    //     this.orgjournalentriesform.value.ActivityDate = this.formatdate.dateCalendarToYMD(this.orgjournalentriesform.value.ActivityDate); 
    //     this.orgjournalentriesform.value.ActivityId = "157"; 

    //     //convert object to json
    //     let data = JSON.stringify(this.orgjournalentriesform.value);

    //     //post action
    //     this.service.httpClientPos("api/OrganizationJournalEntries", data);
    //   }

    //   fileOverBase(e: any): void {
    //     this.hasBaseDropZoneOver = e;
    //   }

    //   fileOverAnother(e: any): void {
    //     this.hasAnotherDropZoneOver = e;
    //   }
    
    deleteInvoice(invoice){
        swal({
            title: 'Are you sure?',
            text: "This action will delete all related data!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then(result => {
            if (result == true) {
                this.loading = true;
                this.service.httpClientPost('api/MainInvoices/DeleteInvoice/'+invoice.InvoiceId,{}).subscribe((res)=>{
                    console.log(res)
                    this.loading = false;
                    this.datainvoices = this.datainvoices.filter((el)=>el.InvoiceId != invoice.InvoiceId)
                },(err)=>{
                    this.loading = false;
                    swal(
                        'Error',
                        'There was an error when deleting invoice, try again later',
                        'error'
                    )
                })
            }
        }).catch(swal.noop);
    }


    printselected(): void {
        let printContents, popupWin;
        printContents = document.getElementById('forprint').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <style>
                <h1>dd</h1>
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }

    getClassByValue(value: string) {
        switch (value) {
          case "a": return "col-md-2 col-xl-2";
          case "b": return "doNotPrint";
        }
      }

    deleteOrganizationbyID(id,orgId){
        let isAdmin = JSON.parse(localStorage.getItem('autodesk-data')).UserLevelId.indexOf("SUPERADMIN")>-1
        if (!this.fileUploader.valid() && !isAdmin)
            return swal('No File Found','Please add atleast 1 file','error')
        let fileData = []
        this.fileUploader.files.map((file)=>{
            let reader = new FileReader()
            let name = file.name
            reader.onload = ()=>{
                fileData.push({ name:name, data: reader.result })
            }
            reader.readAsDataURL(file);
        })
        swal({
          title: 'Are you sure?',
          text: "This action will delete all related data!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then(result => {
          if (result == true) {
            this.loading = true;
    
           var data =
           {
            id: id,
            orgId :orgId,
            files: fileData
           }
            let delData = JSON.stringify(data);
            this.service.httpClientPost("api/MainOrganization/Delete",delData)
              .subscribe(result => {
                var res :any= result;
                if(res['code'] == "1"){
                  swal(
                    'Information!',
                    res['message'],
                    'success'
                  );
                  this.router.navigate(['/manage/organization/search-organization']);
                  this.loading = false;
                }else{
                  swal(
                    'Information!',
                    res['message'],
                    'success'
                  );
                  this.loading = false;
                }
              }, error => {
                this.loading = false;
              });
          }
      }).catch(swal.noop);
    }

}
