import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DetailOrganizationEPDBComponent } from './detail-organization-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";

import { DataFilterSitePipe, DataFilterContactPipe,DataFilterPartnerTypePipe, DataFilterOrganizationPipe, DataFilterInvoicePipe, ConvertDatePipe, ReplacePipe, ReplacePipeCurrency } from './detail-organization-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { SessionService } from '../../shared/service/session.service';

export const DetailOrganizationEPDBRoutes: Routes = [
  {
    path: '',
    component: DetailOrganizationEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.detail.detail_organization',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailOrganizationEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [DetailOrganizationEPDBComponent, DataFilterSitePipe, DataFilterContactPipe, DataFilterPartnerTypePipe,DataFilterOrganizationPipe, DataFilterInvoicePipe, ConvertDatePipe, ReplacePipe, ReplacePipeCurrency],
  providers: [AppService, AppFormatDate, SessionService, DatePipe],
})
export class DetailOrganizationEPDBModule { }
