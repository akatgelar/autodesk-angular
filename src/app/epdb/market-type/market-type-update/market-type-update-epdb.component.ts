import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import { AppFilterGeo } from "../../../shared/filter-market-type/app.filter-geo";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-market-update-type-epdb',
  templateUrl: './market-type-update-epdb.component.html',
  styleUrls: [
    './market-type-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class MarketTypeUpdateEPDBComponent implements OnInit {

  dropdownListGeo = [];
  selectedItemsGeo = [];

  dropdownListRegion = [];
  selectedItemsRegion = [];

  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];

  dropdownListCountry = [];
  selectedItemsCountry = [];

  dropdownSettings = {};

  updatemarkettype: FormGroup;
  updateCountry: FormGroup;
  private _serviceUrl = 'api/MarketType';
  public data: any;
  public listCountry: any;
  public country: any;
  id: string;
  messageError: string = '';
  selectedCountryId = [];
  public loading = false;
  public sortBy: string = "";
  public sortOrder: string = "desc";
  public useraccesdata: any;
  public showFilter = false;

  constructor(public http: Http, private service: AppService, private router: Router, private route: ActivatedRoute, private filterGeo: AppFilterGeo, private session: SessionService) {

    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let MarketTypeId = new FormControl('');
    let MarketType = new FormControl('');

    this.updatemarkettype = new FormGroup({
      MarketTypeId: MarketTypeId,
      MarketType: MarketType
    });

    let CountryCode = new FormControl('');
    let Country = new FormControl('');
    let Geo = new FormControl('');
    let Region = new FormControl('');
    let SubRegion = new FormControl('');
    let GeoName = new FormControl('');
    let RegionName = new FormControl('');
    let SubRegionName = new FormControl('');
    let CountryTelCode = new FormControl('');
    let Embargoed = new FormControl('');

    this.updateCountry = new FormGroup({
      CountryCode: CountryCode,
      Country: Country,
      Geo: Geo,
      Region: Region,
      SubRegion: SubRegion,
      GeoName: GeoName,
      RegionName: RegionName,
      SubRegionName: SubRegionName,
      CountryTelCode: CountryTelCode,
      Embargoed: Embargoed,
      MarketTypeId: MarketTypeId
    });
  }

  ngOnInit() {
    this.loading = true;
    this.id = this.route.snapshot.params['id'];
    var data = '';
    this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.loading = false;
        }
        else {
          this.data = result;
          this.buildForm();
          this.loading = false;
        }
      },
        error => {
          this.service.errorserver();
          this.loading = false;
        });

    this.getGeo();
    this.getCountries();
    this.getCountry(this.id);

    //setting dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120
    };
  }

  onChangeFilter(isChecked: boolean) {
    if (isChecked) {
      this.showFilter = true;
    } else {
      this.showFilter = false;
    }
  }

  getGeo() {
    // Geo
    var data = '';
    this.service.httpClientGet("api/Geo", data)
      .subscribe(result => {
        this.data = result;
        this.dropdownListGeo = this.data.map((item) => {
          return {
            id: item.geo_code,
            itemName: item.geo_name
          }
        })
      },
        error => {
          this.service.errorserver();
        });
    this.selectedItemsGeo = [];
  }

  getCountry(id) {
    var listCountry = '';
    this.service.httpClientGet("api/Countries2/where/{'MarketTypeId':'" + id + "'}", listCountry)
      .subscribe(result => {
        this.listCountry = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getCountries() {
    var country = '';
    this.service.httpClientGet("api/Countries2", country)
      .subscribe(result => {
        this.country = result;
        this.country.forEach(item => {
          if (item.MarketTypeId == 1) {
            this.dropdownListCountry.push(item);
          }
        })
      },
        error => {
          this.service.errorserver();
        });
  }

  onUpdate() {
    //data and get id
    let data = JSON.stringify(this.updatemarkettype.value);

    //update action
    this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
      .subscribe(res=>{
        console.log(res)
      });
    //this.service.httpCLientPut(this._serviceUrl,this.id, data);

    //redirect
    this.router.navigate(['/admin/market-type/market-type-list']);
    this.loading = false;
  }

  onGeoSelect(item: any) {
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    // console.log(this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  OnGeoDeSelect(item: any) {
    this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onGeoSelectAll(items: any) {
    this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onGeoDeSelectAll(items: any) {
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
  }

  OnRegionDeSelect(item: any) {
    this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
  }

  onRegionSelectAll(items: any) {
    this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
  }

  onRegionDeSelectAll(items: any) {
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  OnSubRegionDeSelect(item: any) {
    this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  onSubRegionSelectAll(items: any) {
    this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  onSubRegionDeSelectAll(items: any) {
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
  }

  onCountriesSelect(item: any) { }
  OnCountriesDeSelect(item: any) { }
  onCountriesSelectAll(item: any) { }
  onCountriesDeSelectAll(item: any) { }

  buildForm(): void {
    let MarketTypeId = new FormControl(this.data.MarketTypeId);
    let MarketType = new FormControl(this.data.MarketType);

    this.updatemarkettype = new FormGroup({
      MarketTypeId: MarketTypeId,
      MarketType: MarketType
    });
  }

  openConfirmsSwal(id, index) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(result => {
        if (result == true) {
          var listCountry = '';
          this.service.httpClientGet("api/Countries2/" + id, listCountry)
            .subscribe(value => {
              let resource : any = value;
              this.updateCountry.value.CountryCode = resource.countries_code;
              this.updateCountry.value.Country = resource.countries_name;
              this.updateCountry.value.Geo = resource.geo_code;
              this.updateCountry.value.Region = resource.region_code;
              this.updateCountry.value.SubRegion = resource.subregion_code;
              this.updateCountry.value.GeoName = resource.geo_name;
              this.updateCountry.value.RegionName = resource.region_name;
              this.updateCountry.value.SubRegionName = resource.subregion_name;
              this.updateCountry.value.CountryTelCode = resource.countries_tel;
              this.updateCountry.value.MarketTypeId = 1;
              this.updateCountry.value.Embargoed = resource.Embargoed;
              let dataUpdate = JSON.stringify(this.updateCountry.value);
              this.service.httpCLientPut('api/Countries2/'+id, dataUpdate)
                .subscribe(res=>{
                  console.log(res)
                });
             //this.service.httpCLientPut('api/Countries2',id, dataUpdate);
              var index = this.listCountry.findIndex(x => x.CountryId == id);
              if (index !== -1) {
                this.listCountry.splice(index, 1);
              }
            },
              error => {
                this.messageError = <any>error
                this.service.errorserver();
              });
        }

      }).catch(swal.noop);
  }

  onChange(id: any, isChecked: boolean) {
    if (isChecked) {
      this.selectedCountryId.push(id);
    } else {
      let index = this.selectedCountryId.indexOf(id)
      if (index > -1) {
        this.selectedCountryId.splice(index, 1);
      }
    }
  }

  onSubmit() {

    this.loading = true;
    this.updatemarkettype.value.muid = this.useraccesdata.ContactName;      
    this.updatemarkettype.value.cuid = this.useraccesdata.ContactName;
    this.updatemarkettype.value.UserId = this.useraccesdata.UserId;
    let data = JSON.stringify(this.updatemarkettype.value);
    var country = '';
    this.service.httpCLientPut("api/MarketType/" + this.updatemarkettype.value.MarketTypeId, data)
      .subscribe(result => {
        var update = result;
        if (update["code"] == 1 && this.selectedCountryId.length != 0) {
          for (let i = 0; i < this.selectedCountryId.length; i++) {
            this.service.httpClientGet("api/Countries2/" + this.selectedCountryId[i], country)
              .subscribe(value => {
                var dataCountry : any = value;
                this.updateCountry.value.CountryCode = dataCountry.countries_code;
                this.updateCountry.value.Country = dataCountry.countries_name;
                this.updateCountry.value.Geo = dataCountry.geo_code;
                this.updateCountry.value.Region = dataCountry.region_code;
                this.updateCountry.value.SubRegion = dataCountry.subregion_code;
                this.updateCountry.value.GeoName = dataCountry.geo_name;
                this.updateCountry.value.RegionName = dataCountry.region_name;
                this.updateCountry.value.SubRegionName = dataCountry.subregion_name;
                this.updateCountry.value.CountryTelCode = dataCountry.countries_tel;
                this.updateCountry.value.MarketTypeId = this.updatemarkettype.value.MarketTypeId;
                this.updateCountry.value.Embargoed = dataCountry.Embargoed;
                let dataUpdate = JSON.stringify(this.updateCountry.value);

                this.service.httpCLientPut('api/Countries2/'+this.selectedCountryId[i], dataUpdate)
                  .subscribe(res=>{
                    console.log(res)
                  });
                  
                this.router.navigate(['/admin/market-type/market-type-list'], { queryParams: { marketName: this.updatemarkettype.value.MarketType } });
                this.loading = false;
              });
          }
        } else if (update["code"] == 1 && this.selectedCountryId.length == 0) {
          this.service.openSuccessSwal(update['message']);
          this.loading = false;
        } else {
          swal(
            'Information!',
            update["message"],
            'error'
          );
          this.loading = false;
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
      }
    


      // .subscribe(result => {
      //   var update = JSON.parse(result);
      //   if (update["code"] == 1 && this.selectedCountryId.length != 0) {
      //     for (let i = 0; i < this.selectedCountryId.length; i++) {
      //       this.service.httpClientGet("api/Countries2/" + this.selectedCountryId[i], country)
      //         .subscribe(value => {
      //           let dataCountry: any = value;
      //           this.updateCountry.value.CountryCode = dataCountry.countries_code;
      //           this.updateCountry.value.Country = dataCountry.countries_name;
      //           this.updateCountry.value.Geo = dataCountry.geo_code;
      //           this.updateCountry.value.Region = dataCountry.region_code;
      //           this.updateCountry.value.SubRegion = dataCountry.subregion_code;
      //           this.updateCountry.value.GeoName = dataCountry.geo_name;
      //           this.updateCountry.value.RegionName = dataCountry.region_name;
      //           this.updateCountry.value.SubRegionName = dataCountry.subregion_name;
      //           this.updateCountry.value.CountryTelCode = dataCountry.countries_tel;
      //           this.updateCountry.value.MarketTypeId = this.updatemarkettype.value.MarketTypeId;
      //           this.updateCountry.value.Embargoed = dataCountry.Embargoed;
      //           let dataUpdate = JSON.stringify(this.updateCountry.value);

      //           // this.service.put('api/Countries2/'+this.selectedCountryId[i], dataUpdate)
      //           //   .subscribe(res=>{
      //           //     console.log(res)
      //           //   });
      //           this.service.httpCLientPut('api/Countries2',this.selectedCountryId[i], dataUpdate);
      //           this.router.navigate(['/admin/market-type/market-type-list'], { queryParams: { marketName: this.updatemarkettype.value.MarketType } });
      //           this.loading = false;
      //         });
      //     }
      //   } else if (update["code"] == 1 && this.selectedCountryId.length == 0) {
      //     this.service.openSuccessSwal(update['message']);
      //     this.loading = false;
      //   } else {
      //     swal(
      //       'Information!',
      //       update["message"],
      //       'error'
      //     );
      //     this.loading = false;
      //   }
      // },
      //   error => {
      //     this.messageError = <any>error
      //     this.service.errorserver();
      //   });

}