import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketTypeUpdateEPDBComponent } from './market-type-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import {AppService} from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-market-type/app.filter-geo";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const MarketTypeUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: MarketTypeUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.market_type.add_update_market_type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MarketTypeUpdateEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [MarketTypeUpdateEPDBComponent],
  providers: [AppService, AppFilterGeo]
})
export class MarketTypeUpdateEPDBModule { }
