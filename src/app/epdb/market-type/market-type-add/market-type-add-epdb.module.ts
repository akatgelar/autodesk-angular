import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketTypeAddEPDBComponent } from './market-type-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-market-type/app.filter-geo";
import { DataFilterCountryPipe } from './market-type-add-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const MarketTypeAddEPDBRoutes: Routes = [
  {
    path: '',
    component: MarketTypeAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.market_type.add_update_market_type',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MarketTypeAddEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [MarketTypeAddEPDBComponent, DataFilterCountryPipe],
  providers: [AppService, AppFilterGeo]
})
export class MarketTypeAddEPDBModule { }
