import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { Router } from '@angular/router';
import { AppFilterGeo } from "../../../shared/filter-market-type/app.filter-geo";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterCountry' })
export class DataFilterCountryPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.countries_name.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-market-add-type-epdb',
  templateUrl: './market-type-add-epdb.component.html',
  styleUrls: [
    './market-type-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class MarketTypeAddEPDBComponent implements OnInit {

  dropdownListGeo = [];
  selectedItemsGeo = [];

  dropdownListRegion = [];
  selectedItemsRegion = [];

  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];

  dropdownListCountry = [];
  selectedItemsCountry = [];

  dropdownSettings = {};

  geo_code = [];
  region_code = [];
  subregion_code = [];
  country_code = [];

  addmarkettype: FormGroup;
  updateCountry: FormGroup;
  private _serviceUrl = 'api/MarketType';
  public data;
  private geo;
  private region;
  private subregion;
  private country;
  public showFilter = false;
  listCountry = [];
  public selectedCountry = false;
  filter = [];
  public market;
  messageError: string = '';
  selectedCountryId = [];
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "countries_name";
  public sortOrder: string = "asc";
  public loading = false;
  public useraccesdata: any;

  constructor(private service: AppService, private router: Router, private filterGeo: AppFilterGeo, private fb: FormBuilder, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let MarketTypeId = new FormControl('');
    let MarketType = new FormControl('', Validators.required);

    this.addmarkettype = new FormGroup({
      MarketTypeId: MarketTypeId,
      MarketType: MarketType,
    });

    let CountryCode = new FormControl('');
    let Country = new FormControl('', Validators.required);
    let Geo = new FormControl('');
    let Region = new FormControl('');
    let SubRegion = new FormControl('');
    let GeoName = new FormControl('');
    let RegionName = new FormControl('');
    let SubRegionName = new FormControl('');
    let CountryTelCode = new FormControl('');
    let Embargoed = new FormControl('');

    this.updateCountry = new FormGroup({
      CountryCode: CountryCode,
      Country: Country,
      Geo: Geo,
      Region: Region,
      SubRegion: SubRegion,
      GeoName: GeoName,
      RegionName: RegionName,
      SubRegionName: SubRegionName,
      CountryTelCode: CountryTelCode,
      Embargoed: Embargoed,
      MarketTypeId: MarketTypeId
    });
  }

  ngOnInit() {
    this.loading = true;
    // Geo
    var data = '';
    this.service.httpClientGet("api/Geo", data)
      .subscribe(result => {
        this.data = result;
        this.dropdownListGeo = this.data.map((item) => {
          return {
            id: item.geo_code,
            itemName: item.geo_name
          }
        })
        this.loading = false;
      },
        error => {
          this.service.errorserver();
          this.loading = false;
        });
    this.selectedItemsGeo = [];

    //get country
    this.getCountries();

    //setting dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120
    };
  }

  onChangeFilter(isChecked: boolean) {
    if (isChecked) {
      this.showFilter = true;
    } else {
      this.showFilter = false;
    }
  }

  getCountries() {
    var data = '';
    this.service.httpClientGet("api/Countries2", data)
      .subscribe(result => {
        this.country = result;
        this.country.forEach(item => {
          if (item.MarketTypeId == 1) {
            this.dropdownListCountry.push(item);
          }
        })
      },
        error => {
          this.service.errorserver();
        });
  }

  onGeoSelect(item: any) {
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  OnGeoDeSelect(item: any) {
    this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onGeoSelectAll(items: any) {
    this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onGeoDeSelectAll(items: any) {
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
  }

  OnRegionDeSelect(item: any) {
    this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
  }

  onRegionSelectAll(items: any) {
    this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
  }

  onRegionDeSelectAll(items: any) {
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  OnSubRegionDeSelect(item: any) {
    this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  onSubRegionSelectAll(items: any) {
    this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  onSubRegionDeSelectAll(items: any) {
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
  }

  onCountriesSelect(item: any) { }
  OnCountriesDeSelect(item: any) { }
  onCountriesSelectAll(item: any) { }
  onCountriesDeSelectAll(item: any) { }

  onChange(id: any, isChecked: boolean) {
    if (isChecked) {
      this.selectedCountryId.push(id);
    } else {
      let index = this.selectedCountryId.indexOf(id)
      if (index > -1) {
        this.selectedCountryId.splice(index, 1);
      }
    }
  }

  //submit form
  submitted: boolean;
  onSubmit() {

    this.addmarkettype.controls['MarketType'].markAsTouched();
    let format = /[!$%^&*+\-=\[\]{};':\\|.<>\/?]/
    if(format.test(this.addmarkettype.value.MarketType))
      return swal('ERROR','Special character not allowed in Market name','error')
    if (this.addmarkettype.valid) {
      this.loading = true;
      this.addmarkettype.value.muid = this.useraccesdata.ContactName;      
      this.addmarkettype.value.cuid = this.useraccesdata.ContactName;
      this.addmarkettype.value.UserId = this.useraccesdata.UserId;
      if (this.selectedCountryId.length != 0) {
        this.submitted = true;
        let data = JSON.stringify(this.addmarkettype.value);
        this.service.httpClientPost("api/MarketType", data)
          .subscribe(result => {
            var value = result;
            if (value["code"] == 1) {
              var market = '';
              this.service.httpClientGet("api/MarketType/where/{'MarketType':'" + this.addmarkettype.value.MarketType + "'}", market)
                .subscribe(item => {
                  this.market = item;
                  this.updateCountries(this.market[0]["MarketTypeId"]);
                });
            } else {
              swal('Information!', value["message"], 'error');
              this.loading = false;
            }
          },
            error => {
              this.messageError = <any>error
              this.service.errorserver();
              this.loading = false;
            });
      } else {
        swal('Information!', 'There is no selected Country', 'error');
        this.loading = false;
      }
    }
  }
  resetForm() {
    this.addmarkettype.reset();
  }

  updateCountries(id) {

    for (let i = 0; i < this.selectedCountryId.length; i++) {
      var country = '';
      this.service.httpClientGet("api/Countries2/" + this.selectedCountryId[i], country)
        .subscribe(result => {
          this.country = result;
          this.updateCountry.value.CountryCode = this.country["countries_code"];
          this.updateCountry.value.Country = this.country["countries_name"];
          this.updateCountry.value.Geo = this.country["geo_code"];
          this.updateCountry.value.Region = this.country["region_code"];
          this.updateCountry.value.SubRegion = this.country["subregion_code"];
          this.updateCountry.value.GeoName = this.country["geo_name"];
          this.updateCountry.value.RegionName = this.country["region_name"];
          this.updateCountry.value.SubRegionName = this.country["subregion_name"];
          this.updateCountry.value.CountryTelCode = this.country["countries_tel"];
          this.updateCountry.value.Embargoed = this.country["Embargoed"];
          this.updateCountry.value.MarketTypeId = id;
          let data = JSON.stringify(this.updateCountry.value);
          // this.service.httpCLientPut("api/Countries2", this.selectedCountryId[i], data);
          // this.openSuccessSwal();
          // this.router.navigate(['/admin/market-type/market-type-list'], { queryParams: { marketName: this.addmarkettype.value.MarketType } });
          // this.loading = false;
          this.service.httpCLientPut("api/Countries2/" + this.selectedCountryId[i], data)
          .subscribe(hasil => {
            var hasilUpdate = hasil;
            if (hasilUpdate["code"] == 1) {
              this.openSuccessSwal();
              this.router.navigate(['/admin/market-type/market-type-list'], { queryParams: { marketName: this.addmarkettype.value.MarketType } });
              this.loading = false;
            } else {
              swal(
                'Information!',
                hasilUpdate["message"],
                'error'
              );
              this.loading = false;
            }
          },
            error => {
              this.messageError = <any>error
              this.service.errorserver();
              this.loading = false;
            });
        },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
            this.loading = false;
          });
    }
  }

  //success notification
  openSuccessSwal() {
    swal({
      title: 'Success!',
      text: 'Market Type has been added!',
      type: 'success'
    }).catch(swal.noop);
  }

}