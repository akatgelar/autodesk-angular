import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketTypeListEPDBComponent } from './market-type-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterMarketTypePipe } from './market-type-list-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const MarketTypeListEPDBRoutes: Routes = [
  {
    path: '',
    component: MarketTypeListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.market_type.market_type_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MarketTypeListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [MarketTypeListEPDBComponent, DataFilterMarketTypePipe],
  providers: [AppService, SessionService]
})
export class MarketTypeListEPDBModule { }
