import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterMarketType' })
export class DataFilterMarketTypePipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.countries_name.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-market-list-type-epdb',
    templateUrl: './market-type-list-epdb.component.html',
    styleUrls: [
        './market-type-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class MarketTypeListEPDBComponent implements OnInit {

    public data: any;
    public dataMarket: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "countries_name";
    public sortOrder: string = "asc";
    private _serviceUrl = 'api/MarketType';
    public marketFound = false;
    public showUpdate = true;
    public marketId = "";
    private marketName;
    public loading = false;
    market = [];
    messageError: string = '';
    updateCountry: FormGroup;
    updateMarket: FormGroup;

    constructor(public session: SessionService, public http: Http, private service: AppService, private router: Router, private route: ActivatedRoute) {
        let CountryCode = new FormControl('');
        let Country = new FormControl('');
        let Geo = new FormControl('');
        let Region = new FormControl('');
        let SubRegion = new FormControl('');
        let GeoName = new FormControl('');
        let RegionName = new FormControl('');
        let SubRegionName = new FormControl('');
        let CountryTelCode = new FormControl('');
        let Embargoed = new FormControl('');
        let MarketTypeId = new FormControl('');

        this.updateCountry = new FormGroup({
            CountryCode: CountryCode,
            Country: Country,
            Geo: Geo,
            Region: Region,
            SubRegion: SubRegion,
            GeoName: GeoName,
            RegionName: RegionName,
            SubRegionName: SubRegionName,
            CountryTelCode: CountryTelCode,
            Embargoed: Embargoed,
            MarketTypeId: MarketTypeId
        });
    }

    getDataMarket() {
        this.loading = true;
        var data = '';
        this.market = [];
        this.service.httpClientGet(this._serviceUrl, data)
            .subscribe(result => {
                this.data = result;
                for (let i = 0; i < this.data.length; i++) {
                    if (this.data[i]["MarketTypeId"] != 1 && this.data[i]["countries_code"] != '') {
                        this.market.push(this.data[i]);
                    }
                }
                this.loading = false;
            },
                error => {
                    this.service.errorserver();
                    this.loading = false;
                });
    }

    getMarketId(market) {
        var temp: any;
        this.service.httpClientGet("api/MarketType/where/{'MarketType':'" + market + "'}", temp)
            .subscribe(res => {
                temp = res;
                if (temp != null) {
                    this.marketId = temp[0].MarketTypeId;
                    this.findCountry(this.marketId);
                } else {
                    this.marketId = "";
                }
            }, error => {
                this.messageError = <any>error;
                this.service.errorserver();
            });
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    ngOnInit() {

        this.accesAddBtn = this.session.checkAccessButton("admin/market-type/market-type-add");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/market-type/market-type-add");

        // var sub: any;
        // sub = this.route.queryParams.subscribe(params => {
        //     this.marketName = params['marketName'] || "";
        // });

        // if (this.marketName != "" && this.marketName != undefined) {
        //     this.getDataMarket();
        //     this.getMarketId(this.marketName);
        // } else {
        //     this.getDataMarket();
        //     this.marketId = "";
        // }

        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.marketId = item.idMarket;
            this.findCountry(this.marketId);
        }
        this.getDataMarket();
    }

    //cange table based on combobox
    id: string = "";

    findCountry(value) {
        this.loading = true;
        if (value != "") {
            this.id = value;
            var data = '';
            this.service.httpClientGet("api/Countries2/where/{'MarketTypeId':'" + this.id + "'}", data)
                .subscribe(result => {
                    this.data = result;
                    this.marketFound = true;
                    this.showUpdate = false;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
        }
        else {
            this.id = "";
            this.data = null;
            this.marketFound = false;
            this.showUpdate = true;
            this.loading = false;
        }

        //Buat object untuk filter yang dipilih
        var params =
            {
                idMarket: this.marketId
            }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    }

    openConfirmsSwal(id, index) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then(result => {
                if (result == true) {
                    var data = '';
                    this.service.httpClientGet("api/Countries2/" + id, data)
                        .subscribe(value => {
                            let resource : any = value;
                            this.updateCountry.value.CountryCode = resource.countries_code;
                            this.updateCountry.value.Country = resource.countries_name;
                            this.updateCountry.value.Geo = resource.geo_code;
                            this.updateCountry.value.Region = resource.region_code;
                            this.updateCountry.value.SubRegion = resource.subregion_code;
                            this.updateCountry.value.GeoName = resource.geo_name;
                            this.updateCountry.value.RegionName = resource.region_name;
                            this.updateCountry.value.SubRegionName = resource.subregion_name;
                            this.updateCountry.value.CountryTelCode = resource.countries_tel;
                            this.updateCountry.value.MarketTypeId = 1;
                            this.updateCountry.value.Embargoed = resource.Embargoed;
                            let dataUpdate = JSON.stringify(this.updateCountry.value);
                            // console.log(dataUpdate);
                            this.service.httpCLientPut('api/Countries2/'+id, dataUpdate)
                                .subscribe(res=>{
                                    console.log(res)
                                });
                            //this.service.httpCLientPut('api/Countries2',id, dataUpdate);
                            var index = this.data.findIndex(x => x.CountryId == id);
                            if (index !== -1) {
                                this.data.splice(index, 1);
                            }
                        },
                            error => {
                                this.messageError = <any>error
                                this.service.errorserver();
                            });
                }

            }).catch(swal.noop);
    }

    updateMarketType() {
        this.router.navigate(['/admin/market-type/market-type-update', this.id]);
    }
}