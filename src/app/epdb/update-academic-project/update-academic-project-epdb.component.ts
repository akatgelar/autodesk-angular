import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-update-academic-project-epdb',
    templateUrl: './update-academic-project-epdb.component.html',
    styleUrls: [
        './update-academic-project-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class UpdateAcademicProjectEPDBComponent implements OnInit {

    private _serviceUrl = '';
    editProject: FormGroup;
    messageResult: string = '';
    messageError: string = '';
    public activitytype: any;
    public datadetail: any;
    modelPopup1: NgbDateStruct;
    public listProject;
    listProduct = [];
    public listCountry;
    private completionDate;
    private SiteId;
    private SiteIdInt;
    private useraccessdata;
    private countryCode;
    private projectId;
    private versions;
    public loading = false;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute,
        private parserFormatter: NgbDateParserFormatter, private session: SessionService, private datePipe: DatePipe) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);

        let projecttype = new FormControl('', Validators.required);
        let completiondate = new FormControl('', Validators.required);
        let numbereducator = new FormControl('', Validators.required);
        let numberstudent = new FormControl('', Validators.required);
        let product = new FormControl('', Validators.required);
        let institution = new FormControl('', Validators.required);
        let countryinstitution = new FormControl('', Validators.required);
        let noteproject = new FormControl('');
        // let productVersion = new FormControl('');

        this.editProject = new FormGroup({
            projecttype: projecttype,
            completiondate: completiondate,
            numbereducator: numbereducator,
            numberstudent: numberstudent,
            product: product,
            institution: institution,
            countryinstitution: countryinstitution,
            noteproject: noteproject,
            // productVersion: productVersion,
        });
    }

    getProjectType() {
        var data: any;
        var parent = "ProjectType";
        this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "'}", data)
            .subscribe(res => {
                data = res;
                data.length != 0 ? this.listProject = data : this.listProject = [];
            }, error => {
                this.service.errorserver();
            });
    }

    getProduct() {
        var data: any;
        this.listProduct = [];
        this.service.httpClientGet("api/Product", data)
            .subscribe(res => {
                data = res;
                if (data.length != 0) {
                    for (let i = 0; i < data.length; i++) {
                        var product = {
                            'productId': data[i].productId,
                            'productName': data[i].productName
                        };
                        this.listProduct.push(product);
                    }
                }
            }, error => {
                this.service.errorserver();
            });
    }

    getCountry() {
        var data: any;
        this.service.httpClientGet("api/Countries", data)
            .subscribe(res => {
                data = res;
                data.length != 0 ? this.listCountry = data : this.listCountry = [];
            }, error => {
                this.service.errorserver();
            });
    }

    onSelectDate(date: NgbDateStruct) {
        if (date != null) {
            this.completionDate = this.parserFormatter.format(date);
        } else {
            this.completionDate = "";
        }
    }

    getAcademicProject(projectId) {
        var data: any;
        this.service.httpClientGet("api/AcademicTargetProgram/Project/" + projectId, data)
            .subscribe(res => {
                data = res;
                if (data != null) {
                    this.editProject.patchValue({ projecttype: data.Usage });
                    var date = this.service.DateConversion(data.CompletionDate);
                    var dateTemp = date.split('-');

                    this.modelPopup1 = {
                        "year": parseInt(dateTemp[2]),
                        "month": parseInt(dateTemp[1]),
                        "day": parseInt(dateTemp[0])
                    };
                    this.editProject.patchValue({ numbereducator: data.TargetEducator });
                    this.editProject.patchValue({ numberstudent: data.TargetStudent });
                    this.editProject.patchValue({ product: data.productId });
                    this.findVersion(data.productId);
                    // this.editProject.patchValue({ productVersion: data.productVersion });
                    this.editProject.patchValue({ institution: data.Institution });
                    this.editProject.patchValue({ countryinstitution: data.CountryCode });
                    this.editProject.patchValue({ noteproject: data.Comment });
                } else {
                    swal("Information!", "Data Not Found", "error");
                    this.go_Back_Bro();
                }
            }, error => {
                this.service.errorserver();
            });
    }

    ngOnInit() {
        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.projectId = params['ProjectId'] || '';
            this.SiteIdInt = params['SiteIdInt'] || 0;
        });
        this.getAcademicProject(this.projectId);
        this.getProjectType();
        this.getProduct();
        this.getCountry();
    }

    findVersion(value) {
        if (value != null) {
            var version: any;
            this.service.httpClientGet("api/Product/Version/" + value, version)
                .subscribe(result => {
                    this.versions = result;
                }, error => { this.service.errorserver(); });
        } else {
            this.versions = "";
        }
    }

    onSubmitAcademicProject() {
        this.editProject.controls["projecttype"].markAsTouched();
        this.editProject.controls["completiondate"].markAsTouched();
        this.editProject.controls["numbereducator"].markAsTouched();
        this.editProject.controls["numberstudent"].markAsTouched();
        this.editProject.controls["product"].markAsTouched();
        this.editProject.controls["institution"].markAsTouched();
        this.editProject.controls["countryinstitution"].markAsTouched();

        if (this.editProject.valid) {
            this.loading = true;
            var dataProject = {
                'Usage': this.editProject.value.projecttype,
                'CompletionDate': this.completionDate,
                'TargetEducator': this.editProject.value.numbereducator,
                'TargetStudent': this.editProject.value.numberstudent,
                'productId': this.editProject.value.product,
                // 'productVersion': this.editProject.value.productVersion,
                'Institution': this.editProject.value.institution,
                'CountryCode': this.editProject.value.countryinstitution,
                'Comment': this.editProject.value.noteproject,
                'DateLastAdmin': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                'LastAdminBy': this.useraccessdata.ContactName
            };
            this.service.httpCLientPut("api/AcademicTargetProgram/Project/"+this.projectId, dataProject)
                .subscribe(res=>{
                    console.log(res)
                });
            this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
            this.loading = false;
        }
    }

    go_Back_Bro() {
        this.router.navigate(['manage/site/detail-site/', this.SiteIdInt]);
    }

    resetForm() {
        this.getAcademicProject(this.projectId);
    }
}