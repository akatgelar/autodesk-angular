import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateAcademicProjectEPDBComponent } from './update-academic-project-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const UpdateAcademicProjectEPDBRoutes: Routes = [
  {
    path: '',
    component: UpdateAcademicProjectEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.site.academic_project.edit_academic_project',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UpdateAcademicProjectEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [UpdateAcademicProjectEPDBComponent],
  providers: [AppService, AppFormatDate],
})
export class UpdateAcademicProjectEPDBModule { }