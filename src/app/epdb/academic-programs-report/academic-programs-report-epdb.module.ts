import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcademicProgramsReportEPDBComponent } from './academic-programs-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const AcademicProgramsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: AcademicProgramsReportEPDBComponent,
    data: {
      breadcrumb: 'Academic Programs Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AcademicProgramsReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [AcademicProgramsReportEPDBComponent]
})
export class AcademicProgramsReportEPDBModule { }
