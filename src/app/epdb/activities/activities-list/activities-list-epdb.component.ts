import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.ActivityId.toString().toLowerCase().indexOf(query.trim().toLowerCase()) > -1) || /* issue 23102018 - search box table not working */
                (row.ActivityName.toLowerCase().indexOf(query.trim().toLowerCase()) > -1) ||
                (row.IsUnique.toLowerCase().indexOf(query.trim().toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-activities-list-epdb',
    templateUrl: './activities-list-epdb.component.html',
    styleUrls: [
        './activities-list-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ActivitiesListEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Activity';
    public data: any;
    public datadetail: any;
    public activitytype: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "ActivityName";
    public sortOrder: string = "asc";
    private categoryId;
    private category;
    public dataFound = false;
    public loading = false;
    useraccesdata:any;

    constructor(public session: SessionService, private service: AppService, private route: ActivatedRoute, private router: Router, private location: Location) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

    }

    changetable(category) {
        this.filterQuery = "";
        this.loading = true;
        var listkey = [];
        if (category != "") {
            this.service.httpClientGet(this._serviceUrl + "/where/{'ActivityType':'" + category + "','Status':'A'}", '')
                .subscribe(result => {
                    result == "Not found" || result == null ? this.data = "" : this.data = result;
                    for (let key in this.data[0]) {
                        listkey.push(key);
                    }
                    for(let n = 0; n < this.data.length; n++){
                        for(let j = 0; j < listkey.length; j++){
                            var kolom = listkey[j];
                            if(typeof this.data[n][kolom] === 'object' && this.data[n][kolom].constructor === Object){
                               
                               delete this.data[n][kolom];
                               this.data[n][kolom] = "";
                            }
                        }
                    }
                    this.category = category;
                    this.dataFound = true;
                    this.loading = false;
                },
                    error => {
                        this.service.errorserver();
                        this.loading = false;
                    });
        } else {
            this.dataFound = false;
            this.loading = false;
        }

        //Buat object untuk filter yang dipilih
        var params =
            {
                idKategori: this.categoryId
            }
        //Masukan object filter ke local storage
        localStorage.setItem("filter", JSON.stringify(params));
    }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.datadetail = '';
                }
                else {
                    this.datadetail = result;
                }
            },
                error => {
                    this.service.errorserver();
                    this.datadetail = '';
                });
    }

    // getCategory(value) {
    //     var temp: any;
    //     this.service.httpClientGet("api/ActivityCategory/" + value, temp)
    //         .subscribe(res => {
    //             temp = res;
    //             temp != null ? this.category = temp.activity_category : this.category = "";
    //             this.changetable(this.category);
    //         }, error => {
    //             this.service.errorserver();
    //         });
    //     this.location.go("/admin/activities/activities-list");
    // }

    //delete confirm
   openConfirmsSwal(id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, disable it!'
        }).then(result => {
            if (result == true) {
                this.loading = true;

                /* autodesk plan 10 oct - complete all history log */
                
                var historyLog = 
                    {
                        UserId:this.useraccesdata.UserId,
                        cuid:this.useraccesdata.ContactName
                    }
                
                /* end line autodesk plan 10 oct - complete all history log */

                this.service.httpCLientPut(this._serviceUrl + '/status/'+ id, '')
                    .subscribe(res=>{
                        console.log(res)
                    });
                    
                setTimeout(() => {
                    this.changetable(this.category);
                    this.loading = false;
                }, 1000)
            }
        }).catch(swal.noop);
    }
    

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {
        this.loading = true;
        // var sub: any;
        // sub = this.route.queryParams.subscribe(params => {
        //     this.category = params['category'] || "";
        // });

        // if (this.category != "" && this.category != undefined) {
        //     this.changetable(this.category);
        //     this.categoryId = this.category;
        // }

        //Untuk set filter terakhir hasil pencarian
        if (!(localStorage.getItem("filter") === null)) {
            var item = JSON.parse(localStorage.getItem("filter"));
            this.categoryId = item.idKategori;
            this.changetable(this.categoryId);
        }
        //get data Activity Category
        var data = '';
        this.service.httpClientGet('api/ActivityCategory', data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.activitytype = '';
                    this.loading = false;
                }
                else {
                    this.activitytype = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.activitytype = '';
                    this.loading = false;
                });

        this.accesAddBtn = this.session.checkAccessButton("admin/activities/activities-add");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/activities/activities-update");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/activities/activities-delete");
    }
}
