import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitiesListEPDBComponent } from './activities-list-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";
import { DataFilterActivitiesPipe } from './activities-list-epdb.component';
import { LoadingModule } from 'ngx-loading';
import { SessionService } from '../../../shared/service/session.service';


export const ActivitiesListEPDBRoutes: Routes = [
  {
    path: '',
    component: ActivitiesListEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.activities.activities_list',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivitiesListEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ActivitiesListEPDBComponent, DataFilterActivitiesPipe],
  providers: [AppService,SessionService]
})
export class ActivitiesListEPDBModule { }
