import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-activities-add-epdb',
  templateUrl: './activities-add-epdb.component.html',
  styleUrls: [
    './activities-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ActivitiesAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Activity';
  addactivities: FormGroup;
  public activitytype: any;
  private category;
  private categoryId;
  public useraccesdata: any;
  public loading = false;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //declaration form
    let activity_name = new FormControl('', Validators.required);
    let activity_category = new FormControl('', Validators.required);
    let unique = new FormControl('');
    let description = new FormControl('', Validators.required);
    let status = new FormControl('', Validators.required);

    this.addactivities = new FormGroup({
      activity_name: activity_name,
      activity_category: activity_category,
      unique: unique,
      description: description,
      status: status,
    });
  }

  findCategory(value) {
    if (value != undefined && value != "") {
      var temp: any;
      this.service.httpClientGet("api/ActivityCategory/" + value, temp)
        .subscribe(res => {
          temp = res;
          temp != null ? this.category = temp.activity_category : this.category = "";
        }, error => {
          this.service.errorserver();
        });
      this.categoryId = value;
    } else {
      this.categoryId = 0;
    }
  }

  ngOnInit() {
    //get data Activity Category
    var data = '';
    this.service.httpClientGet('api/ActivityCategory', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.activitytype = '';
        }
        else {
          this.activitytype = result;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  //submit form
  onSubmit() {

    var unique: any;
    //validation
    this.addactivities.controls['activity_name'].markAsTouched();
    this.addactivities.controls['activity_category'].markAsTouched();
    this.addactivities.controls['description'].markAsTouched();
    this.addactivities.controls['status'].markAsTouched();

    let format = /[!#$%^&*+\-=\[\]{}':\\|<>\/]/;
    if(format.test(this.addactivities.value.activity_name))
      return swal('ERROR','Special character not allowed in activity name','error')
    if(format.test(this.addactivities.value.activity_category))
      return swal('ERROR','Special character not allowed in activity category','error')
    if(format.test(this.addactivities.value.description))
      return swal('ERROR','Special character not allowed in description','error')
    if(format.test(this.addactivities.value.status))
      return swal('ERROR','Special character not allowed in status','error')

    //on valid
    if (this.addactivities.valid) {
      //form data
      this.loading = true;
      this.addactivities.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.addactivities.value.mdate = this.formatdate.dateJStoYMD(new Date());
      this.addactivities.value.cuid = this.useraccesdata.ContactName;
      this.addactivities.value.muid = this.useraccesdata.ContactName;

      this.addactivities.value.UserId = this.useraccesdata.UserId;
      if (this.addactivities.value.unique == true)
        this.addactivities.value.unique = "Y";
      else
      this.addactivities.value.unique = "N";
     // this.addactivities.value.Description = this.service.encoder(this.addactivities.value.Description);
      if (this.addactivities.value.unique == true) {
        this.service.httpClientGet("api/Activity/CekUnik/{'ActivityName':'" + this.addactivities.value.activity_name + "'}", unique)
          .subscribe(res => {
            unique = res;
            if (unique != undefined) {
              if (unique.count >= 1) {
                swal('Information!', 'Please enter another activity name', 'error');
                this.addactivities.patchValue({ activity_name: "" });
                this.loading = false;
              } else {
                this.onSubmit();
              }
            } else {
              let data = JSON.stringify(this.addactivities.value);
              this.service.httpClientPost(this._serviceUrl, data)
			  .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});

              this.router.navigate(['/admin/activities/activities-list'], { queryParams: { category: this.addactivities.value.category } });
              this.loading = false;
            }
          }, error => {
            this.service.errorserver();
            this.loading = false;
          });
      } else {
        let data = JSON.stringify(this.addactivities.value);
        this.service.httpClientPost(this._serviceUrl, data)
		.subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
        this.router.navigate(['/admin/activities/activities-list'], { queryParams: { category: this.addactivities.value.category } });
        this.loading = false;
      }
    }
  }

  checkUnique(isChecked: boolean) {
    if (this.addactivities.value.activity_name != "") {
      if (isChecked) {
        var unique: any;
        this.service.httpClientGet("api/Activity/CekUnik/{'ActivityName':'" + this.addactivities.value.activity_name + "'}", unique)
          .subscribe(res => {
            unique = res;
            if (unique != undefined) {
              if (unique.count >= 1) {
                swal('Information!', 'Please enter another activity name', 'error');
                this.addactivities.patchValue({ activity_name: "" });
              }
            }
          }, error => {
            this.service.errorserver();
          });
      }
    } else {
      this.addactivities.controls['activity_name'].markAsTouched();
    }
  }

  //reset form
  resetForm() {
    this.addactivities.reset({
      'status': '',
      'activity_category': ''
    });
  }

}
