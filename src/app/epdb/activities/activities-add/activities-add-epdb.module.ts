import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitiesAddEPDBComponent } from './activities-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";

import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const ActivitiesAddEPDBRoutes: Routes = [
  {
    path: '',
    component: ActivitiesAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.activities.add_new_activities',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivitiesAddEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ActivitiesAddEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class ActivitiesAddEPDBModule { }
