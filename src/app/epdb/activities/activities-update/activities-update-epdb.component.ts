import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { SessionService } from '../../../shared/service/session.service';

@Component({
  selector: 'app-activities-update-epdb',
  templateUrl: './activities-update-epdb.component.html',
  styleUrls: [
    './activities-update-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ActivitiesUpdateEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Activity';
  public data: any;
  updateactivities: FormGroup;
  public activitytype: any;
  id: string;
  private category;
  private categoryId;
  public activitycategory: any;
  public useraccesdata: any;
  public loading = false;

  constructor(private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute, private router: Router, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //declaration form
    let activity_name = new FormControl('');
    let activity_category = new FormControl('');
    let unique = new FormControl('');
    let description = new FormControl('');
    let status = new FormControl('');
    let activity_id = new FormControl('');
    let cdate = new FormControl('');
    let cuid = new FormControl('');

    this.updateactivities = new FormGroup({
      activity_name: activity_name,
      activity_category: activity_category,
      unique: unique,
      description: description,
      status: status,
      activity_id: activity_id,
      cdate: cdate,
      cuid: cuid
    });
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    var data = '';
    var listkey = [];
    //get activity data detail
    this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.data = '';
        }
        else {
          this.data = result;
          for (let key in this.data[0]) {
              listkey.push(key);
          }
          for(let n = 0; n < this.data.length; n++){
          //   if (this.data[n].Description != null && this.data[n].Description != '') {
          //     this.data[n].Description = this.service.decoder(this.data[n].Description);
          // }
              for(let j = 0; j < listkey.length; j++){
                  var kolom = listkey[j];
                  if(typeof this.data[n][kolom] === 'object' && this.data[n][kolom].constructor === Object){
                     
                     delete this.data[n][kolom];
                     this.data[n][kolom] = "";
                  }
              }
          }
          this.data = this.data[0];
          
          // console.log(this.data.IsUnique);
          this.buildForm();
          // this.getdatacategory(this.data);
        }
      },
        error => {
          this.service.errorserver();
        });

    //get data Activity Category
    this.service.httpClientGet('api/ActivityCategory', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.activitytype = '';
        }
        else {
          this.activitytype = result;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  //submit form
  onSubmit() {
    //validation
    this.updateactivities.controls['activity_name'].markAsTouched();
    this.updateactivities.controls['activity_category'].markAsTouched();
    this.updateactivities.controls['description'].markAsTouched();
    this.updateactivities.controls['status'].markAsTouched();

    //on valid
    if (this.updateactivities.valid) {
      this.loading = true;
      //form data
      this.updateactivities.value.cdate = this.formatdate.dateJStoYMD(new Date());
      this.updateactivities.value.mdate = this.formatdate.dateJStoYMD(new Date());
      this.updateactivities.value.cuid = this.useraccesdata.ContactName;
      this.updateactivities.value.muid = this.useraccesdata.ContactName;
      this.updateactivities.value.UserId = this.useraccesdata.UserId;
      
      if (this.updateactivities.value.unique == true) {
        this.updateactivities.value.unique = "Y";
      } else {
        this.updateactivities.value.unique = "N";
      }
      // this.updateactivities.value.description = this.service.encoder(this.updateactivities.value.description);
      let data = JSON.stringify(this.updateactivities.value);

      //update action
      // this.service.httpCLientPut(this._serviceUrl,this.id, data);
      this.service.httpCLientPut(this._serviceUrl+'/'+this.id, data)
        .subscribe(res=>{
          console.log(res)
        });
      setTimeout(() => {
        //redirect
        this.router.navigate(['/admin/activities/activities-list'], { queryParams: { category: this.updateactivities.value.activity_category } });
        this.loading = false;
      }, 1000)

    }
  }

  // getdatacategory(datacategory) {
  //   var data = '';
  //   this.service.httpClientGet('api/ActivityCategory/where/{"activity_category":"' + datacategory.activity_category + '"}', data)
  //     .subscribe(result => {
  //       if (result == "Not found") {
  //         this.service.notfound();
  //         this.activitycategory = '';
  //       }
  //       else {
  //         this.activitycategory = result;
  //         this.buildForm();
  //       }
  //     },
  //       error => {
  //         this.service.errorserver();
  //         this.activitycategory = '';
  //       });
  // }

  //build form update
  buildForm(): void {
    var unik: any;
    if (this.data.IsUnique == "Y") {
      // this.updateactivities.patchValue({ unique: true });
      unik = 'Y';
    } else {
      // this.updateactivities.patchValue({ unique: false });
      unik = '';
    }

    let activity_name = new FormControl(this.data.ActivityName, Validators.required);
    let activity_category = new FormControl(this.data.ActivityType, Validators.required);
    let status = new FormControl(this.data.Status, Validators.required);
    let description = new FormControl(this.data.Description, Validators.required);
    let unique = new FormControl(unik);
    let activity_id = new FormControl(this.data.ActivityId);
    let cdate = new FormControl(this.data.DateAdded);
    let cuid = new FormControl(this.data.AddedBy);

    this.updateactivities = new FormGroup({
      activity_name: activity_name,
      activity_category: activity_category,
      unique: unique,
      status: status,
      description: description,
      activity_id: activity_id,
      cdate: cdate,
      cuid: cuid
    });
  }

  //reset form
  resetForm() {
    this.updateactivities.reset({
      'activity_name': this.data.activity_name,
      'description': this.data.description,
      'status': this.data.status,
      'activity_category': this.activitycategory.activity_category_id,
      'activity_id': this.data.activity_id,
      'unique': this.data.unique,
      'cdate': this.data.cdate,
      'cuid': this.data.cuid
    });
  }

  findCategory(value) {
    if (value != undefined && value != "") {
      var temp: any;
      this.service.httpClientGet("api/ActivityCategory/" + value, temp)
        .subscribe(res => {
          temp = res;
          temp != null ? this.category = temp.activity_category : this.category = "";
        }, error => {
          this.service.errorserver();
        });
      this.categoryId = value;
    } else {
      this.categoryId = 0;
    }
  }
}
