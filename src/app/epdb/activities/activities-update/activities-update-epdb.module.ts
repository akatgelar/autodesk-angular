import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitiesUpdateEPDBComponent } from './activities-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const ActivitiesUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: ActivitiesUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.activities.edit_activities',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivitiesUpdateEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ActivitiesUpdateEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class ActivitiesUpdateEPDBModule { }
