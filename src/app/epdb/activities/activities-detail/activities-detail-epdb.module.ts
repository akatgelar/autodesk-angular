import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitiesDetailEPDBComponent } from './activities-detail-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AppService } from "../../../shared/service/app.service";


export const ActivitiesDetailEPDBRoutes: Routes = [
    {
        path: '',
        component: ActivitiesDetailEPDBComponent,
        data: {
            breadcrumb: 'Detail Activities',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ActivitiesDetailEPDBRoutes),
        SharedModule
    ],
    declarations: [ActivitiesDetailEPDBComponent],
    providers: [AppService]
})
export class ActivitiesDetailEPDBModule { }
