import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-activities-detail-epdb',
    templateUrl: './activities-detail-epdb.component.html',
    styleUrls: [
        './activities-detail-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ActivitiesDetailEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Activity';
    private data: any;
    private id;
    private cdate;
    private cuid;
    private activity_name;
    private unique;
    private description;
    private status;

    constructor(private service: AppService, private route: ActivatedRoute, private router: Router) { }

    //view detail
    viewdetail(id) {
        var data = '';
        this.service.httpClientGet(this._serviceUrl + '/' + id, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.data = '';
                }
                else {
                    this.data = result;
                    this.cdate = this.data.cdate;
                    this.cuid = this.data.cuid;
                    this.activity_name = this.data.activity_name;
                    this.unique = this.data.unique;
                    this.description = this.data.description;
                    this.status = this.data.status;
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                });
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.viewdetail(this.id);
    }
}
