import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateSiteAttributeEPDBComponent } from './update-site-attribute-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const UpdateSiteAttributeEPDBRoutes: Routes = [
    {
        path: '',
        component: UpdateSiteAttributeEPDBComponent,
        data: {
            breadcrumb: 'Update Site Attribute',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UpdateSiteAttributeEPDBRoutes),
        SharedModule,
        LoadingModule
    ],
    declarations: [UpdateSiteAttributeEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class UpdateSiteAttributeEPDBModule { }
