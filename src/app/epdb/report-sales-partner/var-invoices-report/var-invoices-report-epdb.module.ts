import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VARInvoiceReportEPDBComponent } from './var-invoices-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const VARInvoiceReportEPDBRoutes: Routes = [
    {
        path: '',
        component: VARInvoiceReportEPDBComponent,
        data: {
            breadcrumb: 'VAR Invoice Report  ',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(VARInvoiceReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [VARInvoiceReportEPDBComponent]
  })
export class VARInvoiceReportEPDBModule { }