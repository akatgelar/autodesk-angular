import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VARContactAttributeReportEPDBComponent } from './var-contact-attributes-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const VARContactAttributeReportEPDBRoutes: Routes = [
    {
        path: '',
        component: VARContactAttributeReportEPDBComponent,
        data: {
            breadcrumb: 'VAR Contact Attributes Report  ',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(VARContactAttributeReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [VARContactAttributeReportEPDBComponent]
  })
  export class VARContactAttributeReportEPDBModule { }