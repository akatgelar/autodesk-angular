import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VARSiteAttributeReportEPDBComponent } from './var-site-attributes-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const VARSiteAttributeReportEPDBRoutes: Routes = [
    {
        path: '',
        component: VARSiteAttributeReportEPDBComponent,
        data: {
            breadcrumb: 'VAR Site Attribute Report',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(VARSiteAttributeReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [VARSiteAttributeReportEPDBComponent]
  })
export class VARSiteAttributeReportEPDBModule { }