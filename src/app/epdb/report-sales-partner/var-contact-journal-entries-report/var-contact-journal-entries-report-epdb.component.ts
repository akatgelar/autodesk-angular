import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import {Http} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';

@Component({
    selector: 'app-var-contact-journal-entries-report-epdb',
    templateUrl: './var-contact-journal-entries-report-epdb.component.html',
    styleUrls: [
        './var-contact-journal-entries-report-epdb.component.css',
        '../../../../../node_modules/c3/c3.min.css', 
        ], 
    encapsulation: ViewEncapsulation.None
})

export class VARContactJournalEntriesReportEPDBComponent implements OnInit {
    
    dropdownListActivity = [];
    selectedItemsActivity = [];
    dropdownSettingsActivity = {};

    dropdownListPartner = [];
    selectedItemsPartner = [];
    dropdownSettingsPartner = {};
  
    dropdownListPartnerTypeStatus = [];
    selectedItemsPartnerTypeStatus = [];
    dropdownSettingsPartnerTypeStatus = {};
  
    dropdownListMarketType = [];
    selectedItemsMarketType = [];
    dropdownSettingsMarketType = {};

    dropdownListTerritories = [];
    selectedItemsTerritories = [];
    dropdownSettingsTerritories = {};

    dropdownListGeo = [];
    selectedItemsGeo = [];
    dropdownSettingsGeo = {};
  
    dropdownListRegion = [];
    selectedItemsRegion = [];
    dropdownSettingsRegion = {};
  
    dropdownListSubRegion = [];
    selectedItemsSubRegion = [];
    dropdownSettingsSubRegion = {};

    dropdownListCountry = [];
    selectedItemsCountry = [];
    dropdownSettingsCountry = {};
  
    dropdownListSubCountry = [];
    selectedItemsSubCountry = [];
    dropdownSettingsSubCountry = {};
    
    dropdownListFieldSite = [];
    selectedItemsFieldSite = [];
    dropdownSettingsFieldSite = {};

    dropdownListFieldContact = [];
    selectedItemsFieldContact = [];
    dropdownSettingsFieldContact = {};

    dropdownListFieldShow2 = [];
    selectedItemsFieldShow2 = [];
    dropdownSettingsFieldShow2 = {};

    public data: any;
    public partnertype: any;
    public partnertypestatus: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    
    
    constructor(public http: Http) { }

    ngOnInit() {
        this.http.get(`assets/data/activities.json`)
            .subscribe((data)=> {
            this.data = data.json();
        });
        
        this.http.get(`assets/data/partnertype.json`)
            .subscribe((data)=> {
                this.partnertype = data.json();
            }
        );

        this.http.get(`assets/data/partnertypestatus.json`)
            .subscribe((data)=> {
                this.partnertypestatus = data.json();
            }
        );

        // Activity
        this.dropdownListActivity = [
            {"id":297,"itemName":"Siebel Authorization Data"}
        ];
        this.selectedItemsActivity = [];
        this.dropdownSettingsActivity = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Partner
        this.dropdownListPartner = [
            {"id":58,"itemName":"Authorized Academic Partner (AAP)"},
            {"id":1,"itemName":"Authorized Training Center (ATC)"},
            {"id":7,"itemName":"Authors and Publishers"},
            {"id":30,"itemName":"Autodesk Contractor"},
            {"id":9,"itemName":"Autodesk Developer Network (ADN)"}
        ];
        this.selectedItemsPartner = [];
        this.dropdownSettingsPartner = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Partner Type Status
        this.dropdownListPartnerTypeStatus = [
            {"id":"A","itemName":"Active"},
            {"id":"V","itemName":"Approved"},
            {"id":"O","itemName":"Opportunity"},
            {"id":"P","itemName":"Pending"},
            {"id":"R","itemName":"Rejected"}
        ];
        this.selectedItemsPartnerTypeStatus = [];
        this.dropdownSettingsPartnerTypeStatus = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Market Type
        this.dropdownListMarketType = [
            {"id":2,"itemName":"Emerging Markets"},
            {"id":3,"itemName":"Mature Markets"},
            {"id":1,"itemName":"N/A"}
        ];
        this.selectedItemsMarketType = [];
        this.dropdownSettingsMarketType = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Territories
        this.dropdownListTerritories = [
            {"id":3,"itemName":"ASEAN"},
            {"id":10,"itemName":"CIS"},
            {"id":1,"itemName":"Europe"},
            {"id":4,"itemName":"GCR"}
        ];
        this.selectedItemsTerritories = [];
        this.dropdownSettingsTerritories = {
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Geo
        this.dropdownListGeo = [
            {"id":1,"itemName":"AMER"},
            {"id":2,"itemName":"APAC (excl. GCR)"},
            {"id":3,"itemName":"EMEA"},
            {"id":4,"itemName":"GCR"}
        ];
        this.selectedItemsGeo = [];
        this.dropdownSettingsGeo = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Region
        this.dropdownListRegion = [
            {"id":1,"itemName":"ANZ"},
            {"id":2,"itemName":"ASEAN"},
            {"id":3,"itemName":"Canada"},
            {"id":4,"itemName":"Central Europe"}
        ];
        this.selectedItemsRegion = [];
        this.dropdownSettingsRegion = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // SubRegion
        this.dropdownListSubRegion = [
            {"id":1,"itemName":"Africa"},
            {"id":2,"itemName":"ASEAN"},
            {"id":3,"itemName":"Australia / New Zealand"},
            {"id":4,"itemName":"Central Europe"}
        ];
        this.selectedItemsSubRegion = [];
        this.dropdownSettingsSubRegion = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Country
        this.dropdownListCountry = [
            {"id":1,"itemName":"Afganistan"},
            {"id":2,"itemName":"Albania"},
            {"id":3,"itemName":"Algeria"},
            {"id":4,"itemName":"Andorra"}
        ];
        this.selectedItemsCountry = [];
        this.dropdownSettingsCountry = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // SubCountry
        this.dropdownListSubCountry = [
            {"id":1,"itemName":"Ajman"},
            {"id":2,"itemName":"Abia State"},
            {"id":3,"itemName":"Abkhazia"},
            {"id":4,"itemName":"Absheron Rayon"}
        ];
        this.selectedItemsSubCountry = [];
        this.dropdownSettingsSubCountry = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Field Contact
        this.dropdownListFieldContact = [
            {"id":"s.ContactId","itemName":"ContactId"},
            {"id":"s.SiebelId","itemName":"SiebelId"},
            {"id":"s.SiebelStatus","itemName":"SiebelStatus"},
            {"id":"s.PrimarySiteId","itemName":"PrimarySiteId"},
            {"id":"s.ContactName","itemName":"ContactName"}
        ];
        this.selectedItemsFieldContact = [];
        this.dropdownSettingsFieldContact = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        // Field Site
        this.dropdownListFieldSite = [
            {"id":"s.SiteId","itemName":"SiteId"},
            {"id":"s.ATCSiteId","itemName":"ATCSiteId"},
            {"id":"s.OrgId","itemName":"OrgId"},
            {"id":"s.SiteStatus_retired","itemName":"SiteStatus_retired"},
            {"id":"s.SiteName","itemName":"SiteName"}
        ];
        this.selectedItemsFieldSite = [];
        this.dropdownSettingsFieldSite = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };
    
        // Field Show 1
        this.dropdownListFieldShow2 = [
            {"id":"m.MarketType As MarketType","itemName":"Market Type"},
            {"id":"sr.RoleId As PartnerType","itemName":"Partner Type"},
            {"id":"sr.Status","itemName":"Partner Type Status"},
            {"id":"viewsr.CSN","itemName":"Partner Type CSN"},
            {"id":"viewsr.ITS_ID as 'ITS Site ID'","itemName":"ITS Site ID"}
        ];
        this.selectedItemsFieldShow2 = [];
        this.dropdownSettingsFieldShow2 = { 
            singleSelection: false, 
            text:"Please Select",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };
    }

}