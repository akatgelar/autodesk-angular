import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VARContactJournalEntriesReportEPDBComponent } from './var-contact-journal-entries-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const VARContactJournalEntriesReportEPDBRoutes: Routes = [
    {
        path: '',
        component: VARContactJournalEntriesReportEPDBComponent,
        data: {
            breadcrumb: 'VAR Contact Journal Entries Report  ',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(VARContactJournalEntriesReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [VARContactJournalEntriesReportEPDBComponent]
  })
  export class VARContactJournalEntriesReportEPDBModule { }