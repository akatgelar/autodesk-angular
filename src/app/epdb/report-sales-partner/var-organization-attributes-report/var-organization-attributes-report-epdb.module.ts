import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VAROrganizationAttributeReportEPDBComponent } from './var-organization-attributes-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const VAROrganizationAttributeReportEPDBRoutes: Routes = [
    {
        path: '',
        component: VAROrganizationAttributeReportEPDBComponent,
        data: {
            breadcrumb: 'VAR Organization Attributes Report  ',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(VAROrganizationAttributeReportEPDBRoutes),
      SharedModule,
      AngularMultiSelectModule
    ],
    declarations: [VAROrganizationAttributeReportEPDBComponent]
  })
  export class VAROrganizationAttributeReportEPDBModule { }