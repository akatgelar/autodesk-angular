import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListEmailBodyComponent } from './list-email-body.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { DataFilterPipe } from './list-email-body.component';
import { AppService } from "../../shared/service/app.service";
// import { DataFilterLanguagePipe } from './list-language-epdb.component';
import { SessionService } from '../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const ListEmailBodyRoutes: Routes = [
    {
        path: '',
        component: ListEmailBodyComponent,
        data: {
            breadcrumb: 'epdb.admin.email_body.list_email_body',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ListEmailBodyRoutes),
        SharedModule, 
        LoadingModule
    ],
    declarations: [ListEmailBodyComponent, DataFilterPipe],
    providers: [AppService, SessionService]
})
export class ListEmailBodyModule { }
