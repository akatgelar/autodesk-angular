import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceTypeUpdateEPDBComponent } from './invoice-type-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

export const InvoiceTypeUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: InvoiceTypeUpdateEPDBComponent,
    data: {
      breadcrumb: 'Update InvoiceType',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(InvoiceTypeUpdateEPDBRoutes),
    SharedModule
  ],
  declarations: [InvoiceTypeUpdateEPDBComponent],
  providers:[AppService, AppFormatDate],
})
export class InvoiceTypeUpdateEPDBModule { }
