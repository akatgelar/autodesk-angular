import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddMasterDistributorEPDBComponent } from './add-master-distributor-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

import { DataFilterDistributorSitePipe } from './add-master-distributor-epdb.component';
import { LoadingModule } from 'ngx-loading';

export const AddMasterDistributorEPDBRoutes: Routes = [
  {
    path: '',
    component: AddMasterDistributorEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.distributor.add_distributor_site',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddMasterDistributorEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [AddMasterDistributorEPDBComponent, DataFilterDistributorSitePipe],
  providers: [AppService, AppFilterGeo, AppFormatDate]
})
export class AddMasterDistributorEPDBModule { }