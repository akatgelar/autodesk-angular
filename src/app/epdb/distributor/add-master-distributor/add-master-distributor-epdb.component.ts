import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterDistributorSite' })
export class DataFilterDistributorSitePipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.OrgId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Roles.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-add-master-distributor-epdb',
  templateUrl: './add-master-distributor-epdb.component.html',
  styleUrls: [
    './add-master-distributor-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddMasterDistributorEPDBComponent implements OnInit {

  private country;
  private type;
  private geo;
  private countryCode;
  // sites = [];
  public sites;
  selectedSite = [];
  selectedPrimary = [];
  dropdownListGeo = [];
  selectedItemsGeo = [];
  dropdownListRegion = [];
  selectedItemsRegion = [];
  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];
  dropdownListCountry = [];
  selectedItemsCountry = [];
  dropdownSettings = {};
  dropdownCountrySettings = {};
  // data_1 = [];
  // data_2 = [];
  // data_kontak = [];
  // partnerType = [];

  // private _serviceUrl = 'api/MainSite';
  // messageResult: string = '';
  // messageError: string = '';
  // adddistributor: FormGroup;
  // public distributor: any;
  // public data;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  // public geo: string = "";
  // public region: string = "";
  // public subRegion: string = "";
  // public countryName: string = "";
  // private countryCode;
  // public siteID: any;
  // public sites: any;
  // public siteNames: any;
  // public kontak: any;
  // public showAll = false;
  // public dataSite: any;
  // public masterPrimary;
  // public regionalPrimary;
  // public marketName: string = "";
  // public embargoed: string = "";
  // public territory: string = "";
  // addMasterDistributor: FormGroup;
  // setPrimaryAnotherSite: FormGroup;
  // updateContact: FormGroup;
  public loading = false;
  public primarySite: any;
  // public arrSite = [];

  constructor(private router: Router, private service: AppService, private filterGeo: AppFilterGeo, private formatdate: AppFormatDate, private route: ActivatedRoute) {

    // let SiteID = new FormControl('');
    // let SiteName_Master = new FormControl('', Validators.required);
    // let CountryCode = new FormControl('');
    // let EDistributorType = new FormControl('');
    // let PrimaryD = new FormControl('');

    // this.addMasterDistributor = new FormGroup({
    //   SiteName: SiteName_Master,
    //   SiteID: SiteID,
    //   CountryCode: CountryCode,
    //   EDistributorType: EDistributorType,
    //   PrimaryD: PrimaryD
    // });

    // this.setPrimaryAnotherSite = new FormGroup({
    //     SiteID: SiteID,
    //     CountryCode: CountryCode,
    //     EDistributorType: EDistributorType,
    //     PrimaryD: PrimaryD
    // });       

  }

  setDropdownListGeo(data) {
    var exist = false;
    var Geo = {
      'id': data.geo_code,
      'itemName': data.geo_name
    };
    if (this.selectedItemsGeo.length == 0) {
      this.selectedItemsGeo.push(Geo);
      this.onCurrGeoSelect(this.selectedItemsGeo[0]);
    } else {
      for (let n = 0; n < this.selectedItemsGeo.length; n++) {
        if (data.geo_code == this.selectedItemsGeo[n].id) {
          exist = true;
        }
        this.onCurrGeoSelect(this.selectedItemsGeo[n]);
      }
      if (exist == false) {
        this.selectedItemsGeo.push(Geo);
      }
    }
  }

  setDropdownListRegion(data) {
    var exist = false;
    var Region = {
      'id': data.region_id,
      'itemName': data.region_name
    };
    if (this.selectedItemsRegion.length == 0) {
      this.selectedItemsRegion.push(Region);
      this.onCurrRegionSelect(this.selectedItemsRegion[0]);
    } else {
      for (let l = 0; l < this.selectedItemsRegion.length; l++) {
        if (data.region_id == this.selectedItemsRegion[l].id) {
          exist = true;
        }
        this.onCurrRegionSelect(this.selectedItemsRegion[l]);
      }
      if (exist == false) {
        this.selectedItemsRegion.push(Region);
      }
    }
  }

  setDropdownListSubRegion(data) {
    var exist = false;
    var subRegion = {
      'id': data.subregion_id,
      'itemName': data.subregion_name
    };
    if (this.selectedItemsSubRegion.length == 0) {
      this.selectedItemsSubRegion.push(subRegion);
      this.onCurrSubRegionSelect(this.selectedItemsSubRegion[0]);
    } else {
      for (let j = 0; j < this.selectedItemsSubRegion.length; j++) {
        if (data.subregion_id == this.selectedItemsSubRegion[j].id) {
          exist = true;
        }
        this.onCurrSubRegionSelect(this.selectedItemsSubRegion[j]);
      }
      if (exist == false) {
        this.selectedItemsSubRegion.push(subRegion);
      }
    }
  }

  setDropdownListCountry(country) {
    var countryTemp = JSON.parse(country);
    var dataCountry = {
      id: countryTemp[0].id,
      itemName: countryTemp[0].itemName
    };
    this.selectedItemsCountry.push(dataCountry);
  }

  getCountryLinks(args) {
    var temp = JSON.parse(args);
    var data: any;
    this.service.httpClientGet("api/Countries/" + temp[0].id, data)
      .subscribe(res => {
        data = res;
        this.setDropdownListGeo(data);
        this.setDropdownListRegion(data);
        this.setDropdownListSubRegion(data);
        this.getSite(data.countries_code);
      }, error => {
        this.service.errorserver();
      });
  }

  getGeo() {
    var data = '';
    this.service.httpClientGet("api/Geo", data)
      .subscribe(result => {
        this.geo = result;
        this.dropdownListGeo = this.geo.map((item) => {
          return {
            id: item.geo_code,
            itemName: item.geo_name
          }
        })
      },
        error => {
          this.service.errorserver();
        });
    this.selectedItemsGeo = [];
  }

  ngOnInit() {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.country = params['countries'] || [];
      this.type = params['type'] || "";
    });

    if (this.country.length != 0 && this.type != "") {
      this.getCountryLinks(this.country);
      this.setDropdownListCountry(this.country);
    }
    this.getGeo();
    //setting dropdown
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120
    };

    //setting dropdown for Country
    this.dropdownCountrySettings = {
      singleSelection: true,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      // limitSelection: 1,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true
    };

  }

  // checkAvailable(data, country_code) {
  //   this.sites = [];
  //   var siteTemp: any;
  //   for (let i = 0; i < data.length; i++) {
  //     this.service.httpClientGet("api/SiteCountryDistributor/where/{'SiteID':'" + data[i].SiteId + "','CountryCode':'" + country_code + "'}", siteTemp)
  //       .subscribe(result => {
  //         siteTemp = result;
  //         if (siteTemp.length > 0) {
  //           this.sites = [];
  //         } else {
  //           if (this.sites.length == 0) {
  //             this.sites.push(data[i]);
  //           } else {
  //             var exist = false;
  //             for (let j = 0; j < this.sites.length; j++) {
  //               if (data[i].SiteId == this.sites[j].SiteId) {
  //                 exist = true;
  //               }
  //             }
  //             if (exist == false && data[i].SiteId != "") {
  //               this.sites.push(data[i]);
  //             }
  //           }
  //         }
  //       }, error => {
  //         this.service.errorserver();
  //       });
  //   }
  // }

  getSite(country_code) {
    var temp: any;
    // var arTemp = [];
    // arTemp.push('"' + country_code + '"');
    this.service.httpClientGet("api/SiteCountryDistributor/getAnotherDistributor/" + country_code + "/" + this.type, temp)
      .subscribe(res => {
        temp = res;
        console.log(temp);
        if (temp.length != 0) {
          this.sites = temp;
        } else {
          this.sites = "";
        }
        // console.log(temp);
        // if (temp.length != 0) {
        //   this.checkAvailable(temp, country_code);
        // } else {
        //   this.sites = [];
        // }
      });
  }

  search() {
    //Get Country Code
    if (this.selectedItemsCountry.length != 0) {
      var data: any;
      this.service.httpClientGet("api/Countries/" + this.selectedItemsCountry[0].id, data)
        .subscribe(res => {
          data = res;
          if (data != null) {
            this.getSite(data.countries_code);
          } else {
            swal('Information!', 'CountryCode Not Found', 'error');
          }
        }, error => {
          this.service.errorserver();
        });
    }
  }

  addSite(id: any, isChecked: boolean) {
    if (isChecked) {
      this.selectedSite.push(id);
    } else {
      let index = this.selectedSite.indexOf(id)
      if (index > -1) {
        this.selectedSite.splice(index, 1);
      }
    }
  }

  addPrimary(id: any) {
    this.primarySite = id;
  }

  backToAddDistributor() {
    this.router.navigate(['/admin/distributor/add-distributor'], { queryParams: { countries: this.country } });
  }

  saveDistributor(data) {
    // console.log(data);
    this.loading = true;
    var val: any;
    this.service.httpClientPost("api/SiteCountryDistributor", data)
      .subscribe(result => {
        val = result;
        if (val['code'] != 1) {
          swal('Information!', 'Insert Data Failed', 'error');
          this.loading = false;
        }
      }, error => {
        this.service.errorserver();
        this.loading = false;
      });
    this.router.navigate(['/admin/distributor/add-distributor'], { queryParams: { countries: this.country } });
    this.loading = false;
  }

  saveSite() {
    if (this.selectedSite.length != 0) {
      var countryJson = JSON.parse(this.country);
      var countryTemp: any;
      this.service.httpClientGet("api/Countries/" + countryJson[0].id, countryTemp)
        .subscribe(res => {
          countryTemp = res;
          if (countryTemp != null) {
            // this.countryCode = countryTemp.contries_code;
            for (let i = 0; i < this.selectedSite.length; i++) {
              if (this.primarySite != "") {
                if (this.selectedSite[i] == this.primarySite) {
                  var data = {
                    'CountryCode': countryTemp.countries_code,
                    'SiteID': this.selectedSite[i],
                    'EDistributorType': this.type,
                    'PrimaryD': 1
                  };
                  this.saveDistributor(data);
                } else {
                  var data = {
                    'CountryCode': countryTemp.countries_code,
                    'SiteID': this.selectedSite[i],
                    'EDistributorType': this.type,
                    'PrimaryD': 0
                  };
                  this.saveDistributor(data);
                }
              } else {
                var data = {
                  'CountryCode': countryTemp.countries_code,
                  'SiteID': this.selectedSite[i],
                  'EDistributorType': this.type,
                  'PrimaryD': 0
                };
                this.saveDistributor(data);
              }
            }
          } else {
            swal('Information!', 'CountryCode Not Found', 'error');
          }
        }, error => {
          this.service.errorserver();
        });
    } else {
      swal('Information!', 'There is no Site Selected', 'error');
    }
  }
  //Save Master Distributor
  // submitted: boolean;
  // onSubmit() {

  //     this.addMasterDistributor.controls['SiteName'].markAsTouched();

  //     if(this.addMasterDistributor.valid) {

  //     this.submitted = true;

  //     var data = '';
  //     this.service.httpClientGet("api/Countries2/" + this.selectedItemsCountry[0]["id"], data)
  //         .subscribe(value => {
  //         this.data = value;

  //         this.addMasterDistributor.value.CountryCode = this.data.CountryCode;
  //         this.addMasterDistributor.value.SiteID = this.siteID;
  //         this.addMasterDistributor.value.EDistributorType = "MED";
  //         this.addMasterDistributor.value.PrimaryD = 0;

  //         let dataSave = JSON.stringify(this.addMasterDistributor.value);

  //         this.service.post('api/SiteCountryDistributor', dataSave)
  //             .subscribe(result => {
  //             var updated = JSON.parse(result);
  //             if (updated["code"] == 1) {
  //                 this.service.openSuccessSwal(updated['message']);
  //                 this.data_1 = [];
  //                 this.data_2 = [];
  //                 this.getDistributor();
  //             }
  //             });
  //     });

  //     }
  // }

  onCurrGeoSelect(item: any) {
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
  }

  onCurrRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
  }

  onCurrSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
  }

  onGeoSelect(item: any) {
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  OnGeoDeSelect(item: any) {
    this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onGeoSelectAll(items: any) {
    this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    this.selectedItemsRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onGeoDeSelectAll(items: any) {
    this.selectedItemsRegion = [];
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListRegion = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
  }

  OnRegionDeSelect(item: any) {
    this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
  }

  onRegionSelectAll(items: any) {
    this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
  }

  onRegionDeSelectAll(items: any) {
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
  }

  onSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  OnSubRegionDeSelect(item: any) {
    this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  onSubRegionSelectAll(items: any) {
    this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    this.selectedItemsCountry = [];
  }

  onSubRegionDeSelectAll(items: any) {
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
  }

  onCountriesSelect(item: any) { }
  OnCountriesDeSelect(item: any) { }
  onCountriesSelectAll(item: any) { }
  onCountriesDeSelectAll(item: any) { }

  // getDistributor() {
  //     this.data_1 = [];
  //     this.data_2 = [];
  //     var data = '';
  //     this.service.httpClientGet("api/Countries2/" + this.selectedItemsCountry[0]["id"], data)
  //       .subscribe(value => {
  //         this.data = value;
  //         this.countryCode = this.data.CountryCode;
  //         this.getDataDistributor(this.data.CountryCode);
  //         this.getContact(this.data.CountryCode);
  //         this.getEtcVar(this.data.CountryCode, this.data.MarketTypeId);
  //         this.data.Embargoed == 0 ? this.embargoed = "No" : this.embargoed = "Yes";
  //         this.geo = this.selectedItemsGeo[0]["itemName"];
  //         this.region = this.selectedItemsRegion[0]["itemName"];
  //         this.subRegion = this.selectedItemsSubRegion[0]["itemName"];
  //         this.countryName = this.selectedItemsCountry[0]["itemName"];
  //         this.showAll = true;
  //       });
  // }

  // getEtcVar(country_code, market_id) {
  //     var market: any;
  //     var territoryVal: any;
  //     this.service.httpClientGet("api/MarketType/" + market_id, market)
  //       .subscribe(itemMarket => {
  //         market = itemMarket;
  //         market.length != 0 ? this.marketName = market.MarketType : this.marketName = "N/A";
  //         this.service.get("api/Territory/where/{'CountryCode':'" + country_code + "'}", territoryVal)
  //           .subscribe(itemTerritory => {
  //             territoryVal = JSON.parse(itemTerritory);
  //             territoryVal.length != 0 ? this.territory = territoryVal[0].Territory_Name : this.territory = "N/A";
  //           },
  //             error => {
  //               this.messageError = <any>error
  //               this.service.errorserver();
  //             })
  //       },
  //         error => {
  //           this.messageError = <any>error
  //           this.service.errorserver();
  //         });
  // }

  // getDataDistributor(country_code) {
  //     let dataTemp: any;
  //     this.service.httpClientGet("api/MainSite/getSiteCountry/{'CountryCode':'" + country_code + "'}", dataTemp)
  //       .subscribe(result => {
  //         dataTemp = result;
  //         dataTemp.forEach(item => {
  //           var exist = false;
  //           if (item.EDistributorType == "MED") {
  //             if (this.data_1.length == 0) {
  //               this.data_1.push(item);
  //             } else {
  //               this.data_1.forEach(val1 => {
  //                 if (item.SiteIdInt == val1.SiteIdInt) { exist = true; }
  //               })
  //               if (exist == false && item.SiteIdInt != "") {
  //                 this.data_1.push(item);
  //               }
  //             }
  //           } else {
  //             if (this.data_2.length == 0) {
  //               this.data_2.push(item);
  //             } else {
  //               this.data_2.forEach(val2 => {
  //                 if (item.SiteIdInt == val2.SiteIdInt) { exist = true; }
  //               })
  //               if (exist == false && item.SiteIdInt != "") {
  //                 this.data_2.push(item);
  //               }
  //             }
  //           }
  //         });
  //       },
  //         error => {
  //           this.service.errorserver();
  //         });
  // }

  // getContact(country_code) {
  //     this.data_kontak = [];
  //     var kontak: any;
  //     this.service.httpClientGet("api/MainContact/GetDistContact/where/{'CountryCode':'" + country_code + "'}", kontak)
  //       .subscribe(value => {
  //         kontak = value;
  //         if (kontak.length != 0) {
  //           for (let con = 0; con < kontak.length; con++) {
  //             if (kontak[con] != (undefined || null) && kontak[con].ContactIdInt != "") {
  //               this.data_kontak.push(kontak[con]);
  //             }
  //           }
  //         }
  //       },
  //         error => {
  //           this.service.errorserver();
  //         });
  // }

  // getCountry() {
  //     // this.countryCode = this.selectedItemsCountry[0]["id"];
  //     // this.countryName = this.selectedItemsCountry[0]["itemName"];

  //     var siteNames = '';
  //     this.service.httpClientGet("api/MainSite", siteNames)
  //       .subscribe(result => {
  //         this.siteNames = result;
  //       },
  //         error => {
  //           this.service.errorserver();
  //         });
  //     this.siteID = "";
  // }

  // getSiteContact() {
  //     var site: any;
  //     this.partnerType = [];
  //     this.service.httpClientGet("api/Roles", site)
  //       .subscribe(result => {
  //         site = result;
  //         if (site.length != 0) {
  //           for (let i = 0; i < site.length; i++) {
  //             this.partnerType.push(site[i]);
  //           }
  //         } else {
  //           swal(
  //             'Information!',
  //             'Partner Type Data Not Found',
  //             'error'
  //           );
  //         }
  //       },
  //         error => {
  //           this.service.errorserver();
  //         });
  //     this.siteID = "";
  // }

  // selectedSite(newvalue) {
  //     this.siteID = newvalue;
  // }

  // getSitebyCountry() {
  //     this.countryCode = this.selectedItemsCountry[0]["id"];
  //     var siteID = '';
  //     this.service.httpClientGet("api/SiteCountryDistributor/where/{'CountryCode':'" + this.countryCode + "'}", siteID)
  //         .subscribe(result => {
  //         this.siteID = result;
  //         },
  //         error => {
  //             this.service.errorserver();
  //         });
  // }

  // updateDistributorPrimary(site_id, primary, type, country_code) {
  //     var status: any;
  //     this.addMasterDistributor.value.SiteID = site_id;
  //     this.addMasterDistributor.value.CountryCode = country_code;
  //     this.addMasterDistributor.value.EDistributorType = type;
  //     if (primary == 'False') {
  //     status = 1;
  //     } else {
  //     status = 0;
  //     }
  //     this.addMasterDistributor.value.PrimaryD = status;

  //     let data = JSON.stringify(this.addMasterDistributor.value);

  //     this.service.put("api/SiteCountryDistributor/where/{'SiteID':'" + site_id + "','CountryCode':'" + country_code + "','EDistributorType':'" + type + "'}", data)
  //     .subscribe(result => {
  //         var update = JSON.parse(result);
  //         if (update["code"] == 1) {
  //         // if (this.data_1.length > 1 || this.data_2.length > 1 && status == 1) {
  //         //   if (type == "MED") {
  //         //     this.primaryAnotherSite(site_id, country_code, type, this.data_1);
  //         //   } else if (type == "RED") {
  //         //     this.primaryAnotherSite(site_id, country_code, type, this.data_2);
  //         //   }
  //         // }
  //         this.service.openSuccessSwal(update["message"]);
  //         this.data_1 = [];
  //         this.data_2 = [];
  //         this.getDistributor();
  //         } else {
  //         swal(
  //             'Information!',
  //             update["message"],
  //             'error'
  //         );
  //         }
  //     }, error => {
  //         this.messageError = <any>error
  //         this.service.errorserver();
  //     });
  // }

  // primaryAnotherSite(site_id, country_code, type, dataSource) {
  //     for (let i = 0; i < dataSource.length; i++) {
  //       if (dataSource[i].SiteId != site_id) {
  //         this.setPrimaryAnotherSite.value.SiteID = dataSource[i].SiteId;
  //         this.setPrimaryAnotherSite.value.CountryCode = dataSource[i].CountryCode;
  //         this.setPrimaryAnotherSite.value.EDistributorType = dataSource[i].EDistributorType;
  //         this.setPrimaryAnotherSite.value.PrimaryD = 0;

  //         let setAnotherSite = JSON.stringify(this.setPrimaryAnotherSite.value);
  //         this.service.put("api/SiteCountryDistributor/where/{'SiteID':'" + dataSource[i].SiteId + "','CountryCode':'" + country_code + "','EDistributorType':'" + type + "'}", setAnotherSite)
  //           .subscribe(hasil => {
  //             var updateResult = JSON.parse(hasil);
  //             if (updateResult["code"] != 1) {
  //               swal(
  //                 'Information!',
  //                 updateResult["message"],
  //                 'error'
  //               );
  //             }
  //           }, error => {
  //             this.messageError = <any>error
  //             this.service.errorserver();
  //           });
  //       }
  //     }
  //   }

  // openConfirmsMaster(id, index) {
  //     swal({
  //       title: 'Are you sure?',
  //       text: "You won't be able to revert this!",
  //       type: 'warning',
  //       showCancelButton: true,
  //       confirmButtonColor: '#3085d6',
  //       cancelButtonColor: '#d33',
  //       confirmButtonText: 'Yes, delete it!'
  //     })
  //       .then(result => {
  //         if (result == true) {
  //           var data = '';
  //           this.service.delete("api/MainSite/" + id, data)
  //             .subscribe(value => {
  //               var resource = JSON.parse(value);
  //               this.service.openSuccessSwal(resource['message']);
  //               var index = this.data_1.findIndex(x => x.SiteIdInt == id);
  //               if (index !== -1) {
  //                 this.data_1.splice(index, 1);
  //               }
  //             },
  //               error => {
  //                 this.messageError = <any>error
  //                 this.service.errorserver();
  //               });
  //         }

  //     }).catch(swal.noop);
  // }
}