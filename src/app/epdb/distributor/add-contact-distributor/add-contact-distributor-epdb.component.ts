import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Component({
  selector: 'app-add-contact-distributor-epdb',
  templateUrl: './add-contact-distributor-epdb.component.html',
  styleUrls: [
    './add-contact-distributor-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddContactDistributorEPDBComponent implements OnInit {

  private country;
  private type;
  private geo;
  private countryCode;
  addContactDistributor: FormGroup;
  partnerType = [];
  messageResult: string = '';
  messageError: string = '';
  public loading = false;

  public rowsOnPage1: number = 10;
  public filterQuery1: string = "";
  public sortBy1: string = "name";
  public sortOrder1: string = "desc";
  public showResult = false;

  constructor(private router: Router, private service: AppService, private filterGeo: AppFilterGeo, private formatdate: AppFormatDate, private route: ActivatedRoute) {
    let FirstName = new FormControl('');
    let LastName = new FormControl('');
    let PrimaryRoleId = new FormControl('');
    let EmailAddress = new FormControl('');
    let CountryCode = new FormControl('');

    this.addContactDistributor = new FormGroup({
      FirstName: FirstName,
      LastName: LastName,
      CountryCode: CountryCode,
      PrimaryRoleId: PrimaryRoleId,
      EmailAddress: EmailAddress
    });
  }

  getSiteContact() {
    // var site: any;
    // this.partnerType = [];
    // this.service.httpClientGet("api/Roles", site)
    //   .subscribe(result => {
    //     site = result;
    //     if (site.length != 0) {
    //       for (let i = 0; i < site.length; i++) {
    //         this.partnerType.push(site[i]);
    //       }
    //     } else {
    //       swal(
    //         'Information!',
    //         'Partner Type Data Not Found',
    //         'error'
    //       );
    //     }
    //   },
    //     error => {
    //       this.service.errorserver();
    //     });

    var data = '';
    this.service.httpClientGet('api/Roles/where/{"RoleType": "Company"}', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.partnerType = null;
        }
        else {
          let data : any = result;
          this.partnerType = data;
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  ngOnInit() {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
      this.country = params['countries'] || [];
      this.countryCode = params['country_code'] || "";
    });
    this.getSiteContact();
  }

  backToAddDistributor() {
    this.router.navigate(['/admin/distributor/add-distributor'], { queryParams: { countries: this.country } });
  }

  resetForm() {
    this.addContactDistributor.reset({
      'FirstName': '',
      'LastName': '',
      'PrimaryRoleId': '',
      'EmailAddress': ''
    });
  }

  datacontact: any;
  onSubmitContact() {
    this.loading = true;
    let data : any;
    this.service.httpClientGet(
      "api/MainContact/where/{'FirstName': '" + this.addContactDistributor.value.FirstName +
      "','LastName': '" + this.addContactDistributor.value.LastName +
      "','EmailAddress': '" + this.addContactDistributor.value.EmailAddress +
      "','PartnerType': '" + this.addContactDistributor.value.PrimaryRoleId +
      "','ContryDistributor': '" + this.countryCode + "'}", data)
      .subscribe(result => {
        //   try {
        //     if (result == "Not found") {
        //       this.service.notfound();
        //       this.datacontact = '';
        //       this.loading = false;
        //     }
        //     else {
        //       this.datacontact = JSON.parse(result);
        //       // console.log(this.datacontact);
        //       this.loading = false;
        //     }
        //   }
        //   catch (err) {
        //     this.service.errorserver();
        //     this.datacontact = '';
        //     this.loading = false;
        //   }
        // },
        //   error => {
        //     this.service.errorserver();
        //     this.datacontact = '';
        //     this.loading = false;
        //   });
        data = result;
        if (data == 'Not found') {
          this.datacontact = '';
        } else {
          //data = data.replace(/\t|\n|\r|\f|\\|\/|'/g, " ");
          this.datacontact = data;
        }
        this.showResult = true;
        this.loading = false;
      });
  }

  addcontactdistributor(contactid) {
    this.loading = true;
    let data =
      {
        "CountryCode": this.countryCode,
        "ContactId": contactid
      };
    this.service.httpClientPost("api/MainContact/ContactDistributor", data)
      .subscribe(result => {
        var hasil = result;
        if (hasil["code"] == "1") {
          this.backToAddDistributor();
          this.loading = false;
        }
        else {
          this.service.notfound();
          this.loading = false;
        }
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
          this.loading = false;
        });
  }
}