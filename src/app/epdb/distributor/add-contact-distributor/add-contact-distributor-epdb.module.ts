import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactDistributorEPDBComponent } from './add-contact-distributor-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

import { LoadingModule } from 'ngx-loading';

export const AddContactDistributorEPDBRoutes: Routes = [
  {
    path: '',
    component: AddContactDistributorEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.distributor.add_contact_distributor',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddContactDistributorEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [AddContactDistributorEPDBComponent],
  providers: [AppService, AppFilterGeo, AppFormatDate]
})
export class AddContactDistributorEPDBModule { }