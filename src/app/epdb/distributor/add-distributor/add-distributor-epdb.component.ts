import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { Location } from '@angular/common';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../../shared/service/session.service';

@Pipe({ name: 'dataFilterContact' })
export class DataFilterContactPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.ContactId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.InstructorId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.FirstName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.LastName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.EmailAddress.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.RoleName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Affiliate.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'dataFilterMasterDistributor' })
export class DataFilterMasterDistributorPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Roles.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Pipe({ name: 'dataFilterRegionalDistributor' })
export class DataFilterRegionalDistributorPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Roles.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-add-distributor-epdb',
  templateUrl: './add-distributor-epdb.component.html',
  styleUrls: [
    './add-distributor-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddDistributorEPDBComponent implements OnInit {
  dropdownListGeo = [];
  selectedItemsGeo = [];
  dropdownListRegion = [];
  selectedItemsRegion = [];
  dropdownListSubRegion = [];
  selectedItemsSubRegion = [];
  dropdownListCountry = [];
  selectedItemsCountry = [];
  dropdownSettings = {};
  dropdownCountrySettings = {};

  data_1 = [];
  data_2 = [];
  data_kontak = [];
  partnerType = [];

  private _serviceUrl = 'api/MainSite';
  messageResult: string = '';
  messageError: string = '';
  adddistributor: FormGroup;
  public distributor: any;
  public data;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortByMaster: string = "SiteName";
  public sortByRegional: string = "SiteName";
  public sortByContact: string = "FirstName";
  public sortOrder: string = "asc";
  public geo: string = "";
  public region: string = "";
  public subRegion: string = "";
  public countryName: string = "";
  private countryCode;
  public siteID: any;
  public sites: any;
  public siteNames: any;
  public kontak: any;
  public showAll = false;
  public dataSite: any;
  public masterPrimary;
  public regionalPrimary;
  public marketName: string = "";
  public embargoed: string = "";
  public territory: string = "";
  private country;
  addMasterDistributor: FormGroup;
  addRegionalDistributor: FormGroup;
  addContactDistributor: FormGroup;
  setPrimaryAnotherSite: FormGroup;
  updateContact: FormGroup;
  public validCountry = false;
  public loading = false;

  constructor(public session: SessionService, private router: Router, private service: AppService, private filterGeo: AppFilterGeo, private formatdate: AppFormatDate, private route: ActivatedRoute, private location: Location) {
    let SiteID = new FormControl('');
    let SiteName_Master = new FormControl('', Validators.required);
    let SiteName_Regional = new FormControl('', Validators.required);
    let CountryCode = new FormControl('');
    let EDistributorType = new FormControl('');
    let PrimaryD = new FormControl('');

    this.addMasterDistributor = new FormGroup({
      SiteName: SiteName_Master,
      SiteID: SiteID,
      CountryCode: CountryCode,
      EDistributorType: EDistributorType,
      PrimaryD: PrimaryD
    });

    this.addRegionalDistributor = new FormGroup({
      SiteName: SiteName_Regional,
      SiteID: SiteID,
      CountryCode: CountryCode,
      EDistributorType: EDistributorType,
      PrimaryD: PrimaryD
    });

    this.setPrimaryAnotherSite = new FormGroup({
      SiteID: SiteID,
      CountryCode: CountryCode,
      EDistributorType: EDistributorType,
      PrimaryD: PrimaryD
    });

    let FirstName = new FormControl('', Validators.required);
    let LastName = new FormControl('');
    let PrimaryRoleId = new FormControl('', Validators.required);
    let EmailAddress = new FormControl('', Validators.required);

    this.addContactDistributor = new FormGroup({
      FirstName: FirstName,
      LastName: LastName,
      CountryCode: CountryCode,
      PrimaryRoleId: PrimaryRoleId,
      EmailAddress: EmailAddress
    });

    let ContactId = new FormControl('');
    this.updateContact = new FormGroup({
      ContactId: ContactId,
      CountryCode: CountryCode,
      PrimaryD: PrimaryD
    });
  }

  setDropdownListGeo(data) {
    var exist = false;
    var Geo = {
      'id': data.geo_id,
      'itemName': data.geo_name
    };
    if (this.selectedItemsGeo.length == 0) {
      this.selectedItemsGeo.push(Geo);
      this.onCurrGeoSelect(this.selectedItemsGeo[0]);
    } else {
      for (let n = 0; n < this.selectedItemsGeo.length; n++) {
        if (data.geo_id == this.selectedItemsGeo[n].id) {
          exist = true;
        }
        this.onCurrGeoSelect(this.selectedItemsGeo[n]);
      }
      if (exist == false) {
        this.selectedItemsGeo.push(Geo);
      }
    }
  }

  setDropdownListRegion(data) {
    var exist = false;
    var Region = {
      'id': data.region_id,
      'itemName': data.region_name
    };
    if (this.selectedItemsRegion.length == 0) {
      this.selectedItemsRegion.push(Region);
      this.onCurrRegionSelect(this.selectedItemsRegion[0]);
    } else {
      for (let l = 0; l < this.selectedItemsRegion.length; l++) {
        if (data.region_id == this.selectedItemsRegion[l].id) {
          exist = true;
        }
        this.onCurrRegionSelect(this.selectedItemsRegion[l]);
      }
      if (exist == false) {
        this.selectedItemsRegion.push(Region);
      }
    }
  }

  setDropdownListSubRegion(data) {
    var exist = false;
    var subRegion = {
      'id': data.subregion_id,
      'itemName': data.subregion_name
    };
    if (this.selectedItemsSubRegion.length == 0) {
      this.selectedItemsSubRegion.push(subRegion);
      this.onCurrSubRegionSelect(this.selectedItemsSubRegion[0]);
    } else {
      for (let j = 0; j < this.selectedItemsSubRegion.length; j++) {
        if (data.subregion_id == this.selectedItemsSubRegion[j].id) {
          exist = true;
        }
        this.onCurrSubRegionSelect(this.selectedItemsSubRegion[j]);
      }
      if (exist == false) {
        this.selectedItemsSubRegion.push(subRegion);
      }
    }
  }

  // getGeo() {
  //   var data = '';
  //   this.service.httpClientGet("api/Geo", data)
  //     .subscribe(result => {
  //       this.data = result;
  //       this.dropdownListGeo = this.data.map((item) => {
  //         return {
  //           id: item.geo_id,
  //           itemName: item.geo_name
  //         }

  //       });
  //       this.selectedItemsGeo = this.data.map((item) => {
  //         return {
  //           id: item.geo_id,
  //           itemName: item.geo_name
  //         }

  //       })
  //     },
  //       error => {
  //         this.service.errorserver();
  //       });
  //   this.selectedItemsGeo = [];

  // }

  GetDataIntoRegion() {
    var data = '';
    this.service.httpClientGet("api/Region", data)
      .subscribe(result => {
        this.data = result;
        this.dropdownListRegion = this.data.map((item) => {
          return {
            id: item.region_id,
            itemName: item.region_name
          }

        });
      },
        error => {
          this.service.errorserver();
        });
    this.selectedItemsRegion = [];
  }


  GetDataIntoCountry() {
    console.log('result')
    var data = '';
    this.service.httpClientGet("api/Countries/GetAllCountries", data)
      .subscribe(result => {
        this.data = result;
        console.log(result)
        this.dropdownListCountry = this.data.map((item) => {
          return {
            id: item.countries_id,
            itemName: item.countries_name
          }

        });



        var dataCountry = {
          id: this.data[0].countries_id,
          ItemName: this.data[0].countries_name
        };
        this.selectedItemsCountry.push(dataCountry);


      },
        error => {
          this.service.errorserver();
        });

  }



  setDropdownListCountry(country) {
    var countryTemp = JSON.parse(country);
    console.log(countryTemp);
    var dataCountry = {
      id: countryTemp[0].id,
      itemName: countryTemp[0].itemName
    };
    this.selectedItemsCountry.push(dataCountry);
  }

  getCountryLinks(args) {
    var temp = JSON.parse(args);
    var data: any;
    this.service.httpClientGet("api/Countries/" + temp[0].id, data)
      .subscribe(res => {
        data = res;
        this.setDropdownListGeo(data);
        // this.setDropdownListRegion(data);
        this.setDropdownListSubRegion(data);
      }, error => {
        this.service.errorserver();
      });
  }

  accesAddBtnMaster: Boolean = true;
  accesSetBtnMaster: Boolean = true;
  accesDeleteBtnMaster: Boolean = true;

  accesAddBtnRegional: Boolean = true;
  accesSetBtnRegional: Boolean = true;
  accesDeleteBtnRegional: Boolean = true;

  accesAddBtnContact: Boolean = true;
  accesSetBtnContact: Boolean = true;
  accesDeleteBtnContact: Boolean = true;

  // allregion = [];
  // allsubregion = [];
  allcountries = [];
  ngOnInit() {
    // var sub: any;
    // sub = this.route.queryParams.subscribe(params => {
    //   this.country = params['countries'] || [];
    // });

    // if (this.country.length != 0) {
    //   this.getCountryLinks(this.country);
    //   this.setDropdownListCountry(this.country);
    //   this.getDistributor();
    // }

    //Untuk set filter terakhir hasil pencarian
    if (!(localStorage.getItem("filter") === null)) {
      var item = JSON.parse(localStorage.getItem("filter"));
      this.selectedItemsGeo = item.selectGeo;
      this.dropdownListGeo = item.dropGeo;
      this.selectedItemsCountry = item.selectCountry;
      this.dropdownListCountry = item.dropCountry;
      this.getDistributor();
    }
    else {
      // this.getGeo();
      this.GetDataIntoCountry();
  
      this.loading = true;
      var data = '';
      // Geo
      data = '';
      this.service.httpClientGet("api/Geo", data)
        .subscribe(result => {
          if (result == "Not found") {
            this.service.notfound();
            this.dropdownListGeo = null;
            this.loading = false;
          }
          else {
            this.dropdownListGeo = Object.keys(result).map(function (Index) {
              return {
                id: result[Index].geo_code,
                itemName: result[Index].geo_name
              }
            })
            this.selectedItemsGeo = Object.keys(result).map(function (Index) {
              return {
                id: result[Index].geo_code,
                itemName: result[Index].geo_name
              }
            })
            this.loading = false;
          }
        },
          error => {
            this.service.errorserver();
            this.loading = false;
          });
      this.selectedItemsGeo = [];
  
      // Countries
      data = '';
      this.service.httpClientGet("api/Countries", data)
        .subscribe(result => {
          if (result == "Not found") {
            this.service.notfound();
            this.dropdownListCountry = null;
            this.loading = false;
          }
          else {
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
              return {
                id: result[Index].countries_id,
                itemName: result[Index].countries_name
              }
            })
            this.loading = false;
          }
          this.allcountries = this.dropdownListCountry;
        },
          error => {
            this.service.errorserver();
            this.loading = false;
          });
    }


    // this.selectedItemsCountry = [];

    //Hide this code for a while
    // Region
    // var data = '';
    // this.service.get("api/Region",data)
    // .subscribe(result => { 
    //     if(result=="Not found"){
    //         this.service.notfound();
    //         this.dropdownListRegion = null;
    //     }
    //     else{
    //         this.dropdownListRegion = JSON.parse(result).map((item) => {
    //             return {
    //               id: item.region_code,
    //               itemName: item.region_name
    //             }
    //           })
    //     } 
    //     this.allregion = this.dropdownListRegion;
    // },
    // error => {
    //     this.service.errorserver();
    // });
    // this.selectedItemsRegion = [];

    // Sub Region
    // data = '';
    // this.service.get("api/SubRegion",data)
    // .subscribe(result => { 
    //     if(result=="Not found"){
    //         this.service.notfound();
    //         this.dropdownListSubRegion = null;
    //     }
    //     else{
    //         this.dropdownListSubRegion = JSON.parse(result).map((item) => {
    //             return {
    //               id: item.subregion_code,
    //               itemName: item.subregion_name
    //             }
    //           })
    //     } 
    //     this.allsubregion = this.dropdownListSubRegion;
    // },
    // error => {
    //     this.service.errorserver();
    // });
    // this.selectedItemsSubRegion = [];

    // Countries
    // data = '';
    // this.service.get("api/Countries",data)
    // .subscribe(result => { 
    //     if(result=="Not found"){
    //         this.service.notfound();
    //         this.dropdownListCountry = null;
    //     }
    //     else{
    //         this.dropdownListCountry = JSON.parse(result).map((item) => {
    //             return {
    //               id: item.countries_code,
    //               itemName: item.countries_name
    //             }
    //           })
    //     } 
    //     this.allcountries = this.dropdownListCountry;
    // },
    // error => {
    //     this.service.errorserver();
    // });
    // this.selectedItemsCountry = [];
    //---End of Hide this code for a while


    //setting dropdown for Geo, Region, SubRegion
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };

    //setting dropdown for Country
    this.dropdownCountrySettings = {
      singleSelection: true,
      text: "Please Select",
      enableSearchFilter: true,
      enableCheckAll: false,
      // limitSelection: 1,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true
    };

    this.accesAddBtnMaster = this.session.checkAccessButton("admin/distributor/add-master-distributor");
    this.accesSetBtnMaster = this.session.checkAccessButton("admin/distributor/set-master-distributor");
    this.accesDeleteBtnMaster = this.session.checkAccessButton("admin/distributor/delete-master-distributor");

    this.accesAddBtnRegional = this.session.checkAccessButton("admin/distributor/add-master-distributor");
    this.accesSetBtnRegional = this.session.checkAccessButton("admin/distributor/set-regional-distributor");
    this.accesDeleteBtnRegional = this.session.checkAccessButton("admin/distributor/delete-regional-distributor");

    this.accesAddBtnContact = this.session.checkAccessButton("admin/distributor/add-contact-distributor");
    this.accesSetBtnContact = this.session.checkAccessButton("admin/distributor/set-contact-distributor");
    this.accesDeleteBtnContact = this.session.checkAccessButton("admin/distributor/delete-contact-distributor");

    this.GetDataIntoRegion();

  }

  onCurrGeoSelect(item: any) {
    this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
  }

  onCurrRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
  }

  onCurrSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
  }

  //Save Master Distributor
  // submitted: boolean;
  // onSubmit() {

  //   this.addMasterDistributor.controls['SiteName'].markAsTouched();

  //   if (this.addMasterDistributor.valid) {

  //     this.submitted = true;

  //     var data = '';
  //     this.service.httpClientGet("api/Countries2/" + this.selectedItemsCountry[0]["id"], data)
  //       .subscribe(value => {
  //         this.data = value;

  //         this.addMasterDistributor.value.CountryCode = this.data.CountryCode;
  //         this.addMasterDistributor.value.SiteID = this.siteID;
  //         this.addMasterDistributor.value.EDistributorType = "MED";
  //         this.addMasterDistributor.value.PrimaryD = 0;

  //         let dataSave = JSON.stringify(this.addMasterDistributor.value);

  //         this.service.post('api/SiteCountryDistributor', dataSave)
  //           .subscribe(result => {
  //             var updated = JSON.parse(result);
  //             if (updated["code"] == 1) {
  //               this.service.openSuccessSwal(updated['message']);
  //               this.data_1 = [];
  //               this.data_2 = [];
  //               this.getDistributor();
  //             }
  //           });
  //       });
  //   }
  // }

  //Save Regional Distributor
  // submitted1: boolean;
  // onSubmitRegional() {

  //   this.addRegionalDistributor.controls['SiteName'].markAsTouched();

  //   if (this.addRegionalDistributor.valid) {

  //     this.submitted1 = true;
  //     var data = '';
  //     this.service.httpClientGet("api/Countries2/" + this.selectedItemsCountry[0]["id"], data)
  //       .subscribe(value => {
  //         this.data = value;

  //         this.addRegionalDistributor.value.CountryCode = this.data.CountryCode;
  //         this.addRegionalDistributor.value.SiteID = this.siteID;
  //         this.addRegionalDistributor.value.EDistributorType = "RED";
  //         this.addRegionalDistributor.value.PrimaryD = 0;

  //         let dataSave = JSON.stringify(this.addRegionalDistributor.value);

  //         this.service.post('api/SiteCountryDistributor', dataSave)
  //           .subscribe(result => {
  //             var updated = JSON.parse(result);
  //             if (updated["code"] == 1) {
  //               this.service.openSuccessSwal(updated['message']);
  //               this.data_1 = [];
  //               this.data_2 = [];
  //               this.getDistributor();
  //             }
  //           });
  //       });
  //   }
  // }
  arraygeoid = [];
  onGeoSelect(item: any) {
    // this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
    // this.selectedItemsRegion = [];
    // this.dropdownListSubRegion = [];
    // this.dropdownListCountry = [];
    var tmpGeo = "''"
    this.arraygeoid = [];
    if (this.selectedItemsGeo.length > 0) {
      for (var i = 0; i < this.selectedItemsGeo.length; i++) {
        this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
      }
      tmpGeo = this.arraygeoid.toString()
    }

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filter/" + tmpGeo, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListCountry = null;
        }
        else {
          this.dropdownListCountry = Object.keys(result).map(function (Index) {
            return {
              id: result[Index].countries_id,
              itemName: result[Index].countries_name
            }
          })
        }
      },
        error => {
          this.service.errorserver();
        });
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  OnGeoDeSelect(item: any) {
    // this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
    // this.selectedItemsRegion = [];
    // this.selectedItemsSubRegion = [];
    // this.selectedItemsCountry = [];
    // this.dropdownListSubRegion = [];
    // this.dropdownListCountry = [];
    //split countries
    let countriesArrTmp :any;
    this.service.httpClientGet("api/Countries/filter/'" + item.id + "'", data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          countriesArrTmp = null;
        }
        else {
          countriesArrTmp = result;
          for (var i = 0; i < countriesArrTmp.length; i++) {
            var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_id);
            if (index !== -1) {
              this.selectedItemsCountry.splice(index, 1);
            }
          }
        }
      },
        error => {
          this.service.errorserver();
        });

    var index = this.arraygeoid.findIndex(x => x == '"' + item.id + '"');
    this.arraygeoid.splice(index, 1);
    // this.selectedItemsRegion.splice(index, 1);
    // this.selectedItemsSubRegion.splice(index, 1);
    this.selectedItemsCountry.splice(index, 1);
    if (this.selectedItemsGeo.length > 0) {
      var tmpGeo = "''"
      this.arraygeoid = [];
      if (this.selectedItemsGeo.length > 0) {
        for (var i = 0; i < this.selectedItemsGeo.length; i++) {
          this.arraygeoid.push('"' + this.selectedItemsGeo[i].id + '"');
        }
        tmpGeo = this.arraygeoid.toString()
      }

      // Countries
      var data = '';
      this.service.httpClientGet("api/Countries/filter/" + tmpGeo, data)
        .subscribe(result => {
          if (result == "Not found") {
            this.service.notfound();
            this.dropdownListCountry = null;
          }
          else {
            // this.dropdownListCountry = JSON.parse(result).map((item) => {
            //   return {
            //     id: item.countries_id,
            //     itemName: item.countries_name
            //   }
            // })
            this.dropdownListCountry = Object.keys(result).map(function (Index) {
              return {
                id: result[Index].countries_id,
                itemName: result[Index].countries_name
              }
            })

          }
        },
          error => {
            this.service.errorserver();
          });
    }
    else {
      this.dropdownListCountry = this.allcountries;

      this.selectedItemsCountry.splice(index, 1);
    }
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onGeoSelectAll(items: any) {
    // this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo, this.dropdownListRegion);
    // this.selectedItemsRegion = [];
    // this.dropdownListSubRegion = [];
    // this.dropdownListCountry = [];
    this.arraygeoid = [];
    for (var i = 0; i < items.length; i++) {
      this.arraygeoid.push('"' + items[i].id + '"');
    }
    var tmpGeo = this.arraygeoid.toString();

    // Countries
    var data = '';
    this.service.httpClientGet("api/Countries/filter/" + tmpGeo, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListCountry = null;
        }
        else {
          this.dropdownListCountry = Object.keys(result).map(function (Index) {
            return {
              id: result[Index].countries_id,
              itemName: result[Index].countries_name
            }
          })
          console.log(this.dropdownListCountry)
        }
      },
        error => {
          this.service.errorserver();
        });
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onGeoDeSelectAll(items: any) {
    // this.selectedItemsRegion = [];
    // this.selectedItemsSubRegion = [];
    // this.selectedItemsCountry = [];
    // this.dropdownListRegion = [];
    // this.dropdownListSubRegion = [];
    // this.dropdownListCountry = [];
    // this.dropdownListCountry = this.allcountries;

    this.selectedItemsCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onRegionSelect(item: any) {
    this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  OnRegionDeSelect(item: any) {
    this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onRegionSelectAll(items: any) {
    this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
    this.selectedItemsSubRegion = [];
    this.dropdownListCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onRegionDeSelectAll(items: any) {
    this.selectedItemsSubRegion = [];
    this.selectedItemsCountry = [];
    this.dropdownListSubRegion = [];
    this.dropdownListCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onSubRegionSelect(item: any) {
    this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  OnSubRegionDeSelect(item: any) {
    this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onSubRegionSelectAll(items: any) {
    this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
    this.selectedItemsCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onSubRegionDeSelectAll(items: any) {
    this.selectedItemsCountry = [];
    this.dropdownListCountry = [];
    this.country = [];
    this.location.go('/admin/distributor/add-distributor');
    this.showAll = false;
  }

  onCountriesSelect(item: any) {
    this.validCountry = false;
  }
  OnCountriesDeSelect(item: any) {
    // if (this.selectedItemsCountry.length == 0) { this.validCountry = true; }
  }


  //Hide this code for a while
  // arraygeoid=[];
  // onGeoSelect(item:any){ 
  //   this.arraygeoid.push('"'+item.id+'"');

  //   // Region
  //   var data = '';
  //   this.service.get("api/Region/filter/"+this.arraygeoid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListRegion = null;
  //       }
  //       else{
  //           this.dropdownListRegion = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.region_code,
  //                 itemName: item.region_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   // SubRegion
  //   var data = '';
  //   this.service.get("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListSubRegion = null;
  //       }
  //       else{
  //           this.dropdownListSubRegion = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.subregion_code,
  //                 itemName: item.subregion_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   // Countries
  //   var data = '';
  //   this.service.get("api/Countries/filter/"+this.arraygeoid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListCountry = null;
  //       }
  //       else{
  //           this.dropdownListCountry = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.countries_code,
  //                 itemName: item.countries_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });


  // }

  // OnGeoDeSelect(item:any){
  //   var data = '';

  //   //split region
  //   let regionArrTmp = [];
  //   this.service.get("api/Region/filter/'"+item.id+"'",data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           regionArrTmp = null;
  //       }
  //       else{
  //           regionArrTmp = JSON.parse(result);
  //           for(var i=0;i<regionArrTmp.length;i++){
  //               var index = this.selectedItemsRegion.findIndex(x => x.id == regionArrTmp[i].region_code);
  //               if(index !== -1){
  //                   this.selectedItemsRegion.splice(index,1);
  //               }
  //           }
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   //split sub region
  //   let subregionArrTmp = [];
  //   this.service.get("api/SubRegion/filter/'"+item.id+"'",data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           subregionArrTmp = null;
  //       }
  //       else{
  //           subregionArrTmp = JSON.parse(result);
  //           for(var i=0;i<subregionArrTmp.length;i++){
  //               var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
  //               if(index !== -1){
  //                   this.selectedItemsSubRegion.splice(index,1);
  //               }
  //           }
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   //split countries
  //   let countriesArrTmp = [];
  //   this.service.get("api/Countries/filter/'"+item.id+"'",data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           countriesArrTmp = null;
  //       }
  //       else{
  //           countriesArrTmp = JSON.parse(result);
  //           for(var i=0;i<countriesArrTmp.length;i++){
  //               var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
  //               if(index !== -1){
  //                   this.selectedItemsCountry.splice(index,1);
  //               }
  //           }
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   var index = this.arraygeoid.findIndex(x => x == '"'+item.id+'"');
  //   this.arraygeoid.splice(index,1);
  //   this.selectedItemsRegion.splice(index,1);
  //   this.selectedItemsSubRegion.splice(index,1);
  //   this.selectedItemsCountry.splice(index,1);

  //   if(this.arraygeoid.length > 0){
  //     // Region
  //     var data = '';
  //     this.service.get("api/Region/filter/"+this.arraygeoid.toString(),data)
  //     .subscribe(result => { 
  //         if(result=="Not found"){
  //             this.service.notfound();
  //             this.dropdownListRegion = null;
  //         }
  //         else{
  //             this.dropdownListRegion = JSON.parse(result).map((item) => {
  //                 return {
  //                   id: item.region_code,
  //                   itemName: item.region_name
  //                 }
  //               })
  //         } 
  //     },
  //     error => {
  //         this.service.errorserver();
  //     });

  //     // SubRegion
  //     var data = '';
  //     this.service.get("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
  //     .subscribe(result => { 
  //         if(result=="Not found"){
  //             this.service.notfound();
  //             this.dropdownListSubRegion = null;
  //         }
  //         else{
  //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
  //                 return {
  //                   id: item.subregion_code,
  //                   itemName: item.subregion_name
  //                 }
  //               })
  //         } 
  //     },
  //     error => {
  //         this.service.errorserver();
  //     });

  //     // Countries
  //     var data = '';
  //     this.service.get("api/Countries/filter/"+this.arraygeoid.toString(),data)
  //     .subscribe(result => { 
  //         if(result=="Not found"){
  //             this.service.notfound();
  //             this.dropdownListCountry = null;
  //         }
  //         else{
  //             this.dropdownListCountry = JSON.parse(result).map((item) => {
  //                 return {
  //                   id: item.countries_code,
  //                   itemName: item.countries_name
  //                 }
  //               })
  //         } 
  //     },
  //     error => {
  //         this.service.errorserver();
  //     });
  //   }
  //   else{
  //       this.dropdownListRegion = this.allregion;
  //       this.dropdownListSubRegion = this.allsubregion;
  //       this.dropdownListCountry = this.allcountries;

  //       this.selectedItemsRegion.splice(index,1);
  //       this.selectedItemsSubRegion.splice(index,1);
  //       this.selectedItemsCountry.splice(index,1);
  //   }
  // }

  // onGeoSelectAll(items: any){
  //   this.arraygeoid = [];

  //   for(var i=0;i<items.length;i++){
  //     this.arraygeoid.push('"'+items[i].id+'"');
  //   }

  //   // Region
  //   var data = '';
  //   this.service.get("api/Region/filter/"+this.arraygeoid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListRegion = null;
  //       }
  //       else{
  //           this.dropdownListRegion = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.region_code,
  //                 itemName: item.region_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   // SubRegion
  //   var data = '';
  //   this.service.get("api/SubRegion/filter/"+this.arraygeoid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListSubRegion = null;
  //       }
  //       else{
  //           this.dropdownListSubRegion = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.subregion_code,
  //                 itemName: item.subregion_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   // Countries
  //   var data = '';
  //   this.service.get("api/Countries/filter/"+this.arraygeoid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListCountry = null;
  //       }
  //       else{
  //           this.dropdownListCountry = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.countries_code,
  //                 itemName: item.countries_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // onGeoDeSelectAll(items: any){
  //   this.dropdownListRegion = this.allregion;
  //   this.dropdownListSubRegion = this.allsubregion;
  //   this.dropdownListCountry = this.allcountries;

  //   this.selectedItemsRegion = [];
  //   this.selectedItemsSubRegion = [];
  //   this.selectedItemsCountry = [];
  // }

  // arrayregionid=[];

  // onRegionSelect(item:any){
  //   this.arrayregionid.push('"'+item.id+'"');

  //   // SubRegion
  //   var data = '';
  //   this.service.get("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListSubRegion = null;
  //       }
  //       else{
  //           this.dropdownListSubRegion = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.subregion_code,
  //                 itemName: item.subregion_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   // Countries
  //   var data = '';
  //   this.service.get("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListCountry = null;
  //       }
  //       else{
  //           this.dropdownListCountry = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.countries_code,
  //                 itemName: item.countries_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // OnRegionDeSelect(item:any){

  //   //split sub region
  //   let subregionArrTmp = [];
  //   this.service.get("api/SubRegion/filterregion/'"+item.id+"'",data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           subregionArrTmp = null;
  //       }
  //       else{
  //           subregionArrTmp = JSON.parse(result);
  //           for(var i=0;i<subregionArrTmp.length;i++){
  //               var index = this.selectedItemsSubRegion.findIndex(x => x.id == subregionArrTmp[i].subregion_code);
  //               if(index !== -1){
  //                   this.selectedItemsSubRegion.splice(index,1);
  //               }
  //           }
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   //split countries
  //   let countriesArrTmp = [];
  //   this.service.get("api/Countries/filterregion/'"+item.id+"'",data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           countriesArrTmp = null;
  //       }
  //       else{
  //           countriesArrTmp = JSON.parse(result);
  //           for(var i=0;i<countriesArrTmp.length;i++){
  //               var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
  //               if(index !== -1){
  //                   this.selectedItemsCountry.splice(index,1);
  //               }
  //           }
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   var index = this.arrayregionid.findIndex(x => x == '"'+item.id+'"');
  //   this.arrayregionid.splice(index,1);
  //   this.selectedItemsSubRegion.splice(index,1);
  //   this.selectedItemsCountry.splice(index,1);

  //   if(this.arrayregionid.length > 0){
  //     // SubRegion
  //     var data = '';
  //     this.service.get("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
  //     .subscribe(result => { 
  //         if(result=="Not found"){
  //             this.service.notfound();
  //             this.dropdownListSubRegion = null;
  //         }
  //         else{
  //             this.dropdownListSubRegion = JSON.parse(result).map((item) => {
  //                 return {
  //                   id: item.subregion_code,
  //                   itemName: item.subregion_name
  //                 }
  //               })
  //         } 
  //     },
  //     error => {
  //         this.service.errorserver();
  //     });

  //     // Countries
  //     var data = '';
  //     this.service.get("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
  //     .subscribe(result => { 
  //         if(result=="Not found"){
  //             this.service.notfound();
  //             this.dropdownListCountry = null;
  //         }
  //         else{
  //             this.dropdownListCountry = JSON.parse(result).map((item) => {
  //                 return {
  //                   id: item.countries_code,
  //                   itemName: item.countries_name
  //                 }
  //               })
  //         } 
  //     },
  //     error => {
  //         this.service.errorserver();
  //     });
  //   }
  //   else{
  //       this.dropdownListSubRegion = this.allsubregion;
  //       this.dropdownListCountry = this.allcountries;

  //       this.selectedItemsSubRegion.splice(index,1);
  //       this.selectedItemsCountry.splice(index,1);
  //   }
  // }
  // onRegionSelectAll(items: any){
  //   this.arrayregionid = [];

  //   for(var i=0;i<items.length;i++){
  //     this.arrayregionid.push('"'+items[i].id+'"');
  //   }

  //   // SubRegion
  //   var data = '';
  //   this.service.get("api/SubRegion/filterregion/"+this.arrayregionid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListSubRegion = null;
  //       }
  //       else{
  //           this.dropdownListSubRegion = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.subregion_code,
  //                 itemName: item.subregion_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   // Countries
  //   var data = '';
  //   this.service.get("api/Countries/filterregion/"+this.arrayregionid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListCountry = null;
  //       }
  //       else{
  //           this.dropdownListCountry = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.countries_code,
  //                 itemName: item.countries_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // onRegionDeSelectAll(items: any){
  //   this.dropdownListSubRegion = this.allsubregion;
  //   this.dropdownListCountry = this.allcountries;

  //   this.selectedItemsSubRegion = [];
  //   this.selectedItemsCountry = [];
  // }

  // arraysubregionid=[];

  // onSubRegionSelect(item:any){
  //   this.arraysubregionid.push('"'+item.id+'"');

  //   // Countries
  //   var data = '';
  //   this.service.get("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListCountry = null;
  //       }
  //       else{
  //           this.dropdownListCountry = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.countries_code,
  //                 itemName: item.countries_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }

  // OnSubRegionDeSelect(item:any){

  //   //split countries
  //   let countriesArrTmp = [];
  //   this.service.get("api/Countries/filtersubregion/'"+item.id+"'",data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           countriesArrTmp = null;
  //       }
  //       else{
  //           countriesArrTmp = JSON.parse(result);
  //           for(var i=0;i<countriesArrTmp.length;i++){
  //               var index = this.selectedItemsCountry.findIndex(x => x.id == countriesArrTmp[i].countries_code);
  //               if(index !== -1){
  //                   this.selectedItemsCountry.splice(index,1);
  //               }
  //           }
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });

  //   var index = this.arraysubregionid.findIndex(x => x == '"'+item.id+'"');
  //   this.arraysubregionid.splice(index,1);
  //   this.selectedItemsCountry.splice(index,1);

  //   if(this.arraysubregionid.length > 0){
  //     // Countries
  //     var data = '';
  //     this.service.get("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
  //     .subscribe(result => { 
  //         if(result=="Not found"){
  //             this.service.notfound();
  //             this.dropdownListCountry = null;
  //         }
  //         else{
  //             this.dropdownListCountry = JSON.parse(result).map((item) => {
  //                 return {
  //                   id: item.countries_code,
  //                   itemName: item.countries_name
  //                 }
  //               })
  //         } 
  //     },
  //     error => {
  //         this.service.errorserver();
  //     });
  //   }
  //   else{
  //       this.dropdownListCountry = this.allcountries;

  //       this.selectedItemsCountry.splice(index,1);
  //   }
  // }
  // onSubRegionSelectAll(items: any){
  //   this.arraysubregionid = [];

  //   for(var i=0;i<items.length;i++){
  //     this.arraysubregionid.push('"'+items[i].id+'"');
  //   }

  //   // Countries
  //   var data = '';
  //   this.service.get("api/Countries/filtersubregion/"+this.arraysubregionid.toString(),data)
  //   .subscribe(result => { 
  //       if(result=="Not found"){
  //           this.service.notfound();
  //           this.dropdownListCountry = null;
  //       }
  //       else{
  //           this.dropdownListCountry = JSON.parse(result).map((item) => {
  //               return {
  //                 id: item.countries_code,
  //                 itemName: item.countries_name
  //               }
  //             })
  //       } 
  //   },
  //   error => {
  //       this.service.errorserver();
  //   });
  // }
  // onSubRegionDeSelectAll(items: any){
  //   this.dropdownListCountry = this.allcountries;

  //   this.selectedItemsCountry = [];
  // }

  // onCountriesSelect(item: any) {
  //   this.country = [];
  //   this.location.go('/admin/distributor/add-distributor');
  //   this.showAll = false;
  // }

  // OnCountriesDeSelect(item: any) {
  //   if (this.selectedItemsCountry.length == 0) {
  //     this.countryName = "";
  //     this.countryCode = "";
  //     this.geo = "";
  //     this.region = "";
  //     this.subRegion = "";
  //     this.marketName = "";
  //     this.embargoed = "";
  //     this.territory = "";
  //     this.data_1 = [];
  //     this.data_2 = [];
  //     this.data_kontak = [];
  //     this.showAll = false;
  //     this.country = [];
  //     this.showAll = false;
  //   }
  // }

  // onCountriesSelectAll(item: any) {
  //   this.country = [];
  //   this.location.go('/admin/distributor/add-distributor');
  // }

  // onCountriesDeSelectAll(item: any) {
  //   if (this.selectedItemsCountry.length == 0) {
  //     this.countryName = "";
  //     this.countryCode = "";
  //     this.geo = "";
  //     this.region = "";
  //     this.subRegion = "";
  //     this.marketName = "";
  //     this.embargoed = "";
  //     this.territory = "";
  //     this.data_1 = [];
  //     this.data_2 = [];
  //     this.data_kontak = [];
  //     this.showAll = false;
  //     this.country = [];
  //   }
  //   this.location.go('/admin/distributor/add-distributor');
  //   this.showAll = false;
  // }
  //---End of Hide this code for a while

  onSubCountriesSelect(item: any) { }
  OnSubCountriesDeSelect(item: any) { }
  onSubCountriesSelectAll(item: any) { }
  onSubCountriesDeSelectAll(item: any) { }



  getDistributor() {
    if (this.selectedItemsCountry.length == 0) {
      this.validCountry = true;
    } else {
      this.loading = true;
      var data = '';
      // console.log(this.selectedItemsCountry[0].id)
      this.service.httpClientGet("api/Countries/" + this.selectedItemsCountry[0].id, data)
        .subscribe(value => {
          // console.log(value);
          this.data = value;
          this.countryCode = this.data.countries_code;
          this.getDataDistributor(this.data.countries_code);
          this.getContact(this.data.countries_code);
          this.getEtcVar(this.data.countries_code, this.data.MarketTypeId);
          this.data.Embargoed == 0 ? this.embargoed = "No" : this.embargoed = "Yes";
          if (this.selectedItemsGeo.length > 0) {
            this.geo = this.selectedItemsGeo[0]["itemName"];
          }
          else {
            this.geo = "";
          }
          // this.region = this.selectedItemsRegion[0]["itemName"];
          // this.subRegion = this.selectedItemsSubRegion[0]["itemName"];
          this.countryName = this.selectedItemsCountry[0]["itemName"];
          this.showAll = true;
          this.loading = false;
        });
    }

    //Buat object untuk filter yang dipilih
    var params =
    {
      selectGeo: this.selectedItemsGeo,
      dropGeo: this.dropdownListGeo,
      selectCountry: this.selectedItemsCountry,
      dropCountry: this.dropdownListCountry
    }
    //Masukan object filter ke local storage
    localStorage.setItem("filter", JSON.stringify(params));
  }

  getEtcVar(country_code, market_id) {
    var market: any;
    var territoryVal: any;
    this.service.httpClientGet("api/MarketType/" + market_id, market)
      .subscribe(itemMarket => {
        // console.log(itemMarket);
        market = itemMarket;
        market.length != 0 ? this.marketName = market.MarketType : this.marketName = "N/A";
        this.service.get("api/Territory/where/{'CountryCode':'" + country_code + "'}", territoryVal)
          .subscribe(itemTerritory => {
            territoryVal = JSON.parse(itemTerritory);
            territoryVal.length != 0 ? this.territory = territoryVal[0].Territory_Name : this.territory = "N/A";
          },
            error => {
              this.messageError = <any>error
              this.service.errorserver();
            })
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
        });
  }

  getDataDistributor(country_code) {
    this.data_1 = [];
    this.data_2 = [];
    let dataTemp: any;
    this.service.httpClientGet("api/SiteCountryDistributor/getDistributor/{'CountryCode':'" + country_code + "'}", dataTemp)
      .subscribe(result => {
        // console.log(result);
        dataTemp = result;
        // console.log(dataTemp);
        dataTemp.forEach(item => {
          var exist = false;
          if (item.EDistributorType == "MED") {
            if (this.data_1.length == 0) {
              this.data_1.push(item);
            } else {
              this.data_1.forEach(val1 => {
                if (item.SiteId == val1.SiteId) { exist = true; }
              })
              if (exist == false && item.SiteId != "") {
                this.data_1.push(item);
              }
            }
          } else {
            if (this.data_2.length == 0) {
              this.data_2.push(item);
            } else {
              this.data_2.forEach(val2 => {
                if (item.SiteId == val2.SiteId) { exist = true; }
              })
              if (exist == false && item.SiteId != "") {
                this.data_2.push(item);
              }
            }
          }
        });
      },
        error => {
          this.service.errorserver();
        });
  }

  getContact(country_code) {
    this.data_kontak = [];
    var kontak: any;
    this.service.httpClientGet("api/MainContact/GetDistContact/where/{'CountryCode':'" + country_code + "'}", kontak)
      .subscribe(value => {
        // console.log(value);
        kontak = value;
        if (kontak.length != 0) {
          for (let con = 0; con < kontak.length; con++) {
            if (kontak[con] != (undefined || null) && kontak[con].ContactIdInt != "") {
              this.data_kontak.push(kontak[con]);
            }
          }
        }
      },
        error => {
          this.service.errorserver();
        });
  }

  getCountry() {
    // this.countryCode = this.selectedItemsCountry[0]["id"];
    // this.countryName = this.selectedItemsCountry[0]["itemName"];

    var siteNames = '';
    this.service.httpClientGet("api/MainSite", siteNames)
      .subscribe(result => {
        this.siteNames = result;
      },
        error => {
          this.service.errorserver();
        });
    this.siteID = "";
  }

  addContact() {
    var countries = JSON.stringify(this.selectedItemsCountry);
    this.router.navigate(['admin/distributor/add-contact-distributor'], { queryParams: { countries: countries, country_code: this.countryCode } });
  }

  getSiteContact() {
    var site: any;
    this.partnerType = [];
    this.service.httpClientGet("api/Roles", site)
      .subscribe(result => {
        site = result;
        if (site.length != 0) {
          for (let i = 0; i < site.length; i++) {
            this.partnerType.push(site[i]);
          }
        } else {
          swal(
            'Information!',
            'Partner Type Data Not Found',
            'error'
          );
        }
      },
        error => {
          this.service.errorserver();
        });
    this.siteID = "";
  }

  resetFormContact() {
    this.addContactDistributor.reset({
      'PrimaryRoleId': '',
      'FirstName': '',
      'LastName': '',
      'EmailAddress': '',
    });
  }

  selectedSite(newvalue) {
    this.siteID = newvalue;
  }

  getSitebyCountry() {
    this.countryCode = this.selectedItemsCountry[0]["id"];
    var siteID = '';
    this.service.httpClientGet("api/SiteCountryDistributor/where/{'CountryCode':'" + this.countryCode + "'}", siteID)
      .subscribe(result => {
        this.siteID = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  updateDistributorPrimary(site_id, primary, type, country_code) {
    var status: any;
    this.addMasterDistributor.value.SiteID = site_id;
    this.addMasterDistributor.value.CountryCode = country_code;
    this.addMasterDistributor.value.EDistributorType = type;
    if (primary == 'False') {
      status = 1;
    } else {
      status = 0;
    }
    this.addMasterDistributor.value.PrimaryD = status;

    let data = JSON.stringify(this.addMasterDistributor.value);
    // console.log(JSON.parse(data));
    // this.service.httpCLientPut("api/SiteCountryDistributor/where/{'SiteID':'" + site_id + "','CountryCode':'" + country_code + "','EDistributorType':'" + type + "'}",'', data);
    // this.data_1 = [];
    // this.data_2 = [];
    // this.getDistributor();
    this.service.httpCLientPut("api/SiteCountryDistributor/where/{'SiteID':'" + site_id + "','CountryCode':'" + country_code + "','EDistributorType':'" + type + "'}", data)
      .subscribe(result => {
        var update = result;
        if (update["code"] == 1) {
          // if (this.data_1.length > 1 || this.data_2.length > 1 && status == 1) {
          //   if (type == "MED") {
          //     this.primaryAnotherSite(site_id, country_code, type, this.data_1);
          //   } else if (type == "RED") {
          //     this.primaryAnotherSite(site_id, country_code, type, this.data_2);
          //   }
          // }
          this.data_1 = [];
          this.data_2 = [];
          this.getDistributor();
        } else {
          swal(
            'Information!',
            update["message"],
            'error'
          );
        }
      }, error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
  }

  primaryAnotherSite(site_id, country_code, type, dataSource) {
    for (let i = 0; i < dataSource.length; i++) {
      if (dataSource[i].SiteId != site_id) {
        this.setPrimaryAnotherSite.value.SiteID = dataSource[i].SiteId;
        this.setPrimaryAnotherSite.value.CountryCode = dataSource[i].CountryCode;
        this.setPrimaryAnotherSite.value.EDistributorType = dataSource[i].EDistributorType;
        this.setPrimaryAnotherSite.value.PrimaryD = 0;

        let setAnotherSite = JSON.stringify(this.setPrimaryAnotherSite.value);
       // this.service.httpCLientPut("api/SiteCountryDistributor/where/{'SiteID':'" + dataSource[i].SiteId + "','CountryCode':'" + country_code + "','EDistributorType':'" + type + "'}",'', setAnotherSite);
        this.service.httpCLientPut("api/SiteCountryDistributor/where/{'SiteID':'" + dataSource[i].SiteId + "','CountryCode':'" + country_code + "','EDistributorType':'" + type + "'}", setAnotherSite)
          .subscribe(hasil => {
            var updateResult = hasil;
            if (updateResult["code"] != 1) {
              swal(
                'Information!',
                updateResult["message"],
                'error'
              );
            }
          }, error => {
            this.messageError = <any>error
            this.service.errorserver();
          });
      }
    }
  }

  openConfirmsMaster(id, country_code, index) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(result => {
        if (result == true) {
          var data = '';
          this.service.httpClientDelete("api/SiteCountryDistributor/where/{'SiteID':'" + id + "','CountryCode':'" + country_code + "'}", data)
            .subscribe(value => {
              var resource = value;
              if (resource["code"] != 1) {
                swal(
                  'Information!',
                  resource["message"],
                  'error'
                );
              } else {
                var index = this.data_1.findIndex(x => x.SiteId == id);
                if (index !== -1) {
                  this.data_1.splice(index, 1);
                }
              }
            },
              error => {
                this.messageError = <any>error
                this.service.errorserver();
              });
        }

      }).catch(swal.noop);
  }

  openConfirmsRegional(id, country_code, index) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(result => {
        if (result == true) {
          var data = '';
          this.service.httpClientDelete("api/SiteCountryDistributor/where/{'SiteID':'" + id + "','CountryCode':'" + country_code + "'}", data)
            .subscribe(value => {
              var resource = value;
              if (resource["code"] != 1) {
                swal(
                  'Information!',
                  resource["message"],
                  'error'
                );
              } else {
                var index = this.data_2.findIndex(x => x.SiteId == id);
                if (index !== -1) {
                  this.data_2.splice(index, 1);
                }
              }
            },
              error => {
                this.messageError = <any>error
                this.service.errorserver();
              });
        }

      }).catch(swal.noop);
  }

  openConfirmsContact(id, contact_id, index) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    })
      .then(result => {
        if (result == true) {
          var data = '';
          var deleted: any;
          this.service.httpClientDelete("api/MainContact/" + id, data)
            .subscribe(value => {
              var resource = value;
              if (resource['code'] != 1) {
                swal(
                  'Information!',
                  resource["message"],
                  'error'
                );
              } else {
                var index = this.data_kontak.findIndex(x => x.ContactIdInt == id);
                if (index !== -1) {
                  this.data_kontak.splice(index, 1);
                }
                this.service.httpClientDelete("api/MainContact/ContactCountry/" + contact_id, deleted)
                  .subscribe(value1 => {
                    deleted = value1;
                    if (deleted['code'] == 1) {
                      this.getDistributor();
                    } else {
                      swal(
                        'Information!',
                        resource["message"],
                        'error'
                      );
                    }
                  },
                    error => {
                      this.messageError = <any>error
                      this.service.errorserver();
                    });
              }
            },
              error => {
                this.messageError = <any>error
                this.service.errorserver();
              });
        }

      }).catch(swal.noop);
  }

  onSubmitContact() {

    this.addContactDistributor.controls['FirstName'].markAsTouched();
    this.addContactDistributor.controls['PrimaryRoleId'].markAsTouched();
    this.addContactDistributor.controls['EmailAddress'].markAsTouched();

    if (this.addContactDistributor.valid) {

      this.addContactDistributor.value.CountryCode = this.countryCode;

      let data = JSON.stringify(this.addContactDistributor.value);

      this.service.httpClientPost("api/MainContact/ContactDistributor", data)
        .subscribe(result => {
          var hasil = result;
          if (hasil["code"] == "1") {
            this.service.openSuccessSwal(hasil['message']);
            this.getDistributor();
            this.resetFormContact();
          } else {
            swal(
              'Information!',
              hasil["message"],
              'error'
            );
          }
        },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
          });

    }
  }

  updateContactPrimary(contact_id, primary, country_code) {
    var status: any;

    if (primary == 'False') {
      status = 1;
    } else {
      status = 0;
    }

    this.updateContact.value.ContactId = contact_id;
    this.updateContact.value.CountryCode = country_code;
    this.updateContact.value.PrimaryD = status;

    let data = JSON.stringify(this.updateContact.value);
    // this.service.httpCLientPut("api/MainContact/ContactCountry/where/{'ContactId':'" + contact_id + "','CountryCode':'" + country_code + "'}",'', data);
    // if (status == 1) {
    //   this.updateAnotherContact(country_code, contact_id);
    //   // this.service.openSuccessSwal(update["message"]);
    //   this.data_1 = [];
    //   this.data_2 = [];
    //   this.getDistributor();
    // } else {
    //   // this.service.openSuccessSwal(update["message"]);
    //   this.data_1 = [];
    //   this.data_2 = [];
    //   this.getDistributor();
    // }
    this.service.httpCLientPut("api/MainContact/ContactCountry/where/{'ContactId':'" + contact_id + "','CountryCode':'" + country_code + "'}", data)
      .subscribe(result => {
        var update = result;
        if (update["code"] == 1) {
          if (status == 1) {
            this.updateAnotherContact(country_code, contact_id);
            // this.service.openSuccessSwal(update["message"]);
            this.data_1 = [];
            this.data_2 = [];
            this.getDistributor();
          } else {
            // this.service.openSuccessSwal(update["message"]);
            this.data_1 = [];
            this.data_2 = [];
            this.getDistributor();
          }
        } else {
          swal(
            'Information!',
            update["message"],
            'error'
          );
        }
      }, error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
  }

  updateAnotherContact(country_code, contact_id) {
    var contact_temp: any;
    this.service.httpClientGet("api/MainContact/GetContactCountry/where/{'CountryCode':'" + country_code + "'}", contact_temp)
      .subscribe(result1 => {
        contact_temp = result1;
        if (contact_temp.length != 0) {
          for (let i = 0; i < contact_temp.length; i++) {
            if (contact_temp[i].ContactId != contact_id) {
              this.updateContact.value.ContactId = contact_temp[i].ContactId;
              this.updateContact.value.CountryCode = contact_temp[i].CountryCode;
              this.updateContact.value.PrimaryD = 0;

              let anotherContact = JSON.stringify(this.updateContact.value);
              var update: any;
             // this.service.httpCLientPut("api/MainContact/ContactCountry/where/{'CountryCode':'" + country_code + "','ContactId':'" + contact_temp[i].ContactId + "'}", '',anotherContact);
              this.service.httpCLientPut("api/MainContact/ContactCountry/where/{'CountryCode':'" + country_code + "','ContactId':'" + contact_temp[i].ContactId + "'}", anotherContact)
                .subscribe(hasil => {
                  update = hasil;
                  // console.log(update["code"]);
                }, error => {
                  this.messageError = <any>error
                  this.service.errorserver();
                });
            }
          }
        }
      });
  }

  searchDistributorSite(type) {
    var countries = JSON.stringify(this.selectedItemsCountry);
    this.router.navigate(['admin/distributor/add-master-distributor'], { queryParams: { countries: countries, type: type } });
  }
}
