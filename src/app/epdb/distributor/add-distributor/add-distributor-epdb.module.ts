import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDistributorEPDBComponent } from './add-distributor-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { DataFilterContactPipe, DataFilterMasterDistributorPipe, DataFilterRegionalDistributorPipe } from './add-distributor-epdb.component';
import { SessionService } from '../../../shared/service/session.service';
import { LoadingModule } from 'ngx-loading';

export const AddDistributorEPDBRoutes: Routes = [
  {
    path: '',
    component: AddDistributorEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.distributor.add_distributor',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddDistributorEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule,
    LoadingModule
  ],
  declarations: [AddDistributorEPDBComponent, DataFilterContactPipe, DataFilterMasterDistributorPipe, DataFilterRegionalDistributorPipe],
  providers: [AppService, AppFilterGeo, AppFormatDate, SessionService]
})
export class AddDistributorEPDBModule { }
