import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3'; 
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js'; 
import { Http, Headers, Response } from "@angular/http";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { Router } from '@angular/router';

import { AppService } from "../../../shared/service/app.service";
// import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";

import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({ name: 'dataFilterDistributor' })
export class DataFilterDistributorPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row=> 
              (row.SiteId.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
              (row.SiteName.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
              (row.CSOpartner_type.toLowerCase().indexOf(query.toLowerCase()) > -1) || 
              (row.SiteStatus_retired.toLowerCase().indexOf(query.toLowerCase()) > -1));
        } 
        return array;
    }
}

@Component({
  selector     : 'app-list-distributor-epdb',
  templateUrl  : './list-distributor-epdb.component.html',
  styleUrls    : [
    './list-distributor-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class ListDistributorEPDBComponent implements OnInit {

  // dropdownListGeo        = [];
  // selectedItemsGeo       = [];
  // dropdownListRegion     = [];
  // selectedItemsRegion    = [];
  // dropdownListSubRegion  = [];
  // selectedItemsSubRegion = [];
  // dropdownListCountry    = [];
  // selectedItemsCountry   = [];
  // dropdownSettings       = {};

  private _serviceUrl        = 'api/MainSite';
  messageResult     : string = '';
  messageError      : string = '';
  adddistributor    : FormGroup;
  public distributor: any;
  public data       : any;
  public rowsOnPage : number = 10;
  public filterQuery: string = "";
  public sortBy     : string = "type";
  public sortOrder  : string = "asc";
 
  constructor(private router: Router, private service:AppService) { 

    // //validation
    // let PartnerType = new FormControl('', Validators.required);
    // let PartnerName = new FormControl('', Validators.required);
    // let SiebelName = new FormControl('', Validators.required);
    // let Description = new FormControl('', Validators.required);
    // let FrameworkName = new FormControl('', Validators.required);
    // let ApprovedName = new FormControl('', Validators.required);

    // this.addsubpartner = new FormGroup({
    //   PartnerType:PartnerType,
    //   PartnerName:PartnerName,
    //   SiebelName:SiebelName,
    //   Description:Description,
    //   FrameworkName:FrameworkName,
    //   ApprovedName:ApprovedName
    // });
  }

  ngOnInit() {
    this.getAll();

    // Geo
    // var data = '';
    // this.service.httpClientGet("api/Geo",data)
    // .subscribe(result => { 
    //   this.data = result;
    //   this.dropdownListGeo = this.data.map((item) => {
    //     return {
    //       id: item.geo_code,
    //       itemName: item.geo_name
    //     }
    //   }) 
    // },
    // error => {
    //     this.service.errorserver();
    // });
    // this.selectedItemsGeo = [];

    // //setting dropdown
    // this.dropdownSettings = { 
    //   singleSelection   : false, 
    //   text              : "Please Select",
    //   selectAllText     : 'Select All',
    //   unSelectAllText   : 'UnSelect All',
    //   enableSearchFilter: true,
    //   classes           : "myclass custom-class",
    //   disabled          : false
    // };
  }

  // onGeoSelect(item:any){
  //   this.filterGeo.filterGeoOnSelect(item.id, this.dropdownListRegion);
  //   this.selectedItemsRegion    = [];
  //   this.dropdownListSubRegion  = [];
  //   this.dropdownListCountry    = [];
  // }

  // OnGeoDeSelect(item:any){
  //   this.filterGeo.filterGeoOnDeSelect(item.id, this.dropdownListRegion);
  //   this.selectedItemsRegion    = [];
  //   this.selectedItemsSubRegion = [];
  //   this.selectedItemsCountry   = [];
  //   this.dropdownListSubRegion  = [];
  //   this.dropdownListCountry    = [];
  // }

  // onGeoSelectAll(items: any){
  //   this.filterGeo.filterGeoOnSelectAll(this.selectedItemsGeo,this.dropdownListRegion);
  //   this.selectedItemsRegion    = [];
  //   this.dropdownListSubRegion  = [];
  //   this.dropdownListCountry    = [];
  // }

  // onGeoDeSelectAll(items: any){
  //   this.selectedItemsRegion    = [];
  //   this.selectedItemsSubRegion = [];
  //   this.selectedItemsCountry   = [];
  //   this.dropdownListRegion     = [];
  //   this.dropdownListSubRegion  = [];
  //   this.dropdownListCountry    = [];
  // }

  // onRegionSelect(item:any){
  //   this.filterGeo.filterRegionOnSelect(item.id, this.dropdownListSubRegion);
  //   this.selectedItemsSubRegion = [];
  //   this.dropdownListCountry    = [];
  // }

  // OnRegionDeSelect(item:any){
  //   this.filterGeo.filterRegionOnDeSelect(item.id, this.dropdownListSubRegion);
  //   this.selectedItemsSubRegion = [];
  //   this.selectedItemsCountry   = [];
  //   this.dropdownListCountry    = [];
  // }

  // onRegionSelectAll(items: any){
  //   this.filterGeo.filterRegionOnSelectAll(this.selectedItemsRegion, this.dropdownListSubRegion);
  //   this.selectedItemsSubRegion = [];
  //   this.dropdownListCountry    = [];
  // }

  // onRegionDeSelectAll(items: any){
  //   this.selectedItemsSubRegion = [];
  //   this.selectedItemsCountry   = [];
  //   this.dropdownListSubRegion  = [];
  //   this.dropdownListCountry    = [];
  // }

  // onSubRegionSelect(item:any){
  //   this.filterGeo.filterSubRegionOnSelect(item.id, this.dropdownListCountry);
  //   this.selectedItemsCountry   = [];
  // }

  // OnSubRegionDeSelect(item:any){
  //   this.filterGeo.filterSubRegionOnDeSelect(item.id, this.dropdownListCountry);
  //   this.selectedItemsCountry   = [];
  // }

  // onSubRegionSelectAll(items: any){
  //   this.filterGeo.filterSubRegionOnSelectAll(this.selectedItemsSubRegion, this.dropdownListCountry);
  //   this.selectedItemsCountry   = [];
  // }

  // onSubRegionDeSelectAll(items: any){
  //   this.selectedItemsCountry   = [];
  //   this.dropdownListCountry    = [];
  // }

  // onCountriesSelect(item:any){}
  // OnCountriesDeSelect(item:any){}
  // onCountriesSelectAll(item:any){}
  // onCountriesDeSelectAll(item:any){}

  getAll(){
    //get data sites
    var data = '';
    this.service.httpClientGet(this._serviceUrl,data)
    .subscribe(result => {
        this.data = result;
    },
    error => {
        this.service.errorserver();
    });
  }

}
