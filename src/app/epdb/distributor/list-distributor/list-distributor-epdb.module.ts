import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListDistributorEPDBComponent } from './list-distributor-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { AppService } from "../../../shared/service/app.service";
// import { AppFilterGeo } from "../../../shared/filter-geo/app.filter-geo";
// import { AppFormatDate } from "../../../shared/format-date/app.format-date";

import { DataFilterDistributorPipe } from './list-distributor-epdb.component';

export const ListDistributorEPDBRoutes: Routes = [
  {
    path: '',
    component: ListDistributorEPDBComponent,
    data: {
      breadcrumb: 'List Distributor',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListDistributorEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [ListDistributorEPDBComponent, DataFilterDistributorPipe],
  providers:[AppService]
})
export class ListDistributorEPDBModule { }
