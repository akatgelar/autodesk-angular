import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyConversionEditEPDBComponent } from './currency-conversion-edit-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";

export const CurrencyConversionEditEPDBRoutes: Routes = [
  {
    path: '',
    component: CurrencyConversionEditEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.currency.edit_currency_conversion',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CurrencyConversionEditEPDBRoutes),
    SharedModule
  ],
  declarations: [CurrencyConversionEditEPDBComponent],
  providers:[AppService]
})

export class CurrencyConversionEditEPDBModule { }
