import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../shared/service/app.service";

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';

declare const $: any;
declare var Morris: any;

@Component({
    selector: 'app-currency-conversion-edit-epdb',
    templateUrl: './currency-conversion-edit-epdb.component.html',
    styleUrls: [
        './currency-conversion-edit-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class CurrencyConversionEditEPDBComponent implements OnInit {

    private _serviceUrl = 'api/Currency';
    messageResult: string = '';
    messageError: string = '';
    private data;
    currency;
    dataCurrency;
    fyindicator;
    conversion;
    dataConversion;
    currencyValue;

    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "type";
    public sortOrder: string = "asc";
    addCurrency: FormGroup;
    updateCurrency: FormGroup;
    addFinancial: FormGroup;
    addCurrencyConversion: FormGroup;
    updateConversion: FormGroup;
    private id;
    public useraccesdata: any;

    constructor(private router: Router, private service: AppService, private route: ActivatedRoute, private session: SessionService) {

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let Key = new FormControl('', Validators.required);
        let KeyValue = new FormControl('', Validators.required);
        let Parent = new FormControl('');
        let Status = new FormControl('');

        this.addCurrency = new FormGroup({
            Key: Key,
            KeyValue: KeyValue,
            Parent: Parent,
            Status: Status
        });

        let Year = new FormControl('', Validators.required);
        let Currency = new FormControl('', Validators.required);
        let ValuePerUSD = new FormControl('', Validators.required);

        this.addCurrencyConversion = new FormGroup({
            Year: Year,
            Currency: Currency,
            ValuePerUSD: ValuePerUSD,
            Status: Status
        });
    }

    changecurrency(year) {
        if (year != "") {
            console.log(year);
            this.service.httpClientGet(this._serviceUrl + "/SelectByYearCurr/" + year + "/" + this.dataConversion.Currency,  '')
            .subscribe(result => {
                // console.log(result);
                this.currencyValue = result;
            },
                error => {
                    this.service.errorserver();
                });
        }
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        var dataConversion = '';
        this.service.httpClientGet("api/Currency/CurrencyConversion/" + this.id, dataConversion)
            .subscribe(result => {
                this.dataConversion = result;
                this.changecurrency(this.dataConversion.Year);
                if (Object.keys(this.dataConversion).length !== 0 && this.dataConversion.constructor === Object) {
                    this.addCurrencyConversion.patchValue({ Year: this.dataConversion.Year });
                    this.addCurrencyConversion.patchValue({ Currency: this.dataConversion.Currency });
                    this.addCurrencyConversion.patchValue({ ValuePerUSD: this.dataConversion.ValuePerUSD });
                }
            },
                error => {
                    this.service.errorserver();
                });
        // this.dataCurrency = {};
        // this.dataConversion = {};
        // this.getCurrency();
        this.getFYIndicator();
        // this.getCurrencyConversion();
        this.currencyCombobox();
        
    }

    getCurrency() {
        var currency = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'Currencies','Status':'A'}", currency)
            .subscribe(result => {
                this.currency = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    currencyCombobox() {
        var currencyValue = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'Currencies','Status':'A'}", currencyValue)
            .subscribe(result => {
                this.currencyValue = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    getFYIndicator() {
        var fyindicator = '';
        this.service.httpClientGet("api/Currency/where/{'Parent':'FYIndicator','Status':'A'}", fyindicator)
            .subscribe(result => {
                this.fyindicator = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    getCurrencyConversion() {
        var conversion = '';
        this.service.httpClientGet("api/Currency/CurrencyConversion", conversion)
            .subscribe(result => {
                this.conversion = result;
            },
                error => {
                    this.service.errorserver();
                });
    }

    onSubmit() {
        this.addCurrency.controls['Key'].markAsTouched();
        this.addCurrency.controls['KeyValue'].markAsTouched();
        if (this.addCurrency.valid) {
            var check: any;
            this.service.httpClientGet("api/Currency/where/{'Key':'" + this.addCurrency.value.Key + "'}", check)
                .subscribe(result => {
                    check = result;
                    if (check.length != 0) {
                        swal('Information!', 'Duplicate Data In Database', 'error');
                        this.resetCurrency();
                    } else {
                        if (Object.keys(this.dataCurrency).length !== 0 && this.dataCurrency.constructor === Object) {
                            var update = {
                                'Key': this.addCurrency.value.Key,
                                'KeyValue': this.addCurrency.value.KeyValue
                            };
                            this.service.httpCLientPut(this._serviceUrl+'/'+this.dataCurrency.Key, update)
                                .subscribe(res=>{
                                    console.log(res)
                                });
                            //this.service.httpCLientPut(this._serviceUrl,this.dataCurrency.Key, update);
                            this.dataCurrency = {};
                        } else {
                            this.addCurrency.value.Parent = "Currencies";
                            this.addCurrency.value.Status = "A";
                            let data = JSON.stringify(this.addCurrency.value);
                            this.service.httpClientPost(this._serviceUrl, data)
							 .subscribe(result => {
									console.log("success");
								}, error => {
									this.service.errorserver();
								});
                        }
                        this.getCurrency();
                        this.getCurrency();
                        this.addCurrency.reset({
                            'Key': '',
                            'KeyValue': ''
                        });
                    }
                }, error => {
                    this.service.errorserver();
                });
        }
    }

    resetCurrency() {
        this.addCurrency.reset({
            'Key': '',
            'KeyValue': ''
        });
    }

    onSubmitConversion() {
        this.addCurrencyConversion.controls['Year'].markAsTouched();
        this.addCurrencyConversion.controls['Currency'].markAsTouched();
        this.addCurrencyConversion.controls['ValuePerUSD'].markAsTouched();

        if (this.addCurrencyConversion.valid) {
            if (Object.keys(this.dataConversion).length === 0 && this.dataConversion.constructor === Object) {
                this.addCurrencyConversion.value.Status = "A";
                let data = JSON.stringify(this.addCurrencyConversion.value);
                this.service.httpClientPost(this._serviceUrl + "/CurrencyConversion", data)
				 .subscribe(result => {
				console.log("success");
				}, error => {
					this.service.errorserver();
				});
            } else {
                var updateConversion = {
                    'Year': this.addCurrencyConversion.value.Year,
                    'Currency': this.addCurrencyConversion.value.Currency,
                    'ValuePerUSD': this.addCurrencyConversion.value.ValuePerUSD,
                    'Status': 'A',
                    'cuid' : this.useraccesdata.ContactName,
                    'UserId': this.useraccesdata.UserId
                };
                this.service.httpCLientPut(this._serviceUrl + "/CurrencyConversion/"+this.dataConversion.CurrencyConversionId, updateConversion)
                    .subscribe(res=>{
                        console.log(res)
                    });
                //this.service.httpCLientPut(this._serviceUrl + "/CurrencyConversion",this.dataConversion.CurrencyConversionId, updateConversion);
                this.dataConversion = {};
                this.router.navigate(['/admin/currency']);
            }
            this.addCurrencyConversion.reset({
                'Year': '',
                'Currency': '',
                'ValuePerUSD': '',
            });
            this.getCurrencyConversion();
            this.getCurrencyConversion();
        }
    }

    resetConversion() {
        this.addCurrencyConversion.reset({
            'Year': '',
            'Currency': '',
            'ValuePerUSD': '',
        });
    }

    detailCurrency(key) {
        var dataCurrency = '';
        this.service.httpClientGet("api/Currency/" + key, dataCurrency)
            .subscribe(result => {
                this.dataCurrency = result;
                if (Object.keys(this.dataCurrency).length !== 0 && this.dataCurrency.constructor === Object) {
                    this.addCurrency.patchValue({ Key: this.dataCurrency.Key });
                    this.addCurrency.patchValue({ KeyValue: this.dataCurrency.KeyValue });
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    getConversion(id) {
        var dataConversion = '';
        this.service.httpClientGet("api/Currency/CurrencyConversion/" + id, dataConversion)
            .subscribe(result => {
                this.dataConversion = result;
                if (Object.keys(this.dataConversion).length !== 0 && this.dataConversion.constructor === Object) {
                    this.addCurrencyConversion.patchValue({ Year: this.dataConversion.Year });
                    this.addCurrencyConversion.patchValue({ Currency: this.dataConversion.Currency });
                    this.addCurrencyConversion.patchValue({ ValuePerUSD: this.dataConversion.ValuePerUSD });
                }
            },
                error => {
                    this.service.errorserver();
                });
    }

    openConfirmsSwal(id, index) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then(result => {
                if (result == true) {
                    var data = '';
                    this.service.httpClientDelete("api/Currency/CurrencyConversion/" + id, data)
                        .subscribe(value => {
                            var resource = value;
                            this.service.openSuccessSwal(resource['message']);
                            var index = this.conversion.findIndex(x => x.CurrencyConversionId == id);
                            if (index !== -1) {
                                this.conversion.splice(index, 1);
                            }
                        },
                            error => {
                                this.messageError = <any>error
                                this.service.errorserver();
                            });
                }

            }).catch(swal.noop);
    }
}