import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeSiteIdEPDBComponent } from './change-site-id-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ChangeSiteIdEPDBRoutes: Routes = [
{
    path: '',
    component: ChangeSiteIdEPDBComponent,
    data: {
    breadcrumb: 'Administration - Change Site Id',
    icon: 'icofont-home bg-c-blue',
    status: false
    }
}
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ChangeSiteIdEPDBRoutes),
        SharedModule
    ],
    declarations: [ChangeSiteIdEPDBComponent]
})
export class ChangeSiteIdEPDBModule { }