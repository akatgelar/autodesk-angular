import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcademicTargetsReportEPDBComponent } from './academic-targets-report-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const AcademicTargetsReportEPDBRoutes: Routes = [
  {
    path: '',
    component: AcademicTargetsReportEPDBComponent,
    data: {
      breadcrumb: 'Academic Targets Report',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AcademicTargetsReportEPDBRoutes),
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [AcademicTargetsReportEPDBComponent]
})
export class AcademicTargetsReportEPDBModule { }
