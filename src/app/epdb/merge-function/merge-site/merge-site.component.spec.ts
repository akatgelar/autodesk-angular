import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeSiteComponent } from './merge-site.component';

describe('MergeSiteComponent', () => {
  let component: MergeSiteComponent;
  let fixture: ComponentFixture<MergeSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
