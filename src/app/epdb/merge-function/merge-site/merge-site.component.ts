import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import swal from 'sweetalert2';

@Component({
  selector: 'app-merge-site',
  templateUrl: './merge-site.component.html',
  styleUrls: ['./merge-site.component.css']
})
export class MergeSiteComponent implements OnInit {

  loading = false
  searchId = ""
  dup_data:any = {}
  master_data:any = {
    partnerTypes: [],
    contacts: [],
    journalEntries: [],
    attachments: []
  }
  base_master_data:any = {}

  constructor(private router:ActivatedRoute, private service:AppService, private r:Router) { }

  ngOnInit() {
    this.router.params.subscribe((params)=>{
      this.service.httpClientGet('api/Merge/GetSite?siteId='+params['id'],{}).subscribe((res:any)=>{
        this.dup_data = res.data
        if (this.dup_data != null) {
      
          if (this.dup_data.siteCity != null && this.dup_data.siteCity != '') {
            this.dup_data.siteCity = this.service.decoder(this.dup_data.siteCity);
          }
          if (this.dup_data.shippingCity != null && this.dup_data.shippingCity != '') {
            this.dup_data.shippingCity = this.service.decoder(this.dup_data.shippingCity);
          }
          if (this.dup_data.mailingCity != null && this.dup_data.mailingCity != '') {
            this.dup_data.mailingCity = this.service.decoder(this.dup_data.mailingCity);
          }
          if (this.dup_data.shippingAddress1 != null && this.dup_data.shippingAddress1 != '') {
            this.dup_data.shippingAddress1 = this.service.decoder(this.dup_data.shippingAddress1);
          }
          if (this.dup_data.shippingAddress2 != null && this.dup_data.shippingAddress2 != '') {
            this.dup_data.shippingAddress2 = this.service.decoder(this.dup_data.shippingAddress2);
          }
          if (this.dup_data.shippingAddress3 != null && this.dup_data.shippingAddress3 != '') {
            this.dup_data.shippingAddress3 = this.service.decoder(this.dup_data.shippingAddress3);
          }
          if (this.dup_data.mailingAddress1 != null && this.dup_data.mailingAddress1 != '') {
            this.dup_data.mailingAddress1 = this.service.decoder(this.dup_data.mailingAddress1);
          }
          if (this.dup_data.mailingAddress2 != null && this.dup_data.mailingAddress2 != '') {
            this.dup_data.mailingAddress2 = this.service.decoder(this.dup_data.mailingAddress2);
          }
          if (this.dup_data.mailingAddress3 != null && this.dup_data.mailingAddress3 != '') {
            this.dup_data.mailingAddress3 = this.service.decoder(this.dup_data.mailingAddress3);
          }
          if (this.dup_data.siteAddress1 != null && this.dup_data.siteAddress1 != '') {
            this.dup_data.siteAddress1 = this.service.decoder(this.dup_data.siteAddress1);
          }
          if (this.dup_data.siteAddress2 != null && this.dup_data.siteAddress2 != '') {
            this.dup_data.siteAddress2 = this.service.decoder(this.dup_data.siteAddress2);
          }
          if (this.dup_data.siteAddress3 != null && this.dup_data.siteAddress3 != '') {
            this.dup_data.siteAddress3 = this.service.decoder(this.dup_data.siteAddress3);
          }
          if (this.dup_data.siteWebAddress != null && this.dup_data.siteWebAddress != '') {
            this.dup_data.siteWebAddress = this.service.decoder(this.dup_data.siteWebAddress);
          }
          if (this.dup_data.siteAdminFirstName != null && this.dup_data.siteAdminFirstName != '') {
            this.dup_data.siteAdminFirstName = this.service.decoder(this.dup_data.siteAdminFirstName);
          }
          if (this.dup_data.siteAdminLastName != null && this.dup_data.siteAdminLastName != '') {
            this.dup_data.siteAdminLastName = this.service.decoder(this.dup_data.siteAdminLastName);
          }
          if (this.dup_data.siteManagerFirstName != null && this.dup_data.siteManagerFirstName != '') {
            this.dup_data.siteManagerFirstName = this.service.decoder(this.dup_data.siteManagerFirstName);
          }
          if (this.dup_data.siteManagerLastName != null && this.dup_data.siteManagerLastName != '') {
            this.dup_data.siteManagerLastName = this.service.decoder(this.dup_data.siteManagerLastName);
          }
          // if (this.dup_data.DateAdded != null && this.dup_data.DateAdded != '') {
          //   this.dup_data.DateAdded = this.service.decoder(this.dup_data.DateAdded);
          // }
        }
      })
    })
  }

  search(){
    if(this.searchId.toLowerCase().trim()==this.dup_data.siteId.toLowerCase())
      swal('Error','Cant Use Dup Site Id For Search','error')
    else{
      this.loading = true
      this.service.httpClientGet('api/Merge/GetSite?siteId='+this.searchId,{}).subscribe((res:any)=>{
        this.loading = false
        if(res.code==200)
          this.base_master_data = {...res.data}
        else
          swal('Error','No Site found','error')
      })
    }
  }

  checkField(event,fieldName,data){
    if(event.target.checked){
      if(Array.isArray(this.dup_data[fieldName])){
        this.master_data[fieldName].push(data)
      }else{
        this.master_data[fieldName] = this.dup_data[fieldName]
      }
    }else{
      if(Array.isArray(this.dup_data[fieldName])){
        if(data.type)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.type!=data.type)
        if(data.contactId)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.contactId!=data.contactId)
        if(data.journalId)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.journalId!=data.journalId)
        if(data.id)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.id!=data.id)
      }else{
        delete this.master_data[fieldName]
      }
    }
  }

  merge(){
    this.loading = true
    this.service.httpCLientPut('api/Merge/MarkSiteDuplicate?siteId='+this.dup_data.siteId,{}).subscribe((res:any)=>{
      if(res.code==200){
        this.service.httpClientPost('api/Merge/MergeSite?newSiteId='+this.base_master_data.siteId,this.master_data).subscribe((res: any)=>{
          this.loading = false;
          if(res.code==200){
            this.search()
            swal('Success','Sites are merged successfully. The active site is '+this.base_master_data.siteId,'success').then((value)=>{
              this.r.navigate(['/manage/site/search-site']);
            })
          }else{
            swal('ERROR','Merge Site Failed','error')
          }
        })
      }else{
        this.loading = false
        swal('ERROR','Merge Site Failed','error')
      }
    })
    
  }

  reset(){
    this.base_master_data = {};
    this.master_data = {
      partnerTypes: [],
      contacts: [],
      journalEntries: [],
      attachments: []
    }
    let inputs = document.querySelectorAll("input[type='checkbox']");
    for(var i = 0; i < inputs.length; i++) {
      inputs[i]['checked'] = false;   
    }
  }
}
