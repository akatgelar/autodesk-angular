import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import swal from 'sweetalert2';

@Component({
  selector: 'app-merge-student',
  templateUrl: './merge-student.component.html',
  styleUrls: ['./merge-student.component.css']
})
export class MergeStudentComponent implements OnInit {

  loading = false
  searchId = ""
  dup_data:any = {}
  master_data:any = {
    additionSites: [],
    journalEntries: []
  }
  base_master_data:any = {}

  constructor(private router:ActivatedRoute, private service:AppService, private r:Router) { }

  ngOnInit() {
    this.router.params.subscribe((params)=>{
      this.service.httpClientGet('api/Merge/GetStudent?studentId='+params['id'],{}).subscribe((res:any)=>{
        this.dup_data = res.data
        if (this.dup_data.firstname != null && this.dup_data.firstname != '') {
          this.dup_data.firstname = this.service.decoder(this.dup_data.firstname);
      }
      if (this.dup_data.lastname != null && this.dup_data.lastname != '') {
        this.dup_data.lastname = this.service.decoder(this.dup_data.lastname);
      }
      })
    })
  }

  search(){
    console.log(this.dup_data.studentID)
    if(this.searchId.toLowerCase().trim()==this.dup_data.studentID)
      swal('Error','Cant Use Dup Student Id For Search','error')
    else{
      this.loading = true
      this.service.httpClientGet('api/Merge/GetStudent?studentId='+this.searchId,{}).subscribe((res:any)=>{
        this.loading = false
        if(res.code==200)
          this.base_master_data = {...res.data}
        else
          swal('Error','No Student found','error')
      })
    }
  }

  checkField(event,fieldName,data){
    if(event.target.checked){
      if(Array.isArray(this.dup_data[fieldName])){
        this.master_data[fieldName].push(data)
      }else{
        this.master_data[fieldName] = this.dup_data[fieldName]
      }
    }else{
      if(Array.isArray(this.dup_data[fieldName])){
        if(data.siteId)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.siteId!=data.siteId)
        if(data.journalId){
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.journalId!=data.journalId)
        }
      }else{
        delete this.master_data[fieldName]
      }
    }
  }

  merge(){
    this.loading = true
    this.service.httpCLientPut('api/Merge/MarkStudentDuplicate?studentId='+this.dup_data.studentID,{}).subscribe((res:any)=>{
      if(res.code==200){
        this.service.httpClientPost('api/Merge/MergeStudent?newStudentId='+this.base_master_data.studentID,this.master_data).subscribe((res: any)=>{
          this.loading = false
          if(res.code==200){
            this.search()
            swal('Success','Students are merged successfully. The active student is '+this.base_master_data.studentID,'success').then((value)=>{
              this.r.navigate(['/studentinfo/search-student']);
            })
          }else{
            swal('ERROR','Merge Student Failed','error')
          }
        })
      }else{
        this.loading = false
        swal('ERROR','Merge Student Failed','error')
      }
    })
    
  }

  reset(){
    this.base_master_data = {};
    this.master_data = {
      additionSites: [],
      journalEntries: []
    }
    let inputs = document.querySelectorAll("input[type='checkbox']");
    for(var i = 0; i < inputs.length; i++) {
      inputs[i]['checked'] = false;   
    }
  }

}
