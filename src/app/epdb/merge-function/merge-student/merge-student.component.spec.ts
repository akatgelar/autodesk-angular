import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeStudentComponent } from './merge-student.component';

describe('MergeStudentComponent', () => {
  let component: MergeStudentComponent;
  let fixture: ComponentFixture<MergeStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
