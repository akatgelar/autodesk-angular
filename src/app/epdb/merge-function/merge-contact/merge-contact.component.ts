import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import swal from 'sweetalert2';

@Component({
  selector: 'app-merge-contact',
  templateUrl: './merge-contact.component.html',
  styleUrls: ['./merge-contact.component.css']
})
export class MergeContactComponent implements OnInit {

  loading = false
  searchId = ""
  dup_data:any = {}
  master_data:any = {
    additionSites: [],
    journalEntries: []
  }
  base_master_data:any = {}

  constructor(private router:ActivatedRoute, private service:AppService, private r:Router) { }

  ngOnInit() {
    this.router.params.subscribe((params)=>{
      this.service.httpClientGet('api/Merge/GetContact?contactId='+params['id'],{}).subscribe((res:any)=>{
        this.dup_data = res.data
        if (this.dup_data != null) {
          
          if (this.dup_data.firstName != null && this.dup_data.firstName != "") {
            this.dup_data.firstName = this.service.decoder(this.dup_data.firstName);
          }
          if (this.dup_data.lastName != null && this.dup_data.lastName != "") {
            this.dup_data.lastName = this.service.decoder(this.dup_data.lastName);
          }
          
        }
      })
    })
  }

  search(){
    if(this.searchId.toLowerCase().trim()==this.dup_data.contactId.toLowerCase())
      swal('Error','Cant Use Dup Contact Id For Search','error')
    else{
      this.loading = true
      this.service.httpClientGet('api/Merge/GetContact?contactId='+this.searchId,{}).subscribe((res:any)=>{
        this.loading = false
        if(res.code==200)
          this.base_master_data = {...res.data}
        else
          swal('Error','No Contact found','error')
      })
    }
  }

  checkField(event,fieldName,data){
    if(event.target.checked){
      if(Array.isArray(this.dup_data[fieldName])){
        this.master_data[fieldName].push(data)
      }else{
        this.master_data[fieldName] = this.dup_data[fieldName]
      }
    }else{
      if(Array.isArray(this.dup_data[fieldName])){
        if(data.siteId)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.siteId!=data.siteId)
        if(data.journalId){
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.journalId!=data.journalId)
        }
      }else{
        delete this.master_data[fieldName]
      }
    }
  }

  merge(){
    this.loading = true
    this.service.httpCLientPut('api/Merge/MarkContactDuplicate?contactId='+this.dup_data.contactId,{}).subscribe((res:any)=>{
      if(res.code==200){
        this.service.httpClientPost('api/Merge/MergeContact?newContactId='+this.base_master_data.contactId,this.master_data).subscribe((res: any)=>{
          this.loading = false
          if(res.code==200){
            this.search()
            swal('Success','Contacts are merged successfully. The active contact is '+this.base_master_data.contactId,'success').then((value)=>{
              this.r.navigate(['/manage/contact/search-contact']);
            })
          }else{
            swal('ERROR','Merge Contact Failed','error')
          }
        })
      }else{
        this.loading = false
        swal('ERROR','Merge Contact Failed','error')
      }
    })
    
  }

  reset(){
    this.base_master_data = {};
    this.master_data = {
      additionSites: [],
      journalEntries: []
    }
    let inputs = document.querySelectorAll("input[type='checkbox']");
    for(var i = 0; i < inputs.length; i++) {
      inputs[i]['checked'] = false;   
    }
  }

}
