import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeContactComponent } from './merge-contact.component';

describe('MergeContactComponent', () => {
  let component: MergeContactComponent;
  let fixture: ComponentFixture<MergeContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
