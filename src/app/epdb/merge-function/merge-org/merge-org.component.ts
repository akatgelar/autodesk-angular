import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from "../../../shared/service/app.service";
import swal from 'sweetalert2';

@Component({
  selector: 'app-merge-org',
  templateUrl: './merge-org.component.html',
  styleUrls: ['./merge-org.component.css']
})
export class MergeOrgComponent implements OnInit {

  loading = false
  searchId = ""
  dup_data:any = {}
  master_data:any = {
    sites:[],
    journalEntries: []
  }
  base_master_data:any = {}

  constructor(private router:ActivatedRoute, private service:AppService,private r:Router) { }

  ngOnInit() {
    this.router.params.subscribe((params)=>{
      this.service.httpClientGet('api/Merge/GetOrg?orgId='+params['id'],{}).subscribe((res:any)=>{
        this.dup_data = res.data
        
                    this.dup_data.atcdirectorFirstName = this.service.decoder(this.dup_data.atcdirectorFirstName);
                    this.dup_data.atcdirectorLastName = this.service.decoder(this.dup_data.atcdirectorLastName);
                    this.dup_data.legalContactFirstName = this.service.decoder(this.dup_data.legalContactFirstName);
                    this.dup_data.legalContactLastName = this.service.decoder(this.dup_data.legalContactLastName);
                    this.dup_data.billingContactFirstName = this.service.decoder(this.dup_data.billingContactFirstName);
                    this.dup_data.billingContactLastName = this.service.decoder(this.dup_data.billingContactLastName);
                    this.dup_data.registeredAddress1 = this.service.decoder(this.dup_data.registeredAddress1);
                    this.dup_data.registeredAddress2 = this.service.decoder(this.dup_data.registeredAddress2);
                    this.dup_data.registeredAddress3 = this.service.decoder(this.dup_data.registeredAddress3);
                    this.dup_data.registeredCity = this.service.decoder(this.dup_data.registeredCity);
                    this.dup_data.invoicingAddress1 = this.service.decoder(this.dup_data.invoicingAddress1);
                    this.dup_data.invoicingAddress2 = this.service.decoder(this.dup_data.invoicingAddress2);
                    this.dup_data.invoicingAddress3 = this.service.decoder(this.dup_data.invoicingAddress3);
                    this.dup_data.invoicingCity = this.service.decoder(this.dup_data.invoicingCity);
                    this.dup_data.contractAddress1 = this.service.decoder(this.dup_data.contractAddress1);
                    this.dup_data.contractAddress2= this.service.decoder(this.dup_data.contractAddress2);
                    this.dup_data.contractAddress3 = this.service.decoder(this.dup_data.contractAddress3);
                    this.dup_data.contractCity = this.service.decoder(this.dup_data.contractCity);
                  
      })
    })
  }

  searchOrg(){
    if(this.searchId.toLowerCase().trim()==this.dup_data.orgId.toLowerCase())
      swal('Error','Cant Use Dup Org Id For Search','error')
    else{
      this.loading = true;
      this.service.httpClientGet('api/Merge/GetOrg?orgId='+this.searchId,{}).subscribe((res:any)=>{
        this.loading = false;
        if(res.code==200)
          this.base_master_data = {...res.data}
        else
          swal('Error','No Org found','error')
      })
    }
  }

  checkField(event,fieldName,data){
    if(event.target.checked){
      if(Array.isArray(this.dup_data[fieldName])){
        this.master_data[fieldName].push(data)
      }else{
        this.master_data[fieldName] = this.dup_data[fieldName]
      }
    }else{
      if(Array.isArray(this.dup_data[fieldName])){
        if(data.siteId)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.siteId!=data.siteId)
        if(data.journalId)
          this.master_data[fieldName] = this.master_data[fieldName].filter((el)=> el.journalId!=data.journalId)
      }else{
        delete this.master_data[fieldName]
      }
    }
  }

  merge(){
    this.loading = true
    this.service.httpCLientPut('api/Merge/MarkOrgDuplicate?orgId='+this.dup_data.orgId,{}).subscribe((res:any)=>{
      if(res.code==200){
        this.service.httpClientPost('api/Merge/MergeOrg?newOrgId='+this.base_master_data.orgId,this.master_data).subscribe((res: any)=>{
          this.loading = false;
          if(res.code==200){
            this.searchOrg()
            swal('Success','Organisations are merged successfully. The active organisation is '+this.base_master_data.orgId,'success').then((value)=>{
              this.r.navigate(['/manage/organization/search-organization']);
            })
          }else{
            swal('ERROR','Merge Org Failed','error')
          }
        })
      }else{
        this.loading = false
        swal('ERROR','Merge Org Failed','error')
      }
    })
    
  }

  reset(){
    this.base_master_data = {};
    this.master_data = {
      sites:[],
      journalEntries: []
    }
    let inputs = document.querySelectorAll("input[type='checkbox']");
    for(var i = 0; i < inputs.length; i++) {
      inputs[i]['checked'] = false;   
    }
  }
}
