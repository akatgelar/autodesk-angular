import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeOrgComponent } from './merge-org.component';

describe('MergeOrgComponent', () => {
  let component: MergeOrgComponent;
  let fixture: ComponentFixture<MergeOrgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeOrgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeOrgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
