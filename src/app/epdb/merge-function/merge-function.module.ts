import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { MergeOrgComponent } from './merge-org/merge-org.component';
import { MergeSiteComponent } from './merge-site/merge-site.component';
import { MergeContactComponent } from './merge-contact/merge-contact.component';
import { MergeStudentComponent } from './merge-student/merge-student.component';

export const SKUEPDBRoutes: Routes = [
  {
    path: 'org/:id',
    component: MergeOrgComponent,
    data: {
      breadcrumb: 'Merge org',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: 'site/:id',
    component: MergeSiteComponent,
    data: {
      breadcrumb: 'Merge site',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: 'contact/:id',
    component: MergeContactComponent,
    data: {
      breadcrumb: 'Merge contact',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  },
  {
    path: 'student/:id',
    component: MergeStudentComponent,
    data: {
      breadcrumb: 'Merge student',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LoadingModule,
    RouterModule.forChild(SKUEPDBRoutes)
  ],
  declarations: [MergeOrgComponent, MergeSiteComponent, MergeContactComponent, MergeStudentComponent],
  providers:[AppService]
})
export class MergeFunctionModule { }
