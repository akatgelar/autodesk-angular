import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-dashboard-epdb',
  templateUrl: './dashboard-epdb.component.html',
  styleUrls: [
    './dashboard-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class DashboardEPDBComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {}

}
