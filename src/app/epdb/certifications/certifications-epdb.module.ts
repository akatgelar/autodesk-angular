import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificationsEPDBComponent } from './certifications-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const CertificationsEPDBRoutes: Routes = [
  {
    path: '',
    component: CertificationsEPDBComponent,
    data: {
      breadcrumb: 'Learning History',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CertificationsEPDBRoutes),
    SharedModule
  ],
  declarations: [CertificationsEPDBComponent]
})
export class CertificationsEPDBModule { }
