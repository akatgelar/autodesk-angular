import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactJournalEPDBComponent } from './add-contact-journal-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { LoadingModule } from 'ngx-loading';

export const AddContactJournalEPDBRoutes: Routes = [
    {
        path: '',
        component: AddContactJournalEPDBComponent,
        data: {
            breadcrumb: 'epdb.manage.contact.partner_type_spec.add_contact_journal_entries',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AddContactJournalEPDBRoutes),
        SharedModule,
        // AngularMultiSelectModule
        LoadingModule
    ],
    declarations: [AddContactJournalEPDBComponent],
    providers: [AppService, AppFormatDate]
})
export class AddContactJournalEPDBModule { }
