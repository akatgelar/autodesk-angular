import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditOrganizationEPDBComponent } from './edit-organization-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const EditOrganizationEPDBRoutes: Routes = [
  {
    path: '',
    component: EditOrganizationEPDBComponent,
    data: {
      breadcrumb: 'epdb.manage.organization.add_edit.edit_organization',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditOrganizationEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [EditOrganizationEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class EditOrganizationEPDBModule { }
