import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

@Component({
  selector: 'app-edit-organization-epdb',
  templateUrl: './edit-organization-epdb.component.html',
  styleUrls: [
    './edit-organization-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class EditOrganizationEPDBComponent implements OnInit {

  @ViewChild('fileUploader') fileUploader 
  private attachments = []
  private attachments_remove = []
  private _serviceUrl = 'api/MainOrganization';
  messageError: string = '';
  editorganization: FormGroup;
  public datadetail: any;
  public datacountry: any;
  public datastate1: any;
  public datastate2: any;
  public datastate3: any;
  public useraccesdata: any;
  public loading = false;
  public dataSite: any;
  public partnertype: any;
  private OrgId: string;
  public isAvailableLogo: boolean = false;
  public orgLogoUpload = false;
  public imgLogoName1 = [];
  //public imgsrc = [];
  imgLogoName: string = "";
  imgSrc: string = "";
  logoId: string= "0";
  //site datatable
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "SiteName";
  public sortOrder: string = "desc";

  private PrimarySiteId: string;

  constructor(private router: Router, private session: SessionService, private service: AppService, private formatdate: AppFormatDate, private route: ActivatedRoute) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    let OrgName = new FormControl('', Validators.required);
    let EnglishOrgName = new FormControl('');
    let CommercialOrgName = new FormControl('');
    let ATCCSN = new FormControl('');
    let VARCSN = new FormControl('');
    let AUPCSN = new FormControl('');
    let TaxExempt = new FormControl('');
    let VATNumber = new FormControl('');
    let YearJoined = new FormControl('');
    let OrgWebAddress = new FormControl('');
    let RegisteredDepartment = new FormControl('');
    let RegisteredAddress1 = new FormControl('', Validators.required);
    let RegisteredAddress2 = new FormControl('');
    let RegisteredAddress3 = new FormControl('');
    let RegisteredCity = new FormControl('', Validators.required);
    let RegisteredStateProvince = new FormControl('');
    let RegisteredPostalCode = new FormControl('');
    let RegisteredCountryCode = new FormControl('', Validators.required);
    let InvoicingDepartment = new FormControl('');
    let InvoicingAddress1 = new FormControl('', Validators.required);
    let InvoicingAddress2 = new FormControl('');
    let InvoicingAddress3 = new FormControl('');
    let InvoicingCity = new FormControl('', Validators.required);
    let InvoicingStateProvince = new FormControl('');
    let InvoicingPostalCode = new FormControl('');
    let InvoicingCountryCode = new FormControl('', Validators.required);
    let ContractDepartment = new FormControl('');
    let ContractAddress1 = new FormControl('', Validators.required);
    let ContractAddress2 = new FormControl('');
    let ContractAddress3 = new FormControl('');
    let ContractCity = new FormControl('', Validators.required);
    let ContractStateProvince = new FormControl('');
    let ContractPostalCode = new FormControl('');
    let ContractCountryCode = new FormControl('', Validators.required);
    let ATCDirectorFirstName = new FormControl('', Validators.required);
    let ATCDirectorLastName = new FormControl('', Validators.required);
    let ATCDirectorEmailAddress = new FormControl('', Validators.required);
    let ATCDirectorTelephone = new FormControl('', Validators.required);
    let ATCDirectorFax = new FormControl('');
    let LegalContactFirstName = new FormControl('', Validators.required);
    let LegalContactLastName = new FormControl('', Validators.required);
    let LegalContactEmailAddress = new FormControl('', Validators.required);
    let LegalContactTelephone = new FormControl('', Validators.required);
    let LegalContactFax = new FormControl('');
    let BillingContactFirstName = new FormControl('', Validators.required);
    let BillingContactLastName = new FormControl('', Validators.required);
    let BillingContactEmailAddress = new FormControl('', Validators.required);
    let BillingContactTelephone = new FormControl('', Validators.required);
    let BillingContactFax = new FormControl('');
    let AdminNotes = new FormControl('');

    let OrganizationId = new FormControl();
    let OrgId = new FormControl();
    let OrgIdATCOrgId = new FormControl();
    let OrgStatus_retired = new FormControl();
    let SAPSoldTo = new FormControl();
    let Status = new FormControl();
    let DateAdded = new FormControl();
    let AddedBy = new FormControl();
    let DateLastAdmin = new FormControl();
    let LastAdminBy = new FormControl();
    let CSOResellerUUID = new FormControl();
    let CSOResellerName = new FormControl();
    let CSOPartnerManager = new FormControl();
    let CSOUpdatedOn = new FormControl();
    let CSOVersion = new FormControl();
    let PrimarySiteId = new FormControl();
    let Files = new FormControl();
    let orgLogo = new FormControl();
    let LogoId = new FormControl();
    

    this.editorganization = new FormGroup({
      OrganizationId: OrganizationId,
      OrgId: OrgId,
      OrgIdATCOrgId: OrgIdATCOrgId,
      OrgStatus_retired: OrgStatus_retired,
      OrgName: OrgName,
      EnglishOrgName: EnglishOrgName,
      CommercialOrgName: CommercialOrgName,
      YearJoined: YearJoined,
      TaxExempt: TaxExempt,
      VATNumber: VATNumber,
      SAPSoldTo: SAPSoldTo,
      VARCSN: VARCSN,
      ATCCSN: ATCCSN,
      AUPCSN: AUPCSN,
      OrgWebAddress: OrgWebAddress,
      RegisteredDepartment: RegisteredDepartment,
      RegisteredAddress1: RegisteredAddress1,
      RegisteredAddress2: RegisteredAddress2,
      RegisteredAddress3: RegisteredAddress3,
      RegisteredCity: RegisteredCity,
      RegisteredStateProvince: RegisteredStateProvince,
      RegisteredPostalCode: RegisteredPostalCode,
      RegisteredCountryCode: RegisteredCountryCode,
      InvoicingDepartment: InvoicingDepartment,
      InvoicingAddress1: InvoicingAddress1,
      InvoicingAddress2: InvoicingAddress2,
      InvoicingAddress3: InvoicingAddress3,
      InvoicingCity: InvoicingCity,
      InvoicingStateProvince: InvoicingStateProvince,
      InvoicingPostalCode: InvoicingPostalCode,
      InvoicingCountryCode: InvoicingCountryCode,
      ContractDepartment: ContractDepartment,
      ContractAddress1: ContractAddress1,
      ContractAddress2: ContractAddress2,
      ContractAddress3: ContractAddress3,
      ContractCity: ContractCity,
      ContractStateProvince: ContractStateProvince,
      ContractPostalCode: ContractPostalCode,
      ContractCountryCode: ContractCountryCode,
      ATCDirectorFirstName: ATCDirectorFirstName,
      ATCDirectorLastName: ATCDirectorLastName,
      ATCDirectorEmailAddress: ATCDirectorEmailAddress,
      ATCDirectorTelephone: ATCDirectorTelephone,
      ATCDirectorFax: ATCDirectorFax,
      LegalContactFirstName: LegalContactFirstName,
      LegalContactLastName: LegalContactLastName,
      LegalContactEmailAddress: LegalContactEmailAddress,
      LegalContactTelephone: LegalContactTelephone,
      LegalContactFax: LegalContactFax,
      BillingContactFirstName: BillingContactFirstName,
      BillingContactLastName: BillingContactLastName,
      BillingContactEmailAddress: BillingContactEmailAddress,
      BillingContactTelephone: BillingContactTelephone,
      BillingContactFax: BillingContactFax,
      AdminNotes: AdminNotes,
      Status: Status,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy,
      CSOResellerUUID: CSOResellerUUID,
      CSOResellerName: CSOResellerName,
      CSOPartnerManager: CSOPartnerManager,
      CSOUpdatedOn: CSOUpdatedOn,
      CSOVersion: CSOVersion,
      PrimarySiteId: PrimarySiteId,
      Files: Files,
      orgLogo: orgLogo,
      LogoId: LogoId
    });

  }

  ngOnInit() {

    //get data organization
    var id = this.route.snapshot.params['id'];
    this.OrgId = this.route.snapshot.params['orgId'];

    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + id, data)
      .subscribe(result => {
        this.datadetail = result;
        this.datadetail.RegisteredStateProvince = this.service.decoder(this.datadetail.RegisteredStateProvince);
        this.datadetail.ContractStateProvince = this.service.decoder(this.datadetail.ContractStateProvince);
        this.datadetail.InvoicingStateProvince = this.service.decoder(this.datadetail.InvoicingStateProvince);
        this.datadetail.DateAdded = this.service.decoder(this.datadetail.DateAdded);
        this.attachments = this.datadetail.Files || [];
        this.imgLogoName1 = this.datadetail.orgLogo || [];
        this.PrimarySiteId = this.datadetail.PrimarySiteId;
        this.buildForm();
      },
        error => {
          this.messageError = <any>error
          this.service.errorserver();
          this.datadetail = null;
        });

    //get data country
    var data = '';
    this.service.httpClientGet('api/Countries', data)
      .subscribe(result => {
        this.datacountry = result;
      },
        error => {
          this.service.errorserver();
        });

    this.showsite(this.OrgId);
  }

  showsite(id) {
    var data = '';
    this.service.httpClientGet("api/MainSite/getsiteswithcountcontact/" + id, data)
      .subscribe(result => {
        // console.log(result);
        if (result == "Not found") {
          this.dataSite = '';
        } else {
          this.dataSite = result;
          for (var i = 0; i < this.dataSite.length; i++) {
            if (this.dataSite[i].SiteName != null && this.dataSite[i].SiteName != '') {
                this.dataSite[i].SiteName = this.service.decoder(this.dataSite[i].SiteName);
            }
          }
        }
      },
        error => {
          this.service.errorserver();
          this.dataSite = '';
        });

    this.service.httpClientGet('api/MainSite/partnertypeonsiteorg/' + id, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.partnertype = '';
        } else {
          this.partnertype = result;
        }
      },
        error => {
          this.service.errorserver();
          this.partnertype = '';
        });
  }

  onCheckboxChange(siteId) {
    this.PrimarySiteId = siteId;
  }

  getState1(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate1 = Object.keys(result).map(function (Index) {
          return {
            RegisteredStateProvince: result[Index].StateName
          }
        })
       // this.datastate1 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  getState2(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate2 = Object.keys(result).map(function (Index) {
          return {
            ContractStateProvince: result[Index].StateName
          }
        })
        //this.datastate2 = result;
      },
        error => {
          this.service.errorserver();
        });
  }
  
  getState3(value) {
    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + value + '"}', data)
      .subscribe(result => {
        this.datastate3 = Object.keys(result).map(function (Index) {
          return {
            InvoicingStateProvince: result[Index].StateName
          }
        })
        //this.datastate3 = result;
      },
        error => {
          this.service.errorserver();
        });
  }

  //submit form
  onSubmit() {

    this.editorganization.controls['OrgName'].markAsTouched();
    this.editorganization.controls['RegisteredAddress1'].markAsTouched();
    this.editorganization.controls['RegisteredCity'].markAsTouched();
    this.editorganization.controls['RegisteredCountryCode'].markAsTouched();
    this.editorganization.controls['InvoicingAddress1'].markAsTouched();
    this.editorganization.controls['InvoicingCity'].markAsTouched();
    this.editorganization.controls['InvoicingCountryCode'].markAsTouched();
    this.editorganization.controls['ContractAddress1'].markAsTouched();
    this.editorganization.controls['ContractAddress2'].markAsTouched();
    this.editorganization.controls['ContractCity'].markAsTouched();
    this.editorganization.controls['ContractCountryCode'].markAsTouched();
    this.editorganization.controls['ATCDirectorFirstName'].markAsTouched();
    this.editorganization.controls['ATCDirectorLastName'].markAsTouched();
    this.editorganization.controls['ATCDirectorEmailAddress'].markAsTouched();
    this.editorganization.controls['ATCDirectorTelephone'].markAsTouched();
    this.editorganization.controls['LegalContactFirstName'].markAsTouched();
    this.editorganization.controls['LegalContactLastName'].markAsTouched();
    this.editorganization.controls['LegalContactEmailAddress'].markAsTouched();
    this.editorganization.controls['LegalContactTelephone'].markAsTouched();
    this.editorganization.controls['BillingContactFirstName'].markAsTouched();
    this.editorganization.controls['BillingContactLastName'].markAsTouched();
    this.editorganization.controls['BillingContactEmailAddress'].markAsTouched();
    this.editorganization.controls['BillingContactTelephone'].markAsTouched();

    if (this.editorganization.valid) {
      let fileData = []
      this.fileUploader.files.map((file)=>{
        let reader = new FileReader()
        let name = file.name
        reader.onload = ()=>{
          fileData.push({ name:name, data:reader.result })
          if(this.fileUploader.files.length == fileData.length)
            this.editorganization.patchValue({
              Files: fileData
            })
        }
        reader.readAsDataURL(file);
      })
      swal({
        title: 'Are you sure?',
        text: "If you wish to Edit Organization!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, sure!'
      }).then(result => {
        if (result == true) {
          this.loading = true;
          this.editorganization.value.LogoId = this.datadetail.LogoId;
          this.editorganization.value.LastAdminBy = this.useraccesdata.ContactName;
          this.editorganization.value.PrimarySiteId = this.PrimarySiteId;
          this.editorganization.value.AddedBy = this.useraccesdata.ContactName;
          this.editorganization.value.OrgName = this.service.encoder(this.editorganization.value.OrgName);
          this.editorganization.value.EnglishOrgName = this.service.encoder(this.editorganization.value.EnglishOrgName);
          this.editorganization.value.CommercialOrgName = this.service.encoder(this.editorganization.value.CommercialOrgName);
          this.editorganization.value.RegisteredDepartment = this.service.encoder(this.editorganization.value.RegisteredDepartment);
          this.editorganization.value.RegisteredAddress1 = this.service.encoder(this.editorganization.value.RegisteredAddress1);
          this.editorganization.value.RegisteredAddress2 = this.service.encoder(this.editorganization.value.RegisteredAddress2);
          this.editorganization.value.RegisteredAddress3 = this.service.encoder(this.editorganization.value.RegisteredAddress3);
          this.editorganization.value.RegisteredCity = this.service.encoder(this.editorganization.value.RegisteredCity);
          this.editorganization.value.InvoicingDepartment = this.service.encoder(this.editorganization.value.InvoicingDepartment);
          this.editorganization.value.InvoicingAddress1 = this.service.encoder(this.editorganization.value.InvoicingAddress1);
          this.editorganization.value.InvoicingAddress2 = this.service.encoder(this.editorganization.value.InvoicingAddress2);
          this.editorganization.value.InvoicingAddress3 = this.service.encoder(this.editorganization.value.InvoicingAddress3);
          this.editorganization.value.InvoicingCity = this.service.encoder(this.editorganization.value.InvoicingCity);
          this.editorganization.value.ContractDepartment = this.service.encoder(this.editorganization.value.ContractDepartment);
          this.editorganization.value.ContractAddress1 = this.service.encoder(this.editorganization.value.ContractAddress1);
          this.editorganization.value.ContractAddress2 = this.service.encoder(this.editorganization.value.ContractAddress2);
          this.editorganization.value.ContractAddress3 = this.service.encoder(this.editorganization.value.ContractAddress3);
          this.editorganization.value.ContractCity = this.service.encoder(this.editorganization.value.ContractCity);
          this.editorganization.value.ATCDirectorFirstName = this.service.encoder(this.editorganization.value.ATCDirectorFirstName);
          this.editorganization.value.ATCDirectorLastName = this.service.encoder(this.editorganization.value.ATCDirectorLastName);
          this.editorganization.value.LegalContactFirstName = this.service.encoder(this.editorganization.value.LegalContactFirstName);
          this.editorganization.value.LegalContactLastName = this.service.encoder(this.editorganization.value.LegalContactLastName);
          this.editorganization.value.BillingContactFirstName = this.service.encoder(this.editorganization.value.BillingContactFirstName);
          this.editorganization.value.BillingContactLastName = this.service.encoder(this.editorganization.value.BillingContactLastName);
          this.editorganization.value.AdminNotes = this.service.encoder(this.editorganization.value.AdminNotes);

          //Audit Log
          this.editorganization.value.cuid = this.useraccesdata.ContactName;
          this.editorganization.value.UserId = this.useraccesdata.UserId;
          //End Audit Log
    
          //convert object to json
          let data = JSON.stringify(this.editorganization.value);

          let finaldata = data.replace(/\'/g, "\\\\'");
          finaldata.replace(/"/g, '\"');
         
          // console.log(JSON.parse(finaldata));
          //put action
		  console.log(this.attachments_remove);
          this.service.httpClientPost('api/MainOrganization/DeleteFile',this.attachments_remove).subscribe(res=>{
              var result = res;
			  if(result['code'] == '1'){
				  this.service.httpCLientPut(this._serviceUrl+'/'+this.editorganization.value.OrganizationId, finaldata)
					.subscribe(res=>{
            if(res['code'] == '1'){
            this.router.navigate(['/manage/organization/detail-organization', this.editorganization.value.OrganizationId]);
            this.loading = false;
            }
					});
				}else{
					this.loading = false;
					swal(
							'Error!',
							'Failed to delete the attachments!',
							'error'
					)
				}
            });
          

          // setTimeout(() => {
          //   //redirect
          //   this.router.navigate(['/manage/organization/detail-organization', this.editorganization.value.OrganizationId]);
          //   this.loading = false;
          // }, 3000)
        }
      }).catch(swal.noop);
    }
    else {
      this.loading = false;
      if(this.fileUploader.valid())
        swal(
          'Field is Required!',
          'Please enter the Add Organization form required!',
          'error'
        )
      else
        swal(
          'Field is Required!',
          'Please enter the Edit Organization form required!',
          'error'
        )
    }


  }

  buildForm(): void {

    let imageLogo = {};
    let img;
    if (this.datadetail.orgLogo != "") {
      imageLogo = {
        filename: this.datadetail.LogoName,
        filetype: this.datadetail.LogoType,
        value: this.datadetail.orgLogo
      }
      this.imgSrc = "data:image/png;base64," + this.datadetail.orgLogo;
      this.imgLogoName = this.datadetail.LogoName;
      this.isAvailableLogo = true;
      this.orgLogoUpload = false;
    } else {
      this.isAvailableLogo = false;
      this.orgLogoUpload = true;
      imageLogo = {
        filename: "",
        filetype: "",
        value: ""
      }
    }
    let OrganizationId = new FormControl(this.datadetail.OrganizationId);
    let OrgName = new FormControl(this.service.decoder(this.datadetail.OrgName), Validators.required);
    let EnglishOrgName = new FormControl(this.service.decoder(this.datadetail.EnglishOrgName));
    let CommercialOrgName = new FormControl(this.service.decoder(this.datadetail.CommercialOrgName));
    let ATCCSN = new FormControl(this.datadetail.ATCCSN);
    let VARCSN = new FormControl(this.datadetail.VARCSN);
    let AUPCSN = new FormControl(this.datadetail.AUPCSN);
    let TaxExempt = new FormControl(this.datadetail.TaxExempt);
    let VATNumber = new FormControl(this.datadetail.VATNumber);
    let YearJoined = new FormControl(this.datadetail.YearJoined);
    let OrgWebAddress = new FormControl(this.service.decoder(this.datadetail.OrgWebAddress));
    let RegisteredDepartment = new FormControl(this.service.decoder(this.datadetail.RegisteredDepartment));
    let RegisteredAddress1 = new FormControl(this.service.decoder(this.datadetail.RegisteredAddress1), Validators.required);
    let RegisteredAddress2 = new FormControl(this.service.decoder(this.datadetail.RegisteredAddress2));
    let RegisteredAddress3 = new FormControl(this.service.decoder(this.datadetail.RegisteredAddress3));
    let RegisteredCity = new FormControl(this.service.decoder(this.datadetail.RegisteredCity), Validators.required);
    let RegisteredStateProvince = new FormControl(this.service.decoder(this.datadetail.RegisteredStateProvince));
    let RegisteredPostalCode = new FormControl(this.datadetail.RegisteredPostalCode);
    let RegisteredCountryCode = new FormControl(this.datadetail.RegisteredCountryCode, Validators.required);
    let InvoicingDepartment = new FormControl(this.service.decoder(this.datadetail.InvoicingDepartment));
    let InvoicingAddress1 = new FormControl(this.service.decoder(this.datadetail.InvoicingAddress1), Validators.required);
    let InvoicingAddress2 = new FormControl(this.service.decoder(this.datadetail.InvoicingAddress2));
    let InvoicingAddress3 = new FormControl(this.service.decoder(this.datadetail.InvoicingAddress3));
    let InvoicingCity = new FormControl(this.service.decoder(this.datadetail.InvoicingCity), Validators.required);
    let InvoicingStateProvince = new FormControl(this.service.decoder(this.datadetail.InvoicingStateProvince));
    let InvoicingPostalCode = new FormControl(this.datadetail.InvoicingPostalCode);
    let InvoicingCountryCode = new FormControl(this.datadetail.InvoicingCountryCode, Validators.required);
    let ContractDepartment = new FormControl(this.service.decoder(this.datadetail.ContractDepartment));
    let ContractAddress1 = new FormControl(this.service.decoder(this.datadetail.ContractAddress1), Validators.required);
    let ContractAddress2 = new FormControl(this.service.decoder(this.datadetail.ContractAddress2));
    let ContractAddress3 = new FormControl(this.service.decoder(this.datadetail.ContractAddress3));
    let ContractCity = new FormControl(this.service.decoder(this.datadetail.ContractCity), Validators.required);
    let ContractStateProvince = new FormControl(this.service.decoder(this.datadetail.ContractStateProvince));
    let ContractPostalCode = new FormControl(this.datadetail.ContractPostalCode);
    let ContractCountryCode = new FormControl(this.datadetail.ContractCountryCode, Validators.required);
    let ATCDirectorFirstName = new FormControl(this.service.decoder(this.datadetail.ATCDirectorFirstName), Validators.required);
    let ATCDirectorLastName = new FormControl(this.service.decoder(this.datadetail.ATCDirectorLastName), Validators.required);
    let ATCDirectorEmailAddress = new FormControl(this.datadetail.ATCDirectorEmailAddress, [Validators.required, Validators.email]);
    let ATCDirectorTelephone = new FormControl(this.datadetail.ATCDirectorTelephone, Validators.required);
    let ATCDirectorFax = new FormControl(this.datadetail.ATCDirectorFax);
    let LegalContactFirstName = new FormControl(this.service.decoder(this.datadetail.LegalContactFirstName), Validators.required);
    let LegalContactLastName = new FormControl(this.service.decoder(this.datadetail.LegalContactLastName), Validators.required);
    let LegalContactEmailAddress = new FormControl(this.datadetail.LegalContactEmailAddress, [Validators.required, Validators.email]);
    let LegalContactTelephone = new FormControl(this.datadetail.LegalContactTelephone, Validators.required);
    let LegalContactFax = new FormControl(this.datadetail.LegalContactFax);
    let BillingContactFirstName = new FormControl(this.service.decoder(this.datadetail.BillingContactFirstName), Validators.required);
    let BillingContactLastName = new FormControl(this.service.decoder(this.datadetail.BillingContactLastName), Validators.required);
    let BillingContactEmailAddress = new FormControl(this.datadetail.BillingContactEmailAddress, [Validators.required, Validators.email]);
    let BillingContactTelephone = new FormControl(this.datadetail.BillingContactTelephone, Validators.required);
    let BillingContactFax = new FormControl(this.datadetail.BillingContactFax);
    let AdminNotes = new FormControl(this.service.decoder(this.datadetail.AdminNotes));

    let OrgId = new FormControl(this.datadetail.OrgId);
    let ATCOrgId = new FormControl(this.datadetail.ATCOrgId);
    let OrgStatus_retired = new FormControl(this.datadetail.OrgStatus_retired);
    let SAPSoldTo = new FormControl(this.datadetail.SAPSoldTo);
    let Status = new FormControl(this.datadetail.Status);
    let DateAdded = new FormControl(this.datadetail.DateAdded);
    let AddedBy = new FormControl(this.datadetail.AddedBy);
    let DateLastAdmin = new FormControl(this.datadetail.DateLastAdmin);
    let LastAdminBy = new FormControl(this.datadetail.LastAdminBy);
    let CSOResellerUUID = new FormControl(this.datadetail.CSOResellerUUID);
    let CSOResellerName = new FormControl(this.datadetail.CSOResellerName);
    let CSOPartnerManager = new FormControl(this.datadetail.CSOPartnerManager);
    let CSOUpdatedOn = new FormControl(this.datadetail.CSOUpdatedOn);
    let CSOVersion = new FormControl(this.datadetail.CSOVersion);
    let PrimarySiteId = new FormControl(this.datadetail.PrimarySiteId);
    let Files = new FormControl();
    let orgLogo = new FormControl(imageLogo);
    this.webaddress =this.service.decoder(this.datadetail.OrgWebAddress);
    this.logoId = this.datadetail.LogoId;

    //get data state
    var data = '';
    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.RegisteredCountryCode + '"}', data)
      .subscribe(result => {
        //this.datastate1 = result;
        this.datastate1 = Object.keys(result).map(function (Index) {
          return {
            RegisteredStateProvince: result[Index].StateName
          }
        })
      },
        error => {
          this.service.errorserver();
        });

    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.InvoicingCountryCode + '"}', data)
      .subscribe(result => {
        this.datastate2 = Object.keys(result).map(function (Index) {
          return {
            ContractStateProvince: result[Index].StateName
          }
        })
       // this.datastate2 = result;
      },
        error => {
          this.service.errorserver();
        });

    this.service.httpClientGet('api/State/where/{"CountryCode":"' + this.datadetail.ContractCountryCode + '"}', data)
      .subscribe(result => {
        this.datastate3 = Object.keys(result).map(function (Index) {
          return {
            InvoicingStateProvince: result[Index].StateName
          }
        })
        //this.datastate3 = result;
      },
        error => {
          this.service.errorserver();
        });
    
    this.editorganization = new FormGroup({
      OrganizationId: OrganizationId,
      OrgId: OrgId,
      ATCOrgId: ATCOrgId,
      OrgStatus_retired: OrgStatus_retired,
      OrgName: OrgName,
      EnglishOrgName: EnglishOrgName,
      CommercialOrgName: CommercialOrgName,
      YearJoined: YearJoined,
      TaxExempt: TaxExempt,
      VATNumber: VATNumber,
      SAPSoldTo: SAPSoldTo,
      VARCSN: VARCSN,
      ATCCSN: ATCCSN,
      AUPCSN: AUPCSN,
      OrgWebAddress: OrgWebAddress,
      RegisteredDepartment: RegisteredDepartment,
      RegisteredAddress1: RegisteredAddress1,
      RegisteredAddress2: RegisteredAddress2,
      RegisteredAddress3: RegisteredAddress3,
      RegisteredCity: RegisteredCity,
      RegisteredStateProvince: RegisteredStateProvince,
      RegisteredPostalCode: RegisteredPostalCode,
      RegisteredCountryCode: RegisteredCountryCode,
      InvoicingDepartment: InvoicingDepartment,
      InvoicingAddress1: InvoicingAddress1,
      InvoicingAddress2: InvoicingAddress2,
      InvoicingAddress3: InvoicingAddress3,
      InvoicingCity: InvoicingCity,
      InvoicingStateProvince: InvoicingStateProvince,
      InvoicingPostalCode: InvoicingPostalCode,
      InvoicingCountryCode: InvoicingCountryCode,
      ContractDepartment: ContractDepartment,
      ContractAddress1: ContractAddress1,
      ContractAddress2: ContractAddress2,
      ContractAddress3: ContractAddress3,
      ContractCity: ContractCity,
      ContractStateProvince: ContractStateProvince,
      ContractPostalCode: ContractPostalCode,
      ContractCountryCode: ContractCountryCode,
      ATCDirectorFirstName: ATCDirectorFirstName,
      ATCDirectorLastName: ATCDirectorLastName,
      ATCDirectorEmailAddress: ATCDirectorEmailAddress,
      ATCDirectorTelephone: ATCDirectorTelephone,
      ATCDirectorFax: ATCDirectorFax,
      LegalContactFirstName: LegalContactFirstName,
      LegalContactLastName: LegalContactLastName,
      LegalContactEmailAddress: LegalContactEmailAddress,
      LegalContactTelephone: LegalContactTelephone,
      LegalContactFax: LegalContactFax,
      BillingContactFirstName: BillingContactFirstName,
      BillingContactLastName: BillingContactLastName,
      BillingContactEmailAddress: BillingContactEmailAddress,
      BillingContactTelephone: BillingContactTelephone,
      BillingContactFax: BillingContactFax,
      AdminNotes: AdminNotes,
      Status: Status,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateLastAdmin: DateLastAdmin,
      LastAdminBy: LastAdminBy,
      CSOResellerUUID: CSOResellerUUID,
      CSOResellerName: CSOResellerName,
      CSOPartnerManager: CSOPartnerManager,
      CSOUpdatedOn: CSOUpdatedOn,
      CSOVersion: CSOVersion,
      PrimarySiteId: PrimarySiteId,
      Files: Files,
      orgLogo: orgLogo
    });
  }

  //reset form
  resetForm() {
    this.attachments_remove = []
    this.editorganization.reset({
      'OrgName': this.service.decoder(this.datadetail.OrgName),
      'EnglishOrgName': this.service.decoder(this.datadetail.EnglishOrgName),
      'CommercialOrgName': this.service.decoder(this.datadetail.CommercialOrgName),
      'ATCCSN': this.datadetail.ATCCSN,
      'VARCSN': this.datadetail.VARCSN,
      'AUPCSN': this.datadetail.AUPCSN,
      'TaxExempt': this.datadetail.TaxExempt,
      'VATNumber': this.datadetail.VATNumber,
      'OrgWebAddress': this.service.decoder(this.datadetail.OrgWebAddress),
      'RegisteredAddress1': this.service.decoder(this.datadetail.RegisteredAddress1),
      'RegisteredAddress2': this.service.decoder(this.datadetail.RegisteredAddress2),
      'RegisteredAddress3': this.service.decoder(this.datadetail.RegisteredAddress3),
      'RegisteredCity': this.service.decoder(this.datadetail.RegisteredCity),
      'RegisteredCountryCode': this.datadetail.RegisteredCountryCode,
      'RegisteredStateProvince': this.datadetail.RegisteredStateProvince,
      'RegisteredPostalCode': this.datadetail.RegisteredPostalCode,
      'ContractDepartment': this.service.decoder(this.datadetail.ContractDepartment),
      'ContractAddress1': this.service.decoder(this.datadetail.ContractAddress1),
      'ContractAddress2': this.service.decoder(this.datadetail.ContractAddress2),
      'ContractAddress3': this.service.decoder(this.datadetail.ContractAddress3),
      'ContractCity': this.service.decoder(this.datadetail.ContractCity),
      'ContractCountryCode': this.datadetail.ContractCountryCode,
      'ContractStateProvince': this.datadetail.ContractStateProvince,
      'ContractPostalCode': this.datadetail.ContractPostalCode,
      'InvoicingDepartment': this.service.decoder(this.datadetail.InvoicingDepartment),
      'InvoicingAddress1': this.service.decoder(this.datadetail.InvoicingAddress1),
      'InvoicingAddress2': this.service.decoder(this.datadetail.InvoicingAddress2),
      'InvoicingAddress3': this.service.decoder(this.datadetail.InvoicingAddress3),
      'InvoicingCity': this.service.decoder(this.datadetail.InvoicingCity),
      'InvoicingCountryCode': this.datadetail.InvoicingCountryCode,
      'InvoicingStateProvince': this.datadetail.InvoicingStateProvince,
      'InvoicingPostalCode': this.datadetail.InvoicingPostalCode,
      'ATCDirectorFirstName': this.service.decoder(this.datadetail.ATCDirectorFirstName),
      'ATCDirectorLastName': this.service.decoder(this.datadetail.ATCDirectorLastName),
      'ATCDirectorEmailAddress': this.datadetail.ATCDirectorEmailAddress,
      'ATCDirectorTelephone': this.datadetail.ATCDirectorTelephone,
      'ATCDirectorFax': this.datadetail.ATCDirectorFax,
      'LegalContactFirstName': this.service.decoder(this.datadetail.LegalContactFirstName),
      'LegalContactLastName': this.service.decoder(this.datadetail.LegalContactLastName),
      'LegalContactEmailAddress': this.datadetail.LegalContactEmailAddress,
      'LegalContactTelephone': this.datadetail.LegalContactTelephone,
      'LegalContactFax': this.datadetail.LegalContactFax,
      'BillingContactFirstName': this.service.decoder(this.datadetail.BillingContactFirstName),
      'BillingContactLastName': this.service.decoder(this.datadetail.BillingContactLastName),
      'BillingContactEmailAddress': this.datadetail.BillingContactEmailAddress,
      'BillingContactTelephone': this.datadetail.BillingContactTelephone,
      'BillingContactFax': this.datadetail.BillingContactFax,
      'AdminNotes': this.service.decoder(this.datadetail.AdminNotes),
    });
  }

  webaddress: string = "";
  getwebaddress(value) {
    this.webaddress = value;
  }

  testwebaddress() {
    if (this.webaddress != "") {
      window.open("//" + this.webaddress);
    } else {
      alert("insert web address");
    }
  }

  // for button Copy
  ContractDepartment = "";
  ContractAddress1 = "";
  ContractAddress2 = "";
  ContractAddress3 = "";
  ContractCity = "";
  ContractCountryCode = "";
  ContractStateProvince = "";
  ContractPostalCode = "";
  getRegisteredContract() {
    this.ContractDepartment = this.editorganization.value.RegisteredDepartment;
    this.ContractAddress1 = this.editorganization.value.RegisteredAddress1;
    this.ContractAddress2 = this.editorganization.value.RegisteredAddress2;
    this.ContractAddress3 = this.editorganization.value.RegisteredAddress3;
    this.ContractCity = this.editorganization.value.RegisteredCity;
    this.editorganization.patchValue({ ContractCountryCode: this.editorganization.value.RegisteredCountryCode });
    this.getState2(this.editorganization.value.RegisteredCountryCode);
    this.editorganization.patchValue({ ContractStateProvince: this.editorganization.value.RegisteredStateProvince });
    this.ContractPostalCode = this.editorganization.value.RegisteredPostalCode;
  }

  InvoicingDepartment = "";
  InvoicingAddress1 = "";
  InvoicingAddress2 = "";
  InvoicingAddress3 = "";
  InvoicingCity = "";
  InvoicingCountryCode = "";
  InvoicingStateProvince = "";
  InvoicingPostalCode = "";
  getRegisteredInvoice() {
    this.InvoicingDepartment = this.editorganization.value.RegisteredDepartment;
    this.InvoicingAddress1 = this.editorganization.value.RegisteredAddress1;
    this.InvoicingAddress2 = this.editorganization.value.RegisteredAddress2;
    this.InvoicingAddress3 = this.editorganization.value.RegisteredAddress3;
    this.InvoicingCity = this.editorganization.value.RegisteredCity;
    this.editorganization.patchValue({ InvoicingCountryCode: this.editorganization.value.RegisteredCountryCode });
    this.getState3(this.editorganization.value.RegisteredCountryCode);
    this.editorganization.patchValue({ InvoicingStateProvince: this.editorganization.value.RegisteredStateProvince });
    this.InvoicingPostalCode = this.editorganization.value.RegisteredPostalCode;
  }

  LegalContactFirstName = "";
  LegalContactLastName = "";
  LegalContactEmailAddress = "";
  LegalContactTelephone = "";
  LegalContactFax = "";
  getATCLegal() {
    this.LegalContactFirstName = this.editorganization.value.ATCDirectorFirstName;
    this.LegalContactLastName = this.editorganization.value.ATCDirectorLastName;
    this.LegalContactEmailAddress = this.editorganization.value.ATCDirectorEmailAddress;
    this.LegalContactTelephone = this.editorganization.value.ATCDirectorTelephone;
    this.LegalContactFax = this.editorganization.value.ATCDirectorFax;
  }

  BillingContactFirstName = "";
  BillingContactLastName = "";
  BillingContactEmailAddress = "";
  BillingContactTelephone = "";
  BillingContactFax = "";
  getATCBilling() {
    this.BillingContactFirstName = this.editorganization.value.ATCDirectorFirstName;
    this.BillingContactLastName = this.editorganization.value.ATCDirectorLastName;
    this.BillingContactEmailAddress = this.editorganization.value.ATCDirectorEmailAddress;
    this.BillingContactTelephone = this.editorganization.value.ATCDirectorTelephone;
    this.BillingContactFax = this.editorganization.value.ATCDirectorFax;
  }
  // for button Copy

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.editorganization.get('orgLogo').setValue({
          filename: file.name,
          filetype: file.type,
          value: (<string>reader.result).split(',')[1]
        })
      };
    }
  }
  RemoveLogo() {
    let imageLogo = {};
    imageLogo = {
      filename: this.datadetail.LogoName,
      filetype: this.datadetail.LogoType,
      value: this.datadetail.orgLogo
    }
    this.imgSrc = "data:image/png;base64," + this.datadetail.orgLogo;
    this.imgLogoName = this.datadetail.LogoName;
    swal({
      title: 'Are you sure?',
      text: "You will lost earlier logo!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, sure!'
    }).then(result => {
      if (result == true) {
        this.isAvailableLogo = false;
        this.orgLogoUpload = true;
        this.editorganization.get('orgLogo').setValue({
          filename: '',
          filetype: '',
          value: ''
        })
      }
    });

  }

  clearLogo() {
    this.isAvailableLogo = false;
    this.orgLogoUpload = true;
  }
}
