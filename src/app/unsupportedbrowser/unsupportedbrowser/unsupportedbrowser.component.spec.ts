import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnsupportedbrowserComponent } from './unsupportedbrowser.component';

describe('UnsupportedbrowserComponent', () => {
  let component: UnsupportedbrowserComponent;
  let fixture: ComponentFixture<UnsupportedbrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnsupportedbrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsupportedbrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
